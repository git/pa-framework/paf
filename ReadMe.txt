To build the libraries from Windows command prompt:
1. In pa\build\tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
2. In pa\build\target.mk, update the variable 'ROOTDIR' to reflect PAF installation path
3. Install sed version 4.2.1 from http://gnuwin32.sourceforge.net/packages/sed.htm and update Windows path variable so that sed is in the execution path
4. Install python version 2.7.6 from http://www.python.org/download
5. If Cygwin is already installed on the machine, ensure Cygwin binaries are not present in locations specified in the Windows path variable
6. From pa\build directory issue the following commands
	- <CCS installation path>\ccsv5\utils\bin\gmake clean
	- <CCS installation path>\ccsv5\utils\bin\gmake install
7. To build 'i' area issue the following commands from pa\build
	- <CCS installation path>\ccsv5\utils\bin\gmake ROM=K001 clean
	- <CCS installation path>\ccsv5\utils\bin\gmake ROM=K001 install

To build the libraries from Cygwin:
1. Copy the contents of pa/build/rules_cygwin_linux.mk to pa/build/rules.mk 
2. Copy the the contents of pa/build/target_cygwin_linux.mk to pa/build/target.mk
3. Copy the the contents of pa/build/tools_cygwin.mk to pa/build/tools.mk
4. In pa/build/tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
5. In pa/build/target.mk
	- update the variable 'ROOTDIR' to reflect PAF installation path
6. Ensure python, sed and make packages are installed on Cygwin
7. From pa/build directory issue the following commands
	- make clean
	- make install
8. To build 'i' area issue the following commands from pa/build
	- make ROM=K001 clean
	- make ROM=K001 install

To build the libraries from Linux:
1. Copy the contents of pa/build/rules_cygwin_linux.mk to pa/build/rules.mk 
2. Copy the the contents of pa/build/target_cygwin_linux.mk to pa/build/target.mk
3. In pa/build/tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
4. In pa/build/target.mk
	- update the variable 'ROOTDIR' to reflect PAF installation path
5. In pa/build/rules.mk
	- Replace all the occurences of `cygpath -w $<` with $<
6. Ensure python, sed and make packages are installed on Linux host
7. From pa/build directory issue the following commands
	- make clean
	- make install
8. To build 'i' area issue the following commands from pa/build
	- make ROM=K001 clean
	- make ROM=K001 install

To build Y15 Project from CCS:
1. Import the project in CCS. Project is in pa/f/s19/y15 folder
2. Go to project properties in CCS and update all the defined varible values at the following locations
	- Build -> Variables 
	- Build -> C6000 Linker->Command File Processing
	- Resource -> Linked Resources -> Path Variables
