
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX CBR Service Routine data structures
//
//
//

#ifndef CBR_H_ 
#define CBR_H_ 

#include <cbr.inc>
#include <fifo.h>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* CBR macros */
#define CBR_MKEE(QTSL,SYNC,TCC,ATCINT,TCINT,RLOAD,EWM,PTE,PP,ETYPE) \
    ((((QTSL)&CBR_QTSL_MSK)<<CBR_QTSL_SHFT) |       \
     (((SYNC)&CBR_SYNC_MSK)<<CBR_SYNC_SHFT) |       \
     (((PP)>>4&CBR_PP_P3_MSK)<<CBR_PP_P3_SHFT) |    \
     (((TCC)&CBR_TCC_MSK)<<CBR_TCC_SHFT) |          \
     (((ATCINT)&CBR_ATCINT_MSK)<<CBR_ATCINT_SHFT) | \
     (((TCINT)&CBR_TCINT_MSK)<<CBR_TCINT_SHFT) |    \
     (((PP)>>3&CBR_PP_P2_MSK)<<CBR_PP_P2_SHFT) |    \
     (((RLOAD)&CBR_RLOAD_MSK)<<CBR_RLOAD_SHFT) |    \
     (((EWM)&CBR_EWM_MSK)<<CBR_EWM_SHFT) |          \
     (((PTE)>>2&CBR_PTE_MSK)<<CBR_PTE_SHFT) |       \
     (((PP)&CBR_PP_P1_MSK)<<CBR_PP_P1_SHFT) |       \
     (((ETYPE)&CBR_ETYPE_MSK)<<CBR_ETYPE_SHFT))

/* CBR parameter format */
typedef struct cbr_cnt{
    Uint32 cnt0:16;
    Uint32 cnt1:15;
    Uint32 rs:1;
}cbr_cnt;

typedef struct cbr{
    Uint32 dst;
    fifo *fd;
    union{
        Uint32 full;
        cbr_cnt part;
    }cnt;
    Int16 dst_idx0;
    Uint16 res1;
    Int16 dst_idx1;
    Uint16 res2;
    union{
        Uint32 full;
        cbr_cnt part;
    }cnt_ref;
    Uint32 dst_rld0;
    Uint32 dst_rld1;
    Uint32 dt0;
    Uint32 dt1;
    Uint32 res3;
}cbr;

/* CBR API declarations */
extern Uint32 dMAX_cbrOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 pp,Uint32 tcc,Fxn fxn);
extern void dMAX_cbrClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 pp,Uint32 tcc,Uint32 pt);
extern void dMAX_cbrCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern void dMAX_cbrCfgPT(dMAX_Handle handle,Uint32 pt,cbr *pcbr);
extern void dMAX_cbrCopy(dMAX_Handle handle,Uint32 evt);
extern void dMAX_cbrNoWait(dMAX_Handle handle,Uint32 tcc);
#endif // CBR_H_

