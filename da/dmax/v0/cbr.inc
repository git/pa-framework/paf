//
// $Source: /cvsstl/ti2/da/dmax/v0/cbr.inc,v $
// $Revision: 1.4 $
//
// dMAX CBR Service Routine constants
//
// Copyright 2005, Texas Instruments India.  All rights reserved.
//
// $Log: cbr.inc,v $
//

#ifndef CBR_
#define CBR_

/* CBR Event Entry constants */
#define CBR_ETYPE_MSK  0x1F    // ETYPE Mask
#define CBR_ETYPE_SHFT 0       // ETYPE Shift

#define CBR_PP_P1_MSK  0x7     // PP Part1 Mask
#define CBR_PP_P1_SHFT 5       // PP Part1 Shift 
#define CBR_PP_P2_MSK  1       // PP Part2 Mask
#define CBR_PP_P2_SHFT 21      // PP Part2 Shift 
#define CBR_PP_P3_MSK  1       // PP Part3 Mask
#define CBR_PP_P3_SHFT 28      // PP Part3 Shift 

#define CBR_PTE_MSK    0x7F    // PTE Mask
#define CBR_PTE_SHFT   8       // PTE Shift

#define CBR_EWM_DIS    0       // EVM Disable
#define CBR_EWM_ENA    1       // EVM Enable
#define CBR_EWM_MSK    1       // EVM Mask 
#define CBR_EWM_SHFT   16      // EVM Shift 

#define CBR_RLOAD_DIS  0       // RLOAD disabled 
#define CBR_RLOAD_ENA  1       // RLOAD enabled 
#define CBR_RLOAD_MSK  1       // RLOAD Mask 
#define CBR_RLOAD_SHFT 20      // RLOAD Shift 

#define CBR_TCINT_DIS  0       // TCINT disabled
#define CBR_TCINT_ENA  1       // TCINT enabled
#define CBR_TCINT_MSK  1       // TCINT Mask 
#define CBR_TCINT_SHFT 22      // TCINT Shift 

#define CBR_ATCINT_DIS 0       // ATCINT disabled
#define CBR_ATCINT_ENA 1       // ATCINT enabled
#define CBR_ATCINT_MSK 1       // ATCINT Mask 
#define CBR_ATCINT_SHFT 23     // ATCINT Shift 

#define CBR_TCC_MSK    0x0F    // TCC Mask
#define CBR_TCC_SHFT   24      // TCC Shift

#define CBR_SYNC_DIS   0       // Sync disabled
#define CBR_SYNC_ENA   1       // Sync enabled
#define CBR_SYNC_MSK   1       // Sync Mask 
#define CBR_SYNC_SHFT  29      // Sync Shift 

#define CBR_QTSL0      0       // QTSL0 - 1 element
#define CBR_QTSL1      1       // QTSL1 - 4 elements
#define CBR_QTSL2      2       // QTSL2 - 8 elements
#define CBR_QTSL3      3       // QTSL3 - 16 elements
#define CBR_QTSL_MSK   3       // QTSL Mask
#define CBR_QTSL_SHFT  30      // QTSL Shift

/* CBR Parameter Table constants */
#define CBR_PP0        0       // PP0 - Active Parameter 0
#define CBR_PP1        1       // PP0 - Active Parameter 1
#define CBR_PP1_MSK    1       // PP Mask 
#define CBR_PP1_SHFT   31      // PP Shift 

// CBR parameter field lengths and offsets
#define CBR_DST_OFF    0       // CBR dst offset
#define CBR_DST_LEN    4       // CBR dst length
#define CBR_FD_OFF     4       // CBR fd offset
#define CBR_FD_LEN     4       // CBR fd length
#define CBR_CNT_OFF    8       // CBR cnt offset
#define CBR_CNT_LEN    4       // CBR cnt length
#define CBR_DST_IDX0_OFF 12    // CBR dst_idx0 offset
#define CBR_DST_IDX0_LEN 2     // CBR dst_idx0 length
#define CBR_DST_IDX1_OFF 16    // CBR dst_idx1 offset
#define CBR_DST_IDX1_LEN 2     // CBR dst_idx1 length
#define CBR_CNT_REF_OFF 20     // CBR cnt_ref offset
#define CBR_CNT_REF_LEN 4      // CBR cnt_ref length
#define CBR_DST_RLD0_OFF 24    // CBR dst_rld0 offset
#define CBR_DST_RLD0_LEN 4     // CBR dst_rld0 length
#define CBR_DST_RLD1_OFF 28    // CBR dst_rld1 offset
#define CBR_DST_RLD1_LEN 4     // CBR dst_rld1 length
#define CBR_DT0_OFF    32      // CBR dt0 offset
#define CBR_DT0_LEN    4       // CBR dt0 length
#define CBR_DT1_OFF    36      // CBR dt1 offset
#define CBR_DT1_LEN    4       // CBR dt1 length

#endif // CBR_
