/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef CBW_
#define CBW_

/* CBW Event Entry constants */
#define CBW_ETYPE_MSK  0x1F    // ETYPE Mask
#define CBW_ETYPE_SHFT 0       // ETYPE Shift

#define CBW_PP_P1_MSK  0x7     // PP Part1 Mask
#define CBW_PP_P1_SHFT 5       // PP Part1 Shift 
#define CBW_PP_P2_MSK  1       // PP Part2 Mask
#define CBW_PP_P2_SHFT 21      // PP Part2 Shift 
#define CBW_PP_P3_MSK  1       // PP Part3 Mask
#define CBW_PP_P3_SHFT 28      // PP Part3 Shift 

#define CBW_PTE_MSK    0x7F    // PTE Mask
#define CBW_PTE_SHFT   8       // PTE Shift

#define CBW_EWM_DIS    0       // EVM Disable
#define CBW_EWM_ENA    1       // EVM Enable
#define CBW_EWM_MSK    1       // EVM Mask 
#define CBW_EWM_SHFT   16      // EVM Shift 

#define CBW_RLOAD_DIS  0       // RLOAD disabled 
#define CBW_RLOAD_ENA  1       // RLOAD enabled 
#define CBW_RLOAD_MSK  1       // RLOAD Mask 
#define CBW_RLOAD_SHFT 20      // RLOAD Shift 

#define CBW_TCINT_DIS  0       // TCINT disabled
#define CBW_TCINT_ENA  1       // TCINT enabled
#define CBW_TCINT_MSK  1       // TCINT Mask 
#define CBW_TCINT_SHFT 22      // TCINT Shift 

#define CBW_ATCINT_DIS 0       // ATCINT disabled
#define CBW_ATCINT_ENA 1       // ATCINT enabled
#define CBW_ATCINT_MSK 1       // ATCINT Mask 
#define CBW_ATCINT_SHFT 23     // ATCINT Shift 

#define CBW_TCC_MSK    0x0F    // TCC Mask
#define CBW_TCC_SHFT   24      // TCC Shift

#define CBW_SYNC_DIS   0       // Sync disabled
#define CBW_SYNC_ENA   1       // Sync enabled
#define CBW_SYNC_MSK   1       // Sync Mask 
#define CBW_SYNC_SHFT  29      // Sync Shift 

#define CBW_QTSL0      0       // QTSL0 - 1 element
#define CBW_QTSL1      1       // QTSL1 - 4 elements
#define CBW_QTSL2      2       // QTSL2 - 8 elements
#define CBW_QTSL3      3       // QTSL3 - 16 elements
#define CBW_QTSL_MSK   3       // QTSL Mask
#define CBW_QTSL_SHFT  30      // QTSL Shift

/* CBW Parameter Table constants */
#define CBW_PP0        0       // PP0 - Active Parameter 0
#define CBW_PP1        1       // PP0 - Active Parameter 1
#define CBW_PP1_MSK    1       // PP Mask 
#define CBW_PP1_SHFT   31      // PP Shift 

// CBW parameter field lengths and offsets
#define CBW_SRC_OFF    0       // CBW src offset
#define CBW_SRC_LEN    4       // CBW src length
#define CBW_FD_OFF     4       // CBW fd offset
#define CBW_FD_LEN     4       // CBW fd length
#define CBW_CNT_OFF    8       // CBW cnt offset
#define CBW_CNT_LEN    4       // CBW cnt length
#define CBW_SRC_IDX0_OFF 12    // CBW src_idx0 offset
#define CBW_SRC_IDX0_LEN 2     // CBW src_idx0 length
#define CBW_SRC_IDX1_OFF 16    // CBW src_idx1 offset
#define CBW_SRC_IDX1_LEN 2     // CBW src_idx1 length
#define CBW_CNT_REF_OFF 20     // CBW cnt_ref offset
#define CBW_CNT_REF_LEN 4      // CBW cnt_ref length
#define CBW_SRC_RLD0_OFF 24    // CBW src_rld0 offset
#define CBW_SRC_RLD0_LEN 4     // CBW src_rld0 length
#define CBW_SRC_RLD1_OFF 28    // CBW src_rld1 offset
#define CBW_SRC_RLD1_LEN 4     // CBW src_rld1 length
#define CBW_DT0_OFF    32      // CBW dt0 offset
#define CBW_DT0_LEN    4       // CBW dt0 length
#define CBW_DT1_OFF    36      // CBW dt1 offset
#define CBW_DT1_LEN    4       // CBW dt1 length

#endif // CBW_
