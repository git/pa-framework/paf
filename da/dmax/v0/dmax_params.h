
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX parameter tables
//
//
//

#ifndef dMAX_PARAMS_H_ 
#define dMAX_PARAMS_H_ 

/* Parameter strcuture definitions */
#include "cbr.h"
#include "cbw.h"
#include "dra.h"
#include "int.h"
#include "i2crx.h"
#include "i2ctx.h"
#include "kpt.h"
//#include "mcasp.h"
#include "mem1d.h"
#include "mem1dg.h"
#include "mem1dn.h"
#include "mem3d.h"
#include "spimst.h"
#include "spi_i2c.h"

/* Parameter Types */
#define PT0              0       // Parameter Type 0
#define PT1              1       // Parameter Type 1
#define PT2              2       // Parameter Type 2
#define PT_MEM3D         3       // Parameter Type MEM3D 
#define PT_CBW           4       // Parameter Type CBW 
#define PT_CBR           5       // Parameter Type CBR 
#define PT6              6       // Parameter Type 6
#define PT_INT           7       // Parameter Type INT 
#define PT_DRA           8       // Parameter Type DRA 
#define PT_KPT           9       // Parameter Type KPT 
#define PT10             10      // Parameter Type 10
#define PT11             11      // Parameter Type 11
#define PT12             12      // Parameter Type 12
#define PT13             13      // Parameter Type 13
#define PT14             14      // Parameter Type 14
#define PT15             15      // Parameter Type 15
#define PT_McASP0        16      // Parameter Type McASP0 
#define PT_McASP1        17      // Parameter Type McASP1 
#define PT_McASP2        18      // Parameter Type McASP2 
#define PT_McASP3        19      // Parameter Type McASP3 
#define PT_McASP4        20      // Parameter Type McASP4 
#define PT_McASP5        21      // Parameter Type McASP5 
#define PT_McASP6        22      // Parameter Type McASP6
#define PT_SPIMST        23      // Parameter Type SPIMST 
#define PT_SPI_I2C       24      // Parameter Type SPI_I2C 
#define PT_MEM1D         25      // Parameter Type MEM1D 
#define PT_MEM1DN        26      // Parameter Type MEM1DN 
#define PT_MEM1DG        27      // Parameter Type MEM1DG
#define PT28             28      // Parameter Type 28
#define PT_I2CRX         29      // Parameter Type I2CRX 
#define PT_I2CTX         30      // Parameter Type I2CTX 
#define PT31             31      // Parameter Type 31

#endif // dMAX_PARAMS_H_

