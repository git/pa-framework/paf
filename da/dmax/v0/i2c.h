
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// I2C constants and definitions
//
//
//

#ifndef I2C_
#define I2C_

/* I2c Device Identifiers */
#define I2C_DEV0          0
#define I2C_DEV1          1

/* I2C Base address definitions */
#define I2C0_BASE         0x49000000
#define I2C1_BASE         0x4a000000

/* I2C register definitions */
#define I2C_ICOAR         0x00
#define I2C_ICIMR         0x01
#define I2C_ICSTR         0x02
#define I2C_ICCLKL        0x03
#define I2C_ICCLKH        0x04
#define I2C_ICCNT         0x05
#define I2C_ICDRR         0x06
#define I2C_ICSAR         0x07
#define I2C_ICDXR         0x08
#define I2C_ICMDR         0x09
#define I2C_ICIVR         0x0A
#define I2C_ICEMDR        0x0B
#define I2C_ICPSC         0x0C
#define I2C_ICPID1        0x0D
#define I2C_ICPID2        0x0E
#define I2C_ICPFUNC       0x12
#define I2C_ICPDIR        0x13
#define I2C_ICPDIN        0x14
#define I2C_ICPDOUT       0x15
#define I2C_ICPDSET       0x16
#define I2C_ICPDCLR       0x17 

#endif /* I2C_ */
