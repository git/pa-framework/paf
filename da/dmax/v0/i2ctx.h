
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX I2CTX Service routine data structures 
//
//
//

#ifndef I2CTX_H_
#define I2CTX_H_

#include <i2ctx.inc>
#include <i2c.h>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* I2CTX macros */
#define I2CTX_MKEE(PTB, PTYPE)                   \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PTYPE))

/* I2CTX parameter format */
typedef struct i2ctx {
    Uint8  ctrl;
    Uint8  txpend;
    Uint16 res;
    Uint32 dxr;
    Uint32 buf;
    Uint16 len;
    Uint8  tcint;
    Uint8  tcc;
    Uint16 paycnt;
    Uint8  paynum;
    Uint8  paybig;
    Uint16 rptr;
    Uint16 fulllen;
    Uint16 wptr;
    Uint16 avalen;
    Uint16 newlen;
    Uint8  newnum;
    Uint8  res1;
    Uint32 emptyreg;
    Uint32 emptyval;
} i2ctx;

/* I2CTX API declarations */
extern Uint32 dMAX_i2ctxOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Uint32 xpp,Uint32 tcc,Uint32 tcint,Fxn fxn);
extern void dMAX_i2ctxClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                            Uint32 xpp,Uint32 tcc,Uint32 tcint,Uint32 pt);
extern void dMAX_i2ctxCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                            Uint32 ee);
extern void dMAX_i2ctxCfgPT(dMAX_Handle handle,Uint32 pt,i2ctx *pi2ctx);
extern void dMAX_i2ctxStart(dMAX_Handle handle,Uint32 evt);
extern Uint32 dMAX_i2ctxWrite(dMAX_Handle handle,Uint32 pt,Uint32 buf,
                              Uint32 len,Uint32 abuf,Uint32 alen,Uint32 tcc);
#endif // I2CTX_H_

