
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX Kill pending transfer Service Routine data structures
//
//
//

#ifndef KPT_H_ 
#define KPT_H_ 

#include <kpt.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* KPT macros */
#define KPT_MKCTRL(TCINT)                        \
    ((TCINT) << KPT_TCINT_SHFT)

#define KPT_MKEE(PTB, PT)                        \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PT))

/* KPT parameter format */
typedef struct kpt {
    Uint8  ctrl;
    Uint8  pend;
    Uint8  tcint;
    Uint8  tcc;
} kpt;

/* KPT API declarations */
extern Uint32 dMAX_kptOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 tcc,Int32 tcint,Fxn fxn);
extern void dMAX_kptClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 tcc,Int32 tcint,Uint32 pt);
extern void dMAX_kptCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern void dMAX_kptCfgPT(dMAX_Handle handle,Uint32 pt,kpt *pkpt);
extern void dMAX_kptKill(dMAX_Handle handle,Uint32 evt);
extern void dMAX_kptWait(dMAX_Handle handle,Uint32 tcc);
extern void dMAX_kptNoWait(dMAX_Handle handle,Uint32 tcc);
extern Uint32 dMAX_kptBusy(dMAX_Handle handle,Uint32 tcc);
#endif // KPT_H_

