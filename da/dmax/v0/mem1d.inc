/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
//

#ifndef MEM1D_
#define MEM1D_

// The MEM1D service routine facilitates 1D:1D trasnfer. The
// following Parameter Table is used to specify a 1D:1D mem1d.
//
// 31        24        16        8       2         1          0  
//  __________________________________________________________
// | PEND_PRI| TCC     | TCINT   | RES   | LINKFLG | TCINTFLG |
// |_________|_________|_________|_______|_________|__________|
// |                    SRC                                   |
// |__________________________________________________________|
// |                    DST                                   |
// |__________________________________________________________|
// | LINK    | BURSTLEN|           CNT                        |
// |_________|_________|______________________________________| 
//
#define MEM1D_TCINT_DIS  0       // TCINT Disable 
#define MEM1D_TCINT_ENA  1       // TCINT Enable
#define MEM1D_TCINT_MSK  1       // TCINT Mask 
#define MEM1D_TCINT_SHFT 0       // TCINT Shift 

#define MEM1D_LINK_DIS   0       // LINK Disable
#define MEM1D_LINK_ENA   1       // LINK Enable
#define MEM1D_LINK_MSK   1       // LINK Mask 
#define MEM1D_LINK_SHFT  1       // LINK Shift 

// MEM1D parameter field lengths and offsets
#define MEM1D_CTRL_OFF   0       // MEM1D ctrl offset
#define MEM1D_CTRL_LEN   1       // MEM1D ctrl length       
#define MEM1D_TCINT_OFF  1       // MEM1D tcint offset
#define MEM1D_TCINT_LEN  1       // MEM1D tcint length       
#define MEM1D_TCC_OFF    2       // MEM1D tcc offset
#define MEM1D_TCC_LEN    1       // MEM1D tcc length       
#define MEM1D_PEND_OFF   3       // MEM1D pend offset
#define MEM1D_PEND_LEN   1       // MEM1D pend length       
#define MEM1D_SRC_OFF    4       // MEM1D src offset
#define MEM1D_SRC_LEN    4       // MEM1D src length       
#define MEM1D_DST_OFF    8       // MEM1D dst offset
#define MEM1D_DST_LEN    4       // MEM1D dst length       
#define MEM1D_CNT_OFF    12      // MEM1D cnt offset
#define MEM1D_CNT_LEN    2       // MEM1D cnt length       
#define MEM1D_BLEN_OFF   14      // MEM1D blen offset
#define MEM1D_BLEN_LEN   1       // MEM1D blen length       
#define MEM1D_LINK_OFF   15      // MEM1D link offset
#define MEM1D_LINK_LEN   1       // MEM1D link length       

#endif // MEM1D_
