/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
//

#ifndef MEM1DN_
#define MEM1DN_

// The MEM1DN service routine facilitates 1D:1D trasnfer with N bursts 
// and check for new events. Parameter Table:
//
// 31        24        16        8        4     2         1          0  
//  __________________________________________________________________
// | PEND_PRI| TCC     | TCINT   | BURSTS | RES | LINKFLG | TCINTFLG |
// |_________|_________|_________|________|_____|_________|__________|
// |                    SRC                                          |
// |_________________________________________________________________|
// |                    DST                                          |
// |_________________________________________________________________|
// | LINK    | BURSTLEN|           CNT                               |
// |_________|_________|_____________________________________________| 
//
#define MEM1DN_TCINT_DIS 0       // TCINT Disable 
#define MEM1DN_TCINT_ENA 1       // TCINT Enable
#define MEM1DN_TCINT_MSK 1       // TCINT Mask 
#define MEM1DN_TCINT_SHFT 0      // TCINT Shift 

#define MEM1DN_LINK_DIS  0       // LINK Disable
#define MEM1DN_LINK_ENA  1       // LINK Enable
#define MEM1DN_LINK_MSK  1       // LINK Mask 
#define MEM1DN_LINK_SHFT 1       // LINK Shift 

#define MEM1DN_NB_MSK    0x0F    // NB Mask
#define MEM1DN_NB_SHFT   4       // NB Shift

// MEM1DN parameter field lengths and offsets
#define MEM1DN_CTRL_OFF  0       // MEM1DN ctrl offset
#define MEM1DN_CTRL_LEN  1       // MEM1DN ctrl length       
#define MEM1DN_TCINT_OFF 1       // MEM1DN tcint offset
#define MEM1DN_TCINT_LEN 1       // MEM1DN tcint length       
#define MEM1DN_TCC_OFF   2       // MEM1DN tcc offset
#define MEM1DN_TCC_LEN   1       // MEM1DN tcc length       
#define MEM1DN_PEND_OFF  3       // MEM1DN pend offset
#define MEM1DN_PEND_LEN  1       // MEM1DN pend length       
#define MEM1DN_SRC_OFF   4       // MEM1DN src offset
#define MEM1DN_SRC_LEN   4       // MEM1DN src length       
#define MEM1DN_DST_OFF   8       // MEM1DN dst offset
#define MEM1DN_DST_LEN   4       // MEM1DN dst length       
#define MEM1DN_CNT_OFF   12      // MEM1DN cnt offset
#define MEM1DN_CNT_LEN   2       // MEM1DN cnt length       
#define MEM1DN_BLEN_OFF  14      // MEM1DN blen offset
#define MEM1DN_BLEN_LEN  1       // MEM1DN blen length       
#define MEM1DN_LINK_OFF  15      // MEM1DN link offset
#define MEM1DN_LINK_LEN  1       // MEM1DN link length       

#endif // MEM1DN_
