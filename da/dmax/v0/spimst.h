
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX SPI Master Service routines data structures 
//
//
//

#ifndef SPIMST_H_
#define SPIMST_H_

#include <spimst.inc>
#include <spi.h>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* SPIMST macros */
#define SPIMST_MKEE(PTB, PTYPE)                  \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PTYPE))

#define SPIMST_MKCTRL(SRDY, RFIL, XFIL, LINKFLG, TCINTFLG)   \
    ((((SRDY)&SPIMST_SRDY_MSK)<<SPIMST_SRDY_SHFT) |    \
     (((RFIL)&SPIMST_RFIL_MSK)<<SPIMST_RFIL_SHFT) |    \
     (((XFIL)&SPIMST_XFIL_MSK)<<SPIMST_XFIL_SHFT) |    \
     (((LINKFLG)&SPIMST_LINK_MSK)<<SPIMST_LINK_SHFT) | \
     (((TCINTFLG)&SPIMST_TCINT_MSK)<<SPIMST_TCINT_SHFT))

/* SPIMST parameter format */
typedef struct spimst {
    Uint8 ctrl;
    Uint8 wlen;
    Uint8 tcc;
    Uint8 tcint;
    Uint32 drr;
    Uint32 dxr;
    Uint32 rbuf;
    Uint32 xbuf;
    Uint16 cnt;
    Uint8 pend;
    Uint8 res;
    Uint16 xpat;
    Uint16 rpat;
    Uint32 link;
} spimst;

/* SPIMST API declarations */
extern Uint32 dMAX_spimstOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                              Int32 pp,Uint32 tcc,Int32 tcint,Fxn fxn);
extern void dMAX_spimstClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Int32 pp,Uint32 tcc,Int32 tcint,Uint32 pt);
extern void dMAX_spimstCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Uint32 ee);
extern void dMAX_spimstCfgPT(dMAX_Handle handle,Uint32 pt,spimst *pspimst);
extern void dMAX_spimstStart(dMAX_Handle handle,Uint32 evt);
extern void dMAX_spimstWait(dMAX_Handle handle,Uint32 tcc);
extern void dMAX_spimstNoWait(dMAX_Handle handle,Uint32 tcc);
extern Uint32 dMAX_spimstBusy(dMAX_Handle handle,Uint32 tcc);
extern Uint32 dMAX_spimstLnkOpen(dMAX_Handle handle,Uint32 maxid,Int32 tcc,
                                 Int32 tcint,Int32 pt,Fxn fxn);
extern void dMAX_spimstLnkClose(dMAX_Handle handle,Uint32 maxid,Int32 tcc,
                                Int32 tcint,Int32 pt);
extern Uint32 dMAX_spimstLnkAdd(dMAX_Handle handle,Uint32 maxid,Uint32 cpt);
#endif // SPIMST_H_

