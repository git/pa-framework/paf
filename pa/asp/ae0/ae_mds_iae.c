
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module implementation - MDS implementation of an
 *  ASP Example Demonstration algorithm.
 */

#include <std.h>

#include <iae.h>
#include <ae_mds.h>
#include <ae_mds_priv.h>
#include <aeerr.h>

#include  <std.h>
#include <xdas.h>

#include "paftyp.h"

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== AE_MDS_apply ========
 *  MDS's implementation of the apply operation.
 *
 *  This function is called after the AE_MDS_reset function, below, and
 *  deals with control data AND sample data.
 *
 */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

Int 
AE_MDS_apply(IAE_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    AE_MDS_Obj * restrict ae = (Void *)handle;

    Int i;

    Int sampleCount = pAudioFrame->sampleCount; // Number of samples in the audio frame
                                                // (in each channel).

    // Get pointers to left, right, and center channel audio sample buffers, from the 
    // Audio Frame Structure:
    
    PAF_AudioData * restrict left = pAudioFrame->data.sample[PAF_LEFT];
    PAF_AudioData * restrict rght = pAudioFrame->data.sample[PAF_RGHT];
    PAF_AudioData * restrict cntr = pAudioFrame->data.sample[PAF_CNTR];

    //PAF_AudioData * restrict lhsi = pAudioFrame->data.sample[PAF_LHSI];
    //PAF_AudioData * restrict rhsi = pAudioFrame->data.sample[PAF_RHSI];

    PAF_AudioData ccScale = ae->pStatus->scaleQ15 / 32768.; // Cntr scale value.
    PAF_AudioData lrScale = 1. - ccScale; // Left/Rght scale value.

    PAF_AudioSize * restrict samsiz = pAudioFrame->data.samsiz; // Audio size.

    // Check mode, scale, and the channel configuration of the input:
    
    // The mode value has a default value of one, which we set in iae.c, so this 
    // processing is enabled by default.  To disable it, you can send the 
    // 'writeAEModeDisable' Alpha Code  (as defined in the file ./alpha/ae_a.h).  
    // You can enable it again by sending the 'writeAEModeEnable' Alpha Code.
    // To write and read the scaleQ15 value (which is zero by default), use the 
    // writeAEScaleQ15(N) and readAEScaleQ15 Alpha Codes.

    if (ae->pStatus->mode == 0)
        return 0;   // If mode is zero, ASP is Disabled, so exit.
    if (ae->pStatus->scaleQ15 == 0)
        return 0;   // If scale value is zero, we don't want scaling, so exit.
    if (pAudioFrame->channelConfigurationStream.part.sat != PAF_CC_SAT_STEREO)
        return 0;   // The Input is not stereo, so exit.
            
    // THIS IS WHERE THE AUDIO SAMPLE PROCESSING FOR THIS EXAMPLE IS DONE:
    
    // Perform mixing of Left and Right channels to Center channel for all samples in 
    // the frame:
    
    for (i=0; i < sampleCount; i++) {
        cntr[i] = ccScale * (left[i] + rght[i]);
        left[i] *= lrScale;
        rght[i] *= lrScale;
        //lhsi[i] = left[i];// -1.0f*left[i];
        //rhsi[i] =  rght[i];//-1.0f*rght[i];
    }

    // Set audio size for Center channel:
    //  Samsiz is in units of 0.5 dB. So 2*6 is 6dB (a factor of two). This
    //  calculation is an approximation.  When samsiz[PAF_LEFT] equals
    //  samsiz[PAF_RGHT] then it is exact, the center is twice the size
    //  of left or right.  A better approximation would be
    //  samsiz[PAF_CNTR] = 2*6 + max(samsiz[PAF_LEFT],samsiz[PAF_RGHT])
    samsiz[PAF_CNTR] = 2*6 + samsiz[PAF_LEFT];

    // Set new channel configuration:
    pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_3STEREO;
   
    //    
    //Extended channel mask update in ASP:
    pAudioFrame->channelConfigurationStream.part.extMask |= (PAF_CC_EXTMASK_LwRw);    
    //pAudioFrame->channelConfigurationStream.part.extMask |= (PAF_CC_EXTMASK_LhsRhs);
    


    return 0;
}

/*
 *  ======== AE_MDS_reset ========
 *  MDS's implementation of the reset function, which is like an "information" 
 *  operation.  This is always called by the framework before calling the AE_MDS_apply
 *  function, and only deals with control data, not sample data.
 */

Int 
AE_MDS_reset(IAE_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    // AE_MDS_Obj * restrict ae = (Void *)handle;

    // To do...  put your specific initialization code here.
    
    /* It is important to reset any "state" buffers here.  This example does not have
       any states that need to be maintained, so there is no code here to do that.

       This example does not affect the audio frame parameters, so it does not do
       anything with them.  This example only changes the audio data.  If you are
       writing an ASP that needs to change the audio frame parameters, you will have to
       use the pAudioFrame pointer to manipulate things like the
       channelConfigurationRequest structure (as is done by the BM0 ASP demo).  
    */
    
    return 0;
}
