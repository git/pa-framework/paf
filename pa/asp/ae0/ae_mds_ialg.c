
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module IALG implementation - MDS's implementation of the
 *  IALG interface for the ASP Example Demonstration algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include <iae.h>
#include <ae_mds.h>
#include <ae_mds_priv.h>

/*
 *  ======== AE_MDS_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== AE_MDS_alloc ========
 */
Int AE_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IAE_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IAE_PARAMS;  /* set default parameters */
    }

    /* Request memory for AE object */
    memTab[0].size = (sizeof(AE_MDS_Obj)+3)/4*4 + (sizeof(AE_MDS_Status)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_EXTERNAL;
    memTab[0].attrs = IALG_PERSIST;

    return (1);
}

/*
 *  ======== AE_MDS_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== AE_MDS_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== AE_MDS_initObj ========
 */
Int AE_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    AE_MDS_Obj *ae = (Void *)handle;
    const IAE_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IAE_PARAMS;  /* set default parameters */
    }

    ae->pStatus = (AE_MDS_Status *)((char *)ae + (sizeof(AE_MDS_Obj)+3)/4*4);

    *ae->pStatus = *params->pStatus;
    ae->config = params->config;

    return (IALG_EOK);
}


/*
 *  ======== AE_MDS_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== AE_MDS_moved ========
 */
  /* COM_TIH_moved */
