
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <std.h>

//How to create basic PAF ASP Structure?

//We can use ASPCodeGenerator.exe (avialable at pa/bin) from pa-pa17_da7xx_XXXXXX (Firmware)
//deliverable of PA17 SDK to create a basic PAF ASP Structure.

//ASPCodegenerator requires 2 inputs for creating basic ASP Structure
//1. ASP Name for this Example it is "ae" (ASP Example)
//2. VEN Name = MDS

//Steps to create Basic ASP Structure.
//1. Run ASPCodeGenerator.exe
//2. Ignore Step1
//3. For Step2 select the folder where new ASP should be created(Typically the folder 
//       (will be pa/asp/xxx, folder name is same as ASP Name.
//4. Enter ASP Name and VEN Name, and Ignore the ASP Location
//5. Click on Generate button in step4
//6. Click ok on the warning reported 
//7. The ASP structure will be created in specified Location.

//For FILE I/O demonstration Project for Custom ASP
//1. Create the demo project and include this file along with the linker cmd file in this pjt
//2. Include all the source files of newly created ASP except ae.c and ae_mds_ext.c
//3. In this File Change the ASP/VEN Name as required.
//4. Include the ASP folder and "T:\pa\i\d710e001\pa\f\std\std_incl.opt" for all PAF related header files


//This is demo wrapper code for demonstrating PAF ASP usage with File IO

#include "ae.h"
#include "iae.h"		//These are ASP Specific change according to ASP/Vendor name
#include "ae_mds.h"


#define MAX_NUM_SAMPLES  256
PAF_AudioFrame frm;
static PAF_AudioData data[PAF_MAXNUMCHAN][MAX_NUM_SAMPLES];
static PAF_AudioData *data_ptrs[PAF_MAXNUMCHAN];
static PAF_AudioSize samsiz[PAF_MAXNUMCHAN];
static short buf[MAX_NUM_SAMPLES][2];
static FILE *fpIn, *fpOut[4];


#define CONVERSION_CONST 32768.0f	//Maximum 16bit signed int Value
#define ROUND(acc) ((acc > 1.0)? 1.0 :((acc < -1.0)? -1.0 : acc))


//Function to read data from file to PAF
//The below function currently reads 2Chan 16 bit PCM data.
int
read_data (PAF_AudioFrame * pAudioFrame, int n)
{
  int i;

  if ((n = fread (buf, 4, n, fpIn)) <= 0)
    return (0);

  //PAF requires normalized Floating Point Input input should be between (-1.0 to +1.0)
  //So Normalize the Input read from file
  for (i = 0; i < n; i++)
    {
      pAudioFrame->data.sample[PAF_LEFT][i] = buf[i][0] / CONVERSION_CONST;
      pAudioFrame->data.sample[PAF_RGHT][i] = buf[i][1] / CONVERSION_CONST;
    }

  pAudioFrame->sampleCount = n;
  pAudioFrame->sampleRate = PAF_SAMPLERATE_48000HZ;
  pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_STEREO;
  pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;
  pAudioFrame->data.nSamples = MAX_NUM_SAMPLES;
  return (1);
}

//Function to write data from PAF to File
//The below function currently all 8Chan 16 bit PCM data.
//The Actual valid channel's are indicated via pAudioFrame->channelConfigurationStream

int
write_data (PAF_AudioFrame * pAudioFrame)
{
  int i, n = pAudioFrame->sampleCount;

  //dump LEFT and RGHT Channels
  memset (buf, 0, sizeof (buf));
  for (i = 0; i < n; i++)
    {
      buf[i][0] =
	ROUND (pAudioFrame->data.sample[PAF_LEFT][i]) * CONVERSION_CONST;
      buf[i][1] =
	ROUND (pAudioFrame->data.sample[PAF_RGHT][i]) * CONVERSION_CONST;
    }
  if ((n = fwrite (buf, 4, n, fpOut[0])) <= 0)
    return 0;

  //dump LSUR and RSUR Channels
  memset (buf, 0, sizeof (buf));
  for (i = 0; i < n; i++)
    {
      buf[i][0] =
	ROUND (pAudioFrame->data.sample[PAF_LSUR][i]) * CONVERSION_CONST;
      buf[i][1] =
	ROUND (pAudioFrame->data.sample[PAF_RSUR][i]) * CONVERSION_CONST;
    }
  if ((n = fwrite (buf, 4, n, fpOut[1])) <= 0)
    return 0;

  //dump CNTR and SUBW Channels
  memset (buf, 0, sizeof (buf));
  for (i = 0; i < n; i++)
    {
      buf[i][0] =
	ROUND (pAudioFrame->data.sample[PAF_CNTR][i]) * CONVERSION_CONST;
      buf[i][1] =
	ROUND (pAudioFrame->data.sample[PAF_SUBW][i]) * CONVERSION_CONST;
    }
  if ((n = fwrite (buf, 4, n, fpOut[2])) <= 0)
    return 0;

  //dump LBAK and RBAK Channels
  memset (buf, 0, sizeof (buf));
  for (i = 0; i < n; i++)
    {
      buf[i][0] =
	ROUND (pAudioFrame->data.sample[PAF_LBAK][i]) * CONVERSION_CONST;
      buf[i][1] =
	ROUND (pAudioFrame->data.sample[PAF_RBAK][i]) * CONVERSION_CONST;
    }
  if ((n = fwrite (buf, 4, n, fpOut[3])) <= 0)
    return 0;

  return (n);
}

//This Function can be used to Modify Status structure values.
//Typically all run time controls of ASP are controlled with this structure in PAF.
void
tweakStatus (void *pStatus)
{
  IAE_Status *pS = pStatus;	//This is ASP Specific change according to ASP/Vendor name

  pS->scaleQ15 = 16384;		//Scale factor of 0.5

}

int
main ()
{
  PAF_AudioFrame *pAudioFrame = &frm;
  AE_Handle pAsp;		//This is ASP Specific change according to ASP/Vendor name    
  IAE_Status *pStatus;		//This is ASP Specific change according to ASP/Vendor name
  unsigned int clk1, clk2, clk_max = 0;
  int i, fno = 0;

  // Open Source Audio Data  read from input.raw(default file)
  if ((fpIn = fopen ("..\\input.raw", "rb")) == 0)
    {
      printf ("%s not availble\n", "input.raw");
      exit (0);
    }

  //Open OutFiles for ASP, by default creates
  //4 OutputFiles for  Fronts, Srrd, CntLfe and Rear's
  if ((fpOut[0] = fopen ("aedemoFront.pcm", "wb")) == 0)
    {
      printf ("%s not availble\n", "aedemoFront.pcm");
      exit (0);
    }

  if ((fpOut[1] = fopen ("aedemoSrrd.pcm", "wb")) == 0)
    {
      printf ("%s not availble\n", "aedemoSrrd.pcm");
      exit (0);
    }

  if ((fpOut[2] = fopen ("aedemoCntLfe.pcm", "wb")) == 0)
    {
      printf ("%s not availble\n", "aedemoCntLfe.pcm");
      exit (0);
    }

  if ((fpOut[3] = fopen ("aedemoRear.pcm", "wb")) == 0)
    {
      printf ("%s not availble", "aedemoRear.pcm");
      exit (0);
    }


  // Initialize pAudioFrame 
  pAudioFrame->mode = 1;
  pAudioFrame->sampleDecode = 0;
  for (i = 0; i < PAF_MAXNUMCHAN; ++i)
    {
      data_ptrs[i] = data[i];
      samsiz[i] = 0;
    }
  pAudioFrame->data.sample = data_ptrs;
  pAudioFrame->data.samsiz = samsiz;
  pAudioFrame->channelConfigurationStream.legacy = 0;
  //ChannelConfiguration Stream indicates the Input channel configuration to ASP
  //ChannelConfiguration Stream can be accesses separately for satellite and subwoofer channels
  //PAF Supported satellite channel configurations
  //pAudioFrame->channelConfigurationStream.part.sat can be
  //PAF_CC_SAT_MONO   indicates CNTR(C) Channel is present
  //PAF_CC_SAT_STEREO Or PAF_CC_SAT_PHANTOM0   indicates LEFT(L), RGHT(R), Channels are present
  //PAF_CC_SAT_PHANTOM1   indicates LEFT(L), RGHT(R), LSUR(Ls) Channels are present
  //PAF_CC_SAT_PHANTOM2   indicates LEFT(L), RGHT(R), LSUR(Ls), RSUR(Rs) Channels are present
  //PAF_CC_SAT_PHANTOM3   indicates LEFT(L), RGHT(R), LSUR(Ls), RSUR(Rs), LBAK(Lb) Channels are present
  //PAF_CC_SAT_PHANTOM4   indicates LEFT(L), RGHT(R), LSUR(Ls), RSUR(Rs), LBAK(Lb), RBAK(Rb) Channels are present
  //PAF_CC_SAT_3STEREO Or PAF_CC_SAT_SURROUND0   indicates LEFT(L), RGHT(R), CNTR(C)  Channels are present
  //PAF_CC_SAT_SURROUND1   indicates LEFT(L), RGHT(R), CNTR(C), LSUR(Ls) Channels are present
  //PAF_CC_SAT_SURROUND2   indicates LEFT(L), RGHT(R), CNTR(C), LSUR(Ls), RSUR(Rs) Channels are present
  //PAF_CC_SAT_SURROUND3   indicates LEFT(L), RGHT(R), CNTR(C), LSUR(Ls), RSUR(Rs), LBAK(Lb) Channels are present
  //PAF_CC_SAT_SURROUND4   indicates LEFT(L), RGHT(R), CNTR(C), LSUR(Ls), RSUR(Rs), LBAK(Lb), RBAK(Rb) Channels are present

  //PAF Supported subwoofer channel configurations 
  //pAudioFrame->channelConfigurationStream.part.sub can be
  //PAF_CC_SUB_ZERO  indicates no subwoofer channel is present
  //PAF_CC_SUB_ONE  indicates one subwoofer channel is present

  pAudioFrame->channelConfigurationRequest.legacy = 0;
  //ChannelConfiguration  Request indicates the requested channel configuration from ASP
  //ChannelConfiguration  Request will also takes one of the above channel configurations

  //Consider a case where ASP generates upto 7.1 channel configurations from 2.0 input 
  //For a particular case of generating 5.1 output from 2.0 input the sequence will be
  //pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_STEREO;
  //pAudioFrame->channelConfigurationStream.part.sub = PAF_CC_SUB_ZERO;
  //pAudioFrame->channelConfigurationRequest.part.sat = PAF_CC_SAT_SURROUND2;
  //pAudioFrame->channelConfigurationRequest.part.sub = PAF_CC_SUB_ONE;
  //Call apply() function of ASP, if ASP is Enabled and active then after apply call
  //the channel configurations should be updated to
  //pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_SURROUND2;
  //pAudioFrame->channelConfigurationStream.part.sub = PAF_CC_SUB_ONE;
  //pAudioFrame->channelConfigurationRequest.part.sat = PAF_CC_SAT_SURROUND2;
  //pAudioFrame->channelConfigurationRequest.part.sub = PAF_CC_SUB_ONE;


  //Create ASP Handle this is ASP Spectific replace AE_MDS_IAE, IAE_PARAMS with required ASP/VEN Names
  if (!(pAsp = (AE_Handle) ALG_create ((IALG_Fxns *) & AE_MDS_IAE, 0,(IALG_Params *) & IAE_PARAMS)))
    {
      //ALG_create does the following series of steps
      //1. calls AE_MDS_numAlloc to get the number of memtabs requested by Algorithm, by default
      //this is defined as Null and allocates 4 memtabs, if your ASP needs more than 4 memtabs define this 
      //function      AE_MDS_numAlloc in ae_mds_ialgv.c instead of NULL, This function can be declared as following 
      //in  ae_mds_ialg.c if you need more than 4 mentabs.
      /*Int AE_MDS_numAlloc()
         {
         return(6);  //incases you need 6 memtabs
         } */

      //2. Allocates scratch memory for requested mentabs and calls AE_MDS_alloc() function to know
      //how much memory is requested by this ASP. So if ASP needs any heap memory     the request for
      //memory should be requested through memtabs in AE_MDS_alloc().

      //3.Allocates all the memory requested by AE_MDS_alloc()from heap and calls AE_MDS_initObj()
      //AE_MDS_initObj() does initialization of the memory allocated to ASP handle

      printf ("Unable to create an instance of ASP.  Increase heap size?\n");
    }

  //4. If you want to know how much memory ASP is requesting you can enable following code
#if 0
  {
    IALG_MemRec memTab[35];	//declare some default number of memtabs
    int scr = 0, pers = 0, n;

    //Get the Number of MemTabs used by Algorithm.
    n = AE_MDS_alloc ((IALG_Params *) & IAE_PARAMS, (IALG_Fxns **) & AE_MDS_IAE, memTab);

    //Get the Memory (RAM) Uasge by this Algorithm.
    for (i = 0; i < n; i++)
      {
		if (memTab[i].attrs == IALG_SCRATCH)
		  scr += memTab[i].size;	//Scratch memory can be reused across ALG's
		else
		  pers += memTab[i].size;	//Persist memory 
      }

    printf ("\nRAM: Persistant = %d Bytes (%.2f KB)\n", pers, pers / 1024.0);
    printf ("RAM: Scratch = %d Bytes(%.2f KB)\n", scr, scr / 1024.0);
  }
#endif


  //Get the access to Status Structure, this is ASP specific replace  IAE_GETSTATUSADDRESS1 with new ASP Name
  if (pAsp->fxns->ialg.algControl ((Void *) pAsp, IAE_GETSTATUSADDRESS1, (void *) &pStatus))
    printf ("error getting status struct\n");

  //Enable ASP Mode in case it is disabled.
  ((IAE_Status *) pStatus)->mode = 1;
  // ((IAE_Status *) pStatus)->use = 1;


  //All PAF Alpha codes go here in Status structure.
  //we can use the below function to change status values.
  tweakStatus ((void *) pStatus);


  //Do ASP reset before first Apply call
  pAsp->fxns->reset (pAsp, pAudioFrame);


  while (read_data (pAudioFrame, MAX_NUM_SAMPLES))
    {

      int ret;

      clk1 = clock ();
      if (ret = pAsp->fxns->apply (pAsp, pAudioFrame))
		printf ("\nASP_apply failed with %d", ret);
      clk2 = clock ();

      // printf ("Clocks:%d\n", clk2 - clk1);
      if (fno && !(fno % 25))
		printf ("Processed %5d Frames\n", fno);

      if (fno && (clk2 - clk1 > clk_max))	// Ignore first frame
		clk_max = clk2 - clk1;

      write_data (pAudioFrame);

      fno += 1;
    }

  printf ("\nClocks:%d  MCPS = %6.2f\n", clk_max,
	  clk_max * 48000.0f / (1e6 * MAX_NUM_SAMPLES));


  ALG_delete((ALG_Handle)pAsp);

  fclose (fpIn);
  fclose (fpOut[0]);
  fclose (fpOut[1]);
  fclose (fpOut[2]);
  fclose (fpOut[3]);

}



/*
 *  ======== COM_TII_free ========
 */
//#pragma CODE_SECTION(COM_TII_delete,".text:_COM_TIH_delete")
Int
COM_TII_free (IALG_Handle handle, IALG_MemRec memTab[])
{
  return (*handle->fxns->algAlloc) (NULL, NULL, memTab);
}

/*
 *  ======== COM_TII_control ========
 *
 *  This function provides common control operations for ASP Asporithms
 *  that do not utilize common memory.
 */
Int
COM_TII_control (IALG_Handle handle, IAE_Cmd cmd, IAE_Status * pStatus)
{
  COM_TII_Obj *obj = (Void *) handle;

  switch (cmd)
    {
    case ICOM_NULL:
      return ((Int) 0);
    case ICOM_GETSTATUSADDRESS1:
      *(Void **) pStatus = (Void *) obj->pStatus;
      return ((Int) 0);
    case ICOM_GETSTATUSADDRESS2:
      return ((Int) 1);
    case ICOM_GETSTATUS:
      memcpy ((Void *) pStatus, (Void *) obj->pStatus,
	      (Int) obj->pStatus->size);
      return ((Int) 0);
    case ICOM_SETSTATUS:
      memcpy ((Void *) obj->pStatus, (Void *) pStatus,
	      (Int) obj->pStatus->size);
      return ((Int) 0);
    default:
      return ((Int) - 1);
    }
}

Void
COM_TII_null ()
{
}

Void
COM_TII_deactivate ()
{
}

//#pragma CODE_SECTION(PAF_ASP_activate,".text:_PAF_ASP_activate")
Void
PAF_ASP_activate (ALG_Handle asp)
{
  if (asp->fxns->algActivate != NULL)
    {
      asp->fxns->algActivate (asp);
    }
}

//#pragma CODE_SECTION(PAF_ASP_deactivate,".text:_PAF_ASP_deactivate")
Void
PAF_ASP_deactivate (ALG_Handle asp)
{
  if (asp->fxns->algDeactivate != NULL)
    {
      asp->fxns->algDeactivate (asp);
    }
}

static char *gpDmaTransSrc = NULL;
static char *gpDmaTransDst = NULL;
static int gnDmaTransLength = 0;
static int gnDmaTransIndex = -1;

int
DAT_copy (void *pSrc, void *pDst, int length)
{
  gpDmaTransSrc = pSrc;
  gpDmaTransDst = pDst;
  gnDmaTransLength = length;
  gnDmaTransIndex = gnDmaTransIndex + 1;

  return gnDmaTransIndex;
}

int
DAT_wait (int id)
{
  if (gnDmaTransIndex == id)
    {
      if ((!gpDmaTransSrc) || (!gpDmaTransDst))
	printf ("Bug in DMA control\n");
      memcpy (gpDmaTransDst, gpDmaTransSrc, gnDmaTransLength);
      gpDmaTransSrc = NULL;
      gpDmaTransDst = NULL;
    }
  else
    {
      printf ("Bug in DMA control\n");
    }
  return 0;
}

void
TSK_enable (void)
{
}

void
TSK_disable (void)
{
}
