
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the AE
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef AE_
#define AE_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <iae.h>

#include "paftyp.h"

/*
 *  ======== AE_Handle ========
 *  ASP Example Demonstration algorithm instance handle
 */
typedef struct IAE_Obj *AE_Handle;

/*
 *  ======== AE_Params ========
 *  ASP Example Demonstration algorithm instance creation parameters
 */
typedef struct IAE_Params AE_Params;

/*
 *  ======== AE_PARAMS ========
 *  Default instance parameters
 */
#define AE_PARAMS IAE_PARAMS

/*
 *  ======== AE_PARAMS ========
 *  Different instance parameters
 */

extern const IAE_Params IAE_PARAMS_DIRECT;
extern const IAE_Params IAE_PARAMS_SIMPLE_DMA;
extern const IAE_Params IAE_PARAMS_CONCURRENT_DMA;

/*
 *  ======== AE_Status ========
 *  Status structure for getting AE instance attributes
 */
typedef volatile struct IAE_Status AE_Status;

/*
 *  ======== AE_Cmd ========
 *  This typedef defines the control commands AE objects
 */
typedef IAE_Cmd   AE_Cmd;

/*
 * ===== control method commands =====
 */
#define AE_NULL IAE_NULL
#define AE_GETSTATUSADDRESS1 IAE_GETSTATUSADDRESS1
#define AE_GETSTATUSADDRESS2 IAE_GETSTATUSADDRESS2
#define AE_GETSTATUS IAE_GETSTATUS
#define AE_SETSTATUS IAE_SETSTATUS

/*
 *  ======== AE_create ========
 *  Create an instance of a AE object.
 */
static inline AE_Handle AE_create(const IAE_Fxns *fxns, const AE_Params *prms)
{
    return ((AE_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== AE_delete ========
 *  Delete a AE instance object
 */
static inline Void AE_delete(AE_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== AE_apply ========
 */
extern Int AE_apply(AE_Handle, PAF_AudioFrame *);

/*
 *  ======== AE_reset ========
 */
extern Int AE_reset(AE_Handle, PAF_AudioFrame *);

/*
 *  ======== AE_exit ========
 *  Module finalization
 */
extern Void AE_exit(Void);

/*
 *  ======== AE_init ========
 *  Module initialization
 */
extern Void AE_init(Void);

#endif  /* AE_ */
