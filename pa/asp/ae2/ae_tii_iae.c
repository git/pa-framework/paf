
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module implementation - TII implementation of an
 *  ASP Example Demonstration algorithm.
 */

#include <std.h>

#include <iae.h>
#include <ae_tii.h>
#include <ae_tii_priv.h>
#include <aeerr.h>

#include  <std.h>
#include <xdas.h>

#include "paftyp.h"
#include "cpl.h"
#include <string.h>             // for memset, memcpy

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif                          /* PAF_AUDIODATATYPE_FIXED */

#ifndef _TMS320C6X
#define restrict
#endif                          /* _TMS320C6X */

/*
 *  ======== AE_TII_apply ========
 *  TII's implementation of the apply operation.
 *
 *  This function is called after the AE_TII_reset function, below, and
 *  deals with control data AND sample data.
 *
 *  This function contains code that detemines if we need to run our
 *  algorithm:
 *      If yes, we call setup() to set up the coefficients
 *      followed by process() to apply the filters etc.
 *      If no, we flag a discontinuity by calling reset and return.
 *
 */

Int AE_TII_apply(IAE_Handle handle, PAF_AudioFrame * pAudioFrame)
{
    AE_TII_Obj *restrict ae = (Void *) handle;

    if (ae->pStatus->mode == 0)
        goto no_process_return;     // If mode is zero, ASP is Disabled, so exit.
    if (pAudioFrame->channelConfigurationStream.part.sat != PAF_CC_SAT_STEREO)
        goto no_process_return;     // The Input is not stereo, so exit.
    if (pAudioFrame->sampleRate != PAF_SAMPLERATE_48000HZ &&
            pAudioFrame->sampleRate != PAF_SAMPLERATE_44100HZ)
        goto no_process_return;     // Unsupported sample rate

    // Update coefficients to reflect pStatus changes, if any
    handle->fxns->setup(handle, pAudioFrame);
    
    // Process the data
    handle->fxns->process(handle, pAudioFrame); 

    return 0;

no_process_return:
    // If we get here it means there is a discontinuity in processing.
    // We ensures that state memories are properly cleared for the next
    // call by calling reset.
    handle->fxns->reset(handle, pAudioFrame);
                      
    return 0;
}


/*
 *  ======== AE_TII_reset ========
 *  TII's implementation of the reset function, which is like an
 *  "information"  operation.  This is always called by the framework
 *  before calling the AE_TII_apply function, and only deals with
 *  control data, not sample data.
 *
 *  We do not actually reset our state here.  Instead we flag a discontinuity
 *  via a flag and return immediately.  This has two advantages:
 *      - It prevents us from repeatedly clearing buffers in cases where 
 *        AE2 is not operating
 *      - It allows us to combine code that handles real-time state changes
 *        and discontinuos state changes into one function (AE_TII_setup())
 *        making it easier to maintain.
 */

Int AE_TII_reset(IAE_Handle handle, PAF_AudioFrame * pAudioFrame)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    ae->initDone = 0;
    return 0;
}


/*
 *  ======== AE_TII_process ========
 *  TII's implementation of the process operation.
 *
 *  This function is called by AE_TII_apply above, after the coefficients
 *  have been set up and the filters need to be run.
 *
 */

Int AE_TII_process(IAE_Handle handle, PAF_AudioFrame * pAudioFrame)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    Int sampleCount = pAudioFrame->sampleCount; // Number of samples in the audio frame
                                                // (in each channel).

    // Get pointers to left, right, and center channel audio sample buffers, from the 
    // Audio Frame Structure:
    PAF_AudioData *restrict left = pAudioFrame->data.sample[PAF_LEFT];
    PAF_AudioData *restrict rght = pAudioFrame->data.sample[PAF_RGHT];

    PAF_AudioSize *restrict samsiz = pAudioFrame->data.samsiz;  // Audio size.


    if (ae->skipCombs) { 
        // By pass comb filters
        CPL_vecScale(ae->preScale, left, ae->reverbOut[PAF_LEFT], sampleCount);
        CPL_vecScale(ae->preScale, rght, ae->reverbOut[PAF_RGHT], sampleCount);
    } else {
        // Prescale the left channel
        CPL_vecScale(ae->preScale, left, ae->reverbIn[PAF_LEFT], sampleCount);
        // Clear the reverb output buffer
        CPL_vecSet(0.0f, ae->reverbOut[PAF_LEFT], pAudioFrame->sampleCount);
        // Apply the comb filters
        ae->config.applyFilter(handle, handle->fxns->comb, 
                ae->reverbIn[PAF_LEFT], ae->reverbOut[PAF_LEFT],
                ae->combBuf[PAF_LEFT], ae->combState[PAF_LEFT],
                ae->combCoef[PAF_LEFT],
                ae->config.numComb, pAudioFrame->sampleCount);
        // Repeat for the right channel
        CPL_vecScale(ae->preScale, rght, ae->reverbIn[PAF_RGHT], sampleCount);
        CPL_vecSet(0.0f, ae->reverbOut[PAF_RGHT], pAudioFrame->sampleCount);
        ae->config.applyFilter(handle, handle->fxns->comb, 
                ae->reverbIn[PAF_RGHT], ae->reverbOut[PAF_RGHT],
                ae->combBuf[PAF_RGHT], ae->combState[PAF_RGHT],
                ae->combCoef[PAF_RGHT],
                ae->config.numComb, pAudioFrame->sampleCount);
    }

    if (!ae->skipAllPass) {
        // Apply the all-pass filters
        ae->config.applyFilter(handle, handle->fxns->allPass,
                ae->reverbOut[PAF_LEFT], ae->reverbOut[PAF_LEFT],
                ae->allPassBuf[PAF_LEFT], NULL, ae->allPassCoef[PAF_LEFT],
                ae->config.numAllPass, pAudioFrame->sampleCount);
        ae->config.applyFilter(handle, handle->fxns->allPass,
                ae->reverbOut[PAF_RGHT], ae->reverbOut[PAF_RGHT],
                ae->allPassBuf[PAF_RGHT], NULL, ae->allPassCoef[PAF_RGHT],
                ae->config.numAllPass, pAudioFrame->sampleCount);
    }

    // Final mix:
    //  left = dry*left + wet*revOutLeft + xwet*revOutRght
    CPL_vecLinear2(ae->dryMix, left, ae->wetMix, ae->reverbOut[PAF_LEFT], left, sampleCount);
    CPL_vecLinear2(1.0f, left, ae->xwetMix, ae->reverbOut[PAF_RGHT], left, sampleCount);
    // rght = dry*rght + wet*revOutRght + xwet*revOutLeft
    CPL_vecLinear2(ae->dryMix, rght, ae->wetMix, ae->reverbOut[PAF_RGHT], rght, sampleCount);
    CPL_vecLinear2(1.0f, rght, ae->xwetMix, ae->reverbOut[PAF_LEFT], rght, sampleCount);
    
    // Set audio size for Center channel:
    //  Samsiz is in units of 0.5 dB. So 2*6 is 6dB (a factor of two). 
    samsiz[PAF_LEFT] = 2 * 6 + samsiz[PAF_LEFT];
    samsiz[PAF_RGHT] = 2 * 6 + samsiz[PAF_RGHT];

    return 0;
}


/*
 *  ======== AE_TII_setup ========
 *  TII's implementation of the setup operation.
 *
 *  This function is called by AE_TII_apply above to ready the filter
 *  states and coefficients prior to processing the data. One of the two 
 *  scenarios below are possible:
 *      A. There has been a discontinuity in processing the data
 *         i.e., either this is the first time the algorithm is being called
 *         or the first time after a gap when the algorithm is able to run
 *         or the first call after the framework explicity called the
 *         AE_TII_reset function.
 *         This state is indicated when ae->initDone flag is FALSE
 *      B. The function is being called in steady state. i.e., the previous
 *         AE_TII_apply call succeeded in procesing audio
 *         This state is indicated when ae->initDone flag is TRUE
 *
 *  The actions needed to taken by this function is slightly different in
 *  two cases.
 *      If ae->initDone is FALSE
 *          - Clear all delay buffer
 *          - Clear all other filter states
 *          - Re-calculate scaling and filter coefficients based on
 *            current pStatus settings
 *          - Set ae->initDone to TRUE
 *      If ae->initDone is TRUE
 *          - Re-calculate scaling and filter coefficients based on
 *            current pStatus settings and take these values used by
 *            the AE_TII_process function closer to their target values
 *  
 */

Int AE_TII_setup(IAE_Handle handle, PAF_AudioFrame * pAudioFrame)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    Int i, j;
    PAF_AudioData a, b;

    if (ae->skipCombs != ae->pStatus->skipCombs ||
            ae->skipAllPass != ae->pStatus->skipAllPass) {
        ae->skipCombs = ae->pStatus->skipCombs;
        ae->skipAllPass = ae->pStatus->skipAllPass;
        ae->initDone = 0;
    }
    if (!ae->initDone) {
        if (ae->pScratch[0])
            memset(ae->pScratch[0], 0, AE_MAX_FSIZE*sizeof(PAF_AudioData));
        for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
            for (j = 0; j  < ae->config.numComb; ++j) {
                ae->config.cdlClear(handle, &(ae->combBuf[i][j]));
                ae->combBuf[i][j].ndx = 0;
                ae->combState[i][j][0] = 0;
            }
        }
        for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
            for (j = 0; j  < ae->config.numAllPass; ++j) {
                ae->config.cdlClear(handle, &(ae->allPassBuf[i][j]));
                ae->allPassBuf[i][j].ndx = 0;
                ae->allPassCoef[i][j][0] = 0.5;
                ae->allPassCoef[i][j][1] = 1.0;
            }
        }
    }

    // Re-calculate mix coefficients as per user request
    if (!ae->initDone) {
        a = 0.0f;
        b = 1.0f;
    } else {
        a = 1.0f - ae->config.alphaScale;
        b = ae->config.alphaScale;
    }
    ae->preScale = a*ae->preScale + b*(ae->pStatus->preScale/32768.0f);
    ae->dryMix = a*ae->dryMix + b*(ae->pStatus->dryMix/32768.0f);
    ae->wetMix = a*ae->wetMix + b*(ae->pStatus->wetMix/32768.0f);
    ae->xwetMix = a*ae->xwetMix + b*(ae->pStatus->xwetMix/32768.0f);

    // Re-calculate filter coefficients as per user request
    if (!ae->initDone) {
        a = ae->pStatus->damping/32768.0f;
        b = ae->pStatus->roomSize/32768.0f;
    } else {
        a = (1.0f - ae->config.alphaCoef)*ae->combCoef[0][0][1] +
                ae->config.alphaCoef*ae->pStatus->damping/32768.0f;
        b = (1.0f - ae->config.alphaCoef)*ae->combCoef[0][0][2] +
                ae->config.alphaCoef*ae->pStatus->roomSize/32768.0f;
    }
    for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
        for (j = 0; j < ae->config.numComb; ++j) {
            ae->combCoef[i][j][0] = 1 - a;
            ae->combCoef[i][j][1] = a;
            ae->combCoef[i][j][2] = b;
        }
    }
    ae->initDone = 1;
    return 0;
}

/*
 *  ======== AE_TII_comb ========
 *  TII's implementation of the comb operation.
 *
 *  This function implements one instance of the comb filter
 */

Int AE_TII_comb(IAE_Handle handle, PAF_AudioData * pIn,
                PAF_AudioData * pOut, PAF_AudioData * pDelay,
                PAF_AudioData * pState, PAF_AudioData * pCoef,
                Int nSamples)
{
    Int i;

    for (i = 0; i < nSamples; ++i) {
        pOut[i] += pDelay[i];
        pState[0] = pDelay[i] * pCoef[0] + pState[0] * pCoef[1];
        pDelay[i] = pIn[i] + pState[0] * pCoef[2];
    }
    return 0;
}

/*
 *  ======== AE_TII_allPass ========
 *  TII's implementation of the allPass operation.
 *
 *  This function implements one instance of the all-pass filter
 */

Int AE_TII_allPass(IAE_Handle handle, PAF_AudioData * pIn,
                   PAF_AudioData * pOut, PAF_AudioData * pDelay,
                   PAF_AudioData * pState, PAF_AudioData * pCoef,
                   Int nSamples)
{
    Int i;
    PAF_AudioData out;
    _nassert(pIn == pOut);
    for (i = 0; i < nSamples; ++i) {
        out = pDelay[i] - pIn[i];
        pDelay[i] = pIn[i] * pCoef[1] + pDelay[i] * pCoef[0];
        pOut[i] = out;
    }
    return 0;
}
