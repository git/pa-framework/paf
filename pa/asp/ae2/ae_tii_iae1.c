
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module implementation - TII implementation of an
 *  ASP Example Demonstration algorithm.
 */

#include <std.h>

#include <iae.h>
#include <ae_tii.h>
#include <ae_tii_priv.h>
#include <aeerr.h>

#include  <std.h>
#include <xdas.h>

#include "paftyp.h"
#include "cpl.h"
#include <string.h>             // for memset, memcpy

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif                          /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== AE_TII_applyFilterDirect ========
 *  TII's implementation of the  AE_TII_applyFilter operation.
 *
 *  This function is simplest of the three versions of functions that
 *  apply a set of filters (Comb or All Pass) on the input audio data. In
 *  this version, we do not do anything special to speed up external
 *  memory access and simply access these buffers directly from off chip. 
 *
 *  On the DA6xx series of devices, when the L2 cache is used, this 
 *  function will undergo some performance degradation on account of L2
 *  and SDRAM  latencies. 
 *
 *  On the DA7xx series of devices (which do not have an L2 cache), 
 *  this function will underperform by a big margin.
 *
 */

#ifndef _TMS320C6X
#define restrict
#endif                          /* _TMS320C6X */

Int AE_TII_applyFilterDirect(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut, 
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount)
{
    //AE_TII_Obj *restrict ae = (Void *) handle;
    Int i, sC, cOffset;
        
    for (i = 0; i < filterCount; ++i) {
        sC = sampleCount;
        cOffset = 0;
        if (pBuf[i].ndx + sC > pBuf[i].len) {
            cOffset = pBuf[i].len - pBuf[i].ndx;
            (*pFilter)(handle, pIn, pOut,
                    pBuf[i].ptr + pBuf[i].ndx, 
                    pState?pState[i]:NULL, pCoef?pCoef[i]:NULL,
                    cOffset);
            pBuf[i].ndx = 0;
        }
        (*pFilter)(handle, pIn + cOffset, pOut + cOffset,
                pBuf[i].ptr + pBuf[i].ndx, 
                pState?pState[i]:NULL, pCoef?pCoef[i]:NULL,
                sC - cOffset);
        pBuf[i].ndx += sC - cOffset;
        if (pBuf[i].ndx >= pBuf[i].len)
            pBuf[i].ndx -= pBuf[i].len;
    }
    return 0;
}

Int AE_TII_cdlClearDirect(IAE_Handle handle, IAE_Cdl *pBuf)
{
    //AE_TII_Obj *restrict ae = (Void *) handle;
    memset(pBuf->ptr, 0, pBuf->len*sizeof(PAF_AudioData));
    return 0;
}
