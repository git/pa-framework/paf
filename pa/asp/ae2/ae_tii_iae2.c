
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module implementation - TII implementation of an
 *  ASP Example Demonstration algorithm.
 */

#include <std.h>

#include <iae.h>
#include <ae_tii.h>
#include <ae_tii_priv.h>
#include <aeerr.h>

#include  <std.h>
#include <xdas.h>

#if (PAF_DEVICE&0xFF000000) == 0xD8000000
/* Include the legacy csl 2.0 dat header */
#include <csl_dat_primus.h>

/* Include EDMA3 low level driver specific implementation APIs for dat */
#include <csl2_dat_edma3lld.h>
#endif

#include "paftyp.h"
#include "cpl.h"
#include <string.h>             // for memset, memcpy
#if (PAF_DEVICE&0xFF000000) != 0xD8000000
#if (PAF_DEVICE&0xFF000000) == 0xD7000000
#include <dmax_dat.h> 
#else
#include <csl.h>
#include <csl_dat.h>
#endif
#endif
#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif                          /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== AE_TII_applyFilterSimpleDMA ========
 *  TII's implementation of the  AE_TII_applyFilter operation.
 *
 *  This function is one of the three versions of functions that
 *  apply a set of filters (Comb or All Pass) on the input audio data.  In this version,
 *  we DMA the external delay buffers on-chip before actually processing the
 *  data.  In other words, we take care to never directly access an off-chip 
 *  buffer.  However, some amount CPU cycles are sacrificed waiting for the
 *  DMA transfers to complete.
 *
 *  On the DA6xx series of devices, this function will not perform 
 *  significantly better than the simpler implementation [applyFilterDirect()]
 *  that leverages the L2 cache.  In some cases, depending on system use of
 *  the EDMA, this function may even perform worse than applyFilterDirect().
 *  However, it may allow for on-chip memory to be recovered for other purposes
 *  without sacrificing too many MHz.
 *
 *  On the DA7xx series of devices (which do not have an L2 cache), 
 *  this function will outperform applyFilterDirect(), but will still not
 *  make optimal use of available CPU and memory band-width.
 *
 */

#ifndef _TMS320C6X
#define restrict
#endif                          /* _TMS320C6X */

Int AE_TII_applyFilterSimpleDMA(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut, 
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    Int i, cOffset, txr;
    PAF_AudioData *pScratch = ae->pScratch[0];
    for (i = 0; i < filterCount; ++i) {
        // Transfer the delay buffer to on-chip scratch area
        if (pBuf[i].ndx + sampleCount > pBuf[i].len) {
            cOffset = pBuf[i].len - pBuf[i].ndx;
            txr = DAT_copy(pBuf[i].ptr + pBuf[i].ndx, pScratch, 
                    cOffset*sizeof(PAF_AudioData));
            DAT_wait(txr);
            txr = DAT_copy(pBuf[i].ptr, pScratch + cOffset,
                    (sampleCount - cOffset)*sizeof(PAF_AudioData));
            DAT_wait(txr);
        } else {
            txr = DAT_copy(pBuf[i].ptr + pBuf[i].ndx, pScratch,
                    sampleCount*sizeof(PAF_AudioData));
            DAT_wait(txr);
        }
        
        // Call the filter
        (*pFilter)(handle, pIn, pOut, pScratch, 
                pState?pState[i]:NULL, pCoef?pCoef[i]:NULL,
                sampleCount);
        
        // Transfer the on-chip scratch delay buffer to off-chip
        if (pBuf[i].ndx + sampleCount > pBuf[i].len) {
            cOffset = pBuf[i].len - pBuf[i].ndx;
            txr = DAT_copy(pScratch, pBuf[i].ptr + pBuf[i].ndx,
                    cOffset*sizeof(PAF_AudioData));
            DAT_wait(txr);
            txr = DAT_copy(pScratch + cOffset, pBuf[i].ptr,
                    (sampleCount - cOffset)*sizeof(PAF_AudioData));
            DAT_wait(txr);
        } else {
            txr = DAT_copy(pScratch, pBuf[i].ptr + pBuf[i].ndx,
                    sampleCount*sizeof(PAF_AudioData));
            DAT_wait(txr);
        }
        
        // Update the circular delay buffer pointers
        pBuf[i].ndx += sampleCount;
        if (pBuf[i].ndx >= pBuf[i].len)
            pBuf[i].ndx -= pBuf[i].len;
    }
    return 0;
}

Int AE_TII_cdlClearDMA(IAE_Handle handle, IAE_Cdl *pBuf)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    PAF_AudioData *ptr, *pScratch = ae->pScratch[0];
    Int txr, len, clen;
    ptr = pBuf->ptr;
    len = pBuf->len;
    while (len) {
        clen = (len < AE_MAX_FSIZE)?len:AE_MAX_FSIZE;
        txr = DAT_copy(pScratch, ptr, clen*sizeof(PAF_AudioData));
        DAT_wait(txr);
        len -= clen;
    }
    return 0;
}


