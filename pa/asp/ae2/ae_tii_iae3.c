
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module implementation - TII implementation of an
 *  ASP Example Demonstration algorithm.
 */

#include <std.h>

#if(PAF_DEVICE&0xFF000000) == 0xD8000000
/* Include the legacy csl 2.0 dat header */
#include <csl_dat_primus.h>

/* Include EDMA3 low level driver specific implementation APIs for dat */
#include <csl2_dat_edma3lld.h>
#define SBUF(fNo)    ((fNo)&3)
#else
#include <csl.h>
#include <csl_dat.h>
#define SBUF(fNo)    ((fNo)&1)
#endif
#include "cpl.h"
#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif                          /* PAF_AUDIODATATYPE_FIXED */

#include <iae.h>
#include <ae_tii.h>
#include <ae_tii_priv.h>
#include <aeerr.h>

#include  <std.h>
#include <xdas.h>

#include "paftyp.h"

#include <string.h>             // for memset, memcpy

#if (PAF_DEVICE&0xFF000000) == 0xD7000000
#include <dmax_dat.h>
#define SBUF(fNo)    ((fNo)&3)
#endif


/*
 *  ======== AE_TII_applyFilterConcurrentDMA ========
 *  TII's implementation of the  AE_TII_applyFilter operation.
 *
 *  This function is one of the three versions of functions that apply a
 *  set of filters (Comb or All Pass) on the input audio data.  In this
 *  version, we DMA the external delay buffers on-chip before actually
 *  processing the data.  In other words, we take care to never directly
 *  access an off-chip  buffer.  We also ensure that we do not spend too
 *  many CPU cycles waiting  for DMA transfers to complete by making sure
 *  that transfers are fired well before the need for a buffer arises.
 *  Achiving this parallelism demands some amount of implementation
 *  complexity, but the results more than justify it.
 *
 *  On the DA6xx series of devices, this function will perform  better
 *  than the simpler implementation [applyFilterDirect()] that leverages
 *  the L2 cache and may also allow for recovering some amount of on-chip
 *  memory from the L2 cache.  It will also be better than the simpler
 *  use of DMA as illustrated in  AE_TII_applyFilterSimpleDMA
 *
 *  On the DA7xx series of devices (which do not have an L2 cache),
 *  this function will outperform applyFilterDirect() and achieves
 *  near optimal use of CPU and memory bandwidth.
 *
 */

#ifndef _TMS320C6X
#define restrict
#endif                          /* _TMS320C6X */

Int AE_TII_applyFilterConcurrentDMA(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut,
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount)
{
    AE_TII_Obj *restrict ae = (Void *) handle;
    Int fNo, cOffset, iTxr[2], oTxr[2];

    // Either 2 or 4 scratch buffers are needed to provide space on-chip
    // to hold portions of delay buffers that we are working on or have
    // just finished working on
    // On DA6xx, the DAT API internally uses QDMA (see spru189f.pdf).
    // Using the QDMA guarantees that transfers always finish in the
    // same sequence in which they were fired.  This allows us to
    // get away with using only 2 on-chip buffers.
    // On DA7xx, the DAT API uses dMAX (see XYZ). Here, no
    // guarantees are made with respect to sequence in which transfers
    // complete.  Given the way we implement the state machine, this will
    // require us to keep atleast 3 on-chip buffers (buffer being filled with
    // input, buffer filled with output which is being transferred off-chip,
    // and a buffer that CPU is working on).  It is simpler to implement if
    // we use 4 buffers, though, and since this costs us a mere 1K extra we
    // do so (see the SBUF() macro above).
    PAF_AudioData **pScratch = ae->pScratch;
    enum {
        // The filters are applied from within a state machine that takes
        // care of bringing buffers on-chip and taking them off-chip when
        // done.  Using an FSM allows us to effectively _anticipate_
        // when buffers will be required resulting in parallel use of the
        // CPU and memory bandwidth.  Here's ehat each state is used for:
        START,      // - Initialize variables
        INPUT_WAIT, // - Wait for input transfer(s) to finish
        INPUT,      // - Fire a DMA transfer(s) to bring input on-chip
        PROCESS,    // - Apply a single filter on delay buffer present on-chip
        OUTPUT,     // - Fire a DMA transfer(s) to take delay buffer off-chip
        OUTPUT_WAIT,// - Wait for output transfer(s) to finish
        DONE        // - Use to exit the FSM
    } state = START;
    // To get the all the delay buffer regions required to run one filter, one
    // or two transfers may need to be  started depending on whether a chunk
    // of data "wraps around" the circular buffer or not. These two variables
    // indicate if two transfers have been fired (or not) for the last input
    // and output respectively
    Int i2Txr, o2Txr;

    // A single parameter (fNo) is passed between states to indicate the
    // filter on which the state must operate.  In the comments below, the
    // notation "STATE(n)" is used to indicate that the state named "STATE"
    // is invoked with "fNo" set to "n"
    //
    // The state machine should transition as follows:
    //  START   ->  INPUT(0) ->
    //              INPUT_WAIT(0) ->
    //              INPUT(1) ->
    //              PROCESS(0) ->
    //              OUTPUT(0) ->
    //              **                <-----\
    //              INPUT_WAIT(1) ->        |
    //              INPUT(2) ->             | This part is repeated
    //              PROCESS(1) ->           | for each filter between
    //              OUTPUT_WAIT(0) ->       | the first and the last
    //              OUTPUT(1) ->            |
    //              **                >-----/
    //              INPUT_WAIT(N-1) ->
    //              PROCESS(N-1) ->
    //              OUTPUT_WAIT(N-2) ->
    //              OUTPUT(N-1) ->
    //              OUTPUT_WAIT(N-1) ->         DONE
    // The transition from each state's perspective looks as follows:
    //  START           ->  INPUT(0)
    //  INPUT(n)        ->  INPUT_WAIT(n), if n == 0
    //                   >  PROCESS(n-1),  o.w.
    //  INPUT_WAIT(n)   ->  PROCESS(n), if n == N-1
    //                   >  INPUT(n+1), o.w.
    //  PROCESS(n)      ->  OUTPUT(n), if n == 0
    //                   >  OUTPUT_WAIT(n-1), o.w.
    //  OUTPUT(n)       ->  OUTPUT_WAIT(n), if n == N-1
    //                   >  INPUT_WAIT(n+1), o.w.,
    //  OUTPUT_WAIT(n)  ->  DONE, if n == N-1
    //                   >  OUTPUT(n+1), o.w.
    //
    while (state != DONE) {
        switch (state) {
        case START:
            pScratch = ae->pScratch;
            i2Txr = o2Txr = 0;
            state = INPUT, fNo = 0;
            break;
        case INPUT:
            if (pBuf[fNo].ndx + sampleCount > pBuf[fNo].len) {
                cOffset = pBuf[fNo].len - pBuf[fNo].ndx;
                iTxr[0] = DAT_copy(pBuf[fNo].ptr + pBuf[fNo].ndx,
                            pScratch[SBUF(fNo)],
                            cOffset*sizeof(PAF_AudioData));
                iTxr[1] = DAT_copy(pBuf[fNo].ptr,
                            pScratch[SBUF(fNo)] + cOffset,
                            (sampleCount-cOffset)*sizeof(PAF_AudioData));
                i2Txr = 1;
            } else {
                iTxr[0] = DAT_copy(pBuf[fNo].ptr + pBuf[fNo].ndx,
                            pScratch[SBUF(fNo)],
                            sampleCount*sizeof(PAF_AudioData));
                i2Txr = 0;
            }
            if (fNo == 0)
                state = INPUT_WAIT;
            else
                state = PROCESS, --fNo;
            break;
        case INPUT_WAIT:
            DAT_wait(iTxr[0]);
            if (i2Txr)
                DAT_wait(iTxr[1]);
            if (fNo == filterCount-1)
                state = PROCESS;
            else
                state = INPUT, ++fNo;
            break;
        case PROCESS:
            (*pFilter)(handle, pIn, pOut, pScratch[SBUF(fNo)],
                pState?pState[fNo]:NULL, pCoef?pCoef[fNo]:NULL,
                sampleCount);
            if (fNo == 0)
                state = OUTPUT;
            else
                state = OUTPUT_WAIT, --fNo;
            break;
        case OUTPUT:
            if (pBuf[fNo].ndx + sampleCount > pBuf[fNo].len) {
                cOffset = pBuf[fNo].len - pBuf[fNo].ndx;
                oTxr[0] = DAT_copy(pScratch[SBUF(fNo)],
                            pBuf[fNo].ptr + pBuf[fNo].ndx,
                            cOffset*sizeof(PAF_AudioData));
                oTxr[1] = DAT_copy(pScratch[SBUF(fNo)] + cOffset,
                            pBuf[fNo].ptr,
                            (sampleCount-cOffset)*sizeof(PAF_AudioData));
                o2Txr = 1;
            } else {
                oTxr[0] = DAT_copy(pScratch[SBUF(fNo)],
                            pBuf[fNo].ptr + pBuf[fNo].ndx,
                            sampleCount*sizeof(PAF_AudioData));
                o2Txr = 0;
            }
            // Delay buffer pointers are updated here
            pBuf[fNo].ndx += sampleCount;
            if (pBuf[fNo].ndx >= pBuf[fNo].len)
                pBuf[fNo].ndx -= pBuf[fNo].len;
            if (fNo == filterCount-1)
                state = OUTPUT_WAIT;
            else
                state = INPUT_WAIT, ++fNo;
            break;
        case OUTPUT_WAIT:
            DAT_wait(oTxr[0]);
            if (o2Txr)
                DAT_wait(oTxr[1]);
            if (fNo == filterCount-1)
                state = DONE;
            else
                state = OUTPUT, ++fNo;
            break;
        }
    }
    return 0;
}
