
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IAE default instance creation parameters
 */
#include <std.h>

#include <iae.h>

#include "paftyp.h"

/*
 *  ======== IAE_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of AE objects.
 */
const IAE_Status IAE_PARAMS_STATUS = {
    sizeof(IAE_Status),     /* Size of this structure.  */
    1,                      /* AE Mode Control Register.  1=enabled. 0=disabled.  */
    0,                      /* Unused.  */

    27525,                  /* roomSize */
    6554,                   /* damping */

    983,                    /* preScale */

    10892,                  /* wetMix */
    10892,                  /* xwetMix */
    10892,                  /* dryMix */

    0,                      /* skipCombs */
    0,                      /* skipAllPass */
};
const IAE_Params IAE_PARAMS_DIRECT = {
    sizeof(IAE_Params),     /* size */
    &IAE_PARAMS_STATUS,     /* pStatus */
    /* config */
    IAE_CDL_ACCESS_DIRECT, /* cdlAccess */
    AE_TII_applyFilterDirect,
    AE_TII_cdlClearDirect,
    0.1,        /* alphaScale */
    0.01,       /* alphaCoef  */
    8, /* numComb */
    {   /* combLen */                    
        {                   /* left channel */
            1215,
            1293,
            1390,
            1476,
            1548,
            1623,
            1695,
            1760,
        },
        {                   /* right channel */
            1240,
            1318,
            1415,
            1501,
            1573,
            1648,
            1720,
            1785,
        }
    },
    4, /* numAllPass */
    { /* allPassLen */
        {                   /* left channel */
            605,
            480,
            371,
            260,
        },
        {                    /* right channel */
            630,
            505,
            396,
            270,
        }
    }
};

asm(" .global _IAE_PARAMS");
asm("_IAE_PARAMS .set _IAE_PARAMS_DIRECT");
