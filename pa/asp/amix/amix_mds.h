
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) One-to-Many Audio Mixer Algorithm interface declarations
//
//
//

/*
 *  Vendor specific (MDS) interface header for One-to-Many Audio Mixer Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular AMIX implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef AMIX_MDS_
#define AMIX_MDS_

#include <ialg.h>

#include "iamix.h"
#include "icom.h"

/*
 *  ======== AMIX_MDS_exit ========
 *  Required module finalization function
 */
#define AMIX_MDS_exit COM_TII_exit

/*
 *  ======== AMIX_MDS_init ========
 *  Required module initialization function
 */
#define AMIX_MDS_init COM_TII_init

/*
 *  ======== AMIX_MDS_IALG ========
 *  MDS's implementation of AMIX's IALG interface
 */
extern IALG_Fxns AMIX_MDS_IALG; 

/*
 *  ======== AMIX_MDS_IAMIX ========
 *  MDS's implementation of AMIX's IAMIX interface
 */
extern const IAMIX_Fxns AMIX_MDS_IAMIX; 

/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's One-to-Many Audio Mixer Algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== AMIX_MDS_Handle ========
 */
typedef struct AMIX_MDS_Obj *AMIX_MDS_Handle;

/*
 *  ======== AMIX_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IAMIX.
 */
typedef IAMIX_Params AMIX_MDS_Params;

/*
 *  ======== AMIX_MDS_Status ========
 *  We don't add any new status to the standard one defined by IAMIX.
 */
typedef IAMIX_Status AMIX_MDS_Status;

/*
 *  ======== AMIX_MDS_Config ========
 *  We don't add any new config to the standard one defined by IAMIX.
 */
typedef IAMIX_Config AMIX_MDS_Config;

/*
 *  ======== AMIX_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define AMIX_MDS_PARAMS   IAMIX_PARAMS

/*
 *  ======== AMIX_MDS_create ========
 *  Create a AMIX_MDS instance object.
 */
extern AMIX_MDS_Handle AMIX_MDS_create(const AMIX_MDS_Params *params);

/*
 *  ======== AMIX_MDS_delete ========
 *  Delete a AMIX_MDS instance object.
 */
#define AMIX_MDS_delete COM_TII_delete

#endif  /* AMIX_MDS_ */
