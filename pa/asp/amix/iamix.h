
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common One-to-Many Audio Mixer Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the One-to-Many Audio Mixer Algorithm.
 */
#ifndef IAMIX_
#define IAMIX_

#include <ialg.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"

/*
 *  ======== IAMIX_Obj ========
 *  Every implementation of IAMIX *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IAMIX_Obj {
    struct IAMIX_Fxns *fxns;    /* function list: standard, public, private */
} IAMIX_Obj;

/*
 *  ======== IAMIX_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IAMIX_Obj *IAMIX_Handle;

/*
 *  ======== IAMIX_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  by alpha commands during real-time operation of the algorithm.
 */
typedef volatile struct IAMIX_Status {
    Int size;               // standard
    XDAS_Int8 mode;	
    XDAS_Int8 unused1;      // keep alignment
    // write 1 to ack to trigger update to equal gain
    XDAS_Int8 stream_A_EqualBalance_ack;
    XDAS_Int8 stream_B_EqualBalance_ack;
           
    // A gain value in half dB units, -240 up to +24
    XDAS_Int16 stream_A_EqualBalance_set;    
    XDAS_Int16 stream_B_EqualBalance_set;    

    // writing here applies this output gain equally to all channels in a stream.
    // no need for ack as this is converted to float each time through the loop.
    XDAS_Int16 stream_A_OutputGain_set;    
    XDAS_Int16 stream_B_OutputGain_set;    

    // one gain value for each channel of each stream into each output channel.
    // value is in half dB.  Allow positive and negative.
    XDAS_Int16 gainMatrix_A[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];
    XDAS_Int16 gainMatrix_B[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];

    XDAS_UInt16 rampTime_A;
    XDAS_UInt16 rampTime_B;
} IAMIX_Status;

/*
 *  ======== IAMIX_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IAMIX_Config {
    float volumeRamp_A;
    float volumeRamp_B;
    float fGainMatrix_A[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];
    float fGainMatrix_B[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];
} IAMIX_Config;

/*
 *  ======== IAMIX_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a AMIX object.
 *
 *  Every implementation of IAMIX *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IAMIX_Params {
    Int size;
    const IAMIX_Status *pStatus;
    IAMIX_Config config;
} IAMIX_Params;

/*
 *  ======== IAMIX_PARAMS ========
 *  Default instance creation parameters (defined in iamix.c)
 */
extern const IAMIX_Params IAMIX_PARAMS;

/*
 *  ======== IAMIX_Fxns ========
 *  All implementation's of AMIX must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is AMIX_<vendor>_IAMIX, where
 *  <vendor> is the vendor name.
 */
typedef struct IAMIX_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IAMIX_Handle, PAF_AudioFrame *);
    Int         (*apply)(IAMIX_Handle, PAF_AudioFrame *);
    /* private */
    Int         (*mixit)(IAMIX_Handle, PAF_AudioFrame *, PAF_AudioFrame *, PAF_AudioFrame *, Int);
    float       (*volRamp)(IAMIX_Handle, PAF_AudioFrame *, 
                           float, PAF_AudioSize, XDAS_UInt16);
} IAMIX_Fxns;

/*
 *  ======== IAMIX_Cmd ========
 *  The Cmd enumeration defines the control commands for the AMIX
 *  control method.
 */
typedef enum IAMIX_Cmd {
    IAMIX_NULL                   = ICOM_NULL,
    IAMIX_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IAMIX_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IAMIX_GETSTATUS              = ICOM_GETSTATUS,
    IAMIX_SETSTATUS              = ICOM_SETSTATUS
} IAMIX_Cmd;

#endif  /* IAMIX_ */
