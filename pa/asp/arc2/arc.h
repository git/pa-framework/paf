
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ARC algorithm interace declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by
 *  applications that use the ARC (Asynchronous sample-Rate Conversion) algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the ARC
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */

#ifndef ARC_
#define ARC_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <iarc.h>

#include "paftyp.h"

/*
 *  ======== ARC_Handle ========
 *  ARC (Asynchronous sample-Rate Conversion) algorithm instance handle
 */
typedef struct IARC_Obj *ARC_Handle;

/*
 *  ======== ARC_Params ========
 *  ARC (Asynchronous sample-Rate Conversion) algorithm instance creation parameters
 */
typedef struct IARC_Params ARC_Params;

/*
 *  ======== ARC_PARAMS ========
 *  Default instance parameters
 */
#define ARC_PARAMS IARC_PARAMS

/*
 *  ======== ARC_Status ========
 *  Status structure for getting ARC instance attributes
 */
typedef volatile struct IARC_Status ARC_Status;

/*
 *  ======== ARC_Cmd ========
 *  This typedef defines the control commands ARC objects
 */
typedef IARC_Cmd   ARC_Cmd;

/*
 * ===== control method commands =====
 */
#define ARC_NULL IARC_NULL
#define ARC_GETSTATUSADDRESS1 IARC_GETSTATUSADDRESS1
#define ARC_GETSTATUSADDRESS2 IARC_GETSTATUSADDRESS2
#define ARC_GETSTATUS IARC_GETSTATUS
#define ARC_SETSTATUS IARC_SETSTATUS

/*
 *  ======== ARC_create ========
 *  Create an instance of a ARC object.
 */
static inline ARC_Handle ARC_create(const IARC_Fxns *fxns, const ARC_Params *prms)
{
    return ((ARC_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== ARC_delete ========
 *  Delete a ARC instance object
 */
static inline Void ARC_delete(ARC_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== ARC_apply ========
 */
extern Int ARC_apply(ARC_Handle, PAF_AudioFrame *);

/*
 *  ======== ARC_reset ========
 */
extern Int ARC_reset(ARC_Handle, PAF_AudioFrame *);

/*
 *  ======== ARC_exit ========
 *  Module finalization
 */
extern Void ARC_exit(Void);

/*
 *  ======== ARC_init ========
 *  Module initialization
 */
extern Void ARC_init(Void);

#endif  /* ARC_ */
