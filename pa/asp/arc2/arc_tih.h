
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ARC algorithm interface delarations
//
//
//

/*
 *  Vendor specific (TIH) interface header for
 *  ARC (Asynchronous sample-Rate Conversion) algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular ARC implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */

#ifndef ARC_TIH_
#define ARC_TIH_

#include <ialg.h>

#include <iarc.h>
#include <icom.h>

/*
 *  ======== ARC_TIH_exit ========
 *  Required module finalization function
 */
#define ARC_TIH_exit COM_TII_exit

/*
 *  ======== ARC_TIH_init ========
 *  Required module initialization function
 */
#define ARC_TIH_init COM_TII_init

/*
 *  ======== ARC_TIH_IALG ========
 *  TIH's implementation of ARC's IALG interface
 */
extern IALG_Fxns ARC_TIH_IALG; 

/*
 *  ======== ARC_TIH_IARC ========
 *  TIH's implementation of ARC's IARC interface
 */
extern const IARC_Fxns ARC_TIH_IARC; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TIH's
 *  ARC (Asynchronous sample-Rate Conversion) algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== ARC_TIH_Handle ========
 */
typedef struct ARC_TIH_Obj *ARC_TIH_Handle;

/*
 *  ======== ARC_TIH_Params ========
 *  We don't add any new parameters to the standard ones defined by IARC.
 */
typedef IARC_Params ARC_TIH_Params;

/*
 *  ======== ARC_TIH_Status ========
 *  We don't add any new status to the standard one defined by IARC.
 */
typedef IARC_Status ARC_TIH_Status;

/*
 *  ======== ARC_TIH_Config ========
 *  We don't add any new config to the standard one defined by IARC.
 */
typedef IARC_Config ARC_TIH_Config;

/*
 *  ======== ARC_TIH_PARAMS ========
 *  Define our default parameters.
 */
#define ARC_TIH_PARAMS   IARC_PARAMS

/*
 *  ======== ARC_TIH_create ========
 *  Create a ARC_TIH instance object.
 */
extern ARC_TIH_Handle ARC_TIH_create(const ARC_TIH_Params *params);

/*
 *  ======== ARC_TIH_delete ========
 *  Delete a ARC_TIH instance object.
 */
#define ARC_TIH_delete COM_TII_delete

#endif  /* ARC_TIH_ */
