
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common ARC algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the ARC (Asynchronous sample-Rate Conversion) algorithm.
 */

#ifndef IARC_
#define IARC_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"

/*
 *  ======== IARC_Obj ========
 *  Every implementation of IARC *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IARC_Obj {
    struct IARC_Fxns *fxns;    /* function list: standard, public, private */
} IARC_Obj;

/*
 *  ======== IARC_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IARC_Obj *IARC_Handle;

/*
 *  ======== IARC_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in iarc.c.
 */
typedef volatile struct IARC_Status {
    Int size;             /* This value must always be here, and must be set to the
                             total size of this structure in 8-bit bytes, as the
                             sizeof() operator would do. */
    XDAS_Int8 mode;       /* This is the 8-bit ARC Mode Control Register.  All
                             Algorithms must have a mode control register.  */
    XDAS_Int8 unused1[3];
    XDAS_UInt32 inputsPerOutputQ24; /* (mixed) no. of inputs per output sample */
    XDAS_UInt32 outputsRemainingQ24; /* (mixed) no. of (internally buffered) outputs */
    XDAS_UInt32 maxInputsPerOutputQ24; /* Max allowed no. of inputs per output sample */
} IARC_Status;

/*
 *  ======== IARC_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IARC_Config {
    XDAS_Int8 nChannels;
    XDAS_Int8 nTaps;
    XDAS_Int16 mask;
    /// FilterCoefs **coefs;
    PAF_AudioData *pState;
} IARC_Config;

/*
 *  ======== IARC_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a ARC object.
 *
 *  Every implementation of IARC *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 *  This structure is actually instantiated and initialized in iarc.c.
 */
typedef struct IARC_Params {
    Int size;
    const IARC_Status *pStatus;
    IARC_Config config;
} IARC_Params;

/*
 *  ======== IARC_PARAMS ========
 *  Default instance creation parameters (defined in iarc.c)
 */
extern const IARC_Params IARC_PARAMS;

/*
 *  ======== IARC_Fxns ========
 *  All implementation's of ARC must declare and statically
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is ARC_<vendor>_IARC, where
 *  <vendor> is the vendor name.
 */
typedef struct IARC_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IARC_Handle, PAF_AudioFrame *);
    Int         (*apply)(IARC_Handle, PAF_AudioFrame *);
    /* private */
} IARC_Fxns;

/*
 *  ======== IARC_Cmd ========
 *  The Cmd enumeration defines the control commands for the ARC control method.
 */
typedef enum IARC_Cmd {
    IARC_NULL                    = ICOM_NULL,
    IARC_GETSTATUSADDRESS1       = ICOM_GETSTATUSADDRESS1,
    IARC_GETSTATUSADDRESS2       = ICOM_GETSTATUSADDRESS2,
    IARC_GETSTATUS               = ICOM_GETSTATUS,
    IARC_SETSTATUS               = ICOM_SETSTATUS
} IARC_Cmd;

#endif  /* IARC_ */
