
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Join alpha codes
//
//
//

#ifndef _ASJ_A
#define _ASJ_A

#include <acpbeta.h>
#include <pafmac_a.h>
#include <paftyp_a.h>

#define  readASJMode 0xc200+STD_BETA_ASJ,0x0400
#define writeASJModeDisable 0xca00+STD_BETA_ASJ,0x0400
#define writeASJModeEnable 0xca00+STD_BETA_ASJ,0x0401

#if PAF_MAXNUMCHAN == 16

#define  readASJForm 0xc600+STD_BETA_ASJ,0x0810
#define wroteASJForm 0xce00+STD_BETA_ASJ,0x0810
#define writeASJForm16(N0,N1,N2,N3,N4,N5,N6,N7,N8,N9,Na,Nb,Nc,Nd,Ne,Nf) \
        0xce00+STD_BETA_ASJ,0x0810,TWOUP(N0,N1),TWOUP(N2,N3),TWOUP(N4,N5),TWOUP(N6,N7), \
        TWOUP(N8,N9),TWOUP(Na,Nb),TWOUP(Nc,Nd),TWOUP(Ne,Nf)
        /* ...16 (PAF_LBAK, PAF_RBAK, ...) : Left to LBak, Rght to RBak, etc. */

#define writeASJFormDefault \
        writeASJForm16 (0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f,0x0f, 0x0f, 0x30, 0x31, 0x0f, 0x0f, 0x0f, 0x0f)

#define writeASJFormOptionFromToLMN(LL,MM,NN) \
        0xca00+STD_BETA_ASJ,0x0800+(((NN)&0x0f)<<8)+(((LL)&0x0f)<<4)+(((MM)&0x0f)<<0)
        /* Used only indirectly as shown below */

#define writeASJFormNoneFromToMN(MM,NN) writeASJFormOptionFromToLMN (0,MM,NN)
#define writeASJFormAddFromToMN(MM,NN)  writeASJFormOptionFromToLMN (1,MM,NN)
#define writeASJFormInFromToMN(MM,NN)   writeASJFormOptionFromToLMN (2,MM,NN)
#define writeASJFormOnFromToMN(MM,NN)   writeASJFormOptionFromToLMN (3,MM,NN)
        /* ...FromTo (PAF_LEFT, PAF_LBAK) : Left to LBak */
        /* ...FromTo (PAF_RGHT, PAF_RBAK) : Rght to RBak */
        /* ...FromTo (...,      ...)      : etc. */

// To set the Master Gain control for the Primary Input
#define writeASJChannelMasterGainO(N) 0xcb00+STD_BETA_ASJ,0x0018,N
#define readASJChannelMasterGainO 	  0xc300+STD_BETA_ASJ,0x0018

// To set the Master Gain control for the Secondary Input
#define writeASJChannelMasterGainI(N) 0xcb00+STD_BETA_ASJ,0x001A,N
#define readASJChannelMasterGainI 	  0xc300+STD_BETA_ASJ,0x001A

// In alpha codes of form writeASJChannelGainX(N0,N1) to set the Trim Gain,
// N0 - is the channel number for which channel gain is specified
// N1 - channel gain value
#define writeASJChannelTrimGainON(N0,N1) \
        0xcb00+STD_BETA_ASJ,0x1C+N0*2,N1

#define writeASJChannelTrimGainIN(N0,N1) \
        0xcb00+STD_BETA_ASJ,0x3C+N0*2,N1

#define readASJChannelTrimGainON(N0) \
		0xc300+STD_BETA_ASJ,0x1C+N0*2

#define readASJChannelTrimGainIN(N0) \
		0xc300+STD_BETA_ASJ,0x3C+N0*2

#define  readASJChannelTrimGainO 0xc600+STD_BETA_ASJ,0x1C20
#define  wroteASJChannelTrimGainO 0xce00+STD_BETA_ASJ,0x1C20
#define  readASJChannelTrimGainI 0xc600+STD_BETA_ASJ,0x3C20
#define  wroteASJChannelTrimGainI 0xce00+STD_BETA_ASJ,0x3C20

// Alpha commands to set the Ramp Time in msec/dB.
#define writeASJrampTimeO(N) 0xcb00+STD_BETA_ASJ,0x005C,N
#define writeASJrampTimeI(N) 0xcb00+STD_BETA_ASJ,0x005E,N

#define readASJrampTimeO 0xc300+STD_BETA_ASJ,0x005C
#define readASJrampTimeI 0xc300+STD_BETA_ASJ,0x005E


#endif /* PAF_MAXNUMCHAN == 16 */

#define  readASJStatus 0xc508,STD_BETA_ASJ
#define  readASJControl \
         readASJMode, \
         readASJForm, \
         readASJChannelMasterGainO, \
         readASJChannelMasterGainI, \
         readASJChannelTrimGainO, \
         readASJChannelTrimGainI, \
         readASJrampTimeO, \
         readASJrampTimeI

#endif /* _ASJ_A */

