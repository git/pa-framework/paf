
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard Audio Stream Join Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the Audio Stream Join Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the ASJ
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef ASJ_
#define ASJ_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>

#include "iasj.h"
#include "paftyp.h"

/*
 *  ======== ASJ_Handle ========
 *  Audio Stream Join Algorithm instance handle
 */
typedef struct IASJ_Obj *ASJ_Handle;

/*
 *  ======== ASJ_Params ========
 *  Audio Stream Join Algorithm instance creation parameters
 */
typedef struct IASJ_Params ASJ_Params;

/*
 *  ======== ASJ_PARAMS ========
 *  Default instance parameters
 */
#define ASJ_PARAMS IASJ_PARAMS

/*
 *  ======== ASJ_Status ========
 *  Status structure for getting ASJ instance attributes
 */
typedef volatile struct IASJ_Status ASJ_Status;

/*
 *  ======== ASJ_Cmd ========
 *  This typedef defines the control commands ASJ objects
 */
typedef IASJ_Cmd   ASJ_Cmd;

/*
 * ===== control method commands =====
 */
#define ASJ_NULL IASJ_NULL
#define ASJ_GETSTATUSADDRESS1 IASJ_GETSTATUSADDRESS1
#define ASJ_GETSTATUSADDRESS2 IASJ_GETSTATUSADDRESS2
#define ASJ_GETSTATUS IASJ_GETSTATUS
#define ASJ_SETSTATUS IASJ_SETSTATUS

/*
 *  ======== ASJ_create ========
 *  Create an instance of a ASJ object.
 */
static inline ASJ_Handle ASJ_create(const IASJ_Fxns *fxns, const ASJ_Params *prms)
{
    return ((ASJ_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== ASJ_delete ========
 *  Delete a ASJ instance object
 */
static inline Void ASJ_delete(ASJ_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== ASJ_apply ========
 */
extern Int ASJ_apply(ASJ_Handle, PAF_AudioFrame *);

/*
 *  ======== ASJ_reset ========
 */
extern Int ASJ_reset(ASJ_Handle, PAF_AudioFrame *);

/*
 *  ======== ASJ_exit ========
 *  Module finalization
 */
extern Void ASJ_exit(Void);

/*
 *  ======== ASJ_init ========
 *  Module initialization
 */
extern Void ASJ_init(Void);

#endif  /* ASJ_ */
