
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Audio Stream Join Algorithm IALG implementation
//
//
//

/*
 *  ASJ Module IALG implementation - MDS's implementation of the
 *  IALG interface for the Audio Stream Join Algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include "iasj.h"
#include "asj_mds.h"
#include "asj_mds_priv.h"

/*
 *  ======== ASJ_MDS_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== ASJ_MDS_alloc ========
 */
Int ASJ_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IASJ_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IASJ_PARAMS;  /* set default parameters */
    }

    /* Request memory for ASJ object */
    memTab[0].size = (sizeof(ASJ_MDS_Obj)+3)/4*4 + (sizeof(ASJ_MDS_Status)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    return (1);
}

/*
 *  ======== ASJ_MDS_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== ASJ_MDS_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== ASJ_MDS_initObj ========
 */
Int ASJ_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    ASJ_MDS_Obj *asj = (Void *)handle;
    const IASJ_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IASJ_PARAMS;  /* set default parameters */
    }

    asj->pStatus = (ASJ_MDS_Status *)((char *)asj + (sizeof(ASJ_MDS_Obj)+3)/4*4);

    *asj->pStatus = *params->pStatus;
    asj->config = params->config;

    return (IALG_EOK);
}

/*
 *  ======== ASJ_MDS_control ========
 *  MDS's implementation of the control operation.
 */
  /* COM_TIH_control */

/*
 *  ======== ASJ_MDS_moved ========
 */
  /* COM_TIH_moved */

