
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Audio Stream Join Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the Audio Stream Join Algorithm.
 */
#ifndef IASJ_
#define IASJ_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

//#include "temp.h"
#include "icom.h"
#include "paftyp.h"

#include "ztop.h"  // hack, to enable 3 input version
#ifdef NUM_AS_OUT_INPUT_STREAMS
 #if NUM_AS_OUT_INPUT_STREAMS  ==  3
  #define THREE_INPUT
 #endif
#endif

/*
 *  ======== IASJ_Obj ========
 *  Every implementation of IASJ *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IASJ_Obj {
    struct IASJ_Fxns *fxns;    /* function list: standard, public, private */
} IASJ_Obj;

/*
 *  ======== IASJ_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IASJ_Obj *IASJ_Handle;

/*
 *  ======== IASJ_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IASJ_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 unused[3];
    XDAS_Int8 form[PAF_MAXNUMCHAN];
    PAF_AudioSize masterGainO;
    PAF_AudioSize masterGainI;
    PAF_AudioSize chanGainO[PAF_MAXNUMCHAN];
    PAF_AudioSize chanGainI[PAF_MAXNUMCHAN];
    XDAS_UInt16 rampTimeO;
    XDAS_UInt16 rampTimeI;
} IASJ_Status;

/*
 *  ======== IASJ_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IASJ_Config {
    XDAS_Int8 offsetI;
    XDAS_Int8 offsetO;
    XDAS_Int8 unused[2];
    float volumeRampO;
    float volumeRampI;
} IASJ_Config;

/*
 *  ======== IASJ_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a ASJ object.
 *
 *  Every implementation of IASJ *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IASJ_Params {
    Int size;
    const IASJ_Status *pStatus;
    IASJ_Config config;
} IASJ_Params;

/*
 *  ======== IASJ_PARAMS ========
 *  Default instance creation parameters (defined in iasj.c)
 */
extern const IASJ_Params IASJ_PARAMS;

/*
 *  ======== IASJ_Fxns ========
 *  All implementation's of ASJ must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is ASJ_<vendor>_IASJ, where
 *  <vendor> is the vendor name.
 */
typedef struct IASJ_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IASJ_Handle, PAF_AudioFrame *);
    Int         (*apply)(IASJ_Handle, PAF_AudioFrame *);
    /* private */
#ifdef THREE_INPUT
    Int         (*mixit)(IASJ_Handle, PAF_AudioFrame *, PAF_AudioFrame *, PAF_AudioFrame *, Int);
#else
    Int         (*mixit)(IASJ_Handle, PAF_AudioFrame *, PAF_AudioFrame *, Int);
#endif
    float       (*volRamp)(IASJ_Handle, PAF_AudioFrame *, 
                           float, PAF_AudioSize, XDAS_UInt16);
} IASJ_Fxns;

/*
 *  ======== IASJ_Cmd ========
 *  The Cmd enumeration defines the control commands for the ASJ
 *  control method.
 */
typedef enum IASJ_Cmd {
    IASJ_NULL                   = ICOM_NULL,
    IASJ_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IASJ_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IASJ_GETSTATUS              = ICOM_GETSTATUS,
    IASJ_SETSTATUS              = ICOM_SETSTATUS
} IASJ_Cmd;

#endif  /* IASJ_ */
