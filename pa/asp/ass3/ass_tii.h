
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Audio Stream Split Algorithm interface declarations
//
//
//

/*
 *  Vendor specific (TII) interface header for Audio Stream Split Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular ASS implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef ASS_TII_
#define ASS_TII_

#include <ialg.h>

#include "iass.h"
#include "icom.h"

/*
 *  ======== ASS_TII_exit ========
 *  Required module finalization function
 */
#define ASS_TII_exit COM_TII_exit

/*
 *  ======== ASS_TII_init ========
 *  Required module initialization function
 */
#define ASS_TII_init COM_TII_init

/*
 *  ======== ASS_TII_IALG ========
 *  TII's implementation of ASS's IALG interface
 */
extern IALG_Fxns ASS_TII_IALG; 

/*
 *  ======== ASS_TII_IASS ========
 *  TII's implementation of ASS's IASS interface
 */
extern const IASS_Fxns ASS_TII_IASS; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TII's Audio Stream Split Algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== ASS_TII_Handle ========
 */
typedef struct ASS_TII_Obj *ASS_TII_Handle;

/*
 *  ======== ASS_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IASS.
 */
typedef IASS_Params ASS_TII_Params;

/*
 *  ======== ASS_TII_Status ========
 *  We don't add any new status to the standard one defined by IASS.
 */
typedef IASS_Status ASS_TII_Status;

/*
 *  ======== ASS_TII_Config ========
 *  We don't add any new config to the standard one defined by IASS.
 */
typedef IASS_Config ASS_TII_Config;

/*
 *  ======== ASS_TII_PARAMS ========
 *  Define our default parameters.
 */
#define ASS_TII_PARAMS   IASS_PARAMS1

/*
 *  ======== ASS_TII_create ========
 *  Create a ASS_TII instance object.
 */
extern ASS_TII_Handle ASS_TII_create(const ASS_TII_Params *params);

/*
 *  ======== ASS_TII_delete ========
 *  Delete a ASS_TII instance object.
 */
#define ASS_TII_delete COM_TII_delete

#endif  /* ASS_TII_ */
