
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Audio Stream Split Algorithm functionality implementation
//
//   Split one audio stream into multiple, using downmixing as needed.
//
//   The only significant difference between an ASS Algorithm and an ASP
//   Algorithm is that (1) the former references an array of Audio Frames
//   (Audio Frame Array or AFA) while the latter references a single Audio 
//   Frame (Audio Frame Pointer or AFP), and (2) the former downmixes from 
//   the first element of the AFF into later elements of the AFF without
//   altering the first while the latter modifies the Audio Frame via the
//   AFP.
//
//   The downmix implemented here for ASS Algorithm, Number 3, is in accordance 
//   with what's described in DMX Algorithms of various numbers (1, 2, etc.). See
//   Appendices N and O of "pa17-ug_da7xx_<date>.pdf".
//
//   For more information on the implementation of an ASS Algorithm, see
//   the Custom Audio Stream Split Algorithm, Number 1 which provides a
//   template for such implementation.
//
//
//

/*
 *  ASS Module implementation - TII implementation of a Audio Stream Split Algorithm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <iass.h>
#include <ass_tii.h>
#include <ass_tii_priv.h>
#include <asserr.h>

#include "paftyp.h"
#include "ccm.h"

#include "cpl.h"

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

/*
 *  ======== ASS_TII_apply ========
 *  TII's implementation of the apply operation.
 */
Int 
ASS_TII_apply(IASS_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASS_TII_Obj *ass = (Void *)handle;

    PAF_AudioFrame * restrict pAFI = &pAudioFrame[ass->config.offsetI];
    PAF_AudioFrame * restrict pAFO = &pAudioFrame[ass->config.offsetO];

    PAF_ChannelConfiguration FromConfig, ToConfig;
    PAF_ChannelConfiguration ccr;

    CPL_CDM *cdmParam;

    if (ass->pStatus->mode == 0)
        return 0;

    pAFO->sampleDecode = pAFI->sampleDecode;
    pAFO->sampleRate = pAFI->sampleRate;
    pAFO->sampleCount = pAFI->sampleCount;

    CPL_ivecCopy(pAFI->sampleProcess, pAFO->sampleProcess, PAF_SAMPLEPROCESS_N);

    ccr = ass->pStatus->channelConfigurationRequest;

    pAFO->channelConfigurationRequest = ccr;

    if(pAFI->channelConfigurationStream.part.extMask != 0)
	{
	  // Untill ASS and CDM understand handling Height/Width extensions,
	  // ignore the extensions.
	  // do nothing and declare the ccStream as PAF_CC_NONE
       pAFO->channelConfigurationStream.legacy = PAF_CC_NONE;
	}
	else
	{
    cdmParam = &(ass->cdmParam);
    cdmParam->LfeDmxInclude = ass->pStatus->LFEDownmixInclude;
    cdmParam->channelConfigurationFrom = pAFI->channelConfigurationStream;
	/* 
     * CDM copies .1 from pAFI to pAFO even when CCR of pAFO is .0
     *      This is done to allow BM to downmix .1 based on speaker size etc
     * This creates a problem when .1 is not allocated in pAFO
     *      Problem applicable only to ASS
     * Here we check if CCR is .0 *and* LfeDmxInclude is not requested
     *      In this case, we fake input has having .0 irrespective of actual
     *  When CCR is .1, there is no issue
     *  When CCR is .0 and LfeDmxInclude is requested
     *          CDM takes .1 from pAFI and writes only to sat channels in pAFO
     *          So no issue
     *
     *  Original Comment from Maya:
     *     If LFE is not to be included in the downmix, the Sub part of the 
	 *     channelConfigurationFrom needs to be explicitely set to 0. Otherwise
	 *     CDM which only looks at channelConfigurationFrom will assume Sub is present
	 *     even in channelConfigurationRequest. For secondary stream, as Sub is absent
	 *     this will result in a bad write which may lead to crash. If LFE is to be
	 *     included in downmix, CDM behaves correctly.
	 */
	if(ccr.part.sub == PAF_CC_SUB_ZERO && !cdmParam->LfeDmxInclude)
		cdmParam->channelConfigurationFrom.part.sub = PAF_CC_SUB_ZERO;

    cdmParam->channelConfigurationRequest = ccr;
    cdmParam->sourceDual = 0;
    cdmParam->clev = pAudioFrame->fxns->dB2ToLinear(ass->pStatus->CntrMixLev);
    cdmParam->slev = pAudioFrame->fxns->dB2ToLinear(ass->pStatus->SurrMixLev);
    cdmParam->LFEDmxVolume =
            pAudioFrame->fxns->dB2ToLinear(ass->pStatus->LFEDownmixVolume);

    CPL_cdm_downmixConfig(NULL, cdmParam);
	//Settingup out of place flag.
	cdmParam->inplace = 0;

    CPL_cdm_downmixSetUp(NULL, pAFO, cdmParam);
    
    cdmParam->channelConfigurationFrom = pAFI->channelConfigurationStream;
    FromConfig = cdmParam->channelConfigurationFrom;
    ToConfig = cdmParam->channelConfigurationTo; 

    cdmParam->FromMask = pAudioFrame->fxns->channelMask (pAudioFrame, FromConfig);
    cdmParam->ToMask = pAudioFrame->fxns->channelMask (pAudioFrame, ToConfig);

	//copy the samsiz of input channels to AFO.
	//CPL_cdm_samsiz uses these values to in samsiz (of AFO) calculations
	memcpy(pAFO->data.samsiz,pAFI->data.samsiz,
					PAF_MAXNUMCHAN*sizeof(pAFO->data.samsiz[0]));
    CPL_cdm_samsiz(NULL, pAFO, cdmParam);
    CPL_cdm_downmixApply(NULL,
            pAFI->data.sample, 
            pAFO->data.sample, 
            cdmParam, pAFO->sampleCount);

    // Fixes for CDM issues with out-of-place processing
    // -------------------------------------------------
    //  CDM follows a table-driven downmix approach that implicitly assumes
    //  in-place processing.  This causes issues only in a known and small number of 
    //  cases involving upmix to a stereo pair such as shown below.  In these cases,
    //  the output should have identical samples in each channel of the pair, but CDM
    //  leaves off with the right-half holding the old left-half rather than the new one
    //  (http://cow-pa.hou.asp.ti.com/pa/support/bugtracker/view.php?id=1048#12913)
    //  So here, we fix things with a simple copy:
#       define COPY_CHANNEL(from, to) do  {                                                     \
                                        memcpy(pAFO->data.sample[to], pAFO->data.sample[from],  \
                                                pAFO->sampleCount*4);                             \
                                      } while (0)
                                      
   //  Case 1b: Identical Mono (L,R) from all channels  
    if (ToConfig.part.aux == PAF_CC_AUX_STEREO_MONO &&  ToConfig.part.sat == PAF_CC_SAT_STEREO)
        COPY_CHANNEL(PAF_LEFT, PAF_RGHT);
    //  Case 1c: Dual Mono 
    if (FromConfig.part.sat == PAF_CC_SAT_STEREO &&  ToConfig.part.sat == PAF_CC_SAT_STEREO && FromConfig.part.aux == PAF_CC_AUX_STEREO_DUAL &&  cdmParam->sourceDual !=0)

        COPY_CHANNEL(PAF_LEFT, PAF_RGHT);
//  Case 2: (Ls,Rs) from Ls
	if ((FromConfig.part.sat== PAF_CC_SAT_PHANTOM1) &&  (ToConfig.part.sat== PAF_CC_SAT_PHANTOM2))
		COPY_CHANNEL(PAF_LSUR, PAF_RSUR);
	if ((FromConfig.part.sat== PAF_CC_SAT_SURROUND1) &&  (ToConfig.part.sat== PAF_CC_SAT_PHANTOM2  ||  
                                                      ToConfig.part.sat == PAF_CC_SAT_SURROUND2))
		COPY_CHANNEL(PAF_LSUR, PAF_RSUR);
//  Case 3: (Lb,Rb) from Lb 
	if ((FromConfig.part.sat== PAF_CC_SAT_PHANTOM3) &&  (ToConfig.part.sat== PAF_CC_SAT_PHANTOM4))
		COPY_CHANNEL(PAF_LBAK, PAF_RBAK);

	if ((FromConfig.part.sat== PAF_CC_SAT_SURROUND3) &&  (ToConfig.part.sat== PAF_CC_SAT_PHANTOM4  ||  
                                                      ToConfig.part.sat == PAF_CC_SAT_SURROUND4))
		COPY_CHANNEL(PAF_LBAK, PAF_RBAK);
 

        
            
            
    pAFO->channelConfigurationStream = cdmParam->channelConfigurationTo;
	}
    ass->pStatus->channelConfigurationDownmix = pAFO->channelConfigurationStream;

#ifdef PAF_PROCESS_ASS
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASS);
#endif /* PAF_PROCESS_ASS */

    return 0;
}

/*
 *  ======== ASS_TII_reset ========
 *  TII's implementation of the information operation.
 */
Int 
ASS_TII_reset(IASS_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASS_TII_Obj *ass = (Void *)handle;

    PAF_AudioFrame * restrict pAFI = &pAudioFrame[ass->config.offsetI];
    PAF_AudioFrame * restrict pAFO = &pAudioFrame[ass->config.offsetO];

    PAF_ChannelConfiguration ccr;

    if (ass->pStatus->mode == 0)
        return 0;

    pAFO->sampleDecode = pAFI->sampleDecode;
    pAFO->sampleRate = pAFI->sampleRate;
    pAFO->sampleCount = pAFI->sampleCount;
    CPL_ivecCopy(pAFI->sampleProcess, pAFO->sampleProcess, PAF_SAMPLEPROCESS_N);
    ccr = ass->pStatus->channelConfigurationRequest;

    pAFO->channelConfigurationRequest = ccr;
    pAFO->channelConfigurationStream.legacy = PAF_CC_NONE;

    ass->pStatus->channelConfigurationDownmix = pAFO->channelConfigurationStream;

#ifdef PAF_PROCESS_ASS
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASS);
#endif /* PAF_PROCESS_ASS */

    return 0;
}

