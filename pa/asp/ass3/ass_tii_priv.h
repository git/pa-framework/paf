
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Audio Stream Split Algorithm internal interface declarations
//
//
//

/*
 *  Internal vendor specific (TII) interface header for ASS
 *  algorithm. Only the implementation source files include
 *  this header; this header is not shipped as part of the
 *  algorithm.
 *
 *  This header contains declarations that are specific to
 *  this implementation and which do not need to be exposed
 *  in order for an application to use the Audio Stream Split Algorithm.
 */
#ifndef ASS_TII_PRIV_
#define ASS_TII_PRIV_

#include <ialg.h>
#include <log.h>

#include "iass.h"
#include "com_tii_priv.h"

typedef struct ASS_TII_Obj {
    IALG_Obj alg;           /* MUST be first field of all XDAS algs */
	int mask;
    ASS_TII_Status *pStatus; /* public interface */
    ASS_TII_Config config;   /* private interface */
    CPL_CDM        cdmParam; /* scratch technically, but why bother? */
} ASS_TII_Obj;

#define ASS_TII_activate COM_TII_null

#define ASS_TII_deactivate COM_TII_null

extern Int ASS_TII_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);

#define ASS_TII_free COM_TII_free

#define ASS_TII_control COM_TII_control

extern Int ASS_TII_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
                
#define ASS_TII_moved COM_TII_moved
                
extern Int ASS_TII_apply(IASS_Handle, PAF_AudioFrame *);

extern Int ASS_TII_reset(IASS_Handle, PAF_AudioFrame *);

extern Int ASS_TII_mixStereo(PAF_AudioFrame *, PAF_AudioFrame *, Int);
extern Int ASS_TII_mixStereoLtRt(PAF_AudioFrame *, PAF_AudioFrame *, Int);
extern Int ASS_TII_mixStereoMono(PAF_AudioFrame *, PAF_AudioFrame *, Int);

#endif  /* ASS_TII_PRIV_ */
