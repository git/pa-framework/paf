
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Audio Stream Split Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the Audio Stream Split Algorithm.
 */
#ifndef IASS_
#define IASS_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"
#include "cpl.h"

/*
 *  ======== IASS_Obj ========
 *  Every implementation of IASS *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IASS_Obj {
    struct IASS_Fxns *fxns;    /* function list: standard, public, private */
} IASS_Obj;

/*
 *  ======== IASS_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IASS_Obj *IASS_Handle;

/*
 *  ======== IASS_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IASS_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 LFEDownmixVolume;
    XDAS_Int8 LFEDownmixInclude;    
    XDAS_Int8 unused;
    PAF_ChannelConfiguration channelConfigurationRequest;
    PAF_ChannelConfiguration channelConfigurationDownmix;
    XDAS_Int16 CntrMixLev;
    XDAS_Int16 SurrMixLev;
} IASS_Status;

/*
 *  ======== IASS_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IASS_Config {
    XDAS_Int8 offsetI;
    XDAS_Int8 offsetO;
    XDAS_Int8 unused[2];
} IASS_Config;

/*
 *  ======== IASS_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a ASS object.
 *
 *  Every implementation of IASS *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IASS_Params {
    Int size;
    const IASS_Status *pStatus;
    IASS_Config config;
} IASS_Params;

/*
 *  ======== IASS_PARAMS ========
 *  Default instance creation parameters (defined in iass.c)
 */
extern const IASS_Params IASS_PARAMS3;

/*
 *  ======== IASS_Fxns ========
 *  All implementation's of ASS must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is ASS_<vendor>_IASS, where
 *  <vendor> is the vendor name.
 */
typedef struct IASS_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IASS_Handle, PAF_AudioFrame *);
    Int         (*apply)(IASS_Handle, PAF_AudioFrame *);
    /* private */
} IASS_Fxns;

/*
 *  ======== IASS_Cmd ========
 *  The Cmd enumeration defines the control commands for the ASS
 *  control method.
 */
typedef enum IASS_Cmd {
    IASS_NULL                   = ICOM_NULL,
    IASS_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IASS_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IASS_GETSTATUS              = ICOM_GETSTATUS,
    IASS_SETSTATUS              = ICOM_SETSTATUS
} IASS_Cmd;

#endif  /* IASS_ */
