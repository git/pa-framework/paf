
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common IALG control operation (TII)
//
//
//

#include <std.h>
#include <alg.h>
#include <ialg.h>

#include "icom.h"
#include "com_tii_priv.h"

#include <string.h>											/* memcpy() declaration */

/*
 *  ======== COM_TII_null ========
 *  The common null function.
 */
Void COM_TII_null ()
{
}

/*
 *  ======== COM_TII_free ========
 */
Int COM_TII_free (IALG_Handle handle, IALG_MemRec memTab[])
{
	return (*handle->fxns->algAlloc) (NULL, NULL, memTab);
}

/*
 *  ======== COM_TII_control ========
 *
 *  This function provides common control operations for ASP Algorithms
 *  that do not utilize common memory.
 */
Int COM_TII_control (IALG_Handle handle, IALG_Cmd cmd, IALG_Status * pStatus)
{
	COM_TII_Obj *obj = (Void *) handle;

	switch (cmd)
	{
		case ICOM_NULL:
			return ((Int) 0);
		case ICOM_GETSTATUSADDRESS1:
			*(Void **) pStatus = (Void *) obj->pStatus;
			return ((Int) 0);
		case ICOM_GETSTATUSADDRESS2:
			return ((Int) 1);
		case ICOM_GETSTATUS:
			memcpy ((Void *) pStatus, (Void *) obj->pStatus, (Int) obj->pStatus->size);
			return ((Int) 0);
		case ICOM_SETSTATUS:
			memcpy ((Void *) obj->pStatus, (Void *) pStatus, (Int) obj->pStatus->size);
			return ((Int) 0);
		default:
			return ((Int) -1);
	}
}

Void COM_TII_deactivate (IALG_Handle handle)
{
	COM_TII_deactivateCommon_ (handle, ((COM_TII_Obj *) handle)->pCommon);
}

Int COM_TII_activateInternal (IALG_Handle handle)
{
	return (COM_TII_activateCommon_ (handle, ((COM_TII_Obj *) handle)->pCommon));
}

// ==== Common memory functions called through pafhjt ====

Int COM_TII_snatchCommon_ (IALG_Handle handle, PAF_IALG_Common * common)
{
	Int flag = (Int) handle;

	// Forced allocation of a common memory area, used by decoders etc
	common->size = sizeof (*common);
	common->flag = flag;

	return 0;
}

Int COM_TII_activateCommon_ (IALG_Handle handle, PAF_IALG_Common * common)
{
	Int flag = (Int) handle;

	if (!common->size)
	{
		// Common memory section unallocated: allocate to self
		common->size = sizeof (*common);
		common->flag = flag;
	}
	else if (common->size == sizeof (*common) && common->flag == flag)
	{
		// Common memory section allocated to self: continue
		;
	}
	else
	{
		// Common memory section allocated to other: error
		return 1;
	}

	return 0;
}

Void COM_TII_deactivateCommon_ (IALG_Handle handle, PAF_IALG_Common * common)
{
	Int flag = (Int) handle;

	if (common->size == sizeof (*common) && common->flag == flag)
	{
		// Common memory section allocated to self: deallocate.
		common->size = 0;
		common->flag = 0;
	}
}

