/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  DEL Module implementation - MDS implementation of a Speaker Location Delay algoritm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <idel.h>
#include <del_mds.h>
#include <del_mds_priv.h>

#include "cpl.h"

#include <paftyp.h>
#include "delerr.h"


static Int resetDelayState(IDEL_Handle handle, PAF_AudioFrame *pAudioFrame);
/*
 *  ======== DEL_MDS_apply ========
 *  MDS's implementation of the apply operation.
 *
 */

Int 
DEL_MDS_apply(IDEL_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    DEL_MDS_Obj * restrict del = (Void *)handle;

    PAF_ChannelConfiguration stream
        = pAudioFrame->channelConfigurationStream;

    PAF_ChannelMask streamMask
        = pAudioFrame->fxns->channelMask (pAudioFrame, stream);

    PAF_DelayState * restrict state = del->pConfig->state;

    Int unit = del->pStatus->unit;

    Int errno = 0;
    Int errch;

    Int iC, iS;

    // Check requested mode.

    if((del->pStatus->mode==0)&& (del->pStatus->resetFlag==0))
    {
		/* Set the reset flag so that delay state can be properly reset when this instance is activated without decoder reset */
		del->pStatus->resetFlag = 1;
    }

    if (del->pStatus->mode == 0) {
        /* Deactivate the algorithm so that Common Memory is freed. */
        handle->fxns->ialg.algDeactivate((IALG_Handle)handle);
        return errno;
    }
    else {
        /* Attempt to activate the algorithm. If return is non-zero, algorithm is not 
           active and algorithm cannot operate. */
        if (((Int (*)(IALG_Handle handle) )handle->fxns->ialg.algActivate)((IALG_Handle)handle) != 0) {
            return 0;
        }
    }

	if((del->pStatus->mode ==1)&&(del->pStatus->resetFlag==1)){
        // Reset delay state.
        errno = resetDelayState( handle, pAudioFrame );
		del->pStatus->resetFlag = 0;
	}

    // Compute delay using time- or location-based method:

    if (! (unit & 0x80)) {

        // Time-based method.

        volatile XDAS_UInt16 * restrict delay = del->pStatus->delay;

        for (iC=0, iS=0;
            iC < del->pStatus->numc && iS < del->pStatus->nums; 
            iC++) {
            if (del->pConfig->mask.bits & (1 << iC))
            {
                state[iS++].delay = delay[iC] + del->pStatus->masterDelay;
            }    
        }
    }
    else {

        // Location-based method.

        volatile XDAS_UInt16 * restrict delay = del->pStatus->delay;

        Uns locmax;

        locmax = delay[0];
        for (iC=1; iC < del->pStatus->numc; iC++) 
            if (locmax < delay[iC])
                locmax = delay[iC];

        for (iC=0, iS=0;
            iC < del->pStatus->numc && iS < del->pStatus->nums; 
            iC++) {
            if (del->pConfig->mask.bits & (1 << iC))
                state[iS++].delay = locmax - delay[iC];
        }
    }

    unit &= 0x7f;

    // Perform delay for channels in the configuration; reset delay for others.

    for (iC=0, iS=0; iC < del->pStatus->numc && iS < del->pStatus->nums; iC++) {
        if (del->pConfig->mask.bits & (1 << iC)) {
#if (PAF_DEVICE&0xFF000000)==0xD7000000
		if(!del->pConfig->useDATCopy)
            errch = CPL_delay_ (pAudioFrame, &state[iS++], 
                streamMask & (1 << iC), iC, unit);
		else
            errch = CPL_delayDAT_ (pAudioFrame, &state[iS++],
                            (PAF_AudioData *)(del->pScrach->delScrachPtr), 
                            streamMask & (1 << iC), iC, unit);
#else /* PAF_DEVICE */
		if(!del->pConfig->useDATCopy)
            errch = CPL_delay (pAudioFrame, &state[iS++], 
                streamMask & (1 << iC), iC, unit);
		else
            errch = CPL_delayDAT_ (pAudioFrame, &state[iS++],
                            (PAF_AudioData *)(del->pScrach->delScrachPtr), 
                            streamMask & (1 << iC), iC, unit);
#endif /* PAF_DEVICE */

            if (errch && ! errno)
                errno = errch + iC;
        }
    }

    return errno;
}

/*
 *  ======== DEL_MDS_reset ========
 *  MDS's implementation of the information operation.
 */
Int 
DEL_MDS_reset(IDEL_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    DEL_MDS_Obj * restrict del = (Void *)handle;

    PAF_DelayState * restrict state = del->pConfig->state;

    Int errno = 0;
    Int errch;

    Int iC, iS;

    del->pStatus->resetFlag = 1;

    // Check requested mode.
    if (del->pStatus->mode == 0) {
        /* Deactivate the algorithm so that Common Memory is freed. */
        handle->fxns->ialg.algDeactivate((IALG_Handle)handle);
        return errno;
    }
    else {
        /* Attempt to activate the algorithm. If return is non-zero, algorithm is not 
           active and algorithm cannot operate. */
        if (((Int (*)(IALG_Handle handle) )handle->fxns->ialg.algActivate)((IALG_Handle)handle) != 0) {
            return 0;
        }
    }

    // Reset delay state.
    errno = resetDelayState( handle, pAudioFrame );

#ifdef PAF_PROCESS_DEL
    if (! errno) 
        PAF_PROCESS_SET (pAudioFrame->sampleProcess, DEL);
#endif /* PAF_PROCESS_DEL */

    return errno;
}



static Int
resetDelayState(IDEL_Handle handle, PAF_AudioFrame *pAudioFrame){

    DEL_MDS_Obj * restrict del = (Void *)handle;

    PAF_DelayState * restrict state = del->pConfig->state;

    Int errno = 0;
    Int errch;

    Int iC, iS;

    for (iC=0, iS=0; iC < del->pStatus->numc && iS < del->pStatus->nums; iC++) {
        if (del->pConfig->mask.bits & (1 << iC)) {
#if (PAF_DEVICE&0xFF000000)==0xD7000000
			if(!del->pConfig->useDATCopy)
                errch = CPL_delay_ (pAudioFrame, &state[iS++], 
                    0, iC, 0);
			else
                errch = CPL_delayDAT_ (pAudioFrame, &state[iS++],
                    (PAF_AudioData *)(del->pScrach->delScrachPtr), 0, iC, 0);
#else /* PAF_DEVICE */
			if(!del->pConfig->useDATCopy)
                errch = CPL_delay (pAudioFrame, &state[iS++], 
                    0, iC, 0);
			else
                errch = CPL_delayDAT_ (pAudioFrame, &state[iS++],
                    (PAF_AudioData *)(del->pScrach->delScrachPtr), 0, iC, 0);
#endif /* PAF_DEVICE */


            if (errch && ! errno)
                errno = errch + iC;
        }
    }

	return errno;

}

