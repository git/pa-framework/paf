/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IDEL default instance creation parameters
 */
#include <std.h>

#include <idel.h>

#include <paftyp.h>

/*
 *  ======== IDEL_PARAMS_BASIC ========
 *  This static initialization defines the default parameters used to
 *  create instances of DEL objects - 5-channel @ 48 kHz.
 */

const IDEL_Status IDEL_PARAMS_STATUS_BASIC = {
    sizeof(IDEL_Status), /* size */
    1, /* mode */
    1, /* unit */
    PAF_MAXNUMCHAN, 5, /* numc, nums */
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,/* delay */
	0, /* masterDelay */
};

typedef struct IDEL_Config5 {
    Int size;
	XDAS_UInt16 sizeofScrach;
	XDAS_UInt8 useDATCopy;
	XDAS_UInt8 delCommonMemNumber;
	XDAS_UInt16 unused;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[5];
} IDEL_Config5;

const IDEL_Config5 IDEL_PARAMS_CONFIG_BASIC = {
    sizeof(IDEL_Config5), /* "size" of the structure */
    
    /* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of dMAX resource to transfer between external and internal RAM.
	 A value of 0 for "useDATCopy" implies that delay buffer is placed in the IRAM itself. */
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	/* unused */
	0,
    (1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK),
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
};

const IDEL_Params IDEL_PARAMS_BASIC = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_BASIC, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_BASIC, /* pConfig */
};

/*
 *  ======== IDEL_PARAMS_DOLBY ========
 *  This static initialization defines the default parameters used to
 *  create instances of DEL objects - 5-channel @ 96 kHz.
 */

const IDEL_Config5 IDEL_PARAMS_CONFIG_DOLBY = {
    sizeof(IDEL_Config5), /* size */
    /* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of dMAX resource to transfer between external and internal RAM.
	 A value of 0 for "useDATCopy" implies that delay buffer is placed in the IRAM itself. */
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	
	/* unused */
	0,
    (1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK),
    0, 0,  5*96, 0, NULL, 0, 
    0, 0, 15*96, 0, NULL, 0, 
    0, 0, 15*96, 0, NULL, 0, 
    0, 0, 15*96, 0, NULL, 0, 
    0, 0, 15*96, 0, NULL, 0, 
};

const IDEL_Params IDEL_PARAMS_DOLBY = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_BASIC, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_DOLBY, /* pConfig */
};
#ifdef __TI_EABI__
asm(" .global IDEL_PARAMS");
asm("IDEL_PARAMS .set IDEL_PARAMS_DOLBY");
#else
asm(" .global _IDEL_PARAMS");
asm("_IDEL_PARAMS .set _IDEL_PARAMS_DOLBY");
#endif
/*
 *  ======== IDEL_PARAMS_THX ========
 *  This static initialization defines the parameters used to create 
 *  instances of DEL objects - 8-channel @ 96 kHz.
 */

const IDEL_Status IDEL_PARAMS_STATUS_THX = {
    sizeof(IDEL_Status), /* size */
    1, /* mode */
    1, /* unit */
    PAF_MAXNUMCHAN, 8, /* numc, nums */
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */
	0, /* masterDelay */
};

typedef struct IDEL_Config8 {
    Int size;
	XDAS_UInt16 sizeofScrach;
	XDAS_UInt8 useDATCopy;
	XDAS_UInt8 delCommonMemNumber;
	XDAS_UInt16 unused;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[8];
} IDEL_Config8;

const IDEL_Config8 IDEL_PARAMS_CONFIG_THX = {
    sizeof(IDEL_Config8), /* size */
    /* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of dMAX resource to transfer between external and internal RAM.
	 A value of 0 for "useDATCopy" implies that delay buffer is placed in the IRAM itself. */
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	
	/* unused */
	0,
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)|
    (1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW),
    0, 0, 10*96, 0, NULL, 0, 
    0, 0, 10*96, 0, NULL, 0, 
    0, 0, 10*96, 0, NULL, 0, 
    0, 0, 20*96, 0, NULL, 0, 
    0, 0, 20*96, 0, NULL, 0, 
    0, 0, 20*96, 0, NULL, 0, 
    0, 0, 20*96, 0, NULL, 0, 
    0, 0, 10*96, 0, NULL, 0, 
};
const IDEL_Params IDEL_PARAMS_THX = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_THX, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_THX, /* pConfig */
};

