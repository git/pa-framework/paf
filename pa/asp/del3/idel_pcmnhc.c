/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IDEL default instance creation parameters
 */
#include <std.h>

#include <idel.h>

#include <paftyp.h>

#define DELAY_INMS  (50)

#define DEFAULT_DELAY   DELAY_INMS*192


const IDEL_Status IDEL_PARAMS_STATUS_PCMNHC = {
    sizeof(IDEL_Status), /* size */
    1, /* mode */
    1, /* unit */
    PAF_MAXNUMCHAN, 2, /* numc, nums */
    DELAY_INMS, DELAY_INMS, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* delay */
    0, /* masterDelay */
};

typedef struct IDEL_Config_PCMNHC {
    Int size;
    XDAS_UInt16 sizeofScrach;
    XDAS_UInt8 useDATCopy;
    XDAS_UInt8 delCommonMemNumber;
    XDAS_UInt16 pcmOnly;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[2];
} IDEL_Config_PCMNHC;

const IDEL_Config_PCMNHC IDEL_PARAMS_CONFIG_PCMNHC = {
    sizeof(IDEL_Config_PCMNHC), /* size */
    /* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
    256*4,

    /* "useDATCopy" when set to 1, will make use of dMAX resource to transfer between external and internal RAM.
     A value of 0 for "useDATCopy" implies that delay buffer is placed in the IRAM itself. */
    1,

    /* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
    5,

    /* unused */
    1,      /* delay only for PCM input */
    (1<<PAF_LEFT)|(1<<PAF_RGHT),
    0, 0, DEFAULT_DELAY, 0, NULL, 0,
    0, 0, DEFAULT_DELAY, 0, NULL, 0,
};
const IDEL_Params IDEL_PARAMS_PCMNHC = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_PCMNHC, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_PCMNHC, /* pConfig */
};


