
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Downmix algorithm
// IALG implementation
//
//
//

/*
 *  DM Module IALG implementation - TII's implementation of the
 *  IALG interface for the Downmix algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include <idm.h>
#include <dm_tii.h>
#include <dm_tii_priv.h>

/*
 *  ======== DM_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== DM_TII_alloc ========
 */
Int DM_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IDM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IDM_PARAMS;  /* set default parameters */
    }

    /* Request memory for DM object */
    memTab[0].size = (sizeof(DM_TII_Obj)+3)/4*4 + (sizeof(DM_TII_Status)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    memTab[1].size = (sizeof(CPL_CDM)+3)/4*4;
    memTab[1].alignment = 4;
    memTab[1].space = IALG_SARAM;
    memTab[1].attrs = IALG_SCRATCH;

    return (2);
}

/*
 *  ======== DM_TII_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== DM_TII_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== DM_TII_initObj ========
 */
Int DM_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    DM_TII_Obj *dm = (Void *)handle;
    const IDM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IDM_PARAMS;  /* set default parameters */
    }

    dm->pStatus = (DM_TII_Status *)((char *)dm + (sizeof(DM_TII_Obj)+3)/4*4);

    *dm->pStatus = *params->pStatus;
    dm->config = params->config;
    dm->pCDMParam = memTab[1].base;

    return (IALG_EOK);
}


/*
 *  ======== DM_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== DM_TII_moved ========
 */
  /* COM_TIH_moved */
