
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Downmix algorithm
// functionality implementation
//
//
//

/*
 *  DM Module implementation - TII implementation of an
 *  Downmix algorithm.
 */

#include <std.h>

#include <idm.h>
#include <dm_tii.h>
#include <dm_tii_priv.h>
#include <dmerr.h>

#include  <std.h>
#include <xdas.h>

#include "paftyp.h"

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== DM_TII_apply ========
 *  TII's implementation of the apply operation.
 *
 *  This function is called after the DM_TII_reset function, below, and
 *  deals with control data AND sample data.
 *
 */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

Int 
DM_TII_apply(IDM_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    DM_TII_Obj * restrict dm = (Void *) handle;
    PAF_AudioData **sample;
    CPL_CDM *CDMParam;
    PAF_ChannelConfiguration request;
    PAF_ChannelConfiguration stream;

    if(!dm->pStatus->mode) return 0;

    // The following test exists to ensure that if the DM Channel Config
    // request is UNKNOWN, then DM won't run. This works around a "feature"
    // in the cdm_downmixConfig() function whereby if the aux fields for an
    // incoming stream contain anything other than UNKNOWN and the downmix
    // is not stereo, the aux fields are overriden with UNKNOWN. This 
    // override causes the DM test for processing to pass, since the override
    // will cause the From and To to be different with e.g. LtRt input.

    if (dm->pStatus->request.legacy == PAF_CC_UNKNOWN) return 0;

    dm      = (Void *)handle;
    sample   = pAudioFrame->data.sample;
    CDMParam = (CPL_CDM *)dm->pCDMParam;
    request  = dm->pStatus->request;
    stream   = pAudioFrame->channelConfigurationStream;
		
    CDMParam->LfeDmxInclude = dm->pStatus->LFEDownmixInclude;
    CDMParam->channelConfigurationFrom = stream;
    CDMParam->channelConfigurationRequest = request;
    CDMParam->sourceDual = 0; // pAudioFrame->sourceDual;
    CDMParam->clev = pAudioFrame->fxns->dB2ToLinear(dm->pStatus->CntrMixLev);
    CDMParam->slev = pAudioFrame->fxns->dB2ToLinear(dm->pStatus->SurrMixLev);
    CDMParam->LFEDmxVolume = pAudioFrame->fxns->dB2ToLinear(dm->pStatus->LFEDownmixVolume);

    CPL_CALL(cdm_downmixConfig)(NULL,CDMParam);

    if( CDMParam->channelConfigurationFrom.legacy != 
        CDMParam->channelConfigurationTo.legacy)
    {
            
        CPL_CALL(cdm_downmixSetUp)(NULL,pAudioFrame,CDMParam);
        CPL_CALL(cdm_samsiz)(NULL,pAudioFrame,CDMParam);
        CPL_CALL(cdm_downmixApply)(NULL,sample,sample, \
                        CDMParam,pAudioFrame->sampleCount);
        
#ifdef PAF_PROCESS_DMX
        PAF_PROCESS_SET (pAudioFrame->sampleProcess, DMX);
#endif /* PAF_PROCESS_DMX */
    
    }
	
	pAudioFrame->channelConfigurationStream =  CDMParam->channelConfigurationTo;   

    return 0;
}

/*
 *  ======== DM_TII_reset ========
 *  TII's implementation of the reset function, which is like an "information" 
 *  operation.  This is always called by the framework before calling the DM_TII_apply
 *  function, and only deals with control data, not sample data.
 */

Int 
DM_TII_reset(IDM_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    // DM_TII_Obj * restrict dm = (Void *)handle;

    return 0;
}
