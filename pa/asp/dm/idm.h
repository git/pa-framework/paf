
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Downmix algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the Downmix algorithm.
 */
#ifndef IDM_
#define IDM_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"
#include "cpl.h"

/*
 *  ======== IDM_Obj ========
 *  Every implementation of IDM *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IDM_Obj {
    struct IDM_Fxns *fxns;    /* function list: standard, public, private */
} IDM_Obj;

/*
 *  ======== IDM_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IDM_Obj *IDM_Handle;

/*
 *  ======== IDM_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in iDM.c.
 */
typedef volatile struct IDM_Status {

    Int size;         /* This value must always be here, and must be set to the 
                         total size of this structure in 8-bit bytes, as the 
                         sizeof() operator would do. */
    XDAS_Int8 mode;   /* This is the 8-bit DM Mode Control Register.  All 
                         Algorithms must have a mode control register.  */
    XDAS_Int8 LFEDownmixVolume;
    XDAS_Int8 LFEDownmixInclude;    
    XDAS_Int8 unused;
    XDAS_Int16 CntrMixLev;
    XDAS_Int16 SurrMixLev;
    PAF_ChannelConfiguration request;
} IDM_Status;

/*
 *  ======== IDM_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IDM_Config {
    Int dummy;
} IDM_Config;

/*
 *  ======== IDM_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a DM object.
 *
 *  Every implementation of IDM *must* declare this structure as
 *  the first member of the implementation's parameter structure.  This structure is actually
 *  instantiated and initialized in iDM.c.
 */
typedef struct IDM_Params {
    Int size;
    const IDM_Status *pStatus;
    IDM_Config config;
} IDM_Params;

/*
 *  ======== IDM_PARAMS ========
 *  Default instance creation parameters (defined in iDM.c)
 */
extern const IDM_Params IDM_PARAMS;

/*
 *  ======== IDM_Fxns ========
 *  All implementation's of DM must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is DM_<vendor>_IDM, where
 *  <vendor> is the vendor name.
 */
typedef struct IDM_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IDM_Handle, PAF_AudioFrame *);
    Int         (*apply)(IDM_Handle, PAF_AudioFrame *);
    /* private */
} IDM_Fxns;

/*
 *  ======== IDM_Cmd ========
 *  The Cmd enumeration defines the control commands for the DM
 *  control method.
 */
typedef enum IDM_Cmd {
    IDM_NULL                    = ICOM_NULL,
    IDM_GETSTATUSADDRESS1       = ICOM_GETSTATUSADDRESS1,
    IDM_GETSTATUSADDRESS2       = ICOM_GETSTATUSADDRESS2,
    IDM_GETSTATUS               = ICOM_GETSTATUS,
    IDM_SETSTATUS               = ICOM_SETSTATUS
} IDM_Cmd;

#endif  /* IDM_ */
