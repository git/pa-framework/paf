
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the FIL-ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the FEA
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef FEA_
#define FEA_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <ifea.h>

#include "paftyp.h"

/*
 *  ======== FEA_Handle ========
 *  FIL-ASP Example Demonstration algorithm instance handle
 */
typedef struct IFEA_Obj *FEA_Handle;

/*
 *  ======== FEA_Params ========
 *  FIL-ASP Example Demonstration algorithm instance creation parameters
 */
typedef struct IFEA_Params FEA_Params;

/*
 *  ======== FEA_PARAMS ========
 *  Default instance parameters
 */
#define FEA_PARAMS IFEA_PARAMS

/*
 *  ======== FEA_Status ========
 *  Status structure for getting FEA instance attributes
 */
typedef volatile struct IFEA_Status FEA_Status;

/*
 *  ======== FEA_Cmd ========
 *  This typedef defines the control commands FEA objects
 */
typedef IFEA_Cmd   FEA_Cmd;

/*
 * ===== control method commands =====
 */
#define FEA_NULL IFEA_NULL
#define FEA_GETSTATUSADDRESS1 IFEA_GETSTATUSADDRESS1
#define FEA_GETSTATUSADDRESS2 IFEA_GETSTATUSADDRESS2
#define FEA_GETSTATUS IFEA_GETSTATUS
#define FEA_SETSTATUS IFEA_SETSTATUS

/*
 *  ======== FEA_create ========
 *  Create an instance of a FEA object.
 */
static inline FEA_Handle FEA_create(const IFEA_Fxns *fxns, const FEA_Params *prms)
{
    return ((FEA_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== FEA_delete ========
 *  Delete a FEA instance object
 */
static inline Void FEA_delete(FEA_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== FEA_apply ========
 */
extern Int FEA_apply(FEA_Handle, PAF_AudioFrame *);

/*
 *  ======== FEA_reset ========
 */
extern Int FEA_reset(FEA_Handle, PAF_AudioFrame *);

/*
 *  ======== FEA_exit ========
 *  Module finalization
 */
extern Void FEA_exit(Void);

/*
 *  ======== FEA_init ========
 *  Module initialization
 */
extern Void FEA_init(Void);

#endif  /* FEA_ */
