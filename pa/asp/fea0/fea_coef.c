
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* Headers */
#include "std.h"
#include "fil.h"
#include "fil_tii.h"

#define FIL_FIR_TYPE_FEA 0x200C0801 /* SP, 64 tap FIR, unicoeff, ch=1. */ 
#define FIL_CH_SELECT_FEA 0x7 /* Ch - L, R, Cnt */

Float gFIR_Coeff_FEA[65] = {
-0.000365487721819,
-0.000232862645709,
-0.000156207486155,
0.000097591446156 ,
0.000586426810956 ,
0.001343216534866 ,
0.002357704225987 ,
0.003561469341011 ,
0.004819817298641 ,
0.005934726566569 ,
0.006661691185914 ,
0.006740255029476 ,
0.005936933769284 ,
0.004094613541583 ,
0.001181510311074 ,
-0.002668203604452,
-0.007131822816823,
-0.011699442207520,
-0.015706253488364,
-0.018391837619357,
-0.018984418848016,
-0.016801943119425,
-0.011355353281019,
-0.002438505889964,
0.009806476616893 ,
0.024860596733660 ,
0.041850585994021 ,
0.059610320435603 ,
0.076790762169061 ,
0.091995105945524 ,
0.103929254366304 ,
0.111545720465277 ,
0.114163267894499 ,
0.111545720465277 ,
0.103929254366304 ,
0.091995105945524 ,
0.076790762169061 ,
0.059610320435603 ,
0.041850585994021 ,
0.024860596733660 ,
0.009806476616893 ,
-0.002438505889964,
-0.011355353281019,
-0.016801943119425,
-0.018984418848016,
-0.018391837619357,
-0.015706253488364,
-0.011699442207520,
-0.007131822816823,
-0.002668203604452,
0.001181510311074 ,
0.004094613541583 ,
0.005936933769284 ,
0.006740255029476 ,
0.006661691185914 ,
0.005934726566569 ,
0.004819817298641 ,
0.003561469341011 ,
0.002357704225987 ,
0.001343216534866 ,
0.000586426810956 ,
0.000097591446156 ,
-0.000156207486155,
-0.000232862645709,
-0.000365487721819
};

/* FEA's FIL-coefficient struct, similar to PAF_FilCoef struct */
typedef struct FilCoef_FEA{
    LgUns type;       /* Filter type field */
    LgUns sampRate;   /* Sample rate bit field */
    Float *coef;      /* Coefficient ptr */
} FilCoef_FEA;

/* FEA's FIR initial coefficient structure */
FilCoef_FEA FilCoefFIR_FEA = {
    FIL_FIR_TYPE_FEA, /* Filter type field */
    0x0,              /* Sample rate bit field */                    
    gFIR_Coeff_FEA,   /* Coefficient ptr */
};

/* Status */
IFIL_Status IFIL_Status_FEA = {
    sizeof(IFIL_Status), /* size */
    FIL_CH_SELECT_FEA,   /* mode, ch - L,R,C */
    0x1,                 /* use, default is 'Enable' */                 
    FIL_CH_SELECT_FEA,   /* mask select, ch - L,R,C */  
    0x0                  /* mask status */ 
};

/* Config */
IFIL_Config IFIL_Config_FEA = {
    sizeof(IFIL_Config),           /* size */
    (PAF_FilCoef *)&FilCoefFIR_FEA /* FIR filter Coefficients */
};

/* Param */
const IFIL_Params IFIL_PARAMS_FEA = {
    sizeof(IFIL_Params), /* size */ 
    1,                     /* use */
    &IFIL_Status_FEA,
    &IFIL_Config_FEA,
};

/* EOF */









