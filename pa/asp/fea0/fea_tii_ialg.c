
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  FEA Module IALG implementation - TII's implementation of the
 *  IALG interface for the FIL-ASP Example Demonstration algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAIS.
 */ 

/* Standard headers */
#include <std.h>
#include <ialg.h>

/* FEA headers */
#include <ifea.h>
#include <fea_tii.h>
#include <fea_tii_priv.h>

/*
 *  ======== FEA_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== FEA_TII_alloc ========
 */
Int FEA_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IFEA_Params *params = (Void *)algParams;
    Int n;
    
    if (params == NULL) {
        params = &IFEA_PARAMS;  /* set default parameters */
    }

    /* Request memory for FEA object */
    memTab[0].size = (sizeof(FEA_TII_Obj) + 3) /4*4 + 
                     (sizeof(FEA_TII_Status) + 3 ) /4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;
    
    /* Call FIL alg alloc, with the next available memTab ptr */
    n = FEA_TII_IFEA.filFxns->ialg.algAlloc((const IALG_Params *)params->pFilParams,
                                                                     pf, &memTab[1]);
    
    return (1 + n); /* Return the number of memory requests */
}

/*
 *  ======== FEA_TII_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== FEA_TII_free ========
 */
Int FEA_TII_free(IALG_Handle handle, IALG_MemRec memTab[])
{
    FEA_TII_Obj *fea = (Void *)handle;
    
    memTab[1].base = fea->firHandle;
    
    /* Call alloc function to set the remaining parameters of memTabs */
    return (*handle->fxns->algAlloc)(NULL, NULL, memTab);
}

/*
 *  ======== FEA_TII_initObj ========
 */
Int FEA_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    FEA_TII_Obj *fea = (Void *)handle;
    const IFEA_Params *params = (Void *)algParams;
    FIL_Handle fil;    

    if (params == NULL) 
    {
        params = &IFEA_PARAMS;  /* set default parameters */
    }

    /* Get the status and config. pointers */
    fea->pStatus = (FEA_TII_Status *)((char *)fea + (sizeof(FEA_TII_Obj)+3)/4*4);
    *fea->pStatus = *params->pStatus;    
    fea->config = *params->config;
    
    fil = (FIL_Handle)memTab[1].base;
    
    /* Get the FIR-FIL handle into the status structure */
    fea->firHandle = fil;                                         
    
    /* Initialize the fxns pointer */        
    fil->fxns = (IFIL_Fxns *)((IFEA_Fxns *)(fea->alg.fxns))->filFxns;
    
    /* Call FIL algInit function */
    fil->fxns->ialg.algInit((IALG_Handle)fil, &memTab[1], p, 
                            ( const IALG_Params *)params->pFilParams);
                                     
    return (IALG_EOK);
}

/*
 *  ======== FEA_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== FEA_TII_moved ========
 */
  /* COM_TIH_moved */

Int FEA_TII_numAlloc()
{
    return(1 + FEA_TII_IFEA.filFxns->ialg.algNumAlloc());
}
