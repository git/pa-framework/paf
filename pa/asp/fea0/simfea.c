
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <std.h>
#include <stdio.h>
#include <stdlib.h>

#include <asp0.h>
#include <paftyp.h>
#include <paftyp_a.h>
#include <stdasp.h>

#include <simasp.h>

#include <fea.h>
#include <ifea.h>
#include <fea_tii.h>
#include <fea_tii_priv.h>

#ifndef OUTDATAFILE
#define OUTDATAFILE "simasp_out.dat"
#endif

#ifndef INDATAFILE
#define INDATAFILE "simasp_in.dat"
#endif

#define NUMCHAN 2

const IFEA_Status SIM_IFEA_PARAMS_STATUS = {
    sizeof(IFEA_Status), /* Size of this structure.  */
    1,                   /* FEA Mode Control Register.  1=enabled. 0=disabled.  */
    1,                   /* Unused. Set the reset flag */
};
const IFEA_Config SIM_IFEA_PARAMS_CONFIG = {
    sizeof(IFEA_Config), /* size */
};
const IFEA_Params SIM_IFEA_PARAMS = {
    sizeof(IFEA_Params),         /* size */
    &SIM_IFEA_PARAMS_STATUS,     /* pStatus */
    &SIM_IFEA_PARAMS_CONFIG,     /* config */
    &IFIL_PARAMS_FEA,    
};

// -----------------------------------------------------------------------------

int simASP (void)
{
    ALG_Handle alg;
    FILE *pOutFile;
    int errno;
    int i;
    

    // allocate memory for audio frame
    errno = initAudioFrame(NUMCHAN);
    if (errno) {
        fprintf( stderr, "initAudioFrame failed ... Aborting\n" );
        exit(EXIT_FAILURE);
    }

    //use input  = impulse for now
    audioFrame[0].data.sample[PAF_LEFT][0] = 1.0;
    audioFrame[0].data.sample[PAF_RGHT][0] = 1.0;
    for (i=1; i < FRAMELENGTH; i++) {
        audioFrame[0].data.sample[PAF_LEFT][i] = 0;
        audioFrame[0].data.sample[PAF_RGHT][i] = 0;
    }
    
    // apply ASP
    alg = PAF_ALG_create(&FEA_TII_IALG, NULL, (IALG_Params *) &SIM_IFEA_PARAMS ,NULL, NULL);
    ((ASP_Handle) alg)->fxns->apply( (ASP_Handle) alg, &audioFrame[0]);
    
    //output audio frame to file
    pOutFile = fopen( OUTDATAFILE, "w+");
    for (i=0; i < FRAMELENGTH; i++)
        fprintf(pOutFile, "% 0.7f\t% 0.7f\t% 0.7f\n",
                audioFrame[0].data.sample[PAF_LEFT][i],
                audioFrame[0].data.sample[PAF_RGHT][i],
                audioFrame[0].data.sample[PAF_CNTR][i] );
    fclose( pOutFile );

    // indicate done
    fprintf( stdout, "Finished\n");

    return errno;
} //simASP

// -----------------------------------------------------------------------------
