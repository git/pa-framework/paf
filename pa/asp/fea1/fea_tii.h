
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Vendor specific (TII) interface header for
 *  FIL-ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular FEA implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef FEA_TII_
#define FEA_TII_

#include <ialg.h>

#include <ifea.h>
#include <icom.h>

/*
 *  ======== FEA_TII_exit ========
 *  Required module finalization function
 */
#define FEA_TII_exit COM_TII_exit

/*
 *  ======== FEA_TII_init ========
 *  Required module initialization function
 */
#define FEA_TII_init COM_TII_init

/*
 *  ======== FEA_TII_IALG ========
 *  TII's implementation of FEA's IALG interface
 */
extern IALG_Fxns FEA_TII_IALG; 

/*
 *  ======== FEA_TII_IFEA ========
 *  TII's implementation of FEA's IFEA interface
 */
extern const IFEA_Fxns FEA_TII_IFEA; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TII's
 *  FIL-ASP Example Demonstration algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== FEA_TII_Handle ========
 */
typedef struct FEA_TII_Obj *FEA_TII_Handle;

/*
 *  ======== FEA_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IFEA.
 */
typedef IFEA_Params FEA_TII_Params;

/*
 *  ======== FEA_TII_Status ========
 *  We don't add any new status to the standard one defined by IFEA.
 */
typedef IFEA_Status FEA_TII_Status;

/*
 *  ======== FEA_TII_Config ========
 *  We don't add any new config to the standard one defined by IFEA.
 */
typedef IFEA_Config FEA_TII_Config;

/*
 *  ======== FEA_TII_PARAMS ========
 *  Define our default parameters.
 */
#define FEA_TII_PARAMS   IFEA_PARAMS

/*
 *  ======== FEA_TII_create ========
 *  Create a FEA_TII instance object.
 */
extern FEA_TII_Handle FEA_TII_create(const FEA_TII_Params *params);

/*
 *  ======== FEA_TII_delete ========
 *  Delete a FEA_TII instance object.
 */
#define FEA_TII_delete COM_TII_delete

#endif  /* FEA_TII_ */
