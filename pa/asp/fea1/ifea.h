
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the FIL-ASP Example Demonstration algorithm.
 */
#ifndef IFEA_
#define IFEA_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"
#include "fil.h"
#include "fil_tii.h"
#include "fil_table.h"

/*
 *  ======== IFEA_Obj ========
 *  Every implementation of IFEA *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IFEA_Obj {
    struct IFEA_Fxns *fxns;    /* function list: standard, public, private */
} IFEA_Obj;

/*
 *  ======== IFEA_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IFEA_Obj *IFEA_Handle;

/*
 *  ======== IFEA_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in ifea.c.
 */
typedef volatile struct IFEA_Status {

    Int size;             /* This value must always be here, and must be set to the 
                             total size of this structure in 8-bit bytes, as the 
                             sizeof() operator would do. */
    XDAS_Int8 mode;       /* This is the 8-bit FEA Mode Control Register.  All 
                             Algorithms must have a mode control register.  */
    XDAS_Int8 unused;     /* This 8-bit register is unused, and is not only useful as a 
                             "spare", but also provides 16-bit alignment to the next 
                             value. */
} IFEA_Status;

/*
 *  ======== IFEA_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IFEA_Config {
    Int size; /* Size of IFEA_Config */
    Float *firCoeff; /* Pointer to FIR filter coefficient structure */
} IFEA_Config;

/*
 *  ======== IFEA_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a FEA object.
 *
 *  Every implementation of IFEA *must* declare this structure as
 *  the first member of the implementation's parameter structure.  This structure is actually
 *  instantiated and initialized in ifea.c.
 */
typedef struct IFEA_Params {
    Int size;
    const IFEA_Status *pStatus;
    IFEA_Config *pConfig;
} IFEA_Params;

/*
 *  ======== IFEA_PARAMS ========
 *  Default instance creation parameters (defined in ifea.c)
 */
extern const IFEA_Params IFEA_PARAMS;

/*
 *  ======== IFEA_Fxns ========
 *  All implementation's of FEA must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is FEA_<vendor>_IFEA, where
 *  <vendor> is the vendor name.
 */
typedef struct IFEA_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IFEA_Handle, PAF_AudioFrame *);
    Int         (*apply)(IFEA_Handle, PAF_AudioFrame *);
    /* private */
} IFEA_Fxns;

/*
 *  ======== IFEA_Cmd ========
 *  The Cmd enumeration defines the control commands for the FEA
 *  control method.
 */
typedef enum IFEA_Cmd {
    IFEA_NULL                   = ICOM_NULL,
    IFEA_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IFEA_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IFEA_GETSTATUS              = ICOM_GETSTATUS,
    IFEA_SETSTATUS              = ICOM_SETSTATUS
} IFEA_Cmd;

#endif  /* IFEA_ */
