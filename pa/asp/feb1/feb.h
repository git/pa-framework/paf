
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the FIL-ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the FEB
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef FEB_
#define FEB_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <ifeb.h>

#include "paftyp.h"

/*
 *  ======== FEB_Handle ========
 *  FIL-ASP Example Demonstration algorithm instance handle
 */
typedef struct IFEB_Obj *FEB_Handle;

/*
 *  ======== FEB_Params ========
 *  FIL-ASP Example Demonstration algorithm instance creation parameters
 */
typedef struct IFEB_Params FEB_Params;

/*
 *  ======== FEB_PARAMS ========
 *  Default instance parameters
 */
#define FEB_PARAMS IFEB_PARAMS

/*
 *  ======== FEB_Status ========
 *  Status structure for getting FEB instance attributes
 */
typedef volatile struct IFEB_Status FEB_Status;

/*
 *  ======== FEB_Cmd ========
 *  This typedef defines the control commands FEB objects
 */
typedef IFEB_Cmd   FEB_Cmd;

/*
 * ===== control method commands =====
 */
#define FEB_NULL IFEB_NULL
#define FEB_GETSTATUSADDRESS1 IFEB_GETSTATUSADDRESS1
#define FEB_GETSTATUSADDRESS2 IFEB_GETSTATUSADDRESS2
#define FEB_GETSTATUS IFEB_GETSTATUS
#define FEB_SETSTATUS IFEB_SETSTATUS

/*
 *  ======== FEB_create ========
 *  Create an instance of a FEB object.
 */
static inline FEB_Handle FEB_create(const IFEB_Fxns *fxns, const FEB_Params *prms)
{
    return ((FEB_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== FEB_delete ========
 *  Delete a FEB instance object
 */
static inline Void FEB_delete(FEB_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== FEB_apply ========
 */
extern Int FEB_apply(FEB_Handle, PAF_AudioFrame *);

/*
 *  ======== FEB_reset ========
 */
extern Int FEB_reset(FEB_Handle, PAF_AudioFrame *);

/*
 *  ======== FEB_exit ========
 *  Module finalization
 */
extern Void FEB_exit(Void);

/*
 *  ======== FEB_init ========
 *  Module initialization
 */
extern Void FEB_init(Void);

#endif  /* FEB_ */
