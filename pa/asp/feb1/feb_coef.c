
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* Headers */
#include "std.h"
#include "fil.h"
#include "fil_tii.h"

/* TYPE : Filter group=IR Cascade(0x1) and Filter sub-group=IIR-SOS-DF2(0x1) */
/*        SP,10 Biquad Cascades(tap=20), unicoeff, ch=1                      */
#define FIL_IIRSOSDF2_TYPE_FEB 0x208C0281

/* Channel select template */ 
#define FIL_CH_SELECT_FEB 0x1F07    /* Ch - L,R,C,Ls,Rs,Lb,Rb,Sbw */

/* Band centre frequecies */
Double  gFEB_EQFreqs[10] ={
32.7, 65.4, 130.8, 261.6, 523.3, 1046.5, 2093.0, 4186.0, 8372.0, 16744.0
}; 

/* PA/F sample rates, corresponding to its index order */
Double gByEqSampleRate[15] = {
0.00003125,          /* 1/32000.0  */
2.26757369614512e-5, /* 1/44100.0  */
2.08333333333333e-5, /* 1/48000.0  */
1.13378684807256e-5, /* 1/88200.0  */
1.04166666666666e-5, /* 1/96000.0  */
5.20833333333333e-6, /* 1/192000.0 */
0.000015625,         /* 1/64000.0  */
0.0000078125,        /* 1/128000.0 */ 
5.66893424036281e-6, /* 1/176400.0 */ 
0.000125,            /* 1/8000.0   */ 
9.07029478458049e-5, /* 1/11025.0  */ 
8.33333333333333e-5, /* 1/12000.0  */
0.0000625,           /* 1/16000.0  */
4.53514739229024e-5, /* 1/22050.0  */
4.16666666666666e-5, /* 1/24000.0  */
};

/* FEB coefficient struct, similar to PAF_FilCoef_PAF struct */
typedef struct FilCoef_FEB {
    LgUns type;     /* Filter type field */
    LgUns sampRate; /* Sample rate bit field */
    Float *coef;    /* Coefficient ptr */
} FilCoef_FEB;

/* Initial value */
FilCoef_FEB FilCoefEQ_FEB = 
{
    FIL_IIRSOSDF2_TYPE_FEB, /* Filter type field */
    0x0,                    /* Sample rate bit field */        
    0,                      /* Coefficient ptr */
};

/* Status */
IFIL_Status IFIL_Status_FEB = {
    sizeof(IFIL_Status),/* size */
    FIL_CH_SELECT_FEB,  /* mode, ch - L,R,C,Ls,Rs,Lb,Rb,Sbw */
    0x1,                /* use, default is 'enable' */                 
    FIL_CH_SELECT_FEB,  /* mask select, ch - L,R,C,Ls,Rs,Lb,Rb,Sbw */
    0x0                 /* mask status */ 
};

/* Config */
IFIL_Config IFIL_Config_FEB = {
    sizeof(IFIL_Config),          /* size */
    (PAF_FilCoef *)&FilCoefEQ_FEB /* Eq filter Coefficients */
};

/* Param */
const IFIL_Params IFIL_PARAMS_FEB = {
    sizeof(IFIL_Params),    /* size */ 
    1,                        /* use */
    &IFIL_Status_FEB,
    &IFIL_Config_FEB
};

/* EOF */









