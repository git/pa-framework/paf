
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  TII implementation of an FIL-ASP Example Demonstration algorithm -
 *  A Mutichannel, Ten-Band Equalizer implementation
 */

/* Standard headers */
#include <std.h>
#include  <std.h>
#include <xdas.h>
#include <math.h>

/* PAF framework headers */
#include "paftyp.h"

/* FEB module headers */
#include <ifeb.h>
#include <feb_tii.h>
#include <feb_tii_priv.h>
#include <feberr.h>

#if PAF_AUDIODATATYPE_FIXED
    #error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== FEB_TII_apply ========
 *  TII's implementation of the apply operation.
 *
 *  This function is called after the FEB_TII_reset function, and
 *  deals with control data AND sample data.
 *
 */

#ifndef _TMS320C6X
    #define restrict
#endif /* _TMS320C6X */

/* This function is made private to the application */
static Void FEB_EQSetup(FEB_TII_Obj *feb, PAF_AudioFrame *pAudioFrame);

Int FEB_TII_apply(IFEB_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    FEB_TII_Obj * restrict feb = (Void *)handle; /* Module handle */

    /* On disabling FEB module, set the reset flag; so it resets the when enabled */ 
    if((feb->pStatus->mode==0) && (feb->pStatus->unused==0))
        feb->pStatus->unused = 1;
            
    if (feb->pStatus->mode == 0)
        return 0;   /* If mode is zero, ASP is Disabled, so exit. */
        
    /* Reset the filter states when, FEB is enabled while the reset flag is '1'*/
    if((feb->pStatus->mode==1) && (feb->pStatus->unused==1))
    {      
          /* Reset the filter object by calling FIL reset function */
          ((IFIL_Fxns *)((IFEB_Fxns *)(feb->alg.fxns))->filFxns)->reset
                                     (feb->filHandle, pAudioFrame);
              
          feb->pStatus->unused = 0;
    }
    
    /* Update equalizer coeffs, depending on band gain and fs, before filtering */
    FEB_EQSetup(feb, pAudioFrame); 
 
    /* FIL apply function */
   ((IFIL_Fxns *)((IFEB_Fxns *)(feb->alg.fxns))->filFxns)->apply
                                      (feb->filHandle,pAudioFrame);
    return 0;
}

/*
 *  ======== FEB_TII_reset ========
 *  TII's implementation of the reset function, which is like an "information" 
 *  operation.  This is always called by the framework before calling the 
 *  FEB_TII_apply function, and only deals with control data, filter states etc
 *  and not sample data.
 */

Int FEB_TII_reset(IFEB_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    FEB_TII_Obj * restrict feb = (Void *)handle; /* FEB handle */

    /* Reset the filter objects, one by one, by calling FIL reset function */
    ((IFIL_Fxns *)((IFEB_Fxns *)(feb->alg.fxns))->filFxns)->reset
                                     (feb->filHandle, pAudioFrame);
          
    feb->pStatus->unused = 0; /* Set the reset flag to '0' */
    
    return 0;
}

/*
 *  ======== FEB_EQSetup ========
 */
static Void FEB_EQSetup(FEB_TII_Obj *feb, PAF_AudioFrame *pAudioFrame)
{
    Int i, bands = 10;
    Double pi = 3.142857142857;
    Double bySampleRate;
    Double by_2Q = 1.0/(2.0*1.2); /* Q factor of the filter */
    Float maxGain, delta = 0.05; /* delta is the grain size of gain */
    Uint filteredCh, mask;
    Float gain, gainTarget, maxGainDiff;
    Float *cPtr;
    Int coeffFlag = 0;
    Double by_a0, by_b0;
    
    /* Check whether coefficient calculation is required */
    for (i = 0; i < bands; i++)
    {
        maxGainDiff = feb->pStatus->bandGain[i] - feb->pStatus->gainStatus[i];
        
        /* Is target gain of any band much different from its current status */
        if( (maxGainDiff > 0.01) || (maxGainDiff < -0.01))
            coeffFlag = 1;   
            
    }

    /* Reset maxGain, which is used to update samSize */      
    maxGain = 0; 

    /* The gain is made to reach its target in a granular manner */    
    for (i = 0; i < bands; i++)
    {
        /* Get the present and target gains */
        gainTarget = feb->pStatus->bandGain[i];
        gain       = feb->pStatus->gainStatus[i];
        
        /* Calculate the changed gain */
        gain = (gainTarget - gain)*delta + gain;
        
        feb->pStatus->gainStatus[i] = gain;
        
        /* Update maxGain */
        if(gain > maxGain)
            maxGain = gain;            
    }

    /* Update the samSize of the filtered channels */    
    mask = 1;
    filteredCh = ((FIL_TII_Obj *)(feb->filHandle))->pStatus->maskStatus;
    
    for(mask = 1, i = 0; i < PAF_MAXNUMCHAN; i++, mask = mask<< 1 )
    {
        /* Reduce samSize by (maxGain/10.0)*2 */
        if(filteredCh & mask)
            pAudioFrame->data.samsiz[i] -= (PAF_AudioSize)(maxGain*0.2); 
    }
            
    /* If coeff. calculation is not required, return without processing */
    if(!coeffFlag)
        return;
    
    /* Get the sample rate by referring to PA/F handle sample rate index */
    bySampleRate = gByEqSampleRate[pAudioFrame->sampleRate -2];

    /* Initialy set input gain as 1.0 */
    cPtr = (Float *)((PAF_FilCoef_Void *)(((FIL_TII_Obj *)(feb->filHandle))
                                           ->pConfig->pCoefs))->cPtr[0];
    cPtr[0] = 1.0;    
    
    /* Calculate the coefficients for each band */
    for (i = 0; i < bands; i++) 
    {
        Double w, c, alpha, A;
        Double a0, a1, a2, b0, b1, b2;
        
        /* Get the band gain, after factoring by 10 */
        gain = feb->pStatus->gainStatus[i]*0.1;
        
        /* w = 2Pi * cutOffFreq/sampFreq */
        w = 2.0 * pi * gFEB_EQFreqs[i] * bySampleRate;
        
        c = -2.0 * cos(w);
        
        alpha = sin(w) * by_2Q;
        
        A     = pow(10.0, gain*0.025);         
        a0    = 1.0 + alpha/A; 
        by_a0 = 1.0/a0;
        
        a1 = c * by_a0;          
        a2 = (1.0 - (alpha/A)) * by_a0;
        b0 = (1.0 + (alpha*A)) * by_a0;
        b1 = a1;
        b2 = (1.0 - (alpha*A)) * by_a0; 
        a0 = 1.0;
        
        /* b0 of each band-biquad is made a single input gain */
        by_b0          = 1.0/b0;
        cPtr[0 ]       = cPtr[0 ]*b0; 
        cPtr[4*i + 1 ] = b1*by_b0; 
        cPtr[4*i + 2 ] = b2*by_b0; 
        cPtr[4*i + 3 ] = -a1; 
        cPtr[4*i + 4 ] = -a2;
        
    } /* for (i = 0; i < bands; i++) */
        
} /* FEB_EQSetup() */

/* EOF */
