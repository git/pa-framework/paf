/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil.h ========
 *  This header defines all types, constants, and functions used by 
 *  applications that use the FIL algoritm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the FIL
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
 
#ifndef FIL_
#define FIL_

#include <alg.h>
#include <ifil.h>
#include <ialg.h>
#include <paf_alg.h>


/*
 *  ======== FIL_Handle ========
 *  FIL algoritm instance handle
 */
typedef struct IFIL_Obj *FIL_Handle;

/*
 *  ======== FIL_Params ========
 *  FIL algoritm algoritm instance creation parameters
 */
typedef struct IFIL_Params FIL_Params;

/*
 *  ======== FIL_PARAMS ========
 *  Default instance parameters
 */
#define FIL_PARAMS IFIL_PARAMS

/*
 *  ======== FIL_Status ========
 *  Status structure for getting FIL instance attributes
 */
typedef volatile struct IFIL_Status FIL_Status;

/*
 *  ======== Fil_create ========
 *  Create an instance of a FIL object.
 */
static inline FIL_Handle FIL_create(const IFIL_Fxns *fxns, const FIL_Params *prms)
{
    return ((FIL_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== Fil_delete ========
 *  Delete a FIL instance object
 */
static inline Void FIL_delete(FIL_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== Fil_apply ========
 */
extern Int FIL_apply(FIL_Handle, PAF_AudioFrame * );

/*
 *  ======== FIL_reset ========
 */
extern Int FIL_reset(FIL_Handle, PAF_AudioFrame * );

/*
 *  ======== FIL_exit ========
 *  Module finalization
 */
extern Void FIL_exit(Void);

/*
 *  ======== FIL_init ========
 *  Module initialization
 */
extern Void FIL_init(Void);

/*
 * ===== control method commands =====
 */
#define FIL_NULL IFIL_NULL
#define FIL_GETSTATUSADDRESS1 IFIL_GETSTATUSADDRESS1
#define FIL_GETSTATUSADDRESS2 IFIL_GETSTATUSADDRESS2
#define FIL_GETSTATUS         IFIL_GETSTATUS
#define FIL_SETSTATUS         IFIL_SETSTATUS


#endif  /* FIL_ */
