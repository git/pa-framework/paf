/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil_tii.h ========
 *  Vendor specific (TII) interface header for FIL algoritm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular FIL implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */

#ifndef FIL_TII_
#define FIL_TII_

#include <ialg.h>
#include <ifil.h>
#include <icom.h>


/*
 *  ======== FIL_TII_Handle ========
 */
typedef struct FIL_TII_Obj *FIL_TII_Handle;

/*
 *  ======== FIL_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IFIL.
 */
typedef IFIL_Params FIL_TII_Params;


/*
 *  ======== FIL_TII_Status ========
 *  We don't add any new status to the standard one defined by IFIL.
 */
typedef IFIL_Status FIL_TII_Status;

/*
 *  ======== FIL_TII_Config ========
 *  We add a new field pParams for the inner layer of the filter.
 */
typedef struct FIL_TII_Config {
    Int size;
    Const PAF_FilCoef *pCoefs; 
    Void *pVars;
    IFIL_FunRec (*pIIR)[FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN];
    IFIL_FunRec (*pFIR)[FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN];
    IFIL_FunRec (*pSOS_DF2)[FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN];         
    PAF_FilParam *pParams;
}FIL_TII_Config;

/*
 *  ======== FIL_TII_Active ========
 *  We don't add any new active to the standard one defined by IFIL.
 */
 
/*
 *  ======== FIL_TII_PARAMS ========
 *  Define our default parameters.
 */
#define FIL_TII_PARAMS   IFIL_PARAMS

/*
 *  ======== FIL_TII_exit ========
 *  Required module finalization function
 */
#define FIL_TII_exit COM_TII_exit

/*
 *  ======== FIL_TII_init ========
 *  Required module initialization function
 */
#define FIL_TII_init COM_TII_init

/*
 *  ======== FIL_TII_delete ========
 *  Delete a FIL_TII instance object.
 */
#define FIL_TII_delete COM_TII_delete

/*
 *  ======== FIL_TII_create ========
 *  Create a FIL_TII instance object.
 */
extern FIL_TII_Handle FIL_TII_create(const FIL_TII_Params *params);

/*
 *  ======== FIL_TII_IALG ========
 *  TII's implementation of FIL's IALG interface
 */
extern const IALG_Fxns FIL_TII_IALG; 

/*
 *  ======== FIL_TII_IFIL ========
 *  TII's implementation of FIL's IFIL interface
 */
extern const IFIL_Fxns FIL_TII_IFIL; 


#endif  /* FIL_TII_ */
