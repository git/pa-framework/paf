/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil_tii_ifil.c ========
 *  FIL implementation - TII implementation of Generic FIL filter functions.
 */

#include <std.h>
#include <clk.h>
#include <log.h>

#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include  <std.h>
#include <xdas.h>
#include <paftyp.h>
#include <filtyp.h>


/*
 *  ======== FIL_TII_apply ========
 *  TII's implementation of the apply operation.
 *
 */
Int FIL_TII_apply(FIL_Handle handle, PAF_AudioFrame *pAudioFrame)
{    

    PAF_FilParam *param = ((FIL_TII_Obj *)handle)->pConfig->pParams;
    FIL_TII_Config *configPtr = (FIL_TII_Config *)((FIL_TII_Obj *)handle)->pConfig;
    
    Int maskLength, i;
    PAF_ChannelMask applyChMask = 0;
    PAF_ChannelMask satMask, subWMask; /*, satMaskRequest, subWMaskRequest; */
    Int samplePrec;
    Int noChannels;    
    Uint typeField;
    Short coeffPrec; 
    FIL_filterFnPtr filterFnPtr;
   
    /* MaskStatus temp is put in a local variable, and this field is resetted */
    ((FIL_TII_Obj *)handle)->pStatus->maskStatus = 0x0;  
    
    /* Return "Success" if FIL is switched OFF */
    if( !((FIL_TII_Obj *)handle)->pStatus->use )
        return(FIL_SUCCESS);
    
    /* Check whether maskSelect has become super set to the ch template */
    if( (  ((FIL_TII_Obj *)handle)->pStatus->maskSelect | ((FIL_TII_Obj *)handle)->pStatus->mode   ) 
           ^
           ((FIL_TII_Obj *)handle)->pStatus->mode
      )
      return( FIL_ERROR );
    
    /* If ch selected/template = 0, simply return SUCCESS */  
    if( ((FIL_TII_Obj *)handle)->pStatus->mode == 0 || ((FIL_TII_Obj *)handle)->pStatus->maskSelect == 0)
      return( FIL_SUCCESS );    
                   
    param->sampleCount   = pAudioFrame->sampleCount;      
    
    /* Derive input sample precision from PAF_AUDIODATATYPE; refer paftyp.h */
    #ifdef  PAF_AUDIODATATYPE
        #if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE
        samplePrec = PAF_AUDIODATATYPE_DOUBLE; /* 0 */ 
        #elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
        samplePrec = PAF_AUDIODATATYPE_FLOAT; /* 1 */ 
        #elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT32
        samplePrec = PAF_AUDIODATATYPE_INT32; /* 2 */
        #elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT16
        samplePrec = PAF_AUDIODATATYPE_INT16; /* 3 */
        #elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT8
        samplePrec = PAF_AUDIODATATYPE_INT8; /* 4 */
        #else
         #error
        #endif
    #endif    

    /* Satellite channel bit mask. */
    satMask = 
          pAudioFrame->pChannelConfigurationMaskTable->sat.pMask
          [
           (XDAS_UInt8 )pAudioFrame->channelConfigurationStream.part.sat >=
           pAudioFrame->pChannelConfigurationMaskTable->sat.length       ? 
           0 : (XDAS_UInt8 )pAudioFrame->channelConfigurationStream.part.sat
          ]; 
          
    /* SubWoofer channel bit mask. */
    subWMask = 
          pAudioFrame->pChannelConfigurationMaskTable->sub.pMask
          [
           (XDAS_UInt8 )pAudioFrame->channelConfigurationStream.part.sub >= 
           pAudioFrame->pChannelConfigurationMaskTable->sub.length       ? 
           0 : (XDAS_UInt8 )pAudioFrame->channelConfigurationStream.part.sub
          ];
          
    #if 0   /* Filter reset conditions is still to be discussed */
    /* AudioStream request mask */
    /* Satellite channel bit mask. */
    satMaskRequest = 
          pAudioFrame->pChannelConfigurationMaskTable->sat.pMask
          [
           (XDAS_UInt8 )pAudioFrame->channelConfigurationRequest.part.sat >=
           pAudioFrame->pChannelConfigurationMaskTable->sat.length       ? 
           0 : (XDAS_UInt8 )pAudioFrame->channelConfigurationRequest.part.sat
          ];
    /* SubWoofer channel bit mask. */
    subWMaskRequest = 
          pAudioFrame->pChannelConfigurationMaskTable->sub.pMask
          [
           (XDAS_UInt8 )pAudioFrame->channelConfigurationRequest.part.sub >= 
           pAudioFrame->pChannelConfigurationMaskTable->sub.length       ? 
           0 : (XDAS_UInt8 )pAudioFrame->channelConfigurationRequest.part.sub
          ];
    #endif 
             
    /* Eliminate channels which are not present in the audio stream. */ 
    applyChMask = ((FIL_TII_Obj *)handle)->pStatus->maskSelect & ( satMask | subWMask );

#if 0 /* Removed, as the logic is not sufficient to decide whether to reset or not */    
    /* Vars Resetting. */
    if(  ( applyChMask ^ maskStatus )
         /* || 
         (  (satMaskRequest | subWMaskRequest) ^ (satMask | subWMask)  )*/
      )
          FIL_varReset( handle );
#endif
    
    /* Get the filter type and coeff prec */ 
    typeField  = configPtr->pCoefs->type;
    coeffPrec  = FilMacro_GetPrecField(typeField); 
    
    /* Input sample precision <= filter precision. */
    if( samplePrec < coeffPrec )
        return( FIL_ERROR );
    
    /* Scan the ch select mask to get no of channels filtered */
    maskLength = sizeof(PAF_ChannelMask) * FIL_BITSPERBYTE;     
    FilMacro_1sToRight(applyChMask, maskLength, noChannels, i);

    param->channels = noChannels;
     
    /* Filter Group */ 
    switch(FilMacro_GetGroupField(typeField))
    {
        /* Filter Group - Impulse Response . */
        case FIL_IR :
        {
            Int tap;   
            Int tableCount = 0;   
                                                                                                                      
            tap = FilMacro_GetIRTap(typeField) ; 
            
            /* Minus 1, for mapping to arrays */
            tap--;
            noChannels--;
            
            /* Select proper precision array */
            if( samplePrec == 0 && coeffPrec == 0 )
                  tableCount = 0;
            else if( samplePrec == 1 && coeffPrec == 1 )
                     tableCount = 0; 
                 else if ( samplePrec == 1 && coeffPrec == 0 )
                          tableCount = 1; 
                                              
            /* Filter SubGroup - IIR */
            if( FilMacro_GetSubGroupField(typeField) == FIL_IR_IIR )
            {
                {
                    /* Declare a local var for the table */
                    IFIL_FunRec (*filTablePtr)[FIL_IR_IIRCHGEN];
                     
                    filTablePtr = configPtr->pIIR[tableCount];
                    
                    /* Limits the channels from crossing the array boundary */
                    if( noChannels > (FIL_IR_IIRCHGEN - 2) )
                        noChannels = FIL_IR_IIRCHGEN - 1;
                        
                    /* Limits the taps from crossing the array boundary */
                    if( tap > (FIL_IR_IIRTAPGEN - 2) )
                        tap = FIL_IR_IIRTAPGEN - 1;                                           
                        
                    /* Check whether the implementation is directly present. */
                    if( (filterFnPtr = filTablePtr[tap][noChannels].fnPtr) == 0 ) 
                    {        
                        /* Check whether implementation for N channels is there for given tap. */
                        if( (filterFnPtr = filTablePtr[tap][FIL_IR_IIRCHGEN - 1].fnPtr) != 0 )
                        {
                                noChannels = FIL_IR_IIRCHGEN - 1;
                        }        
                        /* If given taps is not present, then use the Ntap case. */               
                        else 
                        {
                            tap = FIL_IR_IIRTAPGEN - 1;
                            /* Check whether there is Ntap-Given channels implementation. */
                            if( (filterFnPtr = filTablePtr[FIL_IR_IIRTAPGEN - 1][noChannels].fnPtr) == 0   )
                                noChannels = FIL_IR_IIRCHGEN - 1;
                        } 
                    }
                        
                }
            }/* if IIR */  
            /* -------------- FIR -------------------------- */       
            /* Filter SubGroup - FIR */
            if( FilMacro_GetSubGroupField(typeField) == FIL_IR_FIR )
            {
                {
                    /* Declare a local var for the table */
                    IFIL_FunRec (*filTablePtr)[FIL_IR_FIRCHGEN];
                     
                    filTablePtr = configPtr->pFIR[tableCount];
                    
                    /* Limits the channels from crossing the array boundary */
                    if( noChannels > (FIL_IR_FIRCHGEN - 2) )
                        noChannels = FIL_IR_FIRCHGEN - 1;
                        
                    /* Limits the taps from crossing the array boundary */
                    if( tap > (FIL_IR_FIRTAPGEN - 2) )
                        tap = FIL_IR_FIRTAPGEN - 1;                   

                    /* Check whether the implementation is directly present. */
                    if( (filterFnPtr = filTablePtr[tap][noChannels].fnPtr) == 0 ) 
                    {        
                        /* Check whether implementation for N channels is there for given tap. */
                        if( (filterFnPtr = filTablePtr[tap][FIL_IR_FIRCHGEN - 1].fnPtr) != 0 )
                        {
                                noChannels = FIL_IR_FIRCHGEN - 1;
                        }        
                        /* If given taps is not present, then use the Ntap case. */               
                        else 
                        {
                            tap = FIL_IR_FIRTAPGEN - 1;
                            /* Check whether there is Ntap-Given channels implementation. */
                            if( (filterFnPtr = filTablePtr[FIL_IR_FIRTAPGEN - 1][noChannels].fnPtr) == 0   )
                                noChannels = FIL_IR_FIRCHGEN - 1;
                        } 
                    }
                        
                }
            }/* if FIR */ 

            /* If the filter function ptr is valid, .. */
            if( filterFnPtr != 0 )
            {
                /* Update the mask status field to the channels finally filtered */
                ((FIL_TII_Obj *)handle)->pStatus->maskStatus =  applyChMask; 
                
                /* Do setup of the param handle etc */
                if(handle->fxns->setup( handle, pAudioFrame, param ) != 0)
                    return(FIL_ERROR);
                    
                /* Filter function call */
                if( (*filterFnPtr)(param) != 0 )
                    return(FIL_ERROR);
            }
            else
            {
                return(FIL_FAIL);
            }
                    
        }/*case FIL_IR */
        break;
      
        case FIL_CASCADE_IR: 
        {
            Int tap;   
            Int tableCount = 0;   
                                                                                                                      
            tap = FilMacro_GetIRTap(typeField) ; 
            
            /* Minus 1, for mapping to arrays */
            tap--;
            noChannels--;
            
            /* Select proper precision array */
            if( samplePrec == 0 && coeffPrec == 0 )
                  tableCount = 0;
            else if( samplePrec == 1 && coeffPrec == 1 )
                     tableCount = 0; 
                 else if ( samplePrec == 1 && coeffPrec == 0 )
                          tableCount = 1; 
            
            if(tableCount)
                return(FIL_FAIL);
                
            // If subGroup == FIL_IR_IIR_CASC_SOS_DF2
            if( FilMacro_GetSubGroupField(typeField) == FIL_IR_IIR_CASC_SOS_DF2 )
            {
                // Get the function pointer for the specific channels and taps
                {
                    /* Declare a local var for the table */
                    IFIL_FunRec (*filTablePtr)[FIL_CASC_IR_SOS_DF2_CASC_GEN];
                    
                    if( ((tap + 1) < 4) || ((tap + 1) & 0x1) ) // Currently supports biquads
                        return(FIL_ERROR);
                    
                    tap = ((tap + 1) >> 1) - 2;
                     
                    filTablePtr = configPtr->pSOS_DF2[tableCount];
                    
                    /* Limits the channels from crossing the array boundary */
                    if( noChannels > (FIL_CASC_IR_SOS_DF2_CH_GEN - 2) )
                        noChannels = FIL_CASC_IR_SOS_DF2_CH_GEN - 1;
                        
                    /* Limits the taps from crossing the array boundary */
                    if( tap > (FIL_CASC_IR_SOS_DF2_CASC_GEN - 2) )
                        tap = FIL_CASC_IR_SOS_DF2_CASC_GEN - 1;                                           
                        
                    /* Check whether the implementation is directly present. */
                    if( (filterFnPtr = filTablePtr[noChannels][tap].fnPtr) == 0 ) 
                    {        
                        /* Check whether implementation for N channels is there for given tap. */
                        if( (filterFnPtr = filTablePtr[noChannels][FIL_CASC_IR_SOS_DF2_CASC_GEN - 1].fnPtr) != 0 )
                        {
                                tap = FIL_CASC_IR_SOS_DF2_CASC_GEN - 1;
                        }        
                        /* If given taps is not present, then use the Ntap case. */               
                        else 
                        {
                            noChannels = FIL_CASC_IR_SOS_DF2_CH_GEN - 1;
                            /* Check whether there is Ntap-Given channels implementation. */
                            if( (filterFnPtr = filTablePtr[FIL_CASC_IR_SOS_DF2_CH_GEN - 1][tap].fnPtr) == 0   )
                                tap = FIL_CASC_IR_SOS_DF2_CASC_GEN - 1;
                        } 
                    }
                        
                }
                
            }

            /* If the filter function ptr is valid, .. */
            if( filterFnPtr != 0 )
            {
                /* Update the mask status field to the channels finally filtered */
                ((FIL_TII_Obj *)handle)->pStatus->maskStatus =  applyChMask; 
                
                /* Do setup of the param handle etc */
                if(FIL_setup( handle, pAudioFrame, param ) != 0)
                    return(FIL_ERROR);
                    
                /* Filter function call */
                if( (*filterFnPtr)(param) != 0 )
                    return(FIL_ERROR);
            }
            else
            {
                return(FIL_FAIL);
            }                    
        }
        break;
      
        default : return (FIL_FAIL);

    }/* switch Group */
    
    /* Update the mask status field to the channels finally filtered */
    ((FIL_TII_Obj *)handle)->pStatus->maskStatus =  applyChMask; 
    
    return (FIL_SUCCESS);
        
} /* FIL_TII_apply() */


/*
 *  ======== FIL_TII_initfilter ========
 *  TII's implementation of the init filter operation.
 */
Int FIL_TII_initfilter(IFIL_Handle handle)
{                                   
/* Some initialisations can be done. */
    
    return 0;
}     

/*
 *  ======== FIL_TII_resetfilter ========
 *  TII's implementation of the reset filter operation.
 */
Int FIL_TII_resetfilter(IFIL_Handle handle)
{
    FIL_varReset( handle );
    return 0;
}   
    
/*  
 *  ======== FIL_TII_reset ========
 *  TII's implementation of the information operation.
 */ 
Int FIL_TII_reset(FIL_Handle handle, PAF_AudioFrame *pAudioFrame)
{

    handle->fxns->resetfilter(handle); 
  
    handle->fxns->initfilter(handle);
   
    return 0;
}

