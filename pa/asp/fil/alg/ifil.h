/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the FIL algoritm.
 */

#ifndef IFIL_
#define IFIL_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>
#include <filtyp.h>

#include "icom.h"
#include "paftyp.h"

/*
 *  ======== IFIL_Obj ========
 *  Every implementation of IFIL *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IFIL_Obj {
    struct IFIL_Fxns *fxns;    /* function list: standard, public, private */
} IFIL_Obj;

/*
 *  ======== IFIL_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IFIL_Obj *IFIL_Handle;

/*
 *  ======== IFIL_Fxns ========
 *  All implementation's of FIL must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is FIL_<vendor>_IFIL, where
 *  <vendor> is the vendor name.
 */
typedef struct IFIL_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IFIL_Handle handle, PAF_AudioFrame * pAudioFrame);
    Int         (*apply)(IFIL_Handle handle, PAF_AudioFrame *pFrame);
    
    /* private */
    Int         (*varsPerCh)( Uint type );
    Void        (*enquire)(IALG_Params *algParams, IALG_MemRec *filter_memTab);
    Void        (*memReset)( Void *varPtr, Int size );
    Int         (*initfilter)(IFIL_Handle handle);
    Int         (*resetfilter)(IFIL_Handle handle);
    Int         (*setup)(IFIL_Handle handle, PAF_AudioFrame *pAudioFrame, 
                         PAF_FilParam *pParam );
    Void        (*varReset)( IFIL_Handle handle );
    Int 		(*applyIR)(IFIL_Handle handle, PAF_AudioFrame *pAudioFrame, 
    						Int samplePrec, PAF_ChannelMask applyChMask) ;
      
} IFIL_Fxns;

typedef volatile struct IFIL_Status {
    Int size;
    PAF_ChannelMask mode; // The bitfield showing the max case of maskSelect for filtering. 
    XDAS_Int16  use;  // Global control register: 0: disable, 1: enable, etc. 
    PAF_ChannelMask maskSelect; // Selects channels to apply filter over.
    PAF_ChannelMask maskStatus; // filter actually applied 
} IFIL_Status; 

typedef struct IFIL_Config {
    Int size;
    Const PAF_FilCoef *pCoefs; 
    Void *pVars;
    IFIL_FunRec (*pIIR)[FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN];
    IFIL_FunRec (*pFIR)[FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN];
    IFIL_FunRec (*pSOS_DF2)[FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN];        
} IFIL_Config;

extern const IFIL_Fxns FIL_TII_IFIL;

/*
 *  ======== IFIL_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a FIL object.
 *
 *  Every implementation of IFIL *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IFIL_Params {
    Int size;
    Int use;
    const IFIL_Status  *pStatus;     
    const IFIL_Config  *pConfig;  
} IFIL_Params;

/*
 *  ======== IFIL_PARAMS ========
 *  Default instance creation parameters (defined in ifil.c)
 */
extern const IFIL_Params IFIL_PARAMS;

/*
 *  ======== IFIL_Cmd ========
 *  The Cmd enumeration defines the control commands for the FIL
 *  control method.
 */
typedef enum IFIL_Cmd {
    IFIL_NULL                   = ICOM_NULL,
    IFIL_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IFIL_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IFIL_GETSTATUS              = ICOM_GETSTATUS,
    IFIL_SETSTATUS              = ICOM_SETSTATUS
} IFIL_Cmd;

#endif  /* IFIL_ */
