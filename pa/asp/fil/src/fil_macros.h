/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil_macros.h ========
 *  This header defines all inner FIL macros, etc. 
 */

#ifndef FIL_MACROS_
#define FIL_MACROS_

#define PRAGMA #pragma 

/* Macros to get filter parameters from type fields */ 

/* Common to all */
#define FilMacro_GetGroupField(type) ( ( (type)&FIL_TYPE_CGMASK)>>23 )
#define FilMacro_GetPrecField(type)  ( ( (type)&FIL_TYPE_CPRECMASK)>>29 )
#define FilMacro_GetSubGroupField(type) ( ( (type)&FIL_TYPE_CSGMASK)>>19 )
/* IR Specific */
#define FilMacro_GetIRTap(type) (( (type)&FIL_TYPE_IRTAPMASK)>>5)
#define FilMacro_GetIRCh(type) ( (type)&FIL_TYPE_IRCHMASK )
#define FilMacro_GetIRCascade(type) ( (type & FIL_TYPE_IRCASCADEMASK) >> 15 ) 

/* Gets the no of 1's to right of a given bit no, in macro variable bitCount */
#define FilMacro_1sToRight(bitField, till, bitCount, i) \
        for( (i) = 0, (bitCount) = 0; (i) < (till); (i)++) \
        { \
            if( (bitField) & (0x1 << (i)) )\
                (bitCount)++; \
        } \

/* Gets the bit no of the left most 1, as macro variable 'bitCount' */
#define FilMacro_Left_1(bitField, fieldLength, bitCount, i) \
        for( (i) = (fieldLength-1), (bitCount) = 0; (i) >= 0; (i)--) \
        { \
            if( (bitField) & (0x1 << (i)) )\
                break; \
            else \
                (bitCount)++; \
        } \

#define FilMacro_GetIR_IIR_ProcField(type)  ( ( (type) & 0x00038000 ) >> 15 )

#endif

