/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil_vars.c ========
 *  FIL var memory handling functions. This includes all filter cases.
 */

/* Std header files */
#include <std.h>

/* Header files */
#include <paftyp.h>
#include <ialg.h>
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>

/*
 *  ======== FIL_varsPerCh() ========
 *  Calculates the var memory for one channel, for a given filter. 
 */
/* Memory section for the function code */
Int FIL_varsPerCh( Uint type )
{
    Int coeffPrec, bytesPerVar;
    Int i, count, adjPower2;

    coeffPrec = FilMacro_GetPrecField(type); /* Get coefficient precision */
    
    /* Identify the coeff prec and find the bytes/var-memory element */
    if( coeffPrec == PAF_AUDIODATATYPE_DOUBLE )
        bytesPerVar = 8;
    else if( coeffPrec == PAF_AUDIODATATYPE_FLOAT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT16 )
        bytesPerVar = 2;
    
    /* Switch to the given filter group */           
    switch(FilMacro_GetGroupField(type))
    {
        /* Impulse Response Group */
        case FIL_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);
            taps = FilMacro_GetIRTap(type) ;

            /* IIR Sub-group */
            if (subGroup == FIL_IR_IIR_DF1) {
#ifdef PAF_DEVICE
                if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1) {
                    bytesPerVar = 8; /* Internal processing is Double precision */
					if (taps == 2) return 24;		// MX1 T2 is DF1 implementation;
				}
#endif // PAF_DEVICE                    
 
                return( taps * bytesPerVar );
            }
			else if (subGroup == FIL_IR_IIR_DF2 || subGroup == FIL_IR_IIR_DF2T) {
#ifdef PAF_DEVICE
                if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1) {
                    bytesPerVar = 8; /* Internal processing is Double precision */
					if (taps == 2) return 24;		// MX1 T2 is DF1 implementation;
				}
#endif // PAF_DEVICE
				return (bytesPerVar * taps);	
			}
            /* FIR Sub-group */
            else
            {
            /* State mem is made ^2, for easy circularing */
                /* count=bits from MSB bit to left most '1' */                
                FilMacro_Left_1(taps, 16, count, i); 
                                
                adjPower2 = 0x8000 >> count;/* Get the ^2 just below 'taps' */
                /* If taps is not ^2 */
                if( taps & (~adjPower2) )
                    adjPower2 <<= 1; /* Shift, to get next ^2 no, above taps */
                                  
                return( adjPower2 * bytesPerVar );
            }
        } /* case FIL_IR */
               
        case FIL_CASCADE_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);

#ifdef PAF_DEVICE
            if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1)
                bytesPerVar = 8; /* Internal processing is Double precision */
#endif // PAF_DEVICE

            if( subGroup == FIL_IR_IIR_CASC_SOS_DF2 || subGroup == FIL_IR_IIR_CASC_SOS_DF2T)
            {
                // Get the no. of cascades 
                taps = FilMacro_GetIRTap(type) ;
                                
                // Return 2*cascades
                return( taps * bytesPerVar );
            }
            else
                return(FIL_ERROR);
        }
               
        default :  
            return(0);
            
    } /* switch(FilMacro_GetGroupField(type)) */

} /* Int FIL_varsPerCh() */

/*
 *  ======== FIL_varReset() ========
 *  Resets the filter var memory to 0, given FIL handle. 
 */
/* Memory section for the function code */
Void FIL_varReset( FIL_Handle handle )
{
    FIL_TII_Config *configPtr = (FIL_TII_Config *)((FIL_TII_Obj *)handle)->pConfig;
    Int maxCh, i;
    
    /* Find max no of channels filtered, from the channel select template */
    FilMacro_1sToRight(((FIL_TII_Obj *)handle)->pStatus->mode, 
                      sizeof(PAF_ChannelMask)*8, maxCh, i);
    
    /* Reset the var memory to 0 */
    handle->fxns->memReset( (Void *)configPtr->pVars, maxCh*handle->fxns->varsPerCh(configPtr->pCoefs->type) );    
}
