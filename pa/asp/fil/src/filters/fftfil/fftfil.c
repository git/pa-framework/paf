/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// ............................................................................

#include <math.h>
#include <stdlib.h>

// ............................................................................

#ifdef _TMS320C6X
extern void fft_SPXSP_asm( int n, float (*cx)[2], /* const */ float (*cW)[2], float (*cY)[2], const unsigned char *brev, int n_min, int offset, int n_max);

// bit-reversal index table

const unsigned char brev[] = {
    0x0, 0x20, 0x10, 0x30, 0x8, 0x28, 0x18, 0x38,
    0x4, 0x24, 0x14, 0x34, 0xc, 0x2c, 0x1c, 0x3c,
    0x2, 0x22, 0x12, 0x32, 0xa, 0x2a, 0x1a, 0x3a,
    0x6, 0x26, 0x16, 0x36, 0xe, 0x2e, 0x1e, 0x3e,
    0x1, 0x21, 0x11, 0x31, 0x9, 0x29, 0x19, 0x39,
    0x5, 0x25, 0x15, 0x35, 0xd, 0x2d, 0x1d, 0x3d,
    0x3, 0x23, 0x13, 0x33, 0xb, 0x2b, 0x1b, 0x3b,
    0x7, 0x27, 0x17, 0x37, 0xf, 0x2f, 0x1f, 0x3f,
};
#endif  // _TMS320C6X

// ----------------------------------------------------------------------------

#ifdef FFTCLK
#include <stdio.h>
#include <time.h>
#endif  // FFTCLK

void fftfil(
                float (* restrict cT)[2], // (complex) temporary storage (n)
    /* const */ float (* restrict cW)[2], // (complex) twiddle factors (n)
    /* const */ float (* restrict cWr)[2], // (complex) twiddle factors (n/2)
    /* const */ float (* restrict cR)[2], // twiddle factors (real-input FFT) (n/2)
    /* const */ float (* restrict cH)[2], // (complex) filter (transform) ((n/2+1) x nk)
                float (* restrict cX)[2], // (complex) input  (transform) ((n/2+1) x nk)
                float (* restrict cx)[2], // real input -> complex output (n)
          const int n,   // FFT size (i.e., one block)
          const int rad, // radix (2 or 4)
          const int nk,  // no. of "multi-blocks"
                int * const pkk)  // modulo-nk counter (updated here)
{
    int j, k;
    const int no2 = n>>1;
    float rT, iT, rQ, iQ;
    float (* restrict cS)[2] = cT;
    float (* restrict cD)[2] = cX + *pkk * (no2+1); // current block in input delay line

#ifdef FFTCLK
    clock_t clk[5];
#endif  // FFTCLK

// ............................................................................

    /* cT[0..n/2-1] = FFT[n/2]( real cx) using twiddle cWr */

    // even/odd samples of real input "cx", of length "n", will be interpreted as being
    // real/imag parts of complex input of length "no2" = n/2

#ifdef FFTCLK
    clk[0] = clock();
#endif  // FFTCLK

#ifdef _TMS320C6X
    fft_SPXSP_asm( no2, cx, cWr, cT, brev, (4+2) - rad, 0, no2);
#endif  // _TMS320C6X

#ifdef FFTCLK
    clk[1] = clock();
#endif  // FFTCLK

// ............................................................................

    /* convert output of half-size FFT as if computed w/ full-size FFT:
       cD[0..n/2+1] = "convert"( cT/cS[0..n/2-1]) */

    // this simplifies construction of below loop
    cT[no2][0] = cT[0][0];
    cT[no2][1] = cT[0][1];

    // ii = 5 / 2 -- I/O bound
    _nassert( (int) cR % sizeof( float[2]) == 0);
    _nassert( (int) cT % sizeof( float[2]) == 0);
    _nassert( (int) cS % sizeof( float[2]) == 0);
    _nassert( (int) cD % sizeof( float[2]) == 0);
#pragma MUST_ITERATE(4,,4)
#pragma UNROLL(2)

    for( j = 0; j < no2; j++)
    {
        rQ = cT[j][0] - cS[no2-j][0];
        iQ = cT[j][1] + cS[no2-j][1];

        rT = cT[j][0] + cS[no2-j][0];
        iT = cT[j][1] - cS[no2-j][1];

        cD[j][0] = rT + rQ * cR[j][1] + iQ * cR[j][0];
        cD[j][1] = iT - rQ * cR[j][0] + iQ * cR[j][1];
    }

    cD[no2][0] = 2 * (cT[0][0] - cT[0][1]); // scale by 2 to counteract "/ 2" built into cH
    cD[no2][1] = 0;

#ifdef FFTCLK
    clk[2] = clock();
#endif  // FFTCLK

// ............................................................................

    /* clear memory used as "accumulator" */

    // * note: could optimize to remove this (& other code) for nk == 1 *

    // ii = 1
    for( j = 0; j <= no2; j++)
        cT[j][1] = cT[j][0] = 0;

// ............................................................................

    for( k = 0; k < nk; k++)
    {

// ............................................................................

        /* complex dot product: cT/cS[0..n-1] = cD[0..n/2] * cH[0..n/2] */

        /* ref. http://en.wikipedia.org/wiki/Discrete_Fourier_transform#Expressing_the_inverse_DFT_in_terms_of_the_DFT :
         *   IFFT(X(k)) = FFT*(X*(k)) / N
         * Note that, for real output, this simplifies to:
         *   IFFT(X(k)) = FFT (X*(k)) / N
         * This is effected by
         *   (1) negation in calculating "cT[j][1]" below, and
         *   (2) applying "1/N" to H.
         */

        // ii = 3
        _nassert( (int) cH % sizeof( float[2]) == 0);
        _nassert( (int) cT % sizeof( float[2]) == 0);
        _nassert( (int) cD % sizeof( float[2]) == 0);
#pragma MUST_ITERATE(2,,)

        for( j = 1; j < no2; j++)
        {
            rT = cD[j][0] , iT = cD[j][1];
            cT[j][0] +=   rT * cH[j][0] - iT * cH[j][1];
            // (inner) negation for IFFT impl. w/ FFT
            cT[j][1] += - rT * cH[j][1] - iT * cH[j][0];
        }

        // (just two iterations -- dc & fs/2)
#pragma MUST_ITERATE(2,2,2)

        for( j = 0; j <= no2; j += no2)
        {
            rT = cD[j][0] , iT = cD[j][1];
            cT[j][0] +=   rT * cH[j][0] - iT * cH[j][1];
            cT[j][1] += - rT * cH[j][1] - iT * cH[j][0];
        }

// ............................................................................

        cH += (no2+1);

        *pkk = (*pkk + 1) % nk;
        cD = cX + *pkk * (no2+1);
    }

    *pkk = (*pkk + (nk-1)) % nk; // effectively backs up 1 block, for next call

// ............................................................................

    /* populate rest of cT[] using symmetry */

    {
        float * restrict pS = &cS[n-1][1];

        // ii = 2
        _nassert( (int) cT % sizeof( float[2]) == 0);
     // _nassert( (int) cS % sizeof( float[2]) == 0);
#pragma MUST_ITERATE(2,,)

        for( j = 1; j < no2; j++)
        {
            /* cS[n-j][1] */ *pS-- = -cT[j][1];
            /* cS[n-j][0] */ *pS-- =  cT[j][0];
        }
    }

// ............................................................................

    /* cx = IFFT( cT) using twiddle cW */

#ifdef FFTCLK
    clk[3] = clock();
#endif  // FFTCLK

#ifdef _TMS320C6X
    fft_SPXSP_asm( n, cT, cW, cx, brev, rad, 0, n);
#endif  // _TMS320C6X

#ifdef FFTCLK
    clk[4] = clock();
#endif  // FFTCLK

// ............................................................................

#ifdef FFTCLK
    fprintf( stderr, "FFT: %d, Real->Complex: %d, Complex Dot Product: %d, IFFT: %d cycles => %d cycles/sample\n",
        clk[1] - clk[0], clk[2] - clk[1], clk[3] - clk[2], clk[4] - clk[3],
        // * really need to divide by L, rather than N/2,
        // in case filter length not a power of two *
        (int) ((double) (clk[4] - clk[0]) / (n/2)) );
#endif  // FFTCLK
}

// ----------------------------------------------------------------------------

#ifdef _TMS320C6X

/* Function for generating Specialized sequence of twiddle factors */

void tw_gen( float (*cW)[2], int n)
{
    const double PI = 3.141592654;
    int i, j, k, l;

    k = 0;
    for( j = 1; j <= n>>2; j <<= 2)
        for( i = 0; i < n>>2; i += j)
            for( l = 1; l <= 3; l++)
            {
                const double theta = l*2*PI*i/n;
                cW[k][0] = (float) cos( theta);
                cW[k][1] = (float) sin( theta);
                k++;
            }
}

#endif  // _TMS320C6X

// ----------------------------------------------------------------------------

/* simple twiddle table */

void tw_gen2( float (*cW)[2], int n)
{
    const double PI = 3.141592654;
    int i, k;

    k = 0;
    for( i = 0; i < n>>1; i++)
    {
        const double theta = 2*PI*i/n;
        cW[k][0] = (float) cos( theta);
        cW[k][1] = (float)-sin( theta);
        k++;
    }
}

// ----------------------------------------------------------------------------

#ifdef TEST

#include <stdio.h>
#include <stdarg.h>

const char *tool = NULL;

// ............................................................................

static void usage()
{
    fprintf( stderr, "Usage: %s options files\n", tool);
    fprintf( stderr, "Options:\n");
    fprintf( stderr, "\t-f filterCoeffs\tSpecify filter-coefficients file\n");
    fprintf( stderr, "\t-h\t\tDisplay this help\n");
    fprintf( stderr, "\t-n numPoints\tSpecify FFT size\n");

    exit( 0);
}

// ............................................................................

static void error( char *fmt, ...)
{
    va_list args;

    va_start( args, fmt);

    if( tool != NULL)
        fprintf( stderr, "%s: ", tool);

    vfprintf( stderr, fmt, args);

    va_end( args);

    exit( 1);
}

// ----------------------------------------------------------------------------

int main( int argc, const char **argv) {

    float *h = NULL;
    int N = 1024; // (default) FFT size
    int i, j, NH;

// ............................................................................

    /* process command-line arguments */

    tool = argv[0];

    if( argc <= 1)
        usage();

    for( i = 1; i < argc; i++) {
        const char * arg = argv[i];

        switch( *arg++) {
            case( '-'):
                switch( *arg++) {
                    case( 'f'):
                    {
                        FILE * fin = NULL;

                        if( !( (fin = fopen( arg, "r")) != NULL))
                            error( "Option \"%c%c\" can't open file \"%s\"\n",
                                arg[-2], arg[-1], arg);

                        for( j = 0; ; j++) {
                            float x;

                            // value discarded -- just determining file length in 1st pass
                            if( !( /* !feof( fin) && */ fscanf( fin, "%g", &x) == 1)) {
                                // ** RTS bug? if file not empty, last value read twice; feof() doesn't help **
                                NH = j == 0 ? j : j-1; //  % (total) filter length
                                break;
                            }
                        }

                        if( !( (h = (float *) malloc( NH * sizeof( *h))) != NULL))
                            error( "Memory allocation failure (%s)\n", "h");

                        rewind( fin);

                        for( j = 0; j < NH; j++)
                            if( !( fscanf( fin, "%g", &h[j]) == 1))
                                error( "Failed to properly rewind file \"%s\"\n", arg);

                        fclose( fin);
                    }
                        break;
                    case( 'h'):
                        usage();
                    case( 'n'):
                        if( !( sscanf( arg, "%d", &N) == 1 && N > 0))
                            error( "Option \"%c%c\" requires a positive-integer argument\n",
                                arg[-2], arg[-1]);
                        break;
                    default:
                        error( "Invalid option: \"%s\"\n", argv[i]);
                }
                break;
            default:
                error( "Invalid argument: \"%s\"\n", argv[i]);
        }
    }

// ............................................................................

    for( j = 0; j <= 31; j++)
        if( (N & (1 << j)))
            break;

    {
        const int
            rad = j % 2 == 0 ? 4 : 2,
            L = N/2, // no. of (new) inputs/outputs per processing block
            M = N/2, // filter segment length
            K = ceil( (double) NH / M); // no. of filter segments to be processed
        int k, kk;
        float (*cT)[2], (*cW)[2], (*cWr)[2], (*cR)[2], (*cH)[2], (*cX)[2], (*cx)[2], *xsav;

// ............................................................................

        /* set up FFT twiddle factors */

        if( !( (cW = (float (*)[2]) malloc( N * sizeof( *cW))) != NULL))
            error( "Memory allocation failure (%s)\n", "cW");

#ifdef _TMS320C6X
        tw_gen( cW, N);
#endif  // _TMS320C6X

        if( !( (cWr = (float (*)[2]) malloc( N/2 * sizeof( *cWr))) != NULL))
            error( "Memory allocation failure (%s)\n", "cWr");

#ifdef _TMS320C6X
        tw_gen( cWr, N/2);
#endif  // _TMS320C6X

        if( !( (cR = (float (*)[2]) malloc( N/2 * sizeof( *cR))) != NULL))
            error( "Memory allocation failure (%s)\n", "cR");

        tw_gen2( cR, N);

// ............................................................................

        /* partition filter coefficients and calculate FFTs */

        if( !( (cT = (float (*)[2]) malloc( N * sizeof( *cT))) != NULL))
            error( "Memory allocation failure (%s)\n", "cT");

        // * could reduce ((K-1) * (N/2+1) + N) to (K * (N/2+1))
        //   if different/temporary memory used for output of fft_SPXSP_asm() below *
        if( !( (cH = (float (*)[2]) malloc( ((K-1) * (N/2+1) + N) * sizeof( *cH))) != NULL))
            error( "Memory allocation failure (%s)\n", "cH");

        i = 0;
        for( k = 0; k < K; k++) {
            for( j = 0; j < N; j++) {
                // pre-scale filter coefficients -- compensate for FFT used in place of IFFT
                // "/ 2" is for real-input FFT optimizations

                // ** the below cast to (float) *shouldn't* be necessary,
                // but if not performed, the 2nd call to fft_SPXSP_asm()
                // gives zero output, w/ N = 512 **

                cT[j][0] = j < M && i < NH ? h[i++] / (float) N / 2. : 0;
                cT[j][1] = 0;
            }

//  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

#ifdef _TMS320C6X
            // writes to N (complex) locations
            fft_SPXSP_asm( N, cT, cW, cH + k*(N/2+1), brev, rad, 0, N);
#endif  // _TMS320C6X
        }

        free( h); h = NULL;

// ............................................................................

#if 0
        {
            int j;
            FILE * fout = NULL;
            const char sout[] = "cH.text";

            if( !( (fout = fopen( sout, "w")) != NULL))
                error( "Unable to create output file \"%s\"\n", sout);

            fprintf( fout, "# Created by 'fftfil.c'\n");
            fprintf( fout, "# name: %s\n", "cH");
            fprintf( fout, "# type: complex matrix\n");
            fprintf( fout, "# rows: %d\n", N);
            fprintf( fout, "# columns: %d\n", K);

            for( j = 0; j < N; j++)
            {
                int k;
                for( k = 0; k < K; k++)
                    fprintf( fout, " (%.13g,%.13g)", cH[k*N+j][0], cH[k*N+j][1]);
                fprintf( fout, "\n");
            }

            fclose( fout);
        }
#endif

// ............................................................................

        /* allocate and initialize input (transform) delay line */

        if( !( (cX = (float (*)[2]) malloc( K * (N/2+1) * sizeof( *cX))) != NULL))
            error( "Memory allocation failure (%s)\n", "cX");

        // clear delay line
        for( j = 0; j < K * (N/2+1); j++)
            cX[j][1] = cX[j][0] = 0;

        kk = 0; // modulo-K counter for input delay line

// ............................................................................

        if( !( (cx = (float (*)[2]) malloc( N * sizeof( *cx))) != NULL))
            error( "Memory allocation failure (%s)\n", "cx");

        if( !( (xsav = (float *) malloc( (M-1) * sizeof( *xsav))) != NULL))
            error( "Memory allocation failure (%s)\n", "xsav");

        // pre-load (M-1) "past" inputs (initially zero)
        for( j = 0; j < M-1; j++)
            xsav[j] = 0;

        while( 1)
        {
            int kOut;
            float *x = (float *) cx; // form (N) "real" inputs for fftfil()

            // zero-fill beg. of array -- essentially "don't care", but avoid NaN etc.
            for( j = 0; j < (N-L-(M-1)); j++)
                x[j] = 0;

            // load (M-1) "shifted" input samples (for overlap-save)
            for( j = 0; j < M-1; j++)
                x[j+(N-L-(M-1))] = xsav[j];

            // load (L) new inputs
            kOut = N; // (expected) input/output count (not index)
            for( j = N-L; j < kOut; j++)
                if( !( scanf( "%g", &x[j]) == 1))
                {
                    // ** RTS bug? if file not empty, last value read twice **
                    kOut = j-1; // on EOF, shorten kOut to last *good* sample
                    break;
                }
            if( kOut <= N-L)  // no inputs?
                break;

            // zero-pad after end of input samples (if needed)
            for( ; j < N; j++)
                x[j] = 0;

            // save last (M-1) input samples (for overlap-save)
            for( j = 0; j < M-1; j++)
                xsav[j] = x[j+(N-(M-1))];

            fftfil( cT, cW, cWr, cR, cH, cX, cx, N, rad, K, &kk);

//  . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

            // output is *last* L samples (N-L .. N-1)
            for( j = N-L; j < kOut; j++)
                printf( "%.13g\n", cx[j][0]);

            if( kOut < N)  // partial input block (presume EOF)?
                break;
        }
    }

    return( 0);
}

// ............................................................................

#endif  // TEST
