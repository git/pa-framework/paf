% /*
% * Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
% * All rights reserved.	
% *
% *  Redistribution and use in source and binary forms, with or without
% *  modification, are permitted provided that the following conditions
% *  are met:
% *
% *    Redistributions of source code must retain the above copyright
% *    notice, this list of conditions and the following disclaimer.
% *
% *    Redistributions in binary form must reproduce the above copyright
% *    notice, this list of conditions and the following disclaimer in the
% *    documentation and/or other materials provided with the
% *    distribution.
% *
% *    Neither the name of Texas Instruments Incorporated nor the names of
% *    its contributors may be used to endorse or promote products derived
% *    from this software without specific prior written permission.
% *
% *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
% *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% *
% */

% .............................................................................

function v = rev1( v)
  v = shift( v(length(v):-1:1), 1);
end % function

% .............................................................................

N = 256;

[t, g] = mksin( 2*N, 1, 1, 0); % generates 2*N+1 samples
g = g(1:2*N)'; % col, 2*N samples

% g = randn(2*N,1); % col

xg = reshape(g,2,N)'; % two col vectors
x = xg(:,1) + j*xg(:,2);

X = fft( x, N);

k = linspace( 0, N-1, N)'; % col
W = exp( -j*2*pi*k/(2*N));

A = (1 - j*W)/2;
B = (1 + j*W)/2;

G = X .* A + conj( rev1( X)) .* B;

% .............................................................................

% below is same formulation as above, but w/ Re/Im detail -- needs 8 multiplies:

Xr = real( X); Xi = imag( X);
Ar = real( A); Ai = imag( A);
Br = real( B); Bi = imag( B);

Gr = Xr.*Ar - Xi.*Ai + rev1( Xr).*Br + rev1( Xi).*Bi;
Gi = Xi.*Ar + Xr.*Ai + rev1( Xr).*Bi - rev1( Xi).*Br;
% G = Gr + j*Gi;

% .............................................................................

% per O & S, P6.10:

X1 = (X + conj( rev1( X)))/2;
X2 = (X - conj( rev1( X)))/(2*j);
% G = X1 + W .* X2;

% .............................................................................

% ignoring the "/ 2", the following re-formulation requires only 4 multiplies:

Wr = real( W); Wi = imag( W);

Gr = Xr + rev1( Xr); Gi = Xi - rev1( Xi);
Qr = Xr - rev1( Xr); Qi = Xi + rev1( Xi);

Gr +=  Wi .* Qr + Wr .* Qi;
Gi += -Wr .* Qr + Wi .* Qi;

G = (Gr + j * Gi) / 2;

% .............................................................................

Gref = fft( g, 2*N)(1:N);
Gdif = abs( G - Gref);

stats( Gdif);
% plot( Gdif); pause;
