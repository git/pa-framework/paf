
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#..............................................................................

TOOL= fftfil

EXTRA= fft_SPXSP_h.asm
FFTCLK= -D"FFTCLK"
CFLAGS= -q -o3 -ml3 -mv67p -mw -os -al -on2
    # -o3	-- enables file-level optimizations
    # -ml3	-- defaults all data and calls to far
    # -mv67p	-- target-specific instructions and alignment for C672x
    # -mw	-- output extra information about software-pipelined loops
    # -os	-- interlists optimizer comments with assembly
    #              (never changes code performance; warns if used w/o -o)
    #    => -k	-- retains the assembly language output from the compiler (*.asm)
    # -al	-- produce an assembly listing (*.lst)
    # -on2	-- create verbose info file (*.nfo)

    # don't use -- "can have a negative performance and/or code size impact"
    # -s	-- invokes the interlist utility ("Source interlist")
    # -ss	-- invokes the interlist utility ("C source-code interlist")

TOPT= # verbose, bplot, bsim

TARGET= $(TOOL).out

default: test

.PHONY: build test delivery clean

%.out: %.c
	. ccs_setup.sh "c:/CCStudio_v3.1"; \
	cl6x $(CFLAGS) -D"TEST" $(FFTCLK) "$<" $(EXTRA) \
		-z -c -x -heap 0x23800 --args=1024 -l"rts67plus.lib" -o"$@" -m"$(basename $@).map"

build: $(TARGET)

test: build
	( echo "test( $(TOPT) )" | octave -q ) 2>&1 | tee test.tmp
ifeq ($(strip $(TOPT)),) # compare only if no TOPTs specified
	diff test.txt test.tmp
endif

#..............................................................................

FILES= $(basename $(TARGET)).c $(EXTRA) fftfil.m
DIR= delog
DATE:= $(shell date +%y%m%d )
ZIP= $(basename $(TARGET))_$(DATE).zip

delivery:
	sed -e '/nargin >= 2/,/^end/ d' -e "s/h = fil.num.c';/h = fil;/" t:/pa/bin/fftfil.m > fftfil.m
	delog -d"$(DIR)" $(FILES)
	rm -v fftfil.m
	zip -r -j $(ZIP) "$(DIR)"

#..............................................................................

clean:
	@rm -vf $(TARGET) *.obj *.map *.lst *.nfo *.tmp *.text # *.txt
	@rm -vfr "$(DIR)" $(ZIP)
