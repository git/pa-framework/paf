

	.global	_fir2Ntap8Msample

BK0_SHFT	.equ	16
	
_fir2Ntap8Msample

		;	parameters	sptr, cptr, iptr, optr, scnt, ntap, oinc
		;	sptr:	pointer to delay(filter state) buffer where the FIR computation starts.  sptr should be
		; 			aligned at double-word boundary.  The buffer start address is assumed to be the nearest and
		;			lower 2^N word boundary, where 2^N >= ntap.
		;	cptr:	pointer to coefficient buffer.  cptr should be aligned at double-word boundary.
		;	iptr:	pointer to input buffer.  iptr should be aligned at double-word boundary.
		;	optr:	pointer to output buffer. optr should be aligned at word boundary.
		;	scnt:	number of output samples to process.  scnt should be a integer multiple of 8.
		;	ntap:	number of taps in the FIR filter.  This also dictates delay buffer size and alignment.
		;			ntap should be an even number bigger than 16.
		;	oinc:	output pointer increment value in word addressing.  Should be 1 in normal FIR use.
		;			Useful in upsampler. 2 for 2x upsampling, and 4 for 4x upsampling.
		;
		;	***		IRP is used as a temporary storage, and this loop cannot be interruptible.
		
		.asg	a4,sptra
		.asg	b4,sptrb
		.asg	a8,iptr
		.asg	b6,optrb
		.asg	a0,optra
		.asg	b13,ntap
			
		.asg	b2,scnt
		.asg	b1,tcnt
				
		.asg 	a2,in08
		.asg	a3,in1
		.asg	a6,in2
		.asg	a7,in3
		.asg	a12,in4
		.asg	a13,in5
		.asg	a14,in6
		.asg	a15,in7
		.asg	b10,cf0
		.asg 	b11,cf1
							
		.asg	a5,proda
		.asg	b5,prodb
		
		.asg	a1,o0
		.asg	a9,o2
		.asg	a10,o4
		.asg	a11,o6
		.asg	b7,o1		
		.asg	b9,o3
		.asg	b3,o5
		.asg	b12,o7
		
		.asg	b8,offset
		.asg	b14,cptr
		.asg	b0,oav
											
	; reassign parameters and save registers on stack

		stw		a10,*b15--[1]
||		mvc		csr,b0

		stw		b10,*b15--[1]
	
		stw		a11,*b15--[1]
||		clr		b0,0,0,b0	

		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
	
		stw		a13,*b15--[1]
||		mvc		b0,csr	

		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]
	
		mv		a8,scnt
		mv		a6,iptr
		mv		b4,cptr
		mv		b8,ntap
	
	; set up AMR, pointers, constants, loops, etc
		
		add		ntap,ntap,b8
		sub		b8,1,b8
		norm	b8,b8
		sub		b8,31,b8
		neg		b8,b8
		shl		b8,BK0_SHFT,b8
		set		b8,0,0,b8
		set		b8,8,8,b8			; set 0th/4th bit of logSz: a4/b4 circular with BK0
		mvc		b8,amr
		sub		ntap,10,offset
		mv		sptra,sptrb
		addaw	sptrb,ntap,sptrb
		mpy		a10,4,oav
		mv		optrb,optra
		add		optrb,oav,optrb
		zero	oav
		add		a10,a10,a10
		mvc		a10,irp
		
LOOP1
			lddw	*cptr++[1],cf1:cf0
||			lddw	*sptra++[1],in1:in08

			lddw	*sptra++[1],in3:in2
||			zero	o0
||			zero	o1
||			sub		ntap,12,tcnt

			lddw	*sptra++[1],in5:in4

			lddw	*sptra++[1],in7:in6

			ldw		*sptra--[6],in08

	[oav]	stw		o2,*optra++[in6]
||	[oav]	stw		o3,*optrb++[oav]
||			zero	o2
||			zero	o3
||			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb

	[oav]	stw		o4,*optra++[in6]
||	[oav]	stw		o5,*optrb++[oav]
||			zero	o4
||			zero	o5
||			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			

	[oav]	stw		o6,*optra++[in6]
||	[oav]	stw		o7,*optrb++[oav]
||			zero	o6
||			zero	o7
||			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			

			lddw	*cptr++[1],cf1:cf0
||			lddw	*sptra++[1],in1:in08			
||			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb
||			mvc		irp,oav

			lddw	*sptra++[1],in3:in2
||			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			lddw	*sptra++[1],in5:in4
||			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3			

			lddw	*sptra++[1],in7:in6
||			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5			

			ldw		*sptra--[6],in08
||			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7			

			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3

			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5

LOOP2
				lddw	*cptr++[1],cf1:cf0			
||				lddw	*sptra++[1],in1:in08
||				mpysp	in6,cf0,proda
||				mpysp	in7,cf0,prodb			
||				addsp	proda,o6,o6
||				addsp	prodb,o7,o7

				lddw	*sptra++[1],in3:in2
||				mpysp	in1,cf1,proda
||				mpysp	in2,cf1,prodb			
||				addsp	proda,o0,o0
||				addsp	prodb,o1,o1			
||		[tcnt]	sub		tcnt,2,tcnt
	
				lddw	*sptra++[1],in5:in4
||				mpysp	in3,cf1,proda
||				mpysp	in4,cf1,prodb			
||				addsp	proda,o2,o2
||				addsp	prodb,o3,o3			
||		[tcnt]	b		LOOP2

				lddw	*sptra++[1],in7:in6
||				mpysp	in5,cf1,proda
||				mpysp	in6,cf1,prodb			
||				addsp	proda,o4,o4
||				addsp	prodb,o5,o5			

				ldw		*sptra--[6],in08
||				mpysp	in7,cf1,proda
||				mpysp	in08,cf1,prodb			
||				addsp	proda,o6,o6
||				addsp	prodb,o7,o7			

				mpysp	in08,cf0,proda
||				mpysp	in1,cf0,prodb			
||				addsp	proda,o0,o0
||				addsp	prodb,o1,o1			

				mpysp	in2,cf0,proda
||				mpysp	in3,cf0,prodb			
||				addsp	proda,o2,o2
||				addsp	prodb,o3,o3

				mpysp	in4,cf0,proda
||				mpysp	in5,cf0,prodb			
||				addsp	proda,o4,o4
||				addsp	prodb,o5,o5

LOOP2_END

			lddw	*cptr++[1],cf1:cf0
||			lddw	*sptra++[1],in1:in08
||			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7

			lddw	*sptra++[1],in3:in2
||			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			
	
			lddw	*sptra++[1],in5:in4
||			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3			

			lddw	*sptra--[2],in7:in6
||			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5			

			ldw		*iptr,in08
||			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7			

			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3

			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5

			lddw	*cptr++[1],cf1:cf0			
||			lddw	*sptra++[1],in1:in08
||			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7

			lddw	*sptra++[1],in3:in2
||			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			
	
			lddw	*sptra--[1],in5:in4
||			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3			

			lddw	*iptr++[1],in7:in6
||			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5			

			ldw		*iptr--[2],in08
||			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7			

			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3

			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5

			lddw	*cptr++[1],cf1:cf0			
||			lddw	*sptra++[1],in1:in08
||			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7

			lddw	*sptra,in3:in2
||			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			
	
			lddw	*iptr++[1],in5:in4
||			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3			

			lddw	*iptr++[1],in7:in6
||			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5			

			ldw		*iptr--[4],in08
||			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7			

			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3

			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5
||			stw		in4,*sptrb++[1]

			lddw	*cptr++[1],cf1:cf0			
||			lddw	*sptra,in1:in08
||			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7

			lddw	*iptr++[1],in3:in2
||			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			
	
			lddw	*iptr++[1],in5:in4
||			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3			

			lddw	*iptr++[1],in7:in6
||			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5			

			ldw	*iptr++[1],in08
||			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7			

			ldw	*iptr++[1],in1
||			mpysp	in08,cf0,proda
||			mpysp	in1,cf0,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1			

			mpysp	in2,cf0,proda
||			mpysp	in3,cf0,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3
			
			mpysp	in4,cf0,proda
||			mpysp	in5,cf0,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5
||			stw		in3,*sptrb++[1]

			mpysp	in6,cf0,proda
||			mpysp	in7,cf0,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7
||			subaw	cptr,ntap,cptr
				
			mpysp	in1,cf1,proda
||			mpysp	in2,cf1,prodb			
||			addsp	proda,o0,o0
||			addsp	prodb,o1,o1
||			stw		in4,*sptrb++[1]

			mpysp	in3,cf1,proda
||			mpysp	in4,cf1,prodb			
||			addsp	proda,o2,o2
||			addsp	prodb,o3,o3
||			stw		in5,*sptrb++[1]

			mpysp	in5,cf1,proda
||			mpysp	in6,cf1,prodb			
||			addsp	proda,o4,o4
||			addsp	prodb,o5,o5
||			sub		scnt,8,scnt
||			stw		in6,*sptrb++[1]

LOOP1_BRANCH

			mpysp	in7,cf1,proda
||			mpysp	in08,cf1,prodb			
||			addsp	proda,o6,o6
||			addsp	prodb,o7,o7
||	 [scnt]	b		LOOP1
||			stw		in7,*sptrb++[1]

			addsp	proda,o0,o0
||			addsp	prodb,o1,o1
||			stw		in08,*sptrb++[1]

			addsp	proda,o2,o2
||			addsp	prodb,o3,o3
||			mv		offset,in08
||			stw		in1,*sptrb++[1]

			addsp	proda,o4,o4
||			addsp	prodb,o5,o5
||			subaw	sptra,in08,sptra

			addsp	proda,o6,o6
||			addsp	prodb,o7,o7
||			mv		oav,in6
	 
			stw		o0,*optra++[in6]
||			stw		o1,*optrb++[oav]

LOOP1_END

		stw		o2,*optra++[in6]
||		stw		o3,*optrb++[oav]

		stw		o4,*optra++[in6]
||		stw		o5,*optrb++[oav]

		stw		o6,*optra++[in6]
||		stw		o7,*optrb++[oav]

		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
	
		ldw		*++b15[1],b14
||		mvc		csr,b0	

		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
	
		ldw		*++b15[1],a12
||		set 	b0,0,0,b0	

		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
 		zero	a1
 		mvc		a1,amr
 		mvc		b0,csr	
		nop		1
