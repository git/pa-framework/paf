/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== firntap1ch.c ========
 *  FIR filter implementation for tap-N & channels-1.
 */
 
/************************ FIR FILTER *****************************************/
/****************** ORDER N    and CHANNELS 1 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2 + b3*z~3 + - - - -        */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + - - +                     */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,           x(n)   - *x                                     */
/*      b0 - filtCfsB[0], .. bk - filtCfsB[k]                                */
/*                                                                           */
/*                                                                           */
/*                        b0                                                 */
/*     x(n)-->----@------->>----[+]--->--y(n)                                */
/*                |              |                                           */
/*                v              ^                                           */
/*             +-----+    b1     |                                           */
/*             | Z~1 |---->>----[+]                                          */
/*             +-----+           |                                           */
/*                |              |                                           */
/*                v              ^                                           */
/*             +-----+    b2     |                                           */
/*             | Z~1 |---->>----[+]                                          */
/*             +-----+           |                                           */
/*                |                                                          */
/*                v              ^                                           */
/*                v              ^                                           */
/*                v              ^                                           */
/*                |                                                          */
/*             +-----+    bk     |                                           */
/*             | Z~1 |---->>-----                                            */
/*             +-----+                                                       */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include <filters.h>

/*
 *  ======== Filter_firTNCh1() ========
 *  FIR filter, taps-N & channels-1 
 */
/* Memory section for the function code */
Int Filter_firTNCh1( PAF_FilParam *pParam ) 
{
    Float * restrict ipPtr; /* input buffer */
    Float * restrict opPtr; /* output buffer */
    Float * restrict coef;  /* coefficient */
    Float * restrict var;   /* delay memory ptr */
    Int idx;                /* pointer to delay memory */
    Int taps;               /* length of Fir filter */
    Int samp;               /* sample count */
    /* Additional FIR param structure, pointed by 'use' */
    PAF_FilParamFir *fir_param = (PAF_FilParamFir *)(&pParam->use);

    Int count, t, k, i;       /* Loop counters */
    Float y0, y1, y2, y3;     /* Accumulators set-1 */
    Float y00, y11, y22, y33; /* Accumulators set-2 */
    
    Float d0, d1, d2, d3, d_1, d_2, d_3; /* Data registers */
    Float b0; /* Gain coefficient variable */
    Int adjPower2, offset;
    
    /* Get filter parameters into local variables */
    ipPtr = (Float *)pParam->pIn[0]; /* I/p sample ptr */
    opPtr = (Float *)pParam->pOut[0]; /* O/p sample ptr */
    coef  = (Float *)pParam->pCoef[0]; /* Coeff ptr */
    var   = (Float *)pParam->pVar[0]; /* Var memory ptr */
    samp  = pParam->sampleCount; /* I/p block sample length */
    taps  = (Int)fir_param->taps; /* Filter taps */
    idx   = (Int)fir_param->cIdx; /* Circular buffer current index */
    k     = idx; /* Temp cicular index */
    
    /* Get the no of 1's in taps-binary form */
    FilMacro_1sToRight(taps, 16, t, i);
    
    /* If tap is ^2 and sample count is *4 */
    if( ( t == 1 ) && ( !(samp&0x3) )  )
    {
        k--; /* Get oldest state. */
        taps--; /* Circular counter mask. */
    
    #pragma MUST_ITERATE(16, 2048, 4)
        for( count=0; count < samp; count += 4 )
        {
            /* Get the 4 oldest delays into regs, and replace with present x(n) */
            d3 = var[k & taps];
            var[(k--) & taps] = *ipPtr++;
            
            d2 = var[k & taps];
            var[(k--) & taps] = *ipPtr++;
            
            d1 = var[k & taps]; 
            var[(k--) & taps] = *ipPtr++;
            
            d0 = var[k & taps]; 
            var[(k--) & taps] = *ipPtr++; 
                 
            d_1 = var[(k--) & taps];        
            
            t = (taps+1) >> 1; /* Divide tap counter by 2 */
            
            /* Reset the y accumulators */
            y0 = 0; y1 = 0; y2 = 0; y3 = 0; 
            y00 = 0; y11 = 0; y22 = 0; y33 = 0; 
            
            coef += (taps + 1); /* Shift the coeff pointer to the end. */                      
            
            while(t--)
            {
                /* MAC for the coeff0, for 4 o/p samples at time. */
                y3 += (*coef)   * d0;
                y2 += (*coef)   * d1;
                y1 += (*coef)   * d2;
                y0 += (*coef--) * d3;
                
                /* MAC for the coeff1, for 4 o/p samples at time. */
                y33 += (*coef)   * d_1;
                y22 += (*coef)   * d0;
                y11 += (*coef)   * d1;
                y00 += (*coef--) * d2;
                
                /* Get the next two delays. */
                d_2 = var[(k--) & taps];
                d_3 = var[(k--) & taps];
                
                /* Update data registers, shifting by two */
                d3  = d1;
                d2  = d0;
                d1  = d_1;           
                d0  = d_2;
                d_1 = d_3;
    
            } /* while(t--) */
            
            
            k++; /* Required due to the extra 'post' decrement of k. */
            
            b0 = *coef; /* Gain coefficient. */
            
            /* Update the o/p's. */
            *opPtr++ = y0 + y00 + b0*d3;
            *opPtr++ = y1 + y11 + b0*d2;
            *opPtr++ = y2 + y22 + b0*d1;
            *opPtr++ = y3 + y33 + b0*d0;
            
        } /* for( count=0; count < samp; count += 4 ) */
        
        idx = (++k) & taps; /* Update the index. */
        
    } /* if( ( t == 1 ) && ( !(samp&0x3) )  ) */
    
    /* General case */
    else
    {
    /* State mem is made ^2, for easy circularing */
        /* count = bits from MSB bit to left most '1' */
        FilMacro_Left_1(taps, 16, count, i);
        
        adjPower2 = 0x8000 >> count; /* Get the ^2 just below 'taps' */
        /* If taps is not ^2 */
        if( taps & (~adjPower2) )
            adjPower2 <<= 1; /* Shift, to get next ^2 no, above taps */
        
        /* Offset between actual state mem length and ^2 length */    
        offset = adjPower2 - taps;
        adjPower2--; /* Circular mask */      
        
        k += (taps-1); /* Get the circular index to the oldest state. */    
        
        /* Do filtering for 4*x samples    */   
        for( count=0; count < (samp & 0xFFFFFFFC); count += 4 )
        {
            /* Get the 4 oldest delays into regs, and replace with present x(n) */
            d3 = var[k & adjPower2];
            var[(k+offset)&adjPower2] = *ipPtr++;
            k--;
            
            d2 = var[k & adjPower2];
            var[(k+offset)&adjPower2] = *ipPtr++;
            k--;
            
            d1 = var[k & adjPower2]; 
            var[(k+offset)&adjPower2] = *ipPtr++;
            k--;
            
            d0 = var[k & adjPower2]; 
            var[(k+offset)&adjPower2] = *ipPtr++;
            k--;
            
            d_1 = var[k & adjPower2];        
            k--;
            
            t = taps >> 1; /* Divide taps by 2 */
            
            /* Reset the y accumulators */
            y0 = 0; y1 = 0; y2 = 0; y3 = 0;
            y00 = 0; y11 = 0; y22 = 0; y33 = 0;
            
            coef += taps; /* Shift the coeff pointer to the end. */                   

            #pragma MUST_ITERATE(2,,);
            while(t--)
            {
                /* MAC for the coeff0, for 4 samples at time. */
                y3 += (*coef)   * d0; 
                y2 += (*coef)   * d1; 
                y1 += (*coef)   * d2; 
                y0 += (*coef--) * d3; 
                
                /* MAC for the coeff1, for 4 samples at time. */
                y33 += (*coef)   * d_1;
                y22 += (*coef)   * d0; 
                y11 += (*coef)   * d1; 
                y00 += (*coef--) * d2; 
                
                /* Shift part of the data registers */
                d3  = d1;
                d2  = d0;
                d1  = d_1;  

                /* Get the next two delays. */
                d_2 = var[(k--)&(adjPower2)];
                d_3 = var[(k--)&(adjPower2)];
                
                /* Shift the remaining data registers */
                d0  = d_2;
                d_1 = d_3;
    
            } /* while(t--) */
            
            
            k += (taps+1); /* Required due to extra 'post' decrement of k */
            
            /* If taps is odd, where fir MACs=taps + 1,kernel->taps-1 */
            if( taps & 0x1 )
            {
                k--;
                b0 = *coef--; /* Get coefficient b1 */
                
                /* Apply to the 4 o/p samples */
                y0 += b0*d3; 
                y1 += b0*d2; 
                y2 += b0*d1;
                y3 += b0*d0;

                /* Shift the data samples */
                d3 = d2;
                d2 = d1;
                d1 = d0;
                d0 = d_1;
            } /* if( taps & 0x1 ) */
                            
            /* Update the o/p's. */ 
            y0 += y00;
            y1 += y11;
            y2 += y22;
            y3 += y33;

            b0 = *coef; /* Get coefficient b0 */
            
            /* Apply to the 4 o/p samples */
            y0 += b0*d3;
            y1 += b0*d2;
            y2 += b0*d1;
            y3 += b0*d0;
            
            /* Put the o/p into its memory */
            *opPtr++ = y0;
            *opPtr++ = y1;
            *opPtr++ = y2;
            *opPtr++ = y3;
            
        } /* for( count=0; count < (samp & 0xFFFFFFFC); count += 4 ) */

        k += (offset+1); /* Rewind back to coefficient 'bn' */
        
        /* Filter the remaining s(<4) samples. */
        for( count = 0; count < (samp & 0x3); count++ )
        {
            y0 = 0; /* Reset accumulator */
            
            /* FIR filtering for taps */
            for( t = 0; t < taps; t++ )
                y0 += coef[t+1]*var[(k++)&adjPower2];
            
            k += (offset-1); /* Rewind the index to 'b1' */    
            var[k&adjPower2] = *ipPtr; /* Update the state mem */
            
            *opPtr++ = y0 + (*ipPtr++)*coef[0]; /* Store the o/p */
        }        
        
        idx = k & adjPower2; /* Update the index. */  
            
    } /* else */
  
    fir_param->cIdx = idx; /* Update the circular index */
    
    return(FIL_SUCCESS);
    
} /* Int Filter_firTNCh1() */





