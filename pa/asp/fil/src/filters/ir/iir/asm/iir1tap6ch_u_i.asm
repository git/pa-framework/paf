*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.3.0 *
;* Date/Time created: Mon Jul 03 12:13:42 2006                                *
;******************************************************************************

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C671x                                          *
;*   Optimization      : Enabled at level 3                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o3, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss

*
* $Source: /cvsstl/ti/pa/asp/fil/src/filters/ir/iir/asm/iir1tap6ch_u_i.asm,v $
* $Revision: 1.3 $
*
* IIR filter implementation for tap-1 & channels-6, unicoefficient/inplace, SP.
* DA6xx work around for MID 959
*
* Copyright 2002 by Texas Instruments Incorporated.
*
* $Log: iir1tap6ch_u_i.asm,v $
* Revision 1.3  2006/07/03 08:36:22  rvanga
* Really Fixing the build errors  MID 959.
*
* Revision 1.2  2006/07/03 07:44:19  rvanga
* Fixing the build errors in previous commit MID 959.
*
* Revision 1.1  2006/06/27 04:57:58  rvanga
* Initial version.
*
* Revision 1.3  2003/09/11 11:51:52  tjohn
* Made the file_ps header match the Coding Standards.
*

*
*  Copyright 2002 by Texas Instruments Incorporated.
*  All rights reserved. Property of Texas Instruments Incorporated.
*  Restricted rights to use, duplicate or disclose this code are
*  granted through contract.
*  
*
*
*  ======== iir1tap6ch_u_i.sa ========
*  IIR filter implementation for tap-1 & channels-6, unicoefficient/inplace, SP.
*
	.sect	".text"
 
************************ IIR FILTER *****************************************
****************** ORDER 1    and CHANNELS 7 ********************************
*                                                                           *
* Filter equation  : H(z) =  b0 + b1*z~1                                    *
*                           -------------                                   *
*                            1  - a1*z~1                                    *
*                                                                           *
*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 *
*                                                                           *
*   Canonical form : w(n) = x(n)    + a1*w(n-1)                             *
*                    y(n) = b0*w(n) + b1*w(n-1)                             *
*                                                                           *
*   Filter variables :                                                      *
*      y(n) - *y,         x(n)   - *x                                       *
*      w(n) -  w,         w(n-1) - w1                                       *
*      b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                 *
*                                                                           *
*                                                                           *
*                              w(n)    b0                                   *
*     x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |      a1   +-----+   b1      |                            *
*               [+]-----<<---| Z~1 |--->>-----[+]                           *
*                            +-----+                                        *
*                                                                           *
*****************************************************************************  
*Note:C equivalent of s-asm code lines are given as comments,for simplicity * 

            .global _Filter_iirT1Ch6_ui ;Declared as Global function 
            .sect ".text:Filter_iirT1Ch6_ui" ;Memory section for the function                         
	.sect	".text:Filter_iirT1Ch6_ui:_Filter_iirT1Ch6_ui"
	.clink

;******************************************************************************
;* FUNCTION NAME: Filter_iirT1Ch6_ui                                          *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP                                           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP                                        *
;******************************************************************************
_Filter_iirT1Ch6_ui:

    .asg B4, filtVars3
    .asg B6, filtVars3_p
    .asg A3, filtVars4
    .asg A5, filtVars4_p
    .asg B7, filtVars5
    .asg B5, filtVars5_p
    .asg A4, filtVars6
    .asg A5, filtVars6_p
    .asg A8, res1
    .asg A13, res1$1
    .asg B7, res1$2
    .asg B9, res1$3
    .asg A15, res1$4
    .asg A2, res1$5
    .asg A14, res1$6
    .asg B13, res1$7
    .asg B12, res1$8
    .asg A11, res1$9
    .asg B10, res1$10
    .asg B5, res1$11
    .asg A3, res1$12
    .asg B4, res1$13
    .asg A5, res1$14
    .asg B2, res2
    .asg A9, res2$1
    .asg B11, res2$2
    .asg A2, res2$3
    .asg A1, res2$4
    .asg A0, res2$5
    .asg A7, res2$6
    .asg B12, res2$7
    .asg A5, res2$8
    .asg B5, res2$9
    .asg A4, res2$10
    .asg A1, input_data
    .asg B10, input_data$1
    .asg A9, input_data$2
    .asg B2, input_data$3
    .asg B7, input_data$4
    .asg A11, input_data$5
    .asg B4, input_data$6
    .asg A6, input_data$7
    .asg B12, input_data$8
    .asg A3, input_data$9
    .asg B13, input_data$10
    .asg A4, input_data$11
    .asg A5, filtCfsA1
    .asg A5, filtCfsB1
    .asg A4, pParam
    .asg B2, accum
    .asg A9, accum$1
    .asg A2, accum$2
    .asg A1, accum$3
    .asg A0, accum$4
    .asg B4, accum$5
    .asg A3, accum$6
    .asg B9, accum$7
    .asg A4, accum$8
    .asg B6, accum$9
    .asg B7, accum$10
    .asg B0, count
    .asg A1, count_p
    .asg A4, count_p_p
    .asg A3, pCoef
    .asg B7, pIn
    .asg A7, x1
    .asg B10, x1_p
    .asg B6, x2
    .asg A11, x2_p
    .asg B3, x3
    .asg A10, x4
    .asg A5, CfsA0_1
    .asg B2, CfsA0_1_p
    .asg B1, x5
    .asg A12, x6
    .asg A4, CfsB0_1
    .asg B0, CfsB0_1_p
    .asg B9, CfsB1_1
    .asg A0, CfsB1_1$1
    .asg B5, CfsB1_1$2
    .asg B7, CfsB1_1$3
    .asg A6, pVar
    .asg B5, w_1
    .asg A0, w_1_p
    .asg B8, w_2
    .asg B4, w_3
    .asg A9, w_3_p
    .asg A6, w_4
    .asg B7, w_4_p
    .asg A8, w_5
    .asg B7, w_5$1
    .asg B9, w_5$2
    .asg B6, w_5$3
    .asg A3, w_6
    .asg B6, w_6_p
    .asg B5, filtVars1
    .asg B4, filtVars1_p
    .asg B5, filtVars2
    .asg B4, filtVars2_p


;** --------------------------------------------------------------------------*
; _Filter_iirT1Ch6_ui: .cproc pParam ;Filter_iirT1Ch6_ui(*pParam), C callable fn
;              .no_mdep ;no memory aliasing in this function
;              .reg  x1 ;I/p ptr-O/p ptr 
;              .reg  filtVars1 ;Var ptr 
;              .reg  filtCfsB1, filtCfsA1 ;Coeff ptr, common to channels
;              .reg  w_1, w1_1 ;Filter states    
;              .reg  x2 ;I/p ptr-O/p ptr 
;              .reg  filtVars2 ;Var ptr 
;              .reg  w_2, w1_2 ;Filter states    
;              .reg  x3 ;I/p ptr-O/p ptr 
;              .reg  filtVars3 ;Var ptr 
;              .reg  w_3, w1_3 ;Filter states    
;              .reg  x4 ;I/p ptr-O/p ptr 
;              .reg  filtVars4 ;Var ptr 
;              .reg  w_4, w1_4 ;Filter states    
;              .reg  x5 ;I/p ptr-O/p ptr 
;              .reg  filtVars5 ;Var ptr 
;              .reg  w_5, w1_5 ;Filter states    
;              .reg  x6 ;I/p ptr-O/p ptr 
;              .reg  filtVars6 ;Var ptr 
;              .reg  w_6, w1_6 ;Filter states    
;              .reg  res1, res2, res3 ;Temp accumulators
;              .reg  CfsB0_1, CfsB1_1, CfsA0_1 ;Coeff regs, common to channels
;              .reg  input_data ;I/p data reg
;              .reg  accum ;O/p accumulator register, common to channels
;              .reg  pIn ;Ptr to, array of ch i/p  ptrs
;              .reg  pCoef ;Ptr to, array of ch o/p  ptrs
;              .reg  pVar ;Ptr to, array of ch var  ptrs
;              .reg  count ;Sample counter

           LDW     .D1T1   *+pParam(12),pVar ; |100| pVar  = pParam[3]
||         MV      .L1X    SP,A9             ; |57| 

           STW     .D2T1   A11,*SP--(80)     ; |57| 
||         LDW     .D1T2   *pParam,pIn       ; |98| pIn   = pParam[0]

           LDW     .D1T1   *+pParam(8),pCoef ; |99| pCoef = pParam[2]
||         STW     .D2T2   B3,*+SP(72)       ; |57| 

           LDW     .D1T2   *+pParam(16),count ; |101| count = pParam[4]

           STW     .D1T1   A12,*-A9(40)      ; |57| 
||         STW     .D2T2   B10,*+SP(56)      ; |57| 

           LDW     .D1T2   *+pVar(4),filtVars2 ; |119| filtVars2 = pVar[1]        

           LDW     .D1T1   *+pVar(20),filtVars6 ; |123| filtVars6 = pVar[5]
||         LDW     .D2T2   *+pIn(4),x2       ; |104| x2 = pIn[1]

           LDW     .D1T1   *pCoef,filtCfsB1  ; |111| filtCfsB1 = pCoef[0] /* Common */
||         LDW     .D2T2   *+pIn(16),x5      ; |107| x5 = pIn[4]

           LDW     .D1T1   *+pVar(12),filtVars4 ; |121| filtVars4 = pVar[3]
||         LDW     .D2T2   *+pIn(8),x3       ; |105| x3 = pIn[2]
||         CMPGTU  .L2     count,1,B2

           LDW     .D2T1   *pIn,x1           ; |103| x1 = pIn[0]
||         LDW     .D1T2   *+pVar(8),filtVars3 ; |120| filtVars3 = pVar[2]
||         MV      .L1X    B2,A1             ; guard predicate rewrite

           STW     .D2T2   filtVars2,*+SP(8) ; |119| 
||         STW     .D1T1   A10,*-A9(4)       ; |57| 

           LDW     .D1T2   *pVar,filtVars1   ; |118| filtVars1 = pVar[0]  
||         LDW     .D2T1   *+pIn(20),x6      ; |108| x6 = pIn[5]

           LDW     .D2T1   *+pIn(12),x4      ; |106| x4 = pIn[3]
           STW     .D2T1   filtVars6,*+SP(20) ; |105| 
           STW     .D2T1   filtVars4,*+SP(12) ; |107| 

           STW     .D2T2   filtVars3,*+SP(4) ; |120| 
||         STW     .D1T1   A13,*-A9(36)      ; |57| 

           STW     .D2T2   filtVars1,*+SP(16) ; |106| 
||         STW     .D1T1   A14,*-A9(32)      ; |57| 

           LDW     .D1T2   *+pVar(16),filtVars5_p ; |122| filtVars5 = pVar[4]

           LDW     .D1T1   *filtVars4,w_4    ; |136| w4(n)   = filtVars4[0]  
||         LDW     .D2T2   *filtVars3,w_3    ; |133| w3(n)   = filtVars3[0]  

           LDW     .D1T1   *filtVars6,w_6    ; |142| w6(n)   = filtVars6[0]  
||         STW     .D2T2   B11,*+SP(60)      ; |57| 

           LDW     .D1T1   *filtCfsB1,CfsB0_1 ; |148| CfsB0_1 = filtCfsB1[0]
||         STW     .D2T2   B12,*+SP(64)      ; |57| 

           STW     .D1T1   A15,*-A9(28)      ; |57| 
||         STW     .D2T2   B13,*+SP(68)      ; |57| 

           STW     .D2T2   filtVars5_p,*+SP(24) ; |103| 

           LDW     .D1T2   *+filtCfsB1(4),CfsB1_1$2 ; |149| CfsB1_1 = filtCfsB1[1] 
||         ADD     .L1     0x8,filtCfsB1,filtCfsA1 ; |115| filtCfsA1=filtCfsB1+8     
|| [ A1]   MV      .S1X    w_3,w_3_p

           LDW     .D2T2   *+SP(24),filtVars5 ; |127| 
           LDW     .D1T1   *filtCfsA1,CfsA0_1 ; |146| CfsA0_1 = filtCfsA1[0]
           NOP             2
           STW     .D2T2   CfsB1_1$2,*+SP(28) ; |149| 
           LDW     .D2T2   *+SP(8),filtVars2 ; |133| 
           LDW     .D2T2   *filtVars5,w_5$1  ; |139| w5(n)   = filtVars5[0]  
           NOP             3
           LDW     .D2T2   *filtVars2,w_2    ; |130| w2(n)   = filtVars2[0]  

           LDW     .D2T2   *+SP(16),filtVars1 ; |130| 
|| [ B2]   B       .S1     $C$L2

           STW     .D2T2   w_5$1,*+SP(32)    ; |146| 
   [ B2]   LDW     .D2T1   *+SP(32),w_5
           NOP             2
           LDW     .D2T2   *filtVars1,w_1    ; |127| w1(n)   = filtVars1[0]  
           ; BRANCHCC OCCURS {$C$L2} 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L1
;** --------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_Filter_iirT1Ch6_ui$2$B:

           LDW     .D2T2   *+SP(32),w_5$1    ; |231| 
||         LDW     .D1T1   *x4,input_data$2  ; |213| input_data = *x4++    
||         MPYSP   .M1     w_4,CfsA0_1,res1$5 ; |209| res1 = w_4*CfsA0_1
||         MPYSP   .M2X    w_3,CfsA0_1,res1$7 ; |191| res1 = w_3*CfsA0_1
||         ADD     .L2     0xffffffff,count,count ; |262| count = count - 1

           LDW     .D2T1   *+SP(28),CfsB1_1$1
||         MPYSP   .M1     w_6,CfsA0_1,res1$1 ; |245| res1 = w_6*CfsA0_1
||         MPYSP   .M2X    w_2,CfsA0_1,res1$8 ; |173| res1 = w_2*CfsA0_1

           LDW     .D2T2   *x5,input_data$1  ; |231| input_data = *x5++    
||         LDW     .D1T1   *x6,input_data    ; |249| input_data = *x6++   

           LDW     .D2T2   *x3,input_data$3  ; |195| input_data = *x3++    
||         LDW     .D1T1   *x1,input_data$5  ; |159| input_data = *x1++      

           LDW     .D2T1   *+SP(32),w_5      ; |159| 
           MPYSP   .M2X    w_5$1,CfsA0_1,res1$3 ; |227| res1 = w_5*CfsA0_1
           MV      .L2X    A0,B7             ; |191| 

           MPYSP   .M2X    w_4,CfsB1_1$3,res2$2 ; |210| res2 = w_4*CfsB1_1
||         LDW     .D2T2   *x2,input_data$4  ; |177| input_data = *x2++   
||         ADDSP   .L1     input_data$2,res1$5,w_4 ; |216| w_4 = input_data + res1
||         MPYSP   .M1X    w_3,CfsB1_1$1,res2$3 ; |192| res2 = w_3*CfsB1_1

           ADDSP   .L2     input_data$3,res1$7,w_3 ; |198| w_3 = input_data + res1

           ADDSP   .L2     input_data$1,res1$3,w_5$2 ; |234| w_5 = input_data + res1
||         MPYSP   .M2X    w_1,CfsA0_1,res1$10 ; |155| res1 = w_1*CfsA0_1
||         MPYSP   .M1     w_5,CfsB1_1$1,res2$1 ; |228| res2 = w_5*CfsB1_1

           NOP             1
           MPYSP   .M1     w_4,CfsB0_1,res1$4 ; |219| res1  = w_4*CfsB0_1          
           MPYSP   .M1X    w_3,CfsB0_1,res1$6 ; |201| res1  = w_3*CfsB0_1          

           MV      .L2X    A0,B9             ; |198| 
||         STW     .D2T2   w_5$2,*+SP(32)    ; |234| 

           LDW     .D2T2   *+SP(32),w_5$1    ; |219| 
||         ADDSP   .L2     input_data$4,res1$8,w_2 ; |180| w_2 = input_data + res1
||         MPYSP   .M1X    w_2,CfsB1_1$1,res2$4 ; |174| res2 = w_2*CfsB1_1
||         ADDSP   .L1     input_data,res1$1,w_6 ; |252| w_6 = input_data + res1
||         MPYSP   .M2X    w_6,CfsB1_1,res2  ; |246| res2 = w_6*CfsB1_1

           ADDSP   .L2X    input_data$5,res1$10,w_1 ; |162| w_1 = input_data + res1
||         MPYSP   .M1X    w_1,CfsB1_1$1,res2$5 ; |156| res2 = w_1*CfsB1_1

           ADDSP   .L1     res1$6,res2$3,accum$2 ; |202| accum = res1 + res2
||         ADDSP   .L2X    res1$4,res2$2,accum$7 ; |220| accum = res1 + res2

           NOP             1
           MPYSP   .M1     w_6,CfsB0_1,res1  ; |255| res1  = w_6*CfsB0_1          

           MPYSP   .M2X    w_5$1,CfsB0_1,res1$2 ; |237| res1  = w_5*CfsB0_1          
||         MPYSP   .M1X    w_1,CfsB0_1,res1$9 ; |165| res1  = w_1*CfsB0_1          

           MPYSP   .M1X    w_2,CfsB0_1,res1$1 ; |183| res1  = w_2*CfsB0_1          
||         STW     .D2T1   accum$2,*x3++     ; |205| *x3++ = accum
||         STW     .D1T2   accum$7,*x4++     ; |223| *x4++ = accum

           NOP             1
           ADDSP   .L2X    res1,res2,accum   ; |256| accum = res1 + res2   
           ADDSP   .L1X    res1$2,res2$1,accum$1 ; |238| accum = res1 + res2

   [ count] B      .S1     $C$L1             ; |263|  if(count != 0) goto LOOP                          
||         ADDSP   .L1     res1$1,res2$4,accum$3 ; |184| accum = res1 + res2

   [!count] B      .S1     $C$L6
||         ADDSP   .L1     res1$9,res2$5,accum$4 ; |166| accum = res1 + res2

           STW     .D1T2   accum,*x6++       ; |259| *x6++ = accum
           STW     .D2T1   accum$1,*x5++     ; |241| *x5++ = accum
           STW     .D2T1   accum$3,*x2++     ; |187| *x2++ = accum
           STW     .D1T1   accum$4,*x1++     ; |169| *x1++ = accum
           ; BRANCHCC OCCURS {$C$L1}         ; |263| 
$C$DW$L$_Filter_iirT1Ch6_ui$2$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(24),filtVars5 ; |274| 
           ; BRANCH OCCURS {$C$L6} 
;** --------------------------------------------------------------------------*
$C$L2:    
           MVC     .S2     CSR,B4

           MV      .L2X    w_4,w_4_p
||         LDW     .D2T1   *x5,input_data$7  ; |231| (P) <0,0> input_data = *x5++    
||         MV      .L1X    x2,x2_p
||         LDW     .D1T2   *x4,input_data$8  ; |213| (P) <0,2> input_data = *x4++    

           MV      .L1X    B4,A13
||         AND     .L2     -2,B4,B4
||         MV      .S2X    w_6,w_6_p
||         MPYSP   .M1     w_5,CfsA0_1,res1$12 ; |227| 
||         LDW     .D2T2   *+SP(28),CfsB1_1

           MVC     .S2     B4,CSR            ; interrupts off
||         MV      .L2X    CfsA0_1,CfsA0_1_p

           MV      .L1X    w_1,w_1_p
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 155
;*      Loop closing brace source line   : 263
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 8
;*      Unpartitioned Resource Bound     : 9
;*      Partitioned Resource Bound(*)    : 9
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     6        6     
;*      .S units                     0        1     
;*      .D units                     6        6     
;*      .M units                     9*       9*    
;*      .X cross paths               9*       0     
;*      .T address paths             6        6     
;*      Long read paths              3        3     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             3        4     
;*      Bound(.L .S .D .LS .LSD)     5        5     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 9  Schedule found with 3 iterations in parallel
;*      Done
;*
;*      Epilog not entirely removed
;*      Collapsed epilog stages     : 1
;*
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh8
;*
;*      Minimum safe trip count     : 2
;*----------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP PROLOG

           SUB     .L1X    count,1,count_p
||         LDW     .D1T2   *x2_p,input_data$10 ; |177| (P) <0,0> input_data = *x2++   
||         MPYSP   .M2     w_4_p,CfsA0_1_p,res1$13 ; |209| (P) <0,3>  ^ res1 = w_4*CfsA0_1
||         LDW     .D2T1   *x3,input_data$9  ; |195| (P) <0,3> input_data = *x3++    

           MV      .L2X    x1,x1_p
||         MPYSP   .M1X    w_3_p,CfsA0_1_p,res1$14 ; |191| (P) <0,4>  ^ res1 = w_3*CfsA0_1
||         MPYSP   .M2     w_2,CfsA0_1_p,res1$13 ; |173| (P) <0,4>  ^ res1 = w_2*CfsA0_1

           SUB     .L1X    count,1,A2
||         MV      .L2X    CfsB0_1,CfsB0_1_p
||         LDW     .D2T1   *x1_p,input_data$11 ; |159| (P) <0,5> input_data = *x1++      

           LDW     .D1T2   *x6,input_data$6  ; |249| (P) <0,6> input_data = *x6++   
||         MPYSP   .M1X    w_1_p,CfsA0_1_p,res1$14 ; |155| (P) <0,6>  ^ res1 = w_1*CfsA0_1
||         ADDSP   .L1     input_data$7,res1$12,w_5 ; |234| (P) <0,6>  ^ w_5 = input_data + res1

           MPYSP   .M1X    w_5,CfsB1_1,res2$6 ; |228| (P) <0,7> res2 = w_5*CfsB1_1
||         ADDSP   .L2     input_data$8,res1$13,w_4_p ; |216| (P) <0,7>  ^ w_4 = input_data + res1
||         MPYSP   .M2     w_6_p,CfsA0_1_p,res1$11 ; |245| (P) <0,7>  ^ res1 = w_6*CfsA0_1

           MPYSP   .M1X    w_3_p,CfsB1_1,res2$8 ; |192| (P) <0,8> res2 = w_3*CfsB1_1
||         ADDSP   .L2     input_data$10,res1$13,w_2 ; |180| (P) <0,8>  ^ w_2 = input_data + res1
||         ADDSP   .L1     input_data$9,res1$14,w_3_p ; |198| (P) <0,8>  ^ w_3 = input_data + res1
||         MPYSP   .M2     w_2,CfsB1_1,res2$9 ; |174| (P) <0,8> res2 = w_2*CfsB1_1

           MPYSP   .M2     w_4_p,CfsB1_1,res2$7 ; |210| (P) <0,9> res2 = w_4*CfsB1_1
||         LDW     .D1T2   *+x2_p(4),input_data$10 ; |177| (P) <1,0> input_data = *x2++   
||         LDW     .D2T1   *+x5(4),input_data$7 ; |231| (P) <1,0> input_data = *x5++    
||         MPYSP   .M1X    w_1_p,CfsB1_1,res2$10 ; |156| (P) <0,9> res2 = w_1*CfsB1_1

           MPYSP   .M2     w_6_p,CfsB1_1,res2$2 ; |246| (P) <0,10> res2 = w_6*CfsB1_1
||         ADDSP   .L1     input_data$11,res1$14,w_1_p ; |162| (P) <0,10>  ^ w_1 = input_data + res1
||         MPYSP   .M1X    w_5,CfsB0_1_p,res1$12 ; |237| (P) <0,10> res1  = w_5*CfsB0_1          

           ADDSP   .L2     input_data$6,res1$11,w_6_p ; |252| (P) <0,11>  ^ w_6 = input_data + res1
||         MPYSP   .M1X    w_5,CfsA0_1_p,res1$12 ; |227| (P) <1,2>  ^ res1 = w_5*CfsA0_1
||         LDW     .D1T2   *+x4(4),input_data$8 ; |213| (P) <1,2> input_data = *x4++    
||         MPYSP   .M2     w_4_p,CfsB0_1_p,res1$13 ; |219| (P) <0,11> res1  = w_4*CfsB0_1          

           MPYSP   .M2     w_4_p,CfsA0_1_p,res1$13 ; |209| (P) <1,3>  ^ res1 = w_4*CfsA0_1
||         LDW     .D2T1   *+x3(4),input_data$9 ; |195| (P) <1,3> input_data = *x3++    
||         MPYSP   .M1X    w_3_p,CfsB0_1_p,res1$12 ; |201| (P) <0,12> res1  = w_3*CfsB0_1          

           MPYSP   .M1X    w_3_p,CfsA0_1_p,res1$14 ; |191| (P) <1,4>  ^ res1 = w_3*CfsA0_1
||         MPYSP   .M2     w_2,CfsA0_1_p,res1$13 ; |173| (P) <1,4>  ^ res1 = w_2*CfsA0_1

           SUB     .S1     A2,1,A2           ; init epilog collapse predicate
||         MPYSP   .M2     w_2,CfsB0_1_p,res1$13 ; |183| (P) <0,14> res1  = w_2*CfsB0_1          
||         MPYSP   .M1X    w_1_p,CfsB0_1_p,res1$14 ; |165| (P) <0,14> res1  = w_1*CfsB0_1          
||         LDW     .D2T1   *+x1_p(4),input_data$11 ; |159| (P) <1,5> input_data = *x1++      
||         ADDSP   .L1     res1$12,res2$6,accum$6 ; |238| (P) <0,14> accum = res1 + res2

;** --------------------------------------------------------------------------*
$C$L4:    ; PIPED LOOP KERNEL
$C$DW$L$_Filter_iirT1Ch6_ui$6$B:

           MPYSP   .M2     w_6_p,CfsB0_1_p,res1$11 ; |255| <0,15> res1  = w_6*CfsB0_1          
||         ADDSP   .L2     res1$13,res2$7,accum$5 ; |220| <0,15> accum = res1 + res2
||         LDW     .D1T2   *+x6(4),input_data$6 ; |249| <1,6> input_data = *x6++   
||         ADDSP   .L1     input_data$7,res1$12,w_5 ; |234| <1,6>  ^ w_5 = input_data + res1
||         MPYSP   .M1X    w_1_p,CfsA0_1_p,res1$14 ; |155| <1,6>  ^ res1 = w_1*CfsA0_1

           ADDSP   .L1     res1$12,res2$8,accum$6 ; |202| <0,16> accum = res1 + res2
||         MPYSP   .M1X    w_5,CfsB1_1,res2$6 ; |228| <1,7> res2 = w_5*CfsB1_1
||         MPYSP   .M2     w_6_p,CfsA0_1_p,res1$11 ; |245| <1,7>  ^ res1 = w_6*CfsA0_1
||         ADDSP   .L2     input_data$8,res1$13,w_4_p ; |216| <1,7>  ^ w_4 = input_data + res1

   [ count_p] ADD   .S1     0xffffffff,count_p,count_p ; |262| <0,17> count = count - 1
||         MPYSP   .M1X    w_3_p,CfsB1_1,res2$8 ; |192| <1,8> res2 = w_3*CfsB1_1
||         MPYSP   .M2     w_2,CfsB1_1,res2$9 ; |174| <1,8> res2 = w_2*CfsB1_1
||         ADDSP   .L2     input_data$10,res1$13,w_2 ; |180| <1,8>  ^ w_2 = input_data + res1
||         ADDSP   .L1     input_data$9,res1$14,w_3_p ; |198| <1,8>  ^ w_3 = input_data + res1

           ADDSP   .L2     res1$13,res2$9,accum$5 ; |184| <0,18> accum = res1 + res2
|| [ count_p] B     .S2     $C$L4             ; |263| <0,18>  if(count != 0) goto LOOP                          
||         ADDSP   .L1     res1$14,res2$10,accum$6 ; |166| <0,18> accum = res1 + res2
||         MPYSP   .M1X    w_1_p,CfsB1_1,res2$10 ; |156| <1,9> res2 = w_1*CfsB1_1
||         MPYSP   .M2     w_4_p,CfsB1_1,res2$7 ; |210| <1,9> res2 = w_4*CfsB1_1
|| [ A2]   LDW     .D1T2   *+x2_p(8),input_data$10 ; |177| <2,0> input_data = *x2++   
|| [ A2]   LDW     .D2T1   *+x5(8),input_data$7 ; |231| <2,0> input_data = *x5++    

           ADDSP   .L2     res1$11,res2$2,accum$5 ; |256| <0,19> accum = res1 + res2   
||         STW     .D2T1   accum$6,*x5++     ; |241| <0,19> *x5++ = accum
||         STW     .D1T2   accum$5,*x4++     ; |223| <0,19> *x4++ = accum
||         MPYSP   .M2     w_6_p,CfsB1_1,res2$2 ; |246| <1,10> res2 = w_6*CfsB1_1
||         MPYSP   .M1X    w_5,CfsB0_1_p,res1$12 ; |237| <1,10> res1  = w_5*CfsB0_1          
||         ADDSP   .L1     input_data$11,res1$14,w_1_p ; |162| <1,10>  ^ w_1 = input_data + res1

           STW     .D2T1   accum$6,*x3++     ; |205| <0,20> *x3++ = accum
||         ADDSP   .L2     input_data$6,res1$11,w_6_p ; |252| <1,11>  ^ w_6 = input_data + res1
||         MPYSP   .M2     w_4_p,CfsB0_1_p,res1$13 ; |219| <1,11> res1  = w_4*CfsB0_1          
||         MPYSP   .M1X    w_5,CfsA0_1_p,res1$12 ; |227| <2,2>  ^ res1 = w_5*CfsA0_1
|| [ A2]   LDW     .D1T2   *+x4(4),input_data$8 ; |213| <2,2> input_data = *x4++    

           MPYSP   .M1X    w_3_p,CfsB0_1_p,res1$12 ; |201| <1,12> res1  = w_3*CfsB0_1          
||         MPYSP   .M2     w_4_p,CfsA0_1_p,res1$13 ; |209| <2,3>  ^ res1 = w_4*CfsA0_1
|| [ A2]   LDW     .D2T1   *+x3(4),input_data$9 ; |195| <2,3> input_data = *x3++    

           STW     .D1T2   accum$5,*x2_p++    ; |187| <0,22> *x2++ = accum
||         STW     .D2T1   accum$6,*x1_p++    ; |169| <0,22> *x1++ = accum
||         MPYSP   .M2     w_2,CfsA0_1_p,res1$13 ; |173| <2,4>  ^ res1 = w_2*CfsA0_1
||         MPYSP   .M1X    w_3_p,CfsA0_1_p,res1$14 ; |191| <2,4>  ^ res1 = w_3*CfsA0_1

   [ A2]   SUB     .S1     A2,1,A2           ; <0,23> 
||         STW     .D1T2   accum$5,*x6++     ; |259| <0,23> *x6++ = accum
||         MPYSP   .M2     w_2,CfsB0_1_p,res1$13 ; |183| <1,14> res1  = w_2*CfsB0_1          
||         ADDSP   .L1     res1$12,res2$6,accum$6 ; |238| <1,14> accum = res1 + res2
||         MPYSP   .M1X    w_1_p,CfsB0_1_p,res1$14 ; |165| <1,14> res1  = w_1*CfsB0_1          
|| [ A2]   LDW     .D2T1   *+x1_p(4),input_data$11 ; |159| <2,5> input_data = *x1++      

$C$DW$L$_Filter_iirT1Ch6_ui$6$E:
;** --------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP EPILOG

           MV      .L1X    w_4_p,w_4
||         STW     .D2T1   w_5,*+SP(32)
||         MV      .S2X    A13,B9
||         ADDSP   .L2     res1$13,res2$7,accum$10 ; |220| (E) <2,15> accum = res1 + res2
||         MPYSP   .M2     w_6_p,CfsB0_1_p,res1$13 ; |255| (E) <2,15> res1  = w_6*CfsB0_1          

           MV      .L2X    count_p,count
||         ADDSP   .L1     res1$12,res2$8,accum$8 ; |202| (E) <2,16> accum = res1 + res2

           NOP             1

           MV      .S2X    w_1_p,w_1
||         ADDSP   .L1     res1$14,res2$10,accum$8 ; |166| (E) <2,18> accum = res1 + res2
||         ADDSP   .L2     res1$13,res2$9,accum$9 ; |184| (E) <2,18> accum = res1 + res2

           MV      .S2X    w_3_p,w_3
||         MV      .S1X    w_6_p,w_6
||         STW     .D2T1   accum$6,*x5++     ; |241| (E) <2,19> *x5++ = accum
||         STW     .D1T2   accum$10,*x4++    ; |223| (E) <2,19> *x4++ = accum
||         ADDSP   .L2     res1$13,res2$2,accum$9 ; |256| (E) <2,19> accum = res1 + res2   

           STW     .D2T1   accum$8,*x3++     ; |205| (E) <2,20> *x3++ = accum
           NOP             1

           MVC     .S2     B9,CSR            ; interrupts on
||         STW     .D1T2   accum$9,*x2_p++    ; |187| (E) <2,22> *x2++ = accum
||         STW     .D2T1   accum$8,*x1_p++    ; |169| (E) <2,22> *x1++ = accum

;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(24),filtVars5 ; |274| 
           STW     .D1T2   accum$9,*x6++     ; |259| (E) <2,23> *x6++ = accum
;** --------------------------------------------------------------------------*
$C$L6:    

           LDW     .D2T2   *+SP(32),w_5$3    ; |274| 
||         MV      .L1X    SP,A9             ; |276| 
             
           LDW     .D2T2   *+SP(72),B3       ; |276| 
||         LDDW    .D1T1   *+A9(40),A13:A12  ; |276| 
||         MV      .L1X    count,count_p_p     ; |274| 

           LDW     .D2T1   *+SP(12),filtVars4_p ; |269| 

           LDDW    .D1T1   *+A9(48),A15:A14  ; |276| 
||         LDDW    .D2T2   *+SP(56),B11:B10  ; |276| 

           LDW     .D1T1   *+A9(76),A10      ; |276| 
||         LDDW    .D2T2   *+SP(64),B13:B12  ; |276| 

           STW     .D2T2   w_5$3,*filtVars5  ; |271| filtVars5[0] = w_5
           LDW     .D2T2   *+SP(4),filtVars3_p ; |271| 
           STW     .D1T1   w_4,*filtVars4_p   ; |270| filtVars4[0] = w_4
           LDW     .D2T1   *+SP(20),filtVars6_p ; |268| 
           NOP             2
           STW     .D2T2   w_3,*filtVars3_p   ; |269| filtVars3[0] = w_3      
           LDW     .D2T2   *+SP(8),filtVars2_p ; |269| 
           STW     .D1T1   w_6,*filtVars6_p   ; |272| filtVars6[0] = w_6
           NOP             3
           STW     .D2T2   w_2,*filtVars2_p   ; |268| filtVars2[0] = w_2

           LDW     .D2T2   *+SP(16),filtVars1_p ; |268| 
||         RET     .S2     B3                ; |276| 

           LDW     .D2T1   *++SP(80),A11     ; |276| 
           NOP             3

           STW     .D2T2   w_1,*filtVars1_p   ; |267| filtVars1[0] = w_1
           ; BRANCH OCCURS {B3}              ; |276| 


;              .endproc ;C callable function, directive


