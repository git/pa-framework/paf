*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v5.3.0 *
;* Date/Time created: Mon Jul 03 12:13:48 2006                                *
;******************************************************************************

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C671x                                          *
;*   Optimization      : Enabled at level 3                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o3, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


*
* $Source: /cvsstl/ti/pa/asp/fil/src/filters/ir/iir/asm/iir1tap7ch_ui.asm,v $
* $Revision: 1.3 $
*
* IIR filter implementation for tap-1 & channels-7, unicoefficient/inplace, SP.
* DA6xx work around for MID 959
*
* Copyright 2002 by Texas Instruments Incorporated.
*
* $Log: iir1tap7ch_ui.asm,v $
* Revision 1.3  2006/07/03 08:36:30  rvanga
* Really Fixing the build errors  MID 959.
*
* Revision 1.2  2006/07/03 07:44:29  rvanga
* Fixing the build errors in previous commit MID 959.
*
* Revision 1.1  2006/06/27 04:58:16  rvanga
* Initial version.
*
* Revision 1.4  2003/09/11 11:51:52  tjohn
* Made the file_ps header match the Coding Standards.
*

*
*  Copyright 2002 by Texas Instruments Incorporated.
*  All rights reserved. Property of Texas Instruments Incorporated.
*  Restricted rights to use, duplicate or disclose this code are
*  granted through contract.
*  
*
*
*  ======== iir1tap7ch_ui.sa ========
*  IIR filter implementation for tap-1 & channels-7, unicoefficient/inplace, SP.
*
	.sect	".text"
 
************************ IIR FILTER *****************************************
****************** ORDER 1    and CHANNELS 7 ********************************
*                                                                           *
* Filter equation  : H(z) =  b0 + b1*z~1                                    *
*                           -------------                                   *
*                            1  - a1*z~1                                    *
*                                                                           *
*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 *
*                                                                           *
*   Canonical form : w(n) = x(n)    + a1*w(n-1)                             *
*                    y(n) = b0*w(n) + b1*w(n-1)                             *
*                                                                           *
*   Filter variables :                                                      *
*      y(n) - *y,         x(n)   - *x                                       *
*      w(n) -  w,         w(n-1) - w1                                       *
*      b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                 *
*                                                                           *
*                                                                           *
*                              w(n)    b0                                   *
*     x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |      a1   +-----+   b1      |                            *
*               [+]-----<<---| Z~1 |--->>-----[+]                           *
*                            +-----+                                        *
*                                                                           *
***************************************************************************** 
*Note:C equivalent of s-asm code lines are given as comments,for simplicity *

            .global _Filter_iirT1Ch7_ui ;Declared as Global function 
            .sect ".text:Filter_iirT1Ch7_ui" ;Memory section for the function           
	.sect	".text:Filter_iirT1Ch7_ui:_Filter_iirT1Ch7_ui"
	.clink

;******************************************************************************
;* FUNCTION NAME: Filter_iirT1Ch7_ui                                          *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP                                        *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP                                        *
;******************************************************************************
_Filter_iirT1Ch7_ui:

    .asg B6, filtVars3
    .asg B4, filtVars3_p
    .asg B7, filtVars4
    .asg B4, filtVars4_p
    .asg A3, filtVars5
    .asg A5, filtVars5_p
    .asg B9, filtVars6
    .asg B4, filtVars6_p
    .asg A7, filtVars7
    .asg A5, filtVars7_p
    .asg B3, res1
    .asg A1, res1$1
    .asg B6, res1$2
    .asg B0, res1$3
    .asg A9, res1$4
    .asg B13, res1$5
    .asg B9, res1$6
    .asg A8, res1$7
    .asg A7, res1$8
    .asg A3, res1$9
    .asg A5, res1$10
    .asg A4, res1$11
    .asg DP, res1$12
    .asg A6, res1$13
    .asg B8, res1$14
    .asg B13, res2
    .asg A0, res2$1
    .asg B8, res2$2
    .asg A4, res2$3
    .asg A15, res2$4
    .asg A14, res2$5
    .asg B7, res2$6
    .asg DP, res2$7
    .asg A7, res2$8
    .asg A14, input_data
    .asg A0, input_data$1
    .asg A7, input_data$2
    .asg B0, input_data$3
    .asg A8, input_data$4
    .asg A15, input_data$5
    .asg A3, input_data$6
    .asg B8, input_data$7
    .asg SP, input_data$8
    .asg B7, input_data$9
    .asg A4, input_data$10
    .asg B6, input_data$11
    .asg B1, input_data$12
    .asg A5, input_data$13
    .asg A5, filtCfsA1
    .asg A0, filtCfsB1
    .asg A4, pParam
    .asg B8, accum
    .asg A0, accum$1
    .asg B7, accum$2
    .asg A5, accum$3
    .asg A15, accum$4
    .asg A7, accum$5
    .asg A3, accum$6
    .asg A4, accum$7
    .asg B6, accum$8
    .asg A1, count
    .asg B4, count_p
    .asg B7, count_p_p
    .asg A3, pCoef
    .asg B5, pIn
    .asg A6, x1
    .asg B13, x1_p
    .asg A13, x2
    .asg B12, x3
    .asg B11, x4
    .asg A12, CfsA0_1
    .asg A11, x5
    .asg B10, x6
    .asg A10, x7
    .asg A2, CfsB0_1
    .asg B5, CfsB0_1_p
    .asg B4, CfsB1_1
    .asg A0, CfsB1_1_p
    .asg A5, CfsB1_1_p_p
    .asg A5, pVar
    .asg A3, w_1
    .asg B3, w_1_p
    .asg B8, w_1_p_p
    .asg B2, w_2
    .asg B1, w_3
    .asg B5, w_4
    .asg B0, w_4_p
    .asg B9, w_4_p_p
    .asg A9, w_5
    .asg B9, w_6
    .asg B6, w_6$1
    .asg B3, w_6$2
    .asg B5, w_6$3
    .asg A8, w_7
    .asg A3, w_7$1
    .asg A5, w_7$2
    .asg A7, w_7$3
    .asg A6, w_7$4
    .asg A4, w_7$5
    .asg A3, filtVars1
    .asg A6, filtVars1_p
    .asg A5, filtVars1_p_p
    .asg B8, filtVars2
    .asg B4, filtVars2_p

;** --------------------------------------------------------------------------*
; _Filter_iirT1Ch7_ui: .cproc pParam ;Filter_iirT1Ch7_ui(*pParam), C callable fn
;              .no_mdep ;no memory aliasing in this function
;              .reg  x1 ;I/p ptr-O/p ptr 
;              .reg  filtVars1 ;Var ptr 
;              .reg  filtCfsB1, filtCfsA1 ;Coeff ptr
;              .reg  w_1, w1_1 ;Filter states    
;              .reg  x2 ;I/p ptr-O/p ptr 
;              .reg  filtVars2 ;Var ptr 
;              .reg  w_2, w1_2 ;Filter states    
;              .reg  x3 ;I/p ptr-O/p ptr 
;              .reg  filtVars3 ;Var ptr 
;              .reg  w_3, w1_3 ;Filter states    
;              .reg  x4 ;I/p ptr-O/p ptr 
;              .reg  filtVars4 ;Var ptr 
;              .reg  w_4, w1_4 ;Filter states    
;              .reg  x5 ;I/p ptr-O/p ptr 
;              .reg  filtVars5 ;Var ptr 
;              .reg  w_5, w1_5 ;Filter states    
;              .reg  x6 ;I/p ptr-O/p ptr 
;              .reg  filtVars6 ;Var ptr 
;              .reg  w_6, w1_6 ;Filter states    
;              .reg  x7 ;I/p ptr-O/p ptr 
;              .reg  filtVars7 ;Var ptr 
;              .reg  w_7, w1_7 ;Filter states    
;              .reg  res1, res2, res3 ;Temp accumulators
;              .reg  CfsB0_1, CfsB1_1, CfsA0_1 ;Coeff regs, common to channels
;              .reg  input_data ;I/p data reg
;              .reg  accum ;O/p accumulator register, common to channels
;              .reg  pIn ;Ptr to, array of ch i/p  ptrs
;              .reg  pCoef ;Ptr to, array of ch o/p  ptrs
;              .reg  pVar ;Ptr to, array of ch var  ptrs
;              .reg  count ;Sample counter

           LDW     .D1T1   *+pParam(12),pVar ; |104| pVar  = pParam[3]
||         MV      .L1X    SP,A9             ; |57| 

           LDW     .D1T1   *+pParam(8),pCoef ; |103| pCoef = pParam[2]

           LDW     .D1T2   *+pParam(16),count_p ; |105| count = pParam[4]
||         STW     .D2T1   A11,*SP--(88)     ; |57| 

           LDW     .D1T2   *pParam,pIn       ; |102| pIn   = pParam[0]

           STW     .D1T1   A12,*-A9(40)      ; |57| 
||         STW     .D2T2   B10,*+SP(64)      ; |57| 

           LDW     .D1T1   *+pVar(24),filtVars7 ; |129| filtVars7 = pVar[6]
||         STW     .D2T2   B12,*+SP(72)      ; |57| 

           LDW     .D1T1   *pCoef,filtCfsB1  ; |116| filtCfsB1 = pCoef[0] /* Common */
||         STW     .D2T2   B11,*+SP(68)      ; |57| 

           LDW     .D1T1   *+pVar(16),filtVars5 ; |127| filtVars5 = pVar[4]
||         STW     .D2T2   count_p,*+SP(12)   ; |105| 

           LDW     .D1T2   *+pVar(12),filtVars4 ; |126| filtVars4 = pVar[3]
||         LDW     .D2T1   *+pIn(16),x5      ; |111| x5 = pIn[4]

           LDW     .D1T1   *pVar,filtVars1_p  ; |123| filtVars1 = pVar[0]  
||         LDW     .D2T2   *+pIn(20),x6      ; |112| x6 = pIn[5]

           STW     .D1T1   A13,*-A9(36)      ; |57| 
||         LDW     .D2T2   *+pIn(8),x3       ; |109| x3 = pIn[2]

           STW     .D1T1   A14,*-A9(32)      ; |57| 
||         LDW     .D2T2   *+pIn(12),x4      ; |110| x4 = pIn[3]

           STW     .D1T1   A15,*-A9(28)      ; |57| 
||         STW     .D2T2   B13,*+SP(76)      ; |57| 

           STW     .D1T1   A10,*-A9(4)       ; |57| 
||         STW     .D2T2   filtVars4,*+SP(28) ; |108| 

           LDW     .D1T1   *filtVars5,w_5    ; |145| w5(n)   = filtVars5[0]  
||         STW     .D2T2   B3,*+SP(80)       ; |57| 

           STW     .D2T1   filtVars5,*+SP(4) ; |127| 
||         LDW     .D1T2   *+pVar(20),filtVars6 ; |128| filtVars6 = pVar[5]

           LDW     .D1T1   *filtVars7,w_7$1  ; |151| w7(n)   = filtVars7[0]  

           STW     .D2T1   filtVars1_p,*+SP(24) ; |109| 
||         LDW     .D1T2   *+pVar(8),filtVars3 ; |125| filtVars3 = pVar[2]

           LDW     .D2T1   *pIn,x1           ; |107| x1 = pIn[0]
||         LDW     .D1T2   *+pVar(4),filtVars2 ; |124| filtVars2 = pVar[1]        
||         ADD     .L1     0x8,filtCfsB1,filtCfsA1 ; |120| filtCfsA1=filtCfsB1+8 

           LDW     .D2T1   *+pIn(24),x7      ; |113| x7 = pIn[6]
||         LDW     .D1T2   *+filtCfsB1(4),CfsB1_1 ; |158| CfsB1_1 = filtCfsB1[1]  

           LDW     .D2T1   *+pIn(4),x2       ; |108| x2 = pIn[1]

           LDW     .D2T2   *filtVars4,w_4    ; |142| w4(n)   = filtVars4[0]  
||         LDW     .D1T1   *filtCfsA1,CfsA0_1 ; |155| CfsA0_1 = filtCfsA1[0]

           LDW     .D2T2   *+SP(12),count_p_p
||         LDW     .D1T1   *filtCfsB1,CfsB0_1 ; |157| CfsB0_1 = filtCfsB1[0]

           STW     .D2T1   w_7$1,*+SP(36)    ; |139| 
           LDW     .D2T1   *+SP(24),filtVars1 ; |136| 
           STW     .D2T2   filtVars3,*+SP(8) ; |125| 
           LDW     .D2T2   *filtVars3,w_3    ; |139| w3(n)   = filtVars3[0]  

           CMPGTU  .L2     count_p_p,2,B0
||         LDW     .D2T2   *filtVars6,w_6$1  ; |148| w6(n)   = filtVars6[0]  

           STW     .D2T1   filtVars7,*+SP(20) ; |110| 
|| [ B0]   B       .S1     $C$L2

           LDW     .D1T1   *filtVars1,w_1    ; |133| w1(n)   = filtVars1[0]  
||         STW     .D2T2   filtVars6,*+SP(32) ; |107| 

           STW     .D2T2   filtVars2,*+SP(16) ; |111| 
           LDW     .D2T2   *filtVars2,w_2    ; |136| w2(n)   = filtVars2[0]  
           STW     .D2T2   w_6$1,*+SP(40)    ; |155| 
           NOP             1
           ; BRANCHCC OCCURS {$C$L2} 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L1
;** --------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_Filter_iirT1Ch7_ui$2$B:

           LDW     .D2T1   *+SP(36),w_7      ; |258| 
||         MPYSP   .M2X    w_1,CfsB1_1,res2$6 ; |165| res2 = w_1*CfsB1_1
||         MPYSP   .M1     w_1,CfsA0_1,res1$9 ; |164| res1 = w_1*CfsA0_1
||         MV      .L1X    CfsB1_1,CfsB1_1_p_p

           LDW     .D1T1   *x1,input_data$5  ; |168| input_data = *x1++  
||         LDW     .D2T2   *+SP(40),w_6$2    ; |258| 
||         MPYSP   .M2X    w_5,CfsB1_1,res2$2 ; |237| res2 = w_5*CfsB1_1
||         MPYSP   .M1     w_5,CfsA0_1,res1$4 ; |236| res1 = w_5*CfsA0_1

           LDW     .D1T1   *x5,input_data$1  ; |240| input_data = *x5++    
||         MPYSP   .M2X    w_4,CfsA0_1,res1$5 ; |218| res1 = w_4*CfsA0_1
||         LDW     .D2T2   *x6,input_data$3  ; |258| input_data = *x6++  
||         MPYSP   .M1X    w_4,CfsB1_1_p_p,res2$3 ; |219| res2 = w_4*CfsB1_1

           LDW     .D1T1   *x7,input_data    ; |276| input_data = *x7++    
||         MPYSP   .M2X    w_3,CfsA0_1,res1$6 ; |200| res1 = w_3*CfsA0_1

           LDW     .D2T1   *x4,input_data$2  ; |222| 

           MPYSP   .M1     w_7,CfsA0_1,res1$1 ; |272| res1 = w_7*CfsA0_1
||         LDW     .D1T1   *x2,input_data$4  ; |186| input_data = *x2++   

           ADDSP   .L1     input_data$5,res1$9,w_1 ; |171| w_1 = input_data + res1
||         MPYSP   .M1X    w_3,CfsB1_1_p_p,res2$4 ; |201| res2 = w_3*CfsB1_1
||         MPYSP   .M2X    w_6$2,CfsA0_1,res1$2 ; |254| res1 = w_6*CfsA0_1

           ADDSP   .L1     input_data$1,res1$4,w_5 ; |243| w_5 = input_data + res1
||         MPYSP   .M1X    w_6$2,CfsB1_1_p_p,res2$1 ; |255| res2 = w_6*CfsB1_1
||         STW     .D2T2   input_data$3,*+SP(44) ; |258| 
||         MPYSP   .M2X    w_2,CfsA0_1,res1  ; |182| res1 = w_2*CfsA0_1

           LDW     .D2T2   *x3,input_data$3  ; |204| input_data = *x3++  

           ADDSP   .L1     input_data,res1$1,w_7$2 ; |279| w_7 = input_data + res1
||         MPYSP   .M1X    w_2,CfsB1_1_p_p,res2$5 ; |183| res2 = w_2*CfsB1_1
||         ADDSP   .L2X    input_data$2,res1$5,w_4 ; |225| 
||         LDW     .D2T1   *+SP(36),w_7$3    ; |225| 

           LDW     .D2T2   *+SP(44),input_data$12 ; |225| 
           NOP             1
           ADDSP   .L2X    input_data$4,res1,w_2 ; |189| w_2 = input_data + res1

           MPYSP   .M1     w_5,CfsB0_1,res1$10 ; |246| res1  = w_5*CfsB0_1          
||         STW     .D2T1   w_7$2,*+SP(36)    ; |261| 

           LDW     .D2T1   *+SP(36),w_7      ; |228| 
||         MPYSP   .M1     w_1,CfsB0_1,res1$8 ; |174| res1  = w_1*CfsB0_1           
||         MPYSP   .M2X    w_7$3,CfsB1_1,res2 ; |273| res2 = w_7*CfsB1_1

           ADDSP   .L2     input_data$12,res1$2,w_6$1 ; |261| w_6 = input_data + res1
           ADDSP   .L2     input_data$3,res1$6,w_3 ; |207| w_3 = input_data + res1

           MPYSP   .M1X    w_4,CfsB0_1,res1$10 ; |228| res1  = w_4*CfsB0_1           
||         MV      .L2X    res1$10,res1$3    ; |246| 

           ADDSP   .L2     res1$3,res2$2,accum$2 ; |247| accum = res1 + res2
||         ADDSP   .L1X    res1$8,res2$6,accum$5 ; |175| accum = res1 + res2

           MPYSP   .M2X    w_6$1,CfsB0_1,res1$2 ; |264| res1  = w_6*CfsB0_1          
||         STW     .D2T2   w_6$1,*+SP(40)    ; |261| 
||         MPYSP   .M1     w_7,CfsB0_1,res1$7 ; |282| res1  = w_7*CfsB0_1           

           MPYSP   .M1X    w_3,CfsB0_1,res1$1 ; |210| res1  = w_3*CfsB0_1           
           ADDSP   .L1     res1$10,res2$3,accum$3 ; |229| accum = res1 + res2
           STW     .D1T2   accum$2,*x5++     ; |250| *x5++ = accum

           STW     .D1T1   accum$5,*x1++     ; |178| *x1++ = accum
||         MPYSP   .M1X    w_2,CfsB0_1,res1$7 ; |192| res1  = w_2*CfsB0_1          
||         MV      .L2X    res1$7,res1       ; |282| 
||         LDW     .D2T2   *+SP(12),count_p_p  ; |214| 

           ADDSP   .L2     res1,res2,accum   ; |283| accum = res1 + res2   
||         ADDSP   .L1     res1$1,res2$4,accum$4 ; |211| accum = res1 + res2

           STW     .D2T1   accum$3,*x4++     ; |232| *x4++ = accum
           ADDSP   .L1X    res1$2,res2$1,accum$1 ; |265| accum = res1 + res2    
           ADDSP   .L1     res1$7,res2$5,accum$5 ; |193| accum = res1 + res2

           STW     .D2T1   accum$4,*x3++     ; |214| *x3++ = accum
||         STW     .D1T2   accum,*x7++       ; |286| *x7++ = accum
||         ADD     .L2     0xffffffff,count_p_p,count_p_p ; |290| 
||         ADD     .S1X    0xffffffff,count_p_p,count ; |290| 

           STW     .D2T2   count_p_p,*+SP(12)  ; |196| 
|| [ count] B      .S1     $C$L1             ; |291|  if(count != 0) goto LOOP                        

   [!count] B      .S1     $C$L6
||         STW     .D2T1   accum$1,*x6++     ; |268| *x6++ = accum

           STW     .D1T1   accum$5,*x2++     ; |196| *x2++ = accum
           NOP             3
           ; BRANCHCC OCCURS {$C$L1}         ; |291| 
$C$DW$L$_Filter_iirT1Ch7_ui$2$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(28),filtVars4_p ; |303| 
           ; BRANCH OCCURS {$C$L6} 
;** --------------------------------------------------------------------------*
$C$L2:    

           LDW     .D2T1   *+SP(36),w_7$5
||         MVC     .S2     CSR,B8

           STW     .D2T2   B8,*+SP(36)
||         AND     .L2     -2,B8,B9
||         MVC     .S2     IRP,B8            ; save irp

           STW     .D2T2   DP,*+SP(12)
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 164
;*      Loop closing brace source line   : 291
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 8
;*      Unpartitioned Resource Bound     : 11
;*      Partitioned Resource Bound(*)    : 11
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     6        8     
;*      .S units                     1        0     
;*      .D units                     6        8     
;*      .M units                    11*      10     
;*      .X cross paths               8       11*    
;*      .T address paths             7        7     
;*      Long read paths              4        3     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             4        4     
;*      Bound(.L .S .D .LS .LSD)     5        6     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 11 Cannot allocate machine registers
;*                   Regs Live Always   :  7/6  (A/B-side)
;*                   Max Regs Live      : 15/18
;*                   Max Cond Regs Live :  1/0 
;*         ii = 11 Schedule found with 3 iterations in parallel
;*      Done
;*
;*      Epilog not removed
;*      Collapsed epilog stages     : 0
;*
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh8
;*
;*      Minimum safe trip count     : 3
;*----------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP PROLOG

           MV      .L2X    w_1,w_1_p_p
||         STW     .D2T2   B8,*+SP(40)
||         MVC     .S2     B9,CSR            ; interrupts off

           MV      .L2     w_4,w_4_p_p
||         MVC     .S2     SP,IRP            ; save sp
||         LDW     .D2T2   *x4,input_data$8  ; |222| (P) <0,0> input_data = *x4++    

           LDW     .D1T1   *x7,input_data$13 ; |276| (P) <0,0> input_data = *x7++    
||         MPYSP   .M2X    w_4_p_p,CfsA0_1,res1$3 ; |218| (P) <0,0>  ^ res1 = w_4*CfsA0_1

           MV      .L2X    x1,x1_p
||         MV      .S2     B6,B5
||         LDW     .D2T2   *x6,input_data$11 ; |258| (P) <0,2> input_data = *x6++  
||         MPYSP   .M1     w_7$5,CfsA0_1,res1$9 ; |272| (P) <0,1>  ^ res1 = w_7*CfsA0_1

           MPYSP   .M2X    B5,CfsA0_1,res1$3 ; |254| (P) <0,3>  ^ res1 = w_6*CfsA0_1

           MPYSP   .M2X    w_3,CfsA0_1,res1$12 ; |200| (P) <0,2>  ^ res1 = w_3*CfsA0_1
||         LDW     .D2T2   *x3,input_data$11 ; |204| (P) <0,4> input_data = *x3++  
||         LDW     .D1T1   *x5,input_data$6  ; |240| (P) <0,4> input_data = *x5++    

           MV      .L1X    CfsB1_1,CfsB1_1_p
||         LDW     .D1T1   *x2,input_data$6  ; |186| (P) <0,5> input_data = *x2++   
||         ADDSP   .L2     input_data$8,res1$3,w_4_p ; |225| (P) <0,5>  ^ w_4 = input_data + res1
||         MPYSP   .M1     w_5,CfsA0_1,res1$10 ; |236| (P) <0,5>  ^ res1 = w_5*CfsA0_1
||         MPYSP   .M2X    w_2,CfsA0_1,res1$6 ; |182| (P) <0,5>  ^ res1 = w_2*CfsA0_1

           MV      .L2X    CfsB0_1,CfsB0_1_p
||         LDW     .D2T2   *x1_p,input_data$9 ; |168| (P) <0,6> input_data = *x1++  
||         ADDSP   .L1     input_data$13,res1$9,w_7 ; |279| (P) <0,6>  ^ w_7 = input_data + res1
||         MPYSP   .M1X    B5,CfsB1_1_p,res2$8 ; |255| (P) <0,6> res2 = w_6*CfsB1_1

           ADDSP   .L2     input_data$11,res1$3,w_6 ; |261| (P) <0,7>  ^ w_6 = input_data + res1
||         MPYSP   .M1X    w_3,CfsB1_1_p,res2$4 ; |201| (P) <0,7> res2 = w_3*CfsB1_1
||         MPYSP   .M2X    w_1_p_p,CfsA0_1,res1$2 ; |164| (P) <0,7>  ^ res1 = w_1*CfsA0_1

           MPYSP   .M1X    w_4_p_p,CfsB1_1_p,res2$3 ; |219| (P) <0,8> res2 = w_4*CfsB1_1
||         MPYSP   .M2X    w_5,CfsB1_1,res2$2 ; |237| (P) <0,8> res2 = w_5*CfsB1_1

           MPYSP   .M2X    w_7$5,CfsB1_1,res2$7 ; |273| (P) <0,9> res2 = w_7*CfsB1_1
||         MPYSP   .M1X    w_4_p,CfsB0_1,res1$13 ; |228| (P) <0,9> res1  = w_4*CfsB0_1           
||         ADDSP   .L1     input_data$6,res1$10,w_5 ; |243| (P) <0,9>  ^ w_5 = input_data + res1
||         ADDSP   .L2     input_data$11,res1$12,w_3 ; |207| (P) <0,9>  ^ w_3 = input_data + res1

           SUB     .L1X    B7,2,count
||         ADDSP   .L2X    input_data$6,res1$6,w_2 ; |189| (P) <0,10>  ^ w_2 = input_data + res1
||         MPYSP   .M1     w_7,CfsB0_1,res1$10 ; |282| (P) <0,10> res1  = w_7*CfsB0_1           
||         MPYSP   .M2     w_1_p_p,CfsB1_1,res2$6 ; |165| (P) <0,10> res2 = w_1*CfsB1_1

           ADDSP   .L2     input_data$9,res1$2,w_1_p ; |171| (P) <0,11>  ^ w_1 = input_data + res1
||         LDW     .D1T1   *+x7(4),input_data$6 ; |276| (P) <1,0> input_data = *x7++    
||         MPYSP   .M2X    w_4_p,CfsA0_1,res1$2 ; |218| (P) <1,0>  ^ res1 = w_4*CfsA0_1
||         LDW     .D2T2   *+x4(4),input_data$8 ; |222| (P) <1,0> input_data = *x4++    
||         MPYSP   .M1X    w_2,CfsB1_1_p,res2$5 ; |183| (P) <0,11> res2 = w_2*CfsB1_1

           MPYSP   .M1     w_7,CfsA0_1,res1$11 ; |272| (P) <1,1>  ^ res1 = w_7*CfsA0_1
||         MPYSP   .M2X    w_6,CfsB0_1,res1$12 ; |264| (P) <0,12> res1  = w_6*CfsB0_1          

           MPYSP   .M1     w_5,CfsB0_1,res1$10 ; |246| (P) <0,13> res1  = w_5*CfsB0_1          
||         LDW     .D2T2   *+x6(4),input_data$7 ; |258| (P) <1,2> input_data = *x6++  
||         MPYSP   .M2X    w_3,CfsA0_1,res1$12 ; |200| (P) <1,2>  ^ res1 = w_3*CfsA0_1

           MPYSP   .M1X    w_3,CfsB0_1,res1$9 ; |210| (P) <0,14> res1  = w_3*CfsB0_1           
||         MPYSP   .M2X    w_6,CfsA0_1,res1$2 ; |254| (P) <1,3>  ^ res1 = w_6*CfsA0_1

;** --------------------------------------------------------------------------*
$C$L4:    ; PIPED LOOP KERNEL
$C$DW$L$_Filter_iirT1Ch7_ui$6$B:

           MPYSP   .M2     w_1_p,CfsB0_1_p,res1$2 ; |174| <0,15> res1  = w_1*CfsB0_1           
||         ADDSP   .L1     res1$13,res2$3,accum$7 ; |229| <0,15> accum = res1 + res2
||         ADDSP   .L2X    res1$10,res2$7,accum ; |283| <0,15> accum = res1 + res2   
||         MPYSP   .M1X    w_2,CfsB0_1,res1$10 ; |192| <0,15> res1  = w_2*CfsB0_1          
||         LDW     .D2T2   *+x3(4),input_data$9 ; |204| <1,4> input_data = *x3++  
||         LDW     .D1T1   *+x5(4),input_data$2 ; |240| <1,4> input_data = *x5++    

           ADDSP   .L1X    res1$12,res2$8,accum$6 ; |265| <0,16> accum = res1 + res2    
||         LDW     .D1T1   *+x2(4),input_data$10 ; |186| <1,5> input_data = *x2++   
||         MPYSP   .M2X    w_2,CfsA0_1,res1$2 ; |182| <1,5>  ^ res1 = w_2*CfsA0_1
||         ADDSP   .L2     input_data$8,res1$2,w_4_p ; |225| <1,5>  ^ w_4 = input_data + res1
||         MPYSP   .M1     w_5,CfsA0_1,res1$13 ; |236| <1,5>  ^ res1 = w_5*CfsA0_1

           ADDSP   .L2X    res1$10,res2$2,accum$2 ; |247| <0,17> accum = res1 + res2
||         MPYSP   .M1X    w_6,CfsB1_1_p,res2$8 ; |255| <1,6> res2 = w_6*CfsB1_1
||         LDW     .D2T2   *+x1_p(4),input_data$11 ; |168| <1,6> input_data = *x1++  
||         ADDSP   .L1     input_data$6,res1$11,w_7 ; |279| <1,6>  ^ w_7 = input_data + res1

           MPYSP   .M2X    w_1_p,CfsA0_1,res1$14 ; |164| <1,7>  ^ res1 = w_1*CfsA0_1
||         ADDSP   .L2     input_data$7,res1$2,w_6 ; |261| <1,7>  ^ w_6 = input_data + res1
||         MPYSP   .M1X    w_3,CfsB1_1_p,res2$4 ; |201| <1,7> res2 = w_3*CfsB1_1

   [ count] ADD    .S1     0xffffffff,count,count ; |290| <0,19> count = count - 1
||         ADDSP   .L2     res1$2,res2$6,accum$8 ; |175| <0,19> accum = res1 + res2
||         ADDSP   .L1     res1$9,res2$4,accum$6 ; |211| <0,19> accum = res1 + res2
||         MPYSP   .M2X    w_5,CfsB1_1,res2$2 ; |237| <1,8> res2 = w_5*CfsB1_1
||         MPYSP   .M1X    w_4_p,CfsB1_1_p,res2$3 ; |219| <1,8> res2 = w_4*CfsB1_1

           STW     .D2T1   accum$7,*x4++     ; |232| <0,20> *x4++ = accum
|| [ count] B      .S1     $C$L4             ; |291| <0,20>  if(count != 0) goto LOOP                        
||         MPYSP   .M2X    w_7,CfsB1_1,res2$7 ; |273| <1,9> res2 = w_7*CfsB1_1
||         ADDSP   .L2     input_data$9,res1$12,w_3 ; |207| <1,9>  ^ w_3 = input_data + res1
||         MPYSP   .M1X    w_4_p,CfsB0_1,res1$13 ; |228| <1,9> res1  = w_4*CfsB0_1           
||         ADDSP   .L1     input_data$2,res1$13,w_5 ; |243| <1,9>  ^ w_5 = input_data + res1

           STW     .D1T2   accum,*x7++       ; |286| <0,21> *x7++ = accum
||         STW     .D2T1   accum$6,*x6++     ; |268| <0,21> *x6++ = accum
||         ADDSP   .L1     res1$10,res2$5,accum$6 ; |193| <0,21> accum = res1 + res2
||         MPYSP   .M2     w_1_p,CfsB1_1,res2$6 ; |165| <1,10> res2 = w_1*CfsB1_1
||         ADDSP   .L2X    input_data$10,res1$2,w_2 ; |189| <1,10>  ^ w_2 = input_data + res1
||         MPYSP   .M1     w_7,CfsB0_1,res1$10 ; |282| <1,10> res1  = w_7*CfsB0_1           

           MPYSP   .M1X    w_2,CfsB1_1_p,res2$5 ; |183| <1,11> res2 = w_2*CfsB1_1
||         ADDSP   .L2     input_data$11,res1$14,w_1_p ; |171| <1,11>  ^ w_1 = input_data + res1
||         LDW     .D1T1   *+x7(4),input_data$6 ; |276| <2,0> input_data = *x7++    
||         MPYSP   .M2X    w_4_p,CfsA0_1,res1$2 ; |218| <2,0>  ^ res1 = w_4*CfsA0_1
||         LDW     .D2T2   *+x4(4),input_data$8 ; |222| <2,0> input_data = *x4++    

           STW     .D1T2   accum$2,*x5++     ; |250| <0,23> *x5++ = accum
||         STW     .D2T1   accum$6,*x3++     ; |214| <0,23> *x3++ = accum
||         MPYSP   .M2X    w_6,CfsB0_1,res1$12 ; |264| <1,12> res1  = w_6*CfsB0_1          
||         MPYSP   .M1     w_7,CfsA0_1,res1$11 ; |272| <2,1>  ^ res1 = w_7*CfsA0_1

           MPYSP   .M1     w_5,CfsB0_1,res1$10 ; |246| <1,13> res1  = w_5*CfsB0_1          
||         MPYSP   .M2X    w_3,CfsA0_1,res1$12 ; |200| <2,2>  ^ res1 = w_3*CfsA0_1
||         LDW     .D2T2   *+x6(4),input_data$7 ; |258| <2,2> input_data = *x6++  

           STW     .D2T2   accum$8,*x1_p++    ; |178| <0,25> *x1++ = accum
||         STW     .D1T1   accum$6,*x2++     ; |196| <0,25> *x2++ = accum
||         MPYSP   .M1X    w_3,CfsB0_1,res1$9 ; |210| <1,14> res1  = w_3*CfsB0_1           
||         MPYSP   .M2X    w_6,CfsA0_1,res1$2 ; |254| <2,3>  ^ res1 = w_6*CfsA0_1

$C$DW$L$_Filter_iirT1Ch7_ui$6$E:
;** --------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP EPILOG

           ADDSP   .L1     res1$13,res2$3,accum$7 ; |229| (E) <1,15> accum = res1 + res2
||         ADDSP   .L2X    res1$10,res2$7,accum ; |283| (E) <1,15> accum = res1 + res2   
||         MPYSP   .M1X    w_2,CfsB0_1,res1$10 ; |192| (E) <1,15> res1  = w_2*CfsB0_1          
||         LDW     .D2T2   *+x3(4),input_data$9 ; |204| (E) <2,4> input_data = *x3++  
||         LDW     .D1T1   *+x5(4),input_data$2 ; |240| (E) <2,4> input_data = *x5++    
||         MPYSP   .M2     w_1_p,CfsB0_1_p,res1$2 ; |174| (E) <1,15> res1  = w_1*CfsB0_1           

           MVC     .S2     IRP,SP            ; restore sp
||         ADDSP   .L1X    res1$12,res2$8,accum$6 ; |265| (E) <1,16> accum = res1 + res2    
||         MPYSP   .M2X    w_2,CfsA0_1,res1$2 ; |182| (E) <2,5>  ^ res1 = w_2*CfsA0_1
||         ADDSP   .L2     input_data$8,res1$2,w_4_p ; |225| (E) <2,5>  ^ w_4 = input_data + res1
||         MPYSP   .M1     w_5,CfsA0_1,res1$13 ; |236| (E) <2,5>  ^ res1 = w_5*CfsA0_1
||         LDW     .D1T1   *+x2(4),input_data$10 ; |186| (E) <2,5> input_data = *x2++   

           ADDSP   .L2X    res1$10,res2$2,accum$2 ; |247| (E) <1,17> accum = res1 + res2
||         LDW     .D2T2   *+x1_p(4),input_data$11 ; |168| (E) <2,6> input_data = *x1++  
||         ADDSP   .L1     input_data$6,res1$11,w_7 ; |279| (E) <2,6>  ^ w_7 = input_data + res1
||         MPYSP   .M1X    w_6,CfsB1_1_p,res2$8 ; |255| (E) <2,6> res2 = w_6*CfsB1_1

           MPYSP   .M2X    w_1_p,CfsA0_1,res1$14 ; |164| (E) <2,7>  ^ res1 = w_1*CfsA0_1
||         MPYSP   .M1X    w_3,CfsB1_1_p,res2$4 ; |201| (E) <2,7> res2 = w_3*CfsB1_1
||         ADDSP   .L2     input_data$7,res1$2,w_6 ; |261| (E) <2,7>  ^ w_6 = input_data + res1

           ADDSP   .L1     res1$9,res2$4,accum$6 ; |211| (E) <1,19> accum = res1 + res2
||         MPYSP   .M2X    w_5,CfsB1_1,res2$2 ; |237| (E) <2,8> res2 = w_5*CfsB1_1
||         MPYSP   .M1X    w_4_p,CfsB1_1_p,res2$3 ; |219| (E) <2,8> res2 = w_4*CfsB1_1
||         ADDSP   .L2     res1$2,res2$6,accum$8 ; |175| (E) <1,19> accum = res1 + res2

           STW     .D2T1   accum$7,*x4++     ; |232| (E) <1,20> *x4++ = accum
||         ADDSP   .L2     input_data$9,res1$12,w_3 ; |207| (E) <2,9>  ^ w_3 = input_data + res1
||         MPYSP   .M1X    w_4_p,CfsB0_1,res1$13 ; |228| (E) <2,9> res1  = w_4*CfsB0_1           
||         ADDSP   .L1     input_data$2,res1$13,w_5 ; |243| (E) <2,9>  ^ w_5 = input_data + res1
||         MPYSP   .M2X    w_7,CfsB1_1,res2$7 ; |273| (E) <2,9> res2 = w_7*CfsB1_1

           STW     .D1T2   accum,*x7++       ; |286| (E) <1,21> *x7++ = accum
||         ADDSP   .L1     res1$10,res2$5,accum$6 ; |193| (E) <1,21> accum = res1 + res2
||         MPYSP   .M2     w_1_p,CfsB1_1,res2$6 ; |165| (E) <2,10> res2 = w_1*CfsB1_1
||         ADDSP   .L2X    input_data$10,res1$2,w_2 ; |189| (E) <2,10>  ^ w_2 = input_data + res1
||         MPYSP   .M1     w_7,CfsB0_1,res1$10 ; |282| (E) <2,10> res1  = w_7*CfsB0_1           
||         STW     .D2T1   accum$6,*x6++     ; |268| (E) <1,21> *x6++ = accum

           LDW     .D2T2   *+SP(40),B4
||         MPYSP   .M1X    w_2,CfsB1_1_p,res2$5 ; |183| (E) <2,11> res2 = w_2*CfsB1_1
||         ADDSP   .L2     input_data$11,res1$14,w_1_p ; |171| (E) <2,11>  ^ w_1 = input_data + res1

           STW     .D1T2   accum$2,*x5++     ; |250| (E) <1,23> *x5++ = accum
||         MPYSP   .M2X    w_6,CfsB0_1,res1$12 ; |264| (E) <2,12> res1  = w_6*CfsB0_1          
||         STW     .D2T1   accum$6,*x3++     ; |214| (E) <1,23> *x3++ = accum

           STW     .D2T2   w_6,*+SP(40)
||         MPYSP   .M1     w_5,CfsB0_1,res1$10 ; |246| (E) <2,13> res1  = w_5*CfsB0_1          

           STW     .D2T2   accum$8,*x1_p++    ; |178| (E) <1,25> *x1++ = accum
||         MPYSP   .M1X    w_3,CfsB0_1,res1$9 ; |210| (E) <2,14> res1  = w_3*CfsB0_1           
||         STW     .D1T1   accum$6,*x2++     ; |196| (E) <1,25> *x2++ = accum

           MV      .S2     w_4_p,w_4
||         MPYSP   .M2     w_1_p,CfsB0_1_p,res1$2 ; |174| (E) <2,15> res1  = w_1*CfsB0_1           
||         ADDSP   .L2X    res1$10,res2$7,accum ; |283| (E) <2,15> accum = res1 + res2   
||         MPYSP   .M1X    w_2,CfsB0_1,res1$10 ; |192| (E) <2,15> res1  = w_2*CfsB0_1          
||         ADDSP   .L1     res1$13,res2$3,accum$7 ; |229| (E) <2,15> accum = res1 + res2

           MVC     .S2     B4,IRP            ; restore irp
||         LDW     .D2T2   *+SP(12),DP
||         ADDSP   .L1X    res1$12,res2$8,accum$6 ; |265| (E) <2,16> accum = res1 + res2    

           STW     .D2T1   count,*+SP(12)
||         ADDSP   .L2X    res1$10,res2$2,accum$2 ; |247| (E) <2,17> accum = res1 + res2

           NOP             1

           ADDSP   .L2     res1$2,res2$6,accum$8 ; |175| (E) <2,19> accum = res1 + res2
||         ADDSP   .L1     res1$9,res2$4,accum$6 ; |211| (E) <2,19> accum = res1 + res2

           STW     .D2T1   accum$7,*x4++     ; |232| (E) <2,20> *x4++ = accum

           STW     .D1T2   accum,*x7++       ; |286| (E) <2,21> *x7++ = accum
||         ADDSP   .L1     res1$10,res2$5,accum$6 ; |193| (E) <2,21> accum = res1 + res2
||         STW     .D2T1   accum$6,*x6++     ; |268| (E) <2,21> *x6++ = accum

           LDW     .D2T2   *+SP(36),B8

           STW     .D1T2   accum$2,*x5++     ; |250| (E) <2,23> *x5++ = accum
||         STW     .D2T1   accum$6,*x3++     ; |214| (E) <2,23> *x3++ = accum

;** --------------------------------------------------------------------------*
           STW     .D2T1   w_7,*+SP(36)
           LDW     .D2T2   *+SP(28),filtVars4_p ; |303| 
           STW     .D1T1   accum$6,*x2++     ; |196| (E) <2,25> *x2++ = accum

           MVC     .S2     B8,CSR            ; interrupts on
||         STW     .D2T2   accum$8,*x1_p++    ; |178| (E) <2,25> *x1++ = accum
||         MV      .L1X    w_1_p,w_1

;** --------------------------------------------------------------------------*
$C$L6:    

           LDW     .D2T1   *+SP(4),filtVars5_p ; |298| 
||         MV      .L1     A1,A4

           LDW     .D2T2   *+SP(80),B3       ; |305| 
           LDW     .D2T1   *+SP(36),w_7$4    ; |297| 
           LDDW    .D2T2   *+SP(64),B11:B10  ; |305| 
           STW     .D2T2   w_4,*filtVars4_p   ; |298| filtVars4[0] = w_4

           LDW     .D2T2   *+SP(8),filtVars3_p ; |298| 
||         STW     .D1T1   w_5,*filtVars5_p   ; |299| filtVars5[0] = w_5
||         MV      .L1X    SP,A9             ; |305| 

           LDW     .D2T1   *+SP(20),filtVars7_p ; |297| 

           LDW     .D2T2   *+SP(40),w_6$3    ; |296| 
||         LDDW    .D1T1   *+A9(48),A13:A12  ; |305| 

           LDDW    .D1T1   *+A9(56),A15:A14  ; |305| 
||         LDDW    .D2T2   *+SP(72),B13:B12  ; |305| 

           LDW     .D1T1   *+A9(84),A10      ; |305| 
           STW     .D2T2   w_3,*filtVars3_p   ; |297| filtVars3[0] = w_3      

           LDW     .D2T2   *+SP(16),filtVars2_p ; |297| 
||         STW     .D1T1   w_7$4,*filtVars7_p ; |301| filtVars7[0] = w_7

           LDW     .D2T1   *+SP(24),filtVars1_p_p ; |296| 
           NOP             3
           STW     .D2T2   w_2,*filtVars2_p   ; |296| filtVars2[0] = w_2

           STW     .D1T1   w_1,*filtVars1_p_p  ; |295| filtVars1[0] = w_1
||         RET     .S2     B3                ; |305| 
||         LDW     .D2T2   *+SP(32),filtVars6_p ; |296| 

           LDW     .D2T1   *++SP(88),A11     ; |305| 
           NOP             3

           STW     .D2T2   w_6$3,*filtVars6_p ; |300| filtVars6[0] = w_6
           ; BRANCH OCCURS {B3}              ; |305| 

;              .endproc ;C callable function, directive
             

