*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*

;******************************************************************************
;* TMS320C6x ANSI C Codegen                                      Version 4.20 *
;* Date/Time created: Mon Sep 01 15:37:36 2003                                *
;******************************************************************************

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C671x                                          *
;*   Optimization      : Enabled at level 3                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o3, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Memory Model      : Small                                                *
;*   Calls to RTS      : Near                                                 *
;*   Pipelining        : Enabled                                              *
;*   Speculative Load  : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : No Debug Info                                        *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss

			

*
*  Copyright 2002 by Texas Instruments Incorporated.
*  All rights reserved. Property of Texas Instruments Incorporated.
*  Restricted rights to use, duplicate or disclose this code are
*  granted through contract.
*  
*
*
*  ======== iir2tap2ch_ui_mx.sa ========
*  IIR filter implementation for tap-2 & channels-2, unicoeff & inplace,SP-DP.
*

************************ IIR FILTER *****************************************
****************** ORDER 2    and CHANNELS 2 ********************************
*                                                                           *
* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           *
*                           ----------------------                          *
*                            1  - a1*z~1 - a2*z~2                           *
*                                                                           *
*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) *
*                                                                           *
*   Implementation : w(n) = b0*x(n) + a1*w(n-1) + a2*w(n-2)                 *
*                    y(n) = w(n) + b1/b0*w(n-1) + b2/b0*w(n-2)              *
*                                                                           *
*   Filter variables :                                                      *
*      y(n) - *y,         x(n)   - *x                                       *
*      w(n) -             w(n-1) - w1         w(n-2) - w2                   *
*      c0=b0 - filtCfsB[0], c1=b1/b0 - filtCfsB[1], c2=b2/b0 - filtCfsB[2]  *
*      c3=a1 - filtCfsA[0], c4=a2 - filtCfsA[1]                             *
*                                                                           *
*                                                                           *
*           b0                 w(n)                                         *
*     x(n)-->>--[+]---->--------@------->-----[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a1    +-----+  b1/b0    |                            *
*               [+]----<<----| Z~1 |---->>----[+]                           *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a2    +-----+  b2/b0    |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                             +-----+                                       *
*                                                                           *
*****************************************************************************
*Note:C equivalent of s-asm code lines are given as comments,for simplicity *

		    .global _Filter_iirT2Ch2_ui_mx ;Declared as Global function
            .sect	".text:Filter_iirT2Ch2_ui_mx" ;Function memory section
	.sect	".text:Filter_iirT2Ch2_ui_mx"

;******************************************************************************
;* FUNCTION NAME: _Filter_iirT2Ch2_ui_mx                                      *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP                                           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP                                           *
;******************************************************************************
_Filter_iirT2Ch2_ui_mx:
;** --------------------------------------------------------------------------*
; _Filter_iirT2Ch2_ui_mx: .cproc  pParam ;Filter_iirT2Ch2_ui_mx(*pParam),C-call fn
; 			.reg    w10:w11, w20:w21, w30:w31, w40:w41 ;Filter states
; 			.reg    sum00:sum01, sum10:sum11
; 			.reg    sum20:sum21, sum30:sum31
; 			.reg    accum10:accum11, accum20:accum21, accum30:accum31
; 			.reg    accum40:accum41, accum50:accum51, accum60:accum61
; 			.reg    _accum10:_accum11, _accum20:_accum21, _accum30:_accum31
; 			.reg    _accum40:_accum41, _accum50:_accum51, _accum60:_accum61
; 			.reg    c00:c01,c10:c11,c20:c21,c30:c31,c40:c41,c50:c51;Coeff regs  
; 			.reg    temp, temp1, num ;Temp and count registers
; 			.reg    inval00:inval01
; 			.reg    inval10:inval11
; 			.asg    "inval00:inval01", inval
; 			.asg    "inval10:inval11", inval1
; 			.asg    "w10:w11", w1
; 			.asg    "w20:w21", w2
; 			.asg    "w30:w31", w3
; 			.asg    "w40:w41", w4
; 			.asg    "sum00:sum01", sum
; 			.asg    "sum10:sum11", sum1
; 			.asg    "accum10:accum11", accum1
; 			.asg    "accum20:accum21", accum2
; 			.asg    "accum30:accum31", accum3
; 			.asg    "accum40:accum41", accum4
; 			.asg    "accum50:accum51", accum5
; 			.asg    "accum60:accum61", accum6															
; 			.asg    "sum20:sum21", sum2
; 			.asg    "sum30:sum31", sum3			
; 			.asg    "_accum10:_accum11", _accum1
; 			.asg    "_accum20:_accum21", _accum2
; 			.asg    "_accum30:_accum31", _accum3
; 			.asg    "_accum40:_accum41", _accum4
; 			.asg    "_accum50:_accum51", _accum5
; 			.asg    "_accum60:_accum61", _accum6
; 			.asg    "c00:c01", c0
; 			.asg    "c10:c11", c1
; 			.asg    "c20:c21", c2
; 			.asg    "c30:c31", c3
; 			.asg    "c40:c41", c4
; 			.asg    "c50:c51", c5
;             .no_mdep ;no memory aliasing in this function						 			
;             .reg  pIn   ;Ptr to, array of ch i/p  ptrs
;             .reg  pOut  ;Ptr to, array of ch o/p  ptrs
;             .reg  pCoef ;Ptr to, array of ch coeff  ptrs
;             .reg  pVar  ;Ptr to, array of ch var  ptrs
;             .reg  count ;Sample counter
;             .reg  x1, x2 ;I/O ptrs
;             .reg  filtCfs,filtVars1,filtVars2 ;Coeff and var ptrs
; LOOP:	    .trip 16, 2048, 4
           STW     .D2T2   B13,*SP--(56)     ; |59| 
           STW     .D2T1   A10,*+SP(16)      ; |59| 
           STW     .D2T1   A13,*+SP(28)      ; |59| 
           STW     .D2T1   A11,*+SP(20)      ; |59| 
           STW     .D2T1   A12,*+SP(24)      ; |59| 
           STW     .D2T1   A14,*+SP(32)      ; |59| 
           STW     .D2T2   B10,*+SP(44)      ; |59| 
           STW     .D2T1   A15,*+SP(36)      ; |59| 
           STW     .D2T2   B3,*+SP(40)       ; |59| 
           STW     .D2T2   B11,*+SP(48)      ; |59| 
           STW     .D2T2   B12,*+SP(52)      ; |59| 
                      		
           NOP             1
           LDW     .D1T1   *+A4(12),A0       ; |133| pVar  = pParam[3] 
           LDW     .D1T2   *A4,B6            ; |130| pIn   = pParam[0] 
           NOP             2
           LDW     .D1T2   *+A4(8),B5        ; |132| pCoef = pParam[2] 
           LDW     .D1T2   *A0,B4            ; |142| 
           LDW     .D2T1   *B6,A15           ; |137| x1 = pIn[0]
           NOP             2
           LDW     .D2T2   *B5,B1            ; |140| 
           LDDW    .D2T2   *+B4(8),B13:B12   ; |147| w2 = filtVars1[1]

           LDW     .D1T2   *A15,B11          ; (P) |161| inval00 = *x1
||         LDDW    .D2T1   *B4,A3:A2         ; |146| w1 = filtVars1[0]

           STW     .D2T2   B4,*+SP(4)        ; |142| 
||         MVC     .S2     CSR,B4

           STW     .D2T2   B4,*+SP(12)
||         AND     .S2     -2,B4,B4

           MVC     .S2     B4,CSR            ; interrupts off
||         LDDW    .D2T2   *B1,B5:B4         ; (P) |153| c0 = filtCfs[0] // I/p gain

           LDW     .D2T1   *+B6(4),A14       ; |138| x2 = pIn[1]
           LDW     .D1T1   *+A0(4),A0        ; |143| 
           SPDP    .S2     B11,B11:B10       ; (P) |162| inval = (double)inval00
           LDDW    .D2T2   *+B1(32),B3:B2    ; (P) |155| c4 = filtCfs[4]               
           MPYDP   .M2     B11:B10,B5:B4,B9:B8 ; (P) |169| accum1=inval*c0
           LDW     .D1T1   *A14,A5           ; (P) |164| inval10 = *x2
           LDDW    .D1T1   *+A0(8),A7:A6     ; |149| w4 = filtVars2[1]              
           NOP             1
           MPYDP   .M2     B13:B12,B3:B2,B11:B10 ; (P) |170| accum2=w2*c4
           NOP             1
           LDDW    .D2T2   *+B1(24),B7:B6    ; (P) |154| c3 = filtCfs[3]               

           LDW     .D1T2   *+A4(16),B0       ; |134| num   = pParam[4] 
||         SPDP    .S1     A5,A5:A4          ; (P) |165| inval1 = (double)inval10
||         MPYDP   .M1X    A7:A6,B3:B2,A11:A10 ; (P) |180| _accum2=w4*c4

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop label : LOOP
;*      Known Minimum Trip Count         : 16
;*      Known Maximum Trip Count         : 2048
;*      Known Max Trip Count Factor      : 4
;*      Loop Carried Dependency Bound(^) : 18
;*      Unpartitioned Resource Bound     : 20
;*      Partitioned Resource Bound(*)    : 20
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     9        9     
;*      .S units                     2        3     
;*      .D units                     4        5     
;*      .M units                    20*      20*    
;*      .X cross paths              18       14     
;*      .T address paths             3        6     
;*      Long read paths              1        1     
;*      Long write paths             1        4     
;*      Logical  ops (.LS)           2        2     (.L or .S unit)
;*      Addition ops (.LSD)          4        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             7        7     
;*      Bound(.L .S .D .LS .LSD)     7        7     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 20 Register is live too long
;*                   |174| -> |209|
;*                   |174| -> |209|
;*         ii = 20 Did not find schedule
;*         ii = 21 Did not find schedule
;*         ii = 22 Did not find schedule
;*         ii = 23 Did not find schedule
;*         ii = 24 Register is live too long
;*                   |174| -> |209|
;*                   |174| -> |209|
;*                   |184| -> |212|
;*                   |184| -> |212|
;*                   |156| -> |185|
;*                   |156| -> |185|
;*         ii = 24 Did not find schedule
;*         ii = 25 Schedule found with 3 iterations in parallel
;*      done
;*
;*      Epilog not removed
;*      Collapsed epilog stages     : 0
;*
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count     : 3
;*----------------------------------------------------------------------------*
;*   SINGLE SCHEDULED ITERATION
;*
;*   LOOP:
;*              LDW     .D1T2   *A15,B11          ; |161| inval00 = *x1
;*              NOP             2
;*              LDDW    .D2T2   *B1,B5:B4         ; |153| c0 = filtCfs[0] // I/p gain
;*              NOP             1
;*              LDDW    .D2T2   *+B1(32),B3:B2    ; |155| c4 = filtCfs[4]               
;*   ||         SPDP    .S2     B11,B11:B10       ; |162| inval = (double)inval00
;*              NOP             1
;*              LDW     .D1T1   *A14,A5           ; |164| inval10 = *x2
;*              MPYDP   .M2     B11:B10,B5:B4,B9:B8 ; |169| accum1=inval*c0
;*              NOP             1
;*              MPYDP   .M1X    A7:A6,B3:B2,A11:A10 ; |180| _accum2=w4*c4
;*              NOP             1
;*              MPYDP   .M2     B13:B12,B3:B2,B11:B10 ; |170| accum2=w2*c4
;*              SPDP    .S1     A5,A5:A4          ; |165| inval1 = (double)inval10
;*              LDDW    .D2T2   *+B1(24),B7:B6    ; |154| c3 = filtCfs[3]               
;*              NOP             1
;*              LDDW    .D2T2   *+B1(16),B5:B4    ; |157| c2 = filtCfs[2]
;*              MPYDP   .M2X    A5:A4,B5:B4,B11:B10 ; |179| _accum1=inval1*c0
;*              NOP             1
;*              MPYDP   .M1X    A3:A2,B7:B6,A1:A0 ;  ^ |171| accum3=w1*c3
;*              NOP             2
;*              MPYDP   .M2X    A7:A6,B5:B4,B7:B6 ; |184| 
;*   ||         ADDDP   .L2     B9:B8,B11:B10,B9:B8 ; |189| sum=accum1+accum2
;*              MPYDP   .M1X    A9:A8,B7:B6,A5:A4 ;  ^ |181| _accum3=w3*c3
;*              NOP             2
;*              LDDW    .D2T1   *+B1(8),A11:A10   ; |156| c1 = filtCfs[1]               
;*              ADDDP   .L1X    B11:B10,A11:A10,A13:A12 ; |192| sum2=_accum1+_accum2
;*              NOP             1
;*              MPYDP   .M2     B13:B12,B5:B4,B11:B10 ; |174| accum5=w2*c2
;*   ||         ADDDP   .L2X    B9:B8,A1:A0,B9:B8 ;  ^ |190| sum+=_accum3
;*              NOP             1
;*              MPYDP   .M1     A3:A2,A11:A10,A1:A0 ; |175| accum4=w1*c1
;*              NOP             2
;*              MV      .S2X    A2,B12            ; |198| 
;*   ||         MV      .D1     A9,A7             ; |202|  w4 = w3   // MV of double type
;*   ||         MV      .S1     A8,A6             ; |203| 
;*              ADDDP   .L1     A13:A12,A5:A4,A13:A12 ;  ^ |193| sum2+=_accum3
;*              MV      .S2X    A3,B13            ; |197|  w2 = w1  // MV of double type
;*              NOP             2
;*              ADDDP   .L2     B9:B8,B11:B10,B3:B2 ; |209| sum1=accum5+sum
;*              MPYDP   .M1     A9:A8,A11:A10,A5:A4 ; |185| 
;*   ||         MV      .S1X    B8,A2             ;  ^ |200| 
;*              MV      .S1X    B9,A3             ;  ^ |199|  w1 = sum // MV of double type
;*              ADDDP   .L1X    A13:A12,B7:B6,A13:A12 ; |212| sum3=_accum5+sum2
;*              MV      .D1     A13,A9            ;  ^ |204|  w3 = sum2 // MV of double type
;*   ||         MV      .S1     A12,A8            ;  ^ |205| 
;*              NOP             6
;*              ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; |213| sum3+=_accum4
;*              ADDDP   .L2X    B3:B2,A1:A0,B3:B2 ; |210| sum1+=accum4
;*              NOP             4
;*      [ B0]   ADD     .D2     0xffffffff,B0,B0  ; |223| if(num) num=num-1;
;*              DPSP    .L1     A13:A12,A0        ; |220| temp1 = (float)sum3
;*   || [ B0]   B       .S2     LOOP              ; |224| 
;*              DPSP    .L2     B3:B2,B10         ; |217| temp = (float)sum1
;*              NOP             2
;*              STW     .D1T1   A0,*A14++         ; |221| *x2++ = temp1
;*              STW     .D1T2   B10,*A15++        ; |218| *x1++ = temp
;*              ; BRANCH OCCURS                   ; |224| 
;*----------------------------------------------------------------------------*
L1:    ; PIPED LOOP PROLOG
           LDDW    .D2T2   *+B1(16),B5:B4    ; (P) |157| c2 = filtCfs[2]
           MPYDP   .M2X    A5:A4,B5:B4,B11:B10 ; (P) |179| _accum1=inval1*c0
           LDDW    .D1T1   *A0,A9:A8         ; |148| w3 = filtVars2[0]

           STW     .D2T1   A0,*+SP(8)        ; |143| 
||         MPYDP   .M1X    A3:A2,B7:B6,A1:A0 ; (P)  ^ |171| accum3=w1*c3

           NOP             2

           MPYDP   .M2X    A7:A6,B5:B4,B7:B6 ; (P) |184| 
||         ADDDP   .L2     B9:B8,B11:B10,B9:B8 ; (P) |189| sum=accum1+accum2

           MPYDP   .M1X    A9:A8,B7:B6,A5:A4 ; (P)  ^ |181| _accum3=w3*c3
           NOP             1
           LDW     .D1T2   *+A15(4),B11      ; (P) @|161| inval00 = *x1
           LDDW    .D2T1   *+B1(8),A11:A10   ; (P) |156| c1 = filtCfs[1]               
           ADDDP   .L1X    B11:B10,A11:A10,A13:A12 ; (P) |192| sum2=_accum1+_accum2
           LDDW    .D2T2   *B1,B5:B4         ; (P) @|153| c0 = filtCfs[0] // I/p gain

           ADDDP   .L2X    B9:B8,A1:A0,B9:B8 ; (P)  ^ |190| sum+=_accum3
||         MPYDP   .M2     B13:B12,B5:B4,B11:B10 ; (P) |174| accum5=w2*c2

           LDDW    .D2T2   *+B1(32),B3:B2    ; (P) @|155| c4 = filtCfs[4]               
||         SPDP    .S2     B11,B11:B10       ; (P) @|162| inval = (double)inval00

           MPYDP   .M1     A3:A2,A11:A10,A1:A0 ; (P) |175| accum4=w1*c1
           LDW     .D1T1   *+A14(4),A5       ; (P) @|164| inval10 = *x2
           MPYDP   .M2     B11:B10,B5:B4,B9:B8 ; (P) @|169| accum1=inval*c0

           MV      .D1     A9,A7             ; (P) |202|  w4 = w3   // MV of double type
||         MV      .S1     A8,A6             ; (P) |203| 
||         MV      .S2X    A2,B12            ; (P) |198| 

           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; (P)  ^ |193| sum2+=_accum3
||         MPYDP   .M1X    A7:A6,B3:B2,A11:A10 ; (P) @|180| _accum2=w4*c4

           MV      .S2X    A3,B13            ; (P) |197|  w2 = w1  // MV of double type

           SUB     .D2     B0,2,B0
||         MPYDP   .M2     B13:B12,B3:B2,B11:B10 ; (P) @|170| accum2=w2*c4

;** --------------------------------------------------------------------------*
LOOP:    ; PIPED LOOP KERNEL
           SPDP    .S1     A5,A5:A4          ; @|165| inval1 = (double)inval10

           ADDDP   .L2     B9:B8,B11:B10,B3:B2 ; |209| sum1=accum5+sum
||         LDDW    .D2T2   *+B1(24),B7:B6    ; @|154| c3 = filtCfs[3]               

           MPYDP   .M1     A9:A8,A11:A10,A5:A4 ; |185| 
||         MV      .S1X    B8,A2             ;  ^ |200| 

           MV      .S1X    B9,A3             ;  ^ |199|  w1 = sum // MV of double type
||         LDDW    .D2T2   *+B1(16),B5:B4    ; @|157| c2 = filtCfs[2]

           ADDDP   .L1X    A13:A12,B7:B6,A13:A12 ; |212| sum3=_accum5+sum2
||         MPYDP   .M2X    A5:A4,B5:B4,B11:B10 ; @|179| _accum1=inval1*c0

           MV      .D1     A13,A9            ;  ^ |204|  w3 = sum2 // MV of double type
||         MV      .S1     A12,A8            ;  ^ |205| 

           MPYDP   .M1X    A3:A2,B7:B6,A1:A0 ; @ ^ |171| accum3=w1*c3
           NOP             2

           MPYDP   .M2X    A7:A6,B5:B4,B7:B6 ; @|184| 
||         ADDDP   .L2     B9:B8,B11:B10,B9:B8 ; @|189| sum=accum1+accum2

           MPYDP   .M1X    A9:A8,B7:B6,A5:A4 ; @ ^ |181| _accum3=w3*c3
           NOP             1

           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; |213| sum3+=_accum4
||         LDW     .D1T2   *+A15(8),B11      ; @@|161| inval00 = *x1

           ADDDP   .L2X    B3:B2,A1:A0,B3:B2 ; |210| sum1+=accum4
||         LDDW    .D2T1   *+B1(8),A11:A10   ; @|156| c1 = filtCfs[1]               

           ADDDP   .L1X    B11:B10,A11:A10,A13:A12 ; @|192| sum2=_accum1+_accum2
           LDDW    .D2T2   *B1,B5:B4         ; @@|153| c0 = filtCfs[0] // I/p gain

           MPYDP   .M2     B13:B12,B5:B4,B11:B10 ; @|174| accum5=w2*c2
||         ADDDP   .L2X    B9:B8,A1:A0,B9:B8 ; @ ^ |190| sum+=_accum3

           LDDW    .D2T2   *+B1(32),B3:B2    ; @@|155| c4 = filtCfs[4]               
||         SPDP    .S2     B11,B11:B10       ; @@|162| inval = (double)inval00

   [ B0]   ADD     .D2     0xffffffff,B0,B0  ; |223| if(num) num=num-1;
||         MPYDP   .M1     A3:A2,A11:A10,A1:A0 ; @|175| accum4=w1*c1

   [ B0]   B       .S2     LOOP              ; |224| 
||         DPSP    .L1     A13:A12,A0        ; |220| temp1 = (float)sum3
||         LDW     .D1T1   *+A14(8),A5       ; @@|164| inval10 = *x2

           DPSP    .L2     B3:B2,B10         ; |217| temp = (float)sum1
||         MPYDP   .M2     B11:B10,B5:B4,B9:B8 ; @@|169| accum1=inval*c0

           MV      .S2X    A2,B12            ; @|198| 
||         MV      .D1     A9,A7             ; @|202|  w4 = w3   // MV of double type
||         MV      .S1     A8,A6             ; @|203| 

           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; @ ^ |193| sum2+=_accum3
||         MPYDP   .M1X    A7:A6,B3:B2,A11:A10 ; @@|180| _accum2=w4*c4

           STW     .D1T1   A0,*A14++         ; |221| *x2++ = temp1
||         MV      .S2X    A3,B13            ; @|197|  w2 = w1  // MV of double type

           STW     .D1T2   B10,*A15++        ; |218| *x1++ = temp
||         MPYDP   .M2     B13:B12,B3:B2,B11:B10 ; @@|170| accum2=w2*c4

;** --------------------------------------------------------------------------*
L3:    ; PIPED LOOP EPILOG
           SPDP    .S1     A5,A5:A4          ; (E) @@|165| inval1 = (double)inval10

           LDDW    .D2T2   *+B1(24),B7:B6    ; (E) @@|154| c3 = filtCfs[3]               
||         ADDDP   .L2     B9:B8,B11:B10,B3:B2 ; (E) @|209| sum1=accum5+sum

           MV      .S1X    B8,A2             ; (E) @ ^ |200| 
||         MPYDP   .M1     A9:A8,A11:A10,A5:A4 ; (E) @|185| 

           MV      .S1X    B9,A3             ; (E) @ ^ |199|  w1 = sum // MV of double type
||         LDDW    .D2T2   *+B1(16),B5:B4    ; (E) @@|157| c2 = filtCfs[2]

           ADDDP   .L1X    A13:A12,B7:B6,A13:A12 ; (E) @|212| sum3=_accum5+sum2
||         MPYDP   .M2X    A5:A4,B5:B4,B11:B10 ; (E) @@|179| _accum1=inval1*c0

           MV      .D1     A13,A9            ; (E) @ ^ |204|  w3 = sum2 // MV of double type
||         MV      .S1     A12,A8            ; (E) @ ^ |205| 

           MPYDP   .M1X    A3:A2,B7:B6,A1:A0 ; (E) @@ ^ |171| accum3=w1*c3
           NOP             2

           MPYDP   .M2X    A7:A6,B5:B4,B7:B6 ; (E) @@|184| 
||         ADDDP   .L2     B9:B8,B11:B10,B9:B8 ; (E) @@|189| sum=accum1+accum2

           MPYDP   .M1X    A9:A8,B7:B6,A5:A4 ; (E) @@ ^ |181| _accum3=w3*c3
           NOP             1
           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; (E) @|213| sum3+=_accum4

           ADDDP   .L2X    B3:B2,A1:A0,B3:B2 ; (E) @|210| sum1+=accum4
||         LDDW    .D2T1   *+B1(8),A11:A10   ; (E) @@|156| c1 = filtCfs[1]               

           ADDDP   .L1X    B11:B10,A11:A10,A13:A12 ; (E) @@|192| sum2=_accum1+_accum2
           NOP             1

           LDW     .D2T2   *+SP(12),B4
||         ADDDP   .L2X    B9:B8,A1:A0,B9:B8 ; (E) @@ ^ |190| sum+=_accum3
||         MPYDP   .M2     B13:B12,B5:B4,B11:B10 ; (E) @@|174| accum5=w2*c2

           NOP             1
           MPYDP   .M1     A3:A2,A11:A10,A1:A0 ; (E) @@|175| accum4=w1*c1
           DPSP    .L1     A13:A12,A0        ; (E) @|220| temp1 = (float)sum3
           DPSP    .L2     B3:B2,B10         ; (E) @|217| temp = (float)sum1

           MV      .D1     A9,A7             ; (E) @@|202|  w4 = w3   // MV of double type
||         MV      .S1     A8,A6             ; (E) @@|203| 
||         MV      .S2X    A2,B12            ; (E) @@|198| 

           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; (E) @@ ^ |193| sum2+=_accum3

           MV      .S2X    A3,B13            ; (E) @@|197|  w2 = w1  // MV of double type
||         STW     .D1T1   A0,*A14++         ; (E) @|221| *x2++ = temp1

           STW     .D1T2   B10,*A15++        ; (E) @|218| *x1++ = temp
           NOP             1

           LDW     .D2T2   *+SP(48),B11      ; |241| 
||         ADDDP   .L2     B9:B8,B11:B10,B3:B2 ; (E) @@|209| sum1=accum5+sum

           LDW     .D2T1   *+SP(20),A11      ; |241| 
||         MV      .S1X    B8,A2             ; (E) @@ ^ |200| 
||         MPYDP   .M1     A9:A8,A11:A10,A5:A4 ; (E) @@|185| 

           LDW     .D2T1   *+SP(16),A10      ; |241| 
||         MV      .S1X    B9,A3             ; (E) @@ ^ |199|  w1 = sum // MV of double type

           ADDDP   .L1X    A13:A12,B7:B6,A13:A12 ; (E) @@|212| sum3=_accum5+sum2

           MV      .D1     A13,A9            ; (E) @@ ^ |204|  w3 = sum2 // MV of double type
||         MV      .S1     A12,A8            ; (E) @@ ^ |205| 

           NOP             2
           ADDDP   .L2X    B3:B2,A1:A0,B3:B2 ; (E) @@|210| sum1+=accum4
           NOP             2

           LDW     .D2T2   *+SP(4),B4
||         MVC     .S2     B4,CSR            ; interrupts on

           ADDDP   .L1     A13:A12,A5:A4,A13:A12 ; (E) @@|213| sum3+=_accum4
           MV      .S1X    B0,A4             ; |240| 
           NOP             1

           LDW     .D2T2   *+SP(40),B3       ; |241| 
||         DPSP    .L2     B3:B2,B10         ; (E) @@|217| temp = (float)sum1

           STW     .D2T2   B12,*+B4(8)       ; |231| filtVars1[1]=w2//Doulbe type
           STW     .D2T2   B13,*+B4(12)      ; |232| 
           LDW     .D2T2   *+SP(52),B12      ; |241| 

           LDW     .D2T1   *+SP(24),A12      ; |241| 
||         DPSP    .L1     A13:A12,A0        ; (E) @@|220| temp1 = (float)sum3

           LDW     .D2T1   *+SP(28),A13      ; |241| 
           STW     .D2T1   A3,*+B4(4)        ; |229| 
           STW     .D2T1   A2,*B4            ; |228| filtVars1[0]=w1//Doulbe type
           STW     .D1T1   A0,*A14++         ; (E) @@|221| *x2++ = temp1
           LDW     .D2T1   *+SP(32),A14      ; |241| 

           LDW     .D2T1   *+SP(8),A0        ; |229| 
||         STW     .D1T2   B10,*A15++        ; (E) @@|218| *x1++ = temp

           LDW     .D2T2   *+SP(44),B10      ; |241| 
           LDW     .D2T1   *+SP(36),A15      ; |241| 

           B       .S2     B3                ; |241| 
||         LDW     .D2T2   *++SP(56),B13     ; |241| 

           NOP             1
           STW     .D1T1   A7,*+A0(12)       ; |238| 
           STW     .D1T1   A9,*+A0(4)        ; |235| 
           STW     .D1T1   A6,*+A0(8)        ; |237| filtVars2[1]=w4//Doulbe type
           STW     .D1T1   A8,*A0            ; |234| filtVars2[0]=w3//Doulbe type
           ; BRANCH OCCURS                   ; |241| 


; 			.endproc ;C callable function, directive
