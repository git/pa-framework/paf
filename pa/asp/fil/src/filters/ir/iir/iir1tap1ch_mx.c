/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir1tap1ch_mx.c ========
 *  IIR filter implementation for tap-1 & channels-1, SP-DP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 1    and CHANNELS 1 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1                                    */
/*                           -------------                                   */
/*                            1  - a1*z~1                                    */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 */
/*                                                                           */
/*   Implementation : w(n) = b0*x(n) + a1*w(n-1)                             */
/*                    y(n) = w(n) + b1/b0*w(n-1)                             */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -  w,         w(n-1) - w1                                       */
/*      b0 - filtCfsB[0], b1/b0 - filtCfsB[1], a1 - filtCfsA[0]              */
/*                                                                           */
/*                                                                           */
/*           b0                  w(n)                                        */
/*     x(n)-->>--[+]----->-------@------>------[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+   b1/b0   |                            */
/*                 -----<<----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT1Ch1() ========
 *  IIR filter, taps-1 & channels-1 
 */
/* Memory section for the function code */
Int Filter_iirT1Ch1_mx( PAF_FilParam *pParam )
{
    Int count, samp; /* Loop counters */
    FIL_CONST Double * filtCfsB, *filtCfsA; /* Coeff ptrs */
    Double *restrict filtVars; /* Filter var mem ptr */
    FIL_CONST Float *restrict x; /* Input ptr */
    Float *restrict y; /* Output ptr */
    Double w, w1; /* Filter state regs */
        
    x = (Float *)pParam->pIn[0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtCfsB = (Double *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 2; /* Derive feedback coeff ptr */

    filtVars = (Double *)pParam->pVar[0]; /* Get filter var ptr */
        
    count = pParam->sampleCount; /* I/p sample block-length */
    
    /* Get the filter states into corresponding regs */
    w  = filtVars[0];    
    w1 = filtVars[0];

    /* IIR filtering for i/p block length */
    for(samp=0; samp<count; ++samp)
    {
        w = filtCfsB[0]*x[samp] + filtCfsA[0]*w1; /* w(n)=b0*x(n)+a1*w(n-1) */
        y[samp] = w + filtCfsB[1]*w1; /* y(n)=w(n)+b1/b0*w(n-1) */
        w1  = w; /* State reg shift */
    }
    
    filtVars[0] = w; /* Update state memory */
    
    return(FIL_SUCCESS);
} /* Int Filter_iirT1Ch1_mx() */
