/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir1tap2ch.c ========
 *  IIR filter implementation for tap-1 & channels-2, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 1    and CHANNELS 2 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1                                    */
/*                           -------------                                   */
/*                            1  - a1*z~1                                    */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1)                             */
/*                    y(n) = b0*w(n) + b1*w(n-1)                             */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -  w,         w(n-1) - w1                                       */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                 */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*                 -----<<----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT1Ch2() ========
 *  IIR filter, taps-1 & channels-2 
 */
/* Memory section for the function code */
Int Filter_iirT1Ch2( PAF_FilParam *pParam ) 
{
    FIL_CONST Float *xL, *xR; /* Input ptr */
    Float * restrict yL, * restrict yR; /* Output ptr */
    FIL_CONST Float *filtCfsBL, *filtCfsBR; /* Feedforward ptrs */
    FIL_CONST Float *filtCfsAL, *filtCfsAR; /* Feedback ptrs */
    Float * restrict filtVarsL, * restrict filtVarsR; /* Var mem ptr */
    Float wL, wR, wL1, wR1; /* Filter state regs */
    Int count, samp; /* Loop counters */
    
    /* Get i/p ptr */            
    xL = (Float *)pParam->pIn[0];
    xR = (Float *)pParam->pIn[1];

    /* Get o/p ptr */
    yL = (Float *)pParam->pOut[0];
    yR = (Float *)pParam->pOut[1];
    
    filtCfsBL = (Float *)pParam->pCoef[0]; /* L-Feedforward ptr */
    filtCfsAL = filtCfsBL + 2; /* L-Derive feedback coeff ptr */    
    
    filtCfsBR = (Float *)pParam->pCoef[1]; /* R-Feedforward ptr */
    filtCfsAR = filtCfsBR + 2; /* R-Derive feedback coeff ptr */   
    
    /* Get filter var ptr */
    filtVarsL = (Float *)pParam->pVar[0];
    filtVarsR = (Float *)pParam->pVar[1];
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    /* Get the filter states into corresponding regs */
    wL  = filtVarsL[0];
    wL1 = filtVarsL[0];
    
    wR  = filtVarsR[0];
    wR1 = filtVarsR[0];
    
    /* IIR filtering for i/p block length */    
    #pragma MUST_ITERATE(16, 2048, 4)    
    for(samp = 0; samp<count; ++samp)
    {
        /* Channel-L */
        wL = xL[samp] + filtCfsAL[0]*wL; /* x(n)+a1*w(n-1) */
        yL[samp] = filtCfsBL[0]*wL + filtCfsBL[1]*wL1; /* b0*w(n)+b1*w(n-1) */
        wL1  = wL; /* State reg shift */
        
        /* Channel-R */
        wR = xR[samp] + filtCfsAR[0]*wR; /* x(n)+a1*w(n-1) */
        yR[samp] = filtCfsBR[0]*wR + filtCfsBR[1]*wR1; /* b0*w(n)+b1*w(n-1) */
        wR1  = wR; /* State reg shift */
        
    }
    
    /* Update state memory */
    filtVarsL[0] = wL;
    filtVarsR[0] = wR;
    
    return(FIL_SUCCESS);
} /* Int Filter_iirT1Ch2() */

