/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tap1ch.c ========
 *  IIR filter implementation for tap-2 & channels-1, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS 1 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                 */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                 */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                             +-----+                                       */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT2Ch1() ========
 *  IIR filter, taps-2 & channels-1 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch1( PAF_FilParam *pParam ) 
{
    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Float *restrict x; /* Input ptr */
    Float *restrict y; /* Output ptr */
    Float accum1,accum2,accum3,accum4; /* Accumulator regs */
    Float input_data; /* Data reg */
    Int count, samp; /* Loop counters */        
    Float w1, w2; /* Filter state regs */  
    
    x = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    /* Get the filter states into corresponding regs */
    w1 = filtVars[0];
    w2 = filtVars[1];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        accum1 = filtCfsA[0]*w1; /* a1*w(n-1) */
        accum2 = filtCfsA[1]*w2; /* a2*w(n-2) */
        
        accum3 = filtCfsB[1]*w1; /* b1*w(n-1) */
        accum4 = filtCfsB[2]*w2; /* b2*w(n-2) */
        
        input_data = *x++; /* Get i/p sample */
        
        w2 = w1; /* w(n-1) = w(n) */
        
        /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */
        w1 = input_data + accum2 + accum1;
        
        /* y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) */
        *y++ = filtCfsB[0]*w1 + accum3 + accum4; 
        
    }
    
    /* Update state memory */
    filtVars[0] = w1;
    filtVars[1] = w2;
 
    return(FIL_SUCCESS);
} /* Int Filter_iirT2Ch1() */ 

