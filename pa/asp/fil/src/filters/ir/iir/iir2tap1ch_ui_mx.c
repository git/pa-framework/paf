/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tap1ch_ui_mx.c ========
 *  IIR filter implementation for tap-2 & channels-1, unicoeff/inplace, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS 1 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Implementation : w(n) = b0*x(n) + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = w(n) + b1/b0*w(n-1) + b2/b0*w(n-2)              */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1/b0 - filtCfsB[1], b2/b0 - filtCfsB[2]           */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*           b0                 w(n)                                         */
/*     x(n)-->>--[+]---->--------@------->-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+  b1/b0    |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+  b2/b0    |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                             +-----+                                       */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT2Ch1_ui_mx() ========
 *  IIR filter, taps-2 & channels-1, unicoeff/inplace, SP-DP
 */
/* Memory section for the function code */
Int Filter_iirT2Ch1_ui_mx( PAF_FilParam *pParam )
{
    FIL_CONST Double * filtCfs; /* Coeff ptrs */
    Double *restrict filtVars; /* Filter var mem ptr */
    Float *restrict x; /* Input ptr */

    Int count, samp; /* Loop counters */         
    Double w, w1, w2; /* Filter state regs */     
    
    x = (Float *)pParam->pIn[0]; /* Get i/p ptr */
    
    filtCfs = (Double *)pParam->pCoef[0]; /* Get coeff ptr */
    
    filtVars = (Double *)pParam->pVar[0]; /* Get filter var ptr */
    
    count = (Int)pParam->sampleCount; /* I/p sample block-length */
    
    /* Get the filter states into corresponding regs */
    w1 = filtVars[0];
    w2 = filtVars[1];

    #pragma MUST_ITERATE(16, 2048, 4)
    /* IIR filtering for i/p block length */
    for( samp = 0; samp < count; samp++) 
    {
        /* w(n) = b0*x(n) + a2*w(n-2) + a1*w(n-1) */
        w    = filtCfs[0] * (*x) + (filtCfs[4] * w2) + (filtCfs[3] * w1);
        
        /* y(n) = w(n) + b1/b0*w(n-1) + b2/b0*w(n-2) */
        *x++ = w + (filtCfs[1] * w1) + (filtCfs[2] * w2);
        
        /* Shift state regs */
        w2   = w1;
        w1   = w;
    }

    /* update delay lines */
    filtVars[0] = w1;
    filtVars[1] = w2;
  
    return( FIL_SUCCESS );
} /* Int Filter_iirT2Ch1_ui_mx */
