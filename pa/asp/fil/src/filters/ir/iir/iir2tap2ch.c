/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tap2ch.c ========
 *  IIR filter implementation for tap-2 & channels-2, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS 2 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                 */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                 */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT2Ch2() ========
 *  IIR filter, taps-2 & channels-2 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch2( PAF_FilParam *pParam ) 
{
    Float * restrict xL, * restrict xR; /* Input ptr */
    Float * restrict yL, * restrict yR; /* Output ptr */
    Float * restrict filtCfsBL, * restrict filtCfsBR; /* Feedforward ptrs */
    Float *filtCfsAL, *filtCfsAR; /* Feedback ptrs */
    Float * restrict filtVarsL, * restrict filtVarsR; /* Var mem ptr */
    Float accum1,accum2,accum3,accum4; /* Accumulator regs */
    Float input_dataL, input_dataR;
    Int count, samp; /* Loop counters */
    Float w1L, w2L, w1R, w2R; /* Filter state regs */  
    
    /* Get i/p ptr */     
    xL = (Float *)pParam->pIn[0];
    xR = (Float *)pParam->pIn[1];
    
    /* Get o/p ptr */
    yL = (Float *)pParam->pOut[0];
    yR = (Float *)pParam->pOut[1];
    
    filtCfsBL  = (Float *)pParam->pCoef[0]; /* L-Feedforward ptr */
    filtCfsAL  = filtCfsBL + 3; /* L-Derive feedback coeff ptr */       
    
    filtCfsBR  = (Float *)pParam->pCoef[1]; /* R-Feedforward ptr */
    filtCfsAR  = filtCfsBR + 3; /* R-Derive feedback coeff ptr */      
    
    /* Get filter var ptr */
    filtVarsL = (Float *)pParam->pVar[0];
    filtVarsR = (Float *)pParam->pVar[1];
    
    count = pParam->sampleCount; /* I/p sample block-length */

    /* Get the filter states into corresponding regs */
    w1L = filtVarsL[0];
    w2L = filtVarsL[1];
    
    w1R = filtVarsR[0];
    w2R = filtVarsR[1];    
    
    /* IIR filtering for i/p block length */  
    #pragma MUST_ITERATE(16, 2048, 4)  
    for (samp = 0; samp < count; samp++)
    {
        /* Channel-L */
        accum1 = filtCfsAL[0]*w1L; /* a1*w(n-1) */ 
        accum2 = filtCfsAL[1]*w2L; /* a2*w(n-2) */
        accum3 = filtCfsBL[1]*w1L; /* b1*w(n-1) */
        accum4 = filtCfsBL[2]*w2L; /* b2*w(n-2) */
        
        input_dataL = *xL++; /* Get an input sample */
        
        w2L = w1L; /* Shift state registers */
        
        /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */
        w1L = input_dataL + accum2 + accum1;
        
        /* y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) */
        *yL++ = filtCfsBL[0]*w1L + accum3 + accum4;
        
        /* Channel-R */        
        accum1 = filtCfsAR[0]*w1R; /* a1*w(n-1) */ 
        accum2 = filtCfsAR[1]*w2R; /* a2*w(n-2) */
        accum3 = filtCfsBR[1]*w1R; /* b1*w(n-1) */
        accum4 = filtCfsBR[2]*w2R; /* b2*w(n-2) */
        
        input_dataR = *xR++; /* Get an input sample */
        
        w2R = w1R; /* Shift state registers */
        
        /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */
        w1R = input_dataR + accum2 + accum1;
        
        /* y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) */
        *yR++ = filtCfsBR[0]*w1R + accum3 + accum4;        
    }

    /* Update state memory */
    filtVarsL[0] = w1L;
    filtVarsL[1] = w2L;
    filtVarsR[0] = w1R;
    filtVarsR[1] = w2R;
    
    return(FIL_SUCCESS);
} /* Int Filter_iirT2Ch2() */ 

