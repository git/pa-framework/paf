
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tap1ch_c2_df2.c ========
 *  IIR filter implementation(DF2) for tap-2 & channels-1, SP, cascade-2.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2 , CHANNELS 1, CASCADE-2 **************************/
/* Single stage equations :                                                  */
/*                                                                           */
/* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)    */
/*                                                                           */
/*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    */
/*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    */
/*                                                                           */
/*                                                                           */
/*Implementational structure, with a common input gain :                     */
/*                                                                           */
/*         g         w(n)                w'(n)                               */
/*      x ->-[+]------@------[+]--[+]------@------[+]--->--y(n)              */          
/*            |       |       |    |       |       |                         */                   
/*            ^       v       ^    ^       v       ^                         */                   
/*            |  a1+-----+ b1"|    | a'1+-----+b'1"|                         */                   
/*           [+]-<-| Z~1 |->-[+]  [+]-<-| Z~1 |->-[+]                        */                   
/*            |    +-----+    |    |    +-----+    |                         */                   
/*            |       |       |    |       |       |                         */                   
/*            ^       v       ^    ^       v       ^                         */                   
/*            |  a2+-----+ b2"|    | a'2+-----+b'2"|                         */                   
/*             --<-| Z~1 |->--      --<-| Z~1 |->--                          */                   
/*                 +-----+              +-----+                              */ 
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
                                                                         
/* Header files */                                                       
#include "filters.h"                                                     
                                                                         
/*                                                                       
 *  ======== Filter_iirT2Ch1_c2_df2() ========
 *  IIR filter(DF2), taps-2 ,channels-1 & Cascade-2, SP 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch1_c2_df2( PAF_FilParam *pParam ) 
{
    FIL_CONST Float *filtCfs;          /* Coeff ptrs         */
    Float *restrict filtVars;          /* Filter var mem ptr */
    FIL_CONST Float *restrict x;       /* Input ptr          */
    Float * y;                         /* Output ptr         */
    Float accum1,accum2,accum3,accum4; /* Accumulator regs   */
    Float input_data, output;          /* Data regs          */
    Int count, samp;                   /* Loop counters      */        
    Float w1, w2, w3, w4;              /* Filter state regs  */
    Float b1, b2, a1, a2;              /* Cascade-1 coeffs   */
    Float bb1, bb2, aa1, aa2;          /* Cascade-2 coeffs   */  
    Float b0, gain;                    /* Input gain coeff   */
    
    x = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtCfs = (Float *)pParam->pCoef[0]; /* Get coeff pointer */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    
    count = pParam->sampleCount; /* I/p sample block-length */

    /* Get the coefficients */    
    b0 = filtCfs[0]; 
    b1 = filtCfs[1];
    b2 = filtCfs[2];
    a1 = filtCfs[3];
    a2 = filtCfs[4];
    
    bb1 = filtCfs[5];
    bb2 = filtCfs[6];
    aa1 = filtCfs[7];
    aa2 = filtCfs[8]; 
    
    gain = b0;    
    
    /* Get the filter states into corresponding regs */
    w1 = filtVars[0];
    w2 = filtVars[1];
    w3 = filtVars[2];
    w4 = filtVars[3];
    
    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)    
    for (samp = 0; samp < count; samp++)
    {
        /*    Stage-1    */        
        accum1 = a1*w1; /* a1*w(n-1) */
        accum2 = a2*w2; /* a2*w(n-2) */
        
        accum3 = b1*w1; /* b1*w(n-1) */
        accum4 = b2*w2; /* b2*w(n-2) */
        
        input_data = *x++ * gain; /* Get i/p sample */
        
        w2 = w1; /* w(n-1) = w(n) */
        
        /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */
        w1 = input_data + accum2 + accum1;
        
        /* y(n) = w(n) + b1*w(n-1) + b2*w(n-2) */
        output = w1 + accum3 + accum4;  
        
        /*    Stage-2    */        
        accum1 = aa1*w3; /* a1*w(n-1) */
        accum2 = aa2*w4; /* a2*w(n-2) */
        
        accum3 = bb1*w3; /* b1*w(n-1) */
        accum4 = bb2*w4; /* b2*w(n-2) */
        
        w4 = w3; /* w(n-1) = w(n) */
        
        /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */
        w3 = output + accum2 + accum1;
        
        /* y(n) = w(n) + b1*w(n-1) + b2*w(n-2) */
        *y++ = w3 + accum3 + accum4;          
        
    }
    
    /* Update state memory */
    filtVars[0] = w1;
    filtVars[1] = w2;
    filtVars[2] = w3;
    filtVars[3] = w4;
 
    return(FIL_SUCCESS); 
    
} /* Int Filter_iirT2Ch1_c2_df2() */
    
    

    
    
    
