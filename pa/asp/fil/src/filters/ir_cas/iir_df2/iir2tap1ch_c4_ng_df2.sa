*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*  ======== iir2tap1ch_c4_ng_df2.sa ========
*  IIR filter implementation(DF2) for tap-2, channels-1, SP, cascade 4, no gain.
*
*
 
************************ IIR FILTER *****************************************
****************** ORDER 2, CHANNELS 1, CASCADE 4, NO GAIN ******************
*Single section equations :                                                 *
*                                                                           *
* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           *
*                           ----------------------                          *
*                            1  - a1*z~1 - a2*z~2                           *
*                                                                           *
*   Direct form    : y(n) = x(n) + b1*x(n-1) + a1*y(n-1) + a2*y(n-2)        *
*                                                                           *
*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    *
*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    *
*                                                                           *
*                                                                           *
* Implementation structure of one cascade section                           *
*                              w(n)                                         *
*     x(n)------[+]---->--------@-------------[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a1    +-----+    b1     |                            *
*               [+]----<<----| Z~1 |---->>----[+]                           *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a2    +-----+    b2     |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                            +-----+                                        *
*                                                                           *
*****************************************************************************
* Note: C equivalent of Serial Assembly(SA) code are given as comments.     *

            .global _Filter_iirT2Ch1_c4_ng_df2 ;Declared as Global function 
            .sect ".text:Filter_iirT2Ch1_c4_ng_df2" ;Memory section for the function 
_Filter_iirT2Ch1_c4_ng_df2: .cproc pParam ;Filter_iirT2Ch1_c4_ng_df2(*pParam), C callable fn

             .no_mdep ;no memory aliasing in this function
             
************* General registers *******************************
             .reg  y ;O/p ptr   
             .reg  x ;I/p ptr    
             .reg  filtVars ;Var ptr 
             .reg  filtCoeff, filtCoeff1 ;Coeff ptr              
             .reg  res1, res2, res3, res4, res5 ;Temp accumulators 
             .reg  pIn ;Ptr to, array of ch i/p  ptrs
             .reg  pOut ;Ptr to, array of ch o/p  ptrs
             .reg  pCoef ;Ptr to, array of ch o/p  ptrs
             .reg  pVar ;Ptr to, array of ch var  ptrs
             .reg  count ;Sample counter              

************* Coefficient registers *******************************             
             .reg  CoeffB0_1, CoeffB1_1, CoeffB2_1, CoeffA0_1, CoeffA1_1 ;Section 1
             .reg  CoeffB0_2, CoeffB1_2, CoeffB2_2, CoeffA0_2, CoeffA1_2 ;Section 2
             .reg  CoeffB0_3, CoeffB1_3, CoeffB2_3, CoeffA0_3, CoeffA1_3 ;Section 3
             .reg  CoeffB0_4, CoeffB1_4, CoeffB2_4, CoeffA0_4, CoeffA1_4 ;Section 4 

************* Filter state registers *******************************             
             .reg  w1_1, w2_1 
             .reg  w1_2, w2_2 
             .reg  w1_3, w2_3 
             .reg  w1_4, w2_4   

             .reg  accum1 ;O/p accumulator register              
             .reg  accum2 ;O/p accumulator register 
             .reg  accum3 ;O/p accumulator register 
             .reg  accum4 ;O/p accumulator register                          

             .reg  input_data ;I/p data reg
             .reg  temp
             
             LDW   *pParam[0],          pIn;pIn   = pParam[0]
             LDW   *pParam[1],          pOut;pOut  = pParam[1]
             LDW   *pParam[2],          pCoef ;pCoef = pParam[2]
             LDW   *pParam[3],          pVar;pVar  = pParam[3]
             LDW   *pParam[4],          count;count = pParam[4]
             
             LDW   *pIn[0],             x ;x = pIn[0]
             LDW   *pOut[0],            y ;y = pOut[0] 
             LDW   *pCoef[0],           filtCoeff ;filtCoeff = pCoef[0]
             LDW   *pCoef[0],           filtCoeff1 ;filtCoeff = pCoef[0]
             LDW   *pVar[0],            filtVars ;filtVars = pVar[0]  
                                        
             ;Get filter states into regs
             LDW   *filtVars[0],        w1_1 ;w1(n-1) = filtVars[0] 
             LDW   *filtVars[1],        w2_1 ;w1(n-2) = filtVars[1] 
                                                              
             LDW   *filtVars[2],        w1_2 ;w2(n-1) = filtVars[2]  
             LDW   *filtVars[3],        w2_2 ;w2(n-2) = filtVars[3] 
                                                              
             LDW   *filtVars[4],        w1_3 ;w3(n-1) = filtVars[4]  
             LDW   *filtVars[5],        w2_3 ;w3(n-2) = filtVars[5] 
                                                              
             LDW   *filtVars[6],        w1_4 ;w4(n-1) = filtVars[6]  
             LDW   *filtVars[7],        w2_4 ;w4(n-2) = filtVars[7]              

             ;Get  of feedback coeffs of cascades into regs
             ;Stage 1
             ;LDW    *filtCoeff[0],     temp ;Just to offset by -1 to remove gain
             LDW    *filtCoeff1--[1],   CoeffB1_1 ;CoeffB1_1 = filtCoeff[1]
                
             LDW    *filtCoeff1[1],     CoeffB1_1 ;CoeffB1_1 = filtCoeff1[1]   
             LDW    *filtCoeff[1 ],     CoeffB2_1 ;CoeffB2_1 = filtCoeff[1 ]                
             LDW    *filtCoeff[2 ],     CoeffA0_1 ;CoeffA0_1 = filtCoeff[2 ]                    
             LDW    *filtCoeff[3 ],     CoeffA1_1 ;CoeffA1_1 = filtCoeff[3 ]        
             LDW    *filtCoeff[4 ],     CoeffB1_2 ;CoeffB1_2 = filtCoeff[4 ]                
             LDW    *filtCoeff[5 ],     CoeffB2_2 ;CoeffB2_2 = filtCoeff[5 ]                    
             LDW    *filtCoeff[6 ],     CoeffA0_2 ;CoeffA0_2 = filtCoeff[6 ]                                  
             LDW    *filtCoeff[7 ],     CoeffA1_2 ;CoeffA1_2 = filtCoeff[7 ]                    
             LDW    *filtCoeff[8 ],     CoeffB1_3 ;CoeffB1_3 = filtCoeff[8 ]                 
             LDW    *filtCoeff[9 ],     CoeffB2_3 ;CoeffB2_3 = filtCoeff[9 ]                    
             LDW    *filtCoeff[10],     CoeffA0_3 ;CoeffA0_3 = filtCoeff[10]                     
             LDW    *filtCoeff[11],     CoeffA1_3 ;CoeffA1_3 = filtCoeff[11]                                                 
             LDW    *filtCoeff[12],     CoeffB1_4 ;CoeffB1_4 = filtCoeff[12]                 
             LDW    *filtCoeff[13],     CoeffB2_4 ;CoeffB2_4 = filtCoeff[13]                    
             LDW    *filtCoeff[14],     CoeffA0_4 ;CoeffA0_4 = filtCoeff[14]                     
             LDW    *filtCoeff[15],     CoeffA1_4 ;CoeffA1_4 = filtCoeff[15]                                                 
                                                                        
LOOP:        .trip 16, 2048, 4                                          
                                                                        
************* Stage 1 *******************************                   
                                                                        
             ;Get coeffs into regs                          
             LDW    *filtCoeff[2],      CoeffA0_1 ;CoeffA0_1 = filtCoeff[2]       
             LDW    *filtCoeff[3],      CoeffA1_1 ;CoeffA1_1 = filtCoeff[3]                      
                                                                        
             ;a1*w(n-1) & b1*w(n-1)                                                
             MPYSP  w1_1,               CoeffA0_1,          res1 ;res1 =  w1_1*CoeffA0_1
             MPYSP  w1_1,               CoeffB1_1,          res3 ;res3 =  w1_1*CoeffB1_1             
                                                                        
             ;Get one input sample & increment i/p ptr                  
             LDW    *x++,               input_data ;input_data = *x++    

             ;a2*w(n-2) & b2*w(n-2) 
             MPYSP  w2_1,               CoeffA1_1,          res2 ;res2 = w2_1*CoeffA1_1
             MPYSP  w2_1,               CoeffB2_1,          res4 ;res4 = w2_1*CoeffB2_1

             ;Shift filter states
             MV     w1_1,               w2_1 ;w2_1 = w1_1

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_data,         res2,               res2 ;res2 += input_data
             ADDSP  res1,               res2,               w1_1 ;w1_1 = res1 + res2
             
             ;y(n) = w(n) + 'b1*w(n-1)' + 'b2*w(n-2)' 
             ADDSP  res4,               res3,               accum1 ;accum1 = res3 + res4
             ADDSP  accum1,             w1_1,               accum1 ;accum1 += w1_1

************* Stage 2 *******************************
             
             ;Get coeffs into regs  
             LDW    *filtCoeff[6],      CoeffA0_2 ;CoeffA0_2 = filtCoeff[6]                     
             LDW    *filtCoeff[7],      CoeffA1_2 ;CoeffA1_2 = filtCoeff[7]                          
                          
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_2,               CoeffA0_2,          res1 ;res1 =  w1_2*CoeffA0_2
             MPYSP  w1_2,               CoeffB1_2,          res3 ;res3 =  w1_2*CoeffB1_2             

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_2,               CoeffA1_2,          res2 ;res2 = w2_2*CoeffA1_2
             MPYSP  w2_2,               CoeffB2_2,          res4 ;res4 = w2_2*CoeffB2_2
             
             ;Shift filter states
             MV     w1_2,               w2_2 ;w2_2 = w1_2

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  accum1,             res2,               res2 ;res2 += input_data
             ADDSP  res1,               res2,               w1_2 ;w1_2 = res1 + res2

             ;y(n) = w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum2 ;accum2 = res3 + res4
             ADDSP  accum2,             w1_2,               accum2 ;accum2 += w1_2

************* Stage 3 *******************************
             
             ;Get coeffs into regs  
             LDW    *filtCoeff[8],      CoeffB1_3  ;CoeffB1_3 = filtCoeff[8]  
             LDW    *filtCoeff[10],     CoeffA0_3 ;CoeffA0_3 = filtCoeff[10]          
             
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_3,               CoeffA0_3,          res1 ;res1 =  w1_3*CoeffA0_3
             MPYSP  w1_3,               CoeffB1_3,          res3 ;res3 =  w1_3*CoeffB1_3             
             
             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_3,               CoeffA1_3,          res2 ;res2 = w2_3*CoeffA1_3
             MPYSP  w2_3,               CoeffB2_3,          res4 ;res4 = w2_3*CoeffB2_3

             ;Shift filter states
             MV     w1_3,               w2_3 ;w2_3 = w1_3

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  accum2,             res2,               res2 ;res2 *= input_data
             ADDSP  res1,               res2,               w1_3 ;w1_3 = res1 + res2

             ;y(n) = w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum3 ;accum3 = res3 + res4
             ADDSP  accum3,             w1_3,               accum3 ;accum3 += w1_3

************* Stage 4 *******************************
             
             ;Get coeffs into regs  
             LDW    *filtCoeff[12],      CoeffB1_4 ;CoeffB1_4 = filtCoeff[12]   
             LDW    *filtCoeff[13],      CoeffB2_4 ;CoeffB2_4 = filtCoeff[13]         
             LDW    *filtCoeff[14],      CoeffA0_4 ;CoeffA0_4 = filtCoeff[14]          
             LDW    *filtCoeff[15],      CoeffA1_4 ;CoeffA1_4 = filtCoeff[15]                                      
             
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_4,               CoeffA0_4,          res1 ;res1 = w1_4*CoeffA0_4
             MPYSP  w1_4,               CoeffB1_4,          res3 ;res3 = w1_4*CoeffB1_4             
             
             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_4,               CoeffA1_4,          res2 ;res2 = w2_4*CoeffA1_4
             MPYSP  w2_4,               CoeffB2_4,          res4 ;res4 = w2_4*CoeffB2_4

             ;Shift filter states
             MV     w1_4,               w2_4 ;w2_4 = w1_4

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  accum3,             res2,               res2 ;res2 += input_data
             ADDSP  res1,               res2,               w1_4 ;w1_4 = res1 + res2

             ;y(n) = w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum4 ;accum3 = res3 + res4
             ADDSP  accum4,             w1_4,               accum4 ;accum3 += res5

             ;Store the calculated o/p      
             STW    accum4,            *y++ ;*y++ = accum4                       
                          
********************************************  
           
             SUB    count,              1,                  count ;count = count - 1
   [count]   B      LOOP ; if(count != 0) goto LOOP                         
   
             ;Update filter state memory
             STW    w1_1,           *filtVars[0] ;filtVars[0] = w1_1
             STW    w2_1,           *filtVars[1] ;filtVars[1] = w2_1
             STW    w1_2,           *filtVars[2] ;filtVars[2] = w1_2
             STW    w2_2,           *filtVars[3] ;filtVars[3] = w2_2
             STW    w1_3,           *filtVars[4] ;filtVars[4] = w1_3      
             STW    w2_3,           *filtVars[5] ;filtVars[5] = w2_3
             STW    w1_4,           *filtVars[6] ;filtVars[6] = w1_4      
             STW    w2_4,           *filtVars[7] ;filtVars[7] = w2_4
             

             .return count ;return(count)
             
             .endproc ;C callable function, directive
             
