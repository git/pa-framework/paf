/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== filtyp.h ========
 *  This contains FIL layer2 type, structure declarations 
 */
 
#ifndef FILTYP_
#define FILTYP_

#include <fil_datatype.h>
#include <fil_literals.h>
#include <fil_macros.h>

/* Filter function table element structure */
typedef struct IFIL_FunRec {
    Int  (*fnPtr)(PAF_FilParam *);
} IFIL_FunRec;

/* Void Coefficient structure */
typedef struct {
    Uint   type;
    Uint   sampRate;
    Void   *cPtr[1];
} PAF_FilCoef_Void;

typedef Int (*FIL_filterFnPtr)(PAF_FilParam *);

#ifdef PAF_DEVICE
extern const IFIL_FunRec FIL_IIRFilterRec[5][FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN];
#else
extern const IFIL_FunRec FIL_IIRFilterRec[2][FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN];
#endif

extern const IFIL_FunRec FIL_FIRFilterRec[2][FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN];

#ifdef PAF_DEVICE
extern const IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[4][FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN]; 
#else 
extern const IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[1][FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN]; 
#endif

extern Int  FIL_varsPerCh( Uint type );
extern Void FIL_memReset( Void *varPtr, Int size );
extern Void FIL_offsetParam( PAF_FilParam *pParam, Int offset );

#endif /* FILTYP_ */

