
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <stdio.h>

#include <fe.h>
#include <math.h>


extern testTable gTestArray_IIR_DF2[];
extern testTable gTestArray_IIR_MX[];
extern testTable gTestArray_IIR_CAS[];


#define	FIL_MAX_NUM_COEFS	72		//8Chan * 9 coef [4 tap]
#define	FIL_MAX_NUM_VARS	32		//8Chan * 4 tap
#define FIL_MAX_ITERATION_COUNT  4
#define FIL_MAX_SAMPLE_COUNT  64
#define FIL_MAX_NUM_CHAN	16
#define FIL_MAX_SAMPLES_PER_CHAN (FIL_MAX_ITERATION_COUNT*FIL_MAX_SAMPLE_COUNT)
#define FIL_MAX_NUM_SAMPLES (FIL_MAX_ITERATION_COUNT*FIL_MAX_SAMPLE_COUNT*FIL_MAX_NUM_CHAN)


//(0.000030517578125)      // - (15bit)
//(0.0000152587890625)     // - (16bit)
//(0.00000762939453125)    // - (17bit)
//(0.000003814697265625)   // - (18bit)
//(0.0000019073486328125)  // - (19bit)
//(0.00000095367431640625) // - (20bit)

#define FIL_ERROR_THRESHOLD     (0.000030517578125)      // - (15bit)

// 8388608 = 2^23
double BitError_array[]={
    2/8388608.00,    // 1
    4/8388608.00,    // 2
    8/8388608.00,    // 3
    16/8388608.00,   // 4
    32/8388608.00,   // 5
    64/8388608.00,   // 6
    128/8388608.00,  // 7
    256/8388608.00,  // 8
    512/8388608.00,  // 9
    1024/8388608.00, // 10
    2048/8388608.00, // 11
    4096/8388608.00, // 12
    8192/8388608.00, // 13
    16384/8388608.00, // 14
    32768/8388608.00, // 15
    65536/8388608.00, // 16
    131072/8388608.00, // 17
    262144/8388608.00, // 18
    524288/8388608.00, // 19
	1048576/8388608.00, // 20
    2097152/8388608.00, // 21
    4194304/8388608.00, // 22
    8388608/8388608.00, // 23
};

// FIL Test data structures
PAF_AudioData gCoeff[FIL_MAX_NUM_COEFS];
PAF_AudioData gInArray[FIL_MAX_NUM_SAMPLES];
PAF_AudioData gOutArray[FIL_MAX_NUM_SAMPLES];
PAF_AudioData gVars[FIL_MAX_NUM_VARS]={0};

PAF_AudioData gOutArrayRef[FIL_MAX_NUM_SAMPLES];
PAF_AudioData gVarsRef[FIL_MAX_NUM_VARS]={0};



// FIL MX Test data structures
double gCoeff_mx[FIL_MAX_NUM_COEFS];
double gVars_mx[FIL_MAX_NUM_VARS]={0};
double gVarsRef_mx[FIL_MAX_NUM_VARS]={0};



PAF_AudioData *pIn[FIL_MAX_NUM_CHAN];
PAF_AudioData *pOut[FIL_MAX_NUM_CHAN];
PAF_AudioData *pVar[FIL_MAX_NUM_CHAN];
PAF_AudioData *pCoef[FIL_MAX_NUM_CHAN];

double *pVar_mx[FIL_MAX_NUM_CHAN];
double *pCoef_mx[FIL_MAX_NUM_CHAN];

// Param structure
PAF_FilParam gParamTest={
    (void**)pIn,
    (void**)pOut,
    (void**)pCoef,
    (void**)pVar,
    FIL_MAX_SAMPLE_COUNT,
    0,
    0
};

PAF_FilParam gParamTest_mx={
    (void**)pIn,
    (void**)pOut,
    (void**)pCoef_mx,
    (void**)pVar_mx,
    FIL_MAX_SAMPLE_COUNT,
    0,
    0
};

void TEST_FIL_IIR_DF2(void);
void TEST_FIL_IIR_MX(void);
void TEST_FIL_IIR_CAS(void);

 
void main(void)
{
	//To Test IIR Directform2 Implementaions in FIL
	//For actual filters under test see gTestArray_IIR_DF2 in fe_par.c
	 TEST_FIL_IIR_DF2();

	//To Test IIR MX (DP-DP) Implementaions in FIL
	//For actual filters under test see gTestArray_IIR_MX in fe_par.c
	 TEST_FIL_IIR_MX();

	//To Test IIR CASCADE Implementaions in FIL
	//For actual filters under test see gTestArray_IIR_CAS in fe_par.c
	 TEST_FIL_IIR_CAS();

}



void TEST_FIL_IIR_DF2(void){
    int i,ret;
    int ch,taps;
    int testCount;
    int testIterations;
    int uniCoef,inplace;
    float error_max, error_max1, error_value;
 	int nCh, index;

	printf("\nTesting IIR DF2 Implementaion in FIL\n");
	
    for(i=0;i<FIL_MAX_NUM_COEFS;i++){
        gCoeff[i]=((i+1)&0xF)*0.0015;
    }

    for(i=0;i<FIL_MAX_NUM_SAMPLES;i++)
        gInArray[i]= 0.1*sin(2*3.1415*4000*i/48000); 

    for(testCount=0;gTestArray_IIR_DF2[testCount].filterFn;testCount++){
		// Reset  vars 
       for(i=0;i<FIL_MAX_NUM_VARS;i++){
            gVarsRef[i]=0;
            gVars[i]=0;
        }

        error_max = 0.0;
       // Get input samples & Get coefficients.
        for(i=0;i<FIL_MAX_NUM_SAMPLES;i++){
             gOutArray[i]=gInArray[i];
			 gOutArrayRef[i]=gInArray[i];
          }

        for(testIterations=0;testIterations<FIL_MAX_ITERATION_COUNT;testIterations++){
            // Get a local copy of the fil
            ch=gTestArray_IIR_DF2[testCount].ch;
            taps=gTestArray_IIR_DF2[testCount].taps;
            uniCoef=gTestArray_IIR_DF2[testCount].uniCoef;
            inplace=gTestArray_IIR_DF2[testCount].inplace;
            
                            
            // Setup the param structure for FIL Implementation
            for(i=0;i<ch;i++){

                gParamTest.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(inplace)
                    gParamTest.pIn[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

               	gParamTest.pOut[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(uniCoef)
                    gParamTest.pCoef[i]=gCoeff;
                else
                    gParamTest.pCoef[i]=gCoeff+i*(taps*2+1);

                gParamTest.pVar[i]=gVars+i*taps;
            }  
            gParamTest.channels=ch;
            
            if(gTestArray_IIR_DF2[testCount].filterFn==0)
                continue;
            ret=gTestArray_IIR_DF2[testCount].filterFn(&gParamTest);

			// Setup the param structure for Reference Implementation
            if(ret==0){
                for(i=0;i<ch;i++){
                    gParamTest.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    if(inplace)
                      gParamTest.pIn[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

                    gParamTest.pOut[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    gParamTest.pVar[i]=gVarsRef+i*taps;
					gParamTest.use = taps;
                } 
                    
                // Do reference filtering.
                iir_generic(&gParamTest);

               for(nCh = 0; nCh < ch ; nCh++){
				 error_max1 = 0;
                 for(i=0;i<FIL_MAX_SAMPLE_COUNT;i++){

					index = FIL_MAX_SAMPLES_PER_CHAN*nCh + testIterations*FIL_MAX_SAMPLE_COUNT + i;

                    if(gOutArrayRef[index])
                        error_value=fabs((gOutArrayRef[index]-gOutArray[index])/gOutArrayRef[index]);
                    else
                        error_value=gOutArray[index];

                    if(fabs(error_value)>error_max1)
                        error_max1=fabs(error_value);
                   }
				 if(error_max1>FIL_ERROR_THRESHOLD){
                        printf("Test case [%d], Channel [%d], Iteration [%d] - failed\n",testCount,nCh, testIterations);
                  } 

				 if(error_max1> error_max){
                        error_max = error_max1;
                  } 

				}
            }
            else
                printf("Error: Test case [%d]",testCount);
        }
        for(i=0;i<23;i++){
            if(error_max<=BitError_array[i])
                break;
        }
        if((23-i)>=15)
            printf("Test case [%d], - %d bits match\n",testCount,(23-i));
        else
            printf("Test case [%d], - %d bits match (fail)\n",testCount,(23-i));
    }
}


void TEST_FIL_IIR_MX(void){
    int i,ret;
    int ch,taps;
    int testCount;
    int testIterations;
    int uniCoef,inplace;
    float error_max, error_max1, error_value;
 	int nCh, index;

	printf("\nTesting IIR MX (DP-DP) Implementaion in FIL\n");

    for(i=0;i<FIL_MAX_NUM_COEFS;i++){
        gCoeff_mx[i]=((i+1)&0xF)*0.0015;
    }

    for(i=0;i<FIL_MAX_NUM_SAMPLES;i++)
        gInArray[i]= 0.1*sin(2*3.1415*4000*i/48000); 

    for(testCount=0;gTestArray_IIR_MX[testCount].filterFn;testCount++){
        // Reset vars
       for(i=0;i<FIL_MAX_NUM_VARS;i++){
            gVarsRef_mx[i]=0;
            gVars_mx[i]=0;
        }

		error_max = 0.0;

       // Get input samples & Get coefficients.
        for(i=0;i<FIL_MAX_NUM_SAMPLES;i++){
             gOutArray[i]=gInArray[i];
			 gOutArrayRef[i]=gInArray[i];
          }

        for(testIterations=0;testIterations<FIL_MAX_ITERATION_COUNT;testIterations++){
            // Get a local copy of the fil
            ch=gTestArray_IIR_MX[testCount].ch;
            taps=gTestArray_IIR_MX[testCount].taps;
            uniCoef=gTestArray_IIR_MX[testCount].uniCoef;
            inplace=gTestArray_IIR_MX[testCount].inplace;

        
            // Setup the param structure for FIL Implementation.
            for(i=0;i<ch;i++){
                gParamTest_mx.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(inplace)
                    gParamTest_mx.pIn[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

               	gParamTest_mx.pOut[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(uniCoef)
                    gParamTest_mx.pCoef[i]=gCoeff_mx;
                else
                    gParamTest_mx.pCoef[i]=gCoeff_mx+i*(taps*2+1);

                gParamTest_mx.pVar[i]=gVars_mx+i*taps;
            }  
            gParamTest_mx.channels=ch;
            gParamTest_mx.use=taps;
            if(gTestArray_IIR_MX[testCount].filterFn==0)
                continue;
            
            // Do Test filtering.
            ret=gTestArray_IIR_MX[testCount].filterFn(&gParamTest_mx);

			// Setup the param structure for Reference Implementation.
            if(ret==0){ 
                for(i=0;i<ch;i++){ 
                    gParamTest_mx.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    if(inplace)
                      gParamTest_mx.pIn[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

                    gParamTest_mx.pOut[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    gParamTest_mx.pVar[i]=gVarsRef_mx+i*taps;
					gParamTest_mx.use = taps;
                } 
                // Do reference filtering.
                iir_generic_mx(&gParamTest_mx); 

               for(nCh = 0; nCh < ch ; nCh++){
				 error_max1 = 0;
                 for(i=0;i<FIL_MAX_SAMPLE_COUNT;i++){

					index = FIL_MAX_SAMPLES_PER_CHAN*nCh + testIterations*FIL_MAX_SAMPLE_COUNT + i;

                    if(gOutArrayRef[index])
                        error_value=fabs((gOutArrayRef[index]-gOutArray[index])/gOutArrayRef[index]);
                    else
                        error_value=gOutArray[index];

                    if(fabs(error_value)>error_max1)
                        error_max1=fabs(error_value);
                   }
				 if(error_max1>FIL_ERROR_THRESHOLD){
                        printf("Test case [%d], Channel [%d], Iteration [%d] - failed\n",testCount,nCh, testIterations);
                  } 

				 if(error_max1> error_max){
                        error_max = error_max1;
                  } 

				}
            }
            else
                printf("Error: Test case [%d]",testCount);
        }
        for(i=0;i<23;i++){
            if(error_max<=BitError_array[i])
                break;
        }
        if((23-i)>=15)
            printf("Test case [%d], - %d bits match\n",testCount,(23-i));
        else
            printf("Test case [%d], - %d bits match (fail)\n",testCount,(23-i));
    }

}

void  TEST_FIL_IIR_CAS(void)
{
    int i,ret;
    int ch,taps;
    int testCount;
    int testIterations, cascade;
    int uniCoef,inplace;
    float error_max, error_max1, error_value;
 	int nCh, index;

	printf("\nTesting IIR CASCADE Implementaion in FIL\n");

    for(i=0;i<FIL_MAX_NUM_COEFS;i++){
        gCoeff[i]=((i+1)&0xF)*0.0015;
    }

    for(i=0;i<FIL_MAX_NUM_SAMPLES;i++)
        gInArray[i]= 0.1*sin(2*3.1415*4000*i/48000); 
            
    for(testCount=0;gTestArray_IIR_CAS[testCount].filterFn;testCount++){
		// Reset  vars 
       for(i=0;i<FIL_MAX_NUM_VARS;i++){
            gVarsRef[i]=0;
            gVars[i]=0;
        }

        error_max = 0.0;
       // Get input samples & Get coefficients.
        for(i=0;i<FIL_MAX_NUM_SAMPLES;i++){
             gOutArray[i]=gInArray[i];
			 gOutArrayRef[i]=gInArray[i];
          }

        for(testIterations=0;testIterations<FIL_MAX_ITERATION_COUNT;testIterations++){
            // Get a local copy of the fil
            ch=gTestArray_IIR_CAS[testCount].ch;
            taps=gTestArray_IIR_CAS[testCount].taps;
            uniCoef=gTestArray_IIR_CAS[testCount].uniCoef;
            inplace=gTestArray_IIR_CAS[testCount].inplace;
            cascade=gTestArray_IIR_CAS[testCount].cascade+1;
                
                           
            // Setup the param structure for FIL Implementation.
			for(i=0;i<ch;i++){

                gParamTest.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(inplace)
                    gParamTest.pIn[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

               	gParamTest.pOut[i]=gOutArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                if(uniCoef)
                    gParamTest.pCoef[i]=gCoeff;
                else
                    gParamTest.pCoef[i]=gCoeff+i*(cascade*taps*2+1);
                gParamTest.pVar[i]=gVars+i*2*taps*cascade;
            }  
            gParamTest.channels=ch;
            gParamTest.use=taps*cascade;

            if(gTestArray_IIR_CAS[testCount].filterFn==0)
                continue;
            ret=gTestArray_IIR_CAS[testCount].filterFn(&gParamTest);
            if(ret==0){
                for(i=0;i<ch;i++){
                    gParamTest.pIn[i]=gInArray+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    if(inplace)
                      gParamTest.pIn[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);

                    gParamTest.pOut[i]=gOutArrayRef+(i*FIL_MAX_SAMPLES_PER_CHAN)+ (testIterations*FIL_MAX_SAMPLE_COUNT);
                    gParamTest.pVar[i]=gVarsRef+i*taps*cascade;
                } 
                gParamTest.use=taps;
                gParamTest.use|=((cascade)<<16);
                    
                // Do reference filtering.
                iir_generic_cas(&gParamTest);

               for(nCh = 0; nCh < ch ; nCh++){
				 error_max1 = 0;
                 for(i=0;i<FIL_MAX_SAMPLE_COUNT;i++){

					index = FIL_MAX_SAMPLES_PER_CHAN*nCh + testIterations*FIL_MAX_SAMPLE_COUNT + i;

                    if(gOutArrayRef[index])
                        error_value=fabs((gOutArrayRef[index]-gOutArray[index])/gOutArrayRef[index]);
                    else
                        error_value=gOutArray[index];

                    if(fabs(error_value)>error_max1)
                        error_max1=fabs(error_value);
                   }
				 if(error_max1>FIL_ERROR_THRESHOLD){
                        printf("Test case [%d], Channel [%d], Iteration [%d] - failed\n",testCount,nCh, testIterations);
                  } 

				 if(error_max1> error_max){
                        error_max = error_max1;
                  } 

				}
            }
            else
                printf("Error: Test case [%d]",testCount);
        }
        for(i=0;i<23;i++){
            if(error_max<=BitError_array[i])
                break;
        }
        if((23-i)>=15)
            printf("Test case [%d], - %d bits match\n",testCount,(23-i));
        else
            printf("Test case [%d], - %d bits match (fail)\n",testCount,(23-i));
    }

}

