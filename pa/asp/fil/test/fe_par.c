
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <fe.h>

#ifndef NULL
#define NULL    0
#endif /* NULL */

//	{funtionname,		taps,chan,unicoef,inplace,casc}	

//start with .sa files first
testTable gTestArray_IIR_DF2[]={
    {Filter_iirT1Ch4		,1,4,0,0,0},
    {Filter_iirT1Ch5_u		,1,5,1,0,0},
    {Filter_iirT1Ch6_ui		,1,6,1,1,0},    
    {Filter_iirT1Ch7_ui		,1,7,1,1,0},    
    {Filter_iirT2Ch3		,2,3,0,0,0},    
    {Filter_iirT2Ch4_u		,2,4,1,0,0},    
    {Filter_iirT2Ch4_ui		,2,4,1,1,0}, 
    {NULL					,0,0,0,0,0}, 
   }; 		


testTable gTestArray_IIR_MX[]={
    {Filter_iirT2Ch1_mx_s	,2,1,0,0,0},
    {Filter_iirT2Ch2_u_mx	,2,2,1,0,0},
    {Filter_iirT2Ch2_ui_mx	,2,2,1,1,0},    
    {Filter_iirT4Ch1_mx		,4,1,0,0,0},    
    {NULL					,0,0,0,0,0}, 
   }; 		

testTable gTestArray_IIR_CAS[]={
//    {Filter_iirT2Ch2_c2_i_df2_sub	,2,2,0,1,2},
    {Filter_iirT2Ch1_c4_df2			,2,1,0,0,3},
    {Filter_iirT2Ch1_c5_df2			,2,1,0,0,4}, 
    {NULL							,0,0,0,0,0}, 
   }; 		



