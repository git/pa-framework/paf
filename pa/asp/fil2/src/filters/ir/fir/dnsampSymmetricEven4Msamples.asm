*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  

	.global	_dnsamp2xSymmetricEven4Msample

_dnsamp2xSymmetricEven4Msample

		;	parameters	iptr, cptr, optr, scnt, ntap
		;	iptr:	pointer to delay(filter state) buffer immediately followed by the input buffer
		;			where the FIR computation starts.  sptr should be aligned at double-word boundary.
		;	cptr:	pointer to coefficient buffer.  cptr should be aligned at double-word boundary.
		;	optr:	pointer to output buffer. optr should be aligned at word boundary.
		;	scnt:	number of output samples to process.  scnt should be a integer multiple of 4.
		;	ntap:	number of taps in the FIR filter.  This also dictates delay buffer size.
		;			ntap should be 8N where N is an integer bigger than 1.
		
		.asg	a4,iptra
		.asg	b4,iptrb
		.asg	a15,ioffa
		.asg	b26,ioffb
		.asg	a5,cptra
		.asg	b5,cptrb
		.asg	a22,coffa
		.asg	b3,coffb
		.asg	a6,optra
		.asg	b6,optrb

		.asg	a0,ntap
		.asg	a23,tcnt
		.asg	a2,iter
		.asg	b0,scnt
		.asg	b2,oav
		.asg	a1,first
				
		.asg 	a9,c1
		.asg	a8,c0
		.asg	b9,c3
		.asg	b8,c2

		.asg	a7,suma
		.asg	b1,sumb

		.asg	a10,proda
		.asg	b10,prodb
		
		.asg	a11,o0a
		.asg	a12,o1a
		.asg	a13,o2a
		.asg	a14,o3a
		.asg	b11,o0b		
		.asg	b12,o1b
		.asg	b13,o2b
		.asg	b14,o3b

		.asg	a26,o0w	
		.asg	a27,o2w
		.asg	b27,o1w
		.asg	b28,o3w
		.asg	a30,nula
		.asg	b7,nulb

		.asg	a16,l0
		.asg	a17,l1
		.asg	a18,l2
		.asg	a19,l3
		.asg	a20,l4
		.asg	a21,l5
		.asg	a24,l6
		.asg	a25,l7
		.asg	a28,l8
		.asg	a29,l9
		.asg	b16,r0
		.asg	b17,r1
		.asg	b18,r2
		.asg	b19,r3
		.asg	b20,r4
		.asg	b21,r5
		.asg	b22,r6
		.asg	b23,r7
		.asg	b24,r8
		.asg	b25,r9

		mvc		csr,b0
		and		1,b0,b1
		clr		b0,0,1,b0
 [b1]	or		2,b0,b0
		mvc		b0,csr
						
; reassign parameters and save registers on stack

		stw		a10,*b15--[1]
		stw		b10,*b15--[1]
		stw		a11,*b15--[1]
		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
		stw		a13,*b15--[1]
		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]

		mv		a8,ntap
		mv		b4,cptra
		add		cptra,8,cptrb

		sub		b6,4,scnt

	; set pointers, constants, loops, etc

		add		optra,4,optrb
		shr		ntap,2,coffa		; NTAP = 8M + 16, coffa = ntap / 4 = 2M + 4
		neg		coffa,coffb
		neg		coffa,coffa			; coffa = coffb = - 2M - 4
		add		coffa,4,ioffa		; ioffa = - 2M
		neg		ioffa,ioffb
		add		ioffb,8,ioffb		; ioffb = 2M + 8
		sub		ntap,4,iptrb		; iptrb = 8M + 12
		shl		iptrb,2,iptrb
		add		iptrb,iptra,iptrb	; iptrb = iptra + 8M + 12
		sub		ntap,16,tcnt		; tcnt = 8M 
		zero	nula
		zero	nulb
		zero	o0a
		zero	o1a
		zero	o2a
		zero	o3a
		zero	o0b
		zero	o1b
		zero	o2b
		zero	o3b
		zero	first
		mvk		1,oav

LOOP1_PROLOG

		lddw		*iptra++[1],l1:l0
||		lddw		*iptrb++[1],r1:r0

		lddw		*iptra++[1],l3:l2
||		lddw		*iptrb++[1],r3:r2

LOOP1

			addsp	o0a,o0b,o0w
||			lddw	*iptra++[1],l5:l4
||			lddw	*iptrb++[1],r5:r4

			addsp	o1a,o1b,o1w
||			lddw	*iptra++[1],l7:l6
||			lddw	*iptrb++[1],r7:r6

			addsp	o2a,o2b,o2w
||			lddw	*iptra--[2],l9:l8
||			lddw	*iptrb--[6],r9:r8

			addsp	o3a,o3b,o3w
||			lddw	*cptra++[2],c1:c0
||			lddw	*cptrb++[2],c3:c2

			addsp	l0,r3,suma
||			addsp	l2,r1,sumb
||			mpysp	nula,o0a,o0a
||			mpysp	nulb,o0b,o0b
||			add.d	first,1,first

			addsp	l2,r5,suma
||			addsp	l4,r3,sumb
||			mpysp	nula,o1a,o1a
||			mpysp	nulb,o1b,o1b
||			mv		tcnt,iter

			lddw	*iptrb++[1],r1:r0
||			mpysp	nula,o2a,o2a
||			mpysp	nulb,o2b,o2b
||			addsp	l4,r7,suma
||			addsp	l6,r5,sumb

			lddw	*iptra++[1],l1:l0
||			lddw	*iptrb++[1],r3:r2
||			mpysp	nula,o3a,o3a
||			mpysp	nulb,o3b,o3b
||			addsp	l6,r9,suma
||			addsp	l8,r7,sumb

			lddw	*iptra++[1],l3:l2
||			lddw	*iptrb++[1],r5:r4
||			addsp	l1,r2,suma
||			addsp	l3,r0,sumb
||			mpysp	suma,c0,proda
||			mpysp	sumb,c2,prodb

LOOP2
				lddw	*iptra++[1],l5:l4
||				lddw	*iptrb--[5],r7:r6
||				addsp	l3,r4,suma
||				addsp	l5,r2,sumb
||				mpysp	suma,c0,proda
||				mpysp	sumb,c2,prodb
||	[!first]	addsp	o2a,proda,o2a
||	[!first]	addsp	o2b,prodb,o2b

				lddw	*iptra++[1],l7:l6
||				mv		r5,r9
||				addsp	l5,r6,suma
||				addsp	l7,r4,sumb
||				mpysp	suma,c0,proda
||				mpysp	sumb,c2,prodb
||	[!first]	addsp	o3a,proda,o3a
||	[!first]	addsp	o3b,prodb,o3b

				lddw	*iptra--[2],l9:l8
||				mv		r4,r8
||				addsp	l7,r8,suma
||				addsp	l9,r6,sumb
||				mpysp	suma,c0,proda
||				mpysp	sumb,c2,prodb

				lddw	*cptra++[2],c1:c0
||				lddw	*cptrb++[2],c3:c2
||				mpysp	suma,c1,proda
||				mpysp	sumb,c3,prodb
||				addsp	o0a,proda,o0a
||				addsp	o0b,prodb,o0b
||	[iter]		b		LOOP2

				addsp	l0,r3,suma
||				addsp	l2,r1,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c3,prodb
||				addsp	o1a,proda,o1a
||				addsp	o1b,prodb,o1b
||				zero.d	first

				addsp	l2,r5,suma
||				addsp	l4,r3,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c3,prodb
||				addsp	o2a,proda,o2a
||				addsp	o2b,prodb,o2b

	[iter]		lddw	*iptra++[1],l1:l0
||	[iter]		lddw	*iptrb++[1],r1:r0
||				addsp	l4,r7,suma
||				addsp	l6,r5,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c3,prodb
||				addsp	o3a,proda,o3a
||				addsp	o3b,prodb,o3b

	[iter]		lddw	*iptra++[1],l3:l2
||	[iter]		lddw	*iptrb++[1],r3:r2
||				addsp	l6,r9,suma
||				addsp	l8,r7,sumb
||				addsp	o0a,proda,o0a
||				addsp	o0b,prodb,o0b

	[iter]		sub.d	iter,8,iter
||	[iter]		lddw	*iptrb++[1],r5:r4
||				addsp	l1,r2,suma
||				addsp	l3,r0,sumb
||				mpysp	suma,c0,proda
||				mpysp	sumb,c2,prodb
||				addsp	o1a,proda,o1a
||				addsp	o1b,prodb,o1b

LOOP2_END

			addad	iptra,ioffa,iptra
||			addad	iptrb,ioffb,iptrb
||			addsp	l3,r4,suma
||			addsp	l5,r2,sumb
||			mpysp	suma,c0,proda
||			mpysp	sumb,c2,prodb
||			addsp	o2a,proda,o2a
||			addsp	o2b,prodb,o2b

	[!oav]	stw		o0w,*optra++[2]
||			addad	cptrb,coffb,cptrb
||			addsp	l5,r6,suma
||			addsp	l7,r4,sumb
||			mpysp	suma,c0,proda
||			mpysp	sumb,c2,prodb
||			addsp	o3a,proda,o3a
||			addsp	o3b,prodb,o3b

			addad	cptra,coffa,cptra
||	[!oav]	stw		o1w,*optrb++[2]
||			addsp	l7,r8,suma
||			addsp	l9,r6,sumb
||			mpysp	suma,c0,proda
||			mpysp	sumb,c2,prodb

	[!oav]	stw		o2w,*optra++[2]
||			mpysp	suma,c1,proda
||			mpysp	sumb,c3,prodb
||			addsp	o0a,proda,o0a
||			addsp	o0b,prodb,o0b

	[!oav]	stw		o3w,*optrb++[2]
||			mpysp	suma,c1,proda
||			mpysp	sumb,c3,prodb
||			addsp	o1a,proda,o1a
||			addsp	o1b,prodb,o1b

			mpysp	suma,c1,proda
||			mpysp	sumb,c3,prodb
||			addsp	o2a,proda,o2a
||			addsp	o2b,prodb,o2b
||	[scnt]	b		LOOP1

			mpysp	suma,c1,proda
||			mpysp	sumb,c3,prodb
||			addsp.l	o3a,proda,o3a
||			addsp.l	o3b,prodb,o3b

			addsp.l	o0a,proda,o0a
||			addsp.l	o0b,prodb,o0b
||			zero.d	oav

			addsp.l	o1a,proda,o1a
||			addsp.l	o1b,prodb,o1b

	[scnt]	lddw	*iptra++[1],l1:l0
||	[scnt]	lddw	*iptrb++[1],r1:r0
||			addsp.l	o2a,proda,o2a
||			addsp.l	o2b,prodb,o2b

	[scnt]	lddw	*iptra++[1],l3:l2
||	[scnt]	lddw	*iptrb++[1],r3:r2
||			addsp	o3a,proda,o3a
||			addsp	o3b,prodb,o3b
||	[scnt]	sub.s	scnt,4,scnt

LOOP1_EPILOG
		
		addsp	o0a,o0b,o0w
		addsp	o1a,o1b,o1w
		addsp	o2a,o2b,o2w
		addsp	o3a,o3b,o3w
		stw		o0w,*optra++[2]
		stw		o1w,*optrb++[2]
		stw		o2w,*optra++[2]
		stw		o3w,*optrb++[2]

LOOP1_END

		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
		ldw		*++b15[1],b14
		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
		ldw		*++b15[1],a12
		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
		mvc		csr,b0
		extu	b0,1,1,b1
 [b1]	set		b0,0,0,b0
 		mvc		b0,csr
	
