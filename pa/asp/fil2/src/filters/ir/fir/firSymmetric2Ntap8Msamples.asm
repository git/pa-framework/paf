*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  

	.global	_firSymmetricNtap8Msample

_firSymmetricNtap8Msample

		;	parameters	iptr, cptr, optr, scnt, ntap, oinc
		;	iptr:	pointer to delay(filter state) buffer immediately followed by the input buffer
		;			where the FIR computation starts.  sptr should be aligned at double-word boundary.
		;	cptr:	pointer to coefficient buffer.  cptr should be aligned at double-word boundary.
		;	optr:	pointer to output buffer. optr should be aligned at word boundary.
		;	scnt:	number of output samples to process.  scnt should be a integer multiple of 8.
		;	ntap:	number of taps in the FIR filter.  This also dictates delay buffer size and alignment.
		;			ntap should be integer bigger than 6.
		;	oinc:	output pointer increment value in word addressing.  Should be 1 in normal FIR use.
		;			Useful in upsampler. 2 for 2x upsampling, and 4 for 4x upsampling.
		;
		
		.asg	a4,iptra
		.asg	b4,iptrb
		.asg	a5,cptra
		.asg	b5,cptrb
		.asg	a6,optra
		.asg	b6,optrb

		.asg	a0,ntap
		.asg	b0,tcnt
		.asg	a1,scnt
		.asg	b1,odd
		.asg	a2,oav
		.asg	a15,offseta
		.asg	b2,offsetb
		.asg	a3,oinca
		.asg	b3,oincb
				
		.asg 	a9,c1a
		.asg	a8,c0a
		.asg	b9,c1b
		.asg	b8,c0b

		.asg	a7,suma
		.asg	b7,sumb
		.asg	a10,proda
		.asg	b10,prodb
		
		.asg	a11,o0
		.asg	a12,o2
		.asg	a13,o4
		.asg	a14,o6
		.asg	b11,o1		
		.asg	b12,o3
		.asg	b13,o5
		.asg	b14,o7

		.asg	a27,o4w
		.asg	a28,o6w
		.asg	b27,o5w
		.asg	b28,o7w

		.asg	a16,l0
		.asg	a17,l1
		.asg	a18,l2
		.asg	a19,l3
		.asg	a20,l4
		.asg	a21,l5
		.asg	a22,l6
		.asg	a23,l7
		.asg	a24,l8
		.asg	b16,r0
		.asg	b17,r1
		.asg	b18,r2
		.asg	b19,r3
		.asg	b20,r4
		.asg	b21,r5
		.asg	b22,r6
		.asg	b23,r7
		.asg	b24,r8
				
; reassign parameters and save registers on stack

		stw		a10,*b15--[1]
||		mvc		csr,b0

		stw		b10,*b15--[1]
	
		stw		a11,*b15--[1]
||		clr		b0,0,0,b0	

		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
	
		stw		a13,*b15--[1]
||		mvc		b0,csr	

		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]

		mv		b4,cptra
		add		cptra,0,cptrb
		mv		a4,iptrb
		mv		b6,scnt
		sub		scnt,8,scnt
		mv		optra,optrb
		mv		a8,ntap
		addaw	optrb,b8,optrb
		add		b8,b8,oincb
		mv		oincb,oinca


	; set pointers, constants, loops, etc

		mv		ntap,odd				
		extu	odd,31,31,odd					; odd = LSB(ntap)
		clr		ntap,0,1,ntap				; make ntap even
		zero	oav
		sub		ntap,2,offseta
		mv		offseta,offsetb
		mv		iptra,iptrb
		addaw	iptrb,offsetb,iptrb		

LOOP1
			mpysp	l2,c0a,proda
||			mpysp	l3,c0b,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3
||			lddw	*iptra++[1],l1:l0
||			lddw	*iptrb--[1],r1:r0
||			mv		offsetb,tcnt

			mpysp	l4,c0a,proda
||			mpysp	l5,c0b,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5
||			lddw	*iptra++[1],l3:l2
||			lddw	*iptrb--[1],r3:r2

			mpysp	l6,c0a,proda
||			mpysp	l7,c0b,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7
||			lddw	*iptra++[1],l5:l4
||			lddw	*iptrb--[1],r5:r4
			
			addsp	o0,proda,o0
||			addsp	o1,prodb,o1
||			lddw	*iptra++[1],l7:l6
||			lddw	*iptrb--[1],r7:r6

			addsp	o2,proda,o2
||			addsp	o3,prodb,o3
||			ldw		*iptra--[6],l8
||			ldw		*iptrb++[6],r8

			addsp	o4,proda,o4w
||			addsp	o5,prodb,o5w
||			lddw	*cptra++[1],c1a:c0a

			addsp	o6,proda,o6w
||			addsp	o7,prodb,o7w
||			lddw	*cptrb++[1],c1b:c0b

	[oav]	stw		o0,*optra++[oinca]
||	[oav]	stw		o1,*optrb++[oincb]
||			addsp	l0,r1,suma
||			addsp	l1,r2,sumb

	[oav]	stw		o2,*optra++[oinca]
||	[oav]	stw		o3,*optrb++[oincb]			
||			addsp	l2,r3,suma
||			addsp	l3,r4,sumb

			lddw	*iptra++[1],l1:l0
||			lddw	*iptrb--[1],r1:r0
||			addsp	l4,r5,suma
||			addsp	l5,r6,sumb

			lddw	*iptra++[1],l3:l2
||			lddw	*iptrb--[1],r3:r2
||			addsp	l6,r7,suma
||			addsp	l7,r8,sumb

			lddw	*iptra++[1],l5:l4
||			lddw	*iptrb--[1],r5:r4
||			addsp	l1,r0,suma
||			addsp	l2,r1,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			zero	o0
||			zero	o1

			lddw	*iptra++[1],l7:l6
||			lddw	*iptrb--[1],r7:r6
||			addsp	l3,r2,suma
||			addsp	l4,r3,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			zero	o2
||			zero	o3

			ldw		*iptra--[6],l8
||			ldw		*iptrb++[6],r8
||			addsp	l5,r4,suma
||			addsp	l6,r5,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			zero	o4
||			zero	o5
		
			lddw	*cptra++[1],c1a:c0a
||			addsp	l7,r6,suma
||			addsp	l8,r7,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			zero	o6
||			zero	o7

			lddw	*cptrb++[1],c1b:c0b
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1
			
	[oav]	stw		o4w,*optra++[oinca]
||	[oav]	stw		o5w,*optrb++[oincb]
||			addsp	l0,r1,suma
||			addsp	l1,r2,sumb
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3

	[oav]	stw		o6w,*optra++[oinca]
||	[oav]	stw		o7w,*optrb++[oincb]
||			addsp	l2,r3,suma
||			addsp	l3,r4,sumb
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5

			lddw	*iptra++[1],l1:l0
||			lddw	*iptrb--[1],r1:r0
||			addsp	l4,r5,suma
||			addsp	l5,r6,sumb
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7

			lddw	*iptra++[1],l3:l2
||			lddw	*iptrb--[1],r3:r2
||			addsp	l6,r7,suma
||			addsp	l7,r8,sumb
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1

LOOP2
				lddw	*iptra++[1],l5:l4
||				lddw	*iptrb--[1],r5:r4
||				addsp	l1,r0,suma
||				addsp	l2,r1,sumb
||				mpysp	suma,c0a,proda
||				mpysp	sumb,c0b,prodb
||				addsp	o2,proda,o2
||				addsp	o3,prodb,o3

				lddw	*iptra++[1],l7:l6
||				lddw	*iptrb--[1],r7:r6
||				addsp	l3,r2,suma
||				addsp	l4,r3,sumb
||				mpysp	suma,c0a,proda
||				mpysp	sumb,c0b,prodb
||				addsp	o4,proda,o4
||				addsp	o5,prodb,o5

				ldw		*iptra--[6],l8
||				ldw		*iptrb++[6],r8
||				addsp	l5,r4,suma
||				addsp	l6,r6,sumb
||				mpysp	suma,c0a,proda
||				mpysp	sumb,c0b,prodb
||				addsp	o6,proda,o6
||				addsp	o7,prodb,o7

				lddw	*cptra++[1],c1a:c0a
||				addsp	l7,r6,suma
||				addsp	l8,r7,sumb
||				mpysp	suma,c0a,proda
||				mpysp	sumb,c0b,prodb
||	[tcnt]		b		LOOP2

				lddw	*cptrb++[1],c1b:c0b
||				mpysp	suma,c1a,proda
||				mpysp	sumb,c1b,prodb
||				addsp	o0,proda,o0
||				addsp	o1,prodb,o1
			
				addsp	l0,r1,suma
||				addsp	l1,r2,sumb
||				mpysp	suma,c1a,proda
||				mpysp	sumb,c1b,prodb
||				addsp	o2,proda,o2
||				addsp	o3,prodb,o3

				addsp	l2,r3,suma
||				addsp	l3,r4,sumb
||				mpysp	suma,c1a,proda
||				mpysp	sumb,c1b,prodb
||				addsp	o4,proda,o4
||				addsp	o5,prodb,o5
||	[tcnt]		lddw	*iptrb--[1],r1:r0

				addsp	l4,r5,suma
||				addsp	l5,r6,sumb
||				mpysp	suma,c1a,proda
||				mpysp	sumb,c1b,prodb
||				addsp	o6,proda,o6
||				addsp	o7,prodb,o7
||	[tcnt]		sub		tcnt,2,tcnt
||	[tcnt]		lddw	*iptra++[1],l1:l0

	[tcnt]		lddw	*iptra++[1],l3:l2
||	[tcnt]		lddw	*iptrb--[1],r3:r2
||				addsp	l6,r7,suma
||				addsp	l7,r8,sumb
||				addsp	o0,proda,o0
||				addsp	o1,prodb,o1

LOOP2_END

			addsp	l1,r0,suma
||			addsp	l2,r1,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3

	[odd]	ldw		*cptra--[offseta],c0a
||			addsp	l3,r2,suma
||			addsp	l4,r3,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5

	[odd]	ldw		*cptrb--[offsetb],c0b
||			addsp	l5,r4,suma
||			addsp	l6,r5,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7
		
	[odd]	lddw	*iptra++[1],l1:l0
||			addsp	l7,r6,suma
||			addsp	l8,r7,sumb
||			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||	[scnt]	b		LOOP1

	[odd]	lddw	*iptra++[1],l3:l2
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1

			
	[odd]	lddw	*iptra++[1],l5:l4
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3

	[odd]	lddw	*iptra++[1],l7:l6
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5
||			sub		scnt,8,scnt

	[odd]	zero	c0a
||	[odd]	zero	c0b
||			mpysp	suma,c1a,proda
||			mpysp	sumb,c1b,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7

			mpysp	suma,c0a,proda
||			mpysp	sumb,c0b,prodb
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1
||			set		oav,0,0,oav

			lddw	*iptra++[1],l3:l2
||			lddw	*iptrb--[1],r3:r2
||			addsp	l6,r7,suma
||			addsp	l7,r8,sumb
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1

LOOP1_EPILOG

		mpysp	suma,c0a,proda
||		mpysp	sumb,c0b,prodb
||		addsp	o2,proda,o2
||		addsp	o3,prodb,o3
		
		mpysp	suma,c0a,proda
||		mpysp	sumb,c0b,prodb
||		addsp	o4,proda,o4
||		addsp	o5,prodb,o5

		mpysp	suma,c0a,proda
||		mpysp	sumb,c0b,prodb
||		addsp	o6,proda,o6
||		addsp	o7,prodb,o7

		addsp	o0,proda,o0
||		addsp	o1,prodb,o1

		addsp	o2,proda,o2
||		addsp	o3,prodb,o3
		
		addsp	o4,proda,o4
||		addsp	o5,prodb,o5

		addsp	o6,proda,o6
||		addsp	o7,prodb,o7

		stw		o0,*optra++[oinca]
||		stw		o1,*optrb++[oincb]

		stw		o2,*optra++[oinca]
||		stw		o3,*optrb++[oincb]

		stw		o4,*optra++[oinca]
||		stw		o5,*optrb++[oincb]

		stw		o6,*optra++[oinca]
||		stw		o7,*optrb++[oincb]

LOOP1_END

		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
	
		ldw		*++b15[1],b14
||		mvc		csr,b0	

		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
	
		ldw		*++b15[1],a12
||		set 	b0,0,0,b0	

		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
 		zero	a1
 		mvc		a1,amr
 		mvc		b0,csr	
		nop		1
