/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
void symFIR(float *ip, float *cp, float *op, int ns, int nt)
{
 float	*restrict lip, *restrict rip, *restrict opa = op, *restrict opb = op + 1;
 float	*restrict cpa = cp, *restrict cpb = cp;
 float	out0, out1, out2, out3, out4, out5, out6, out7; 
 int	i, j, lst = nt / 2 - 8, rst = nt / 2 + 8;

 lip = ip;
 rip = lip + nt - 2;

 for (i = 0; i < ns / 8; i++) {
	out0 = out1 = out2 = out3 = out4 = out5 = out6 = out7 = 0.0f;
	for (j = 0; j < nt / 4; j++) {
		out0 += cpa[0] * (lip[0] + rip[1]) + cpa[1] * (lip[1] + rip[0]);
		out1 += cpb[0] * (lip[1] + rip[2]) + cpb[1] * (lip[2] + rip[1]); 
		out2 += cpa[0] * (lip[2] + rip[3]) + cpa[1] * (lip[3] + rip[2]);
		out3 += cpb[0] * (lip[3] + rip[4]) + cpb[1] * (lip[4] + rip[3]);
		out4 += cpa[0] * (lip[4] + rip[5]) + cpa[1] * (lip[5] + rip[4]);
		out5 += cpb[0] * (lip[5] + rip[6]) + cpb[1] * (lip[6] + rip[5]);
		out6 += cpa[0] * (lip[6] + rip[7]) + cpa[1] * (lip[7] + rip[6]);
		out7 += cpb[0] * (lip[7] + rip[8]) + cpb[1] * (lip[8] + rip[7]);
		cpa += 2;
		cpb += 2;
		lip += 2;
		rip -= 2;
	}
	opa[0] = out0;
	opb[0] = out1;
	opa[2] = out2;
	opb[2] = out3;
	opa[4] = out4;
	opb[4] = out5;
	opa[6] = out6;
	opb[6] = out7;

	opa += 8;
	opb += 8;
	cp -= nt / 2;
	lip -= lst;
	rip += rst;
 }		
}
