*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  

	.global	_upsamp2xSymmetricOdd8Msample

_upsamp2xSymmetricOdd8Msample

		;	parameters	iptr, cptr, optr, scnt, ntap
		;	iptr:	pointer to delay(filter state) buffer immediately followed by the input buffer
		;			where the FIR computation starts.  sptr should be aligned at double-word boundary.
		;	cptr:	pointer to coefficient buffer.  cptr should be aligned at double-word boundary.
		;	optr:	pointer to output buffer. optr should be aligned at word boundary.
		;	scnt:	number of output samples to process.  scnt should be a integer multiple of 8.
		;	ntap:	number of taps in the FIR filter.  This also dictates delay buffer size.
		;			ntap should be 8N + 19 where N is an non-negative integer.
		
		.asg	a4,iptra
		.asg	b4,iptrb
		.asg	a15,ioffa
		.asg	b21,ioffb
		.asg	a5,cptra
		.asg	b5,cptrb
		.asg	a22,coffa
		.asg	b3,coffb
		.asg	a6,optra
		.asg	b6,optrb

		.asg	a0,ntap
		.asg	a21,tcnt
		.asg	a2,iter
		.asg	b0,scnt
		.asg	b2,oav
		.asg	a1,first
				
		.asg 	a9,c1
		.asg	a8,c3
		.asg	b9,c0
		.asg	b8,c2

		.asg	a7,suma
		.asg	b1,sumb

		.asg	a10,proda
		.asg	b10,prodb
		
		.asg	a11,o0
		.asg	a12,o2
		.asg	a13,o4
		.asg	a14,o6
		.asg	b11,o1		
		.asg	b12,o3
		.asg	b13,o5
		.asg	b14,o7

		.asg	a26,o0w	
		.asg	a27,o2w
		.asg	a28,o4w
		.asg	a29,o6w
		.asg	b26,o1w
		.asg	b27,o3w
		.asg	b28,o5w
		.asg	b29,o7w
		.asg	a25,nula
		.asg	b25,nulb

		.asg	a16,l0
		.asg	a17,l1
		.asg	a18,l2
		.asg	a19,l3
		.asg	a20,l4
		.asg	b20,r0
		.asg	b16,r1
		.asg	b17,r2
		.asg	b18,r3
		.asg	b19,r4
		.asg	b7,r5
				
		mvc		csr,b0
		and		1,b0,b1
		clr		b0,0,1,b0
 [b1]	or		2,b0,b0
		mvc		b0,csr

; reassign parameters and save registers on stack

		stw		a10,*b15--[1]
		stw		b10,*b15--[1]
		stw		a11,*b15--[1]
		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
		stw		a13,*b15--[1]
		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]

		mv		a8,ntap
		mv		b4,cptrb
		add		cptrb,4,cptra

		sub		b6,8,scnt

	; set pointers, constants, loops, etc

		add		optra,4,optrb
		zero	oav
		shr		ntap,1,coffa		; NTAP = 8M + 19, coffa = ntap / 2 = 4M + 9
		sub		coffa,1,coffa		
		mv		coffa,coffb			; coffa = coffb = 4M + 8
		shr		coffa,2,ioffa		; ioffa = M + 2
		sub		ioffa,1,ioffa		; ioffa = M + 1
		shr		ntap,2,ioffb		; ioffb = 2M + 4
		add		ioffb,2,ioffb		; ioffb = 2M + 6
		sub		coffb,1,iptrb		; iptrb = 4M + 7
		shl		iptrb,2,iptrb
		add		iptrb,iptra,iptrb	; iptrb = iptra + 4M + 7
		sub		ntap,15,tcnt		; tcnt = 8M + 19 - 15 = 8M + 4
		shr		tcnt,1,tcnt			; tcnt = 4M + 2
		sub.s	tcnt,2,tcnt			; tcnt = 4M
		zero	nula
		zero	nulb
		zero	o0
		zero	o1
		zero	o2
		zero	o3
		zero	o4
		zero	o5
		zero	o6
		zero	o7
		zero	first
		mvk		1,oav

LOOP1_PROLOG

		ldw		*iptrb++[1],r0
		ldw		*iptrb++[1],r1

LOOP1
			mpysp	l0,c1,proda
||			mpysp	suma,c0,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3
||			lddw	*iptra++[1],l1:l0
||			ldw		*iptrb++[1],r2

			mpysp	l1,c1,proda
||			mpysp	suma,c0,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5
||			lddw	*iptra++[1],l3:l2
||			ldw		*iptrb++[1],r3

			mpysp	l2,c1,proda
||			mpysp	suma,c0,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7
||			ldw		*iptra,l4
||			ldw		*iptrb++[1],r4

			mpysp	l3,c1,proda
||			mpysp	suma,c0,prodb
||			ldw		*cptra++[2],c1
||			ldw		*iptrb--[7],r5

			addsp	o0,proda,o0w
||			addsp	o1,prodb,o1w
||			mpysp	nula,o0,o0
||			mpysp	nulb,o1,o1
||			ldw		*cptra++[2],c3
||			ldw		*cptrb++[2],c0

			addsp	o2,proda,o2w
||			addsp	o3,prodb,o3w
||			mpysp	nula,o2,o2
||			mpysp	nulb,o3,o3
||			ldw		*cptrb++[2],c2
||			addsp	l0,r1,suma
||			addsp	l0,r2,sumb
||			mv		tcnt,iter

			addsp	o4,proda,o4w
||			addsp	o5,prodb,o5w
||			mpysp	nula,o4,o4
||			mpysp	nulb,o5,o5
||			addsp	l1,r2,suma
||			addsp	l1,r3,sumb
||			add.d	first,1,first

			addsp	o6,proda,o6w
||			addsp	o7,prodb,o7w
||			mpysp	nula,o6,o6
||			mpysp	nulb,o7,o7
||			ldw		*iptrb++[1],r0
||			addsp	l2,r3,suma
||			addsp	l2,r4,sumb

			ldw		*iptrb++[1],r1
||			addsp	l3,r4,suma
||			addsp	l3,r5,sumb

LOOP2
				ldw		*iptrb--[4],r2
||				addsp	l1,r0,suma
||				addsp	l1,r1,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c0,prodb
||	[!first]	addsp	o2,proda,o2
||	[!first]	addsp	o3,prodb,o3

				lddw	*iptra++[1],l3:l2
||				ldw		*cptrb++[2],c0
||				addsp	l2,r1,suma
||				addsp	l2,r2,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c0,prodb
||	[!first]	addsp	o4,proda,o4
||	[!first]	addsp	o5,prodb,o5

				ldw		*iptra,l4
||				mv		r3,r5
||				addsp	l3,r2,suma
||				addsp	l3,r3,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c0,prodb
||	[!first]	addsp	o6,proda,o6
||	[!first]	addsp	o7,prodb,o7

				ldw		*cptra++[2],c1
||				mv		r1,r3
||				addsp	l4,r3,suma
||				addsp	l4,r4,sumb
||				mpysp	suma,c1,proda
||				mpysp	sumb,c0,prodb
||	[iter]		b		LOOP2

				mv		l2,l0
||				mv		r2,r4
||				mpysp	suma,c3,proda
||				mpysp	sumb,c2,prodb
||				addsp	o0,proda,o0
||				addsp	o1,prodb,o1

				mv		l3,l1
||				ldw		*cptrb++[2],c2
||				addsp	l0,r1,suma
||				addsp	l0,r2,sumb
||				mpysp	suma,c3,proda
||				mpysp	sumb,c2,prodb
||				addsp	o2,proda,o2
||				addsp	o3,prodb,o3

				ldw		*cptra++[2],c3
||				addsp	l1,r2,suma
||				addsp	l1,r3,sumb
||				mpysp	suma,c3,proda
||				mpysp	sumb,c2,prodb
||				addsp	o4,proda,o4
||				addsp	o5,prodb,o5

				ldw		*iptrb++[1],r0
||				addsp	l2,r3,suma
||				addsp	l2,r4,sumb
||				mpysp	suma,c3,proda
||				mpysp	sumb,c2,prodb
||				addsp	o6,proda,o6
||				addsp.l	o7,prodb,o7
||				zero	first

				ldw		*iptrb++[1],r1
||				addsp	l3,r4,suma
||				addsp	l3,r5,sumb
||				addsp	o0,proda,o0
||				addsp	o1,prodb,o1
||	[iter]		sub		iter,4,iter

LOOP2_END

			mv		l2,l0
||	[!oav]	stw		o1w,*optrb++[2]
||			addsp	l1,r0,suma
||			addsp	l1,r1,sumb
||			mpysp	suma,c1,proda
||			mpysp	sumb,c0,prodb
||			addsp	o2,proda,o2
||			addsp	o3,prodb,o3

			lddw	*iptra--[ioffa],l3:l2
||	[!oav]	stw		o3w,*optrb++[2]
||			addsp	l2,r1,suma
||			addsp	l2,r2,sumb
||			mpysp	suma,c1,proda
||			mpysp	sumb,c0,prodb
||			addsp	o4,proda,o4
||			addsp.l	o5,prodb,o5

			ldw		*cptrb--[coffb],c0
||			addsp	l3,r2,suma
||			addsp	l3,r3,sumb
||			mpysp	suma,c1,proda
||			mpysp	sumb,c0,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7

			mv		l3,l1
||			mv		r3,r5
||			addsp	l4,r3,suma
||			addsp	l4,r4,sumb
||			mpysp	suma,c1,proda
||			mpysp	sumb,c0,prodb
||	[scnt]	b		LOOP1

			ldw		*cptra--[coffa],c1
||	[!oav]	stw		o5w,*optrb++[2]
||			mpysp	suma,c3,proda
||			mpysp	sumb,c2,prodb
||			addsp.l	o0,proda,o0
||			addsp.l	o1,prodb,o1

	[!oav]	stw		o0w,*optra++[2]
||	[!oav]	stw		o7w,*optrb++[2]
||			addsp	l0,l1,suma
||			mpysp	suma,c3,proda
||			mpysp	sumb,c2,prodb
||			addsp.l	o2,proda,o2
||			addsp.l	o3,prodb,o3

	[!oav]	stw		o2w,*optra++[2]
||			addaw	iptrb,ioffb,iptrb
||			addsp	l1,l2,suma
||			mpysp	suma,c3,proda
||			mpysp	sumb,c2,prodb
||			addsp	o4,proda,o4
||			addsp	o5,prodb,o5

	[!oav]	stw		o4w,*optra++[2]
||	[scnt]	ldw		*iptrb++[1],r0
||			addsp	l2,l3,suma
||			mpysp	suma,c3,proda
||			mpysp	sumb,c2,prodb
||			addsp	o6,proda,o6
||			addsp	o7,prodb,o7

	[!oav]	stw		o6w,*optra++[2]
||	[scnt]	ldw		*iptrb++[1],r1
||			addsp	l3,r5,suma		
||			addsp	o0,proda,o0
||			addsp	o1,prodb,o1
||			mpysp	nulb,oav,oav
||	[scnt]	sub.s	scnt,8,scnt

LOOP1_EPILOG
		
		mpysp	l0,c1,proda
||		mpysp	suma,c0,prodb
||		addsp	o2,proda,o2
||		addsp.l	o3,prodb,o3

		mpysp	l1,c1,proda
||		mpysp	suma,c0,prodb
||		addsp	o4,proda,o4
||		addsp	o5,prodb,o5

		mpysp	l2,c1,proda
||		mpysp	suma,c0,prodb
||		addsp	o6,proda,o6
||		addsp	o7,prodb,o7

		mpysp	l3,c1,proda
||		mpysp	suma,c0,prodb

		addsp	o0,proda,o0
||		addsp	o1,prodb,o1

		addsp	o2,proda,o2
||		addsp	o3,prodb,o3

		addsp	o4,proda,o4
||		addsp	o5,prodb,o5

		addsp	o6,proda,o6
||		addsp	o7,prodb,o7

		stw		o0,*optra++[2]
||		stw		o1,*optrb++[2]

		stw		o2,*optra++[2]
||		stw		o3,*optrb++[2]

		stw		o4,*optra++[2]
||		stw		o5,*optrb++[2]

		stw		o6,*optra++[2]
||		stw		o7,*optrb++[2]

LOOP1_END

		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
		ldw		*++b15[1],b14
		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
		ldw		*++b15[1],a12
		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
		mvc		csr,b0
		extu	b0,1,1,b1
 [b1]	set		b0,0,0,b0
 		mvc		b0,csr