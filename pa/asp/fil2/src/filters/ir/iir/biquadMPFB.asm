*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  

			.global _biquadFB4chMP 

_biquadFB4chMP	;		sptr	iptr	cptr	cnt		optr0	optr1	optr2	optr3
				;		a4		b4		a6		b6		a8		b8		a10		b10

	.asg	a4,sptr
	.asg	a5,iptra
	.asg	b4,iptrb
	.asg	a6,cptr
	.asg	b0,cnt
	.asg	a8,op0
	.asg	b8,op1
	.asg	a10,op2
	.asg	b10,op3

	.asg	a11,outa
	.asg	b9,outb

	.asg	a12,a1a
	.asg	a13,a2a
	.asg	a14,a1c
	.asg	a15,a2c
	.asg	b11,a1b
	.asg	b12,a2b
	.asg	b13,a1d
	.asg	b14,a2d

	.asg	a17:a16,Xa
	.asg	b17:b16,Xb
	.asg	a18,prdal
	.asg	a19,prdah
	.asg	b18,prdbl
	.asg	b19,prdbh

	.asg	a20,y00l
	.asg	a21,y00h
	.asg	a22,y10l
	.asg	a23,y10h
	.asg	a24,y20l
	.asg	a25,y20h
	.asg	a26,y02l
	.asg	a27,y02h
	.asg	a28,y12l
	.asg	a29,y12h
	.asg	a30,y22l
	.asg	a31,y22h
	
	.asg	b20,y01l
	.asg	b21,y01h
	.asg	b22,y11l
	.asg	b23,y11h
	.asg	b24,y21l
	.asg	b25,y21h
	.asg	b26,y03l
	.asg	b27,y03h
	.asg	b28,y13l
	.asg	b29,y13h
	.asg	b30,y23l
	.asg	b31,y23h

	.asg	prdah:prdal,prda
	.asg	prdbh:prdbl,prdb
	.asg	y00h:y00l,y00
	.asg	y10h:y10l,y10
	.asg	y20h:y20l,y20
	.asg	y01h:y01l,y01
	.asg	y11h:y11l,y11
	.asg	y21h:y21l,y21
	.asg	y02h:y02l,y02
	.asg	y12h:y12l,y12
	.asg	y22h:y22l,y22
	.asg	y03h:y03l,y03
	.asg	y13h:y13l,y13
	.asg	y23h:y23l,y23

		mvc		csr,b0
		and		1,b0,b1
		clr		b0,0,1,b0
 [b1]	or		2,b0,b0
		mvc		b0,csr
						
		stw		a10,*b15--[1]
		stw		b10,*b15--[1]
		stw		a11,*b15--[1]
		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
		stw		a13,*b15--[1]
		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]

		; reassign parameters

		sub		b6,1,cnt
		add		iptrb,8,iptrb
||		mv		iptrb,iptra

		; load coefficients

		ldw		*cptr[0],a1a
		ldw		*cptr[1],a2a
		ldw		*cptr[2],a1b
		ldw		*cptr[3],a2b
		ldw		*cptr[4],a1c
		ldw		*cptr[5],a2c
		ldw		*cptr[6],a1d
		ldw		*cptr[7],a2d

		; load filter states

		lddw	*sptr[1],y20
		lddw	*sptr[3],y21
		lddw	*sptr[0],y10
		lddw	*sptr[2],y11
		lddw	*sptr[5],y22
		lddw	*sptr[7],y23
		lddw	*sptr[4],y12
		lddw	*sptr[6],y13

PROLOG

		mpyspdp		a2a,y20,prda
||		mpyspdp		a2b,y21,prdb

		lddw		*iptra++[2],Xa
||		lddw		*iptrb++[2],Xb
||		mv			y10l,y20l
||		mv			y11l,y21l

		mv			y10h,y20h
||		mv			y11h,y21h

		mpyspdp		a2c,y22,prda
||		mpyspdp		a2d,y23,prdb

		lddw		*iptra++[2],Xa
||		lddw		*iptrb++[2],Xb
||		mv			y12l,y22l
||		mv			y13l,y23l,

		mv.d		y12h,y22h
||		mv.d		y13h,y23h

		mpyspdp		a1a,y10,prda
||		mpyspdp		a1b,y11,prdb
||		adddp.l		prda,Xa,y00
||		adddp.l		prdb,Xb,y01

		nop			2

		mpyspdp		a1c,y12,prda
||		mpyspdp		a1d,y13,prdb
||		adddp.l		prda,Xa,y02
||		adddp.l		prdb,Xb,y03

		nop			2

		mpyspdp		a2a,y20,prda
||		mpyspdp		a2b,y21,prdb
||		adddp.l		prda,y00,y00
||		adddp.l		prdb,y01,y01

		lddw		*iptra++[2],Xa
||		lddw		*iptrb++[2],Xb
||		mv			y10l,y20l
||		mv			y11l,y21l

		mv.d		y10h,y10h
||		mv.d		y11h,y21h

		lddw		*iptra++[2],Xa
||		lddw		*iptrb++[2],Xb
||		mpyspdp		a2c,y22,prda
||		mpyspdp		a2d,y23,prdb
||		adddp.l		prda,y02,y02
||		adddp.l		prdb,y03,y03

LOOP
			mv.d		y12l,y22l
||			mv.d		y13l,y23l

			mv.d		y12h,y22h
||			mv.d		y13h,y23h

			mv.d		y00l,y10l
||			mv.d		y01l,y11l
||			mpyspdp		a1a,y00,prda		; effectively a1 * y1
||			mpyspdp		a1b,y01,prdb
||			adddp.s		prda,Xa,y00
||			adddp.s		prdb,Xb,y01

			mv.d		y00h,y10h
||			mv.d		y01h,y11h
||			dpsp		y00,outa
||			dpsp		y01,outb

			sub.d		cnt,1,cnt

			mv.d		y02l,y12l
||			mv.d		y03l,y13l
||			mpyspdp		a1c,y02,prda		; effectively a1 * y1
||			mpyspdp		a1d,y03,prdb
||			adddp.s		prda,Xa,y02
||			adddp.s		prdb,Xb,y03

			mv.d		y02h,y12h
||			mv.d		y03h,y13h
||			dpsp		y02,outa
||			dpsp		y03,outb
|| [cnt]	b			LOOP

			stw			outa,*op0++
||			stw			outb,*op1++

			lddw		*iptra++[2],Xa
||			lddw		*iptrb++[2],Xb
||			mpyspdp		a2a,y10,prda			; effectively y2 of next iteration
||			mpyspdp		a2b,y11,prdb
||			adddp.l		prda,y00,y00
||			adddp.l		prdb,y01,y01

			mv.d		y10l,y20l
||			mv.d		y11l,y21l

			stw			outa,*op2++
||			stw			outb,*op3++
||			mv.l		y10h,y20h
||			mv.l		y11h,y21h

			lddw		*iptra++[2],Xa			
||			lddw		*iptrb++[2],Xb
||			mpyspdp		a2c,y12,prda			; effectively y2 of next iteration
||			mpyspdp		a2d,y13,prdb
||			adddp.l		prda,y02,y02
||			adddp.l		prdb,y03,y03

EPILOG

		nop			3

		dpsp		y00,outa
||		dpsp		y01,outb

		stw			y00l,*sptr[0]
		stw			y00h,*sptr[1]

		dpsp		y02,outa
||		dpsp		y03,outb

		stw			outa,*op0++

		stw			outb,*op1++

		nop			

		stw			outa,*op2++

		stw			outb,*op3++

LOOP_END
			; save filter states
			
		stw			y10l,*sptr[2]
		stw			y10h,*sptr[3]
		stw			y01l,*sptr[4]
		stw			y01h,*sptr[5]
		stw			y11l,*sptr[6]
		stw			y11h,*sptr[7]
		stw			y02l,*sptr[8]
		stw			y02h,*sptr[9]
		stw			y12l,*sptr[10]
		stw			y12h,*sptr[11]
		stw			y03l,*sptr[12]
		stw			y03h,*sptr[13]
		stw			y13l,*sptr[14]
		stw			y13h,*sptr[15]
							
PREPARE_RETURN:
		
		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
		ldw		*++b15[1],b14
		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
		ldw		*++b15[1],a12
		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
		mvc		csr,b0
		extu	b0,1,1,b1
 [b1]	set		b0,0,0,b0
 		mvc		b0,csr
             