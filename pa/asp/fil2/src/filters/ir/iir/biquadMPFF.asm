*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  

			.global _biquadFF1chMP 

_biquadFF1chMP	;		sptr		cptr		iptr		optr		cnt		idx
				;		a4			b4			a6			b6			a8		b8

	.asg	a4,sptr
	.asg	b4,cptr
	.asg	a5,ipa
	.asg	a6,opa
	.asg	b5,ipb
	.asg	b6,opb

	.asg	b0,cnt
	.asg	a1,ia
	.asg	b1,ib

	.asg	a31,s3ah
	.asg	a30,s3al
	.asg	a29,s2ah
	.asg	a28,s2al
	.asg	a27,s1ah
	.asg	a26,s1al
	.asg	a25,s0ah
	.asg	a24,s0al
	.asg	a23,prdah
	.asg	a22,prdal
	.asg	b31,s3bh
	.asg	b30,s3bl
	.asg	b29,s2bh
	.asg	b28,s2bl
	.asg	b27,s1bh
	.asg	b26,s1bl
	.asg	b25,s0bh
	.asg	b24,s0bl
	.asg	b23,prdbh
	.asg	b22,prdbl

	.asg	a21,x3a
	.asg	a20,x2a
	.asg	a19,x1a
	.asg	a18,x0a
	.asg	b21,x3b
	.asg	b20,x2b
	.asg	b19,x1b
	.asg	b18,x0b
	
	.asg	a16,c2
	.asg	b16,c1
	.asg	a15,c0

	.asg	s3ah:s3al,s3a
	.asg	s2ah:s2al,s2a
	.asg	s1ah:s1al,s1a
	.asg	s0ah:s0al,s0a
	.asg	prdah:prdal,prda
	.asg	s3bh:s3bl,s3b
	.asg	s2bh:s2bl,s2b
	.asg	s1bh:s1bl,s1b
	.asg	s0bh:s0bl,s0b
	.asg	prdbh:prdbl,prdb

		mvc		csr,b0
		and		1,b0,b1
		clr		b0,0,1,b0
 [b1]	or		2,b0,b0
		mvc		b0,csr
						
		stw		a10,*b15--[1]
		stw		b10,*b15--[1]
		stw		a11,*b15--[1]
		stw		b11,*b15--[1]
		stw		a12,*b15--[1]
		stw		b12,*b15--[1]
		stw		a13,*b15--[1]
		stw		b13,*b15--[1]
		stw		a14,*b15--[1]
		stw		b14,*b15--[1]
		stw		a15,*b15--[1]
		stw		b3,*b15--[1]

		; reassign parameters

		mpy			a8,b8,b16		; b16 = cnt * spacing = size of DP out array in DW
		shl			b8,1,ia
		sub			ia,1,ia
		add			ia,2,ib
		mv			a6,ipa
		sub			a8,8,cnt
		shl			a8,1,b8			; b8 = cnt * 2
		add			ipa,b8,ipb
		mv			b6,opa
		addaw		opb,b16,opb		; IP/OP set up for side A/B
		addaw		opb,1,opb

		; load filter states/coefficients

		ldw			*cptr[0],c2
		ldw			*sptr[0],x3a
		ldw			*ipb[-2],x3b
		ldw			*sptr[1],x2a
		ldw			*ipb[-1],x2b
		ldw			*cptr[1],c1
		ldw			*cptr[2],c0
			
PROLOG
		ldw			*ipb++,x1b

		ldw			*ipa++,x1a

		ldw			*ipb++,x0b
||		mpysp2dp	c2,x3a,s0a
||		mpysp2dp	c2,x3b,s0b

		ldw			*ipa++,x0a

		mpysp2dp	c2,x2a,s1a
||		mpysp2dp	c2,x2b,s1b

		nop
		
		mpysp2dp	c2,x1a,s2a
||		mpysp2dp	c2,x1b,s2b

		nop
		
		mpysp2dp	c2,x0a,s3a
||		mpysp2dp	c2,x0b,s3b

		ldw			*ipb++,x3b

		mpysp2dp	c1,x2a,prda
||		mpysp2dp	c1,x2b,prdb

		ldw			*ipa++,x3a

		mpysp2dp	c1,x1a,prda
||		mpysp2dp	c1,x1b,prdb

		nop

		mpysp2dp	c1,x0a,prda
||		mpysp2dp	c1,x0b,prdb
||		adddp		prda,s0a,s0a
||		adddp		prdb,s0b,s0b

		nop

		mpysp2dp	c1,x3a,prda
||		mpysp2dp	c1,x3b,prdb
||		adddp		prda,s1a,s1a
||		adddp		prdb,s1b,s1b

		ldw			*ipb++,x2b

		mpysp2dp	c0,x1a,prda
||		mpysp2dp	c0,x1b,prdb
||		adddp		prda,s2a,s2a
||		adddp		prdb,s2b,s2b

		ldw			*ipa++,x2a		

		mpysp2dp	c0,x0a,prda
||		mpysp2dp	c0,x0b,prdb
||		adddp		prda,s3a,s3a
||		adddp		prdb,s3b,s3b

		nop

		mpysp2dp	c0,x3a,prda
||		mpysp2dp	c0,x3b,prdb
||		adddp		prda,s0a,s0a
||		adddp		prdb,s0b,s0b

LOOP		
			ldw			*ipb++,x1b

			mpysp2dp	c0,x2a,prda
||			mpysp2dp	c0,x2b,prdb
||			adddp.s		prda,s1a,s1a
||			adddp.s		prdb,s1b,s1b
||			ldw			*ipa++,x1a

			ldw			*ipb++,x0b

			mpysp2dp	c2,x3a,prda
||			mpysp2dp	c2,x3b,prdb
||			adddp.s		prda,s2a,s2a
||			adddp.s		prdb,s2b,s2b
||			ldw			*ipa++,x0a

			ldw			*ipb++,x3b

			ldw			*ipa++,x3a
||			mpysp2dp	c2,x2a,prda
||			mpysp2dp	c2,x2b,prdb
||			adddp.s		prda,s3a,s3a
||			adddp.s		prdb,s3b,s3b

			stw			s0al,*opa++
||			stw			s0bh,*opb--

			stw			s0ah,*opa++[ia]
||			stw			s0bl,*opb++[ib]
||			mpysp2dp	c2,x1a,prda
||			mpysp2dp	c2,x1b,prdb
||			mv.l		prdal,s0al
||			mv.l		prdbl,s0bl

			stw			s1al,*opa++
||			stw			s1bh,*opb--
||			mv.l		prdah,s0ah
||			mv.l		prdbh,s0bh

			stw			s1ah,*opa++[ia]
||			stw			s1bl,*opb++[ib]
||			mpysp2dp	c2,x0a,prda
||			mpysp2dp	c2,x0b,prdb
||			mv.l		prdal,s1al
||			mv.l		prdbl,s1bl

			stw			s2al,*opa++
||			stw			s2bh,*opb--
||			mv.l		prdah,s1ah
||			mv.l		prdbh,s1bh

			stw			s2ah,*opa++[ia]
||			stw			s2bl,*opb++[ib]
||			mpysp2dp	c1,x2a,prda
||			mpysp2dp	c1,x2b,prdb
||			mv.l		prdal,s2al
||			mv.l		prdbl,s2bl

			stw			s3al,*opa++
||			stw			s3bh,*opb--
||			mv.l		prdah,s2ah
||			mv.l		prdbh,s2bh

			stw			s3ah,*opa++[ia]
||			stw			s3bl,*opb++[ib]
||			mpysp2dp	c1,x1a,prda
||			mpysp2dp	c1,x1b,prdb
||			mv.l		prdal,s3al
||			mv.l		prdbl,s3bl

			mv.l		prdah,s3ah
||			mv.l		prdbh,s3bh

			mpysp2dp	c1,x0a,prda
||			mpysp2dp	c1,x0b,prdb
||			adddp.s		prda,s0a,s0a
||			adddp.s		prdb,s0b,s0b

			sub.l		cnt,8,cnt

			mpysp2dp	c1,x3a,prda
||			mpysp2dp	c1,x3b,prdb
||			adddp.l		prda,s1a,s1a
||			adddp.l		prdb,s1b,s1b
||			ldw			*ipb++,x2b

	[cnt]	b			LOOP		
||			ldw			*ipa++,x2a

			mpysp2dp	c0,x1a,prda
||			mpysp2dp	c0,x1b,prdb
||			adddp.l		prda,s2a,s2a
||			adddp.l		prdb,s2b,s2b

			nop

			mpysp2dp	c0,x0a,prda
||			mpysp2dp	c0,x0b,prdb
||			adddp.s		prda,s3a,s3a
||			adddp.s		prdb,s3b,s3b

			nop

			mpysp2dp	c0,x3a,prda
||			mpysp2dp	c0,x3b,prdb
||			adddp.s		prda,s0a,s0a
||			adddp.s		prdb,s0b,s0b

END
EPILOG
		nop

		mpysp2dp	c0,x2a,prda
||		mpysp2dp	c0,x2b,prdb
||		adddp		prda,s1a,s1a
||		adddp		prdb,s1b,s1b

		stw			x3b,*sptr[0]				; save filter states

		adddp		prda,s2a,s2a
||		adddp		prdb,s2b,s2b

		stw			x2b,*sptr[1]				; save filter states

		adddp		prda,s3a,s3a
||		adddp		prdb,s3b,s3b

		stw			s0al,*opa++
||		stw			s0bh,*opb--

		stw			s0ah,*opa++[ia]
||		stw			s0bl,*opb++[ib]

		stw			s1al,*opa++
||		stw			s1bh,*opb--

		stw			s1ah,*opa++[ia]
||		stw			s1bl,*opb++[ib]

		stw			s2al,*opa++
||		stw			s2bh,*opb--

		stw			s2ah,*opa++[ia]
||		stw			s2bl,*opb++[ib]

		stw			s3al,*opa++
||		stw			s3bh,*opb--

		stw			s3ah,*opa++[ia]
||		stw			s3bl,*opb++[ib]

LOOP_END
PREPARE_RETURN:
		
		ldw		*++b15[1],b3
		ldw		*++b15[1],a15
		ldw		*++b15[1],b14
		ldw		*++b15[1],a14
		ldw		*++b15[1],b13
		ldw		*++b15[1],a13
		ldw		*++b15[1],b12
		ldw		*++b15[1],a12
		ldw		*++b15[1],b11
		ldw		*++b15[1],a11
		ldw		*++b15[1],b10
		b 		b3
		ldw		*++b15[1],a10
		mvc		csr,b0
		extu	b0,1,1,b1
 [b1]	set		b0,0,0,b0
 		mvc		b0,csr
             