/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* ======================================================================== */
/*                                                                          */
/*  NAME                                                                    */
/*      Filter_iirT1Ch8_u -- Filter_iirT1Ch8_u                                */
/*                                                                          */
/*S AUTHOR                                                                 S*/
/*S     TII                                                                S*/
/*S                                                                        S*/
/*S REVISION HISTORY                                                       S*/
/*S     15-Jun-2004 Initial revision . . . . . . . . . . .  Autogen        S*/
/*S                                                                        S*/
/* SAGE                                                                     */
/*                                                                          */
/*   This routine has following C prototype:                                */
/*                                                                          */
/*   void Filter_iirT1Ch8_u(PAF_FilParam *pParam)                            */
/*                                                                          */
/*   where pParam� is a pointer to the lower layer generic filter handle    */
/*   structure �PAF_FilParam�.                                              */
/*                                                                          */
/*   typedef struct PAF_FilParam {                                          */
/*       void  ** pIn ;                                                     */
/*       void  ** pOut ;                                                    */
/*       void  ** pCoef ;                                                   */
/*       void  ** pVar ;                                                    */
/*       int      sampleCount;                                              */
/*       uchar    channels;                                                 */
/*       uint     use;                                                      */
/*    }  PAF_FilParam;                                                      */
/*                                                                          */
/*  Let N be the maximum number of channels that are processed at a time.   */
/*                                                                          */
/*  pIn  -�Pointer to the array of �input channel data� pointers.           */
/*                                                                          */
/*     pIn [0][X0(n)], where X(n) is the input data block, of length �      */
/*         [1][ X1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ XN(n) ]                                                   */
/*                                                                          */
/*  pOut�- Pointer to the array of �output channel data� pointers.          */
/*                                                                          */
/*     pOut[0][Y0(n)]   , where Y(n) is the input data block, of length     */
/*         [1][ Y1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ YN(n) ]                                                   */
/*                                                                          */
/*                                                                          */
/*  pCoef - Pointer to the array of�channel coefficient� pointers.          */
/*                                                                          */
/*     pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of        */
/*          [1][ C1(n) ]                                      a channel.    */
/*          ............                                                    */
/*          [N-1][ CN(n) ]                                                  */
/*                                                                          */
/*  pVar - Pointer to the array of channel state/private memory�pointers.   */
/*                                                                          */
/*     pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to           */
/*          [1][ m1(n) ]                                        a channel.  */
/*         ............                                                     */
/*          [N-1][ mN(n) ]                                                  */
/*                                                                          */
/*  channels - Specifies the number of channels that are to be filtered,    */
/*             starting sequentially from channel 0.                        */
/*                                                                          */
/*  sampleCount - Specifies the sample length of the input data block.      */
/*                                                                          */
/*  use - This is a 32 bit field that can be used, to fit in some extra     */
/*         parameters or as a pointer to additional filter parameters.      */
/*                                                                          */
/*                                                                          */
/* ESCRIPTION                                                               */
/*                                                                          */
/*     This routine implements IIR filter implementation for tap-1 &        */
/*     channels-8, unicoefficient,SP.                                       */
/*                                                                          */
/*                 ORDER 1    and CHANNELS 8                                */
/*                                                                          */
/* lter equation  : H(z) =  b0 + b1*z~1                                     */
/*                         -------------                                    */
/*                          1  - a1*z~1                                     */
/*                                                                          */
/* Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                  */
/*                                                                          */
/* Canonical form : w(n) = x(n)    + a1*w(n-1)                              */
/*                  y(n) = b0*w(n) + b1*w(n-1)                              */
/*                                                                          */
/* Filter variables :                                                       */
/*    y(n) - *y,         x(n)   - *x                                        */
/*    w(n) -  w,         w(n-1) - w1                                        */
/*    b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                  */
/*                                                                          */
/*                                                                          */
/*                            w(n)    b0                                    */
/*   x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                  */
/*              |              |              |                             */
/*              ^              v              ^                             */
/*              |     a1    +-----+    b1     |                             */
/*               -----<<----| Z~1 |---->>-----                              */
/*                          +-----+                                         */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/* C CODE                                                                   */
/*                                                                          */
/*  void Filter_iirT1Ch8_u( PAF_FilParam *pParam )                           */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*      int count, samp; // Loop counters                                   */
/*      const float * filtCfsB, *filtCfsA; // Coeff ptrs                    */
/*      float *restrict filtVars_1,*restrict filtVars_2; // Filter var mem  */
/*      float *restrict filtVars_3,*restrict filtVars_4; // Filter var mem  */
/*      float *restrict filtVars_5,*restrict filtVars_6; // Filter var mem  */
/*      float *restrict filtVars_7,*restrict filtVars_8; // Filter var mem  */
/*      const float *restrict x_1,*restrict x_2; // Input ptr               */
/*      const float *restrict x_3,*restrict x_4; // Input ptr               */
/*      const float *restrict x_5,*restrict x_6; // Input ptr               */
/*      const float *restrict x_7,*restrict x_8; // Input ptr               */
/*      float *restrict y_1,*restrict y_2; // Output ptr                    */
/*      float *restrict y_3,*restrict y_4; // Output ptr                    */
/*      float *restrict y_5,*restrict y_6; // Output ptr                    */
/*      float *restrict y_7,*restrict y_8; // Output ptr                    */
/*      float w_1, w1_1,w_2,w1_2; // Filter state regs                      */
/*      float w_3, w1_3,w_4,w1_4,w_5,w1_5; // Filter state regs             */
/*      float w_6, w1_6,w_7,w1_7,w_8,w1_8; // Filter state regs             */
/*                                                                          */
/*      x_1 = (float *)pParam->pIn[0]; // Get i/p ptr                       */
/*      x_2 = (float *)pParam->pIn[1]; // Get i/p ptr                       */
/*      x_3 = (float *)pParam->pIn[2]; // Get i/p ptr                       */
/*      x_4 = (float *)pParam->pIn[3]; // Get i/p ptr                       */
/*      x_5 = (float *)pParam->pIn[4]; // Get i/p ptr                       */
/*      x_6 = (float *)pParam->pIn[5]; // Get i/p ptr                       */
/*      x_7 = (float *)pParam->pIn[6]; // Get i/p ptr                       */
/*      x_8 = (float *)pParam->pIn[7]; // Get i/p ptr                       */
/*                                                                          */
/*      y_1 = (float *)pParam->pOut[0]; // Get o/p ptr                      */
/*      y_2 = (float *)pParam->pOut[1]; // Get o/p ptr                      */
/*      y_3 = (float *)pParam->pOut[2]; // Get o/p ptr                      */
/*      y_4 = (float *)pParam->pOut[3]; // Get o/p ptr                      */
/*      y_5 = (float *)pParam->pOut[4]; // Get o/p ptr                      */
/*      y_6 = (float *)pParam->pOut[5]; // Get o/p ptr                      */
/*      y_7 = (float *)pParam->pOut[6]; // Get o/p ptr                      */
/*      y_8 = (float *)pParam->pOut[7]; // Get o/p ptr                      */
/*                                                                          */
/*      filtCfsB = (float *)pParam->pCoef[0]; // Feed-forward coeff. ptr    */
/*      filtCfsA = filtCfsB + 2; // Derive feedback coeff ptr               */
/*                                                                          */
/*      filtVars_1 = (float *)pParam->pVar[0]; // Get filter var ptr        */
/*      filtVars_2 = (float *)pParam->pVar[1]; // Get filter var ptr        */
/*      filtVars_3 = (float *)pParam->pVar[2]; // Get filter var ptr        */
/*      filtVars_4 = (float *)pParam->pVar[3]; // Get filter var ptr        */
/*      filtVars_5 = (float *)pParam->pVar[4]; // Get filter var ptr        */
/*      filtVars_6 = (float *)pParam->pVar[5]; // Get filter var ptr        */
/*      filtVars_7 = (float *)pParam->pVar[6]; // Get filter var ptr        */
/*      filtVars_8 = (float *)pParam->pVar[7]; // Get filter var ptr        */
/*                                                                          */
/*      count = pParam->sampleCount; // I/p sample block-length             */
/*                                                                          */
/*      // Get the filter states into corresponding regs                    */
/*      w1_1 = filtVars_1[0]; // Temp copy of filter state                  */
/*      w1_2 = filtVars_2[0]; // Temp copy of filter state                  */
/*      w1_3 = filtVars_3[0]; // Temp copy of filter state                  */
/*      w1_4 = filtVars_4[0]; // Temp copy of filter state                  */
/*      w1_5 = filtVars_5[0]; // Temp copy of filter state                  */
/*      w1_6 = filtVars_6[0]; // Temp copy of filter state                  */
/*      w1_7 = filtVars_7[0]; // Temp copy of filter state                  */
/*      w1_8 = filtVars_8[0]; // Temp copy of filter state                  */
/*                                                                          */
/*      w_1  = w1_1;                                                        */
/*      w_2  = w1_2;                                                        */
/*      w_3  = w1_3;                                                        */
/*      w_4  = w1_4;                                                        */
/*      w_5  = w1_5;                                                        */
/*      w_6  = w1_6;                                                        */
/*      w_7  = w1_7;                                                        */
/*      w_8  = w1_8;                                                        */
/*                                                                          */
/*      // IIR filtering for i/p block length                               */
/*      #pragma MUST_ITERATE(16, 2048, 4)                                   */
/*      for(samp=0; samp<count; ++samp)                                     */
/*        {                                                                 */
/*                                                                          */
/*      //Channel-1                                                         */
/*      w_1 = x_1[samp] + filtCfsA[0]*w_1; // w(n)=x(n)+a1*w(n-1)           */
/*      y_1[samp] = filtCfsB[0]*w_1 + filtCfsB[1]*w1_1; // y(n)=b0*w(n)+b1* */
/*      w1_1  = w_1; // State reg shift                                     */
/*                                                                          */
/*      //Channel-2                                                         */
/*      w_2 = x_2[samp] + filtCfsA[0]*w_2;                                  */
/*      y_2[samp] = filtCfsB[0]*w_2 + filtCfsB[1]*w1_2;                     */
/*      w1_2  = w_2;                                                        */
/*                                                                          */
/*      //Channel-3                                                         */
/*      w_3 = x_3[samp] + filtCfsA[0]*w_3;                                  */
/*      y_3[samp] = filtCfsB[0]*w_3 + filtCfsB[1]*w1_3;                     */
/*      w1_3  = w_3;                                                        */
/*                                                                          */
/*      //Channel-4                                                         */
/*      w_4 = x_4[samp] + filtCfsA[0]*w_4;                                  */
/*      y_4[samp] = filtCfsB[0]*w_4 + filtCfsB[1]*w1_4;                     */
/*      w1_4  = w_4;                                                        */
/*                                                                          */
/*      //Channel-5                                                         */
/*      w_5 = x_5[samp] + filtCfsA[0]*w_5;                                  */
/*      y_5[samp] = filtCfsB[0]*w_5 + filtCfsB[1]*w1_5;                     */
/*      w1_5  = w_5;                                                        */
/*                                                                          */
/*      //Channel-6                                                         */
/*      w_6 = x_6[samp] + filtCfsA[0]*w_6;                                  */
/*      y_6[samp] = filtCfsB[0]*w_6 + filtCfsB[1]*w1_6;                     */
/*      w1_6  = w_6;                                                        */
/*                                                                          */
/*      //Channel-7                                                         */
/*      w_7 = x_7[samp] + filtCfsA[0]*w_7;                                  */
/*      y_7[samp] = filtCfsB[0]*w_7 + filtCfsB[1]*w1_7;                     */
/*      w1_7  = w_7;                                                        */
/*                                                                          */
/*      //Channel-8                                                         */
/*      w_8 = x_8[samp] + filtCfsA[0]*w_8;                                  */
/*      y_8[samp] = filtCfsB[0]*w_8 + filtCfsB[1]*w1_8;                     */
/*      w1_8  = w_8;                                                        */
/*        }                                                                 */
/*                                                                          */
/*      // Update state memory                                              */
/*      filtVars_1[0] = w_1;                                                */
/*      filtVars_2[0] = w_2;                                                */
/*      filtVars_3[0] = w_3;                                                */
/*      filtVars_4[0] = w_4;                                                */
/*      filtVars_5[0] = w_5;                                                */
/*      filtVars_6[0] = w_6;                                                */
/*      filtVars_7[0] = w_7;                                                */
/*      filtVars_8[0] = w_8;                                                */
/*                                                                          */
/*                                                                          */
/* OTES                                                                     */
/*                                                                          */
/*     1. Endian: This code is LITTLE ENDIAN.                               */
/*     2. interruptibility: This code is interrupt-tolerant but not         */
/*        interruptible.                                                    */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2004 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
#pragma CODE_SECTION(Filter_iirT1Ch8_u, ".text:optimized");

//#include "Filter_iirT1Ch8_u_o.h"
#include "filters.h"

/* ======================================================================== */
/*S Place file level definitions here.                                     S*/
/* ======================================================================== */

Int Filter_iirT1Ch8_u(PAF_FilParam * pParam)
{

        int count, samp; // Loop counters 
        const float * filtCfsB, *filtCfsA; // Coeff ptrs 
        float *restrict filtVars_1,*restrict filtVars_2; // Filter var mem ptr 
        float *restrict filtVars_3,*restrict filtVars_4; // Filter var mem ptr 
        float *restrict filtVars_5,*restrict filtVars_6; // Filter var mem ptr 
        float *restrict filtVars_7,*restrict filtVars_8; // Filter var mem ptr 
        const float *restrict x_1,*restrict x_2; // Input ptr 
        const float *restrict x_3,*restrict x_4; // Input ptr 
        const float *restrict x_5,*restrict x_6; // Input ptr 
        const float *restrict x_7,*restrict x_8; // Input ptr 
        float *restrict y_1,*restrict y_2; // Output ptr 
        float *restrict y_3,*restrict y_4; // Output ptr 
        float *restrict y_5,*restrict y_6; // Output ptr 
        float *restrict y_7,*restrict y_8; // Output ptr 
        float w_1, w1_1,w_2,w1_2; // Filter state regs 
        float w_3, w1_3,w_4,w1_4,w_5,w1_5; // Filter state regs 
        float w_6, w1_6,w_7,w1_7,w_8,w1_8; // Filter state regs 
        
        x_1 = (float *)pParam->pIn[0]; // Get i/p ptr 
        x_2 = (float *)pParam->pIn[1]; // Get i/p ptr 
        x_3 = (float *)pParam->pIn[2]; // Get i/p ptr 
        x_4 = (float *)pParam->pIn[3]; // Get i/p ptr 
        x_5 = (float *)pParam->pIn[4]; // Get i/p ptr 
        x_6 = (float *)pParam->pIn[5]; // Get i/p ptr 
        x_7 = (float *)pParam->pIn[6]; // Get i/p ptr 
        x_8 = (float *)pParam->pIn[7]; // Get i/p ptr 
        
        y_1 = (float *)pParam->pOut[0]; // Get o/p ptr 
        y_2 = (float *)pParam->pOut[1]; // Get o/p ptr 
        y_3 = (float *)pParam->pOut[2]; // Get o/p ptr 
        y_4 = (float *)pParam->pOut[3]; // Get o/p ptr 
        y_5 = (float *)pParam->pOut[4]; // Get o/p ptr 
        y_6 = (float *)pParam->pOut[5]; // Get o/p ptr 
        y_7 = (float *)pParam->pOut[6]; // Get o/p ptr 
        y_8 = (float *)pParam->pOut[7]; // Get o/p ptr 
   
        filtCfsB = (float *)pParam->pCoef[0]; // Feed-forward coeff. ptr 
        filtCfsA = filtCfsB + 2; // Derive feedback coeff ptr 

        filtVars_1 = (float *)pParam->pVar[0]; // Get filter var ptr 
        filtVars_2 = (float *)pParam->pVar[1]; // Get filter var ptr 
        filtVars_3 = (float *)pParam->pVar[2]; // Get filter var ptr 
        filtVars_4 = (float *)pParam->pVar[3]; // Get filter var ptr 
        filtVars_5 = (float *)pParam->pVar[4]; // Get filter var ptr 
        filtVars_6 = (float *)pParam->pVar[5]; // Get filter var ptr 
        filtVars_7 = (float *)pParam->pVar[6]; // Get filter var ptr 
        filtVars_8 = (float *)pParam->pVar[7]; // Get filter var ptr 
        
        count = pParam->sampleCount; // I/p sample block-length 
    
        // Get the filter states into corresponding regs 
        w1_1 = filtVars_1[0]; // Temp copy of filter state 
        w1_2 = filtVars_2[0]; // Temp copy of filter state 
        w1_3 = filtVars_3[0]; // Temp copy of filter state 
        w1_4 = filtVars_4[0]; // Temp copy of filter state 
        w1_5 = filtVars_5[0]; // Temp copy of filter state 
        w1_6 = filtVars_6[0]; // Temp copy of filter state 
        w1_7 = filtVars_7[0]; // Temp copy of filter state 
        w1_8 = filtVars_8[0]; // Temp copy of filter state 
        
        w_1  = w1_1;
        w_2  = w1_2;
        w_3  = w1_3;
        w_4  = w1_4;
        w_5  = w1_5;
        w_6  = w1_6;
        w_7  = w1_7;
        w_8  = w1_8;
     
        // IIR filtering for i/p block length 
        #pragma MUST_ITERATE(16, 2048, 4)
        for(samp=0; samp<count; ++samp)
          {
        
        //Channel-1 
        w_1 = x_1[samp] + filtCfsA[0]*w_1; // w(n)=x(n)+a1*w(n-1) 
        y_1[samp] = filtCfsB[0]*w_1 + filtCfsB[1]*w1_1; // y(n)=b0*w(n)+b1*w(n-1) 
        w1_1  = w_1; // State reg shift

        //Channel-2 
        w_2 = x_2[samp] + filtCfsA[0]*w_2;  
        y_2[samp] = filtCfsB[0]*w_2 + filtCfsB[1]*w1_2; 
        w1_2  = w_2; 

        //Channel-3 
        w_3 = x_3[samp] + filtCfsA[0]*w_3;  
        y_3[samp] = filtCfsB[0]*w_3 + filtCfsB[1]*w1_3;  
        w1_3  = w_3; 

        //Channel-4 
        w_4 = x_4[samp] + filtCfsA[0]*w_4;  
        y_4[samp] = filtCfsB[0]*w_4 + filtCfsB[1]*w1_4;  
        w1_4  = w_4; 

        //Channel-5 
        w_5 = x_5[samp] + filtCfsA[0]*w_5;  
        y_5[samp] = filtCfsB[0]*w_5 + filtCfsB[1]*w1_5;  
        w1_5  = w_5; 

        //Channel-6 
        w_6 = x_6[samp] + filtCfsA[0]*w_6;  
        y_6[samp] = filtCfsB[0]*w_6 + filtCfsB[1]*w1_6;  
        w1_6  = w_6; 

        //Channel-7 
        w_7 = x_7[samp] + filtCfsA[0]*w_7;  
        y_7[samp] = filtCfsB[0]*w_7 + filtCfsB[1]*w1_7;  
        w1_7  = w_7; 

        //Channel-8 
        w_8 = x_8[samp] + filtCfsA[0]*w_8;  
        y_8[samp] = filtCfsB[0]*w_8 + filtCfsB[1]*w1_8;  
        w1_8  = w_8; 
          }
     
        // Update state memory
        filtVars_1[0] = w_1;  
        filtVars_2[0] = w_2; 
        filtVars_3[0] = w_3; 
        filtVars_4[0] = w_4; 
        filtVars_5[0] = w_5; 
        filtVars_6[0] = w_6; 
        filtVars_7[0] = w_7; 
        filtVars_8[0] = w_8; 
/* ======================================================================== */
/*S Place source code here.                                                S*/
/* ======================================================================== */
   return(FIL_SUCCESS);

}

/* ======================================================================== */
/*  End of file: Filter_iirT1Ch8_u_o.c                                       */
/* ------------------------------------------------------------------------ */
/*          Copyright (C) 2004 Texas Instruments, Incorporated.             */
/*                          All Rights Reserved.                            */
/* ======================================================================== */
