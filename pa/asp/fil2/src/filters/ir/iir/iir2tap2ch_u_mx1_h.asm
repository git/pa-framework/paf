*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  
*     This routine has following C prototype:                               * 
*                                                                           * 
*     int Filter_iirT2Ch2_u_mx1(PAF_FilParam *pParam)                             * 
*                                                                           * 
*     where pParam� is a pointer to the lower layer generic filter handle   * 
*     structure �PAF_FilParam�.                                             * 
*                                                                           * 
*     typedef struct PAF_FilParam {                                         * 
*         Void  ** pIn ;                                                    * 
*         Void  ** pOut ;                                                   * 
*         Void  ** pCoef ;                                                  * 
*         Void  ** pVar ;                                                   * 
*         Uchar    channels;                                                * 
*         Int      sampleCount;                                             * 
*         Uint     use;                                                     * 
*      }  PAF_FilParam;                                                     * 
*                                                                           * 
*    Let N be the maximum number of channels that are processed at a time.  * 
*                                                                           * 
*    pIn  -�Pointer to the array of �input channel data� pointers.          * 
*                                                                           * 
*       pIn [0][X0(n)], where X(n) is the input data block, of length �     * 
*           [1][ X1(n) ]                                      sampleCount   * 
*           ............                                                    * 
*           [N-1][ XN(n) ]                                                  * 
*                                                                           * 
*    pOut�- Pointer to the array of �output channel data� pointers.         * 
*                                                                           * 
*     pOut[0][Y0(n)]   , where Y(n) is the input data block, of length      * 
*           [1][ Y1(n) ]                                      sampleCount   * 
*           ............                                                    * 
*           [N-1][ YN(n) ]                                                  * 
*                                                                           * 
*                                                                           * 
*    pCoef - Pointer to the array of�channel coefficient� pointers.         * 
*                                                                           * 
*       pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of       * 
*            [1][ C1(n) ]                                      a channel.   * 
*            ............                                                   * 
*            [N-1][ CN(n) ]                                                 * 
*                                                                           * 
*    pVar - Pointer to the array of channel state/private memory�pointers.  * 
*                                                                           * 
*        pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to         * 
*            [1][ m1(n) ]                                        a channel. * 
*            ............                                                   * 
*            [N-1][ mN(n) ]                                                 * 
*                                                                           * 
*    channels - Specifies the number of channels that are to be filtered,   * 
*               starting sequentially from channel 0.                       * 
*                                                                           * 
*    sampleCount - Specifies the sample length of the input data block.     * 
*                                                                           * 
*    use - This is a 32 bit field that can be used, to fit in some extra    * 
*           parameters or as a pointer to additional filter parameters.     * 
*                                                                           * 
*                                                                           * 
*  DESCRIPTION                                                              * 
*                                                                           * 
*       This routine implements IIR filter implementation for tap-2 &       * 
*       channels-2, unicoefficient,SP-DP.                                   * 
*                                                                           * 
*                   ORDER 2    and CHANNELS 2                               * 
*                                                                           * 
*       Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                     * 
*                                   ----------------------                  * 
*                                   1  - a1*z~1 - a2*z~2                    * 
*                                                                           * 
*       Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)                 * 
*                                   +a1*y(n-1)+a2*y(n-2)                    * 
*                                                                           * 
*       Implementation : w(n) = b0*x(n) + a1*w(n-1) + a2*w(n-2)             * 
*                        y(n) = w(n) + b1/b0*w(n-1) + b2/b0*w(n-2)          * 
*                                                                           * 
*       Filter variables :                                                  * 
*           y(n) - *y,         x(n)   - *x                                  * 
*           w(n) -             w(n-1) - w1         w(n-2) - w2              * 
*           c0=b0 - coef[0], c1=b1 - coef[1], c2=b2 - coef[2]               * 
*           c3=a1 - coef[3], c4=a2 - coef[4]                                * 
*                                                                           * 
*                                                                           * 
*                                    w(n)    b0                             * 
*           x(n)------[+]---->--------@------->>----[+]--->--y(n)           * 
*                      ^              v              ^                      * 
*                      |     a1    +-----+    b1     |                      * 
*                     [+]----<<----| Z~1 |---->>----[+]                     * 
*                      |           +-----+           |                      * 
*                      |              |              |                      * 
*                      ^              v              ^                      * 
*                      |     a2    +-----+    b2     |                      * 
*                       ----<<-----| Z~1 |---->>-----                       * 
*                                  +-----+                                  * 
*                                                                           * 
*                                                                           * 
*                                                                           * 
*                                                                           * 
*   C CODE                                                                  * 
*                                                                           * 
*    Int iirT2Ch2_u_mx(PAF_FilParam *pParam)                                * 
*       {                                                                   * 
*           float * restrict inL, * restrict inR, * restrict outL;          * 
*           float * restrict  outR, *coef;                                  * 
*           double * varL, *varR;                                           * 
*           int i, count; // counter                                        * 
*           float b0, b1, b2, a1, a2; // Coeff                              * 
*           double wL, w1L, w2L, wR, w1R, w2R; // Delay                     * 
*                                                                           * 
*           /* Get i/p ptr's */                                             * 
*           inL= (float *)pParam->pIn[0];                                   * 
*           inR= (float *)pParam->pIn[1];                                   * 
*                                                                           * 
*           /* Get o/p ptr's */                                             * 
*           outL= (float *)pParam->pOut[0];                                 * 
*           outR= (float *)pParam->pOut[1];                                 * 
*                                                                           * 
*           /* Get states ptr's */                                          * 
*           varL= (double *)pParam->pVar[0];                                * 
*           varR= (double *)pParam->pVar[1];                                * 
*                                                                           * 
*           /* Get coef ptr' */                                             * 
*           coef= (float *)pParam->pCoef[0];                                * 
*                                                                           * 
*           w1L = varL[0]; // w(n-1)                                        * 
*           w2L = varL[1]; // w(n-2)                                        * 
*                                                                           * 
*           w1R = varR[0]; // w(n-1)                                        * 
*           w2R = varR[1]; // w(n-2)                                        * 
*                                                                           * 
*           b0 = coef[0];                                                   * 
*           b1 = coef[1];                                                   * 
*           b2 = coef[2];                                                   * 
*           a1 = coef[3];                                                   * 
*           a2 = coef[4];                                                   * 
*                                                                           * 
*           for(i = 0; i < count; i++)                                      * 
*            {                                                              * 
*               // Left                                                     * 
*               wL    = inL[i] + a1*w1L + a2*w2L;                           * 
*               outL[i] = b0*wL + b1*w1L + b2*w2L;                          * 
*                                                                           * 
*               w2L  = w1L;                                                 * 
*               w1L  = wL;                                                  * 
*                                                                           * 
*               // Right                                                    * 
*               wR    = inR[i] + a1*w1R + a2*w2R;                           * 
*               outR[i] = b0*wR + b1*w1R + b2*w2R;                          * 
*                                                                           * 
*               w2R  = w1R;                                                 * 
*               w1R  = wR;                                                  * 
*           }                                                               * 
*                                                                           * 
*           //Save the delay part                                           * 
*           varL[0] = w1L;                                                  * 
*           varL[1] = w2L;                                                  * 
*                                                                           * 
*           varR[0] = w1R;                                                  * 
*           varR[1] = w2R;                                                  * 
*       }                                                                   * 
*                                                                           * 
*                                                                           * 
*  NOTES                                                                    * 
*                                                                           * 
*       1. Endian: This code is LITTLE ENDIAN.                              * 
*       2. Interruptibility: This code is interrupt-tolerant but not        * 
*          interruptible.                                                   * 
*                                                                           * 
*                                                                           * 
* ------------------------------------------------------------------------- *
*             Copyright (c) 2004 Texas Instruments, Incorporated.           *
*                            All Rights Reserved.                           *
* ========================================================================= *
                .global _Filter_iirT2Ch2_u_mx1
                .sect ".text:hand"


_Filter_iirT2Ch2_u_mx1:
        MV      .L2X     A4,         B4                 ;Make a copy of A4
 ||     MVC     .S2     CSR,         B7                 ;Get CSR

        LDW     .D1T1   *A4[2],      A6                 ;pParam->pCoef        
 ||     LDW     .D2T2   *B4[3],      B6                 ;pParam->pVar
 ||     AND     .S2     -2,          B7,         B2     ;Disable Interrupts

        LDW     .D1T1   *A4[0],      A5                 ;pParam->pIn        
 ||     LDW     .D2T2   *B4[1],      B5                 ;pParam->pOut
 ||     MVC     .S2     B2,          CSR

        LDW     .D1T1   *A4[4],      A1                 ;pParam->sampleCount
 ||     STW     .D2T2   B7,          *B15--[01]         ;Store CSR
 
        NOP     2
 
        LDW     .D1T1   *A6[0],      A6                 ;Coef = pCoef[0]
 ||     LDW     .D2T2   *B6[1],      B9                 ;varR = pvar[1]
 
        LDW     .D2T1   *B6[0],      A9                 ;varL = pvar[0]
        
        LDW     .D1T1   *A5[0],      A7                 ;inL = pIn[0]        
 ||     LDW     .D2T2   *B5[1],      B8                 ;outR = pOut[1]
 
        LDW     .D1T2   *A5[1],      B7                 ;inR = pIn[1]        
 ||     LDW     .D2T1   *B5[0],      A8                 ;outL = pOut[0]

        NOP     1
        
        LDW     .D1T1   *A6[0],      A3                 ;b0 = Coef[0]
 ||     MV      .S2X    A6,          B4                 ;Copy of *coef           
  
        LDW     .D1T1   *A6[1],      A4                 ;b1 = Coef[1]       
 ||     LDW     .D2T2   *B4[1],      B4                 ;b1 = Coef[1]
            
        LDW     .D1T1   *A6[4],      A31                ;a2 = Coef[4]       
 ||     LDW     .D2T2   *B4[4],      B31                ;a2 = Coef[4]
  
        LDDW    .D1T1   *A9[1],      A19:A18            ;W2L = varL[1]       
 ||     LDDW    .D2T2   *B9[1],      B19:B18            ;W2R = varR[1] 
 
        LDW     .D1T1   *A6[2],      A5                 ;b2 = Coef[2]       
 ||     LDW     .D2T2   *B4[2],      B5                 ;b2 = Coef[2]

        LDW     .D1T1   *A6[3],      A30                ;a1 = Coef[3]       
 ||     LDW     .D2T2   *B4[3],      B30                ;a1 = Coef[3]

        LDDW    .D1T1   *A9[0],      A17:A16            ;W1L = varL[0]       
 ||     LDDW    .D2T2   *B9[0],      B17:B16            ;W1R = varR[0]       
   
;************************************************************************; 
;**********************    PROLOG BEGINS HERE    ************************; 
;************************************************************************; 
        LDW         .D1T1   *A7++,      A6                  ;load inL[i]
 ||     LDW         .D2T2   *B7++,      B6                  ;load inR[i]       
        
        MPYSPDP     .M1     A31,        A19:A18,    A21:A20 ;a2*W2L
 ||     MPYSPDP     .M2     B31,        B19:B18,    B21:B20 ;a2*W2R   

        NOP     3
        
        MPYSP2DP    .M1     A3,         A6,         A23:A22 ;b0*inL[i]
 ||     MPYSP2DP    .M2X    A3,         B6,         B23:B22 ;b0*inR[i]

        NOP     1
        
        MPYSPDP     .M1     A30,        A17:A16,    A25:A24 ;a1*W1L
 ||     MPYSPDP     .M2     B30,        B17:B16,    B25:B24 ;a1*W1R  
 
        NOP     1
        
        ADDDP       .L1     A23:A22,    A21:A20,    A23:A22 ;b0*inL[i]+ a2*W2L      
 ||     ADDDP       .L2     B23:B22,    B21:B20,    B23:B22 ;b0*inR[i]+ a2*W2R
 
        MPYSPDP     .M1     A5,         A19:A18,    A21:A20 ;b2*W2L
 ||     MPYSPDP     .M2     B5,         B19:B18,    B21:B20 ;b2*W2R          
 
        NOP         2
        
        MPYSPDP     .M1     A4,         A17:A16,    A25:A24 ;b1*W1L
 ||     MPYSPDP     .M2     B4,         B17:B16,    B25:B24 ;b1*W1R  
 ||     MV          .S1     A16,        A18                 ;W2L = W1L
 ||     MV          .S2     B16,        B18                 ;W2R = W1R
        
        MV          .S1     A17,        A19                 ;W2L = W1L
 ||     MV          .S2     B17,        B19                 ;W2R = W1R
 
;---------------------------------------------------------------------------;
        LDW         .D1T1   *A7++,      A6                  ;@load inL[i]
 ||     LDW         .D2T2   *B7++,      B6                  ;@load inR[i]       
 ||     ADDDP       .S1     A23:A22,    A25:A24,    A17:A16 ;W1L      
 ||     ADDDP       .S2     B23:B22,    B25:B24,    B17:B16 ;W1R
        
        MPYSPDP     .M1     A31,        A19:A18,    A21:A20 ;@a2*W2L
 ||     MPYSPDP     .M2     B31,        B19:B18,    B21:B20 ;@a2*W2R   

        NOP     2
        
        ADDDP       .L1     A25:A24,    A21:A20,    A27:A26 ;b1*W1L + b2*W2L      
 ||     ADDDP       .L2     B25:B24,    B21:B20,    B27:B26 ;b1*W1R + b2*W2R
        
        MPYSP2DP    .M1     A3,         A6,         A23:A22 ;@b0*inL[i]
 ||     MPYSP2DP    .M2X    A3,         B6,         B23:B22 ;@b0*inR[i]

        NOP     1
        
        MPYSPDP     .M1     A30,        A17:A16,    A25:A24 ;@a1*W1L
 ||     MPYSPDP     .M2     B30,        B17:B16,    B25:B24 ;@a1*W1R  
 ||[A1] SUB         .S1     A1,         1,          A1      ;Decrement loopctr       
;****************************************************************************;
;********************    BEGIN OF PIPELOOPED KERNEL    **********************;
;****************************************************************************;
LOOP: 
        NOP     1
        
        ADDDP       .L1     A23:A22,    A21:A20,    A23:A22 ;@b0*inL[i]+ a2*W2L      
 ||     ADDDP       .L2     B23:B22,    B21:B20,    B23:B22 ;@b0*inR[i]+ a2*W2R
 
        MPYSPDP     .M1     A5,         A19:A18,    A21:A20 ;@b2*W2L
 ||     MPYSPDP     .M2     B5,         B19:B18,    B21:B20 ;@b2*W2R          
 
        ADDDP       .L1     A27:A26,    A17:A16,    A27:A26 ;b1*W1L+b2*W2L+WL     
 ||     ADDDP       .L2     B27:B26,    B17:B16,    B27:B26 ;b1*W1R+b2*W2R+WR
   
   [!A1]LDW         .D2T2   *++B15,     B7                  ;POP out CSR
        
        MPYSPDP     .M1     A4,         A17:A16,    A25:A24 ;@b1*W1L
 ||     MPYSPDP     .M2     B4,         B17:B16,    B25:B24 ;@b1*W1R  
 ||[A1] MV          .S1     A16,        A18                 ;@W2L = W1L
 ||[A1] MV          .S2     B16,        B18                 ;@W2R = W1R
        
   [A1] MV          .S1     A17,        A19                 ;@W2L = W1L
 ||[A1] MV          .S2     B17,        B19                 ;@W2R = W1R
 
;---------------------------------------------------------------------------;

   [A1] LDW         .D1T1   *A7++,      A6                  ;@@load inL[i]
 ||[A1] LDW         .D2T2   *B7++,      B6                  ;@@load inR[i]       
 ||[A1] ADDDP       .S1     A23:A22,    A25:A24,    A17:A16 ;@W1L      
 ||[A1] ADDDP       .S2     B23:B22,    B25:B24,    B17:B16 ;@W1R
        
        MPYSPDP     .M1     A31,        A19:A18,    A21:A20 ;@@a2*W2L
 ||     MPYSPDP     .M2     B31,        B19:B18,    B21:B20 ;@@a2*W2R   

   [A1] B           .S1     LOOP     

        DPSP        .L1     A27:A26,    A28                 ;float(outLA)
 ||     DPSP        .L2     B27:B26,    B28                 ;float(outRA)               
        
        ADDDP       .L1     A25:A24,    A21:A20,    A27:A26 ;@b1*W1L + b2*W2L      
 ||     ADDDP       .L2     B25:B24,    B21:B20,    B27:B26 ;@b1*W1R + b2*W2R
        
        MPYSP2DP    .M1     A3,         A6,         A23:A22 ;@@b0*inL[i]
 ||     MPYSP2DP    .M2X    A3,         B6,         B23:B22 ;@@b0*inR[i]

        NOP     1
        
        MPYSPDP     .M1     A30,        A17:A16,    A25:A24 ;@@a1*W1L
 ||     MPYSPDP     .M2     B30,        B17:B16,    B25:B24 ;@@a1*W1R  
 ||     STW         .D1     A28,        *A8++               ;outL[i] = outLA
 ||     STW         .D2     B28,        *B8++               ;outR[i] = outRA  
 ||[A1] SUB         .S1     A1,         1,          A1      ;Decrement loopctr       
 ||[!A1]MVC         .S2     B7,         CSR                 ;Enable Interrupts     
        
;****************************************************************************;
;********************    END OF PIPE LOOPED KERNEL    ***********************;
;****************************************************************************;
    
        B           .S2     B3                              ;branch to Main
        
        STW         .D1T1   A16,        *A9[0]              ;Store varL[0]
 ||     STW         .D2T2   B16,        *B9[0]              ;Store varR[0]    
 
        STW         .D1T1   A17,        *A9[1]              ;Store varL[0]
 ||     STW         .D2T2   B17,        *B9[1]              ;Store varR[0]    

        STW         .D1T1   A18,        *A9[2]              ;Store varL[1]
 ||     STW         .D2T2   B18,        *B9[2]              ;Store varR[1]    
 
        STW         .D1T1   A19,        *A9[3]              ;Store varL[1]
 ||     STW         .D2T2   B19,        *B9[3]              ;Store varR[1]
 
        ZERO        .L1     A4    
              .end

* ======================================================================== *
*  End of file: Filter_iirT2Ch2_u_mx1_h.asm                                      *
* ------------------------------------------------------------------------ *
*          Copyright (C) 2004 Texas Instruments, Incorporated.             *
*                          All Rights Reserved.                            *
* ======================================================================== *
