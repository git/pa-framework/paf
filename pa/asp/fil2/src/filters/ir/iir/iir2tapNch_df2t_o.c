/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include "filters.h"

Int Filter_iirT2Ch1_df2t( PAF_FilParam *pParam ) 
{
    Float *restrict x, *restrict y, input, output, a1, a2, b0, b1, b2, d0, d1;

    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    x = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0 = filtVars[0];
    d1 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1 = filtCfsA[0];
    a2 = filtCfsA[1];
    b0 = filtCfsB[0];
    b1 = filtCfsB[1];
    b2 = filtCfsB[2];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        input = *x++;
        output = input * b0 + d0;
        d0 = input*b1 + output*a1 + d1;
        d1 = input*b2 + output*a2;
        *y++ = output;
        
    }
    
    /* Update state memory */
    filtVars[0] = d0;
    filtVars[1] = d1;
 
    return FIL_SUCCESS;
} 




Int Filter_iirT2Ch2_df2t( PAF_FilParam *pParam ) 
{
    Float *restrict x_0, *restrict y_0, input_0, output_0, a1_0, a2_0, b0_0, b1_0, b2_0, d0_0, d1_0;
    Float *restrict x_1, *restrict y_1, input_1, output_1, a1_1, a2_1, b0_1, b1_1, b2_1, d0_1, d1_1;

    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    x_0 = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y_0 = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_0 = filtVars[0];
    d1_0 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1_0 = filtCfsA[0];
    a2_0 = filtCfsA[1];
    b0_0 = filtCfsB[0];
    b1_0 = filtCfsB[1];
    b2_0 = filtCfsB[2];

    x_1 = (Float *)pParam->pIn [1]; /* Get i/p ptr */
    y_1 = (Float *)pParam->pOut[1]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_1 = filtVars[0];
    d1_1 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[1]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1_1 = filtCfsA[0];
    a2_1 = filtCfsA[1];
    b0_1 = filtCfsB[0];
    b1_1 = filtCfsB[1];
    b2_1 = filtCfsB[2];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        input_0 = *x_0++;
        output_0 = input_0 * b0_0 + d0_0;
        d0_0 = input_0*b1_0 + output_0*a1_0 + d1_0;
        d1_0 = input_0*b2_0 + output_0*a2_0;
        *y_0++ = output_0;
        
        input_1 = *x_1++;
        output_1 = input_1 * b0_1 + d0_1;
        d0_1 = input_1*b1_1 + output_1*a1_1 + d1_1;
        d1_1 = input_1*b2_1 + output_1*a2_1;
        *y_1++ = output_1;
        
    }
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Update state memory */
    filtVars[0] = d0_0;
    filtVars[1] = d1_0;
 
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Update state memory */
    filtVars[0] = d0_1;
    filtVars[1] = d1_1;
 
    return FIL_SUCCESS;
} 

Int Filter_iirT2Ch3_df2t( PAF_FilParam *pParam ) 
{
    Float *restrict x_0, *restrict y_0, input_0, output_0, a1_0, a2_0, b0_0, b1_0, b2_0, d0_0, d1_0;
    Float *restrict x_1, *restrict y_1, input_1, output_1, a1_1, a2_1, b0_1, b1_1, b2_1, d0_1, d1_1;
    Float *restrict x_2, *restrict y_2, input_2, output_2, a1_2, a2_2, b0_2, b1_2, b2_2, d0_2, d1_2;

    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    x_0 = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y_0 = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_0 = filtVars[0];
    d1_0 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1_0 = filtCfsA[0];
    a2_0 = filtCfsA[1];
    b0_0 = filtCfsB[0];
    b1_0 = filtCfsB[1];
    b2_0 = filtCfsB[2];

    x_1 = (Float *)pParam->pIn [1]; /* Get i/p ptr */
    y_1 = (Float *)pParam->pOut[1]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_1 = filtVars[0];
    d1_1 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[1]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1_1 = filtCfsA[0];
    a2_1 = filtCfsA[1];
    b0_1 = filtCfsB[0];
    b1_1 = filtCfsB[1];
    b2_1 = filtCfsB[2];

    x_2 = (Float *)pParam->pIn [2]; /* Get i/p ptr */
    y_2 = (Float *)pParam->pOut[2]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_2 = filtVars[0];
    d1_2 = filtVars[1];
    
    filtCfsB = (Float *)pParam->pCoef[2]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1_2 = filtCfsA[0];
    a2_2 = filtCfsA[1];
    b0_2 = filtCfsB[0];
    b1_2 = filtCfsB[1];
    b2_2 = filtCfsB[2];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        input_0 = *x_0++;
        output_0 = input_0 * b0_0 + d0_0;
        d0_0 = input_0*b1_0 + output_0*a1_0 + d1_0;
        d1_0 = input_0*b2_0 + output_0*a2_0;
        *y_0++ = output_0;
        
        input_1 = *x_1++;
        output_1 = input_1 * b0_1 + d0_1;
        d0_1 = input_1*b1_1 + output_1*a1_1 + d1_1;
        d1_1 = input_1*b2_1 + output_1*a2_1;
        *y_1++ = output_1;
        
        input_2 = *x_2++;
        output_2 = input_2 * b0_2 + d0_2;
        d0_2 = input_2*b1_2 + output_2*a1_2 + d1_2;
        d1_2 = input_2*b2_2 + output_2*a2_2;
        *y_2++ = output_2;
        
    }
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Update state memory */
    filtVars[0] = d0_0;
    filtVars[1] = d1_0;
 
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Update state memory */
    filtVars[0] = d0_1;
    filtVars[1] = d1_1;
 
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    /* Update state memory */
    filtVars[0] = d0_2;
    filtVars[1] = d1_2;
 
    return FIL_SUCCESS;
} 

Int Filter_iirT2Ch5_u_df2t( PAF_FilParam *pParam ) 
{
    Float a1, a2, b0, b1, b2;
    
    Float *restrict x_0, *restrict y_0, input_0, output_0, d0_0, d1_0;
    Float *restrict x_1, *restrict y_1, input_1, output_1, d0_1, d1_1;
    Float *restrict x_2, *restrict y_2, input_2, output_2, d0_2, d1_2;
    Float *restrict x_3, *restrict y_3, input_3, output_3, d0_3, d1_3;
    Float *restrict x_4, *restrict y_4, input_4, output_4, d0_4, d1_4;

    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1 = filtCfsA[0];
    a2 = filtCfsA[1];
    b0 = filtCfsB[0];
    b1 = filtCfsB[1];
    b2 = filtCfsB[2];

    x_0 = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y_0 = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_0 = filtVars[0];
    d1_0 = filtVars[1];
    
    x_1 = (Float *)pParam->pIn [1]; /* Get i/p ptr */
    y_1 = (Float *)pParam->pOut[1]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_1 = filtVars[0];
    d1_1 = filtVars[1];
    
    x_2 = (Float *)pParam->pIn [2]; /* Get i/p ptr */
    y_2 = (Float *)pParam->pOut[2]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_2 = filtVars[0];
    d1_2 = filtVars[1];
    
    x_3 = (Float *)pParam->pIn [3]; /* Get i/p ptr */
    y_3 = (Float *)pParam->pOut[3]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[3]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_3 = filtVars[0];
    d1_3 = filtVars[1];
    
    x_4 = (Float *)pParam->pIn [4]; /* Get i/p ptr */
    y_4 = (Float *)pParam->pOut[4]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[4]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_4 = filtVars[0];
    d1_4 = filtVars[1];
    
    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        input_0 = *x_0++;
        output_0 = input_0 * b0 + d0_0;
        d0_0 = input_0*b1 + output_0*a1 + d1_0;
        d1_0 = input_0*b2 + output_0*a2;
        *y_0++ = output_0;
        
        input_1 = *x_1++;
        output_1 = input_1 * b0 + d0_1;
        d0_1 = input_1*b1 + output_1*a1 + d1_1;
        d1_1 = input_1*b2 + output_1*a2;
        *y_1++ = output_1;
        
        input_2 = *x_2++;
        output_2 = input_2 * b0 + d0_2;
        d0_2 = input_2*b1 + output_2*a1 + d1_2;
        d1_2 = input_2*b2 + output_2*a2;
        *y_2++ = output_2;
        
        input_3 = *x_3++;
        output_3 = input_3 * b0 + d0_3;
        d0_3 = input_3*b1 + output_3*a1 + d1_3;
        d1_3 = input_3*b2 + output_3*a2;
        *y_3++ = output_3;
        
        input_4 = *x_4++;
        output_4 = input_4 * b0 + d0_4;
        d0_4 = input_4*b1 + output_4*a1 + d1_4;
        d1_4 = input_4*b2 + output_4*a2;
        *y_4++ = output_4;
        
    }
    
    /* Update state memory */
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    filtVars[0] = d0_0;
    filtVars[1] = d1_0;
 
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    filtVars[0] = d0_1;
    filtVars[1] = d1_1;
 
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    filtVars[0] = d0_2;
    filtVars[1] = d1_2;
 
    filtVars = (Float *)pParam->pVar[3]; /* Get filter var ptr */
    filtVars[0] = d0_3;
    filtVars[1] = d1_3;
 
    filtVars = (Float *)pParam->pVar[4]; /* Get filter var ptr */
    filtVars[0] = d0_4;
    filtVars[1] = d1_4;
 
    return FIL_SUCCESS;
} 


Int Filter_iirT2Ch6_u_df2t( PAF_FilParam *pParam ) 
{
    Float a1, a2, b0, b1, b2;
    
    Float *restrict x_0, *restrict y_0, input_0, output_0, d0_0, d1_0;
    Float *restrict x_1, *restrict y_1, input_1, output_1, d0_1, d1_1;
    Float *restrict x_2, *restrict y_2, input_2, output_2, d0_2, d1_2;
    Float *restrict x_3, *restrict y_3, input_3, output_3, d0_3, d1_3;
    Float *restrict x_4, *restrict y_4, input_4, output_4, d0_4, d1_4;
    Float *restrict x_5, *restrict y_5, input_5, output_5, d0_5, d1_5;

    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    filtCfsB = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    filtCfsA = filtCfsB + 3; /* Derive feedback coeff ptr */
    a1 = filtCfsA[0];
    a2 = filtCfsA[1];
    b0 = filtCfsB[0];
    b1 = filtCfsB[1];
    b2 = filtCfsB[2];

    x_0 = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y_0 = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_0 = filtVars[0];
    d1_0 = filtVars[1];
    
    x_1 = (Float *)pParam->pIn [1]; /* Get i/p ptr */
    y_1 = (Float *)pParam->pOut[1]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_1 = filtVars[0];
    d1_1 = filtVars[1];
    
    x_2 = (Float *)pParam->pIn [2]; /* Get i/p ptr */
    y_2 = (Float *)pParam->pOut[2]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_2 = filtVars[0];
    d1_2 = filtVars[1];
    
    x_3 = (Float *)pParam->pIn [3]; /* Get i/p ptr */
    y_3 = (Float *)pParam->pOut[3]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[3]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_3 = filtVars[0];
    d1_3 = filtVars[1];
    
    x_4 = (Float *)pParam->pIn [4]; /* Get i/p ptr */
    y_4 = (Float *)pParam->pOut[4]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[4]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_4 = filtVars[0];
    d1_4 = filtVars[1];
    
    x_5 = (Float *)pParam->pIn [5]; /* Get i/p ptr */
    y_5 = (Float *)pParam->pOut[5]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[5]; /* Get filter var ptr */
    /* Get the filter states into corresponding regs */
    d0_5 = filtVars[0];
    d1_5 = filtVars[1];
    
    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
        input_0 = *x_0++;
        output_0 = input_0 * b0 + d0_0;
        d0_0 = input_0*b1 + output_0*a1 + d1_0;
        d1_0 = input_0*b2 + output_0*a2;
        *y_0++ = output_0;
        
        input_1 = *x_1++;
        output_1 = input_1 * b0 + d0_1;
        d0_1 = input_1*b1 + output_1*a1 + d1_1;
        d1_1 = input_1*b2 + output_1*a2;
        *y_1++ = output_1;
        
        input_2 = *x_2++;
        output_2 = input_2 * b0 + d0_2;
        d0_2 = input_2*b1 + output_2*a1 + d1_2;
        d1_2 = input_2*b2 + output_2*a2;
        *y_2++ = output_2;
        
        input_3 = *x_3++;
        output_3 = input_3 * b0 + d0_3;
        d0_3 = input_3*b1 + output_3*a1 + d1_3;
        d1_3 = input_3*b2 + output_3*a2;
        *y_3++ = output_3;
        
        input_4 = *x_4++;
        output_4 = input_4 * b0 + d0_4;
        d0_4 = input_4*b1 + output_4*a1 + d1_4;
        d1_4 = input_4*b2 + output_4*a2;
        *y_4++ = output_4;
        
        input_5 = *x_5++;
        output_5 = input_5 * b0 + d0_5;
        d0_5 = input_5*b1 + output_5*a1 + d1_5;
        d1_5 = input_5*b2 + output_5*a2;
        *y_5++ = output_5;
        
    }
    
    /* Update state memory */
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    filtVars[0] = d0_0;
    filtVars[1] = d1_0;
 
    filtVars = (Float *)pParam->pVar[1]; /* Get filter var ptr */
    filtVars[0] = d0_1;
    filtVars[1] = d1_1;
 
    filtVars = (Float *)pParam->pVar[2]; /* Get filter var ptr */
    filtVars[0] = d0_2;
    filtVars[1] = d1_2;
 
    filtVars = (Float *)pParam->pVar[3]; /* Get filter var ptr */
    filtVars[0] = d0_3;
    filtVars[1] = d1_3;
 
    filtVars = (Float *)pParam->pVar[4]; /* Get filter var ptr */
    filtVars[0] = d0_4;
    filtVars[1] = d1_4;
 
    filtVars = (Float *)pParam->pVar[5]; /* Get filter var ptr */
    filtVars[0] = d0_5;
    filtVars[1] = d1_5;
 
    return FIL_SUCCESS;
} 


Int Filter_iirT2ChN_df2t( PAF_FilParam *pParam ) 
{

    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0, multiple;
    Uint uniCoef = 0 ;
    
    /* Get handle parameters into temp local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;
    
    /* Check whether unicoeff and inplace */
    for( i = 0; i < pParam->channels; i++ )
    {
      uniCoef |= ( Uint )pParam->pCoef[0] ^  ( Uint )pParam->pCoef[i];
    }
              
    channels = pParam->channels; /* Get the no of chs filtered */
    
    /* Ch-4 */
    if( channels == 4 )
    {
       /* 3-1 chs */    
	   	error += Filter_iirT2Ch3_df2t( pParam );
        FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
        error += Filter_iirT2Ch1_df2t( pParam );
    }
    
    /* Ch-5, as 3-2 chs */   
    else if( channels == 5 )
    {
    	if( !uniCoef ) 
            	error += Filter_iirT2Ch5_u_df2t( pParam );
   
        else
        	{
    
        		error += Filter_iirT2Ch3_df2t( pParam );
        		FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
        		error += Filter_iirT2Ch2_df2t( pParam );
        	}
    }
    
    /* Ch-6, as 3-3 chs */
    else if( channels == 6 )
    {
    	if( !uniCoef ) 
            	error += Filter_iirT2Ch6_u_df2t( pParam );
        
        else
        	{
        		error += Filter_iirT2Ch3_df2t( pParam );
        		FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
        		error += Filter_iirT2Ch3_df2t( pParam );
        	}
    }
    
    /* Ch-7 */
    else if( channels == 7 )
    {
        /* If unicoef, as 5u-2 chs */
        if( !uniCoef )
        {
           	error += Filter_iirT2Ch5_u_df2t( pParam );
           	FIL_offsetParam( pParam, 5 ); /* Offset the param ptrs by 4 */
           	error += Filter_iirT2Ch2_df2t( pParam );
        }
        
        /* General, as 3-3-1 chs */
        else
         {
           	error += Filter_iirT2Ch3_df2t( pParam );
           	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
           	error += Filter_iirT2Ch3_df2t( pParam );
           	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
           	error += Filter_iirT2Ch1_df2t( pParam );
         }
    } /* else if( channels == 7 ) */   

    /* Ch-8 */
    else if( channels == 8 )
    {
        /* If unicoeff, as 6u-2 chs */
        if( !uniCoef )
        {
           	error += Filter_iirT2Ch6_u_df2t( pParam );
           	FIL_offsetParam( pParam, 6 ); /* Offset the param ptrs by 4 */ 
           	error += Filter_iirT2Ch2_df2t( pParam );
        }
        
         /* General, as 3-3-2 chs */
        else
        {
 	    	error += Filter_iirT2Ch3_df2t( pParam );
        	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            error += Filter_iirT2Ch3_df2t( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            error += Filter_iirT2Ch2_df2t( pParam );
        }
    } /* else if( channels == 8 ) */
            
    /* Ch-N */
    else if( channels <= PAF_MAXNUMCHAN )
    {
        multiple  = (Int)(channels / 3); /* Calculate int(ch/3) */
        channels -= (multiple * 3); /* Find ch % 3 */
        
        /* Multiple of 3 channels are filtered using 3-ch implementation */
        while( multiple )
        {
            error += Filter_iirT2Ch3_df2t( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            multiple--; /* Decrement counter */          
        }
        /* Remaining ch=2 */
        if( channels == 2 )
            error += Filter_iirT2Ch2_df2t( pParam );
        /* Remaining ch=1 */        
        else if( channels == 1 )
            error += Filter_iirT2Ch1_df2t( pParam );
    } /* else if( channels <= PAF_MAXNUMCHAN ) */

    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);
}

