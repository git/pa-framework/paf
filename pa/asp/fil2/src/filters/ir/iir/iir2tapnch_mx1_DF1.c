/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tapnch_u_mx.c ========
 *  IIR filter implementation for tap-2 & channels-N, Coeffs = SP, States = DP.
 */

/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS N ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Implementation : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = b0*w(n) + b1/b0*w(n-1) + b2/b0*w(n-2)           */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1/b0 - filtCfsB[1], b2/b0 - filtCfsB[2]           */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*            b0                w(n)                                         */
/*     x(n)-->---[+]---->--------@-------------[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+  b1/b0    |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+  b2/b0    |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h"

extern void biquad2chMP(float *, float *, float *, float *, float *, float *, float *, float *, int, int);

/*
 *  ======== Filter_iirT2ChN_u_mx1() ========
 *  IIR filter, taps-2 & channels-N, Coeficients = SP, States = DP  
 */

/* Memory section for the function code */
#pragma CODE_SECTION(Filter_iirT2ChN_mx1_DF1, ".text:Filter_iirT2ChN_mx1_DF1")
Int Filter_iirT2ChN_mx1_DF1( PAF_FilParam *pParam )
{
	float **pIn, **pOut, **pCoef, **pVar; /* Handle parameters */
    Int channels, ch, sampleCount;
    /* Get handle parameters into temp local variables */
    pIn = (float **)pParam->pIn;
    pOut = (float **)pParam->pOut;
    pCoef = (float **)pParam->pCoef;
    pVar = (float **)pParam->pVar;
	channels = pParam->channels; /* Get the no of chs filtered */
	sampleCount = pParam->sampleCount;

	/* General N ch */
	for (ch = 0; channels > 1; ch += 2) {
		biquad2chMP(pVar[ch], pVar[ch + 1], pCoef[ch], pCoef[ch + 1], pIn[ch], pIn[ch + 1], pOut[ch],
					pOut[ch + 1], sampleCount, 1);
		channels -= 2;
	}
	if (channels) biquad2chMP(pVar[ch], pVar[ch], pCoef[ch], pCoef[ch], pIn[ch], pIn[ch], pOut[ch], pOut[ch],
								sampleCount, 0);
        
	/* Get handles Back */
    pParam->pIn   = (void **)pIn;
    pParam->pOut  = (void **)pOut;
    pParam->pCoef = (void **)pCoef;
    pParam->pVar  = (void **)pVar;
    
    return FIL_SUCCESS;     
} /* Int Filter_iirT2ChN_mx1_DF1() */

