/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*                                                                          */
/* his routine has following C prototype:                                   */
/*                                                                          */
/*   void Filter_iirT2Ch5_u(PAF_FilParam *pParam)                                  */
/*                                                                          */
/*   where pParam� is a pointer to the lower layer generic filter handle    */
/*   structure �PAF_FilParam�.                                              */
/*                                                                          */
/*   typedef struct PAF_FilParam {                                          */
/*       void  ** pIn ;                                                     */
/*       void  ** pOut ;                                                    */
/*       void  ** pCoef ;                                                   */
/*       void  ** pVar ;                                                    */
/*       int      sampleCount;                                              */
/*       uchar    channels;                                                 */
/*       uint     use;                                                      */
/*    }  PAF_FilParam;                                                      */
/*                                                                          */
/*  Let N be the maximum number of channels that are processed at a time.   */
/*                                                                          */
/*  pIn  -�Pointer to the array of �input channel data� pointers.           */
/*                                                                          */
/*     pIn [0][X0(n)], where X(n) is the input data block, of length �      */
/*         [1][ X1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ XN(n) ]                                                   */
/*                                                                          */
/*  pOut�- Pointer to the array of �output channel data� pointers.          */
/*                                                                          */
/*     pOut[0][Y0(n)]   , where Y(n) is the input data block, of length     */
/*         [1][ Y1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ YN(n) ]                                                   */
/*                                                                          */
/*                                                                          */
/*  pCoef - Pointer to the array of�channel coefficient� pointers.          */
/*                                                                          */
/*     pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of        */
/*          [1][ C1(n) ]                                      a channel.    */
/*          ............                                                    */
/*          [N-1][ CN(n) ]                                                  */
/*                                                                          */
/*  pVar - Pointer to the array of channel state/private memory�pointers.   */
/*                                                                          */
/*     pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to           */
/*          [1][ m1(n) ]                                        a channel.  */
/*         ............                                                     */
/*          [N-1][ mN(n) ]                                                  */
/*                                                                          */
/*  channels - Specifies the number of channels that are to be filtered,    */
/*             starting sequentially from channel 0.                        */
/*                                                                          */
/*  sampleCount - Specifies the sample length of the input data block.      */
/*                                                                          */
/*  use - This is a 32 bit field that can be used, to fit in some extra     */
/*         parameters or as a pointer to additional filter parameters.      */
/*                                                                          */
/*                                                                          */
/* ESCRIPTION                                                               */
/*                                                                          */
/*     This routine implements IIR filter implementation for tap-2 &        */
/*     channels-5, unicoefficient,SP.                                       */
/*                                                                          */
/*                 ORDER 2    and CHANNELS 5                                */
/*                                                                          */
/* lter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                            */
/*                         ----------------------                           */
/*                          1  - a1*z~1 - a2*z~2                            */
/*                                                                          */
/* Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)  */
/*                                                                          */
/* Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                  */
/*                  y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                  */
/*                                                                          */
/* Filter variables :                                                       */
/*    y(n) - *y,         x(n)   - *x                                        */
/*    w(n) -             w(n-1) - w1         w(n-2) - w2                    */
/*    b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                  */
/*    a1 - filtCfsA[0], a2 - filtCfsA[1]                                    */
/*                                                                          */
/*                                                                          */
/*                            w(n)    b0                                    */
/*   x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                  */
/*              |              |              |                             */
/*              ^              v              ^                             */
/*              |     a1    +-----+    b1     |                             */
/*             [+]----<<----| Z~1 |---->>----[+]                            */
/*              |           +-----+           |                             */
/*              |              |              |                             */
/*              ^              v              ^                             */
/*              |     a2    +-----+    b2     |                             */
/*               ----<<-----| Z~1 |---->>-----                              */
/*                          +-----+                                         */
/*                                                                          */
/*                                                                          */
/* Int Filter_iirT2Ch5_u( PAF_FilParam *pParam )                                    */
/*                                                                          */
/*  int count, samp;  //Loop counters                                       */
/*  float *restrict x_1,*restrict x_2; // Input ptr                         */
/*  float *restrict x_3,*restrict x_4; // Input ptr                         */
/*  float *restrict x_5; // Input ptr                                       */
/*  float *restrict y_1,*restrict y_2; // Output ptr                        */
/*  float *restrict y_3,*restrict y_4; // Output ptr                        */
/*  float *restrict y_5; // Output ptr                                      */
/*  float *restrict filtVars_1,*restrict filtVars_2; // Filter var mem ptr  */
/*  float *restrict filtVars_3,*restrict filtVars_4; // Filter var mem ptr  */
/*  float *restrict filtVars_5; // Filter var mem ptr                       */
/*  float *filtCfsB, *filtCfsA;  //Feedforward ptrs                         */
/*  float accum1,accum2,accum3,accum4;  //Accumulator regs                  */
/*  float input_1, input_2, input_3, input_4, input_5;                      */
/*  float w1_1, w2_1, w1_2, w2_2, w1_3, w2_3, w1_4, w2_4;//Filter states    */
/*  float w1_5, w2_5;//Filter states                                        */
/*                                                                          */
/*   //Get i/p ptr                                                          */
/*  x_1 = (float *)pParam->pIn[0];                                          */
/*  x_2 = (float *)pParam->pIn[1];                                          */
/*  x_3 = (float *)pParam->pIn[2];                                          */
/*  x_4 = (float *)pParam->pIn[3];                                          */
/*  x_5 = (float *)pParam->pIn[4];                                          */
/*                                                                          */
/*   //Get o/p ptr                                                          */
/*  y_1 = (float *)pParam->pOut[0];                                         */
/*  y_2 = (float *)pParam->pOut[1];                                         */
/*  y_3 = (float *)pParam->pOut[2];                                         */
/*  y_4 = (float *)pParam->pOut[3];                                         */
/*  y_5 = (float *)pParam->pOut[4];                                         */
/*                                                                          */
/*  filtCfsB  = (float *)pParam->pCoef[0];  //Feedforward ptr               */
/*  filtCfsA  = filtCfsB + 3;  //Derive feedback coeff ptr                  */
/*                                                                          */
/*   //Get filter var ptr                                                   */
/*  filtVars_1 = (float *)pParam->pVar[0];                                  */
/*  filtVars_2 = (float *)pParam->pVar[1];                                  */
/*  filtVars_3 = (float *)pParam->pVar[2];                                  */
/*  filtVars_4 = (float *)pParam->pVar[3];                                  */
/*  filtVars_5 = (float *)pParam->pVar[4];                                  */
/*                                                                          */
/*  count = pParam->sampleCount;  //I/p sample block-length                 */
/*                                                                          */
/*   //Get the filter states into corresponding regs                        */
/*  w1_1 = filtVars_1[0];                                                   */
/*  w2_1 = filtVars_1[1];                                                   */
/*                                                                          */
/*  w1_2 = filtVars_2[0];                                                   */
/*  w2_2 = filtVars_2[1];                                                   */
/*                                                                          */
/*  w1_3 = filtVars_3[0];                                                   */
/*  w2_3 = filtVars_3[1];                                                   */
/*                                                                          */
/*  w1_4 = filtVars_4[0];                                                   */
/*  w2_4 = filtVars_4[1];                                                   */
/*                                                                          */
/*  w1_5 = filtVars_5[0];                                                   */
/*  w2_5 = filtVars_5[1];                                                   */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*   //IIR filtering for i/p block length                                   */
/*                                                                          */
/*  #pragma MUST_ITERATE(16, 2048, 4)                                       */
/*  for (samp = 0; samp < count; samp++)                                    */
/*  {                                                                       */
/* Channel-1                                                                */
/*      accum1 = filtCfsA[0]*w1_1;  //a1*w(n-1)                             */
/*      accum2 = filtCfsA[1]*w2_1;  //a2*w(n-2)                             */
/*      accum3 = filtCfsB[1]*w1_1;  //b1*w(n-1)                             */
/*      accum4 = filtCfsB[2]*w2_1;  //b2*w(n-2)                             */
/*                                                                          */
/*      input_1 = *x_1++;  //Get an input sample                            */
/*                                                                          */
/*      w2_1 = w1_1;  //Shift state registers                               */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_1 = input_1 + accum2 + accum1;                                   */
/*                                                                          */
/*       //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           */
/*      *y_1++ = filtCfsB[0]*w1_1 + accum3 + accum4;                        */
/*                                                                          */
/* Channel-2                                                                */
/*      accum1 = filtCfsA[0]*w1_2;  //a1*w(n-1)                             */
/*      accum2 = filtCfsA[1]*w2_2;  //a2*w(n-2)                             */
/*      accum3 = filtCfsB[1]*w1_2;  //b1*w(n-1)                             */
/*      accum4 = filtCfsB[2]*w2_2;  //b2*w(n-2)                             */
/*                                                                          */
/*      input_2 = *x_2++;  //Get an input sample                            */
/*                                                                          */
/*      w2_2 = w1_2;  //Shift state registers                               */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_2 = input_2 + accum2 + accum1;                                   */
/*                                                                          */
/*       //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           */
/*      *y_2++ = filtCfsB[0]*w1_2 + accum3 + accum4;                        */
/*                                                                          */
/* Channel-3                                                                */
/*      accum1 = filtCfsA[0]*w1_3;  //a1*w(n-1)                             */
/*      accum2 = filtCfsA[1]*w2_3;  //a2*w(n-2)                             */
/*      accum3 = filtCfsB[1]*w1_3;  //b1*w(n-1)                             */
/*      accum4 = filtCfsB[2]*w2_3;  //b2*w(n-2)                             */
/*                                                                          */
/*      input_3 = *x_3++;  //Get an input sample                            */
/*                                                                          */
/*      w2_3 = w1_3;  //Shift state registers                               */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_3 = input_3 + accum2 + accum1;                                   */
/*                                                                          */
/*       //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           */
/*      *y_3++ = filtCfsB[0]*w1_3 + accum3 + accum4;                        */
/*                                                                          */
/* Channel-4                                                                */
/*      accum1 = filtCfsA[0]*w1_4;  //a1*w(n-1)                             */
/*      accum2 = filtCfsA[1]*w2_4;  //a2*w(n-2)                             */
/*      accum3 = filtCfsB[1]*w1_4;  //b1*w(n-1)                             */
/*      accum4 = filtCfsB[2]*w2_4;  //b2*w(n-2)                             */
/*                                                                          */
/*      input_4 = *x_4++;  //Get an input sample                            */
/*                                                                          */
/*      w2_4 = w1_4;  //Shift state registers                               */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_4 = input_4 + accum2 + accum1;                                   */
/*                                                                          */
/*       //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           */
/*      *y_4++ = filtCfsB[0]*w1_4 + accum3 + accum4;                        */
/*                                                                          */
/*                                                                          */
/* Channel-5                                                                */
/*      accum1 = filtCfsA[0]*w1_5;  //a1*w(n-1)                             */
/*      accum2 = filtCfsA[1]*w2_5;  //a2*w(n-2)                             */
/*      accum3 = filtCfsB[1]*w1_5;  //b1*w(n-1)                             */
/*      accum4 = filtCfsB[2]*w2_5;  //b2*w(n-2)                             */
/*                                                                          */
/*      input_5 = *x_5++;  //Get an input sample                            */
/*                                                                          */
/*      w2_5 = w1_5;  //Shift state registers                               */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_5 = input_5 + accum2 + accum1;                                   */
/*                                                                          */
/*       //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           */
/*      *y_5++ = filtCfsB[0]*w1_5 + accum3 + accum4;                        */
/*                                                                          */
/*                                                                          */
/*  }                                                                       */
/*                                                                          */
/*   //Update state memory                                                  */
/*                                                                          */
/*  filtVars_1[0] = w1_1;                                                   */
/*  filtVars_1[1] = w2_1;                                                   */
/*  filtVars_2[0] = w1_2;                                                   */
/*  filtVars_2[1] = w2_2;                                                   */
/*  filtVars_3[0] = w1_3;                                                   */
/*  filtVars_3[1] = w2_3;                                                   */
/*  filtVars_4[0] = w1_4;                                                   */
/*  filtVars_4[1] = w2_4;                                                   */
/*                                                                          */
/*  filtVars_5[0] = w1_5;                                                   */
/*  filtVars_5[1] = w2_5;                                                   */
/*                                                                          */
/*                                                                          */
/* OTES                                                                     */
/*                                                                          */
/*     1. Endian: This code is LITTLE ENDIAN.                               */
/*     2. interruptibility: This code is interrupt-tolerant but not         */
/*        interruptible.                                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2004 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
#pragma CODE_SECTION(Filter_iirT2Ch5_u, ".text:optimized");

//#include "Filter_iirT2Ch5_u_o.h"
#include "filters.h"

/* ======================================================================== */
/*S Place file level definitions here.                                     S*/
/* ======================================================================== */

 Int Filter_iirT2Ch5_u(PAF_FilParam * pParam)
{

    int count, samp;  //Loop counters 
    float *restrict x_1,*restrict x_2; // Input ptr 
    float *restrict x_3,*restrict x_4; // Input ptr 
    float *restrict x_5; // Input ptr 
    float *restrict y_1,*restrict y_2; // Output ptr 
    float *restrict y_3,*restrict y_4; // Output ptr 
    float *restrict y_5; // Output ptr 
    float *restrict filtVars_1,*restrict filtVars_2; // Filter var mem ptr 
    float *restrict filtVars_3,*restrict filtVars_4; // Filter var mem ptr 
    float *restrict filtVars_5; // Filter var mem ptr 
    float *filtCfsB, *filtCfsA;  //Feedforward ptrs 
    float accum1,accum2,accum3,accum4;  //Accumulator regs 
    float input_1, input_2, input_3, input_4, input_5;
    float w1_1, w2_1, w1_2, w2_2, w1_3, w2_3, w1_4, w2_4;//Filter states 
    float w1_5, w2_5;//Filter states 
    
     //Get i/p ptr      
    x_1 = (float *)pParam->pIn[0];
    x_2 = (float *)pParam->pIn[1];
    x_3 = (float *)pParam->pIn[2];
    x_4 = (float *)pParam->pIn[3];
    x_5 = (float *)pParam->pIn[4];
    
     //Get o/p ptr 
    y_1 = (float *)pParam->pOut[0];
    y_2 = (float *)pParam->pOut[1];
    y_3 = (float *)pParam->pOut[2];
    y_4 = (float *)pParam->pOut[3];
    y_5 = (float *)pParam->pOut[4];
    
    filtCfsB  = (float *)pParam->pCoef[0];  //Feedforward ptr 
    filtCfsA  = filtCfsB + 3;  //Derive feedback coeff ptr        
    
     //Get filter var ptr 
    filtVars_1 = (float *)pParam->pVar[0];
    filtVars_2 = (float *)pParam->pVar[1];
    filtVars_3 = (float *)pParam->pVar[2];
    filtVars_4 = (float *)pParam->pVar[3];
    filtVars_5 = (float *)pParam->pVar[4];
    
    count = pParam->sampleCount;  //I/p sample block-length 

     //Get the filter states into corresponding regs 
    w1_1 = filtVars_1[0];
    w2_1 = filtVars_1[1];
    
    w1_2 = filtVars_2[0];
    w2_2 = filtVars_2[1];    
    
    w1_3 = filtVars_3[0];
    w2_3 = filtVars_3[1];
    
    w1_4 = filtVars_4[0];
    w2_4 = filtVars_4[1];
    
    w1_5 = filtVars_5[0];
    w2_5 = filtVars_5[1];
    
    
    
     //IIR filtering for i/p block length 
  
    #pragma MUST_ITERATE(16, 2048, 4)  
    for (samp = 0; samp < count; samp++)
    {
 //Channel-1 
        accum1 = filtCfsA[0]*w1_1;  //a1*w(n-1)  
        accum2 = filtCfsA[1]*w2_1;  //a2*w(n-2) 
        accum3 = filtCfsB[1]*w1_1;  //b1*w(n-1) 
        accum4 = filtCfsB[2]*w2_1;  //b2*w(n-2) 
        
        input_1 = *x_1++;  //Get an input sample 
        
        w2_1 = w1_1;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_1 = input_1 + accum2 + accum1;
        
         //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) 
        *y_1++ = filtCfsB[0]*w1_1 + accum3 + accum4;
        
 //Channel-2         
        accum1 = filtCfsA[0]*w1_2;  //a1*w(n-1)  
        accum2 = filtCfsA[1]*w2_2;  //a2*w(n-2) 
        accum3 = filtCfsB[1]*w1_2;  //b1*w(n-1) 
        accum4 = filtCfsB[2]*w2_2;  //b2*w(n-2) 
        
        input_2 = *x_2++;  //Get an input sample 
        
        w2_2 = w1_2;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_2 = input_2 + accum2 + accum1;
        
         //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) 
        *y_2++ = filtCfsB[0]*w1_2 + accum3 + accum4;        
         
 //Channel-3 
        accum1 = filtCfsA[0]*w1_3;  //a1*w(n-1)  
        accum2 = filtCfsA[1]*w2_3;  //a2*w(n-2) 
        accum3 = filtCfsB[1]*w1_3;  //b1*w(n-1) 
        accum4 = filtCfsB[2]*w2_3;  //b2*w(n-2) 
        
        input_3 = *x_3++;  //Get an input sample 
        
        w2_3 = w1_3;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_3 = input_3 + accum2 + accum1;
        
         //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) 
        *y_3++ = filtCfsB[0]*w1_3 + accum3 + accum4;
        
 //Channel-4         
        accum1 = filtCfsA[0]*w1_4;  //a1*w(n-1)  
        accum2 = filtCfsA[1]*w2_4;  //a2*w(n-2) 
        accum3 = filtCfsB[1]*w1_4;  //b1*w(n-1) 
        accum4 = filtCfsB[2]*w2_4;  //b2*w(n-2) 
        
        input_4 = *x_4++;  //Get an input sample 
        
        w2_4 = w1_4;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_4 = input_4 + accum2 + accum1;
        
         //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) 
        *y_4++ = filtCfsB[0]*w1_4 + accum3 + accum4;


 //Channel-5 
        accum1 = filtCfsA[0]*w1_5;  //a1*w(n-1)  
        accum2 = filtCfsA[1]*w2_5;  //a2*w(n-2) 
        accum3 = filtCfsB[1]*w1_5;  //b1*w(n-1) 
        accum4 = filtCfsB[2]*w2_5;  //b2*w(n-2) 
        
        input_5 = *x_5++;  //Get an input sample 
        
        w2_5 = w1_5;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_5 = input_5 + accum2 + accum1;
        
         //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) 
        *y_5++ = filtCfsB[0]*w1_5 + accum3 + accum4;
        
   
    }

     //Update state memory 

    filtVars_1[0] = w1_1;
    filtVars_1[1] = w2_1;
    filtVars_2[0] = w1_2;
    filtVars_2[1] = w2_2;
    filtVars_3[0] = w1_3;
    filtVars_3[1] = w2_3;
    filtVars_4[0] = w1_4;
    filtVars_4[1] = w2_4;
    
    filtVars_5[0] = w1_5;
    filtVars_5[1] = w2_5;

   return(FIL_SUCCESS);

}

/* ======================================================================== */
/*  End of file: Filter_iirT2Ch5_u_o.c                                             */
/* ------------------------------------------------------------------------ */
/*          Copyright (C) 2004 Texas Instruments, Incorporated.             */
/*                          All Rights Reserved.                            */
/* ======================================================================== */
