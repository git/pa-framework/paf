/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
/*
 *  ======== iir2tapNch_cN_df2.c ========
 *  IIR filter implementation(DF2) for tap-2,channels-N, cascade-N and SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2, CHANNELS N & CASCADE N **************************/
/*                                                                           */
/* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)    */
/*                                                                           */
/*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    */
/*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    */
/*                                                                           */
/*                                                                           */
/*          g                   w(n)                                         */
/*     x(n)-->---[+]---->--------@------->-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h"

/*
 *  ======== Filter_iirT2ChN_cN_df2() ========
 *  IIR filter, taps-2, channels-N, cascade-N and in SP 
 */
/* Memory section for the function code */
#pragma CODE_SECTION(Filter_iirT2ChN_cN_df2_mx1, ".text:Filter_iirT2ChN_cN_df2_mx1")
Int Filter_iirT2ChN_cN_df2_mx1( PAF_FilParam *pParam ) 
{
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0;
    Uint uniCoef = 0, inplace = 0;
    Int casc;
    Int channelsBy2, remainBy2;
    
    /* Get handle parameters into temp local variables */
    pIn      = pParam->pIn;
    pOut     = pParam->pOut;
    pCoef    = pParam->pCoef;
    pVar     = pParam->pVar;
    channels = pParam->channels;
    casc     = pParam->use >> 1;

    /* Check whether inplace */
    for( i = 0; i < channels; i++ )
    {
      inplace |= ( Uint )pParam->pIn[i]     ^  ( Uint )pParam->pOut[i];
      uniCoef |= ( ( Uint )pParam->pCoef[0] ^  ( Uint )pParam->pCoef[i] );
    }
    
    /* Implement the remaining filtering for channels */
    for(i = 0; i < channels; i++)
    {
        error += Filter_iirT2Ch1_cN_df2_mx1(pParam);
        FIL_offsetParam( pParam, 1 );
    }
        
    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);     
} /* Int Filter_iirT2ChN_cN_df2() */
