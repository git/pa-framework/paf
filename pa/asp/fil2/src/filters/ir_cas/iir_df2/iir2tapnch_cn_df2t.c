/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  

#include "filters.h"
#include "fil_table.h"

Int Filter_iirT2Ch1_c2_df2t( PAF_FilParam *pParam ) 
{
    Float *restrict x, *restrict y, input, output1, output2;

    Float *restrict filtCfs ; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        

	Float b1, b2, a1, a2;              /* Cascade-1 coeffs   */
    Float bb1, bb2, aa1, aa2;          /* Cascade-2 coeffs   */  
    Float b0 ;                    /* Input gain coeff   */

	Float d0, d1, d2, d3;              /* Filter state regs  */
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    x = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    
    filtCfs = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    
    b0 = filtCfs[0];
    b1 = filtCfs[1];
    b2 = filtCfs[2];
	a1 = filtCfs[3];
    a2 = filtCfs[4];

	bb1 = filtCfs[5];
    bb2 = filtCfs[6];
    aa1 = filtCfs[7];
    aa2 = filtCfs[8];

    
    /* Get the filter states into corresponding regs */
    d0 = filtVars[0];
    d1 = filtVars[1];
    d2 = filtVars[2];
    d3 = filtVars[3];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
       /* Stage1 */

        input = *x++;
        output1 = input * b0 + d0;
        d0 = input*b1 + output1*a1 + d1;
        d1 = input*b2 + output1*a2;

		/* Stage2  */

		output2 = output1 + d2;
		d2   = output1 * bb1 + output2 * aa1 + d3 ;
		d3	 = output1 * bb2 + output2 * aa2 ;
		*y++ = output2 ; 
        
    }
    
    /* Update state memory */
    filtVars[0] = d0;
    filtVars[1] = d1;
	filtVars[2] = d2;
    filtVars[3] = d3;
     
    return FIL_SUCCESS;
} 

Int Filter_iirT2Ch1_c2_ng_df2t( PAF_FilParam *pParam ) 
{
    Float *restrict x, *restrict y, input, output1, output2;

    Float *restrict filtCfs ; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */
    Int count, samp; /* Loop counters */        

	Float b1, b2, a1, a2;              /* Cascade-1 coeffs   */
    Float bb1, bb2, aa1, aa2;          /* Cascade-2 coeffs   */  

	Float d0, d1, d2, d3;              /* Filter state regs  */
    
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    x = (Float *)pParam->pIn [0]; /* Get i/p ptr */
    y = (Float *)pParam->pOut[0]; /* Get o/p ptr */
    
    filtVars = (Float *)pParam->pVar[0]; /* Get filter var ptr */
    
    filtCfs = (Float *)pParam->pCoef[0]; /* Feed-forward coeff. ptr */
    
    b1 = filtCfs[0];
    b2 = filtCfs[1];
	a1 = filtCfs[2];
    a2 = filtCfs[3];

	bb1 = filtCfs[4];
    bb2 = filtCfs[5];
    aa1 = filtCfs[6];
    aa2 = filtCfs[7];

    
    /* Get the filter states into corresponding regs */
    d0 = filtVars[0];
    d1 = filtVars[1];
    d2 = filtVars[2];
    d3 = filtVars[3];

    /* IIR filtering for i/p block length */
    #pragma MUST_ITERATE(16, 2048, 4)
    for (samp = 0; samp < count; samp++)
    {
       /* Stage1 */

        input = *x++;
        output1 = input + d0;
        d0 = input*b1 + output1*a1 + d1;
        d1 = input*b2 + output1*a2;

		/* Stage2  */

		output2 = output1 + d2;
		d2   = output1 * bb1 + output2 * aa1 + d3 ;
		d3	 = output1 * bb2 + output2 * aa2 ;
		*y++ = output2 ; 
        
    }
    
    /* Update state memory */
    filtVars[0] = d0;
    filtVars[1] = d1;
	filtVars[2] = d2;
    filtVars[3] = d3;
     
    return FIL_SUCCESS;
}


Int Filter_iirT2Ch1_cN_df2t( PAF_FilParam *pParam ) 
{
    Void ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int error = 0, multiple;
    Int casc, cascN;
    Float *coefPtr, *inPtr;
    Float *varPtr;
    
    /* Get handle parameters into temp local variables */
    inPtr   = pParam->pIn[0];    
    pCoef   = pParam->pCoef;
    coefPtr = pCoef[0];
    pVar    = pParam->pVar;
    varPtr  = pVar[0];
    casc    = pParam->use >> 1; /* Get number of cascades */
    
    /* 1-stage "cascade" not supported */
    if(casc  == 1)
        return(1);
    
    multiple = casc/2;
    cascN    = 0;
    
    /* Do the gain part */
    if(multiple)
    {
        error   += Filter_iirT2Ch1_c2_df2t(pParam);
        cascN   += 2;
        pCoef[0] = &coefPtr[1 + 4*cascN];
        pVar[0]  = &varPtr[2*cascN];
        pParam->pIn[0] =  pParam->pOut[0];
        
        multiple--;
    }
    
    /* Do the non-gain and multiple 2 part */
    pParam->pIn[0] =  pParam->pOut[0];
    
    for(i =0; i < multiple; i++)
    {
        error   += Filter_iirT2Ch1_c2_ng_df2t(pParam);
        cascN   += 2;
        pCoef[0] = &coefPtr[1 + 4*cascN];
        pVar[0]  = &varPtr[2*cascN];
    }            

    /* Do the last stage (if any.  applicable for odd casccade calls) */
    if (casc&1) {
        // FIXME: HACK, HACK; HACK, STACK; STACK, STACK; STACK, HACK
        Float dummyCoef[8];
        Float  dummyVar[4];
        dummyCoef[0] = coefPtr[1 + 4*cascN + 0];
        dummyCoef[1] = coefPtr[1 + 4*cascN + 1];
        dummyCoef[2] = coefPtr[1 + 4*cascN + 2];
        dummyCoef[3] = coefPtr[1 + 4*cascN + 3];
        dummyCoef[4] = dummyCoef[5] = 
        dummyCoef[6] = dummyCoef[7] = 0;
        dummyVar[0] = varPtr[2*cascN + 0];
        dummyVar[1] = varPtr[2*cascN + 1];
        dummyVar[2] = 0;
        dummyVar[3] = 0;
        pCoef[0] = dummyCoef;
        pVar[0]  = dummyVar;
        error   += Filter_iirT2Ch1_c2_ng_df2t(pParam);
        varPtr[2*cascN + 0] = dummyVar[0];
        varPtr[2*cascN + 1] = dummyVar[1];
    }      
    /* Put back the handle elements */
    pCoef[0]       = coefPtr;
    pParam->pVar   = pVar;
    pVar[0]        = varPtr;
    pParam->pIn[0] = inPtr;
    
    return(error);     
} /* Int Filter_iirT2Ch1_cN_df2t() */

Int Filter_iirT2ChN_cN_df2t( PAF_FilParam *pParam ) 
{

    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0;
    
    /* Get handle parameters into temp local variables */
    pIn      = pParam->pIn;
    pOut     = pParam->pOut;
    pCoef    = pParam->pCoef;
    pVar     = pParam->pVar;
    channels = pParam->channels;

    /* Implement the remaining filtering for channels */
    for(i = 0; i < channels; i++)
    {
        error += Filter_iirT2Ch1_cN_df2t(pParam);
        FIL_offsetParam( pParam, 1 );
    }
        
    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);
}

