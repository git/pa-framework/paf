*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  
*                                                                           * 
*  This routine has following C prototype:                                  * 
*                                                                           * 
*    void iirT2Ch1_mx1_c2(PAF_FilParam *pParam)                             * 
*                                                                           * 
*    where pParam� is a pointer to the lower layer generic filter handle    * 
*    structure �PAF_FilParam�.                                              * 
*                                                                           * 
*    typedef struct PAF_FilParam {                                          * 
*        void  ** pIn ;                                                     * 
*        void  ** pOut ;                                                    * 
*        void  ** pCoef ;                                                   * 
*        void  ** pVar ;                                                    * 
*        int      sampleCount;                                              * 
*        uchar    channels;                                                 * 
*        uint     use;                                                      * 
*     }  PAF_FilParam;                                                      * 
*                                                                           * 
*   Let N be the maximum number of channels that are processed at a time.   * 
*                                                                           * 
*   pIn  -�Pointer to the array of �input channel data� pointers.           * 
*                                                                           * 
*      pIn [0][X0(n)], where X(n) is the input data block, of length �      * 
*          [1][ X1(n) ]                                      sampleCount    * 
*          ............                                                     * 
*          [N-1][ XN(n) ]                                                   * 
*                                                                           * 
*   pOut�- Pointer to the array of �output channel data� pointers.          * 
*                                                                           * 
*      pOut[0][Y0(n)]   , where Y(n) is the input data block, of length     * 
*          [1][ Y1(n) ]                                      sampleCount    * 
*          ............                                                     * 
*          [N-1][ YN(n) ]                                                   * 
*                                                                           * 
*                                                                           * 
*   pCoef - Pointer to the array of�channel coefficient� pointers.          * 
*                                                                           * 
*      pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of        * 
*           [1][ C1(n) ]                                      a channel.    * 
*           ............                                                    * 
*           [N-1][ CN(n) ]                                                  * 
*                                                                           * 
*   pVar - Pointer to the array of channel state/private memory�pointers.   * 
*                                                                           * 
*      pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to           * 
*           [1][ m1(n) ]                                        a channel.  * 
*          ............                                                     * 
*           [N-1][ mN(n) ]                                                  * 
*                                                                           * 
*   channels - Specifies the number of channels that are to be filtered,    * 
*              starting sequentially from channel 0.                        * 
*                                                                           * 
*   sampleCount - Specifies the sample length of the input data block.      * 
*                                                                           * 
*   use - This is a 32 bit field that can be used, to fit in some extra     * 
*          parameters or as a pointer to additional filter parameters.      * 
*                                                                           * 
*                                                                           * 
*  DESCRIPTION                                                              * 
*                                                                           * 
*      This routine implements IIR filter implementation for tap-2 &        * 
*      channels-1, cascade-2, SP-DP.                                        * 
*                                                                           * 
*                                                                           * 
*  Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                          * 
*                            ----------------------                         * 
*                             1  - a1*z~1 - a2*z~2                          * 
*                                                                           * 
*  Direct form    : y(n) = x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)     * 
*                                                                           * 
*  Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                  * 
*                   y(n) = w(n)    + b1*w(n-1) + b2*w(n-2)                  * 
*                                                                           * 
*  Filter variables :                                                       * 
*     y(n) - *y,         x(n)   - *x                                        * 
*     w(n) -             w(n-1) - w1         w(n-2) - w2                    * 
*     b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                  * 
*     a1 - filtCfsA[0], a2 - filtCfsA[1]                                    * 
*                                                                           * 
*                                                                           * 
*  Implementation structure with common i/p gain for 2-Tap,Cascade-2:       * 
*         g         w(n)                           w'(n)                    * 
*    x(n)->-[+]-->---@-->>----[+]->------>-[+]-->---@-->>----[+]->-y(n)     * 
*            |       |         |            |       |         |             * 
*            ^       v         ^            ^       v         ^             * 
*            |  a1 +-----+ b1  |            | a'1 +-----+ b'1 |             * 
*           [+]-<<-| Z~1 |->>-[+]          [+]-<<-| Z~1 |->>-[+]            * 
*            |     +-----+     |            |     +-----+     |             * 
*            |       |         |            |       |         |             * 
*            ^       v         ^            ^       v         ^             * 
*            |  a2 +-----+ b2  |            | a'2 +-----+ b'2 |             * 
*            ^ -<<-| Z~1 |->>--^            ^ -<<-| Z~1 |->>--^             * 
*                  +-----+                        +-----+                   * 
*                                                                           * 
*                                                                           * 
*  void iirT2Ch1_mx1_c2( PAF_FilParam *pParam )                             * 
*   {                                                                       * 
*     float *filtCfs;                    /* Coeff ptrs         */           * 
*     double *restrict filtVars;          /* Filter var mem ptr */          * 
*     float *restrict x;                 /* Input ptr          */           * 
*     float * y;                         /* Output ptr         */           * 
*     double accum1,accum2,accum3,accum4; /* Accumulator regs   */          * 
*     double input_data, output;          /* Data regs          */          * 
*     int count, samp;                   /* Loop counters      */           * 
*     double w1, w2, w3, w4;              /* Filter state regs  */          * 
*     float b1, b2, a1, a2;              /* Cascade-1 coeffs   */           * 
*     float bb1, bb2, aa1, aa2;          /* Cascade-2 coeffs   */           * 
*     float b0, gain;                    /* Input gain coeff   */           * 
*                                                                           * 
*      x = (float *)pParam->pIn [0]; /* Get i/p ptr */                      * 
*      y = (float *)pParam->pOut[0]; /* Get o/p ptr */                      * 
*                                                                           * 
*      filtCfs = (float *)pParam->pCoef[0]; /* Get coeff pointer */         * 
*                                                                           * 
*      filtVars = (double *)pParam->pVar[0]; /* Get filter var ptr */       * 
*                                                                           * 
*      count = pParam->sampleCount; /* I/p sample block-length */           * 
*                                                                           * 
*      /* Get the coefficients */                                           * 
*     b0 = filtCfs[0];                                                      * 
*     b1 = filtCfs[1];                                                      * 
*     b2 = filtCfs[2];                                                      * 
*     a1 = filtCfs[3];                                                      * 
*     a2 = filtCfs[4];                                                      * 
*                                                                           * 
*     bb1 = filtCfs[5];                                                     * 
*     bb2 = filtCfs[6];                                                     * 
*     aa1 = filtCfs[7];                                                     * 
*     aa2 = filtCfs[8];                                                     * 
*                                                                           * 
*     gain = b0;                                                            * 
*                                                                           * 
*   /* Get the filter states into corresponding regs */                     * 
*     w1 = filtVars[0];                                                     * 
*     w2 = filtVars[1];                                                     * 
*     w3 = filtVars[2];                                                     * 
*     w4 = filtVars[3];                                                     * 
*                                                                           * 
*   /* IIR filtering for i/p block length */                                * 
*   #pragma MUST_ITERATE(16, 2048, 4)                                       * 
*   for (samp = 0; samp < count; samp++)                                    * 
*   {                                                                       * 
*       /*    Stage-1    */                                                 * 
*       accum1 = a1*w1; /* a1*w(n-1) */                                     * 
*       accum2 = a2*w2; /* a2*w(n-2) */                                     * 
*                                                                           * 
*       accum3 = b1*w1; /* b1*w(n-1) */                                     * 
*       accum4 = b2*w2; /* b2*w(n-2) */                                     * 
*                                                                           * 
*       input_data = *x++ * gain; /* Get i/p sample */                      * 
*                                                                           * 
*       w2 = w1; /* w(n-1) = w(n) */                                        * 
*                                                                           * 
*       /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */                           * 
*       w1 = input_data + accum2 + accum1;                                  * 
*                                                                           * 
*       /* y(n) = w(n) + b1*w(n-1) + b2*w(n-2) */                           * 
*       output = w1 + accum3 + accum4;                                      * 
*                                                                           * 
*       /*    Stage-2    */                                                 * 
*       accum1 = aa1*w3; /* a1*w(n-1) */                                    * 
*       accum2 = aa2*w4; /* a2*w(n-2) */                                    * 
*                                                                           * 
*       accum3 = bb1*w3; /* b1*w(n-1) */                                    * 
*       accum4 = bb2*w4; /* b2*w(n-2) */                                    * 
*                                                                           * 
*       w4 = w3; /* w(n-1) = w(n) */                                        * 
*                                                                           * 
*       /* w(n) = x(n) + a1*w(n-1) + a2*w(n-2) */                           * 
*       w3 = output + accum2 + accum1;                                      * 
*                                                                           * 
*       /* y(n) = w(n) + b1*w(n-1) + b2*w(n-2) */                           * 
*       *y++ = w3 + accum3 + accum4;                                        * 
*                                                                           * 
*     }                                                                     * 
*                                                                           * 
*    /* Update state memory */                                              * 
*     filtVars[0] = w1;                                                     * 
*     filtVars[1] = w2;                                                     * 
*     filtVars[2] = w3;                                                     * 
*     filtVars[3] = w4;                                                     * 
*                                                                           * 
*   }                                                                       * 
*                                                                           * 
*  NOTES                                                                    * 
*                                                                           * 
*      1. Endian: This code is LITTLE ENDIAN.                               * 
*      2. interruptibility: This code is interrupt-tolerant but not         * 
*         interruptible.                                                    * 
* ------------------------------------------------------------------------- *
*             Copyright (c) 2004 Texas Instruments, Incorporated.           *
*                            All Rights Reserved.                           *
* ========================================================================= *
				.global _Filter_iirT2Ch1_c2_ng_df2_mx1
		        .sect ".text:Filter_iirT2Ch1_c2_ng_df2_mx1"

_Filter_iirT2Ch1_c2_ng_df2_mx1:   .cproc  pParam
            .reg pIn, pOut, pCoef, pVar, count, x, y, input_data
            .reg filtCfs, filtVars, w1h:w1l, w2h:w2l, sum1:sum0
            .reg w3h:w3l, w4h:w4l, cbb1, cbb2, caa1, caa2 
            .reg cb0, cb1, cb2, ca1, ca2, output_data,  oph:opl 
            .reg ac1:ac0, ac3:ac2, ac5:ac4, ac7:ac6, ac9:ac8
            .no_mdep             

             LDW   *pParam[0],        pIn       ;pIn   = pParam[0]
             LDW   *pParam[1],        pOut      ;pOut  = pParam[1]
             LDW   *pParam[2],        pCoef     ;pCoef = pParam[2]
             LDW   *pParam[3],        pVar      ;pVar  = pParam[3]
             LDW   *pParam[4],        count     ;count = pParam[4]

             LDW   *pIn[0],           x         ;x = pIn[0]
             LDW   *pOut[0],          y         ;y = pOut[0]

             ;Get the coeff and var ptrs
             LDW   *pCoef[0],         filtCfs   ;filtCfs = pCoef[0]
             LDW   *pVar[0],          filtVars  ;filtVars = pVar[0]  

             ;Get filter states into regs
             LDDW  *filtVars[0],      w1h:w1l   ;w1 = filtVars[0]
             LDDW  *filtVars[1],      w2h:w2l   ;w2 = filtVars[1]
             LDDW  *filtVars[2],      w3h:w3l   ;w3 = filtVars[2]
             LDDW  *filtVars[3],      w4h:w4l   ;w4 = filtVars[3]
             

             ;Get the coeffs, of type float.
             ;Feedforward
             ;LDW  *filtCfs,          cb0 ;cb0 = filtCfs[0] // I/p gain
             LDW  *filtCfs[0],       cb1 ;cb1 = filtCfs[1]
             LDW  *filtCfs[1],       cb2 ;cb2 = filtCfs[2]
             ;Feedback
             LDW  *filtCfs[2],       ca1 ;ca1 = filtCfs[3]
             LDW  *filtCfs[3],       ca2 ;ca2 = filtCfs[4]      
             ;Feedforward for cascade 2            
             LDW  *filtCfs[4],       cbb1 ;cbb1 = filtCfs[5]
             LDW  *filtCfs[5],       cbb2 ;cbb2 = filtCfs[6]
             ;Feedback
             LDW  *filtCfs[6],       caa1 ;caa1 = filtCfs[7]
             LDW  *filtCfs[7],       caa2 ;caa2 = filtCfs[8]      
                         

LOOP:
             ;Get one input sample 
             
             LDW   *x++,              input_data ;input_data = *x++

             ;Get b0*x(n),a2*w(n-2),a1*w(n-1)
             SPDP       input_data,        ac1:ac0        ;Convert input to DP
             MPYSPDP    ca1,               w1h:w1l,       ac3:ac2;ac2=w1*ca1
             MPYSPDP    ca2,               w2h:w2l,       ac5:ac4;ac3=w2*ca2
            
             ;Get 'b1'*w(n-1),'b2'*w(n-2)
             MPYSPDP    cb1,               w1h:w1l,       ac7:ac6 ;ac4=w1*cb1
             MPYSPDP    cb2,               w2h:w2l,       ac9:ac8 ;ac5=w2*cb2

             ;w(n)='b0*x(n)'+'a1*w(n-1)'+'a2*w(n-2)
             ADDDP      ac1:ac0,           ac5:ac4,       ac1:ac0   ;ac1=ac1+ac3
             ADDDP      ac1:ac0,           ac3:ac2,       sum1:sum0 ;sum+=ac2

             ;Shift filter states, of type double             
             MV         w1h,              w2h       ; w1  = w2
             MV         w1l,              w2l
             MV         sum1,             w1h       ; sum = w1
             MV         sum0,             w1l 

             ;op=w(n)+'b1*w(n-1)'+'b2*w(n-2)'
            
             ADDDP      ac7:ac6,          ac9:ac8,        oph:opl ;op =ac4+ac5
             ADDDP      oph:opl,          sum1:sum0,      oph:opl ;op +=sum
                                             
             ;Get aa2*w(n-2),aa1*w(n-1)
             
             MPYSPDP    caa1,              w3h:w3l,       ac3:ac2;ac2=w3*caa1
             MPYSPDP    caa2,              w4h:w4l,       ac5:ac4;ac3=w4*caa2
            
             ;Get 'bb1'*w(n-1),'bb2'*w(n-2)
             MPYSPDP    cbb1,              w3h:w3l,       ac7:ac6 ;ac4=w3*cbb1
             MPYSPDP    cbb2,              w4h:w4l,       ac9:ac8 ;ac5=w4*cbb2

             ;w(n)= op +'aa1*w(n-1)'+'aa2*w(n-2)
             ADDDP      oph:opl,           ac5:ac4,       ac1:ac0   ;ac1=op+ac3
             ADDDP      ac1:ac0,           ac3:ac2,       sum1:sum0 ;sum+=ac2
             
             ;Shift filter states, of type double             
             MV         w3h,              w4h       ; w3  = w4
             MV         w3l,              w4l
             MV         sum1,             w3h       ; sum = w3
             MV         sum0,             w3l 

             ;y(n)=w(n)+'bb1*w(n-1)'+'bb2*w(n-2)'
            
             ADDDP      ac7:ac6,          ac9:ac8,        oph:opl ;op =ac4+ac5
             ADDDP      oph:opl,          sum1:sum0,      oph:opl ;op +=sum             
             
             ;Convert back to SP and Store the calculated o/p             
             DPSP  oph:opl,          output_data         ;output_data=(float)op 
                                                                                        
             STW   output_data,      *y++                ;*y++ = output_data
                                                                           
   [count]   SUB   count,             1,          count;if(count) count=count-1;
   [count]   B     LOOP ; if(count) goto LOOP 

             ;Update filter state memory, of type double
             STW   w1l,              *filtVars      ;filtVars[0]=w1
             STW   w1h,              *filtVars[1]
             STW   w2l,              *filtVars[2]   ;filtVars[1]=w2
             STW   w2h,              *filtVars[3]      
             STW   w3l,              *filtVars[4]   ;filtVars[2]=w3
             STW   w3h,              *filtVars[5]
             STW   w4l,              *filtVars[6]   ;filtVars[3]=w4
             STW   w4h,              *filtVars[7]      
             

             .return count ;return(count)
             .endproc

* ======================================================================== *
*  End of file: iirT2Ch1_mx1_c2_ng_n.sa                                    *
* ------------------------------------------------------------------------ *
*          Copyright (C) 2004 Texas Instruments, Incorporated.             *
*                          All Rights Reserved.                            *
* ======================================================================== *
