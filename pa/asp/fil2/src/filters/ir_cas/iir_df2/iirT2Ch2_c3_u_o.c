/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
/*                                                                          */
/* his routine has following C prototype:                                   */
/*                                                                          */
/*   void Filter_iirT2Ch2_c3_u(PAF_FilParam *pParam)                               */
/*                                                                          */
/*   where pParam� is a pointer to the lower layer generic filter handle    */
/*   structure �PAF_FilParam�.                                              */
/*                                                                          */
/*   typedef struct PAF_FilParam {                                          */
/*       void  ** pIn ;                                                     */
/*       void  ** pOut ;                                                    */
/*       void  ** pCoef ;                                                   */
/*       void  ** pVar ;                                                    */
/*       int      sampleCount;                                              */
/*       uchar    channels;                                                 */
/*       uint     use;                                                      */
/*    }  PAF_FilParam;                                                      */
/*                                                                          */
/*  Let N be the maximum number of channels that are processed at a time.   */
/*                                                                          */
/*  pIn  -�Pointer to the array of �input channel data� pointers.           */
/*                                                                          */
/*     pIn [0][X0(n)], where X(n) is the input data block, of length �      */
/*         [1][ X1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ XN(n) ]                                                   */
/*                                                                          */
/*  pOut�- Pointer to the array of �output channel data� pointers.          */
/*                                                                          */
/*     pOut[0][Y0(n)]   , where Y(n) is the input data block, of length     */
/*         [1][ Y1(n) ]                                      sampleCount    */
/*         ............                                                     */
/*         [N-1][ YN(n) ]                                                   */
/*                                                                          */
/*                                                                          */
/*  pCoef - Pointer to the array of�channel coefficient� pointers.          */
/*                                                                          */
/*     pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of        */
/*          [1][ C1(n) ]                                      a channel.    */
/*          ............                                                    */
/*          [N-1][ CN(n) ]                                                  */
/*                                                                          */
/*  pVar - Pointer to the array of channel state/private memory�pointers.   */
/*                                                                          */
/*     pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to           */
/*          [1][ m1(n) ]                                        a channel.  */
/*         ............                                                     */
/*          [N-1][ mN(n) ]                                                  */
/*                                                                          */
/*  channels - Specifies the number of channels that are to be filtered,    */
/*             starting sequentially from channel 0.                        */
/*                                                                          */
/*  sampleCount - Specifies the sample length of the input data block.      */
/*                                                                          */
/*  use - This is a 32 bit field that can be used, to fit in some extra     */
/*         parameters or as a pointer to additional filter parameters.      */
/*                                                                          */
/*                                                                          */
/* ESCRIPTION                                                               */
/*                                                                          */
/*     This routine implements IIR filter implementation for tap-2 &        */
/*     channels-2, cascade-3, SP.                                           */
/*                                                                          */
/*                                                                          */
/* lter equation  : H(z) =  1  + b1*z~1 + b2*z~2                            */
/*                         ----------------------                           */
/*                          1  - a1*z~1 - a2*z~2                            */
/*                                                                          */
/* Direct form    : y(n) = x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)     */
/*                                                                          */
/* Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                  */
/*                  y(n) = w(n)    + b1*w(n-1) + b2*w(n-2)                  */
/*                                                                          */
/* Filter variables :                                                       */
/*    y(n) - *y,         x(n)   - *x                                        */
/*    w(n) -             w(n-1) - w1         w(n-2) - w2                    */
/*    b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                  */
/*    a1 - filtCfsA[0], a2 - filtCfsA[1]                                    */
/*                                                                          */
/*                                                                          */
/* Implementation structure with common i/p gain for 2-Tap,Cascade-2:       */
/*        g         w(n)                           w'(n)                    */
/*   x(n)->-[+]-->---@-->>----[+]->------>-[+]-->---@-->>----[+]->-y(n)     */
/*           |       |         |            |       |         |             */
/*           ^       v         ^            ^       v         ^             */
/*           |  a1 +-----+ b1  |            | a'1 +-----+ b'1 |             */
/*          [+]-<<-| Z~1 |->>-[+]          [+]-<<-| Z~1 |->>-[+]            */
/*           |     +-----+     |            |     +-----+     |             */
/*           |       |         |            |       |         |             */
/*           ^       v         ^            ^       v         ^             */
/*           |  a2 +-----+ b2  |            | a'2 +-----+ b'2 |             */
/*           ^ -<<-| Z~1 |->>--^            ^ -<<-| Z~1 |->>--^             */
/*                 +-----+                        +-----+                   */
/*                                                                          */
/*                                                                          */
/* void Filter_iirT2Ch2_c3_u( PAF_FilParam *pParam )                                 */
/*                                                                          */
/*  int count, samp;  //Loop counters                                       */
/*  float *restrict x_1,*restrict x_2; // Input ptr                         */
/*  float *restrict y_1,*restrict y_2; // Output ptr                        */
/*  float *restrict filtVars_1,*restrict filtVars_2;// Filter var mem ptr   */
/*  float *restrict filtCfs;  //Feedforward ptrs                            */
/*  float b0,gain,output_1,output_2;                                        */
/*  float a1_c1,a2_c1,b1_c1,b2_c1; // Cascade-1 Coeffs                      */
/*  float a1_c2,a2_c2,b1_c2,b2_c2; // Cascade-2 Coeffs                      */
/*  float a1_c3,a2_c3,b1_c3,b2_c3; // Cascade-3 Coeffs                      */
/*  float accum1,accum2,accum3,accum4;  //Accumulator regs                  */
/*  float input_1,input_2;                                                  */
/*  float w1_c1_1, w2_c1_1, w1_c2_1, w2_c2_1;  //Filter state regs          */
/*  float w1_c3_1, w2_c3_1;                    //Filter state regs          */
/*                                                                          */
/*  float w1_c1_2, w2_c1_2, w1_c2_2, w2_c2_2;  //Filter state regs          */
/*  float w1_c3_2, w2_c3_2;                    //Filter state regs          */
/*                                                                          */
/*  //Get i/p ptr                                                           */
/*  x_1 = (float *)pParam->pIn[0];                                          */
/*  x_2 = (float *)pParam->pIn[1];                                          */
/*                                                                          */
/*   //Get o/p ptr                                                          */
/*  y_1 = (float *)pParam->pOut[0];                                         */
/*  y_2 = (float *)pParam->pOut[1];                                         */
/*                                                                          */
/*  filtCfs  = (float *)pParam->pCoef[0];  //Feedforward ptr                */
/*  b0    = filtCfs[0];                                                     */
/*                                                                          */
/*  b1_c1 = filtCfs[1];                                                     */
/*  b2_c1 = filtCfs[2];                                                     */
/*  a1_c1 = filtCfs[3];                                                     */
/*  a2_c1 = filtCfs[4];                                                     */
/*                                                                          */
/*  b1_c2 = filtCfs[5];                                                     */
/*  b2_c2 = filtCfs[6];                                                     */
/*  a1_c2 = filtCfs[7];                                                     */
/*  a2_c2 = filtCfs[8];                                                     */
/*                                                                          */
/*  b1_c3 = filtCfs[9];                                                     */
/*  b2_c3 = filtCfs[10];                                                    */
/*  a1_c3 = filtCfs[11];                                                    */
/*  a2_c3 = filtCfs[12];                                                    */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*   //Get filter var ptr                                                   */
/*  filtVars_1 = (float *)pParam->pVar[0];                                  */
/*  filtVars_2 = (float *)pParam->pVar[1];                                  */
/*                                                                          */
/*   //Get the filter states into corresponding regs                        */
/*  w1_c1_1 = filtVars_1[0];                                                */
/*  w2_c1_1 = filtVars_1[1];                                                */
/*                                                                          */
/*  w1_c2_1 = filtVars_1[2];                                                */
/*  w2_c2_1 = filtVars_1[3];                                                */
/*                                                                          */
/*  w1_c3_1 = filtVars_1[4];                                                */
/*  w2_c3_1 = filtVars_1[5];                                                */
/*                                                                          */
/*                                                                          */
/*  w1_c1_2 = filtVars_2[0];                                                */
/*  w2_c1_2 = filtVars_2[1];                                                */
/*                                                                          */
/*  w1_c2_2 = filtVars_2[2];                                                */
/*  w2_c2_2 = filtVars_2[3];                                                */
/*                                                                          */
/*  w1_c3_2 = filtVars_2[4];                                                */
/*  w2_c3_2 = filtVars_2[5];                                                */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*  count = pParam->sampleCount;  //I/p sample block-length                 */
/*  gain  = b0;                                                             */
/*                                                                          */
/*   //IIR filtering for i/p block length                                   */
/*                                                                          */
/*  #pragma MUST_ITERATE(16, 2048, 4)                                       */
/*  for (samp = 0; samp < count; samp++)                                    */
/*  {                                                                       */
/*                                                                          */
/* CH-I/STAGE-1                                                             */
/*      accum1 = a1_c1*w1_c1_1;  //a1*w(n-1)                                */
/*      accum2 = a2_c1*w2_c1_1;  //a2*w(n-2)                                */
/*      accum3 = b1_c1*w1_c1_1;  //b1*w(n-1)                                */
/*      accum4 = b2_c1*w2_c1_1;  //b2*w(n-2)                                */
/*                                                                          */
/*      input_1 = *x_1++ * gain;  //Get an input sample                     */
/*                                                                          */
/*      w2_c1_1 = w1_c1_1;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c1_1 = input_1 + accum2 + accum1;                                */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      output_1  = w1_c1_1 + accum3 + accum4;                              */
/*                                                                          */
/* CH-I/STAGE-2                                                             */
/*      accum1 = a1_c2*w1_c2_1;  //a1*w(n-1)                                */
/*      accum2 = a2_c2*w2_c2_1;  //a2*w(n-2)                                */
/*      accum3 = b1_c2*w1_c2_1;  //b1*w(n-1)                                */
/*      accum4 = b2_c2*w2_c2_1;  //b2*w(n-2)                                */
/*                                                                          */
/*      w2_c2_1 = w1_c2_1;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c2_1 = output_1 + accum2 + accum1;                               */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      output_1  = w1_c2_1 + accum3 + accum4;                              */
/*                                                                          */
/* CH-I/STAGE-3                                                             */
/*      accum1 = a1_c3*w1_c3_1;  //a1*w(n-1)                                */
/*      accum2 = a2_c3*w2_c3_1;  //a2*w(n-2)                                */
/*      accum3 = b1_c3*w1_c3_1;  //b1*w(n-1)                                */
/*      accum4 = b2_c3*w2_c3_1;  //b2*w(n-2)                                */
/*                                                                          */
/*      w2_c3_1 = w1_c3_1;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c3_1 = output_1 + accum2 + accum1;                               */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      *y_1++    = w1_c3_1 + accum3 + accum4;                              */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/* CH-II/STAGE-1                                                            */
/*      accum1 = a1_c1*w1_c1_2;  //a1*w(n-1)                                */
/*      accum2 = a2_c1*w2_c1_2;  //a2*w(n-2)                                */
/*      accum3 = b1_c1*w1_c1_2;  //b1*w(n-1)                                */
/*      accum4 = b2_c1*w2_c1_2;  //b2*w(n-2)                                */
/*                                                                          */
/*      input_2 = *x_2++ * gain;  //Get an input sample                     */
/*                                                                          */
/*      w2_c1_2 = w1_c1_2;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c1_2 = input_2 + accum2 + accum1;                                */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      output_2  = w1_c1_2 + accum3 + accum4;                              */
/*                                                                          */
/* CH-II/STAGE-2                                                            */
/*      accum1 = a1_c2*w1_c2_2;  //a1*w(n-1)                                */
/*      accum2 = a2_c2*w2_c2_2;  //a2*w(n-2)                                */
/*      accum3 = b1_c2*w1_c2_2;  //b1*w(n-1)                                */
/*      accum4 = b2_c2*w2_c2_2;  //b2*w(n-2)                                */
/*                                                                          */
/*      w2_c2_2 = w1_c2_2;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c2_2 = output_2 + accum2 + accum1;                               */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      output_2  = w1_c2_2 + accum3 + accum4;                              */
/*                                                                          */
/* CH-II/STAGE-3                                                            */
/*      accum1 = a1_c3*w1_c3_2;  //a1*w(n-1)                                */
/*      accum2 = a2_c3*w2_c3_2;  //a2*w(n-2)                                */
/*      accum3 = b1_c3*w1_c3_2;  //b1*w(n-1)                                */
/*      accum4 = b2_c3*w2_c3_2;  //b2*w(n-2)                                */
/*                                                                          */
/*      w2_c3_2 = w1_c3_2;  //Shift state registers                         */
/*                                                                          */
/*       //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              */
/*      w1_c3_2 = output_2 + accum2 + accum1;                               */
/*                                                                          */
/*       //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                              */
/*      *y_2++  = w1_c3_2 + accum3 + accum4;                                */
/*                                                                          */
/*                                                                          */
/*  }                                                                       */
/*                                                                          */
/*   //Update state memory                                                  */
/*                                                                          */
/*  filtVars_1[0] = w1_c1_1;                                                */
/*  filtVars_1[1] = w2_c1_1;                                                */
/*  filtVars_1[2] = w1_c2_1;                                                */
/*  filtVars_1[3] = w2_c2_1;                                                */
/*  filtVars_1[4] = w1_c3_1;                                                */
/*  filtVars_1[5] = w2_c3_1;                                                */
/*                                                                          */
/*  filtVars_2[0] = w1_c1_2;                                                */
/*  filtVars_2[1] = w2_c1_2;                                                */
/*  filtVars_2[2] = w1_c2_2;                                                */
/*  filtVars_2[3] = w2_c2_2;                                                */
/*  filtVars_2[4] = w1_c3_2;                                                */
/*  filtVars_2[5] = w2_c3_2;                                                */
/*                                                                          */
/*                                                                          */
/* OTES                                                                     */
/*                                                                          */
/*     1. Endian: This code is LITTLE ENDIAN.                               */
/*     2. interruptibility: This code is interrupt-tolerant but not         */
/*        interruptible.                                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2004 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
#pragma CODE_SECTION(Filter_iirT2Ch2_c3_u_df2, ".text:optimized");

//#include "Filter_iirT2Ch2_c3_u_o.h"
#include "filters.h"

/* ======================================================================== */
/*S Place file level definitions here.                                     S*/
/* ======================================================================== */

Int Filter_iirT2Ch2_c3_u_df2(PAF_FilParam * pParam)
{

    int count, samp;  //Loop counters 
    float *restrict x_1,*restrict x_2; // Input ptr 
    float *restrict y_1,*restrict y_2; // Output ptr 
    float *restrict filtVars_1,*restrict filtVars_2;// Filter var mem ptr
    float *restrict filtCfs;  //Feedforward ptrs 
    float b0,gain,output_1,output_2;
    float a1_c1,a2_c1,b1_c1,b2_c1; // Cascade-1 Coeffs 
    float a1_c2,a2_c2,b1_c2,b2_c2; // Cascade-2 Coeffs
    float a1_c3,a2_c3,b1_c3,b2_c3; // Cascade-3 Coeffs
    float accum1,accum2,accum3,accum4;  //Accumulator regs 
    float input_1,input_2;
    float w1_c1_1, w2_c1_1, w1_c2_1, w2_c2_1;  //Filter state regs   
    float w1_c3_1, w2_c3_1;                    //Filter state regs   
    
    float w1_c1_2, w2_c1_2, w1_c2_2, w2_c2_2;  //Filter state regs   
    float w1_c3_2, w2_c3_2;                    //Filter state regs   
    
    //Get i/p ptr      
    x_1 = (float *)pParam->pIn[0];
    x_2 = (float *)pParam->pIn[1];
    
     //Get o/p ptr 
    y_1 = (float *)pParam->pOut[0];
    y_2 = (float *)pParam->pOut[1];
    
    filtCfs  = (float *)pParam->pCoef[0];  //Feedforward ptr 
    b0    = filtCfs[0];
  
    b1_c1 = filtCfs[1]; 
    b2_c1 = filtCfs[2]; 
    a1_c1 = filtCfs[3]; 
    a2_c1 = filtCfs[4];

    b1_c2 = filtCfs[5]; 
    b2_c2 = filtCfs[6]; 
    a1_c2 = filtCfs[7]; 
    a2_c2 = filtCfs[8];

    b1_c3 = filtCfs[9]; 
    b2_c3 = filtCfs[10]; 
    a1_c3 = filtCfs[11]; 
    a2_c3 = filtCfs[12];
 


     //Get filter var ptr 
    filtVars_1 = (float *)pParam->pVar[0];
    filtVars_2 = (float *)pParam->pVar[1];

     //Get the filter states into corresponding regs 
    w1_c1_1 = filtVars_1[0];
    w2_c1_1 = filtVars_1[1];
    
    w1_c2_1 = filtVars_1[2];
    w2_c2_1 = filtVars_1[3];
    
    w1_c3_1 = filtVars_1[4];
    w2_c3_1 = filtVars_1[5];
    

    w1_c1_2 = filtVars_2[0];
    w2_c1_2 = filtVars_2[1];
    
    w1_c2_2 = filtVars_2[2];
    w2_c2_2 = filtVars_2[3];
    
    w1_c3_2 = filtVars_2[4];
    w2_c3_2 = filtVars_2[5];
    

    
    count = pParam->sampleCount;  //I/p sample block-length 
    gain  = b0;

     //IIR filtering for i/p block length 
  
    #pragma MUST_ITERATE(16, 2048, 4)  
    for (samp = 0; samp < count; samp++)
    {

 //CH-I/STAGE-1  
        accum1 = a1_c1*w1_c1_1;  //a1*w(n-1)  
        accum2 = a2_c1*w2_c1_1;  //a2*w(n-2) 
        accum3 = b1_c1*w1_c1_1;  //b1*w(n-1) 
        accum4 = b2_c1*w2_c1_1;  //b2*w(n-2) 
        
        input_1 = *x_1++ * gain;  //Get an input sample 
        
        w2_c1_1 = w1_c1_1;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c1_1 = input_1 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2) 
        output_1  = w1_c1_1 + accum3 + accum4;
        
 //CH-I/STAGE-2         
        accum1 = a1_c2*w1_c2_1;  //a1*w(n-1)  
        accum2 = a2_c2*w2_c2_1;  //a2*w(n-2) 
        accum3 = b1_c2*w1_c2_1;  //b1*w(n-1) 
        accum4 = b2_c2*w2_c2_1;  //b2*w(n-2) 
        
        w2_c2_1 = w1_c2_1;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c2_1 = output_1 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2) 
        output_1  = w1_c2_1 + accum3 + accum4;        
        
 //CH-I/STAGE-3         
        accum1 = a1_c3*w1_c3_1;  //a1*w(n-1)  
        accum2 = a2_c3*w2_c3_1;  //a2*w(n-2) 
        accum3 = b1_c3*w1_c3_1;  //b1*w(n-1) 
        accum4 = b2_c3*w2_c3_1;  //b2*w(n-2) 
        
        w2_c3_1 = w1_c3_1;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c3_1 = output_1 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)
        *y_1++    = w1_c3_1 + accum3 + accum4;        
        
        
         
 //CH-II/STAGE-1 
        accum1 = a1_c1*w1_c1_2;  //a1*w(n-1)  
        accum2 = a2_c1*w2_c1_2;  //a2*w(n-2) 
        accum3 = b1_c1*w1_c1_2;  //b1*w(n-1) 
        accum4 = b2_c1*w2_c1_2;  //b2*w(n-2) 
        
        input_2 = *x_2++ * gain;  //Get an input sample 
        
        w2_c1_2 = w1_c1_2;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c1_2 = input_2 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2) 
        output_2  = w1_c1_2 + accum3 + accum4;
        
 //CH-II/STAGE-2         
        accum1 = a1_c2*w1_c2_2;  //a1*w(n-1)  
        accum2 = a2_c2*w2_c2_2;  //a2*w(n-2) 
        accum3 = b1_c2*w1_c2_2;  //b1*w(n-1) 
        accum4 = b2_c2*w2_c2_2;  //b2*w(n-2) 
        
        w2_c2_2 = w1_c2_2;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c2_2 = output_2 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2) 
        output_2  = w1_c2_2 + accum3 + accum4;        
        
 //CH-II/STAGE-3         
        accum1 = a1_c3*w1_c3_2;  //a1*w(n-1)  
        accum2 = a2_c3*w2_c3_2;  //a2*w(n-2) 
        accum3 = b1_c3*w1_c3_2;  //b1*w(n-1) 
        accum4 = b2_c3*w2_c3_2;  //b2*w(n-2) 
        
        w2_c3_2 = w1_c3_2;  //Shift state registers 
        
         //w(n) = x(n) + a1*w(n-1) + a2*w(n-2) 
        w1_c3_2 = output_2 + accum2 + accum1;
        
         //y(n) = w(n) + b1*w(n-1) + b2*w(n-2)
        *y_2++  = w1_c3_2 + accum3 + accum4;        
        
        
    }

     //Update state memory 

    filtVars_1[0] = w1_c1_1;
    filtVars_1[1] = w2_c1_1;
    filtVars_1[2] = w1_c2_1;
    filtVars_1[3] = w2_c2_1;
    filtVars_1[4] = w1_c3_1;
    filtVars_1[5] = w2_c3_1;
    
    filtVars_2[0] = w1_c1_2;
    filtVars_2[1] = w2_c1_2;
    filtVars_2[2] = w1_c2_2;
    filtVars_2[3] = w2_c2_2;
    filtVars_2[4] = w1_c3_2;
    filtVars_2[5] = w2_c3_2;
/* ======================================================================== */
/*S Place source code here.                                                S*/
/* ======================================================================== */
    return(FIL_SUCCESS);
}

/* ======================================================================== */
/*  End of file: Filter_iirT2Ch2_c3_u_o.c                                          */
/* ------------------------------------------------------------------------ */
/*          Copyright (C) 2004 Texas Instruments, Incorporated.             */
/*                          All Rights Reserved.                            */
/* ======================================================================== */
