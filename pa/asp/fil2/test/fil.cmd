-heap  0x20000
-stack 0x20000


MEMORY
{
    IRAM0:      o = 80000000h   l = 00040000h
    CE0:        o = 80040000h   l = 00400000h
    FIL_MEM:    o = 80500000h   l = 00200000h
}


SECTIONS
{
    .text       >       CE0 
    .fil_text   >       FIL_MEM 
    .fil_lib    >       FIL_MEM    
    .stack      >       CE0
    .bss        >       IRAM0
    .cinit      >       CE0      
    .const      >       CE0
    .fil_data   >       FIL_MEM     
    .data       >       CE0      
    .far        >       IRAM0
    .switch     >       CE0      
    .sysmem     >       CE0     
    .tables     >       CE0      
    .cio        >       CE0     
}                          
