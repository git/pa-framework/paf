/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  

// std headers
#include <std.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <clk.h>
#include <log.h>
#include  <std.h>
#include <xdas.h>
#include <paftyp.h>

// FIL headers
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>

// Test table structure
typedef struct 
{
    FIL_filterFnPtr filterFn; // Function ptr that is tested
    Uint type;                // type field that will call the function, in FIL
    Int  taps;                // Taps per cascade
    Int  ch;                  // Channels
    Int  uniCoef;             // Unicoef(1) or not(0)
    Int  inplace;             // Inplace(1) or not(0)
    Int  dPrec;               // Input/Output data precision is SP(1) or DP(0)
    Int  cPrec;               // Coefficient data precision is SP(1) or DP(0)
    Int  cascade;             // No of cascades - 1. ie if 4 taps, then this field is 1 (2(cascades) - 1)
} testTable;
                                        
#define FIL_TEST_TOTAL         (384)
#define FIL_TEST_SAMPLE_COUNT  16
#define FIL_TEST_ITERATIONS    10

//(0.000030517578125)      // - (15bit)
//(0.0000152587890625)     // - (16bit)
//(0.00000762939453125)    // - (17bit)
//(0.000003814697265625)   // - (18bit)
//(0.0000019073486328125)  // - (19bit)
//(0.00000095367431640625) // - (20bit)

#define FIL_ERROR_THRESHOLD	 (0.000030517578125)      // - (15bit)

// 8388608 = 2^23
double BitError_array[] = 
{
    2/8388608.00, // 1
    4/8388608.00, // 2
    8/8388608.00, // 3
    16/8388608.00, // 4
    32/8388608.00, // 5
    64/8388608.00, // 6
    128/8388608.00, // 7
    256/8388608.00, // 8
    512/8388608.00, // 9
    1024/8388608.00, // 10
};

double testCase_error[FIL_TEST_TOTAL];

// Constant
PAF_AudioData gInArray[1024];
PAF_AudioData gCoeff[2048];  

// Ref
PAF_AudioData gOutArrayRef[1024];
PAF_AudioData gVarsRef[1024] = {0};

// To be tested
PAF_AudioData gOutArray[1024];
PAF_AudioData gVars[1024] = {0};

// Allocation for the param pointers 
PAF_AudioData *pIn[PAF_MAXNUMCHAN];
PAF_AudioData *pOut[PAF_MAXNUMCHAN];
PAF_AudioData *pVar[PAF_MAXNUMCHAN];
PAF_AudioData *pCoef[PAF_MAXNUMCHAN];

// Param structure
PAF_FilParam gParamTest = {
 (void **)pIn,
 (void **)pOut,
 (void **)pCoef,
 (void **)pVar,
 FIL_TEST_SAMPLE_COUNT,
 0,
 0
};
/*
typedef struct PAF_FilParam {
    Void  ** pIn  ;
    Void  ** pOut ;
    Void  ** pCoef;
    Void  ** pVar ;
    Int      sampleCount;
    Uchar    channels;
    Uchar    use;
}  PAF_FilParam;
*/

extern testTable gTestArray[FIL_TEST_TOTAL];

int generic_IR_filter( PAF_FilParam *, testTable );

void main( void )
{
    int i, j, k, ret;
    int ch, taps, filType, dPrec, cPrec;
    int testCount;
    FIL_filterFnPtr filFn;
    int testIterations = 10;
    int uniCoef, inplace, cascade;
    float error_max;
    
    int start, testIndex;

    for( i = 0; i < 2048; i += 2 )
    {
        gCoeff[i] = ((i+1) & 0xF)*0.0015;
        gCoeff[i+1] = ((i+2) & 0xF)*-0.0015;
    }
    
    for( i = 0; i < FIL_TEST_TOTAL; i++ )
        testCase_error[i] = 0.0;
	        
    // Reset vars and output
    for( i = 0; i < 1024; i++ )
    {
        gOutArrayRef[i] = 0;
        gVarsRef[i] = 0;
        gOutArray[i] = 0;
        gVars[i] = 0;
    }
    
	for( start = 0; start < FIL_TEST_TOTAL; start++ )
	{
	    //for( testIndex = 0; testIndex < 1; testIndex++ )
	    //{
	        testCount = start;
	        
	       /* if( testCount >= FIL_TEST_TOTAL )
	            testCount = 0;*/
	            
	        goto test_filter;  
	          
            testfilterback :	            
	            
	            
	    //}        
	} 
	
	goto fil_end;          
    
    // Start testing filters one by one.
//    for( testCount = FIL_TEST_TOTAL - 1; testCount >= 0; --testCount )
//    {
//        goto test_filter;
        
        
test_filter :
	    // Reset vars
	    for( i = 0; i < 1024; i++ )
	    {
	        gOutArrayRef[i] = 0;
	        gVarsRef[i] = 0;
	        gOutArray[i] = 0;
	        gVars[i] = 0;
	    }
        
        error_max = 0.0;
         
		for( testIterations = 0; testIterations < FIL_TEST_ITERATIONS; testIterations++ )
        {
	        // Get a local copy of the fil
		    ch      = gTestArray[testCount].ch;
		    filType = gTestArray[testCount].type;
		    dPrec   = gTestArray[testCount].dPrec;
		    cPrec   = gTestArray[testCount].cPrec;
		    taps    = gTestArray[testCount].taps;
		    uniCoef = gTestArray[testCount].uniCoef;
		    inplace = gTestArray[testCount].inplace;
            cascade = gTestArray[testCount].cascade + 1;
            
		    // Get input samples & Get coefficients.
		    for( i = 0; i < 1024; i++ )
		    {
		         gInArray[i] = (i & 0xF)*testIterations*0.0001;
		         gOutArray[i] = gInArray[i];
		    }

		    		    
		    // Setup the param structure.
		    for( i = 0; i < ch; i++ )
		    {
		        gParamTest.pIn  [i] = gInArray  + i*FIL_TEST_SAMPLE_COUNT;
		        
		        if( inplace )
		            gParamTest.pIn [i]  = gOutArray + i*FIL_TEST_SAMPLE_COUNT;
		            
		            gParamTest.pOut [i] = gOutArray + i*FIL_TEST_SAMPLE_COUNT;

		        
		        if( uniCoef )
		            gParamTest.pCoef[i] = gCoeff;
		        else
		            gParamTest.pCoef[i] = gCoeff + i*( cascade*taps*2 + 1 );
		            
		        gParamTest.pVar [i] = gVars  + i*2*taps*cascade;
		    }  
		    gParamTest.channels = ch;
		    gParamTest.use      = taps*cascade;
		    //gParamTest.use     |= ( (cascade-1) << 16 );
		          
	        if( gTestArray[testCount].filterFn == 0 )
	            continue;
	            
	        ret = gTestArray[testCount].filterFn( &gParamTest );
	        //ret = generic_IR_filter( &gParamTest, gTestArray[testCount] );
	        
	        if( ret == 0 )
	        {
			    float error;
			    
			    for( i = 0; i < ch; i++ )
			    {
			    
			            gParamTest.pIn [i] = gInArray + i*FIL_TEST_SAMPLE_COUNT;
			            gParamTest.pOut[i] = gOutArrayRef + i*FIL_TEST_SAMPLE_COUNT;
			            
				        gParamTest.pVar[i] = gVarsRef     + i*taps*cascade;
			    } 
			                    
	            gParamTest.use      = taps;
		        gParamTest.use     |= ( (cascade) << 16 );
	            
	            // Do reference filtering.
	            generic_IR_filter( &gParamTest, gTestArray[testCount] );

			    //error_max = 0.0;
			    
			    if((testCount == 7) && (testIterations == 17))
			        error = 0.0;
			    
			    for( i = 0; i < ch*FIL_TEST_SAMPLE_COUNT; i++ )
			    {
			        float error_value;
			        
			        if(gOutArrayRef[i])
			            error_value = fabs( (gOutArrayRef[i] - gOutArray[i])/gOutArrayRef[i] );
			        else
			            error_value = gOutArray[i];
			            
			    if (error_value > FIL_ERROR_THRESHOLD)
			    	printf("Test case [%d], Iteration [%d] - failed(%ef) \n", testCount, testIterations, error_value );
                /* rvanga Commenting these lines print only when there is a failure....*/
			   // else
			    //	printf("Test case [%d], Iteration [%d] - passed \n", testCount, testIterations);
			    
			    /// Vijay commenting these lines
			        /*error = 0.0;
			        
			        if(gOutArrayRef[i])
			            error = (gOutArrayRef[i] - gOutArray[i])/gOutArrayRef[i];*/
			        
			        if(fabs(error_value) > error_max)
			        	error_max = fabs(error_value);
			             
			    }
			    
		        //if( error_max > FIL_MaxError )
		        //{    //while(1);
		            //printf("Test case [%d], Iteration [%d] - failed(%ef) \n", testCount, testIterations, error_max);
		        //}
			    
		    	            
	        }
	        else
	            printf("Error");
	            
	    }// Iterations
	    
        if(testCase_error[testCount] < error_max) 
            testCase_error[testCount] =  error_max;

          for(i = 0; i < 10; i++)
          {
            if(testCase_error[testCount] <= BitError_array[i])
                break;
          }
        
          if((23-i) >= 15)
            printf("Test case [%d], - %d bits match \n", testCount, (23 - i));
          else
            printf("Test case [%d], - %d bits match  (fail)\n", testCount, (23 - i));
 

/*            
		    for( i = 0; i < ch*FIL_TEST_SAMPLE_COUNT; i++ )
		    {
		        if( ( (gOutArrayRef[i] - gOutArray[i]) > 0.00000005 ) ||  (  (gOutArrayRef[i] - gOutArray[i]) < -0.00000005   )   )
		            while(1);
		    }
*/		    
		goto testfilterback;

//    }

    fil_end :

            
}

int iir_generic_cas( PAF_FilParam * );
int iir_generic1( PAF_FilParam * );

int generic_IR_filter( PAF_FilParam * param, testTable testElement)
{
    int i, j, k;
    int ret;

    if( testElement.type )
    ;
    else 
    {
        if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
            return( iir_generic1( param ) );
        else
            return( iir_generic_cas( param ) );
    }
    
}

int iir_generic_cas( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch, casc, c;
    PAF_AudioData accY, accYtemp, accW;
    PAF_AudioData varW, varW_temp,input;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars, * restrict filtVars0;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = (pParam->use) & 0xFFFF;
    casc  = ( (pParam->use) & 0xFFFF0000 ) >> 16;	

    for (ch = 0; ch < pParam->channels; ch++){
    	x = (PAF_AudioData *)pParam->pIn[ch];
		y = (PAF_AudioData *)pParam->pOut[ch];
			   
		for (i = 0; i < count; i++){
			filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
			input 	  =  filtCfsB[0] * x[i];
		    
		    for (c = 0; c < casc; c++){
		    	
		   		filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch] + c*taps*2 + 1;
		    	filtCfsA  = filtCfsB  + taps;
		    	filtVars  = (PAF_AudioData *)pParam->pVar[ch] + c*taps;
		    		
		    	accW = input;	
		    	accY = 0; 
		             
		      	for ( t = 0; t < taps; t++ ){
		      		accW += filtCfsA[t] * filtVars[t];   
		            accY += filtCfsB[t] * filtVars[t];
		        }
		            
		        accY += accW;
		        input = accY;
		        filtVarsTemp0 = filtVars[0];
		             
		        for	( t = 0; t < taps; t++ ){
		            
		            filtVarsTemp1 =  filtVars[t + 1];
		            filtVars[t + 1] = filtVarsTemp0;		                 
		            filtVarsTemp0 =  filtVarsTemp1; 
		        }
		                
		        filtVars[taps] = filtVarsTemp1;          
		        filtVars[0] = accW;
		             
	   		}
		         
			y[i] = input;
		}
		     
	}
	
	 
     return( FIL_SUCCESS );      
}
   
   
   
int iir_generic1( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY, accYtemp, accW;
    PAF_AudioData varW, varW_temp;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars, * restrict filtVars0;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++)
    {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++)   
        {
             accY = 0; 
             accW = *x * filtCfsB[0];
             
             for( t = 0; t < taps; t++ )
             {
                 accW += filtCfsA[t] * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ )
             {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}


            
