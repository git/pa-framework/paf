/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IMDDEFORMAT default instance creation parameters
 */
#include <std.h>
#include <imdDeFormat.h>
#include "paftyp.h"

/*
 *  ======== IMDDEFORMAT_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of MDDEFORMAT objects.
 *
 */
const IMDDEFORMAT_Status IMDDEFORMAT_PARAMS_STATUS = {
    sizeof(IMDDEFORMAT_Status),     /* Size of this structure.  */
    1,                      /* MDDEFORMAT Mode Control Register.  1=enabled. 0=disabled.  */

    0,                      /* Unused.  */
    0,                      /* Unused */
    0,                      /* Unused */

    //  Initialization of run-time controllable parameters
    0,                      /* syncMarkerRxIdx */
    0,                      /* rxPhase */
    0,                      /* phase0Idx */
    XDAS_FALSE,             /* syncd */
    0,                      /* rxFrameCount */
    0                       /* syncCnt */
};

/* MDDEFORMAT default config */
const IMDDEFORMAT_Config IMDDEFORMAT_PARAMS_CONFIG = {
    sizeof(IMDDEFORMAT_Config), /* size */

    //  Initialization "init-time" controllable parameters
    MDDEFORMAT_MAXNUMCHANS, /* max M */
    MDDEFORMAT_MAXNUMSAMPS, /* max N */
    /* memTab config */
    {
        { IALG_SARAM, IALG_PERSIST }
    }
};

const IMDDEFORMAT_Params IMDDEFORMAT_PARAMS = {
    sizeof(IMDDEFORMAT_Params),     /* size */
    &IMDDEFORMAT_PARAMS_STATUS,     /* public pStatus */
    &IMDDEFORMAT_PARAMS_CONFIG,     /* private config */
};

/* MDDEFORMAT config, app-configurable memTab in SDRAM */
const IMDDEFORMAT_Config IMDDEFORMAT_PARAMS_CONFIG_SDRAM = {
    sizeof(IMDDEFORMAT_Config), /* size */

    //  Initialization of "init-time" controllable parameters
    MDDEFORMAT_MAXNUMCHANS, /* max M */
    MDDEFORMAT_MAXNUMSAMPS, /* max N */
    /* memTab config */
    {
        { IALG_EXTERNAL, IALG_PERSIST }
    }
};

const IMDDEFORMAT_Params IMDDEFORMAT_PARAMS_SDRAM = {
    sizeof(IMDDEFORMAT_Params),     /* size */
    &IMDDEFORMAT_PARAMS_STATUS,     /* public pStatus */
    &IMDDEFORMAT_PARAMS_CONFIG_SDRAM,     /* private config */
};
