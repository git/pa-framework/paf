/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Given the well defined nature of the PA/F ASP interface, there is usually
 *  no need to edit or add anything in this header file.
 */

#ifndef MDDEFORMAT_MDS_
#define MDDEFORMAT_MDS_

#include <ialg.h>

#include <imdDeFormat.h>
#include <icom.h>


/*
 *  ======== MDDEFORMAT_MDS_exit ========
 *  Required module finalization function
 */
#define MDDEFORMAT_MDS_exit COM_TII_exit

/*
 *  ======== MDDEFORMAT_MDS_init ========
 *  Required module initialization function
 */
#define MDDEFORMAT_MDS_init COM_TII_init


/*
 *  ======== MDDEFORMAT_MDS_IALG ========
 *  TII's implementation of MDDEFORMAT's IALG interface
 */
extern IALG_Fxns MDDEFORMAT_MDS_IALG;

/*
 *  ======== MDDEFORMAT_MDS_IMDDEFORMAT ========
 *  TII's implementation of MDDEFORMAT's IMDDEFORMAT interface
 */
extern const IMDDEFORMAT_Fxns MDDEFORMAT_MDS_IMDDEFORMAT;


/*
 *  ======== MDDEFORMAT_MDS_Handle ========
 */
typedef struct MDDEFORMAT_MDS_Obj *MDDEFORMAT_MDS_Handle;

/*
 *  ======== MDDEFORMAT_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IMDDEFORMAT.
 */
typedef IMDDEFORMAT_Params MDDEFORMAT_MDS_Params;

/*
 *  ======== MDDEFORMAT_MDS_Status ========
 *  We don't add any new status to the standard one defined by IMDDEFORMAT.
 */
typedef IMDDEFORMAT_Status MDDEFORMAT_MDS_Status;

/*
 *  ======== MDDEFORMAT_MDS_Config ========
 *  We don't add any new config to the standard one defined by IMDDEFORMAT.
 */
typedef IMDDEFORMAT_Config MDDEFORMAT_MDS_Config;

/*
 *  ======== MDDEFORMAT_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define MDDEFORMAT_MDS_PARAMS   IMDDEFORMAT_PARAMS


#endif  /* MDDEFORMAT_MDS_ */
