/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  MDDEFORMAT Module IALG implementation - TII's implementation of the
 *  IALG interface for the MDDEFORMAT algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include <imdDeFormat.h>
#include <mdDeFormat_mds.h>
#include <mdDeFormat_mds_priv.h>

//#define ENABLE_TRACE
#ifdef ENABLE_TRACE
  #include "dp.h" // for debugging.  Should be removed when operating.
  #define TRACE(a) dp a
#else
  #define TRACE(a) 
#endif


/*
 *  ======== MDDEFORMAT_MDS_alloc ========
 */
Int MDDEFORMAT_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IMDDEFORMAT_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IMDDEFORMAT_PARAMS;  /* set default parameters */
    }
    /* Request memory for object */
    memTab[0].size = (sizeof(MDDEFORMAT_MDS_Obj)+7)/8*8 + sizeof(IMDDEFORMAT_Status);
    memTab[0].alignment = 8;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    TRACE((NULL, "%s.%d:  Requesting %d bytes from SARAM, PERSIST.\n", __FUNCTION__, __LINE__, memTab[0].size));

    /* Request memory for persistent buffer */
    memTab[1].size = params->pConfig->maxNumChs * params->pConfig->maxNumSamps * sizeof(PAF_AudioData);
    memTab[1].alignment = 4;
    memTab[1].space = params->pConfig->memRec[0].space;
    memTab[1].attrs = params->pConfig->memRec[0].attrs;

    TRACE((NULL, "%s.%d:  Requesting %d bytes from EXTERNAL, PERSIST.\n", __FUNCTION__, __LINE__, memTab[1].size));

    return MDDEFORMAT_MEMTAB_NO;
}


/*
 *  ======== MDDEFORMAT_MDS_initObj ========
 */
Int MDDEFORMAT_MDS_initObj(IALG_Handle handle,
            const IALG_MemRec memTab[], IALG_Handle ign,
            const IALG_Params *algParams)
{
    MDDEFORMAT_MDS_Obj *mdDeFormat = (Void *)handle;
    const IMDDEFORMAT_Params *params = (Void *)algParams;
    IMDDEFORMAT_Config *pCfg;
    Int16 i;

    if (params == NULL) {
        params = &IMDDEFORMAT_PARAMS;  /* set default parameters */
    }
    // Fill in pointers
    mdDeFormat->pStatus = (MDDEFORMAT_MDS_Status *)((char *)mdDeFormat + (sizeof(MDDEFORMAT_MDS_Obj)+7)/8*8);
    *mdDeFormat->pStatus = *params->pStatus;
    mdDeFormat->pConfig  = (MDDEFORMAT_MDS_Config *)params->pConfig;

    TRACE((NULL, "%s.%d.\n", __FUNCTION__, __LINE__));

    // initial values for private config structure
    pCfg = mdDeFormat->pConfig;

    /* Set up audio buffers */
    for (i = 0; i < pCfg->maxNumChs; i++)
    {
        mdDeFormat->sampleBufP[i] = (PAF_AudioData *)memTab[1].base + i * pCfg->maxNumSamps;
    }

    /* Initialize current bit-stream metadata values */
    mdDeFormat->channelConfigurationStream.full = 0;

    return (IALG_EOK);
}

Int MDDEFORMAT_MDS_numAlloc()
{
    return MDDEFORMAT_MEMTAB_NO;
}
