
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// MIPS Load Demonstration algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the MIPS Load Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the ML
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef ML_
#define ML_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <iml.h>

#include "paftyp.h"

/*
 *  ======== ML_Handle ========
 *  MIPS Load Demonstration algorithm instance handle
 */
typedef struct IML_Obj *ML_Handle;

/*
 *  ======== ML_Params ========
 *  MIPS Load Demonstration algorithm instance creation parameters
 */
typedef struct IML_Params ML_Params;

/*
 *  ======== ML_PARAMS ========
 *  Default instance parameters
 */
#define ML_PARAMS IML_PARAMS

/*
 *  ======== ML_Status ========
 *  Status structure for getting ML instance attributes
 */
typedef volatile struct IML_Status ML_Status;

/*
 *  ======== ML_Cmd ========
 *  This typedef defines the control commands ML objects
 */
typedef IML_Cmd   ML_Cmd;

/*
 * ===== control method commands =====
 */
#define ML_NULL IML_NULL
#define ML_GETSTATUSADDRESS1 IML_GETSTATUSADDRESS1
#define ML_GETSTATUSADDRESS2 IML_GETSTATUSADDRESS2
#define ML_GETSTATUS IML_GETSTATUS
#define ML_SETSTATUS IML_SETSTATUS

/*
 *  ======== ML_create ========
 *  Create an instance of a ML object.
 */
static inline ML_Handle ML_create(const IML_Fxns *fxns, const ML_Params *prms)
{
    return ((ML_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== ML_delete ========
 *  Delete a ML instance object
 */
static inline Void ML_delete(ML_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== ML_apply ========
 */
extern Int ML_apply(ML_Handle, PAF_AudioFrame *);

/*
 *  ======== ML_reset ========
 */
extern Int ML_reset(ML_Handle, PAF_AudioFrame *);

/*
 *  ======== ML_exit ========
 *  Module finalization
 */
extern Void ML_exit(Void);

/*
 *  ======== ML_init ========
 *  Module initialization
 */
extern Void ML_init(Void);

#endif  /* ML_ */
