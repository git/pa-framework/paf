
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
#

#..............................................................................

set -o errexit
# set -o xtrace

#..............................................................................

TOOL="$( basename "${0}" )"

#..............................................................................

TMPFIL="${TEMP}/${TOOL%.sh}.tmp"

for CFIL in ../{src_coef,src_coef_hbw}.c; do

    # locate coefficient arrays
    COEF="$( sed -n -e 's/.*\(cf_.*\)\[\] = {/\1/p' "${CFIL}" )"

    for FIL in ${COEF}; do
        # extract coefficients in array "${FIL}" & remove blank lines & trailing commas
        sed -n -e "/${FIL}.. = {/,/^};/ { /[{}]/! { /^$/ d; s/,//; p } }" "${CFIL}" > "${TMPFIL}"

        ## echo -n "${FIL}"; wc -l <"${TMPFIL}"

        (
            FS="$( echo "${FIL}" | sed -n -e 's/cf_.*to\(.\).*/\1/p' | tr "HQ" "42" )"
            echo '$ SampleRate '"${FS}"

            echo '$ FSum'

            cat "${TMPFIL}"

            if [[ "${FIL}" == "cf_2to4"* \
               || "${FIL}" == "cf_1to2"* ]]; then
                #  odd-length filter -- append all (reversed) coeffs. except 1st
                NT=1
            else
                # even-length filter -- append all (reversed) coeffs.
                NT=0
            fi
            tac "${TMPFIL}" | tail -n +"$(( NT+1 ))"
        ) > "${FIL}".fil
    done

done
