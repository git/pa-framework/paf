
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
#

#..............................................................................

set -o errexit
# set -o xtrace

#..............................................................................

TOOL="$( basename "${0}" )"

#..............................................................................

unset PTERM

while [ ${#} -gt 0 ] ; do
    case "${1}" in

    -h|--help)
        (
        echo "Usage: ${TOOL} [options]"
        echo "Plots SRC4 filters."
        echo "Options:"
        echo -e "    -h,\t--help\t\tShow this usage info"
        echo -e "\t--pterm=TYP\tUse specified plot file type (default is output to screen)"
        echo -e "    -x\t\t\tEnable \"xtrace\""
        ) 1>&2
        exit 0
        ;;

    --pterm=*)
        PTERM="${1#*=}"
        shift
        ;;

    -x)
        set -o xtrace
        shift
        ;;

    -*|*)
        echo 1>&2 "Invalid option/argument: \"${1}\" (try \"${TOOL} --help\")"
        exit 1
        ;;
    esac
done

#..............................................................................

# 1:2, or 1st stage of 1:4	131 ("cf_1to2.fil")	195 ("cf_1to2_hbw.fil")
#         2nd stage of 1:4	 19 ("cf_2to4.fil")	 19 ("cf_2to4_hbw.fil")

# 2:1, or 1st stage of 4:1	128 ("cf_HtoQ.fil")	192 ("cf_HtoQ_hbw.fil")
#         2nd stage of 4:2	 24 ("cf_1toH.fil")	 24 ("cf_1toH_hbw.fil")

#..............................................................................

FIGURES=(
"Lowpass Filter Used for 2:1 Downsampling and 2nd Stage of 4:1 Downsampling"
"Lowpass Filter Used for 1st stage of 4:1 Downsampling"
"Lowpass Filter Used for 1:2 Upsampling and 1st Stage of 1:4 Upsampling"
"Lowpass Filter Used for 2nd Stage of 1:4 Upsampling"
)

I=0
for CF in HtoQ 1toH 1to2 2to4; do
    FIGURE="[Group \"${CF}\"] ${FIGURES[ $((I++)) ]}"
    if [ "${PTERM}" ]; then
        echo 1>&2 "${FIGURE}"
    else
        wpopup.sh "${FIGURE//\"/\\\"}" 3
    fi

    FS="$( echo "${CF}" | sed -n -e 's/.*to\(.\).*/\1/p' | tr "HQ" "42" )"

    filplot.sh $( [ "${PTERM}" ] && echo -o src4_${CF}.ripple.${PTERM} ) \
        -yl -0.02 -yh 0.02 -fh 0.5 -title 'Passband Ripple' \
        cf_${CF}.fil     -id "Standard (STD)" \
        cf_${CF}_hbw.fil -id "High Bandwidth (HBW)"

    FS2="$( echo "printf( '%.10g\n', ${FS}*0.25);" | octave -q )"
    filplot.sh $( [ "${PTERM}" ] && echo -o src4_${CF}.cutoff.${PTERM} ) \
        -yl -3 -fh ${FS2} -title 'Passband Cutoff' \
        cf_${CF}.fil     -id "Standard (STD)" \
        cf_${CF}_hbw.fil -id "High Bandwidth (HBW)"

    if [ "${FS}" = "2" ]; then

    FCP="$( echo "printf( '%.10g\n', ${FS}*0.21);" | octave -q )"
    FCS="$( echo "printf( '%.10g\n', ${FS}*0.26);" | octave -q )"
    filplot.sh $( [ "${PTERM}" ] && echo -o src4_${CF}.transition.${PTERM} ) \
        -yl -160 -fl ${FCP} -fh ${FCS} -title 'Transition' \
        cf_${CF}.fil     -id "Standard (STD)" \
        cf_${CF}_hbw.fil -id "High Bandwidth (HBW)"

    fi

    FD="$( echo "printf( '%.10g\n', ${FS}*0.05);" | octave -q )"
    filplot.sh $( [ "${PTERM}" ] && echo -o src4_${CF}.full.${PTERM} ) \
        -yl -160 -fd ${FD} -title 'Pass+Stopband' \
        cf_${CF}.fil     -id "Standard (STD)" \
        cf_${CF}_hbw.fil -id "High Bandwidth (HBW)"
        FILTERS[${I}]="${GROUP}"
done
