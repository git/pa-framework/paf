
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  SRC Module IALG implementation - TIH's implementation of the
 *  IALG interface for the Synchronous Rate Conversion Algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include "isrc.h"
#include "src_tih.h"
#include "src_tih_priv.h"

/*
 *  ======== SRC_TIH_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== SRC_TIH_alloc ========
 */
Int SRC_TIH_alloc(const IALG_Params *algParams, IALG_Fxns **pf, IALG_MemRec memTab[])
{
 const ISRC_Params *params = (Void *)algParams;
 int	i;

 if (params == NULL) {
	params = &ISRC_PARAMS;  /* set default parameters */
 }

 for(i = 0; i < params->config.nMemRec; i++) {
        memTab[i].size = params->config.pMemRec[i].size;
        memTab[i].space = (IALG_MemSpace)params->config.pMemRec[i].space;
        memTab[i].attrs = (IALG_MemAttrs)params->config.pMemRec[i].attrs;
        memTab[i].alignment = 8;
 }
 return (params->config.nMemRec);
}

/*
 *  ======== SRC_TIH_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== SRC_TIH_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== SRC_TIH_initObj ========
 */
Int SRC_TIH_initObj(IALG_Handle handle, const IALG_MemRec memTab[], IALG_Handle p, const IALG_Params *algParams)
{
    SRC_TIH_Obj *src = (Void *)handle;
    const ISRC_Params *params = (Void *)algParams;

    if (params == NULL) params = &ISRC_PARAMS;  /* set default parameters */

    src->pStatus = (SRC_TIH_Status *)((char *)memTab[0].base + (sizeof(SRC_TIH_Obj) + 3) / 4 * 4
    														 + (sizeof(ISRC_Config) + 3) / 4 * 4);
    *src->pStatus = *params->pStatus;

    src->config = params->config;
    src->config.state = memTab[1].base;
    src->config.tmp = memTab[2].base;

    return (IALG_EOK);
}

/*
 *  ======== SRC_TIH_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== SRC_TIH_moved ========
 */
  /* COM_TIH_moved */
