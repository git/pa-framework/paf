
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the SRC_TIH module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the SRC is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <std.h>
#include <ialg.h>

#include "isrc.h"

#include "src_tih.h"
#include "src_tih_priv.h"

void dnsamp2xSymmetricEven4Msample(float *, float *, float *, int, int);
void upsamp2xSymmetricOdd8Msample(float *, float *, float *, int, int);

#define IALGFXNS \
    &SRC_TIH_IALG,       /* module ID */                         \
    SRC_TIH_activate,    /* activate */                          \
    SRC_TIH_alloc,       /* alloc */                             \
    SRC_TIH_control,     /* control */                           \
    SRC_TIH_deactivate,  /* deactivate */                        \
    SRC_TIH_free,        /* free */                              \
    SRC_TIH_initObj,     /* init */                              \
    SRC_TIH_moved,       /* moved */                             \
    NULL                /* numAlloc (NULL => IALG_MAXMEMRECS) */\

/*
 *  ======== SRC_TIH_ISRC ========
 *  This structure defines TIH's implementation of the ISRC interface
 *  for the SRC_TIH module.
 */
const ISRC_Fxns SRC_TIH_ISRC = {       /* module_vendor_interface */
    IALGFXNS,
    SRC_TIH_reset,
	SRC_TIH_apply,
    SRC_TIH_ratio2,
    SRC_TIH_ratio4,
    SRC_TIH_ratioD,
    SRC_TIH_ratioQ,
    dnsamp2xSymmetricEven4Msample,
    upsamp2xSymmetricOdd8Msample
};

/*
 *  ======== SRC_TIH_IALG ========
 *  This structure defines TIH's implementation of the IALG interface
 *  for the SRC_TIH module.
 */
#ifdef _TMS320C6X

asm("_SRC_TIH_IALG .set _SRC_TIH_ISRC");

#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns SRC_TIH_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif

