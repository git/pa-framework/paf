
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// #define SINGLEONLY /* define for testing to force use of non-dual filters */

/*
 *  SRC Module implementation - TIH implementation of a Synchronous Rate Conversion Algorithm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <isrc.h>
#include <src_tih.h>
#include <src_tih_priv.h>
#include <srcerr.h>

#include "paftyp.h"
#include "cpl.h"

/* For now; to co-exist with CDM (FIXME) */
//#undef CPL_CALL 
//#define CPL_CALL(cplfxn) (src->cplFxns->cplfxn)

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

// -----------------------------------------------------------------------------
#include "logp.h"

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 1 to make it easier to catch any errors.
#define CURRENT_TRACE_MASK  1   // terse only

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // log a half dozen lines per loop
#define TRACE_MASK_VERBOSE  4   // trace full operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// -----------------------------------------------------------------------------

/*
 *  ======== SRC_TIH_apply ========
 *  TIH's implementation of the apply operation.
 */

Int SRC_TIH_apply(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    SRC_TIH_Obj *src = (Void *)handle;
    int rval;
    Uns ratio;

    TRACE_GEN((&TR_MOD, "enter SRC_TIH_apply\n"));
    if (src->pStatus->mode == 0)
    {
        TRACE_GEN((&TR_MOD, "SRC_TIH_apply.%d: Mode = 0, exiting with no action.\n", __LINE__));
        return 0;
    }

    // Detect change in rate request or sample rate
    if ((src->pStatus->rateRequest != src->config.rateRequest) ||
        (pAudioFrame->sampleRate   != src->config.sampleRate))
    {
        TRACE_GEN((&TR_MOD, "SRC_TIH_apply.%d: rateRequest: was 0x%x, now = 0x%x.\n",
            __LINE__, src->config.rateRequest, src->pStatus->rateRequest));
        TRACE_GEN((&TR_MOD, "SRC_TIH_apply.%d: input sample rate: was 0x%x, now = 0x%x.\n",
            __LINE__, pAudioFrame->sampleRate, src->config.sampleRate));
        if (rval = handle->fxns->reset(handle,pAudioFrame))
        {
            TRACE_TERSE((&TR_MOD, "SRC_TIH_apply.%d: rate request change: reset() failed.\n", __LINE__));
            return 0;
        }
    }

    ratio = src->pStatus->rateStream;

  #if ((CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE) != TRACE_MASK_VERBOSE)
    // not verbose, use original compact representation
    if (ratio > 0
        && handle->fxns->ratio[ratio-1]
        && src->pStatus->sampleRate != PAF_SAMPLERATE_NONE
        && ! handle->fxns->ratio[ratio-1] (handle, pAudioFrame, 1))
    {
        pAudioFrame->sampleRate = src->pStatus->sampleRate;
      #ifdef PAF_PROCESS_SRC
        PAF_PROCESS_SET (pAudioFrame->sampleProcess, SRC);
      #endif /* PAF_PROCESS_SRC */
    }
  #else // split it up into pieces to debug
    if (ratio <= 0)
    {
        TRACE_TERSE((&TR_MOD, "SRC_TIH_apply: return as ratio <= 0.\n"));
        return 0;
    }
    if (!handle->fxns->ratio[ratio-1])
    {
        TRACE_TERSE((&TR_MOD, "SRC_TIH_apply: return as no function.\n"));
        return 0;
    }
    if (src->pStatus->sampleRate == PAF_SAMPLERATE_NONE)
    {
        TRACE_VERBOSE((&TR_MOD, "SRC_TIH_apply: return as src->pStatus->sampleRate == PAF_SAMPLERATE_NONE.\n"));
        return 0;
    }
    rval = handle->fxns->ratio[ratio-1] (handle, pAudioFrame, 1);
    if (rval != 0)
    {
        TRACE_TERSE((&TR_MOD, "SRC_TIH_apply: function returns 0x%x.\n", rval));
        return 0;
    }
    TRACE_VERBOSE((&TR_MOD, "SRC_TIH_apply: set srate from %d to %d.\n", 
           pAudioFrame->sampleRate, src->pStatus->sampleRate));
    pAudioFrame->sampleRate = src->pStatus->sampleRate;

   #ifdef PAF_PROCESS_SRC
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, SRC);
   #endif /* PAF_PROCESS_SRC */

  #endif  // end of longer debug version

    return 0;
}

/*
 *  ======== SRC_TIH_reset ========
 *  TIH's implementation of the information operation.
 */

#define RAMAX_2X 0
#define RAMAX_4X 1
#define RAMIN_2X 2
#define RAMIN_4X 3
#define RAMAX_N 4

const SmUns ratioByRate[RAMAX_N][PAF_SAMPLERATE_N] =
{
    {   // Maximum 96 kHz
        /* PAF_SAMPLERATE_UNKNOWN -> */  0,
        /* PAF_SAMPLERATE_NONE -> */     0,
        /* PAF_SAMPLERATE_32000HZ -> */  0,
        /* PAF_SAMPLERATE_44100HZ -> */  0,
        /* PAF_SAMPLERATE_48000HZ -> */  0,
        /* PAF_SAMPLERATE_88200HZ -> */  0,
        /* PAF_SAMPLERATE_96000HZ -> */  0,
        /* PAF_SAMPLERATE_192000HZ -> */ 1,
        /* PAF_SAMPLERATE_64000HZ -> */  0,
        /* PAF_SAMPLERATE_128000HZ -> */ 1,
        /* PAF_SAMPLERATE_176400HZ -> */ 1,
        /* PAF_SAMPLERATE_8000HZ -> */   0,
        /* PAF_SAMPLERATE_11025HZ -> */  0,
        /* PAF_SAMPLERATE_12000HZ -> */  0,
        /* PAF_SAMPLERATE_16000HZ -> */  0,
        /* PAF_SAMPLERATE_22050HZ -> */  0,
        /* PAF_SAMPLERATE_24000HZ -> */  0,
    },
    {   // Maximum 48 kHz
        /* PAF_SAMPLERATE_UNKNOWN -> */  0,
        /* PAF_SAMPLERATE_NONE -> */     0,
        /* PAF_SAMPLERATE_32000HZ -> */  0,
        /* PAF_SAMPLERATE_44100HZ -> */  0,
        /* PAF_SAMPLERATE_48000HZ -> */  0,
        /* PAF_SAMPLERATE_88200HZ -> */  1,
        /* PAF_SAMPLERATE_96000HZ -> */  1,
        /* PAF_SAMPLERATE_192000HZ -> */ 2,
        /* PAF_SAMPLERATE_64000HZ -> */  1,
        /* PAF_SAMPLERATE_128000HZ -> */ 2,
        /* PAF_SAMPLERATE_176400HZ -> */ 2,
        /* PAF_SAMPLERATE_8000HZ -> */   0,
        /* PAF_SAMPLERATE_11025HZ -> */  0,
        /* PAF_SAMPLERATE_12000HZ -> */  0,
        /* PAF_SAMPLERATE_16000HZ -> */  0,
        /* PAF_SAMPLERATE_22050HZ -> */  0,
        /* PAF_SAMPLERATE_24000HZ -> */  0,
    },
    {   // Minimum 64 kHz
        /* PAF_SAMPLERATE_UNKNOWN -> */  0,
        /* PAF_SAMPLERATE_NONE -> */     0,
        /* PAF_SAMPLERATE_32000HZ -> */  3,
        /* PAF_SAMPLERATE_44100HZ -> */  3,
        /* PAF_SAMPLERATE_48000HZ -> */  3,
        /* PAF_SAMPLERATE_88200HZ -> */  0,
        /* PAF_SAMPLERATE_96000HZ -> */  0,
        /* PAF_SAMPLERATE_192000HZ -> */ 0,
        /* PAF_SAMPLERATE_64000HZ -> */  0,
        /* PAF_SAMPLERATE_128000HZ -> */ 0,
        /* PAF_SAMPLERATE_176400HZ -> */ 0,
        /* PAF_SAMPLERATE_8000HZ -> */   4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_11025HZ -> */  4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_12000HZ -> */  4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_16000HZ -> */  4,
        /* PAF_SAMPLERATE_22050HZ -> */  4,
        /* PAF_SAMPLERATE_24000HZ -> */  4,
    },
    {   // Minimum 128 kHz
        /* PAF_SAMPLERATE_UNKNOWN -> */  0,
        /* PAF_SAMPLERATE_NONE -> */     0,
        /* PAF_SAMPLERATE_32000HZ -> */  4,
        /* PAF_SAMPLERATE_44100HZ -> */  4,
        /* PAF_SAMPLERATE_48000HZ -> */  4,
        /* PAF_SAMPLERATE_88200HZ -> */  3,
        /* PAF_SAMPLERATE_96000HZ -> */  3,
        /* PAF_SAMPLERATE_192000HZ -> */ 0,
        /* PAF_SAMPLERATE_64000HZ -> */  3,
        /* PAF_SAMPLERATE_128000HZ -> */ 0,
        /* PAF_SAMPLERATE_176400HZ -> */ 0,
        /* PAF_SAMPLERATE_8000HZ -> */   4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_11025HZ -> */  4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_12000HZ -> */  4, /* yields only minimum 32 kHz */
        /* PAF_SAMPLERATE_16000HZ -> */  4, /* yields only minimum 64 kHz */
        /* PAF_SAMPLERATE_22050HZ -> */  4, /* yields only minimum 64 kHz */
        /* PAF_SAMPLERATE_24000HZ -> */  4, /* yields only minimum 64 kHz */
    },
};

#define RATIO_2_1 0
#define RATIO_4_1 1
#define RATIO_1_2 2
#define RATIO_1_4 3
#define RATIO_N_M 4

const SmUns rateByRatio[RATIO_N_M][PAF_SAMPLERATE_N] =
{
    {   // Ratio 2:1
        /* PAF_SAMPLERATE_UNKNOWN -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_NONE -> */     PAF_SAMPLERATE_NONE,
        /* PAF_SAMPLERATE_32000HZ -> */  PAF_SAMPLERATE_16000HZ,
        /* PAF_SAMPLERATE_44100HZ -> */  PAF_SAMPLERATE_22050HZ,
        /* PAF_SAMPLERATE_48000HZ -> */  PAF_SAMPLERATE_24000HZ,
        /* PAF_SAMPLERATE_88200HZ -> */  PAF_SAMPLERATE_44100HZ,
        /* PAF_SAMPLERATE_96000HZ -> */  PAF_SAMPLERATE_48000HZ,
        /* PAF_SAMPLERATE_192000HZ -> */ PAF_SAMPLERATE_96000HZ,
        /* PAF_SAMPLERATE_64000HZ -> */  PAF_SAMPLERATE_32000HZ,
        /* PAF_SAMPLERATE_128000HZ -> */ PAF_SAMPLERATE_64000HZ,
        /* PAF_SAMPLERATE_176400HZ -> */ PAF_SAMPLERATE_88200HZ,
        /* PAF_SAMPLERATE_8000HZ -> */   PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_11025HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_12000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_16000HZ -> */  PAF_SAMPLERATE_8000HZ,
        /* PAF_SAMPLERATE_22050HZ -> */  PAF_SAMPLERATE_11025HZ,
        /* PAF_SAMPLERATE_24000HZ -> */  PAF_SAMPLERATE_12000HZ,
    },
    {   // Ratio 4:1
        /* PAF_SAMPLERATE_UNKNOWN -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_NONE -> */     PAF_SAMPLERATE_NONE,
        /* PAF_SAMPLERATE_32000HZ -> */  PAF_SAMPLERATE_8000HZ,
        /* PAF_SAMPLERATE_44100HZ -> */  PAF_SAMPLERATE_11025HZ,
        /* PAF_SAMPLERATE_48000HZ -> */  PAF_SAMPLERATE_12000HZ,
        /* PAF_SAMPLERATE_88200HZ -> */  PAF_SAMPLERATE_22050HZ,
        /* PAF_SAMPLERATE_96000HZ -> */  PAF_SAMPLERATE_24000HZ,
        /* PAF_SAMPLERATE_192000HZ -> */ PAF_SAMPLERATE_48000HZ,
        /* PAF_SAMPLERATE_64000HZ -> */  PAF_SAMPLERATE_16000HZ,
        /* PAF_SAMPLERATE_128000HZ -> */ PAF_SAMPLERATE_32000HZ,
        /* PAF_SAMPLERATE_176400HZ -> */ PAF_SAMPLERATE_44100HZ,
        /* PAF_SAMPLERATE_8000HZ -> */   PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_11025HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_12000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_16000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_22050HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_24000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
    },
    {   // Ratio 1:2
        /* PAF_SAMPLERATE_UNKNOWN -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_NONE -> */     PAF_SAMPLERATE_NONE,
        /* PAF_SAMPLERATE_32000HZ -> */  PAF_SAMPLERATE_64000HZ,
        /* PAF_SAMPLERATE_44100HZ -> */  PAF_SAMPLERATE_88200HZ,
        /* PAF_SAMPLERATE_48000HZ -> */  PAF_SAMPLERATE_96000HZ,
        /* PAF_SAMPLERATE_88200HZ -> */  PAF_SAMPLERATE_176400HZ,
        /* PAF_SAMPLERATE_96000HZ -> */  PAF_SAMPLERATE_192000HZ,
        /* PAF_SAMPLERATE_192000HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_64000HZ -> */  PAF_SAMPLERATE_128000HZ,
        /* PAF_SAMPLERATE_128000HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_176400HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_8000HZ -> */   PAF_SAMPLERATE_16000HZ,
        /* PAF_SAMPLERATE_11025HZ -> */  PAF_SAMPLERATE_22050HZ,
        /* PAF_SAMPLERATE_12000HZ -> */  PAF_SAMPLERATE_24000HZ,
        /* PAF_SAMPLERATE_16000HZ -> */  PAF_SAMPLERATE_32000HZ,
        /* PAF_SAMPLERATE_22050HZ -> */  PAF_SAMPLERATE_44100HZ,
        /* PAF_SAMPLERATE_24000HZ -> */  PAF_SAMPLERATE_48000HZ,
    },
    {   // Ratio 1:4
        /* PAF_SAMPLERATE_UNKNOWN -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_NONE -> */     PAF_SAMPLERATE_NONE,
        /* PAF_SAMPLERATE_32000HZ -> */  PAF_SAMPLERATE_128000HZ,
        /* PAF_SAMPLERATE_44100HZ -> */  PAF_SAMPLERATE_176400HZ,
        /* PAF_SAMPLERATE_48000HZ -> */  PAF_SAMPLERATE_192000HZ,
        /* PAF_SAMPLERATE_88200HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_96000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_192000HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_64000HZ -> */  PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_128000HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_176400HZ -> */ PAF_SAMPLERATE_UNKNOWN,
        /* PAF_SAMPLERATE_8000HZ -> */   PAF_SAMPLERATE_32000HZ,
        /* PAF_SAMPLERATE_11025HZ -> */  PAF_SAMPLERATE_44100HZ,
        /* PAF_SAMPLERATE_12000HZ -> */  PAF_SAMPLERATE_48000HZ,
        /* PAF_SAMPLERATE_16000HZ -> */  PAF_SAMPLERATE_64000HZ,
        /* PAF_SAMPLERATE_22050HZ -> */  PAF_SAMPLERATE_88200HZ,
        /* PAF_SAMPLERATE_24000HZ -> */  PAF_SAMPLERATE_96000HZ,
    },
};

Int SRC_TIH_reset(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Uns ratio;
    Uns rate = pAudioFrame->sampleRate;
    Uns rate0;

    TRACE_GEN((&TR_MOD, "enter SRC_TIH_reset\n"));

    if (src->pStatus->mode == 0)
    {
        TRACE_GEN((&TR_MOD, "SRC_TIH_reset.%d: Mode = 0, exiting with no action.\n", __LINE__));
        return 0;
    }

    // save for change detection during apply
    src->config.rateRequest = src->pStatus->rateRequest;

    if (src->pStatus->rateRequest >= 0) 
    {
       ratio = src->pStatus->rateRequest;
       TRACE_VERBOSE((&TR_MOD, "SRC_TIH_reset.%d: ratio is %d.\n", __LINE__, ratio));
    }
    else 
    {
        rate0 = rate >= lengthof (ratioByRate[0]) ? 0 : rate;
        switch ((XDAS_UInt8 )src->pStatus->rateRequest) {
          case 0x80:
            ratio = 0;
            break;
          case 0x81:
            ratio = ratioByRate[RAMAX_2X][rate0];
            break;
          case 0x82:
            ratio = ratioByRate[RAMAX_4X][rate0];
            break;
          case 0x83:
            ratio = ratioByRate[RAMIN_2X][rate0];
            break;
          case 0x84:
            ratio = ratioByRate[RAMIN_4X][rate0];
            break;
          default:
            ratio = 0;
            break;
        }
        TRACE_VERBOSE((&TR_MOD, "SRC_TIH_reset.%d: ratio is %d.\n", __LINE__, ratio));
    }

    rate0 = rate >= lengthof (rateByRatio[0]) ? 0 : rate;
    switch (ratio) 
    {
      case 1:
        rate = rateByRatio[RATIO_2_1][rate0];
        break;
      case 2:
        rate = rateByRatio[RATIO_4_1][rate0];
        break;
      case 3:
        if (2 * pAudioFrame->sampleCount > pAudioFrame->data.nSamples)
            ratio = 0;
        else
            rate = rateByRatio[RATIO_1_2][rate0];
        break;
      case 4:
        if (4 * pAudioFrame->sampleCount > pAudioFrame->data.nSamples)
            ratio = 0;
        else
            rate = rateByRatio[RATIO_1_4][rate0];
        break;
    }
    TRACE_VERBOSE((&TR_MOD, "SRC_TIH_reset.%d: ratio is %d.\n", __LINE__, ratio));

    if (ratio > lengthof (handle->fxns->ratio))
    {
        TRACE_TERSE((&TR_MOD,
            "SRC_TIH_reset.%d: ratio function value exceeds lengthof; setting ratio = 0.\n", __LINE__));
        ratio = 0;
    }
    src->pStatus->rateStream = ratio;

    // save for change detection during apply
    src->config.sampleRate  = pAudioFrame->sampleRate;

    if (ratio > 0
        && handle->fxns->ratio[ratio-1]
        && ! handle->fxns->ratio[ratio-1] (handle, pAudioFrame, 0)) {
        pAudioFrame->sampleRate = src->pStatus->sampleRate = rate;
        TRACE_VERBOSE((&TR_MOD, "SRC_TIH_reset.%d: src->pStatus->sampleRate is %d.\n", __LINE__, rate));
    }
    else {
        src->pStatus->sampleRate = PAF_SAMPLERATE_NONE;
        TRACE_VERBOSE((&TR_MOD, "SRC_TIH_reset.%d: src->pStatus->sampleRate is NONE.\n", __LINE__));
    }
    
#ifdef PAF_PROCESS_SRC
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, SRC);
#endif /* PAF_PROCESS_SRC */

    return 0;
}

Int nbits (Int x)
{
    Int i, n;

    for (i=0, n=0; i < 8 * sizeof (x); i++) {
        if (x & 1)
            n++;
        x >>= 1;
    }

    return n;
}

/*
 *  ======== SRC_TIH_ratio2 ========
 *  TIH's implementation of the ratio2 operation.
 *
 *  Downsample by 2. Requires matching filter coefficients.
 */

Int SRC_TIH_ratio2(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount >> 1;
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);
    Int order = src->pStatus->dnHtoQ->order;
	PAF_AudioData *cf = src->pStatus->dnHtoQ->cf;
	
    if (apply) {
    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->dnHtoQ)) return 1;

    pAudioFrame->sampleCount = kMax;

    if (! apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * order);
        src->config.mask = mask;
    }

    if (apply) {
        Int i, iS;
        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
            PAF_AudioData *x = src->config.tmp;
            PAF_AudioData *y = pAudioFrame->data.sample[i];
            PAF_AudioData *s = &src->config.state[order * iS];
            
            if (!(mask & (1 << i))) continue;
            ++iS;
            
			CPL_CALL(vecScale)(1.0f, s, x, order);
			CPL_CALL(vecScale)(1.0f, y, x + order, kMax * 2);
            handle->fxns->dsamp2x(x, cf, y, kMax, order);
			CPL_CALL(vecScale)(1.0f, x + kMax * 2, s, order);
        }
    }
    return 0;
}

/*
 *  ======== SRC_TIH_ratio4 ========
 *  TIH's implementation of the ratio4 operation.
 *
 *  Downsample by 4. Requires matching filter coefficients.
 */

Int SRC_TIH_ratio4(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int		iMax = src->config.nChannels;
    Int		kMax = pAudioFrame->sampleCount >> 2;
    Int		mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);
    Int		order1 = src->pStatus->dn1toH->order;
    Int		order2 = src->pStatus->dnHtoQ->order;
    float	*cf1 = src->pStatus->dn1toH->cf;
	float	*cf2 = src->pStatus->dnHtoQ->cf;
	
	if (apply) {
    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->dnHtoQ) || !(src->pStatus->dn1toH) ) return 1;

    pAudioFrame->sampleCount = kMax;

    if (! apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * (order1 + order2));
        src->config.mask = mask;
    }

    if (apply) {
        Int i, iS;
        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
            PAF_AudioData *x1 = src->config.tmp;
            PAF_AudioData *x2 = src->config.tmp + order1 + kMax * 4;
            PAF_AudioData *y = pAudioFrame->data.sample[i];
            PAF_AudioData *s1 = &src->config.state[order2 * iMax + order1 * iS];
            PAF_AudioData *s2 = &src->config.state[order2 * iS];
            
            if (!(mask & (1 << i))) continue;
            ++iS;

			CPL_CALL(vecScale)(1.0f, s1, x1, order1);
			CPL_CALL(vecScale)(1.0f, y, x1 + order1, kMax * 4);            
            handle->fxns->dsamp2x(x1, cf1, x2 + order2, kMax * 2, order1);
			CPL_CALL(vecScale)(1.0f, x1 + kMax * 4, s1, order1);            

			CPL_CALL(vecScale)(1.0f, s2, x2, order2);			
			handle->fxns->dsamp2x(x2, cf2, y, kMax, order2);
			CPL_CALL(vecScale)(1.0f, x2 + kMax * 2, s2, order2);
        }
    }
    return 0;
}


/*
 *  ======== SRC_TIH_ratioD ========
 *  TIH's implementation of the ratioD operation.
 *
 *  Upsample by 2 (double). Requires matching filter coefficients.
 */
Int SRC_TIH_ratioD(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int ratio = 2;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount; 
	PAF_AudioData *cf1 = src->pStatus->up1to2->cf;
	Int order = src->pStatus->up1to2->order;
    Int jMax = order / 2 + 1;
	
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);

    if (ratio * kMax > pAudioFrame->data.nSamples) return 1;

    if (apply) {
	    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->up1to2)) return 1;

    pAudioFrame->sampleCount = ratio * kMax;

    if (!apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * jMax);
        src->config.mask = mask;
    }

    if (apply) {
        int				i,iS;

        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
		    PAF_AudioData *x = src->config.tmp;
            PAF_AudioData *y = &pAudioFrame->data.sample[i][0];
            PAF_AudioData *s = &src->config.state[jMax * iS];

            if (!(mask & (1 << i))) continue;
            ++iS;
			
			// put together state/input buffer

			CPL_CALL(vecScale)(1.0f, s, x, jMax);
	        CPL_CALL(vecScale)(2.0f, y, x + jMax, kMax);
			handle->fxns->usamp2x(x, cf1, y, kMax * ratio, order);                
     		CPL_CALL(vecScale)(1.0f, x + kMax, s, jMax);		// update state
		}
    }
    return 0;
}

/*
 *  ======== SRC_TIH_ratioQ ========
 *  TIH's implementation of the ratioQ operation.
 *
 *  Upsample by 4 (quadruple). Requires matching filter coefficients.
 */
Int SRC_TIH_ratioQ(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int ratio = 4;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount; 
	PAF_AudioData *cf1 = src->pStatus->up1to2->cf;
	PAF_AudioData *cf2 = src->pStatus->up2to4->cf;
	Int order1 = src->pStatus->up1to2->order;
	Int order2 = src->pStatus->up2to4->order;
    Int jMax1 = order1 / 2 + 1;
    Int jMax2 = order2 / 2 + 1;
    	
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);

    if (ratio * kMax > pAudioFrame->data.nSamples) return 1;
    
	if (apply) {
	    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->up1to2) || !(src->pStatus->up2to4)) return 1;
    pAudioFrame->sampleCount = ratio * kMax;

    if (!apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * (jMax1 + jMax2));
        src->config.mask = mask;
    }

    if (apply) {
        int i,iS;

        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
		    PAF_AudioData *x1 = src->config.tmp;
		    PAF_AudioData *x2 = src->config.tmp + jMax1 + kMax;
            PAF_AudioData *y = &pAudioFrame->data.sample[i][0];
            PAF_AudioData *s1 = &src->config.state[jMax1 * iS];
            PAF_AudioData *s2 = &src->config.state[jMax1 * iMax + jMax2 * iS];

            if (!(mask & (1 << i))) continue;
            ++iS;
			
			// put together state/input buffer
			CPL_CALL(vecScale)(1.0f, s1, x1, jMax1);
	        CPL_CALL(vecScale)(4.0f, y, x1 + jMax1, kMax);
			handle->fxns->usamp2x(x1, cf1, x2 + jMax2, kMax * ratio / 2, order1);
     		CPL_CALL(vecScale)(1.0f, x1 + kMax, s1, jMax1);

			CPL_CALL(vecScale)(1.0f, s2, x2, jMax2);
			handle->fxns->usamp2x(x2, cf2, y, kMax * ratio, order2);
     		CPL_CALL(vecScale)(1.0f, x2 + kMax * ratio / 2, s2, jMax2);
		}
    }
    return 0;
}
