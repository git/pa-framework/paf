
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.  
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Processing Standard Channel Configuration Mask Definitions
//
//
//

#include <paftyp.h>
#include <pafdec.h>
#include <ccm.h>

const PAF_ChannelMask PAF_ASP_stdCCMSat[PAF_CC_SAT_N+7] = {
    PAF_CHANNELMASK, // PAF_CC_SAT_UNKNOWN
    0, // PAF_CC_SAT_NONE
    mCNTR, // PAF_CC_SAT_MONO
    mLEFT|mRGHT, // PAF_CC_SAT_STEREO
    mLEFT|mRGHT|mSURR, // PAF_CC_SAT_PHANTOM1
    mLEFT|mRGHT|mLSUR|mRSUR, // PAF_CC_SAT_PHANTOM2
    mLEFT|mRGHT|mLSUR|mRSUR|mBACK, // PAF_CC_SAT_PHANTOM3
    mLEFT|mRGHT|mLSUR|mRSUR|mLBAK|mRBAK, // PAF_CC_SAT_PHANTOM4
    mLEFT|mRGHT|mCNTR, // PAF_CC_SAT_3STEREO
    mLEFT|mRGHT|mCNTR|mSURR, // PAF_CC_SAT_SURROUND1
    mLEFT|mRGHT|mCNTR|mLSUR|mRSUR, // PAF_CC_SAT_SURROUND2
    mLEFT|mRGHT|mCNTR|mLSUR|mRSUR|mBACK, // PAF_CC_SAT_SURROUND3
    mLEFT|mRGHT|mCNTR|mLSUR|mRSUR|mLBAK|mRBAK, // PAF_CC_SAT_SURROUND4

// The below are for extension.
#ifndef ATMOS
    mLEFT|mRGHT|mLCTR|mRCTR|mLSUR|mRSUR|mLBAK|mRBAK, // 8  main Channels
    0,                                               // 9  main Channels not provided by default
    mLEFT|mRGHT|mLCTR|mRCTR|mLWID|mRWID|mLSUR|mRSUR|mLBAK|mRBAK, // 10 main Channels
    0,
    0,
    0,
    0
#else
    0,                                                                              // 8  main Channels
    mLEFT|mRGHT|mCNTR|mLSUR|mRSUR|mLBAK|mRBAK|mLTMD|mRTMD,                          // 9  main Channels
    0,                                                                              // 10 main Channels
    mLEFT|mRGHT|mCNTR|mLTRR|mRTRR|mLSUR|mRSUR|mLBAK|mRBAK|mLTFT|mRTFT,              // 11 main Channels
    0,                                                                              // 12 main Channels
    mLEFT|mRGHT|mCNTR|mLWID|mRWID|mLTRR|mRTRR|mLSUR|mRSUR|mLBAK|mRBAK|mLTFT|mRTFT,  // 13 main Channels
    mLEFT|mRGHT|mLCTR|mRCTR|mLWID|mRWID|mLTMD|mRTMD|mLSUR|mRSUR|mLBAK|mRBAK|mLTFT|mRTFT,    // 14 main Channels
#endif
};

const PAF_ChannelMask PAF_ASP_stdCCMSub[PAF_CC_SUB_N] = {
    0, // PAF_CC_SUB_ZERO
    mSUBW, // PAF_CC_SUB_ONE
    mLSUB|mRSUB, // PAF_CC_SUB_TWO
};

const PAF_ChannelConfigurationMaskTable PAF_ASP_stdCCMT = {
    { lengthof(PAF_ASP_stdCCMSat), (PAF_ChannelMask *)PAF_ASP_stdCCMSat, },
    { lengthof(PAF_ASP_stdCCMSub), (PAF_ChannelMask *)PAF_ASP_stdCCMSub, },
};

// ............................................................................

const char AFChanPtrMap[PAF_MAXNUMCHAN+1][PAF_MAXNUMCHAN] =
{
   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,
PAF_CNTR,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 1.0 channel
PAF_LEFT,PAF_RGHT,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 2.0 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 3.0 channels
PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    // 4.0 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 5.0 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_SUBW,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    // 6.0 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LSUB,PAF_RSUB,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    // 5.2 channels
PAF_LEFT,PAF_LSUR,PAF_LBAK,PAF_CNTR,PAF_RGHT,PAF_RSUR,PAF_RBAK,PAF_SUBW,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 7.1 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,   -1,      -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   // 7.2 channels

#ifdef PAF_MAXNUMCHAN_HD
#ifdef ATMOS
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LTMD,PAF_RTMD,   -1,      -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   	// 9.1 channels
/*PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LTRR,PAF_RTRR,PAF_LSUR,PAF_RSUR,PAF_SUBW,PAF_LTFT,PAF_RTFT,   -1,      -1,      -1,      -1,      -1,      -1,     // 9.1 channels with LTFT,RTFT and LTRR,RTRR in place of Lrs,Rrs */
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTMD,PAF_RTMD,   -1,      -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   	// 9.2 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LTRR,PAF_RTRR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LTFT,PAF_RTFT,   -1,      -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,		// 11.1 channels
/*PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LTRR,PAF_RTRR,PAF_LSUR,PAF_RSUR,PAF_SUBW,PAF_LTFT,PAF_RTFT,   -1,      -1,      -1,      -1,     // 11.1 channels with LwRw in  place of Lrs,Rrs */
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LTRR,PAF_RTRR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTFT,PAF_RTFT,   -1,      -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    	// 11.2 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LTMD,PAF_RTMD,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LTFT,PAF_RTFT,   -1,      -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   	// 13.1 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LTMD,PAF_RTMD,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTFT,PAF_RTFT,   -1,   -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,   	// 13.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LTMD,PAF_RTMD,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTFT,PAF_RTFT,     -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LTMD,PAF_RTMD,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTFT,PAF_RTFT,  16,   17,      18,      19,      20,      21,      22,      23,      24,      25,      26,      27,      28,      29,      -1,      -1,        	// 13.2 channels
-1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,      -1,      -1,    -1,      -1,      -1,      -1,  -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,      -1,    		 	// 14.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LTMD,PAF_RTMD,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LTFT,PAF_RTFT,  16,   17,      18,      19,      20,      21,      22,      23,      24,      25,      26,      27,      28,      29,      30,      31,        	// 13.2 channels

#else
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHED,PAF_RHED,   -1,      -1,      -1,      -1,      -1,      -1,    // 9.1 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LHED,PAF_RHED,   -1,      -1,      -1,      -1,      -1,    // 9.2 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHED,PAF_RHED,   -1,      -1,      -1,      -1, // 11.1 channels 
/* PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHSI,PAF_RHSI,   -1,      -1,      -1,      -1,    // 11.1 channels with LHSI & RHSI in place of LHED\RHED */
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LHED,PAF_RHED,   -1,      -1,      -1,    // 11.2 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHED,PAF_RHED,   -1,      -1,    // 13.1 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LHED,PAF_RHED,   -1,    // 13.2 channels
PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LHED,PAF_RHED,PAF_CHED, // 14.2 channels
#endif
#else /* PAF_MAXNUMCHAN_HD */

PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,   -1,      -1,      -1,      -1,      -1,      -1,    // 8.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,   -1,      -1,      -1,      -1,      -1,    // 10.1 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,   -1,      -1,      -1,      -1,    // 10.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,   -1,      -1,      -1,    // 12.1 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,   -1,      -1,    // 12.2 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHED,PAF_RHED,   -1,    // 14.1 channels
PAF_LEFT,PAF_RGHT,PAF_LCTR,PAF_RCTR,PAF_LWID,PAF_RWID,PAF_LOVR,PAF_ROVR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_LSUB,PAF_RSUB,PAF_LHED,PAF_RHED, // 14.2 channels

#endif /* PAF_MAXNUMCHAN_HD */

};

// #define TRACE_CHANNEL_MASKS
#ifdef TRACE_CHANNEL_MASKS
   #include "dp.h"
   #define TRACE(a) dp a
#else
  #define TRACE(a)
#endif

// ............................................................................
PAF_ChannelMask_HD 
PAF_ASP_channelMask (PAF_AudioFrame *pAudioFrame, PAF_ChannelConfiguration cc)
{
    int firstIdx, secondIdx;
    unsigned int mask;
    PAF_ChannelConfigurationMaskTable *ccmt = pAudioFrame->pChannelConfigurationMaskTable;

    TRACE((NULL, "0. sat: 0x%x. sub: 0x%x\n", cc.part.sat, cc.part.sub));
    firstIdx =  cc.part.sat >= ccmt->sat.length ? 0 : cc.part.sat;
    secondIdx = cc.part.sub >= ccmt->sub.length ? 0 : cc.part.sub;
    mask = ccmt->sat.pMask[firstIdx] | ccmt->sub.pMask[secondIdx];
    TRACE((NULL, "1. mask = 0x%x = ccmt->sat.pMask[%d][%d]\n", mask, firstIdx, secondIdx));

    mask = mask
        | (cc.part.extMask & PAF_CC_EXTMASK_LwRw   ? (mLWID|mRWID)       : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_LcRc   ? (mLCTR_HD|mRCTR_HD) : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_LhRh   ? (mLHED|mRHED)       : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_Cvh    ? (mCHED)             : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_Ts     ? (mOVER_HD)          : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_LhsRhs ? (mLHSI|mRHSI)       : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_LhrRhr ? (mLHBK|mRHBK)       : 0)
        | (cc.part.extMask & PAF_CC_EXTMASK_Chr    ? (mCHBK)             : 0)

        | (cc.part.extMask2 & PAF_CC_EXTMASK_LscRsc   ? (mLSC|mRSC)      : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_Ls1Rs1   ? (mLS1|mRS1)      : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_Ls2Rs2   ? (mLS2|mRS2)      : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_Lrs1Rrs1 ? (mLRS1|mRRS1)    : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_Lrs2Rrs2 ? (mLRS2|mRRS2)    : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_LcsRcs   ? (mLCS|mRCS)      : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_Cs       ? (mCS)            : 0)
        | (cc.part.extMask2 & PAF_CC_EXTMASK_LsdRsd   ? (mLSD|mRSD)      : 0)

        | (cc.part.extMask3 & PAF_CC_EXTMASK_LfhRfh   ? (mLTFH|mRTFH)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LtfRtf   ? (mLTFT|mRTFT)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LtmRtm   ? (mLTMD|mRTMD)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LtrRtr   ? (mLTRR|mRTRR)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LrhRrh   ? (mLTRH|mRTRH)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LctRct   ? (mLCTR|mRCTR)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_LhbkRhbk ? (mLHBK|mRHBK)    : 0)
        | (cc.part.extMask3 & PAF_CC_EXTMASK_Chbk     ? (mCHBK)          : 0);

    TRACE((NULL, "4. mask = 0x%x. extMask: 0x%x.  extMask2: 0x%x.  extMask3: 0x%x\n",
            mask, cc.part.extMask, cc.part.extMask2, cc.part.extMask3));

    return( mask);
}

// ............................................................................

PAF_ProgramFormat
PAF_ASP_programFormat (PAF_AudioFrame *pAudioFrame, PAF_ChannelConfiguration cc, Int sourceDual)
{
    PAF_ChannelMask cm = pAudioFrame->fxns->channelMask (pAudioFrame, cc);

    PAF_ChannelForm cf = 0;

    PAF_ProgramFormat pf;

    if (cm & mRSUR)
        cm &= ~mLSUR;
    if (cm & mRBAK)
        cm &= ~mLBAK;

    if (cc.legacy == PAF_CC_MONO || cc.legacy == PAF_CC_STEREO_MONO)
        cf |= mMONO;
    else if (cc.legacy == PAF_CC_STEREO_DUAL) {
        if (sourceDual == PAF_SOURCE_DUAL_MONO1) {
            cm = mLEFT;
            cf |= mMONO|mDUAL;
        }
        else if (sourceDual == PAF_SOURCE_DUAL_MONO2) {
            cm = mRGHT;
            cf |= mMONO|mDUAL;
        }
        else if (sourceDual == PAF_SOURCE_DUAL_MONOMIX) {
            cm = mCNTR;
            cf |= mMONO|mDUAL;
        }
        else { /* sourceDual == PAF_SOURCE_DUAL_STEREO */
            cf |= mDUAL;
        }
    }
    else if (cc.legacy == PAF_CC_STEREO_STEREO)
        cf |= mNSSE;
    else if (cc.legacy == PAF_CC_STEREO_LTRT)
        cf |= mYSSE;

    if (cm & mRSUR) {
        if (cc.part.sat == PAF_CC_SAT_SURROUND2) {
            if (cc.part.aux == PAF_CC_AUX_SURROUND2_STEREO
                || cc.part.aux == PAF_CC_AUX_SURROUND2_MONO)
                cf |= mNBSE;
            else if (cc.part.aux == PAF_CC_AUX_SURROUND2_LTRT)
                cf |= mYBSE;
        }
    }

    pf.mask = cm;
    pf.form = cf;

    return pf;
}
