
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Processing Standard Delay Functions
//
//
//

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <paftyp.h>
#include <stdasp.h>

#define SOS .34713 /* speed of sound in m/ms */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

Int 
PAF_ASP_delay(PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, Int apply, Int channel, Int unit)
{
    static const Float sampleRateScale[] = { 
        0.0,                            // unused
        0.0010,                         // milliseconds (Q0)
        0.0005,                         // milliseconds (Q1)
        0.0010 / (SOS * 100.0),         // milliseconds per centimeter
        0.0010 / (SOS * 3.28084),       // milliseconds per foot
        0.0010 / (SOS * 1.093613),      // milliseconds per yard
        0.0010 / (SOS * 1.0),           // milliseconds per meter
        0.0001,                         // milliseconds (tenths)
    };

    PAF_AudioData * restrict data = pAudioFrame->data.sample[channel];

    //
    // Reset/apply according to flag and presence of data storage.
    //
    // Note that the channel configuration is not checked here to determine
    // the validity of the data in the buffer or actual need for apply/reset.
    // It is expected that such a check will occur in the caller to determine
    // the apply flag value.
    //
    // To facilitate this expected usage, the apply flag is checked only for 
    // zero/non-zero value here.
    //

    if (! apply || ! data) {

        // Reset delay:

        state->count = 0;
        state->pntr = state->base;

    }
    else {

        // Apply delay:

        Uns sampleCount = pAudioFrame->sampleCount;
        Uns startCount;

        Uns delay;

        Uns i;

        // Compute delay according to units, and saturate to buffer length.

        if (unit >= lengthof (sampleRateScale))
            unit = 0;

        if (! unit)
            delay = state->delay;
        else
            delay = sampleRateScale[unit] 
                * pAudioFrame->fxns->sampleRateHz 
                    (pAudioFrame, pAudioFrame->sampleRate, PAF_SAMPLERATEHZ_STD)
                * state->delay;

        if (delay >= state->length)
            delay = state->length;

        // Function as per frame/state length relation & delay

// #define VANILLA
#ifndef VANILLA
        if (sampleCount <= state->length)
#endif /* VANILLA */
        {
            if (delay == 0) {

                // Zero delay -- save to buffer & count, only.

#ifdef     VANILLA
                /* Vanilla code: 14 cycles per sample. */
                for (i=0; i < sampleCount; i++) {
                    *state->pntr++ = *data++;
                    if (state->pntr == state->base + state->length)
                        state->pntr = state->base;
                }
#else     /* VANILLA */
                /* Chocolate code: 1 cycle per sample (likes -mh24). */
                {
                    PAF_AudioData * restrict pntr = state->pntr;
                    Uns spaceCount = state->base + state->length - state->pntr;
                    i = 0;
                    if (spaceCount <= sampleCount) {
                       for (; i < spaceCount; i++)
                           *pntr++ = *data++;
                       if (i == spaceCount)
                           pntr = state->base;
                    }
                    for (; i < sampleCount; i++)
                        *pntr++ = *data++;
                    state->pntr = pntr;
                }
#endif     /* VANILLA */
                if (state->count < state->length)
                    state->count += sampleCount;

            }
            else {

                // Non-zero delay -- implement in zero & non-zero result phases.

                if (state->count < delay) {
                    if ((startCount = delay - state->count) > sampleCount)
                        startCount = sampleCount;
                    state->count += sampleCount;
                }
                else {
                    startCount = 0;
                    if (state->count < state->length)
                        state->count += sampleCount;
                }

#ifdef     VANILLA
                /* Vanilla code: 14 cycles per sample. */
                for (i=0; i < startCount; i++) {
                    *state->pntr++ = *data;
                    *data++ = 0;
                    if (state->pntr == state->base + state->length)
                        state->pntr = state->base;
                }
#else     /* VANILLA */
                /* Chocolate code: 2 cycles per sample (likes -mh8). */
                {
                    PAF_AudioData * restrict pntr = state->pntr;
                    Int spaceCount = state->base + state->length - state->pntr;
                    i = 0;
                    if (spaceCount <= startCount) {
                        for (; i < spaceCount; i++) {
                            *pntr++ = *data;
                            *data++ = 0;
                        }
                        if (i == spaceCount)
                            pntr = state->base;
                    }
                    for (; i < startCount; i++) {
                        *pntr++ = *data;
                        *data++ = 0;
                    }
                    state->pntr = pntr;
                }
#endif     /* VANILLA */

                {
                    PAF_AudioData *pntr = state->pntr;
                    PAF_AudioData *next = pntr - delay;
                    PAF_AudioData x;

                    if (next < state->base)
                        next += state->length;

                    /* Vanilla code: 3 cycles per sample (likes -mh8). */
                    for (; i < sampleCount; i++) {
                        x = *next++;
                        *pntr++ = *data;
                        *data++ = x;
                        if (next == state->base + state->length)
                            next = state->base;
                        if (pntr == state->base + state->length)
                            pntr = state->base;
                    }
                    state->pntr = pntr;
                }
            }
        }
#ifndef VANILLA
        else /* if (sampleCount > state->length) */ {

            if (delay == 0) {

                // Zero delay -- save data tail to buffer, only.

                PAF_AudioData *save = state->base;

                /* Chocolate code: 1 cycle per state (likes -mh24). */
                for (i=0; i < state->length; i++)
                    save[i] = data[i+sampleCount-state->length];

                state->pntr = state->base;
            }
            else {

                // Non-zero delay -- implement in zero & non-zero state phases.

                PAF_AudioData *pntr;
                PAF_AudioData *next;
                PAF_AudioData x;

                /* Chocolate code: 3 cycles per sample (likes -mh8). */

                if (state->count == 0) {
                    // First pass: initialize pointers & load from zero
                    pntr = state->base;
                    next = state->base + state->length - delay;
                    for (i=0; i < state->length; i++) {
                        *next++;
                        *pntr++ = *data;
                        *data++ = 0;
                        if (next == state->base + state->length)
                            next = state->base;
                        if (pntr == state->base + state->length)
                            pntr = state->base;
                    }
                }
                else {
                    // Subsequent pass: set pointers & load from state
                    pntr = state->pntr;
                    next = pntr - delay;
                    if (next < state->base)
                        next += state->length;
                    i=0;
                }

                for (; i < sampleCount; i++) {
                    x = *next++;
                    *pntr++ = *data;
                    *data++ = x;
                    if (next == state->base + state->length)
                        next = state->base;
                    if (pntr == state->base + state->length)
                        pntr = state->base;
                }
                state->pntr = pntr;
            }

            state->count = state->length;
        }
#endif /* VANILLA */

    }

    return 0;
}
