
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Processing Standard Sample Rate Functions
//
//
//

#include <std.h>
#include  <std.h>
#include <xdas.h>
#include <float.h>

#include <stdasp.h>

#if PAF_SAMPLERATEHZ_N != 2
#error internal error
#endif

#ifdef PAF_SAMPLERATEHZ_FLOAT

#define PAF_SAMPLERATEHZ_NOTRECOGNIZED -1.

#define PAF_SAMPLERATEHZ_RECORD(X) { \
    /* PAF_SAMPLERATEHZ_STD */ \
    (X), \
    /* PAF_SAMPLERATEHZ_INV */ \
    (X) < 0. \
    ? (X) > -FLT_MIN ? -FLT_MAX : 1. / (X)  \
    : (X) <  FLT_MIN ?  FLT_MAX : 1. / (X), \
}

#endif /* PAF_SAMPLERATEHZ_FLOAT */

#ifdef PAF_SAMPLERATEHZ_INT32

#define PAF_SAMPLERATEHZ_NOTRECOGNIZED -1

#define PAF_SAMPLERATEHZ_RECORD(X) \
    { (LgInt )(X + .5), (LgInt )(2147483648. / (X) + .5), }
    /* <Q0>PAF_SAMPLERATEHZ_STD, <Q31>PAF_SAMPLERATEHZ_INV */

#endif /* PAF_SAMPLERATEHZ_INT */

const PAF_SampleRateHz 
PAF_ASP_sampleRateHzTable[PAF_SAMPLERATE_N][PAF_SAMPLERATEHZ_N] =
{
    PAF_SAMPLERATEHZ_RECORD (48000.),
    PAF_SAMPLERATEHZ_RECORD (0.),
    PAF_SAMPLERATEHZ_RECORD (32000.),
    PAF_SAMPLERATEHZ_RECORD (44100.),
    PAF_SAMPLERATEHZ_RECORD (48000.),
    PAF_SAMPLERATEHZ_RECORD (88200.),
    PAF_SAMPLERATEHZ_RECORD (96000.),
    PAF_SAMPLERATEHZ_RECORD (192000.),
    PAF_SAMPLERATEHZ_RECORD (64000.),
    PAF_SAMPLERATEHZ_RECORD (128000.),
    PAF_SAMPLERATEHZ_RECORD (176400.),
    PAF_SAMPLERATEHZ_RECORD (32000./4),
    PAF_SAMPLERATEHZ_RECORD (44100./4),
    PAF_SAMPLERATEHZ_RECORD (48000./4),
    PAF_SAMPLERATEHZ_RECORD (32000./2),
    PAF_SAMPLERATEHZ_RECORD (44100./2),
    PAF_SAMPLERATEHZ_RECORD (48000./2),
};

PAF_SampleRateHz
PAF_ASP_sampleRateHz (PAF_AudioFrame *pAudioFrame, Int n, Int m)
{
    return (LgUns )n < PAF_SAMPLERATE_N && (LgUns )m < PAF_SAMPLERATEHZ_N 
        ? PAF_ASP_sampleRateHzTable[n][m]
        : PAF_SAMPLERATEHZ_NOTRECOGNIZED;
}
