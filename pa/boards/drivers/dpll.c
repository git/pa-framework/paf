
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Digital phase-locked loop
//
// All rights reserved.
//
//

/* .......................................................................... */

/*
 * Below is implemented a 2nd-order "digital phase-locked loop" (DPLL),
 * based on Figs. 6 and 7(b) of "Frequency Offset Tracking in MCMA Systems"
 * ( http://bwrc.eecs.berkeley.edu/Classes/EE225C/Papers/christian-pll-report.pdf )
 *
 * Also see:
 * "All digital phase-locked loop: concepts, design and applications."
 * ( http://bwrc.eecs.berkeley.edu/Classes/EE225C/Papers/dpll_ieee.pdf )
 *
 * Also, tuning of FN & ETA may be useful.
 * Need to revise to account for modulo timer operation.
 */

/* .......................................................................... */

#include "dpll.h"

#include <math.h>    /* ldexp(), fmod() */

#define BITSTIM 32    /* no. of bits in Timer counter */
/* use 24 to "exercise" handling of timer wraparound */

/* .......................................................................... */

#define square( x) ((x) * (x))

#define PI 3.14159265358979323846

#define ETA 0.5    /* damping factor */

#ifndef WN
#define WN (2*PI * 0.005)
#endif

/* cf. Eq. 27 for stability criterion: 2*ETA > WN
   -- readily met using current settings */

#define C1 (2*ETA * WN)    /* Eq. 19 */
#define C2  square( WN)    /* Eq. 19 */

// -----------------------------------------------------------------------------
// * also, note that "seed" frequency is passed in at 2x, since I2S *

Int DPLL_Init(DPLL2 *px, double seed, double fcpu)
{
    px->c1 = C1;
    px->c2 = C2;

    px->w = ldexp(fcpu / 4, -BITSTIM) / seed;
    px->v = 0.;
    px->e = 0.;
    px->dv = 0.;

    px->nLast = 0;

    return 0;
} //DPLL_Init

// -----------------------------------------------------------------------------

Int DPLL_Update(DPLL2 *px, double m, int n)
{
    if (px->nLast)  // if previous iteration skipped update due to extremely small n
        n += px->nLast;
    if (n < 0xc00)    // if n (still) extremely small; reference: output frame delta = 0x200, *6
        px->nLast = n;  // save for next iteration then exit
    else
    {
        px->nLast = 0;  // clear to indicate update performed

        px->dv = px->c1 * px->e + px->w;
        /* time ("phase") increment (per sample) */

        px->w += px->c2 * px->e;
        /* update estimate of time ("phase") increment */
        px->v += n * px->dv;    /* update estimate of time ("phase")
                               -- VCO output is a "ramp" */
        if (px->v >= 1.) 
            px->v -= 1.;    /* modulo-wrap VCO output to range [0,1), like timer */

        px->e = ldexp(m, -BITSTIM) - px->v;    /* "phase" (really time) error */
        if (px->e < -0.5) 
            px->e += 1.;    /* assure e in range [-0.5,... */
        else if (px->e >= 0.5) 
            px->e -= 1.;    /* assure e in range [-0.5,0.5) */
        px->e /= n;    /* pro-rate block error to per-sample error */
    }
    return 0;
} //DPLL_Update

// -----------------------------------------------------------------------------
