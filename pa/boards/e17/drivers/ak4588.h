
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// AK4588 Utility Declarations
//
//
//

#ifndef AK4588_H
#define AK4588_H

#include <std.h>

#include <pafsio.h>

// 4-wire serial control timing (page 21 of AK4588 datasheet rev0.9)
//     . all units in nS unless otherwise noted.
#define AK4588_tCCK    200
#define AK4588_tCCKL    80
#define AK4588_tCCKH    80
#define AK4588_tCDS     50
#define AK4588_tCDH     50
#define AK4588_tCSW    150
#define AK4588_tCSS     50
#define AK4588_tCSH     50
#define AK4588_tDCD     45
#define AK4588_tCCZ     70


#define AK4588_MODE_I2C0 0
#define AK4588_MODE_I2C1 1
#define AK4588_MODE_SPI0 2
#define AK4588_MODE_SPI1 3

typedef struct AK4588_Attrs
{
    Int		controlMode;		// SPI or I2C
    Int     segid;              // memory segment
    Int     anaAddr;
    Int     digAddr;
} AK4588_Attrs;

typedef struct AK4588_Obj
{
    Int controlMode;
    Int anaAddr;
    Int digAddr;
    volatile Int *hPort;
} AK4588_Obj, *AK4588_Handle;


extern AK4588_Handle AK4588_create (AK4588_Attrs *pAttrs);
extern Int AK4588_writeDig (AK4588_Handle handle, int reg, int data);
extern Int AK4588_writeAna (AK4588_Handle handle, int reg, int data);
extern Int AK4588_readDig (AK4588_Handle handle, int reg, int *pData);
extern Int AK4588_readDIRStatus (AK4588_Handle handle, PAF_SIO_InputStatus *pStatus);

// SPI defines (here for now)
//#define _SPI_BASE_PORT0      0x47000000u
//#define _SPI_BASE_PORT1      0x48000000u
//maya chnage for primus
#define _SPI_BASE_PORT0      0x01C41000u
#define _SPI_BASE_PORT1      0x01E12000u

#define SPIGCR0       0x0000
#define SPIGCR1       0x0001
#define SPIFLG        0x0004
#define SPIPC0        0x0005
#define SPIDAT0       0x000E
#define SPIDAT1       0x000F
#define SPIBUF        0x0010
#define SPIFMT0       0x0014

#endif //AK4588_H
