
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// I/O device configuration data structure definitions E17 (DA800 EVM).
//
//
//

// -----------------------------------------------------------------------------
// This file contains the necessary configurations and functions for
// using the AC7xx card in the PA environment. In particular, the
// DAP configurations are referenced in the pa17[idyz]-evmda7xx-io.c files
// for use in IOS (Input/Output Switching) shortcuts. Each configuration
// contains settings appropriate to the various devices on the AC7xx;
// the DIR, DACs, ADCs, and DIT output. Also each configuration points to
// a common control function (E17_dapControl), which handles the various
// requests made by the PA framework.

// A note about clocking. There are two different master clocks
// available corresponding to the two primary input choices, DIR and ADC.
// They are the same except for 32kHz
// DIR:
//       . 512fs @ <= 48kHz
//       . 256fs @ > 48kHz & <=96 kHz
//       . 128fs @ > 96kHz
// ADC:
//       . 768fs @ 32kHz
//       . 512fs @ 48kHz
//       . 256fs @ 96kHz
//
// This faciliates the logic used for the McASP transmit sections TX0 (DAC) and
// TX2 (DIT) which divide the master clock down to generate bit and frame clocks.

// -----------------------------------------------------------------------------
// Includes

#include <std.h>
#include <clk.h>
#include <sys.h>

#include <dap.h>
#include <pafsio.h>
#include <paftyp_a.h>

#include <dap_e17.h>
#include <ak4588.h>

#include <da830lib.h>

//#define BOARD_EXT_CLOCKS
// -----------------------------------------------------------------------------
// Local function declarations

XDAS_Int32 E17_dapControl (DEV_Handle device, const PAF_SIO_Params *pParams, XDAS_Int32 code, XDAS_Int32 arg);
static inline XDAS_Int32 initE17 (DEV_Handle device) ;
static XDAS_Int32 clockMuxTx1 (int sel, int force);
static int manageInput  (DEV_Handle device, const DAP_E17_Rx_Params *pParams, PAF_SIO_InputStatus *pStatusOut);
static int manageOutput (DEV_Handle device, const DAP_E17_Tx_Params *pParams, float rateX);
static int FireWorks_read1394Status (PAF_SIO_InputStatus *pStatus);

void HSR4_readStatus (PAF_SIO_InputStatus *pStatus);
void HDMIGpioInit (void);
unsigned int HDMIGpioGetState (void);
// -----------------------------------------------------------------------------
// State machine variables and defines

// AK4588 register file addresses (same for both I2C and SPI)
#define E17_AK4588_DIG_ADDR  0  // fixed by the device
#define E17_AK4588_ANA_ADDR  1  // via hardwired CAD1/0 pins0

// flag to facilitate one time initialization of AC7xx hardware
// 0 ==> not initialized, 1 ==> initialized
static char initDone = 0;

// input status
static PAF_SIO_InputStatus primaryStatus =
{
    0,                                 // lock
    PAF_IEC_AUDIOMODE_UNKNOWN,         // nonaudio
    PAF_IEC_PREEMPHASIS_UNKNOWN,       // emphasis
    PAF_SAMPLERATE_UNKNOWN,            // sampleRateMeasured
    PAF_SAMPLERATE_UNKNOWN,            // sampleRateData
    0,0,0,                             // unused
};

// local handle for accessing the AK4588 CODEC
static AK4588_Handle hAK4588 = NULL;

// The MCLK ratio is dependent on the sample rate but the same
// for both DIR and ADC input. This table is used for setting
// the appropriate register (OCKS) in the AK4588.
static const unsigned char dirOCKS[PAF_SAMPLERATE_N] =
{
    0x2,  //PAF_SAMPLERATE_UNKNOWN
    0x2,  //PAF_SAMPLERATE_NONE
    0x2,  //PAF_SAMPLERATE_32000HZ
    0x2,  //PAF_SAMPLERATE_44100HZ
    0x2,  //PAF_SAMPLERATE_48000HZ
    0x0,  //PAF_SAMPLERATE_88200HZ
    0x0,  //PAF_SAMPLERATE_96000HZ
    0x3,  //PAF_SAMPLERATE_192000HZ
    0x0,  //PAF_SAMPLERATE_64000HZ
    0x3,  //PAF_SAMPLERATE_128000HZ
    0x3,  //PAF_SAMPLERATE_176400HZ
    0x2,  //PAF_SAMPLERATE_8000HZ
    0x2,  //PAF_SAMPLERATE_11025HZ
    0x2,  //PAF_SAMPLERATE_12000HZ
    0x2,  //PAF_SAMPLERATE_16000HZ
    0x2,  //PAF_SAMPLERATE_22050HZ
    0x2,  //PAF_SAMPLERATE_24000HZ
};

// The McASP outputs (both for DAC and DIT) receive a high speed clock
// and in turn generate a bit and frame clock. The needed clock divider
// values are kept here for easy lookup.
unsigned char *pClkxDiv = NULL;
static const unsigned char clkxDivDIR[PAF_SAMPLERATE_N] =
{
    0x8,  //PAF_SAMPLERATE_UNKNOWN
    0x8,  //PAF_SAMPLERATE_NONE
    0x8,  //PAF_SAMPLERATE_32000HZ
    0x8,  //PAF_SAMPLERATE_44100HZ
    0x8,  //PAF_SAMPLERATE_48000HZ
    0x4,  //PAF_SAMPLERATE_88200HZ
    0x4,  //PAF_SAMPLERATE_96000HZ
    0x2,  //PAF_SAMPLERATE_192000HZ
    0x4,  //PAF_SAMPLERATE_64000HZ
    0x2,  //PAF_SAMPLERATE_128000HZ
    0x2,  //PAF_SAMPLERATE_176400HZ
    0x8,  //PAF_SAMPLERATE_8000HZ
    0x8,  //PAF_SAMPLERATE_11025HZ
    0x8,  //PAF_SAMPLERATE_12000HZ
    0x8,  //PAF_SAMPLERATE_16000HZ
    0x8,  //PAF_SAMPLERATE_22050HZ
    0x8,  //PAF_SAMPLERATE_24000HZ
};

static const unsigned char clkxDivHDMI[PAF_SAMPLERATE_N] =
{
    0x8,  //PAF_SAMPLERATE_UNKNOWN
    0x8,  //PAF_SAMPLERATE_NONE
    0x8,  //PAF_SAMPLERATE_32000HZ
    0x8,  //PAF_SAMPLERATE_44100HZ
    0x2,  //PAF_SAMPLERATE_48000HZ
    0x4,  //PAF_SAMPLERATE_88200HZ
    0x4,  //PAF_SAMPLERATE_96000HZ
    0x2,  //PAF_SAMPLERATE_192000HZ
    0x4,  //PAF_SAMPLERATE_64000HZ
    0x2,  //PAF_SAMPLERATE_128000HZ
    0x2,  //PAF_SAMPLERATE_176400HZ
    0x8,  //PAF_SAMPLERATE_8000HZ
    0x8,  //PAF_SAMPLERATE_11025HZ
    0x8,  //PAF_SAMPLERATE_12000HZ
    0x8,  //PAF_SAMPLERATE_16000HZ
    0x8,  //PAF_SAMPLERATE_22050HZ
    0x8,  //PAF_SAMPLERATE_24000HZ
};

static const unsigned char clkxDivADC[PAF_SAMPLERATE_N] =
{
    0x8,  //PAF_SAMPLERATE_UNKNOWN
    0x8,  //PAF_SAMPLERATE_NONE
    0xC,  //PAF_SAMPLERATE_32000HZ
    0x8,  //PAF_SAMPLERATE_44100HZ
    0x8,  //PAF_SAMPLERATE_48000HZ
    0x4,  //PAF_SAMPLERATE_88200HZ
    0x4,  //PAF_SAMPLERATE_96000HZ
    0x2,  //PAF_SAMPLERATE_192000HZ
    0x4,  //PAF_SAMPLERATE_64000HZ
    0x2,  //PAF_SAMPLERATE_128000HZ
    0x2,  //PAF_SAMPLERATE_176400HZ
    0x8,  //PAF_SAMPLERATE_8000HZ
    0x8,  //PAF_SAMPLERATE_11025HZ
    0x8,  //PAF_SAMPLERATE_12000HZ
    0x8,  //PAF_SAMPLERATE_16000HZ
    0x8,  //PAF_SAMPLERATE_22050HZ
    0x8,  //PAF_SAMPLERATE_24000HZ
};


static const unsigned char clkxDivLYNX[PAF_SAMPLERATE_N] =
{
    0x2,  //PAF_SAMPLERATE_UNKNOWN
    0x2,  //PAF_SAMPLERATE_NONE
    0x2,  //PAF_SAMPLERATE_32000HZ
    0x2,  //PAF_SAMPLERATE_44100HZ
    0x2,  //PAF_SAMPLERATE_48000HZ
    0x2,  //PAF_SAMPLERATE_88200HZ
    0x2,  //PAF_SAMPLERATE_96000HZ
    0x2,  //PAF_SAMPLERATE_192000HZ
    0x2,  //PAF_SAMPLERATE_64000HZ
    0x2,  //PAF_SAMPLERATE_128000HZ
    0x2,  //PAF_SAMPLERATE_176400HZ
    0x2,  //PAF_SAMPLERATE_8000HZ
    0x2,  //PAF_SAMPLERATE_11025HZ
    0x2,  //PAF_SAMPLERATE_12000HZ
    0x2,  //PAF_SAMPLERATE_16000HZ
    0x2,  //PAF_SAMPLERATE_22050HZ
    0x2,  //PAF_SAMPLERATE_24000HZ
};


// The ADCs, when operating as the master input, can only
// generate a limited set of audio sample rates since the clock
// is derived from AUXCLK which is the oscillator connected to the DSP.
// This table faciliates the access and definition of these rates.
static const SmUns oscRateTable[5] =
{
    PAF_SAMPLERATE_UNKNOWN,  // 0
    PAF_SAMPLERATE_32000HZ,  // E17_RATE_32KHZ
    PAF_SAMPLERATE_48000HZ,  // E17_RATE_48KHZ
    PAF_SAMPLERATE_96000HZ,  // E17_RATE_96KHZ
    PAF_SAMPLERATE_192000HZ  // E17_RATE_192KHZ
};

// The 1394 input, when operating as the master input, can only
// generate a limited set of audio sample rates since the clock
// is derived from M0, M1 pins from the FireWorks 1394 card. These
// pins only convey the general category for sample rate (Single,
// Double, or Quad speed). The information from the 1394 card forces
// us to limit each range to a single sample rate. This should be
// changed if the defaults are not desired.
// This table faciliates the access and definition of these rates.
static const SmUns RateTable_1394[4] =
{
    PAF_SAMPLERATE_48000HZ,  // 0
    PAF_SAMPLERATE_48000HZ,  // E17_RATE_48KHZ
    PAF_SAMPLERATE_96000HZ,  // E17_RATE_96KHZ
    PAF_SAMPLERATE_192000HZ  // E17_RATE_192KHZ
};

static const SmUns RateTable_hdmi[8] =
{
    PAF_SAMPLERATE_UNKNOWN,  // HSDIO_AudioFreq_RESERVED
    PAF_SAMPLERATE_32000HZ,  // HSDIO_AudioFreq_32K
    PAF_SAMPLERATE_44100HZ,  // HSDIO_AudioFreq_44_1K
    PAF_SAMPLERATE_48000HZ,  // HSDIO_AudioFreq_48K
    PAF_SAMPLERATE_88200HZ,  // HSDIO_AudioFreq_88_2K
    PAF_SAMPLERATE_96000HZ,  // HSDIO_AudioFreq_96_4K
    PAF_SAMPLERATE_176400HZ,  // HSDIO_AudioFreq_176_4K
    PAF_SAMPLERATE_192000HZ  // HSDIO_AudioFreq_UNKNOWN
};

// base mcasp addresses for easy lookup
static volatile Uint32 * mcaspAddr[_MCASP_PORT_CNT] =
{
    (volatile Uint32 *) _MCASP_BASE_PORT0,
    (volatile Uint32 *) _MCASP_BASE_PORT1,
    (volatile Uint32 *) _MCASP_BASE_PORT2
};

// This is the *normal* setting which is:
//    . I2S data format
//    . soft mute off
#define AK4588_ANA_REG0_NORMAL 0xC

// The AC7xx contains a single hardware mute circuit for all the DACs
// which is controlled by AMUTE0. There is no mute control for DIT output.
static inline void dacHardMute (void) {
    volatile Uint32 *mcasp1 = (volatile Uint32 *) _MCASP_BASE_PORT1;
    mcasp1[_MCASP_PDOUT_OFFSET] |= _MCASP_PDOUT_AMUTE_MASK;
}
static inline void dacHardUnMute (void) {
    volatile Uint32 *mcasp1 = (volatile Uint32 *) _MCASP_BASE_PORT1;
    mcasp1[_MCASP_PDOUT_OFFSET] &= ~_MCASP_PDOUT_AMUTE_MASK;
}

// The AK4588 provides soft mute/unmute functionlity.
static inline void dacSoftMute (void) {
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;
    mcasp0[6] = 0x000 ;
    AK4588_writeAna (hAK4588, 0, AK4588_ANA_REG0_NORMAL | 1);
    mcasp0[6] = 0x400 ;
}
static inline void dacSoftUnMute (void) {
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;
    mcasp0[6] = 0x000 ;
    AK4588_writeAna (hAK4588, 0, AK4588_ANA_REG0_NORMAL);
    mcasp0[6] = 0x400 ;
}

// -----------------------------------------------------------------------------
// McASP Input Configuration Definitions

static const MCASP_ConfigRcv rxConfigDIR =
{
    MCASP_RMASK_OF(0xFFFFFFFF),
    MCASP_RFMT_RMK(
        MCASP_RFMT_RDATDLY_1BIT,
        MCASP_RFMT_RRVRS_MSBFIRST,
        MCASP_RFMT_RPAD_RPBIT,
        MCASP_RFMT_RPBIT_OF(0),
        MCASP_RFMT_RSSZ_32BITS,
        MCASP_RFMT_RBUSEL_DAT,
        MCASP_RFMT_RROT_NONE),
    MCASP_AFSRCTL_RMK(
        MCASP_AFSRCTL_RMOD_OF(2),
        MCASP_AFSRCTL_FRWID_WORD,
        MCASP_AFSRCTL_FSRM_EXTERNAL,
        MCASP_AFSRCTL_FSRP_ACTIVELOW),
    MCASP_ACLKRCTL_RMK(
        MCASP_ACLKRCTL_CLKRP_RISING,
        MCASP_ACLKRCTL_CLKRM_EXTERNAL,
        MCASP_ACLKRCTL_CLKRDIV_DEFAULT),
    MCASP_AHCLKRCTL_RMK(
        MCASP_AHCLKRCTL_HCLKRM_EXTERNAL,
        MCASP_AHCLKRCTL_HCLKRP_RISING,
        MCASP_AHCLKRCTL_HCLKRDIV_DEFAULT),
    MCASP_RTDM_OF(3),
    MCASP_RINTCTL_DEFAULT,
    MCASP_RCLKCHK_DEFAULT
};

static const MCASP_ConfigRcv rxConfigADC =
{
    MCASP_RMASK_OF(0xFFFFFFFF),
    MCASP_RFMT_RMK(
        MCASP_RFMT_RDATDLY_1BIT,
        MCASP_RFMT_RRVRS_MSBFIRST,
        MCASP_RFMT_RPAD_RPBIT,
        MCASP_RFMT_RPBIT_OF(0),
        MCASP_RFMT_RSSZ_32BITS,
        MCASP_RFMT_RBUSEL_DAT,
        MCASP_RFMT_RROT_NONE),
    MCASP_AFSRCTL_RMK(
        MCASP_AFSRCTL_RMOD_OF(2),
        MCASP_AFSRCTL_FRWID_WORD,
        MCASP_AFSRCTL_FSRM_INTERNAL,
        MCASP_AFSRCTL_FSRP_ACTIVELOW),
    MCASP_ACLKRCTL_RMK(
        MCASP_ACLKRCTL_CLKRP_RISING,
        MCASP_ACLKRCTL_CLKRM_INTERNAL,
        MCASP_ACLKRCTL_CLKRDIV_DEFAULT),
    MCASP_AHCLKRCTL_RMK(
        MCASP_AHCLKRCTL_HCLKRM_EXTERNAL,
        MCASP_AHCLKRCTL_HCLKRP_RISING,
        MCASP_AHCLKRCTL_HCLKRDIV_DEFAULT),
    MCASP_RTDM_OF(3),
    MCASP_RINTCTL_DEFAULT,
    MCASP_RCLKCHK_DEFAULT
};

// Note JP2 on the audio card must be *unpopulated* to use this IOS
static const MCASP_ConfigRcv rxConfigADC_AUX =
{
    MCASP_RMASK_OF(0xFFFFFFFF),
    MCASP_RFMT_RMK(
        MCASP_RFMT_RDATDLY_1BIT,
        MCASP_RFMT_RRVRS_MSBFIRST,
        MCASP_RFMT_RPAD_RPBIT,
        MCASP_RFMT_RPBIT_OF(0),
        MCASP_RFMT_RSSZ_32BITS,
        MCASP_RFMT_RBUSEL_DAT,
        MCASP_RFMT_RROT_NONE),
    MCASP_AFSRCTL_RMK(
        MCASP_AFSRCTL_RMOD_OF(2),
        MCASP_AFSRCTL_FRWID_WORD,
        MCASP_AFSRCTL_FSRM_INTERNAL,
        MCASP_AFSRCTL_FSRP_ACTIVELOW),
    MCASP_ACLKRCTL_RMK(
        MCASP_ACLKRCTL_CLKRP_RISING,
        MCASP_ACLKRCTL_CLKRM_INTERNAL,
        MCASP_ACLKRCTL_CLKRDIV_DEFAULT),
    MCASP_AHCLKRCTL_RMK(
        MCASP_AHCLKRCTL_HCLKRM_INTERNAL,
        MCASP_AHCLKRCTL_HCLKRP_RISING,
        MCASP_AHCLKRCTL_HCLKRDIV_DEFAULT),
    MCASP_RTDM_OF(3),
    MCASP_RINTCTL_DEFAULT,
    MCASP_RCLKCHK_DEFAULT
};

#if PAF_FST == 8

const MCASP_ConfigRcv RxConfigDSD =
{
    MCASP_RMASK_OF(0xFFFFFFFF),
    MCASP_RFMT_RMK(
        MCASP_RFMT_RDATDLY_1BIT,
        MCASP_RFMT_RRVRS_MSBFIRST,
        MCASP_RFMT_RPAD_RPBIT,
        MCASP_RFMT_RPBIT_OF(0),
        MCASP_RFMT_RSSZ_32BITS,
        MCASP_RFMT_RBUSEL_DAT,
        MCASP_RFMT_RROT_NONE),
    MCASP_AFSRCTL_RMK(
        MCASP_AFSRCTL_RMOD_OF(2),
        MCASP_AFSRCTL_FRWID_WORD,
        MCASP_AFSRCTL_FSRM_INTERNAL,
        MCASP_AFSRCTL_FSRP_ACTIVELOW),
    MCASP_ACLKRCTL_RMK(
        //MCASP_ACLKRCTL_CLKRP_FALLING,
        MCASP_ACLKRCTL_CLKRP_RISING,
        MCASP_ACLKRCTL_CLKRM_EXTERNAL,
        MCASP_ACLKRCTL_CLKRDIV_OF(0)),
    MCASP_AHCLKRCTL_RMK(
        MCASP_AHCLKRCTL_HCLKRM_EXTERNAL,
        MCASP_AHCLKRCTL_HCLKRP_RISING,
        MCASP_AHCLKRCTL_HCLKRDIV_OF(0)),
    MCASP_RTDM_OF(3),
    MCASP_RINTCTL_DEFAULT,
    MCASP_RCLKCHK_DEFAULT
};

#endif

// -----------------------------------------------------------------------------
// McASP Output Configuration Definitions

static const MCASP_ConfigXmt txConfigDAC =
{
    MCASP_XMASK_OF(0x00FFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_MSBFIRST,
        MCASP_XFMT_XPAD_ZERO,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_24BITS),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(2),
        MCASP_AFSXCTL_FXWID_WORD,
#ifdef BOARD_EXT_CLOCKS
        MCASP_AFSXCTL_FSXM_EXTERNAL,
#else
        MCASP_AFSXCTL_FSXM_INTERNAL,
#endif
        MCASP_AFSXCTL_FSXP_ACTIVELOW),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
#ifdef BOARD_EXT_CLOCKS
        MCASP_ACLKXCTL_CLKXM_EXTERNAL,
#else
        MCASP_ACLKXCTL_CLKXM_INTERNAL,
#endif
        MCASP_ACLKXCTL_CLKXDIV_DEFAULT),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(3),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDACSlave =
{
    MCASP_XMASK_OF(0x00FFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_MSBFIRST,
        MCASP_XFMT_XPAD_ZERO,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_24BITS),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(2),
        MCASP_AFSXCTL_FXWID_WORD,
        MCASP_AFSXCTL_FSXM_EXTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVELOW),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_EXTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_DEFAULT),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(3),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

const MCASP_ConfigXmt txConfigDAC_32bit =
{
    MCASP_XMASK_OF(0xFFFFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_MSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_NONE),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(2),
        MCASP_AFSXCTL_FXWID_WORD,
        MCASP_AFSXCTL_FSXM_INTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVELOW),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_INTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_OF(7)),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(3),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDACSlave_32bit =
{
    MCASP_XMASK_OF(0xFFFFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_MSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_NONE),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(2),
        MCASP_AFSXCTL_FXWID_WORD,
        MCASP_AFSXCTL_FSXM_EXTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVELOW),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_EXTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_DEFAULT),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(3),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDIT =
{
    MCASP_XMASK_OF(0x00FFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_LSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_NONE),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(0x180),
        MCASP_AFSXCTL_FXWID_BIT,
        MCASP_AFSXCTL_FSXM_INTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVEHIGH),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_INTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_OF(3)),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(0xFFFFFFFF),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDITSlave =
{
    MCASP_XMASK_OF(0x00FFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_LSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_NONE),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(0x180),
        MCASP_AFSXCTL_FXWID_BIT,
        MCASP_AFSXCTL_FSXM_EXTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVEHIGH),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_EXTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_OF(3)),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(0xFFFFFFFF),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDIT_16bit =
{
    MCASP_XMASK_OF(0x0000FFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_LSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_24BITS),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(0x180),
        MCASP_AFSXCTL_FXWID_BIT,
        MCASP_AFSXCTL_FSXM_INTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVEHIGH),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_INTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_OF(3)),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(0xFFFFFFFF),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

static const MCASP_ConfigXmt txConfigDIT_16bitSlave =
{
    MCASP_XMASK_OF(0x0000FFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_LSBFIRST,
        MCASP_XFMT_XPAD_DEFAULT,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_24BITS),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(0x180),
        MCASP_AFSXCTL_FXWID_BIT,
        MCASP_AFSXCTL_FSXM_EXTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVEHIGH),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_EXTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_OF(3)),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_EXTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_OF(0)),
    MCASP_XTDM_OF(0xFFFFFFFF),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

// Note JP2 on the audio card must be *unpopulated* to use this IOS.
// And blue wire needed for Rev C audio card (Rev B card needs no mods)
static const MCASP_ConfigXmt txConfigDAC_AUX =
{
    MCASP_XMASK_OF(0x00FFFFFF),
    MCASP_XFMT_RMK(
        MCASP_XFMT_XDATDLY_1BIT,
        MCASP_XFMT_XRVRS_MSBFIRST,
        MCASP_XFMT_XPAD_ZERO,
        MCASP_XFMT_XPBIT_DEFAULT,
        MCASP_XFMT_XSSZ_32BITS,
        MCASP_XFMT_XBUSEL_DAT,
        MCASP_XFMT_XROT_24BITS),
    MCASP_AFSXCTL_RMK(
        MCASP_AFSXCTL_XMOD_OF(2),
        MCASP_AFSXCTL_FXWID_WORD,
        MCASP_AFSXCTL_FSXM_INTERNAL,
        MCASP_AFSXCTL_FSXP_ACTIVELOW),
    MCASP_ACLKXCTL_RMK(
        MCASP_ACLKXCTL_CLKXP_FALLING,
        MCASP_ACLKXCTL_ASYNC_ASYNC,
        MCASP_ACLKXCTL_CLKXM_INTERNAL,
        MCASP_ACLKXCTL_CLKXDIV_DEFAULT),
    MCASP_AHCLKXCTL_RMK(
        MCASP_AHCLKXCTL_HCLKXM_INTERNAL,
        MCASP_AHCLKXCTL_HCLKXP_FALLING,
        MCASP_AHCLKXCTL_HCLKXDIV_DEFAULT),
    MCASP_XTDM_OF(3),
    MCASP_XINTCTL_DEFAULT,
    MCASP_XCLKCHK_DEFAULT
};

// -----------------------------------------------------------------------------
// DAP Input Parameter Definitions

const DAP_E17_Rx_Params DAP_E17_RX_DIR =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV0,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xA0000200,                                 // pinMask
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_SLAVE =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_SLAVE =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000400,                                 // pinMask
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_6CH_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000007,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000001,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_96000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_RATE_96KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                        // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_6CH_96000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_RATE_96KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                        // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_96000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000001,                                 // pinMask
    (E17_RATE_96KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

#if PAF_FST == 8

const DAP_E17_Rx_Params DAP_E17_RX_SACD =
{
    sizeof(DAP_E17_Rx_Params),                  // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *) &RxConfigDSD,                      // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xe0007e00,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_DSDtest =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV0,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xA0000200,                                 // pinMask
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

#endif /* PAF_FST == 8 */

const DAP_E17_Rx_Params DAP_E17_RX_1394_STEREO =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000001,                                // pinMask
    (E17_MODE_1394 << E17_MODE_SHIFT) |
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_1394 =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_MODE_1394 << E17_MODE_SHIFT) |
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_HDMI_STEREO =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000001,                                // pinMask
    (E17_MODE_HDMI << E17_MODE_SHIFT) |
    (E17_MCLK_HDMI << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};
const DAP_E17_Rx_Params DAP_E17_RX_HDMI =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_MODE_HDMI << E17_MODE_SHIFT) |
    (E17_MCLK_HDMI << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_LYNX =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigDIR,                       // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_MODE_LYNX << E17_MODE_SHIFT) |
    (E17_MCLK_DIR << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_192000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000407,                                 // pinMask
    (E17_RATE_192KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000007,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_96000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000007,                                 // pinMask
    (E17_RATE_96KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_192000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000007,                                 // pinMask
    (E17_RATE_192KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};


const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_2CH_48000HZ =
{
    sizeof (DAP_E17_Rx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&rxConfigADC_AUX,                   // pConfig
    -1,                                         // wordSize (unused)
    -1,                                         // precision (unused)
    E17_dapControl,                             // control
    0xE0000001,                                 // pinMask
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0                                         // unused[2]
};

// -----------------------------------------------------------------------------
// DAP Output Parameter Definitions

const DAP_E17_Tx_Params DAP_E17_TX_DAC =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
#ifdef BOARD_EXT_CLOCKS
    0x000001e0,
#else
    0x1C0001e0,                                 // pinMask
#endif
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000100,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_SLAVE =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDACSlave_32bit,            // pConfig
    4,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001e0,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_SLAVE =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDACSlave_32bit,            // pConfig
    4,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000100,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_2STEREO_DAC_SLAVE =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDACSlave_32bit,            // pConfig
    4,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000180,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_ASYNC_32000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001E0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_32KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_ASYNC_32000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000100,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_32KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_ASYNC_48000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001E0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_ASYNC_48000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000100,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_OSC << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DIT =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV2,                                 // moduleNum --> mcasp #
    (Void *) &txConfigDIT,                      // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000001,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DIT_SLAVE =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV2,                                 // moduleNum --> mcasp #
    (Void *) &txConfigDITSlave,                 // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000001,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DIT_16BIT =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV2,                                 // moduleNum --> mcasp #
    (Void *) &txConfigDIT_16bit,                // pConfig
    2,                                          // wordSize (in bytes)
    16,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000001,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DIT_16BIT_SLAVE =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV2,                                 // moduleNum --> mcasp #
    (Void *) &txConfigDIT_16bitSlave,           // pConfig
    2,                                          // wordSize (in bytes)
    16,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000001,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_EXT =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC,                       // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
#ifdef BOARD_EXT_CLOCKS
    0x00000be0,
#else
    0x1C000be0,                                 // pinMask
#endif
    0,                                          // mode
    0,0,0                                       // unused[3]
};
const DAP_E17_Tx_Params DAP_E17_TX_DAC_SLAVE_EXT =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDACSlave_32bit,            // pConfig
    4,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C000be0,                                 // pinMask
    0,                                          // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_32000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC_AUX,                   // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001e0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_32KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_48000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC_AUX,                   // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001e0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_192000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC_AUX,                   // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0001e0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_192KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_6CH_48000HZ =
{
    sizeof (DAP_E17_Tx_Params),                 // size
    "DAP",                                      // name
    MCASP_DEV1,                                 // moduleNum --> mcasp #
    (Void *)&txConfigDAC_AUX,                   // pConfig
    3,                                          // wordSize (in bytes)
    24,                                         // precision (in bits)
    E17_dapControl,                             // control
    0x1C0000e0,                                 // pinMask
    (E17_SYNC_ASYNC << E17_SYNC_SHIFT) |
    (E17_RATE_48KHZ << E17_RATE_SHIFT) |
    (E17_MCLK_AUX << E17_MCLK_SHIFT),           // mode
    0,0,0                                       // unused[3]
};

// -----------------------------------------------------------------------------
// One time initialization of the AC7xx hardware.
//     . Creation of global AK4588 handle
//     . DAC format set to I2S
//     . DAC soft mute rate to 256/fs
//     . DIR format set to I2S and clock generation
//     . configure AMUTE0 as GPIO to control DAC mute circuit
//     . Assert DAC mute circuit

static inline XDAS_Int32 initE17 (DEV_Handle device)
{
    AK4588_Attrs attrs;
    int regData;
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;
    volatile Uint32 *mcasp1 = (volatile Uint32 *) _MCASP_BASE_PORT1;

    // create AK4588 instance
    attrs.controlMode = AK4588_MODE_SPI1;
    attrs.segid = device->segid;
    attrs.anaAddr = E17_AK4588_ANA_ADDR;
    attrs.digAddr = E17_AK4588_DIG_ADDR;
    hAK4588 = AK4588_create (&attrs);
    if (!hAK4588)
        return SYS_EALLOC;

    // AXR0[10] is SPI CS
    mcasp0[_MCASP_PFUNC_OFFSET] |= _MCASP_PFUNC_AXR10_MASK ;
    mcasp0[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_AXR10_MASK ;
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;

    // configure DAC for I2S
    regData = AK4588_ANA_REG0_NORMAL;

    mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
    AK4588_writeAna (hAK4588, 0, regData);
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;

    // soft mute rate = 256/fs
    regData = 0x31;
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
    AK4588_writeAna (hAK4588, 9, regData);
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;

    // configure DIR for I2S output and generate bit/frame clocks
    regData = 0x52;
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
    AK4588_writeDig (hAK4588, 1, regData);
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;
  
	// oscillator master -- default
	clockMuxTx1 (E17_MCLK_OSC, -1);

    // configure AMUTE1 as GPIO for mute control; and assert mute
    mcasp1[_MCASP_PFUNC_OFFSET] |= _MCASP_PFUNC_AMUTE_MASK;
    mcasp1[_MCASP_PDIR_OFFSET]  |= _MCASP_PDIR_AMUTE_MASK;
    dacHardMute ();

    mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
    AK4588_readDig (hAK4588, 0, &regData);
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;

    regData |= 0x3|0x20;

    mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
    AK4588_writeDig (hAK4588, 0, regData);
    mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;
	
    return 0;
    
} //initE17

// -----------------------------------------------------------------------------
// The McASP TX1 section is *only* used as a master clock mux.
// Mux functionality is achieved by selecting either an external high
// speed clock (DIR) or the internal AUXCLK (OSC). This is divided down
// by 1 and output via ACLKX1 which is connected to the high speed input
// of TX0 (DAC) and TX2 (DIT).
// Override is provided for asynchronous output functionality. Once the
// mux has been set with override it cannot be changed until the override
// is cleared.
// Force    == -1 --> change only if override is clear
//          ==  0 --> no change but clear override
//          ==  1 --> change and set override

static XDAS_Int32 clockMuxTx1 (int sel, int force)
{
    // select clkxDiv table
    if (sel == E17_MCLK_DIR)
        pClkxDiv = (unsigned char *) clkxDivDIR;		
    else if (sel == E17_MCLK_HDMI)
		pClkxDiv = (unsigned char *) clkxDivHDMI;
	else
        pClkxDiv = (unsigned char *) clkxDivADC;
    return 0;
} //clockMuxTx1

// -----------------------------------------------------------------------------
// This function returns the input status of the specified device. In the
// case of DIR input, this also configures the AK4588 to generate the needed
// MCLK ration. This is called once when the device is opened
// (PAF_SIO_CONTROL_OPEN) and periodically thereafter
// (PAF_SIO_CONTROL_GET_INPUT_STATUS).

static int manageInput (DEV_Handle device, const DAP_E17_Rx_Params *pParams, PAF_SIO_InputStatus *pStatusOut)
{
    PAF_SIO_InputStatus *pStatusIn = &primaryStatus;
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;
	static int PrevSampRate = 0;

    if ((((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_DIR) &
        (((pParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_STD)) {
        int regData;

        // fetch input status from the AK4588 register file
        AK4588_readDIRStatus (hAK4588, pStatusIn);

        // since DIR set MCLK per fs
        regData = 0x3 | (dirOCKS[pStatusIn->sampleRateMeasured] << 2)|0x20;

        mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
        AK4588_writeDig (hAK4588, 0, regData);
        mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;
    }
    else if ((((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_OSC) &
             (((pParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_STD)) {
        int adcRate = (pParams->e17rx.mode & E17_RATE_MASK) >> E17_RATE_SHIFT;
        int regData;
        pStatusIn->lock = 1;
        pStatusIn->nonaudio = PAF_IEC_AUDIOMODE_AUDIO;
        pStatusIn->emphasis = PAF_IEC_PREEMPHASIS_NO;
        pStatusIn->sampleRateMeasured = oscRateTable[adcRate];
        pStatusIn->sampleRateData = pStatusIn->sampleRateMeasured;

        // since DIR set MCLK per fs
        regData = 0x3 | (dirOCKS[pStatusIn->sampleRateMeasured] << 2)|0x30;

        mcasp0[_MCASP_PDOUT_OFFSET] = 0x000 ;
        AK4588_writeDig (hAK4588, 0, regData);
        mcasp0[_MCASP_PDOUT_OFFSET] = 0x400 ;
    }
    else if ((((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_HDMI) &
             (((pParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_HDMI)) {
        pStatusIn->lock = 1;
        pStatusIn->nonaudio = PAF_IEC_AUDIOMODE_AUDIO;
        pStatusIn->emphasis = PAF_IEC_PREEMPHASIS_NO;
		
		if(!HDMIGpioGetState()) {
			HSR4_readStatus (pStatusIn);
			pStatusIn->sampleRateData = pStatusIn->sampleRateMeasured;
			PrevSampRate = pStatusIn->sampleRateMeasured;
		}
		else {
			pStatusIn->sampleRateMeasured = PrevSampRate;
			pStatusIn->sampleRateData = pStatusIn->sampleRateMeasured;
		}

    }
	else if ((((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_DIR) &
             (((pParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_1394)) {
        pStatusIn->lock = 1;
        pStatusIn->nonaudio = PAF_IEC_AUDIOMODE_AUDIO;
        pStatusIn->emphasis = PAF_IEC_PREEMPHASIS_NO;
		
        FireWorks_read1394Status (pStatusIn);
        //pStatusIn->sampleRateMeasured = PAF_SAMPLERATE_192000HZ; //don't use M0, M1 for HSR-4
		//pStatusIn->sampleRateData = pStatusIn->sampleRateMeasured; // both are identical


    }
    else if((((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_DIR) &
            (((pParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_LYNX)) {
        pStatusIn->lock = 1;
        pStatusIn->nonaudio = PAF_IEC_AUDIOMODE_AUDIO;
        pStatusIn->emphasis = PAF_IEC_PREEMPHASIS_NO;
        // For Lynx card via DTS Box input mechanism there
        // is no way we can determine sampleRate.
        // Currently hardcoding it to 48Khz, for inputting
        // 48Khz multichannel PCM required for DTS Encoder operation.
        pStatusIn->sampleRateMeasured = PAF_SAMPLERATE_48000HZ;
        pStatusIn->sampleRateData = pStatusIn->sampleRateMeasured;
    }
    else if (((pParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT) == E17_MCLK_AUX) {
        int adcRate = (pParams->e17rx.mode & E17_RATE_MASK) >> E17_RATE_SHIFT;

        pStatusIn->lock               = 1;
        pStatusIn->nonaudio           = PAF_IEC_AUDIOMODE_AUDIO;
        pStatusIn->emphasis           = PAF_IEC_PREEMPHASIS_NO;
        pStatusIn->sampleRateMeasured = oscRateTable[adcRate];
        pStatusIn->sampleRateData     = pStatusIn->sampleRateMeasured;
    }
    else
        return 0;

    // update another status if requested
    if (pStatusOut)
        *pStatusOut = *pStatusIn;

    return 0;
} //manageInput

// -----------------------------------------------------------------------------
// This function configures the McASP TX clock dividers based on the
// master clock rate. This is called once when the device is opened
// (PAF_SIO_CONTROL_OPEN) and periodically thereafter (PAF_SIO_CONTROL_SET_RATEX).

static int manageOutput (DEV_Handle device, const DAP_E17_Tx_Params *pParams, float rateX)
{
    volatile Uint32 *mcasp = mcaspAddr[pParams->sio.moduleNum];
    PAF_SIO_InputStatus *pStatusIn = &primaryStatus;
    Uint32 divider;


    if (!pClkxDiv)
        return 1;

    // set clock divider
    if (rateX < .354)
        rateX = 0.25;
    else if (rateX < .707)
        rateX = 0.50;
    else if (rateX < 1.6)
        rateX = 1.00;
    else if (rateX < 2.828)
        rateX = 2.00;
    else
        rateX = 4.00;
    // if asynchronous then force clock change (assumes osc master)
    if (pParams->e17tx.mode & E17_SYNC_MASK) {
        int dacRate = (pParams->e17tx.mode & E17_RATE_MASK) >> E17_RATE_SHIFT;
        divider = pClkxDiv[oscRateTable[dacRate]];
    }
    else
        divider = pClkxDiv[pStatusIn->sampleRateMeasured];
    divider /= rateX;

    // DIT requires 2x clock
    if ((mcasp[_MCASP_AFSXCTL_OFFSET] & _MCASP_AFSXCTL_XMOD_MASK) ==
        (MCASP_AFSXCTL_XMOD_OF(0x180) << _MCASP_AFSXCTL_XMOD_SHIFT)) {
        if (divider < 2)
            return (SYS_EINVAL);
        divider >>= 1;
    }

        mcasp[_MCASP_ACLKXCTL_OFFSET] =
         (mcasp[_MCASP_ACLKXCTL_OFFSET] & ~_MCASP_ACLKXCTL_CLKXDIV_MASK) |
     (MCASP_ACLKXCTL_CLKXDIV_OF(divider-1) << _MCASP_ACLKXCTL_CLKXDIV_SHIFT);
           return 0;
} //manageOutput

// -----------------------------------------------------------------------------
// This function is called by the peripheral driver (DAP) in response to
// various SIO_ctrl() calls made by the framework.

XDAS_Int32 E17_dapControl (DEV_Handle device, const PAF_SIO_Params *pParams, XDAS_Int32 code, XDAS_Int32 arg)
{
    const DAP_E17_Rx_Params *pDapE17RxParams = (const DAP_E17_Rx_Params *)pParams;
    const DAP_E17_Tx_Params *pDapE17TxParams = (const DAP_E17_Tx_Params *)pParams;

    volatile Uint32 *mcasp = mcaspAddr[pParams->sio.moduleNum];
    XDAS_Int32 result = 0;


    // perform one time hardware initialization
    if (!initDone) {
        result = initE17 (device);
		
		if (result)
            return result;
		
        initDone = 1;
    }
	

    switch (code) {

// .............................................................................
// This case provides a regular entry point for managing the specified
// input device. Nominally, this is used to provide lock and sample rate
// status to the framework.

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:
            if (device->mode != DEV_INPUT)
                return SYS_EINVAL;
			
            manageInput (device, pDapE17RxParams, (PAF_SIO_InputStatus *) arg);
            break;

// .............................................................................
// This case provides a regular entry point for managing the specified
// output device. Nominally this is used to change the output clock dividers
// in the case of double rate output (e.g. DTS 96/24).

        case PAF_SIO_CONTROL_SET_RATEX:
            // Support only output rate control, for now
            if (device->mode != DEV_OUTPUT)
                return (SYS_EINVAL);

            // configure clock divider (bit and frame clocks)
            manageOutput (device, pDapE17TxParams, *((float *) arg));
            break;

// .............................................................................
// This case is called once when the device is opened/allocated by the framework.
// Here, for both input and output, this allows for configuring all needed
// clocks for proper operation.

        case PAF_SIO_CONTROL_OPEN:
            if (device->mode == DEV_INPUT) {
			
                int adcRate = (pDapE17RxParams->e17rx.mode & E17_RATE_MASK) >> E17_RATE_SHIFT;

                // determine the master clock based on the mode element of the
                // parameter configuration.
                int sel = (pDapE17RxParams->e17rx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT;

                // When DIR input then configure the AK4588 to generate
                // the bit and frame clocks
                manageInput (device, pDapE17RxParams, NULL);

                // select appropriate master clock (but dont force)
                clockMuxTx1 (sel, -1);

                // For Lynx Card Input change the clk divider Table
                // this requires separte divider apart from DIR and ADC
                if(((pDapE17RxParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_LYNX)
                    pClkxDiv = (unsigned char *) clkxDivLYNX;
					
				// HDMI-specific initialization
				if (((pDapE17RxParams->e17rx.mode & E17_MODE_MASK) >> E17_MODE_SHIFT) == E17_MODE_HDMI)
				{
					hsr_init();	 // Init the HS-4 Expansion card for HDMI Rx
					HDMIGpioInit (); //Init GPIO-to-HMINT signal
				}

                // A value of MCLK_OSC means that the high clock signal is coming, 
                // via passthrough the AK4588, from the 24.576 oscillator on the 
                // audio card. On DA8xx EVM AUXCLK = 24MHz which is close to the
                // OSC value so we can use the same divider values per the table.
                // This with the understanding that the effective sample rate will
                // not be exact but only close.
                if ((sel == E17_MCLK_OSC) || (sel == E17_MCLK_AUX)) {
                    Uint32 divider = pClkxDiv[oscRateTable[adcRate]];

                    mcasp[_MCASP_ACLKRCTL_OFFSET] =
                        (mcasp[_MCASP_ACLKRCTL_OFFSET] & ~_MCASP_ACLKRCTL_CLKRDIV_MASK) |
                        (MCASP_ACLKRCTL_CLKRDIV_OF(divider-1) << _MCASP_ACLKRCTL_CLKRDIV_SHIFT);
                }
            }
            else {
                // If appropriate then signal TX0 in use for DAC output.
                // Also deassert the hardware mute and assert the soft mute.
                if (pParams->sio.moduleNum == MCASP_DEV1) {
                    int sel = (pDapE17TxParams->e17tx.mode & E17_MCLK_MASK) >> E17_MCLK_SHIFT;

                    dacHardUnMute ();
                    dacSoftMute ();

                    // If AUX mode then, we ideally, need to configure mux (U16) for IN1
                    // note this is not driven on RevB audio cards but is on
                    // RevC cards with a 24.576 clock. However this isn't currently
                    // working as expected. i.e. when driven high there is no audio output
                    // but when driven low, which is unchanged from default, then audio is good.
                    // TODO: investigate this and add back in this code as needed.
//                     if (sel == E17_MCLK_AUX) {
//                         volatile Uint32 *mcasp1 = (volatile Uint32 *) _MCASP_BASE_PORT1;
//                         mcasp1[_MCASP_PFUNC_OFFSET] |= _MCASP_PFUNC_AXR11_MASK;
//                         mcasp1[_MCASP_PDIR_OFFSET]  |= _MCASP_PDIR_AXR11_MASK;
//                         mcasp1[_MCASP_PDSET_OFFSET] |= ~_MCASP_PDSET_AXR11_MASK;                        
//                     }
                    
                    // if asynchronous then force clock change
                    if (pDapE17TxParams->e17tx.mode & E17_SYNC_MASK)
                        clockMuxTx1 (sel, 1);
                }

                // configure clock divider (bit and frame clocks)
                manageOutput (device, pDapE17TxParams, 1.0);
            }
            break;

// .............................................................................
// This case is called once when the device is closed/freed by the framework.

        case PAF_SIO_CONTROL_CLOSE:
            // If TX0 then signal it is no longer in use by the DACs and
            // configure manually to generate ADC clocks. Also hard mute
            // the DACs since they are not in use.
            if ((device->mode == DEV_OUTPUT) && (pParams->sio.moduleNum == MCASP_DEV1)) {
               
                dacHardMute ();

                // if async then clear forced clock mux
                // if asynchronous then force clock change
                if (pDapE17TxParams->e17tx.mode & E17_SYNC_MASK)
                    clockMuxTx1 (0, 0);
            }
            break;

// .............................................................................
// These cases are called as appropriate by the framework when there is
// valid output data (UNMUTE) or no valid output (MUTE).

        case PAF_SIO_CONTROL_MUTE:
            if ((device->mode == DEV_OUTPUT) && (pParams->sio.moduleNum == MCASP_DEV1))
                dacSoftMute ();
            break;

        case PAF_SIO_CONTROL_UNMUTE:
            if ((device->mode == DEV_OUTPUT) && (pParams->sio.moduleNum == MCASP_DEV1))
                dacSoftUnMute ();
            break;

// .............................................................................
// This case is called when the device is idled.
// There is no specific handling -- but needed to avoid error return.

        case PAF_SIO_CONTROL_IDLE:
            break;

// .............................................................................
// Called from the IDL Loop to allow for clock management and the like
// The call is protected by a TSK_disable and HWI_disable so it is safe
// to read/write shared resources.

        case PAF_SIO_CONTROL_WATCHDOG:
            // call manageInput in case the sample rate has changed resulting
            // in no output clocks which may have blocked the audio processing
            // thread. This call will reconfigure the AK4588 and restart the clocks.
            if (device->mode == DEV_INPUT)
                manageInput (device, pDapE17RxParams, (PAF_SIO_InputStatus *) arg);
            break;

// .............................................................................
// Called from DOB_issue to allow for different values of the channel status
// fields of the SPDIF output.

        case PAF_SIO_CONTROL_SET_DITSTATUS:
            // No action necessary.
            break;
// .............................................................................
// Any other cases are not handled and return an error.

        default:
            return SYS_EINVAL;
    }

    return result;
} //E17_dapControl

// -----------------------------------------------------------------------------

#define _MCASP_PFUNC_OFFSET    4
#define _MCASP_PDIR_OFFSET     5
#define _MCASP_PDOUT_OFFSET    6
#define _MCASP_PDIN_OFFSET     7
#define _MCASP_PDIN_AHCLKR     30
#define _MCASP_PDIN_AMUTE      25
#define _MCASP_PDOUT_AXR10_MASK 0x00000400u

FireWorks_read1394Status (PAF_SIO_InputStatus *pStatus)
{
    volatile unsigned int * pfunc1;
    volatile unsigned int * pdir1;
    volatile unsigned int * pdin1;
    int Rate1394;
#if 0
    pfunc1 = (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PFUNC_OFFSET);
    pdir1 =  (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PDIR_OFFSET);
    pdin1 =  (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PDIN_OFFSET);

    *pfunc1      = *pfunc1 | ((1 << _MCASP_PDIN_AHCLKR) | (1 << _MCASP_PDIN_AMUTE));
    *pdir1       = *pdir1  | ((0 << _MCASP_PDIN_AHCLKR) | (0 << _MCASP_PDIN_AMUTE));

    // Read the M1, M0 values into a register
    Rate1394 = ((*pdin1 >> (_MCASP_PDIN_AHCLKR - 1)) & 0x2) |
        ((*pdin1 >> (_MCASP_PDIN_AMUTE))      & 0x1);
    pStatus->sampleRateMeasured = RateTable_1394[Rate1394];
#endif
	volatile unsigned int * pfunc0;
    volatile unsigned int * pdir0;
    volatile unsigned int * pdin0;
    int m0,m1;
	pfunc1 = (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PFUNC_OFFSET);
	*pfunc1      = *pfunc1 |(1 << 11); // M1 is via AXR1_11 gpio
	pfunc0 = (void *) ( _MCASP_BASE_PORT0 + 4*_MCASP_PFUNC_OFFSET);
	*pfunc0      = *pfunc0 |(1 << 28); // M0 is via AFSX0 gpio
	pdir1 =  (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PDIR_OFFSET);
	*pdir1       = *pdir1  | (0 << 11); // M1 is via AXR1_11 input
	pdir0 =  (void *) ( _MCASP_BASE_PORT0 + 4*_MCASP_PDIR_OFFSET);
	*pdir0       = *pdir0  | (0 << 28); // M0 is via AFSX0 input
	pdin1 =  (void *) ( _MCASP_BASE_PORT1 + 4*_MCASP_PDIN_OFFSET);
	m1 = (*pdin1 >> 11) &0x1; // read M1 into register
	pdin0 =  (void *) ( _MCASP_BASE_PORT0 + 4*_MCASP_PDIN_OFFSET);
	m0 = (*pdin0 >> 28) &0x1; // read M0 into register
	pStatus->sampleRateMeasured = RateTable_1394[(m1<<1) + m0];
    return 0;
}

extern unsigned int read_hdmi_samprate();
int RateHdmi=0;
void HSR4_readStatus (PAF_SIO_InputStatus *pStatus)
{
	//if(!RateHdmi)
	RateHdmi=read_hdmi_samprate();
	pStatus->sampleRateMeasured = RateTable_hdmi[RateHdmi];
}

#define GP_MOD_NUM 0
#define GP_PIN_NUM 13
#define ACTIVE_LOW 1

void HDMIGpioInit (void) {
	gpioInitRead (GP_MOD_NUM, GP_PIN_NUM);
}

unsigned int HDMIGpioGetState (void) {
	return(gpioRead (GP_MOD_NUM, GP_PIN_NUM));
}
// -----------------------------------------------------------------------------

// EOF
