
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/*
 *  I2C implementation
 *
 */
#include <hwi.h> 
#include <stdio.h>
#include "evmc6747_i2c.h"
#define HSR4_I2C_ADDR 0x5d
#include <stdarg.h>
#include "string.h"
#include "vprocCmds_a.h"
extern int alpha_i2c_write(unsigned short, ...);
int gret_val=0;
int alpha_i2c_write(unsigned short var1, ...)
{
	unsigned short alpha_type,length,temp_var;
	int i,offset,ret_val;
	unsigned char cmd[50];
	char *s;
	va_list argp;
	va_start(argp, var1);

	alpha_type = var1>> 8;
	switch(alpha_type)
	{
		case 0xca:
		case 0xc2:
		case 0xc3:
		case 0xc4:
			length = 4;
			break;
		case 0xcb:
			length = 6;
			break;
		case 0xcc:
			length = 8;
			break;
		case 0xcd:
		case 0xc5:
			length= 8; // temporary - data starts after 8 bytes
			break;
	}
	cmd[0]=length;
	temp_var=var1;
	for(i=0;i<length-2;i+=2) // convert to bytes as per protocol
	{
		cmd[i+1]= temp_var & 0xff;
		cmd[i+2]= temp_var >> 8;
		temp_var=va_arg(argp, short);
	}
	cmd[i+1]= temp_var & 0xff;
	cmd[i+2]= temp_var >> 8;
	if(alpha_type == 0xcd) // special processing for variable length
	{
		offset=9;
		s = va_arg(argp, char *); // remaining data is in form of string
		length = temp_var; // last short indicates data length
		cmd[0]+=length;
		for(i=offset;i<offset+length;i++)
			cmd[i]=s[i-offset];
	}
va_end(argp);
ret_val=EVMC6747_I2C_write( HSR4_I2C_ADDR, cmd, cmd[0]+1 );
if(ret_val == -1)
		gret_val++;
return ret_val; 
}

set_audio_desc(unsigned char var1,unsigned char var2,unsigned char var3,unsigned char var4,unsigned char var5)
{
	int ret_val=0;
	do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_FORMAT(var1, var2));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_NUM_CHANNELS(var1, var3));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_SAMPLE_RATES(var1, var4));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_DESC_MISC(var1, var5));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_ALERT(HSDIO_ALERT_INPUT_AUDIO_CHANGE_msk));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_ALERT(HSDIO_ALERT_INPUT_AUDIO_MUTE_msk));}while (ret_val !=0);
}

void hrptredid(unsigned short board)
{
	Uint8 length;
	int ret_val=0,i;
	unsigned char data[50];
	FILE * fp1 = fopen("temp.txt","w");

	do
	{
		EVMC6747_waitusec(1000);ret_val=alpha_i2c_write(HSDIO_EDID_MFR_PNPID("TXN"));
	}while (ret_val !=0);
	
do{EVMC6747_waitusec(1000);ret_val=alpha_i2c_write(HSDIO_GET_EDID_MFR_PNPID);}while (ret_val !=0);
do{EVMC6747_waitusec(1000);ret_val=EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);}while (ret_val !=0);
	do{EVMC6747_waitusec(1000);ret_val=EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);}while (ret_val !=0);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_PRODUCT_CODE(2));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_SHORT_SERIAL_NUMBER(board));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_WEEK_OF_MFR(23));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_YEAR_OF_MFR(19));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_PREFERRED_TIMING(5));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_LATENCY_PROGRESSIVE(0));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_AUDIO_LATENCY_INTERLACED(0));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_PRODUCT_NAME("HRPTR"));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_MFR_NAME("TexasInst"));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_SPEAKER_ALLOCATION_BLOCK(0x4F));}while (ret_val !=0);
	set_audio_desc(0,1,2,0x1f,7);
	set_audio_desc(1,1,8,0x1f,7);
	set_audio_desc(2,2,6,0x7,80);
	set_audio_desc(3,7,6,0x1e,192);
	set_audio_desc(4,7,8,0x6,192);
	set_audio_desc(5,6,6,0x1f,192);
	set_audio_desc(6,10,8,0x07,0);
	set_audio_desc(7,12,8,0x7F,0);
	set_audio_desc(8,11,8,0x7F,1);
	set_audio_desc(9,0,0,0,0);
	set_audio_desc(10,0,0,0,0);
	set_audio_desc(11,0,0,0,0);
	set_audio_desc(12,0,0,0,0);
	set_audio_desc(13,0,0,0,0);
	set_audio_desc(14,0,0,0,0);
	set_audio_desc(15,0,0,0,0);
	set_audio_desc(16,0,0,0,0);
	set_audio_desc(17,0,0,0,0);
	set_audio_desc(18,0,0,0,0);
	set_audio_desc(19,0,0,0,0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_EDID_GO);}while (ret_val !=0);
	fclose(fp1);
}

void  hdmi128()
{
		int ret_val=0;
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_MCLK_TO_HOST(HSDIO_AudioMClk_128X));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_FORMAT_TO_HOST(HSDIO_AudioFmt_I2S));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_IMPLEMENT_AUDIO_TO_HOST_CMDS);}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_PRESENT(HSDIO_AudioPresent_HAS_NO_AUDIO));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_FORMAT(HSDIO_AudioFmt_I2S));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_SAMPLE_SIZE(HSDIO_AudioSampleSize_32));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_FREQ(HSDIO_AudioFreq_48K));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_LAYOUT(HSDIO_AudioLayout_2));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_MCLK(HSDIO_AudioMClk_AUTO));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_AUX_CHANNEL_CNT(1));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_AUX_SPEAKER_MAPPING(0));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_CHSTS_PCM_PREEMPHASIS(HSDIO_CSFormatInfo_PCM_NO_PRE));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_CHSTS_COPYRIGHT(HSDIO_CSCopyright_PROTECTED));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_CHSTS_WORD_LENGTH(HSDIO_CSWordLength_24));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_OUTPUT_GO);}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_AUDIO_ROUTING(HSDIO_AudioRouting_HSDIOIN_HOSTOUT));}while (ret_val !=0);
	EVMC6747_waitusec(1000);do{ret_val=alpha_i2c_write(HSDIO_SYS_CFG_GO);}while (ret_val !=0);
}

unsigned int read_hdmi_samprate()
{
	unsigned char data[50];
	Uint8 length;
	int ret_val=7;
	//int ret_val2=0;
	int clear_to_read=5;
	
	clear_to_read==alpha_i2c_write(HSDIO_ALERT_STS); //clear the interrupt on ~HMINT by reading the Alert Status register
	
	ret_val=alpha_i2c_write(HSDIO_AUDIO_INPUT_FREQ_STS);
	if(!ret_val) EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	if(!ret_val) EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	if(!ret_val) ret_val= data[2]; // indicates sample rate
	else
	ret_val = 0;
	
	/*ret_val2=alpha_i2c_write(HSDIO_GET_AUDIO_OUTPUT_FREQ);
	if(!ret_val2) EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	if(!ret_val2) EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	if(!ret_val2) ret_val2= data[2]; // indicates sample rate
	else
	ret_val2 = 0;*/

	
#if 0
	//unsigned char cmd[]={4,0x01,0xC2,0x00,0x3A};
	//ret_val=EVMC6747_I2C_write( HSR4_I2C_ADDR, cmd, cmd[0]+1 );
	//ret_val=EVMC6747_I2C_read( HSR4_I2C_ADDR, data,5 );
	//if(!ret_val) ret_val= data[3]; // indicates sample rate
	//else
	//ret_val = 0;
#endif
	
	return ret_val;
}
unsigned int read_hdmi_debug()
{
unsigned char data[50];
	Uint8 length;
	int ret_val,i;
	
	FILE * fp1 = fopen("temp.txt","w");
alpha_i2c_write(HSDIO_GET_EDID_MFR_PNPID);
EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_PRODUCT_CODE);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_WEEK_OF_MFR);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_YEAR_OF_MFR);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_PREFERRED_TIMING);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_AUDIO_LATENCY_PROGRESSIVE);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_AUDIO_LATENCY_INTERLACED);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_PRODUCT_NAME);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_MFR_NAME);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	alpha_i2c_write(HSDIO_GET_EDID_SPEAKER_ALLOCATION_BLOCK);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&length,1);
	EVMC6747_I2C_read(HSR4_I2C_ADDR,&data[0],length);
	for(i=0;i<length;i=i+2)
	fprintf(fp1,"0x%2x%2x\n",data[i+1],data[i]);
	fclose(fp1);
	return 0;
}




void hsr_init()
{
	EVMC6747_I2C_init();
	
	hrptredid(6);
	//read_hdmi_debug();
	
	hdmi128();
	
}


	
	


/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  EVMC6747_wait( delay )                                                  *
 *                                                                          *
 *      Wait in a software loop for 'x' delay                               *
 *                                                                          *
 * ------------------------------------------------------------------------ */
void EVMC6747_wait( Uint32 delay )
{
    volatile Uint32 i;
    for ( i = 0 ; i < delay ; i++ ){ };
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  EVMC6747_waitusec( usec )                                               *
 *                                                                          *
 *      Wait in a software loop for 'x' microseconds                        *
 *                                                                          *
 * ------------------------------------------------------------------------ */
void EVMC6747_waitusec( Uint32 usec )
{
    EVMC6747_wait( usec * 3 );
}
Int32 i2c_timeout = 0x10000;

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  _I2C_init( )                                                            *
 *                                                                          *
 *      Enable and initalize the I2C module                                 *
 *      The I2C clk is set to run at 20 KHz                                 *
 *                                                                          *
 * ------------------------------------------------------------------------ */
Int16 EVMC6747_I2C_init( )
{
    I2C_ICMDR   = 0;                // Reset I2C
    I2C_ICPSC   = 2;               // Prescale to get 1MHz I2C internal
    I2C_ICCLKL  = 6;               // Config clk LOW for 20kHz
    I2C_ICCLKH  = 6;               // Config clk HIGH for 20kHz
    I2C_ICMDR  |= ICMDR_IRS;        // Release I2C from reset
    return 0;
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  _I2C_close( )                                                           *
 *                                                                          *
 * ------------------------------------------------------------------------ */
Int16 EVMC6747_I2C_close( )
{
        I2C_ICMDR = 0;                      // Reset I2C
        return 0;
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  _I2C_reset( )                                                           *
 *                                                                          *
 * ------------------------------------------------------------------------ */
Int16 EVMC6747_I2C_reset( )
{
    EVMC6747_I2C_close( );
    EVMC6747_I2C_init( );
    return 0;
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  _I2C_write( i2c_addr, data, len )                                       *
 *                                                                          *
 *      I2C write in Master mode                                            *
 *                                                                          *
 *      i2c_addr    <- I2C slave address                                    *
 *      data        <- I2C data ptr                                         *
 *      len         <- # of bytes to write                                  *
 *                                                                          *
 * ------------------------------------------------------------------------ */
Int16 EVMC6747_I2C_write( Uint16 i2c_addr, Uint8* data, Uint16 len )
{
    Int32 timeout, i;
    Int32   oldMask;


        I2C_ICCNT = len;                    // Set length
        I2C_ICSAR = i2c_addr;               // Set I2C slave address
        I2C_ICMDR = ICMDR_STT               // Set for Master Write
                  | ICMDR_TRX
                  | ICMDR_MST
                  | ICMDR_IRS
                  | ICMDR_FREE;

        EVMC6747_wait( 10 );                        // Short delay

        for ( i = 0 ; i < len ; i++ )
        {
 
            I2C_ICDXR = data[i];            // Write

            timeout = i2c_timeout;
            do
            {
            	EVMC6747_wait( 10 );         
                if ( timeout-- < 0  )
                {
                    EVMC6747_I2C_reset( );
                    return -1;
                }
            } while ( ( I2C_ICSTR & ICSTR_ICXRDY ) == 0 );// Wait for Tx Ready
   
        }

        I2C_ICMDR |= ICMDR_STP;             // Generate STOP

        return 0;

}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  _I2C_read( i2c_addr, data, len )                                        *
 *                                                                          *
 *      I2C read in Master mode                                             *
 *                                                                          *
 *      i2c_addr    <- I2C slave address                                    *
 *      data        <- I2C data ptr                                         *
 *      len         <- # of bytes to write                                  *
 *                                                                          *
 *      Returns:    0: PASS                                                 *
 *                 -1: FAIL Timeout                                         *
 *                                                                          *
 * ------------------------------------------------------------------------ */
Int16 EVMC6747_I2C_read( Uint16 i2c_addr, Uint8* data, Uint16 len )
{
    Int32 timeout, i;
    
    I2C_ICCNT = len;                    // Set length
    I2C_ICSAR = i2c_addr;               // Set I2C slave address
    I2C_ICMDR = ICMDR_STT               // Set for Master Read
              | ICMDR_MST
              | ICMDR_IRS
              | ICMDR_FREE;

    EVMC6747_wait( 10 );                        // Short delay

    for ( i = 0 ; i < len ; i++ )
    {
        timeout = i2c_timeout;
	
        /* Wait for Rx Ready */
        do
        {
        	EVMC6747_wait( 10 );         
            if ( timeout-- < 0 )
            {
                EVMC6747_I2C_reset( );
                return -1;
            }
        } while ( ( I2C_ICSTR & ICSTR_ICRRDY ) == 0 );// Wait for Rx Ready

        data[i] = I2C_ICDRR;            // Read
       
    }

      //I2C_ICMDR |= ICMDR_STP;             // Generate STOP

        return 0;
}

