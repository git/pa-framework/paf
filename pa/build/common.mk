
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#
#
# Common portions of Master Makefiles
#
#
# -----------------------------------------------------------------------------

# .............................................................................
# set quietness

ifdef QUIET
SILENCE := @
endif

# .............................................................................

# replace DIRS variable with only the directories which exist. A := must be
# used to avoid endless recursive expansion
DIRS       := $(wildcard ${DIRS})

DIRCLEAN    = $(addsuffix .clean,$(DIRS))
DIRINSTALL  = $(addsuffix .install,$(DIRS))

# .............................................................................
# override MAKE to omit directory navigation
# use := so that current MAKE variable is evaluated before assignment

export MAKE:=$(MAKE) --no-print-directory

# .............................................................................

.PHONY: all clean install $(DIRS) $(DIRCLEAN) $(DIRINSTALL)

all: $(DIRS)

clean: $(DIRCLEAN)

install: $(DIRINSTALL)

# .............................................................................

$(DIRS)::
	$(SILENCE)$(MAKE) -C $@

$(DIRCLEAN): %.clean:
	$(SILENCE)$(MAKE) -C $* clean

$(DIRINSTALL): %.install:
	$(SILENCE)$(MAKE) -C $* install

# special rule to print any makefile variable. Try, e.g., "make print-DIRS"
print-%:
	@echo $* = $($*)

# .............................................................................
