
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#
# Target makefile (invoked from the source directory)
#
#
# -----------------------------------------------------------------------------

# This forces all (well, almost all) the builtin rules to be removed.
# We'll tell it how to build everything, and we don't want it using any other rules.
# More comprehensive is to use the -r option when invoking make; but this doesn't hurt
.SUFFIXES:

# Export default architecture, configuration, and generated directories
export ARCH   = c674x
export CONFIG = release

export ROOTDIR = D:\Projects\paf_1710
export OBJDIR = $(ARCH)\$(CONFIG)

ifneq (,$(filter "K001","${ROM}"))
export INSTALLDIR = ${ROOTDIR}\pa\i\d800k001\pa\build\$(OBJDIR)
else
export INSTALLDIR = ${ROOTDIR}\pa\build\$(OBJDIR)
endif

MAKETARGET = $(MAKE) -C $@ -f $(CURDIR)/Makefile SRCDIR=$(CURDIR) $(MAKECMDGOALS)

.PHONY: clean $(OBJDIR)

# This rule does the relocation to the target directory and sub-make invocation.
# Also if the target directory doesn't exist then it creates it.
# use the + operator to ensure this jump is made even when make is invoked with -n option
# and the @ operator to hide the clutter of the jump itself from the user.
$(OBJDIR):
	+@if not exist $@ mkdir $@
	+@$(MAKETARGET)

# Without these lines, then, make will attempt to rebuild the makefiles using the
# match-anything rule, invoking itself recursively in the target directory.
Makefile : ;
makefile : ;
%.mk :: ;

# Match-anything implicit rule that forces sub-make invocation on all undefined targets.
# Once make builds the $(OBJDIR) target once, it "knows" that target has been updated
# and won't try to build it again for this invocation. Since the $(OBJDIR) target invokes
# the sub-make with all the command line goals, in $(MAKECMDGOALS), that one invocation is
# enough to build all the targets the user asked for.
% :: $(OBJDIR) ;

# Put the clean target here so we assure that they are invoked in the source directory,
# and that we don't recurse into the target directory at all.
clean:
	@if exist $(OBJDIR) rmdir /s /q $(OBJDIR)

