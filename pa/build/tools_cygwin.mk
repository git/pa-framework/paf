# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#
# Common Tool definitions for PA.
#
#
# -----------------------------------------------------------------------------

# Architecture specific settings of tools and tool options
TI_DIR = c:/ti
CG_TOOLS = ${TI_DIR}/C6000 Code Generation Tools 6.1.13

# Versions
BIOSVER=bios_6_21_00_13
XDCVER=xdctools_3_16_02_32
IPCVER=ipc_1_00_05_60
LINKVER=1_65_02_09
XDAISVER=6_25_01_08
PAFVER=05.00.01.06

CC = "$(CG_TOOLS)/bin/cl6x.exe"
LD = "$(CG_TOOLS)/bin/lnk6x.exe"
AR = "$(CG_TOOLS)/bin/ar6x.exe"
NM = "$(CG_TOOLS)/bin/nm6x.exe"

# BIOS and XDC
BIOS_PCKGS = ${TI_DIR}/${BIOSVER}/packages
XDCROOT    = ${TI_DIR}/${XDCVER}
IPC_PCKGS  = ${TI_DIR}/${IPCVER}/packages
LINK_BASE  = ${TI_DIR}/dsplink_linux_${LINKVER}
XDC_PCKGS  = ${XDCROOT}/packages
XDC_PLAT   = ti.platforms.generic:plat
XS         = export XDCROOT="${XDCROOT}"; export XDCPATH="${BIOS_PCKGS}"; "${XDCROOT}/xs"

# Common includes
INCLUDES += -I"${CG_TOOLS}/include"
INCLUDES += -I"$(BIOS_PCKGS)"
INCLUDES += -I"$(BIOS_PCKGS)/ti/bios/include"
INCLUDES += -I"$(IPC_PCKGS)"
INCLUDES += -I"$(XDC_PCKGS)"
INCLUDES += -I"${TI_DIR}/xdais_${XDAISVER}/packages/ti/xdais"


# PA includes
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/com`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/std`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/dec/com`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/include`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/alpha`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/s3`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/sio/acp1`
INCLUDES += -I`cygpath -w ${ROOTDIR}/da/psp/dat`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/chip/primus/csl`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/chip/primus/csl/c674x`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/boards/drivers`

# FIL includes
FIL_INCLUDES  = -I`cygpath -w ${ROOTDIR}/pa/asp/fil`
FIL_INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/fil/alg`
FIL_INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/fil/src`

DEFINES  += -d"PAF_DEVICE=0xD8000000"


# -pdsw225 generates a compiler warning on implicitly defined functions
# -mv6740, among other things, defines _674_ which is needed for things like hwi.h
#-pdsr1037 # TODO: what is id 1037?
#-mo # Place each function in a separate subsection
#--mem_model:data=far # Const access model
#--mem_model:const=far # Data access model
#-k # Keep the generated .asm file

CFLAGS   = -fr"." $(INCLUDES) $(DEFINES)
CFLAGS  += -pdsr1037
CFLAGS  += -pdse225
CFLAGS  += -mo
CFLAGS  += -mv6740
CFLAGS  += --mem_model:data=far
CFLAGS  += --mem_model:const=far
CFLAGS  += -k

ifeq (${CONFIG},debug)
CFLAGS += -g
endif
ifeq (${CONFIG},release)
CFLAGS += -o3
endif

# -q = quiet
ARFLAGS= rq 
#DEFINES = -"Dxdc_target_types__=ti/targets/std.h"
#DEFINES += -D"xdc_target_name__=C64P"
