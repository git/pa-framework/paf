
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx library function declarations
//
//
//

#define CPUARBU         *(volatile unsigned int*) 0x01841000
#define MSTPRI0         *(volatile unsigned int*) 0x01C14110
#define MSTPRI1         *(volatile unsigned int*) 0x01C14114
#define MSTPRI2         *(volatile unsigned int*) 0x01C14118
#define BPRIO           *(volatile unsigned int*) 0xB0000020

#define L1DCFG			*(volatile unsigned int*) 0x01840040
#define L1DWB			*(volatile unsigned int*) 0x01845040
#define L1DWBINV		*(volatile unsigned int*) 0x01845044
#define L1DINV			*(volatile unsigned int*) 0x01845048

#define L1PCFG			*(volatile unsigned int*) 0x01840020
#define L1PINV			*(volatile unsigned int*) 0x01845028

#define L2CFG			*(volatile unsigned int*) 0x01840000
#define L2WB			*(volatile unsigned int*) 0x01845000
#define L2WBINV			*(volatile unsigned int*) 0x01845004
#define L2INV			*(volatile unsigned int*) 0x01845008

#define PINMUX0         *(volatile unsigned int*) 0x01C14120
#define PINMUX1         *(volatile unsigned int*) 0x01C14124
#define PINMUX2         *(volatile unsigned int*) 0x01C14128
#define PINMUX3         *(volatile unsigned int*) 0x01C1412C
#define PINMUX4         *(volatile unsigned int*) 0x01C14130
#define PINMUX5         *(volatile unsigned int*) 0x01C14134
#define PINMUX6         *(volatile unsigned int*) 0x01C14138
#define PINMUX7         *(volatile unsigned int*) 0x01C1413C
#define PINMUX8         *(volatile unsigned int*) 0x01C14140
#define PINMUX9         *(volatile unsigned int*) 0x01C14144
#define PINMUX10        *(volatile unsigned int*) 0x01C14148
#define PINMUX11        *(volatile unsigned int*) 0x01C1414C
#define PINMUX12        *(volatile unsigned int*) 0x01C14150
#define PINMUX13        *(volatile unsigned int*) 0x01C14154
#define PINMUX14        *(volatile unsigned int*) 0x01C14158
#define PINMUX15        *(volatile unsigned int*) 0x01C1415C
#define PINMUX16        *(volatile unsigned int*) 0x01C14160
#define PINMUX17        *(volatile unsigned int*) 0x01C14164
#define PINMUX18        *(volatile unsigned int*) 0x01C14168
#define PINMUX19        *(volatile unsigned int*) 0x01C1416C

#define CFGCHIP0		*(volatile unsigned int*) 0x01C1417C
#define CFGCHIP1		*(volatile unsigned int*) 0x01C14180
#define CFGCHIP2		*(volatile unsigned int*) 0x01C14184
#define CFGCHIP3		*(volatile unsigned int*) 0x01C14188
#define CFGCHIP4		*(volatile unsigned int*) 0x01C1418C

#define PSC0_MDCTL      *(volatile unsigned int*) 0x01C10A00
#define PSC0_MDSTAT     *(volatile unsigned int*) 0x01C10800
#define PSC0_PTCMD      *(volatile unsigned int*) 0x01C10120
#define PSC0_PTSTAT     *(volatile unsigned int*) 0x01C10128

#define PSC1_MDCTL      *(volatile unsigned int*) 0x01E27A00
#define PSC1_MDSTAT     *(volatile unsigned int*) 0x01E27800
#define PSC1_PTCMD      *(volatile unsigned int*) 0x01E27120
#define PSC1_PTSTAT     *(volatile unsigned int*) 0x01E27128

#define PSC0			0
#define PSC1			1

#define PD0				0
#define PD1				1

#define	LPSC_EDMA_CC0	0
#define	LPSC_EDMA_TC0	1
#define	LPSC_EDMA_TC1	2
#define	LPSC_EMIFA		3
#define	LPSC_SPI0		4
#define	LPSC_MMCSD0		5
#define	LPSC_ARM_AINTC	6
#define	LPSC_ARM_RAMROM	7
#define	LPSC_SCnKM		8
#define	LPSC_UART0		9
#define	LPSC_SCR0		10
#define	LPSC_SCR1		11
#define	LPSC_SCR2		12
#define	LPSC_PRUSS		13
#define	LPSC_ARM		14
#define	LPSC_DSP		15

#define	LPSC_EDMA_CC1	0
#define	LPSC_USB20		1
#define	LPSC_USB11		2
#define	LPSC_GPIO		3
#define	LPSC_UHPI		4
#define	LPSC_EMAC		5
#define	LPSC_DDR		6
#define	LPSC_MCASP0		7
#define	LPSC_SATA		8
#define	LPSC_VPIF		9
#define	LPSC_SPI1		10
#define	LPSC_I2C1		11
#define	LPSC_UART1		12
#define	LPSC_UART2		13
#define	LPSC_MCBSP0		14
#define	LPSC_MCBSP1		15
#define	LPSC_LCDC		16
#define	LPSC_EPWM		17
#define	LPSC_MMCSD1		18
#define	LPSC_UPP		19
#define	LPSC_ECAP		20
#define	LPSC_EDMA_TC2	21
#define	LPSC_SCR_F0		24
#define	LPSC_SCR_F1		25
#define	LPSC_SCR_F2		26
#define	LPSC_SCR_F6		27
#define	LPSC_SCR_F7		28
#define	LPSC_SCR_F8		29
#define	LPSC_BR_F7		30
#define	LPSC_SHARED_RAM	31

void lpscEnable (int pscNum, int pdNum, int lpscNum);
void lpscDisable (int pscNum, int pdNum, int lpscNum);

unsigned int gpioInitRead (int gpioNum, int gpioPin);
unsigned int gpioRead (int gpioNum, int gpioPin);
void gpioInitWrite (int gpioNum, int gpioPin, int gpioValue);
void gpioWrite (int gpioNum, int gpioPin, int gpioValue);

void delay (volatile int n);

void pllConfigure (unsigned int CLKMODE, unsigned int PLLM, unsigned int POSTDIV, unsigned int PLLDIV3, unsigned int PLLDIV5,
				   unsigned int PLLDIV7, unsigned int DIV45_EN);

void emifbConfigure (unsigned int SDCFG, unsigned int SDTIM1, unsigned int SDTIM2, unsigned int SDRFC);

void armBoot (unsigned int address);
