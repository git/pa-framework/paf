
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _CSLR__ECAP_001_H_
#define _CSLR__ECAP_001_H_

#include <cslr.h>

#include <tistdtypes.h>


/* Minimum unit = 1 byte */

/**************************************************************************\
* Register Overlay Structure
\**************************************************************************/
typedef struct  {
    volatile Uint32 TSCTR;
    volatile Uint32 CTRPHS;
    volatile Uint32 CAP1;
    volatile Uint32 CAP2;
    
    volatile Uint32 CAP3;
    volatile Uint32 CAP4;
    volatile Uint32 RESV_18;
    volatile Uint32 RESV_1C;
    
    volatile Uint32 RESV_20;
    volatile Uint32 RESV_24;
    volatile Uint16 ECCTL1;
    volatile Uint16 ECCTL2;
    volatile Uint16 ECEINT;
    volatile Uint16 ECEFLG;
    
    volatile Uint16 ECCLR;
    volatile Uint16 ECFRC;
    volatile Uint32 RESV_34;
    volatile Uint32 RESV_38;
    volatile Uint32 RESV_3C;
    
    volatile Uint32 RESV_40;
    volatile Uint32 RESV_44;
    volatile Uint32 RESV_48;
    volatile Uint32 RESV_4C;
    
    volatile Uint32 RESV_50;
    volatile Uint32 RESV_54;
    volatile Uint32 RESV_58;
    volatile Uint32 PID;
} CSL_EcapRegs;

/**************************************************************************\
* Field Definition Macros
\**************************************************************************/

/* TSCTR */

#define CSL_ECAP_TSCTR_TSCTR_MASK (0xFFFFFFFFu)
#define CSL_ECAP_TSCTR_TSCTR_SHIFT (0x00000000u)
#define CSL_ECAP_TSCTR_TSCTR_RESETVAL (0x00000000u)

#define CSL_ECAP_TSCTR_RESETVAL (0x00000000u)

/* CTRPHS */

#define CSL_ECAP_CTRPHS_CTRPHS_MASK (0xFFFFFFFFu)
#define CSL_ECAP_CTRPHS_CTRPHS_SHIFT (0x00000000u)
#define CSL_ECAP_CTRPHS_CTRPHS_RESETVAL (0x00000000u)

#define CSL_ECAP_CTRPHS_RESETVAL (0x00000000u)

/* CAP1 */

#define CSL_ECAP_CAP1_CAP1_MASK (0xFFFFFFFFu)
#define CSL_ECAP_CAP1_CAP1_SHIFT (0x00000000u)
#define CSL_ECAP_CAP1_CAP1_RESETVAL (0x00000000u)

#define CSL_ECAP_CAP1_RESETVAL (0x00000000u)

/* CAP2 */

#define CSL_ECAP_CAP2_CAP2_MASK (0xFFFFFFFFu)
#define CSL_ECAP_CAP2_CAP2_SHIFT (0x00000000u)
#define CSL_ECAP_CAP2_CAP2_RESETVAL (0x00000000u)

#define CSL_ECAP_CAP2_RESETVAL (0x00000000u)

/* CAP3 */

#define CSL_ECAP_CAP3_CAP3_MASK (0xFFFFFFFFu)
#define CSL_ECAP_CAP3_CAP3_SHIFT (0x00000000u)
#define CSL_ECAP_CAP3_CAP3_RESETVAL (0x00000000u)

#define CSL_ECAP_CAP3_RESETVAL (0x00000000u)

/* CAP4 */

#define CSL_ECAP_CAP4_CAP4_MASK (0xFFFFFFFFu)
#define CSL_ECAP_CAP4_CAP4_SHIFT (0x00000000u)
#define CSL_ECAP_CAP4_CAP4_RESETVAL (0x00000000u)

#define CSL_ECAP_CAP4_RESETVAL (0x00000000u)

/* ECCTL1 */

#define CSL_ECAP_ECCTL1_FREESOFT_MASK (0xC000u)
#define CSL_ECAP_ECCTL1_FREESOFT_SHIFT (0x000Eu)
#define CSL_ECAP_ECCTL1_FREESOFT_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_FREESOFT_STOP (0x0000u)
#define CSL_ECAP_ECCTL1_FREESOFT_RUNUNTIL0 (0x0001u)
#define CSL_ECAP_ECCTL1_FREESOFT_RUNFREE (0x0002u)
#define CSL_ECAP_ECCTL1_FREESOFT_RUNFREE1 (0x0003u)

#define CSL_ECAP_ECCTL1_PRESCALE_MASK (0x3E00u)
#define CSL_ECAP_ECCTL1_PRESCALE_SHIFT (0x0009u)
#define CSL_ECAP_ECCTL1_PRESCALE_RESETVAL (0x0000u)

#define CSL_ECAP_ECCTL1_CAPLDEN_MASK (0x0100u)
#define CSL_ECAP_ECCTL1_CAPLDEN_SHIFT (0x0008u)
#define CSL_ECAP_ECCTL1_CAPLDEN_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CAPLDEN_DISABLE (0x0000u)
#define CSL_ECAP_ECCTL1_CAPLDEN_ENABLE (0x0001u)

#define CSL_ECAP_ECCTL1_CTRRST4_MASK (0x0080u)
#define CSL_ECAP_ECCTL1_CTRRST4_SHIFT (0x0007u)
#define CSL_ECAP_ECCTL1_CTRRST4_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST4_NORESET (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST4_RESET (0x0001u)

#define CSL_ECAP_ECCTL1_CAP4POL_MASK (0x0040u)
#define CSL_ECAP_ECCTL1_CAP4POL_SHIFT (0x0006u)
#define CSL_ECAP_ECCTL1_CAP4POL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CAP4POL_RISINGEDGE (0x0000u)
#define CSL_ECAP_ECCTL1_CAP4POL_FALLINGEDGE (0x0001u)

#define CSL_ECAP_ECCTL1_CTRRST3_MASK (0x0020u)
#define CSL_ECAP_ECCTL1_CTRRST3_SHIFT (0x0005u)
#define CSL_ECAP_ECCTL1_CTRRST3_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST3_NORESET (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST3_RESET (0x0001u)

#define CSL_ECAP_ECCTL1_CAP3POL_MASK (0x0010u)
#define CSL_ECAP_ECCTL1_CAP3POL_SHIFT (0x0004u)
#define CSL_ECAP_ECCTL1_CAP3POL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CAP3POL_RISINGEDGE (0x0000u)
#define CSL_ECAP_ECCTL1_CAP3POL_FALLINGEDGE (0x0001u)

#define CSL_ECAP_ECCTL1_CTRRST2_MASK (0x0008u)
#define CSL_ECAP_ECCTL1_CTRRST2_SHIFT (0x0003u)
#define CSL_ECAP_ECCTL1_CTRRST2_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST2_NORESET (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST2_RESET (0x0001u)

#define CSL_ECAP_ECCTL1_CAP2POL_MASK (0x0004u)
#define CSL_ECAP_ECCTL1_CAP2POL_SHIFT (0x0002u)
#define CSL_ECAP_ECCTL1_CAP2POL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CAP2POL_RISINGEDGE (0x0000u)
#define CSL_ECAP_ECCTL1_CAP2POL_FALLINGEDGE (0x0001u)

#define CSL_ECAP_ECCTL1_CTRRST1_MASK (0x0002u)
#define CSL_ECAP_ECCTL1_CTRRST1_SHIFT (0x0001u)
#define CSL_ECAP_ECCTL1_CTRRST1_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST1_NORESET (0x0000u)
#define CSL_ECAP_ECCTL1_CTRRST1_RESET (0x0001u)

#define CSL_ECAP_ECCTL1_CAP1POL_MASK (0x0001u)
#define CSL_ECAP_ECCTL1_CAP1POL_SHIFT (0x0000u)
#define CSL_ECAP_ECCTL1_CAP1POL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL1_CAP1POL_RISINGEDGE (0x0000u)
#define CSL_ECAP_ECCTL1_CAP1POL_FALLINGEDGE (0x0001u)

#define CSL_ECAP_ECCTL1_RESETVAL (0x0000u)

/* ECCTL2 */


#define CSL_ECAP_ECCTL2_APWMPOL_MASK (0x0400u)
#define CSL_ECAP_ECCTL2_APWMPOL_SHIFT (0x000Au)
#define CSL_ECAP_ECCTL2_APWMPOL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_APWMPOL_ACTIVEHIGH (0x0000u)
#define CSL_ECAP_ECCTL2_APWMPOL_ACTIVELOW (0x0001u)

#define CSL_ECAP_ECCTL2_CAPAPWM_MASK (0x0200u)
#define CSL_ECAP_ECCTL2_CAPAPWM_SHIFT (0x0009u)
#define CSL_ECAP_ECCTL2_CAPAPWM_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_CAPAPWM_CAPMODE (0x0000u)
#define CSL_ECAP_ECCTL2_CAPAPWM_APWMMODE (0x0001u)

#define CSL_ECAP_ECCTL2_SWSYNC_MASK (0x0100u)
#define CSL_ECAP_ECCTL2_SWSYNC_SHIFT (0x0008u)
#define CSL_ECAP_ECCTL2_SWSYNC_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_SWSYNC_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCTL2_SWSYNC_TSCTRLOAD (0x0001u)

#define CSL_ECAP_ECCTL2_SYNCOSEL_MASK (0x00C0u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_SHIFT (0x0006u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_SYNCIN (0x0000u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_CTREQUALPRD (0x0001u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_DISABLE (0x0002u)
#define CSL_ECAP_ECCTL2_SYNCOSEL_DISABLE1 (0x0003u)

#define CSL_ECAP_ECCTL2_SYNCIEN_MASK (0x0020u)
#define CSL_ECAP_ECCTL2_SYNCIEN_SHIFT (0x0005u)
#define CSL_ECAP_ECCTL2_SYNCIEN_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_SYNCIEN_DISABLE (0x0000u)
#define CSL_ECAP_ECCTL2_SYNCIEN_ENABLE (0x0001u)

#define CSL_ECAP_ECCTL2_TSCTRSTOP_MASK (0x0010u)
#define CSL_ECAP_ECCTL2_TSCTRSTOP_SHIFT (0x0004u)
#define CSL_ECAP_ECCTL2_TSCTRSTOP_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_TSCTRSTOP_STOPPED (0x0000u)
#define CSL_ECAP_ECCTL2_TSCTRSTOP_FREERUNNING (0x0001u)

#define CSL_ECAP_ECCTL2_REARM_MASK (0x0008u)
#define CSL_ECAP_ECCTL2_REARM_SHIFT (0x0003u)
#define CSL_ECAP_ECCTL2_REARM_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_REARM_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCTL2_REARM_ARMED (0x0001u)

#define CSL_ECAP_ECCTL2_STOPWRAP_MASK (0x0006u)
#define CSL_ECAP_ECCTL2_STOPWRAP_SHIFT (0x0001u)
#define CSL_ECAP_ECCTL2_STOPWRAP_RESETVAL (0x0003u)
#define CSL_ECAP_ECCTL2_STOPWRAP_CAP1 (0x0000u)
#define CSL_ECAP_ECCTL2_STOPWRAP_CAP2 (0x0001u)
#define CSL_ECAP_ECCTL2_STOPWRAP_CAP3 (0x0002u)
#define CSL_ECAP_ECCTL2_STOPWRAP_CAP4 (0x0003u)

#define CSL_ECAP_ECCTL2_CONTONESHT_MASK (0x0001u)
#define CSL_ECAP_ECCTL2_CONTONESHT_SHIFT (0x0000u)
#define CSL_ECAP_ECCTL2_CONTONESHT_RESETVAL (0x0000u)
#define CSL_ECAP_ECCTL2_CONTONESHT_CONTINUOUS (0x0000u)
#define CSL_ECAP_ECCTL2_CONTONESHT_ONESHOT (0x0001u)

#define CSL_ECAP_ECCTL2_RESETVAL (0x0006u)

/* ECEINT */


#define CSL_ECAP_ECEINT_CTREQUCMP_MASK (0x0080u)
#define CSL_ECAP_ECEINT_CTREQUCMP_SHIFT (0x0007u)
#define CSL_ECAP_ECEINT_CTREQUCMP_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CTREQUCMP_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CTREQUCMP_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CTREQUPRD_MASK (0x0040u)
#define CSL_ECAP_ECEINT_CTREQUPRD_SHIFT (0x0006u)
#define CSL_ECAP_ECEINT_CTREQUPRD_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CTREQUPRD_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CTREQUPRD_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CTROVF_MASK (0x0020u)
#define CSL_ECAP_ECEINT_CTROVF_SHIFT (0x0005u)
#define CSL_ECAP_ECEINT_CTROVF_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CTROVF_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CTROVF_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CEVT4_MASK (0x0010u)
#define CSL_ECAP_ECEINT_CEVT4_SHIFT (0x0004u)
#define CSL_ECAP_ECEINT_CEVT4_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CEVT4_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CEVT4_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CEVT3_MASK (0x0008u)
#define CSL_ECAP_ECEINT_CEVT3_SHIFT (0x0003u)
#define CSL_ECAP_ECEINT_CEVT3_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CEVT3_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CEVT3_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CEVT2_MASK (0x0004u)
#define CSL_ECAP_ECEINT_CEVT2_SHIFT (0x0002u)
#define CSL_ECAP_ECEINT_CEVT2_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CEVT2_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CEVT2_ENABLE (0x0001u)

#define CSL_ECAP_ECEINT_CEVT1_MASK (0x0002u)
#define CSL_ECAP_ECEINT_CEVT1_SHIFT (0x0001u)
#define CSL_ECAP_ECEINT_CEVT1_RESETVAL (0x0000u)
#define CSL_ECAP_ECEINT_CEVT1_DISABLE (0x0000u)
#define CSL_ECAP_ECEINT_CEVT1_ENABLE (0x0001u)


#define CSL_ECAP_ECEINT_RESETVAL (0x0000u)

/* ECEFLG */


#define CSL_ECAP_ECEFLG_CTREQUCMP_MASK (0x0080u)
#define CSL_ECAP_ECEFLG_CTREQUCMP_SHIFT (0x0007u)
#define CSL_ECAP_ECEFLG_CTREQUCMP_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CTREQUCMP_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CTREQUCMP_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CTREQUPRD_MASK (0x0040u)
#define CSL_ECAP_ECEFLG_CTREQUPRD_SHIFT (0x0006u)
#define CSL_ECAP_ECEFLG_CTREQUPRD_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CTREQUPRD_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CTREQUPRD_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CTROVF_MASK (0x0020u)
#define CSL_ECAP_ECEFLG_CTROVF_SHIFT (0x0005u)
#define CSL_ECAP_ECEFLG_CTROVF_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CTROVF_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CTROVF_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CEVT4_MASK (0x0010u)
#define CSL_ECAP_ECEFLG_CEVT4_SHIFT (0x0004u)
#define CSL_ECAP_ECEFLG_CEVT4_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT4_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT4_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CEVT3_MASK (0x0008u)
#define CSL_ECAP_ECEFLG_CEVT3_SHIFT (0x0003u)
#define CSL_ECAP_ECEFLG_CEVT3_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT3_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT3_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CEVT2_MASK (0x0004u)
#define CSL_ECAP_ECEFLG_CEVT2_SHIFT (0x0002u)
#define CSL_ECAP_ECEFLG_CEVT2_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT2_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT2_INT (0x0001u)

#define CSL_ECAP_ECEFLG_CEVT1_MASK (0x0002u)
#define CSL_ECAP_ECEFLG_CEVT1_SHIFT (0x0001u)
#define CSL_ECAP_ECEFLG_CEVT1_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT1_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_CEVT1_INT (0x0001u)

#define CSL_ECAP_ECEFLG_INT_MASK (0x0001u)
#define CSL_ECAP_ECEFLG_INT_SHIFT (0x0000u)
#define CSL_ECAP_ECEFLG_INT_RESETVAL (0x0000u)
#define CSL_ECAP_ECEFLG_INT_NOINT (0x0000u)
#define CSL_ECAP_ECEFLG_INT_INT (0x0001u)

#define CSL_ECAP_ECEFLG_RESETVAL (0x0000u)

/* ECCLR */


#define CSL_ECAP_ECCLR_CTREQUCMP_MASK (0x0080u)
#define CSL_ECAP_ECCLR_CTREQUCMP_SHIFT (0x0007u)
#define CSL_ECAP_ECCLR_CTREQUCMP_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CTREQUCMP_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CTREQUCMP_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CTREQUPRD_MASK (0x0040u)
#define CSL_ECAP_ECCLR_CTREQUPRD_SHIFT (0x0006u)
#define CSL_ECAP_ECCLR_CTREQUPRD_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CTREQUPRD_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CTREQUPRD_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CTROVF_MASK (0x0020u)
#define CSL_ECAP_ECCLR_CTROVF_SHIFT (0x0005u)
#define CSL_ECAP_ECCLR_CTROVF_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CTROVF_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CTROVF_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CEVT4_MASK (0x0010u)
#define CSL_ECAP_ECCLR_CEVT4_SHIFT (0x0004u)
#define CSL_ECAP_ECCLR_CEVT4_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CEVT4_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CEVT4_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CEVT3_MASK (0x0008u)
#define CSL_ECAP_ECCLR_CEVT3_SHIFT (0x0003u)
#define CSL_ECAP_ECCLR_CEVT3_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CEVT3_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CEVT3_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CEVT2_MASK (0x0004u)
#define CSL_ECAP_ECCLR_CEVT2_SHIFT (0x0002u)
#define CSL_ECAP_ECCLR_CEVT2_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CEVT2_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CEVT2_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_CEVT1_MASK (0x0002u)
#define CSL_ECAP_ECCLR_CEVT1_SHIFT (0x0001u)
#define CSL_ECAP_ECCLR_CEVT1_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_CEVT1_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_CEVT1_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_INT_MASK (0x0001u)
#define CSL_ECAP_ECCLR_INT_SHIFT (0x0000u)
#define CSL_ECAP_ECCLR_INT_RESETVAL (0x0000u)
#define CSL_ECAP_ECCLR_INT_NOEFFECT (0x0000u)
#define CSL_ECAP_ECCLR_INT_CLEAR (0x0001u)

#define CSL_ECAP_ECCLR_RESETVAL (0x0000u)

/* ECFRC */


#define CSL_ECAP_ECFRC_CTREQUCMP_MASK (0x0080u)
#define CSL_ECAP_ECFRC_CTREQUCMP_SHIFT (0x0007u)
#define CSL_ECAP_ECFRC_CTREQUCMP_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CTREQUCMP_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CTREQUCMP_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CTREQUPRD_MASK (0x0040u)
#define CSL_ECAP_ECFRC_CTREQUPRD_SHIFT (0x0006u)
#define CSL_ECAP_ECFRC_CTREQUPRD_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CTREQUPRD_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CTREQUPRD_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CTROVF_MASK (0x0020u)
#define CSL_ECAP_ECFRC_CTROVF_SHIFT (0x0005u)
#define CSL_ECAP_ECFRC_CTROVF_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CTROVF_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CTROVF_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CEVT4_MASK (0x0010u)
#define CSL_ECAP_ECFRC_CEVT4_SHIFT (0x0004u)
#define CSL_ECAP_ECFRC_CEVT4_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CEVT4_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CEVT4_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CEVT3_MASK (0x0008u)
#define CSL_ECAP_ECFRC_CEVT3_SHIFT (0x0003u)
#define CSL_ECAP_ECFRC_CEVT3_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CEVT3_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CEVT3_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CEVT2_MASK (0x0004u)
#define CSL_ECAP_ECFRC_CEVT2_SHIFT (0x0002u)
#define CSL_ECAP_ECFRC_CEVT2_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CEVT2_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CEVT2_FORCE (0x0001u)

#define CSL_ECAP_ECFRC_CEVT1_MASK (0x0002u)
#define CSL_ECAP_ECFRC_CEVT1_SHIFT (0x0001u)
#define CSL_ECAP_ECFRC_CEVT1_RESETVAL (0x0000u)
#define CSL_ECAP_ECFRC_CEVT1_NOEFFECT (0x0000u)
#define CSL_ECAP_ECFRC_CEVT1_FORCE (0x0001u)


#define CSL_ECAP_ECFRC_RESETVAL (0x0000u)

#endif
