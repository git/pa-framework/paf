
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _ICE_1_H_
#define _ICE_1_H_
/*********************************************************************
 * Copyright (C) 2003-2004 Texas Instruments Incorporated.
 * All Rights Reserved
 *********************************************************************/

#include <cslr.h>
#include <tistdtypes.h>

/**************************************************************************\
* Register Overlay Structure
\**************************************************************************/
typedef struct  {
    volatile Uint32 VERSION;			/*Version*/
    volatile Uint32 DEBUG_CONFIG_CTRL;	/*Debug Configuration*/
    volatile Uint32 DEBUG_CTRL_STAT; 	/*Debug Control and Status Register*/
    volatile Uint8 RSVD0[4];			/*Reserved*/
    volatile Uint32 TCR;				/*Trigger Control Register*/
    volatile Uint32 RCSR;				/*Reset Control and Status Register*/
    volatile Uint32 TIDC;				/*Thread ID Claim*/
    volatile Uint32 TID;				/*Thread ID*/
    volatile Uint32 IC_STAT;			/*Interrupt Control and Status*/
    volatile Uint32 ETM_CTRL_STAT;      /*ETM Control and Status Register*/
    volatile Uint32 ETM_PROC_ID;		/*ETM Processor ID*/
    volatile Uint32 TEST;				/*Test Register*/
    volatile Uint8 RSVD1[16];    		/*Reserved*/
    volatile Uint32 CNT0_CTRL_STAT;		/*Benchmark Counter 0 Control and Status*/
    volatile Uint32 CNT0;				/*Benchmart Counter 0*/
    volatile Uint8 RSVD2[8];			/*Reserved*/
    volatile Uint32 CNT1_CTRL_STAT;		/*Benchmark Counter 1 Control and Status*/
    volatile Uint32 CNT1;				/*Benchmart Counter 1*/    
    volatile Uint8 RSVD3[40];			/*Reserved*/    
    volatile Uint32 BRK_PNT0_CTRL;      /*Hardware Breakpoint 0 Control*/
    volatile Uint32 BRK_PNT0_ADDR;      /*Hardware Breakpoint 0 Address*/
    volatile Uint32 BRK_PNT0_ADDR_MARK; /*Hardware Breakpoint 0 Address Mark*/
    volatile Uint8 RSVD4[4];			/*Reserved*/
    volatile Uint32 BRK_PNT1_CTRL;      /*Hardware Breakpoint 1 Control*/
    volatile Uint32 BRK_PNT1_ADDR;      /*Hardware Breakpoint 1 Address*/
    volatile Uint32 BRK_PNT1_ADDR_MARK; /*Hardware Breakpoint 1 Address Mark*/
    volatile Uint8 RSVD5[4];			/*Reserved*/    
    volatile Uint32 BRK_PNT2_CTRL;      /*Hardware Breakpoint 2 Control*/
    volatile Uint32 BRK_PNT2_ADDR;      /*Hardware Breakpoint 2 Address*/
    volatile Uint32 BRK_PNT2_ADDR_MARK; /*Hardware Breakpoint 2 Address Mark*/
    volatile Uint8 RSVD6[4];			/*Reserved*/    
    volatile Uint32 BRK_PNT3_CTRL;      /*Hardware Breakpoint 3 Control*/
    volatile Uint32 BRK_PNT3_ADDR;      /*Hardware Breakpoint 3 Address*/
    volatile Uint32 BRK_PNT3_ADDR_MARK; /*Hardware Breakpoint 3 Address Mark*/
    volatile Uint8 RSVD7[64];			/*Reserved*/    
    volatile Uint32 DCON;				/*DCON Register*/
} CSL_IceRegs;

#endif
