
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _PBIST_1_H_
#define _PBIST_1_H_
/**************************************************************************\
* Register Overlay Structure
\**************************************************************************/
typedef struct  {
    volatile Uint32 IR[64]; // 0x00
    volatile Uint32 VAR[4];     // 0x100
    volatile Uint32 VLCR[4]; // 0x110
    volatile Uint32 DR[2];     // 0x120
    volatile Uint32 RSVD1[2];     // 0x128
    volatile Uint32 CAR[4];     // 0x130
    volatile Uint32 CLCR[4];     // 0x140
    volatile Uint32 CIR[4];     // 0x150
    volatile Uint32 RAMCFG;     // 0x160
    volatile Uint32 DLR;     // 0x164
    volatile Uint32 CMS;     // 0x168
    volatile Uint32 STR;     // 0x16c
    volatile Uint32 SCR[2];     // 0x170
    volatile Uint32 CSR;     // 0x178
    volatile Uint32 FDLY;     // 0x17c
    volatile Uint32 PACT;     // 0x180
    volatile Uint32 PBID;     // 0x184
    volatile Uint32 PBOVR;     // 0x188
    volatile Uint32 RSVD2;     // 0x18c
    volatile Uint32 FSRF[2];     // 0x190
    volatile Uint32 FSRC[2];     // 0x198
    volatile Uint32 FSRA[2];     // 0x1a0
    volatile Uint32 FSRDL0;     // 0x1a8
    volatile Uint32 RSVD3;     // 0x1ac
    volatile Uint32 FSRDL1;     // 0x1b0
    volatile Uint32 RSVD4[3];     // 0x1b4
    volatile Uint32 ROM;    // 0x1c0
    volatile Uint32 ALGO;    // 0x1c4
    volatile Uint32 RINFOL; // 0x1c8
    volatile Uint32 RINFOU; // 0x1cc
    volatile Uint32 RSVD5[12];     // 0x1d0 - 1fc
} CSL_PbistRegs;

/**************************************************************************\
* Field Definition Macros
\**************************************************************************/

#endif

