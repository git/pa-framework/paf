
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha File Processing via DSPLINK
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <std.h>
#include <sys.h>
#include <sem.h>
#include <log.h>
#include <tsk.h>

#include <dsplink.h>
#include <dsplinkmsgq.h>
#include <failure.h>
#include <msgq.h>
#include <pool.h>
#include <platform.h>
#include <ringio.h>
#include <sma_pool.h>
#include <zcpy_mqt.h>

#include <acp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>
#include "afp_link.h"
#include <dri_ringio.h>
#include <dro_ringio.h>


// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

#define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif



// A note on #defines controlling ring IO:
//  #ifdef DSPLINK:  Required as all ringIO is based on DSPLINK.
//  #ifdef ENABLE_DSP_DSP_RINGIO:  Set if DRI and DRO are to be connected to each other.
//  #ifdef ENABLE_GPP_DRI:  Set if DRI will be used to receive data from the GPP.
//  #ifdef ENABLE_GPP_DRO:  Set if DRO will be used to send data to the GPP.

#ifdef ENABLE_DSP_DSP_RINGIO
  #include <ztop.h>   // defines PAF buffer sizes
  #include <DRIO.h>   // defines DRI-DRO link buffer sizes
#else  // nothing has been set, default to legacy.
  #define ENABLE_GPP_DRI
  #define ENABLE_GPP_DRO
#endif

#define DSPLINK_DSP_ONLY_MODE (defined(DSPLINK) && (!defined(ENABLE_GPP_DRI) || !defined(ENABLE_GPP_DRO) ) )

  // connect with ARM
  RingIO_Handle PA_DSPLINK_readerHandle = NULL;
  RingIO_Handle PA_DSPLINK_writerHandle = NULL;

  // DSP-DSP, two channels
  RingIO_Handle DSP_DSPLINK_readerHandle_0 = NULL;
  RingIO_Handle DSP_DSPLINK_writerHandle_0 = NULL;
  RingIO_Handle DSP_DSPLINK_readerHandle_1 = NULL;
  RingIO_Handle DSP_DSPLINK_writerHandle_1 = NULL;


#define DSP_DSP_RING_A_NAME  "DspDspRingA"
#ifndef ALL_SYNCHRONOUS_ZSS
  #define DSP_DSP_RING_B_NAME  "DspDspRingB"
#endif // ALL_SYNCHRONOUS_ZSS
#define SMA_POOL_ID         0
#define DSP_RING_POOL_ID    0   // probably need more than one pool to coexist with GPP modes.


Int setup_DRIO_RingIO(char *ringName, RingIO_Handle *readerHandle, RingIO_Handle *writerHandle)
{
    Int             status = SYS_OK;
    RingIO_Attrs    ringIoAttrs;

    // the two GPP based rings are opened separately.
    assert(readerHandle != NULL);
    assert(writerHandle != NULL);

    // Create the ring used by DRI and DRO for DSP-DSP communication
    ringIoAttrs.transportType  = RINGIO_TRANSPORT_DSP_DSP ;
    ringIoAttrs.ctrlPoolId     = DSP_RING_POOL_ID ;
    ringIoAttrs.dataPoolId     = DSP_RING_POOL_ID ;
    ringIoAttrs.attrPoolId     = DSP_RING_POOL_ID ;
    ringIoAttrs.lockPoolId     = DSP_RING_POOL_ID ;

    #define BUFFER_DEPTH   16
    if (!strcmp(DSP_DSP_RING_A_NAME, ringName)) 
    {
        ringIoAttrs.dataBufSize    = DRIO_A_ONE_BUFFER * BUFFER_DEPTH;
        ringIoAttrs.footBufSize    = DRIO_A_ONE_BUFFER;    //
        TRACE((&trace, "%s.%d:  Setup DRIO_A, buffer size %d.",
               __FUNCTION__, __LINE__, DRIO_A_ONE_BUFFER));
        TRACE((&trace, "setup_DRIO_RingIO:  Setup DRIO_A, %d by %d is %d.",
               BUFFER_DEPTH+1, DRIO_A_ONE_BUFFER, (BUFFER_DEPTH+1)*DRIO_A_ONE_BUFFER));
    }
#ifndef ALL_SYNCHRONOUS_ZSS
    else
    {
        ringIoAttrs.dataBufSize    = DRIO_B_ONE_BUFFER*BUFFER_DEPTH;
        ringIoAttrs.footBufSize    = DRIO_B_ONE_BUFFER;    //
        TRACE((&trace, "%s.%d:  Setup DRIO_B, buffer size %d.",
               __FUNCTION__, __LINE__, DRIO_B_ONE_BUFFER));
        TRACE((&trace, "setup_DRIO_RingIO:  Setup DRIO_B, %d by %d is %d.",
               BUFFER_DEPTH+1, DRIO_A_ONE_BUFFER, (BUFFER_DEPTH+1)*DRIO_A_ONE_BUFFER));
    }
#endif // ALL_SYNCHRONOUS_ZSS
    ringIoAttrs.attrBufSize    = 256;  // RING_IO_attrBufSize ;
    status = RingIO_create (GBL_getProcId (), ringName, &ringIoAttrs) ;
    if (status != SYS_OK)
    {
        TRACE((&trace, "%s.%d:  RingIO_create failed with 0x%x.",
               __FUNCTION__, __LINE__, status));
        printf("\n  !%s.%d:  RingIO_create failed with 0x%x\n",
               __FUNCTION__, __LINE__, status);
        return status;
    }

    *writerHandle = RingIO_open (ringName, RINGIO_MODE_WRITE, 0);
    if (!*writerHandle)
    {
        TRACE((&trace, "%s.%d:  RingIO_open for writer failed.",
               __FUNCTION__, __LINE__));
        printf("\n  !%s.%d:  RingIO_open for writer failed.\n",
               __FUNCTION__, __LINE__);
        RingIO_delete (GBL_getProcId (), ringName) ;
        return RINGIO_EFAILURE;
    }
    *readerHandle = RingIO_open (ringName, RINGIO_MODE_READ, 0);
    if (!*readerHandle)
    {
        printf("\n  !%s.%d:  RingIO_open for reader failed.\n",
               __FUNCTION__, __LINE__);
        RingIO_delete (GBL_getProcId (), ringName) ;
        RingIO_delete (GBL_getProcId (), ringName) ;
        return RINGIO_EFAILURE;
    }
    // printf("%s ringio handles are available.\n", ringName);
    TRACE((&trace, "%s ringio handles are available.", ringName));

    return RINGIO_SUCCESS;
}


 // -----------------------------------------------------------------------------
#if !DSPLINK_DSP_ONLY_MODE

SMAPOOL_Params SamplePoolParams = {SMA_POOL_ID, TRUE};

static MSGQ_Obj msgQueues [NUM_MSG_QUEUES];

MSGQ_TransportObj transports [MAX_PROCESSORS] =
{
    MSGQ_NOTRANSPORT,
    MSGQ_NOTRANSPORT
};

MSGQ_Config MSGQ_config =
{
    msgQueues,
    transports,
    NUM_MSG_QUEUES,
    MAX_PROCESSORS,
    0,
    MSGQ_INVALIDMSGQ,
    POOL_INVALIDID
};

static POOL_Obj pools [NUM_POOLS] = {POOL_NOENTRY};

POOL_Config POOL_config = {pools, NUM_POOLS};

SMAPOOL_Params  smaPoolObj;
ZCPYMQT_Params  mqtParams;

volatile unsigned int DSPLINK_initFlag = 0xBABAC0C0;
Bool PA_DSPLINK_isInitialized = FALSE;


// When built using LINK Task mode for communication synchronization then
// grab reference for MQT ctrl. This is modified below to adjust
// the thread priority and stack.
#ifdef DSP_TSK_MODE
 #include <dsplinkmqt.h>
 extern DSPLINKMQT_Ctrl * DSPLINKMQT_ctrlPtr;
#endif


static ACP_Unit rxBuf[PA_LINK_MAX_CODE_LEN/sizeof(ACP_Unit)];
static ACP_Unit txBuf[PA_LINK_MAX_CODE_LEN/sizeof(ACP_Unit)];

Int setup_hosted_RingIO(char *ringName, RingIO_Handle *readerHandle, RingIO_Handle *writerHandle);


void afpLinkTask (void)
{
    Int                          status = SYS_OK;
    MSGQ_Attrs                msgqAttrs = MSGQ_ATTRS;
    MSGQ_LocateAttrs    syncLocateAttrs;
    MSGQ_TransportObj         transport;
    POOL_Obj                    poolObj;

    PA_LINK_Msg         *pMsg;
    AFP_LINK_Obj         afpLinkObj;
    ACP_Handle           acp;
    IALG_Status         *pStatus;

    // .........................................................................
    // Init ACP

    ACP_MDS_init ();
    acp = ACP_create (&ACP_MDS_IACP, &ACP_PARAMS);
    if (!acp)
        return;

    ((ALG_Handle)acp)->fxns->algControl((IALG_Handle)(acp),
                                        ACP_GETSTATUSADDRESS1,
                                        (IALG_Status *)&pStatus);

    acp->fxns->attach (acp,
                       ACP_SERIES_STD,
                       STD_BETA_UART,
                       pStatus);

    acp->fxns->attach (acp,
                       ACP_SERIES_STD,
                       STD_BETA_BETATABLE,
                       (IALG_Status *)((ACP_MDS_Obj *)acp)->config.betaTable[ACP_SERIES_STD]);

    acp->fxns->attach (acp,
                       ACP_SERIES_STD,
                       STD_BETA_PHITABLE,
                       (IALG_Status *)((ACP_MDS_Obj *)acp)->config.phiTable[ACP_SERIES_STD]);

    acp->fxns->attach (acp,
                       ACP_SERIES_STD,
                       STD_BETA_SIGMATABLE,
                       (IALG_Status *)((ACP_MDS_Obj *)acp)->config.sigmaTable[ACP_SERIES_STD]);

    // Set the semaphore to a known state.
    SEM_new (&afpLinkObj.notifySemObj, 0);

#ifdef PA_NO_BOOT
    // .........................................................................
    // General DSP/LINK Initialization

    printf("afp_link: Waiting for host.\n");
    // Wait for ARM signal before initializing communications
    while (DSPLINK_initFlag != 0xC0C0BABA)
        TSK_sleep (1);
    DSPLINK_init ();
    PA_DSPLINK_isInitialized = TRUE;
    printf("afp_link: Host contacted DSP.\n");
#endif

    // .........................................................................
    // POOL Initialization

    smaPoolObj.poolId        = 0;
    smaPoolObj.exactMatchReq = TRUE;
    poolObj.initFxn          = SMAPOOL_init;
    poolObj.fxns             = (POOL_Fxns *) &SMAPOOL_FXNS;
    poolObj.params           = &(smaPoolObj);
    poolObj.object           = NULL;

    status = POOL_open (0, &poolObj);
    if (status != SYS_OK) {
        SET_FAILURE_REASON (status);
        return;
    }

    // open the hosted ring and reverse ring.
    setup_hosted_RingIO(RING_IO_NAME, &PA_DSPLINK_readerHandle, NULL);
    setup_hosted_RingIO(RRING_IO_NAME, NULL, &PA_DSPLINK_writerHandle);

    #ifdef ENABLE_DSP_DSP_RINGIO
    // to do:  Set up the necessary memory pool here.
    #error DSP-DSP Ring IO not supported yet when GPP is involved.
    setup_DRIO_RingIO(DSP_DSP_RING_A_NAME, &DSP_DSPLINK_readerHandle_0, &DSP_DSPLINK_writerHandle_0);
#ifndef ALL_SYNCHRONOUS_ZSS
    setup_DRIO_RingIO(DSP_DSP_RING_B_NAME, &DSP_DSPLINK_readerHandle_1, &DSP_DSPLINK_writerHandle_1);
#endif // ALL_SYNCHRONOUS_ZSS
    #endif


    // .........................................................................
    // MSGQ Initialization

    // If needed adjust dynamically created thread with different priority and
    // stack. If it is non-zero then assume it was adjusted at LINK library
    // build time and don't modifiy.
#ifdef DSP_TSK_MODE
    if (DSPLINKMQT_ctrlPtr->config.arg1 == 0)
        DSPLINKMQT_ctrlPtr->config.arg1 = 9;

    if (DSPLINKMQT_ctrlPtr->config.arg2 == 0)
        DSPLINKMQT_ctrlPtr->config.arg2 = 0x800;
#endif

    mqtParams.poolId  = 0;
    transport.initFxn = SAMPLEMQT_init;          
    transport.fxns    = (MSGQ_TransportFxns *) &SAMPLEMQT_FXNS;
    transport.params  = &mqtParams;
    transport.object  = NULL;
    transport.procId  = ID_GPP;
    
    status = MSGQ_transportOpen (ID_GPP, &transport);
    if (status != SYS_OK) {
        SET_FAILURE_REASON (status);
        return;
    }

    // Fill in the attributes for this message queue.
    msgqAttrs.notifyHandle = &afpLinkObj.notifySemObj;
    msgqAttrs.pend         = (MSGQ_Pend) SEM_pendBinary;
    msgqAttrs.post         = (MSGQ_Post) SEM_postBinary;

    status = MSGQ_open (DSP_MSGQNAME, &afpLinkObj.localMsgq, &msgqAttrs);
    if (status == SYS_OK) {
        // Set the message queue that will receive any async. errors.
        MSGQ_setErrorHandler (afpLinkObj.localMsgq, SMA_POOL_ID);

        // Synchronous
        status = SYS_ENOTFOUND;
        while ((status == SYS_ENOTFOUND) || (status == SYS_ENODEV)) {
            syncLocateAttrs.timeout = SYS_FOREVER;
            status = MSGQ_locate (GPP_MSGQNAME,
                                  &afpLinkObj.locatedMsgq,
                                  &syncLocateAttrs);
            if ((status == SYS_ENOTFOUND) || (status == SYS_ENODEV)) {
                TSK_sleep (1);
            }
            else if (status != SYS_OK) {
                SET_FAILURE_REASON (status);
                return;
            }
        }
    }
    else {
        SET_FAILURE_REASON (status);
        return;
    }

    // .........................................................................
    // Loop waiting for messages from GPP. When present process

    for (;;) {
        Int cnt, i, error, len, res_len;
        Int encap = 0;
        Int rxOffset = 0;
        Int txOffset = 0;
        Int rxIdx = 2;
        Int txIdx = 2;
        Int txStrip = 0;

        status = MSGQ_get (afpLinkObj.localMsgq, (MSGQ_Msg *) &pMsg, SYS_FOREVER);
        if (status != SYS_OK) {
            SET_FAILURE_REASON (status);
            TSK_exit ();
            return;
        }

        // total MSQG payload in bytes (not including first two for count)
        cnt = (pMsg->alfaCode[1] << 8) | pMsg->alfaCode[0];

        // copy, for now, payload to temp buffer -- helps align code with afp5.c
        memcpy (&rxBuf[rxIdx], &pMsg->alfaCode[2], cnt);

        // convert cnt to # of ACP_Units
        cnt /= sizeof (ACP_Unit);

        // if present, strip encapsulation and recompute count
        if ((rxBuf[rxIdx] & 0xFF00) == 0xC900) {
            cnt = rxBuf[rxIdx] & 0x00FF;
            rxIdx++;
            encap = 1;
        }
        else if (rxBuf[rxIdx] == 0xCD01) {
            cnt = rxBuf[rxIdx+1];
            rxIdx +=2;
            encap = 2;
        }

        // process one C-record at a time;
        while (cnt) {

            // check that message is really alpha code
            // if not then stop all processing since the length
            // computation is not meaningful and therefore we are not
            // able to skip to the next possible valid code
            if ((rxBuf[rxIdx] & 0xC000) != 0xC000) {
                txBuf[2] = 0xdead;
                txIdx=3;
                TRACE((&trace, "afpLinkTask.%d: 0xDEAD: alpha error.", __LINE__));
                break;
            }

            // get length of this C-record and it's response length
            len = acp->fxns->length (acp, (ACP_Unit *) &rxBuf[rxIdx]);
            res_len = acp->fxns->htgnel (acp, (ACP_Unit *) &rxBuf[rxIdx]);

            // check that response length will fit in message            
            if (res_len > (PA_LINK_MAX_CODE_LEN/sizeof(ACP_Unit) - txIdx)) {
                txBuf[2] = 0xdead;
                txIdx=3;
                TRACE((&trace, "afpLinkTask.%d: 0xDEAD: alpha error.", __LINE__));
                break;
            }

            // add encapuslation for this C-record (needed by ACP)
            if (len < 256) {
                rxBuf[--rxIdx] = 0xC900 + len;
                rxOffset = 1;
            }
            else {
                rxBuf[--rxIdx] = len;
                rxBuf[--rxIdx] = 0xCD01;
                rxOffset = 2;
            }

            error = acp->fxns->sequence (acp, &rxBuf[rxIdx], &txBuf[txIdx]);
            if (error) {
                if (res_len || encap) {
                    txBuf[txIdx++] = 0xDEAD;
                    TRACE((&trace, "afpLinkTask.%d: 0xDEAD: alpha error.", __LINE__));
                }
                cnt -= len;
                rxIdx += len + rxOffset;
                continue;
            }
            else {
                if ((txBuf[txIdx] & 0xFF00) == 0xC900)
                    txStrip = 1;
                else if (txBuf[txIdx] == 0xCD01)
                    txStrip = 2;
                for (i=0; i < res_len; ++i)
                    txBuf[txIdx+i] = txBuf[txIdx+i+txStrip];
            }

            cnt   -= len;
            rxIdx += len + rxOffset;
            txIdx += res_len;
        } //while (cnt)

        // If input is encapsulated then encapsulate the output
        if (encap) {
            if (txIdx < 258) {
                pMsg->alfaCode[2] = (txIdx - 2);
                pMsg->alfaCode[3] = 0xC9;
                txOffset = 1;
            }
            else {
                // TODO;
                txOffset = 2;
            }
        } //encap

        cnt = 2*(txIdx-2+txOffset);
        pMsg->alfaCode[0] = cnt & 0xFF;
        pMsg->alfaCode[1] = (cnt & 0xFF00) >> 8;
        memcpy (&pMsg->alfaCode[2+2*txOffset], &txBuf[2], 2*txIdx);

        status = MSGQ_put (afpLinkObj.locatedMsgq, (MSGQ_Msg) pMsg);
        if (status != SYS_OK) {
            SET_FAILURE_REASON (status);
            return;
        }

    } //for (;;)

    // .........................................................................
    // TODO: perform MSGQ cleanup on exit

    // return;
} //afpLinkTask

#ifdef PA_NO_BOOT
// -----------------------------------------------------------------------------
// ISR which is executed to tell DSP, now it is time to rum DSPLINK_init
void AFP_LINK_initIsr (Ptr arg)
{
    volatile Uint32 * chipsig_clr = (Uint32 *) 0x1c14178u;

    (Void) arg ;
    /* Set the INIT Flag */
    DSPLINK_initFlag = 0xC0C0BABA;
    *chipsig_clr = 0x4;

} // AFP_LINK_initIsr
#endif



Int setup_hosted_RingIO(char *ringName, RingIO_Handle *readerHandle, RingIO_Handle *writerHandle)
{
    Int             status = SYS_OK;
    RingIO_Attrs    ringIoAttrs;
    Uint32          flags;

    // the two GPP based rings are opened separately.
    ringIoAttrs.transportType  = RINGIO_TRANSPORT_GPP_DSP;
    ringIoAttrs.ctrlPoolId     = SMA_POOL_ID;
    ringIoAttrs.dataPoolId     = SMA_POOL_ID;
    ringIoAttrs.attrPoolId     = SMA_POOL_ID;
    ringIoAttrs.lockPoolId     = SMA_POOL_ID;
    ringIoAttrs.dataBufSize    = RINGIODATABUFSIZE;
    ringIoAttrs.footBufSize    = 0;
    ringIoAttrs.attrBufSize    = RINGIOATTRBUFSIZE;

    status = RingIO_create (ID_GPP, ringName, &ringIoAttrs);
    if (status != RINGIO_SUCCESS)
    {
        TRACE((&trace, "%s.%d:  RingIO_create failed with 0x%x.", __FUNCTION__, __LINE__, status));
        printf("%s.%d:  RingIO_create failed with 0x%x.\n", __FUNCTION__, __LINE__, status);
        SET_FAILURE_REASON (status);
        return status;
    }

    //  Value of the flags indicates:
    //    Cache coherence required for: Control structure
    //                                  Data buffer
    //                                  Attribute buffer
    //    Exact size requirement false.
    flags = (RINGIO_DATABUF_CACHEUSE | RINGIO_ATTRBUF_CACHEUSE | RINGIO_CONTROL_CACHEUSE);

    if (readerHandle)
    {
        *readerHandle = RingIO_open (ringName, RINGIO_MODE_READ, flags);
        if (!*readerHandle)
        {
            TRACE((&trace, "%s.%d:  RingIO_open failed with 0x%x.", __FUNCTION__, __LINE__, status));
            printf("%s.%d:  RingIO_open failed with 0x%x.\n", __FUNCTION__, __LINE__, status);
            return RINGIO_EFAILURE;
        }
        assert(writerHandle == NULL);  // The GPP opens the writer!
    }
    else if (writerHandle)
    {
        *writerHandle = RingIO_open (RRING_IO_NAME, RINGIO_MODE_WRITE, flags);
        if (!*writerHandle)
        {
            TRACE((&trace, "%s.%d:  RingIO_open failed with 0x%x.", __FUNCTION__, __LINE__, status));
            printf("%s.%d:  RingIO_open failed with 0x%x.\n", __FUNCTION__, __LINE__, status);
            return RINGIO_EFAILURE;
        }
        assert(readerHandle == NULL);  // The GPP opens the reader!
    }
    return RINGIO_SUCCESS;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#else  // the DSP only case:  DSPLINK_DSP_ONLY_MODE is true.

#include <_dsplink.h>
#include <dsplinkips.h>
#include <hal_cache.h>
#include <ti/bios/include/staticpool.h>

#define GPP_HANDSHAKE               (Uint32) 0xC0C00000u


static MSGQ_Obj msgQueues [NUM_MSG_QUEUES];

MSGQ_TransportObj transports [MAX_PROCESSORS] =
{
    MSGQ_NOTRANSPORT,
    MSGQ_NOTRANSPORT
};

MSGQ_Config MSGQ_config =
{
    msgQueues,
    transports,
    NUM_MSG_QUEUES,
    MAX_PROCESSORS,
    0,
    MSGQ_INVALIDMSGQ,
    POOL_INVALIDID
};


POOL_Config POOL_config; //  = {pools, NUM_POOLS};

SMAPOOL_Params  smaPoolObj = {0, FALSE}; // ID 0, exact match not required
ZCPYMQT_Params  mqtParams = {0};  // pool ID

volatile unsigned int DSPLINK_initFlag = 0xBABAC0C0;
Bool PA_DSPLINK_isInitialized = FALSE;


// When built using LINK Task mode for communication synchronization then
// grab reference for MQT ctrl. This is modified below to adjust
// the thread priority and stack.
#ifdef DSP_TSK_MODE
 #include <dsplinkmqt.h>
 extern DSPLINKMQT_Ctrl * DSPLINKMQT_ctrlPtr;
#endif


// don't use the SMA pool's allocators.  Here we just want ringIO.
// Create our own allocator based on sma pool.
static int simpleAlloc_open(void**** a,void*** b)
{
    return 0;
}

static void simpleAlloc_close(void*** b)
{ }

static int simpleAlloc_alloc(void*** allocator,
                         void**** allocated,
                         unsigned int size)
{
    // void* result = malloc(size);
    void* result = MEM_alloc(SDRAM, size, 128);

    *allocated = (void***)result;
    TRACE((&trace, "%s:  Malloc'ed %d bytes at 0x%x", __FUNCTION__, size, result));
    // printf("%s:  Malloc'ed %d bytes at 0x%x\n", __FUNCTION__, size, result);
    if (result == 0)
        return 47;
    return 0;
}

static int simpleAlloc_free(void*** allocator,
                         void*** allocated,
                         unsigned int size)
{
    TRACE((&trace, "%s:  freeing %d bytes, from 0x%x", __FUNCTION__, size, allocated));
    // printf("%s:  freeing %d bytes, from 0x%x\n", __FUNCTION__, size, allocated);
    MEM_free(SDRAM, allocated, size);
    return 0;
}

POOL_Fxns simpleAllocator = {
    (POOL_Open)  simpleAlloc_open,
    (POOL_Close) simpleAlloc_close,
    (POOL_Alloc) simpleAlloc_alloc,
    (POOL_Free)  simpleAlloc_free
};

#define NUMALLOCATORS   1
static POOL_Obj allocators[NUMALLOCATORS] =
    {{NULL,                 /* Allocator init function, not used with simple allocator */
      &simpleAllocator,     /* Allocator interface functions */
      NULL,                 /* Allocator configuration : not used by simpleAllocator */
      NULL                  /* Allocator object  : not used by simpleAllocator */
    }};                    

POOL_Config  POOL_config = {allocators, NUMALLOCATORS};

/********************************************************************/

void afpLinkTask(void)
{
    // dummy because it's instantiated in pa.cfg.
}

/********************************************************************/
void dspOnlyLinkInit (void)
{
    extern  Uint32 DSPLINK_shmBaseAddress ;

    // Int                          status = SYS_OK;
    DRV_Ctrl * ctrlPtr  = (DRV_Ctrl *) DSPLINK_shmBaseAddress;
    DSPLINKIPS_Ctrl* ipsPtr;
    RingIO_Ctrl*    rioPtr; 
    SHMDRV_Ctrl *   shmemPtr;   
    MPCS_Ctrl * mpcsPtr;
    int  DSPLINK_mem_config_start_addr;
    int  DSPLINK_shared_mem_start_addr;

    DSPLINK_mem_config_start_addr = DSPLINK_shmBaseAddress;
    DSPLINK_shared_mem_start_addr =DSPLINK_shmBaseAddress - 0x5000;

    /*  
     * CJP:
     * This is tuned to make ringIO work.  I can't vouch for other stuff.
     * It's based on code from uSDK.
     * The allocator is replaced with the simple MEM_alloc() version instead of sma_pool.
     * sma_pool seems to need the host part to be initialized.
     *
     * uSDK - All these address assignments can be removed & address adjusted <not doing now>
     * Only RingIO - ringIoAddr, MPCS - mpcsAddr, <as RingIO uses mpcs structs even in dsp dsp mode>
     * linkAddr, ipsAddr makes sense as SHMDRV_init, DSPLINKIPS_init, NOTIFY_init are invoked
     * during DSPLINK_init. Just dupe dsplink with some valid addresses with correct sizes.
     */
    strcpy (ctrlPtr->version, DSPLINK_VERSION);
    ctrlPtr->procId                 = 0;
    ctrlPtr->linkAddr               = DSPLINK_mem_config_start_addr + 0x100;  // C0C0BABA
    ctrlPtr->drvDspInitDone         = 0;
    ctrlPtr->ipsAddr                = DSPLINK_mem_config_start_addr + 0x180;   // contains DSPLINK_shared_mem_start_addr
    ctrlPtr->ipsDspInitDone         = 0;
    ctrlPtr->poolAddr               = DSPLINK_mem_config_start_addr + 0x2600;
    ctrlPtr->poolConfigured         = 0;
    ctrlPtr->poolDspInitDone        = 0;
    ctrlPtr->mpcsAddr               = DSPLINK_mem_config_start_addr + 0x3100;
    ctrlPtr->mpcsConfigured         = 1;
    ctrlPtr->mpcsDspInitDone        = 0;
    ctrlPtr->mplistAddr             = DSPLINK_mem_config_start_addr + 0xB300;
    ctrlPtr->mplistConfigured       = 0;
    ctrlPtr->mplistDspInitDone      = 1;
    ctrlPtr->mqtAddr                = DSPLINK_mem_config_start_addr + 0xD500;
    ctrlPtr->mqtConfigured          = 0;
    ctrlPtr->mqtDspInitDone         = 0;
    ctrlPtr->dataAddr               = DSPLINK_mem_config_start_addr + 0xDA00;
    ctrlPtr->dataConfigured         = 0;
    ctrlPtr->dataDspInitDone        = 1;
    ctrlPtr->ringIoAddr             = DSPLINK_mem_config_start_addr - 0x2C80;
    ctrlPtr->ringIoConfigured       = 1;
    ctrlPtr->ringIoDspInitDone      = 0;
    ctrlPtr->config.numIpsEntries   = 1;        // 2;
    ctrlPtr->config.numPools        = 1;
    ctrlPtr->config.numDataDrivers  = 1;
    ctrlPtr->config.cpuFreq         = 0xFFFFFFFF;

    HAL_cacheWbInv(ctrlPtr, sizeof (DRV_Ctrl));

    /* MPCS */
    mpcsPtr = (MPCS_Ctrl *) ctrlPtr->mpcsAddr;
    mpcsPtr->dspAddrEntry = (MPCS_Entry *) (DSPLINK_shared_mem_start_addr + 0x5280);
    mpcsPtr->maxEntries = 1;
    mpcsPtr->isInitialized = TRUE;


    /* IPS - 2 entries in hosted mode, one here */
    ipsPtr = (DSPLINKIPS_Ctrl*)ctrlPtr->ipsAddr;

    ipsPtr->dspAddr                 = DSPLINK_shared_mem_start_addr + 0x0000;
    ipsPtr->config.numIpsEvents     = 32;
    ipsPtr->config.gppIntId         = 28;
    ipsPtr->config.dspIntId         = 5;
    ipsPtr->config.dspIntVectorId   = 4;
    ipsPtr->config.arg1 = 50000000;
    ipsPtr->config.arg2 = 0;

    HAL_cacheWbInv(ipsPtr, sizeof (DSPLINKIPS_Ctrl));

    /* Ringio - params */
    rioPtr = (RingIO_Ctrl*) ctrlPtr->ringIoAddr;

    rioPtr->isInitialized   = FALSE;    
    rioPtr->ipsId           = 0;    
    rioPtr->ipsEventNo      = 0;    
    rioPtr->maxEntries      = 4;
    rioPtr->dspAddrEntry    = (RingIO_Entry*) (DSPLINK_shared_mem_start_addr + 0x2580);
    memset(rioPtr->dspAddrEntry, 0, (sizeof(RingIO_Entry) * rioPtr->maxEntries));   

    HAL_cacheWbInv(rioPtr, sizeof (RingIO_Ctrl));
    HAL_cacheWbInv(rioPtr->dspAddrEntry, (sizeof(RingIO_Entry) * rioPtr->maxEntries));

    /* SHMEM - GPP Handshake code  */
    shmemPtr = (SHMDRV_Ctrl *)ctrlPtr->linkAddr; 
    shmemPtr->handshakeGpp = GPP_HANDSHAKE;
    HAL_cacheWbInv(shmemPtr, sizeof (SHMDRV_Ctrl));

    // Do not wait for ARM signal
    DSPLINK_init ();

    // set up the memory pool used for DSP-DSP ring IO
    {
        int         status;
        POOL_Obj    poolObj;
    
        poolObj.initFxn          = NULL;
        poolObj.fxns             = &simpleAllocator;
        poolObj.params           = NULL;
        poolObj.object           = NULL;
        status = POOL_open (DSP_RING_POOL_ID, &poolObj);
        if (status != SYS_OK)
        {
            printf("%s.%d: POOL_open() failed with 0x%x\n",
                   __FUNCTION__, __LINE__, status);
            return;
        }
    }

#if defined (ASYNC_DUAL_ZAA)
    // uses two rings
    setup_DRIO_RingIO(DSP_DSP_RING_A_NAME, &DSP_DSPLINK_readerHandle_0, &DSP_DSPLINK_writerHandle_0);
    setup_DRIO_RingIO(DSP_DSP_RING_B_NAME, &DSP_DSPLINK_readerHandle_1, &DSP_DSPLINK_writerHandle_1);

#elif defined (ASYNC_SINGLE_ZSA)
    // uses one ring, ring B.
    setup_DRIO_RingIO(DSP_DSP_RING_B_NAME, &DSP_DSPLINK_readerHandle_1, &DSP_DSPLINK_writerHandle_1);

#elif defined (ALL_SYNCHRONOUS_ZSS)
    // uses no rings and this code should not be included.
    #warn "The ZSS configuration does not require rings and DSPLINK_DSP_ONLY_MODE should not be defined."
#else
    #warn "This afp_link.c is designed to be used with z topology code."
#endif

    return;
}


#endif  // DSPLINK_DSP_ONLY_MODE


