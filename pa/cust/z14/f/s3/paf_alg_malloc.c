
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Algorithm Memory Allocation.
//
//
//

/*
 * File Inclusions 
 */

#include <std.h>
#include <alg.h>
#include <mem.h>
#include <stdlib.h>
#include <string.h>

#include <paf_alg.h>
#include <paf_alg_priv.h>
#include "pafhjt.h"

extern int IRAM;
extern int SDRAM;

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
//

#define TRACE_MALLOC
#ifdef  TRACE_MALLOC

 #define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
 #if PAF_DEVICE_VERSION == 0xE000
  #define _DEBUG // This is to enable log_printfs
 #endif /* PAF_DEVICE_VERSION */
 #include <logp.h>

 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif


/*
 * The PAF_ALG_allocMemory is derived from _ALG_allocMemory function defined 
 * in the TI Standard XDAIS file alg_malloc.c version "XDAS 2.2.1 12-07-01"
 * It is extended to check for the Performance Audio specific "Common"
 * memory. This function assumes that memory is already
 * allocated for the common memory and shared scratch memory.
 */

Int 
PAF_ALG_allocMemory_ (
    IALG_MemRec memTab[], 
    Int n, 
    const IALG_MemRec common[],
    PAF_IALG_Config *p)
{
    char * commonBase[PAF_IALG_COMMON_MEMN+1];
    Int commonCount[PAF_IALG_COMMON_MEMN+1];
    Int i;

    if(common) {
#ifdef _TMS320C6X
#warn This pragma saves ~125 bytes! --Kurt
#endif
#pragma UNROLL(1)
        for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i) {
            commonBase[i]=common[i].base;
            commonCount[i]=0;
        }
    }
    
    for(i=0;i<n;++i) {
        if( common && 
           (memTab[i].attrs == PAF_IALG_COMMON_MEM0 ||
            memTab[i].attrs >= PAF_IALG_COMMON_MEM1 && 
            memTab[i].attrs <= PAF_IALG_COMMON_MEMN)) {
            memTab[i].base = commonBase[memTab[i].attrs] + 
                             (commonCount[memTab[i].attrs] ? memTab[i].alignment : 0);
            memTab[i].base = (void *)((unsigned int )memTab[i].base & 
                             (~(memTab[i].alignment ? memTab[i].alignment - 1 : 0)));
            commonBase[memTab[i].attrs] = (char *)memTab[i].base + memTab[i].size;
            commonCount[memTab[i].attrs]++;
        }
        else {
            if(memTab[i].size){
                if(p) {
                    if(p->clr){
                    	TRACE((&trace, "PAF_ALG_allocMemory_.%d: calloc %d bytes from space %d",
                    			__LINE__, memTab[i].size, memTab[i].space));
                        if(!(memTab[i].base = (void *)MEM_calloc(
                            PAF_ALG_memSpace(p,memTab[i].space),
                            memTab[i].size, memTab[i].alignment)))
                        {
                        	TRACE((&trace, "PAF_ALG_allocMemory_.%d: calloc %d bytes failed.",
                        	                    			__LINE__, memTab[i].size));
                        	return PAF_ALGERR_PERSIST;
                        }

                    }
                    else{
                    	TRACE((&trace, "PAF_ALG_allocMemory_.%d: alloc %d bytes from space %d",
                    			__LINE__, memTab[i].size, memTab[i].space));
                        if(!(memTab[i].base = (void *)MEM_alloc(
                            PAF_ALG_memSpace(p,memTab[i].space),
                            memTab[i].size,memTab[i].alignment)))
                        {
                        	TRACE((&trace, "PAF_ALG_allocMemory_.%d: alloc %d bytes failed.",
                        	                        	                    			__LINE__, memTab[i].size));
                            return PAF_ALGERR_PERSIST;
                        }
                    }
                }
                else {
                    PAF_IALG_Config pafConfig;
                    PAF_ALG_setup(&pafConfig,IRAM,SDRAM,IRAM,0); 
                    TRACE((&trace, "PAF_ALG_allocMemory_.%d: alloc %d bytes from space %d",
                                        			__LINE__, memTab[i].size, memTab[i].space));
                    if(!(memTab[i].base=(void *)MEM_alloc(
                        PAF_ALG_memSpace(&pafConfig,memTab[i].space),
                        memTab[i].size,memTab[i].alignment)))
                    {
                    	TRACE((&trace, "PAF_ALG_allocMemory_.%d: alloc %d bytes failed.",
                    	                        	                    			__LINE__, memTab[i].size));
                        return PAF_ALGERR_PERSIST;
                    }
                }
            }
            else
                memTab[i].base=NULL;
        }
    }
    return 0;
}

/*
 * The PAF_ALG_mallocMemory function allocates the memory requested by the 
 * memTab's.
 */

Int
PAF_ALG_mallocMemory_ (
    IALG_MemRec common[], 
    PAF_IALG_Config *p)
{
    Int i;

    for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i)
    {
        if(0 == common[i].size)
            continue;

        if(p->clr)
        {
            TRACE((&trace, "PAF_ALG_mallocMemory_.%d: calloc %d bytes from space %d",
                                        			__LINE__, common[i].size, common[i].space));
            common[i].base = (void *)MEM_calloc( PAF_ALG_memSpace(p,common[i].space),
                                                      common[i].size, common[i].alignment);
        }
        else
        {
            TRACE((&trace, "PAF_ALG_mallocMemory_.%d: alloc %d bytes from space %d",
                                                    __LINE__, common[i].size, common[i].space));
            common[i].base = (void *)MEM_alloc(PAF_ALG_memSpace(p,
                        common[i].space),common[i].size, common[i].alignment);
        }

        if(!(common[i].base))
        {
            TRACE((&trace, "PAF_ALG_mallocMemory_.%d: (c)alloc %d bytes failed",
                                        			__LINE__, common[i].size));
            return PAF_ALGERR_COMMON;
        }
    }

    return 0;
}

/*
 * The PAF_ALG_freeMemory is derived from _ALG_freeMemory function defined 
 * in the TI Standard XDAIS file alg_malloc.c version "XDAS 2.2.1 12-07-01"
 * It is extended to check for the Performance Audio specific "Common"
 * memory and scratch memory.
 */

Void 
PAF_ALG_freeMemory(
    IALG_MemRec memTab[], 
    Int n)
{

#ifdef _TMS320C6X
#warn Cannot free memory here because segids are not known
#endif

	// Cannot free memory here bacause there is no way to determine
	// the segid needed by MEM_free.
	// Not an issuue as of now, in PA, because, if alg creation fails
	// system does not do any thing further.
	// 
#if 0
    Int i;
    Int segid;

    for(i=0;i<n;i++){
        if(memTab[i].base != NULL && !(memTab[i].attrs == PAF_IALG_COMMON_MEM0 ||
           memTab[i].attrs >= PAF_IALG_COMMON_MEM1 && memTab[i].attrs <= PAF_IALG_COMMON_MEMN))
		{
#warn MEM_free might be 
			segid = (memTab[i].space == IALG_SARAM)?IRAM:SDRAM;
			MEM_free(segid,memTab[i].base,memTab[i].size);
		}
    }
#endif
}

/* 
 * The PAF_ALG_init function initializes the memTab's to default values.
 */

Void 
PAF_ALG_init_ (
    IALG_MemRec memTab[], 
    Int n, 
    const IALG_MemSpace memSpace[])
{
    Int i;

    for (i=0; i<n; i++) {
        memTab[i].size = 0;
        memTab[i].alignment = 0;
        memTab[i].base = NULL;
        memTab[i].space = memSpace ? memSpace[i] : PAF_IALG_NONE;
    }
}

/*
 *  PAF_ALG_activate is derived from standard XDAIS ALG_activate function defined in the
 *  file alg_malloc.c version "XDAS 2.2.1 12-07-01".
 */

/* DO NOT REMOVE THIS CODE_SECTION. --Kurt */
#pragma CODE_SECTION(PAF_ALG_activate,".text:_PAF_ALG_activate")

Void
PAF_ALG_activate (
    ALG_Handle alg)
{
    asm (" .clink");

    if (alg->fxns->algActivate != NULL) {
        alg->fxns->algActivate (alg);
    }
}

/*
 *  PAF_ALG_deactivate is derived from standard XDAIS ALG_deactivate function defined in the
 *  file alg_malloc.c version "XDAS 2.2.1 12-07-01".
 */

/* DO NOT REMOVE THIS CODE_SECTION. --Kurt */
#pragma CODE_SECTION(PAF_ALG_deactivate,".text:_PAF_ALG_deactivate")

Void 
PAF_ALG_deactivate (
    ALG_Handle alg)
{
    asm (" .clink");

    if (alg->fxns->algDeactivate != NULL) {
        alg->fxns->algDeactivate(alg);
    }   
}

/* 
 * The PAF_ALG_memSpace function is derived from ALGRF_memSpace function 
 * defined in the TI released algrf.h file ver "ALGRF 0.02.06 11-21-01".
 */

Int 
PAF_ALG_memSpace_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space)
{
    switch(space){
        case IALG_DARAM0:
        case IALG_DARAM1:
        case IALG_SARAM:    /* IALG_SARAM0 = IALG_SARAM */
        case IALG_DARAM2:
        case IALG_SARAM2:
            return p->iHeap;

        case IALG_ESDATA:
        case IALG_EXTERNAL:
            return p->eHeap;
		case IALG_SARAM1:
            return p->lHeap;
        default:
            return p->eHeap;
    }
 
}

/* 
 * The PAF_ALG_setup function is derived from ALGRF_setup function defined in
 * the TI released algrf_setup.c file ver "ALGRF 0.02.06 11-21-01" .
 */

#pragma CODE_SECTION(PAF_ALG_setup_,".text:_PAF_ALG_setup_")
Void
PAF_ALG_setup_ (
    PAF_IALG_Config *p, 
    Int internalHeap, 
    Int externalHeap,
    Int l3Heap,
    Int clr)
{ 
    asm (" .clink"); 
    p->iHeap = internalHeap; 
    p->eHeap = externalHeap; 
    p->lHeap = l3Heap;
    p->clr=clr;
}
