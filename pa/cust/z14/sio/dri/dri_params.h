
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Defines setup of DRI
// 
#ifndef _DRI_PARAMS_H
#define _DRI_PARAMS_H

#include <xdas.h>
#include <pafsio.h>
#include <dev.h>
#include <paftyp.h>

#include <ringiodefs.h>
extern RingIO_Handle PA_DSPLINK_readerHandle;

typedef struct Dri_Params
{
    Int size;                                               // Type-specific size
    struct DXX_Params_ sio;                                 // Common parameters
    Int numChans;                                           // number of channels to transfer
                                                            // 
    // Must have been opened in afp_link.c or some place similar.
    // Actually type RingIO_Handle.
    // If NULL, use legacy host ring handle and configuration
    Int   *pReaderHandle;

    // Must match writeENCChannelMap given in shortcut found in io.c.
    XDAS_Int8       channelMask[PAF_MAXNUMCHAN];    // identify which channels are active
} Dri_Params;

extern const Dri_Params DRI_RINGIO;
extern const Dri_Params DRI_DSP_RINGIO;

#endif //  _DRI_PARAMS_H
/* end */
