
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// LINK RingIO interfaces for DRI
//
//
// 
//

#include <stdlib.h>
#include <stdio.h>

#include <std.h>
#include <log.h>
#include <swi.h>
#include <sys.h>
#include <sio.h>
#include <tsk.h>
#include <pool.h>
#include <gbl.h>
#include <string.h>

#include <failure.h>
#include <dsplink.h>
#include <platform.h>
#include <notify.h>

#include <ringio.h>
#include <sma_pool.h>

#include <paftyp.h>

#include "dri.h"
#include "dri_ringio.h"
#include "drierr.h"

#include "statStruct.h"
// -----------------------------------------------------------------------------

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.

#define TRACE_MASK_NONE     0
#define TRACE_MASK_TERSE    (1<<0)      // only flag errors
#define TRACE_MASK_GENERAL  (1<<1)      // describe normal behavior
#define TRACE_MASK_VERBOSE  (1<<2)      // detailed trace of operation

#define CURRENT_TRACE_MASK  1

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// -----------------------------------------------------------------------------

static inline void driFillData (PAF_InpBufConfig * pBufConfig, char * sourcePtr, Uint32 copySize )
{
    Uint32 tempSize = copySize;
    Char *writePtr = (char *)pBufConfig->head.pVoid;
    // sourcePtr is from ringIO.
    // writePtr is iBuf.

    if (((int) writePtr + tempSize) > ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer))
    {   // if we have to wrap,
        tempSize  = ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer) - ((int) writePtr);
        if (tempSize != 0)   // and there is data to copy, copy it.
        {
            memcpy (writePtr, sourcePtr, tempSize);  // might use DAT_copy here.
            TRACE_VERBOSE((&trace, "driFillData: wrapper: copy %d to 0x%x", tempSize, writePtr));
        }
        else
        {   // won't happen because we wrap below.
            TRACE_VERBOSE((&trace, "driFillData (0x%x): wrapping from 0x%x to 0x%x without copying.", pBufConfig, sourcePtr, writePtr));
        }
        // but wrap the pointer.
        sourcePtr = (char *) ((int) sourcePtr + tempSize);
        writePtr = (char *)pBufConfig->base.pVoid;
        tempSize  = copySize - tempSize;
    }
    TRACE_VERBOSE((&trace, "driFillData: copy %d to 0x%x", tempSize, writePtr));
    memcpy (writePtr, sourcePtr, tempSize);

    /* Update head of the frame value */
    pBufConfig->head.pVoid = (Ptr) ((int) writePtr + tempSize); /* writePtr holds the already wrapped around address */
    if ((int)pBufConfig->head.pVoid >= ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer))
    {
        pBufConfig->head.pVoid = pBufConfig->base.pVoid;
    }

    #ifdef ENABLE_DRI_DATA_TRACE
    {
        int *wp = (int*)writePtr;
        TRACE_VERBOSE((&trace, "driFillData: src: 0x%x, dest: 0x%x", sourcePtr, writePtr));
        TRACE_VERBOSE((&trace, "driFillData: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", wp[0], wp[16], wp[99]));
    }
    #endif

    return;
}


static inline void driFillZeros (PAF_InpBufConfig * pBufConfig, Uint32 copySize )
{
    int tempSize = copySize;
    Char *writePtr = (char *)pBufConfig->head.pVoid;

    TRACE_VERBOSE((&trace, "driFillZeros: %d bytes to 0x%x", copySize, writePtr));
    TRACE_VERBOSE((&trace, "driFillZeros: base: 0x%x.  sizeOfBuffer 0x%x", pBufConfig->base.pVoid, pBufConfig->sizeofBuffer));
    if (((int ) writePtr + tempSize) > ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer))
    {
        tempSize = ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer) - ((int) writePtr);
        if (tempSize != 0)
        {
            TRACE_VERBOSE((&trace, "driFillZeros: wrapper: %d bytes to 0x%x", tempSize, writePtr));
            memset (writePtr, 0, tempSize);
            tempSize = copySize - tempSize;
        }
        else
        {
            TRACE_VERBOSE((&trace, "driFillZeros: wrapping at 0x%x without writing.", tempSize, writePtr));
        }
        writePtr = (char *)pBufConfig->base.pVoid;
    }
    TRACE_VERBOSE((&trace, "driFillZeros: %d bytes to 0x%x", tempSize, writePtr));
    memset(writePtr, 0, tempSize);
     
     /* Update head of the frame value */ 
    pBufConfig->head.pVoid = (Ptr) ((int) writePtr + tempSize); /* writePtr holds the already wrapped around address */

    return;
}

Int DRI_RingIO_Reclaim (DRI_DeviceExtension *pDevExt,
                        PAF_InpBufConfig * pBufConfig,
                        Uint32 size)

{
    Int      rdRingStatus = RINGIO_SUCCESS;
    Int            status = SYS_OK;
    Uint32  readerAcqSize = 0;
    Char      * readerBuf;

    while (size != 0) 
    {
        readerAcqSize = size;
        rdRingStatus  = RingIO_acquire (pDevExt->RingHandle, (RingIO_BufPtr *) &readerBuf, &readerAcqSize);
        TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x) acquired %d", pDevExt->device, readerAcqSize));

        if ((rdRingStatus == RINGIO_SUCCESS) ||
            (((readerAcqSize > 0) && ((rdRingStatus == RINGIO_ENOTCONTIGUOUSDATA) ||
                                      (rdRingStatus == RINGIO_EBUFWRAP) || 
                                      (rdRingStatus == RINGIO_SPENDINGATTRIBUTE))))) 
        {
            // Acquired Read buffer. Copy received data to framework buffer.
            if (readerBuf)                 
                driFillData (pBufConfig, readerBuf, readerAcqSize);

            size -= readerAcqSize;
            rdRingStatus = RingIO_release (pDevExt->RingHandle, readerAcqSize);
            TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x) released %d", pDevExt->device, readerAcqSize));
            
            if (rdRingStatus != RINGIO_SUCCESS)
            {
                TRACE_TERSE((&trace, "DRI_RingIO_Reclaim (0x%x) error line %d, 0x%x", pDevExt->device, __LINE__, rdRingStatus));
                SET_FAILURE_REASON (rdRingStatus);          
            }
            continue;
        }
        if ((rdRingStatus == RINGIO_EFAILURE) || (rdRingStatus == RINGIO_EBUFEMPTY)) 
        {
            // fill with zeros.
            // this will happen if the DRO sender is not running.
            // Then we want to keep going, not error out.
            // // size contains the requisite size of data to be zero padded 
            driFillZeros (pBufConfig, size);
            statStruct_LogEmptyRing(pDevExt->RingHandle);

            if (pDevExt->freadEnd == TRUE) {

                TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x): freadEnd == TRUE", pDevExt->device));
                pDevExt->firstRead = TRUE;
                pDevExt->freadEnd = FALSE;
            }
            TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x).%d, Filled with zeros", pDevExt->device, __LINE__));
            return SYS_OK;

          #if 0 // old code
            else {
                Bool        semStatus = TRUE;

                // Wait for the read buffer to be available
                TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x) wait", pDevExt->device));
                semStatus = SEM_pend (&(pDevExt->readerSemObj), 1000);
                if (semStatus == FALSE) {
                    status = RINGIO_EFAILURE;
                    TRACE_TERSE((&trace, "DRI_RingIO_Reclaim (0x%x) RINGIO_EFAILURE", pDevExt->device));
                    SET_FAILURE_REASON (status);
                    return status;
                }
                else
                {
                    TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x) waiting", pDevExt->device));
                    status = SYS_OK;
                }
            }
            #endif  // 0
        } //   if RINGIO_EFAILURE || RINGIO_EBUFEMPTY

    } //while            

    TRACE_GEN((&trace, "DRI_RingIO_Reclaim (0x%x).%d, returning 0x%x", pDevExt->device, __LINE__, status));
    return status;
} //DRI_RingIO_Reclaim



// -----------------------------------------------------------------------------
