
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Reverse Ring IO driver
//
//
//

#include <stdio.h>
#include <string.h>

#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sys.h>
#include <tsk.h>

#include "dro.h"
#include "droerr.h"
#include "dro_ringio.h"
#include "dro_params.h"

#include <clk.h>
#include <log.h>


#include <paftyp.h>
#include <stdasp.h>

Int   DRO_ctrl (DEV_Handle device, Uns code, Arg Arg);
Int   DRO_idle (DEV_Handle device, Bool Flush);
Int   DRO_issue (DEV_Handle device);
Int   DRO_open (DEV_Handle device, String Name);
Int   DRO_reclaim (DEV_Handle device);
Int   DRO_reset (DEV_Handle device,PAF_OutBufConfig *pBufConfig);
Int   DRO_flush (DEV_Handle device);

// Driver function table.
DRO_Fxns DRO_FXNS = {
    NULL,              // close not used in PA/F systems
    DRO_ctrl,
    DRO_idle,
    DRO_issue,
    DRO_open,
    NULL,              // ready not used in PA/F systems
    DRO_reclaim,
    DRO_reset,
    DRO_flush
};

// macros assume pDevExt is available and pDevExt->pFxns is valid

#define DRO_FTABLE_reset(_a,_b)               (*pDevExt->pFxns->reset)(_a,_b)
#define DRO_FTABLE_flush(_a)                  (*pDevExt->pFxns->flush)(_a)

// .............................................................................
// syncState

enum
{
    SYNC_NONE,
    SYNC_PCM_FORCED
};

enum
{
    DRO_STATE_IDLE,
    DRO_STATE_RUNNING
};


// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.

#define TRACE_MASK_NONE     0
#define TRACE_MASK_TERSE    (1<<0)      // only flag errors
#define TRACE_MASK_GENERAL  (1<<1)      // describe normal behavior
#define TRACE_MASK_VERBOSE  (1<<2)      // detailed trace of operation

#define CURRENT_TRACE_MASK  1

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif


// -----------------------------------------------------------------------------

Int DRO_issue (DEV_Handle device)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    Int                  status = 0, xferSize;
    DEV_Frame           *srcFrame;
    PAF_OutBufConfig    *pBufConfig;

    // need valid status pointers
    if (!pDevExt->pBufStatus || !pDevExt->pEncStatus)
        return SYS_EINVAL;

    if (QUE_empty(device->todevice))
    {
        TRACE_TERSE((&TR_MOD, "DRO_issue: todevice queue is empty."));
        return SYS_EINVAL;
    }
    srcFrame   = QUE_get (device->todevice);
    TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x): got srcFrame %x.", device, srcFrame));
    pBufConfig = (PAF_OutBufConfig *) srcFrame->addr;
    if (!pBufConfig)
        return SYS_EINVAL;

    QUE_put (device->fromdevice, srcFrame);

    // CJP
    pDevExt->state = DRO_STATE_RUNNING;
    switch(srcFrame->arg)
    {	// srcFrame->arg has the sync request.  See DIB.
        case PAF_SIO_REQUEST_AUTO:
            TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x) PAF_SIO_REQUEST_AUTO", device));
            break;
        case PAF_SIO_REQUEST_NEWFRAME:
            TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x) PAF_SIO_REQUEST_NEWFRAME", device));
            break;
        case PAF_SIO_REQUEST_SYNC:
            TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x) PAF_SIO_REQUEST_SYNC", device));
            pDevExt->syncRequested = TRUE;
            break;  // don't return because issue only sets sizes.
        case 0:
            TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x) 'zero'.", device));
            break;            
        default:
            TRACE_VERBOSE((&TR_MOD, "DRO_issue (0x%x) ?? 0x%x", device, srcFrame->arg));
            break;
    }


    // if first issue after open then init internal buf config view
    if (pDevExt->syncState == SYNC_NONE) {
        status = DRO_FTABLE_reset (device, pBufConfig);
        if (status)
        {
            TRACE_TERSE((&TR_MOD, "DRO_issue.%d (0x%x) DRO_FTABLE_reset failed with 0x%x", __LINE__, device, status));
            return status;
        }
        pDevExt->syncState = SYNC_PCM_FORCED;
    }

    // lengthofFrame varies with ASRC
    xferSize = pBufConfig->lengthofFrame * pDevExt->bufConfig.sizeofElement * pDevExt->numChans;
    TRACE_GEN((&TR_MOD, "DRO_issue (0x%x): xferSize: %d.  lengthofFrame %d", device, xferSize, pBufConfig->lengthofFrame));

    // pDevExt is used only for state and ring handle.
    // pBufConfig is used only to update the pointer, but it always updates to the same.
    // last param:  size is the number of bytes we need to write.
    status = DRO_RingIO_Issue(pDevExt, pBufConfig, xferSize);
    pDevExt->state = DRO_STATE_RUNNING;
    if (status)
    {
        TRACE_TERSE((&TR_MOD, "DRO_issue.%d (0x%x) DRO_RingIO_Issue failed with 0x%x", __LINE__, device, status));
    }
    return status;
}// DRO_issue

// -----------------------------------------------------------------------------

// Although interface allows for arbitrary BufConfigs we only support 1 -- so
// we can assume the one on the todevice is the one we want

Int DRO_reclaim (DEV_Handle device)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    DEV_Frame          *sourceFrame;
    // PAF_OutBufConfig *pBufConfig;
    
    if (pDevExt->state != DRO_STATE_RUNNING)
    {
        TRACE_TERSE((&TR_MOD, "DRO_reclaim (0x%x).  NOT RUNNING", device));
        return DROERR_NOTRUNNING;
    }

    TRACE_VERBOSE((&TR_MOD, "DRO_reclaim (0x%x).", device));
    sourceFrame = (DEV_Frame *) QUE_head (device->fromdevice);

    if (sourceFrame == (DEV_Frame *) device->fromdevice)
        return DROERR_UNSPECIFIED;
    if (!sourceFrame->addr)
        return DROERR_UNSPECIFIED;

    // pBufConfig = (Ptr) sourceFrame->addr;

    sourceFrame->size = sizeof (PAF_OutBufConfig);

    if (pDevExt->syncRequested)
    {
        pDevExt->syncRequested = FALSE;
        TRACE_VERBOSE((&TR_MOD, "DRO_reclaim (0x%x).  Return after syncRequested.", device));
    }
    return SYS_OK;
} // DRO_reclaim

//--------------------------------------------------------------------------------
Int DRO_initStats (DEV_Handle device, float seed)
{
    DRO_DeviceExtension   *pDevExt = (DRO_DeviceExtension *) device->object;
    int iseed = (unsigned int)(seed+0.1);
    // PAF_SIO_Stats *pStats;
    
    TRACE_VERBOSE((&TR_MOD, "DRO_initStats device =0x%x, iseed =%d", device, iseed));

    // allocate structure only once to avoid fragmentation
    if (!pDevExt->pStats) {
        pDevExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDevExt->pStats)
            return SYS_EALLOC;
    }
    if(iseed == 48000)
            pDevExt->pStats->dpll.dv = 3.637978807091713e-007;
    else if(iseed == 44100)
            pDevExt->pStats->dpll.dv = 3.959703462896842e-007;
    else if(iseed == 32000)
            pDevExt->pStats->dpll.dv = 5.4569682106375695e-007;

    return SYS_OK;
} //DRO_initStats

//--------------------------------------------------------------------------------

Int DRO_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DRO_DeviceExtension     *pDevExt = (DRO_DeviceExtension *) device->object;
    int                     status = 0;

    TRACE_VERBOSE((&TR_MOD, "DRO_ctrl (0x%x). code 0x%x, arg 0x%x", device, code, arg));
    switch (code) {
    
        case PAF_SIO_CONTROL_OPEN:
        {
            const                   Dro_Params *pParams;

            TRACE_GEN((&TR_MOD, "DRO PAF_SIO_CONTROL_OPEN."));
            pParams = (const Dro_Params *) arg;
            pDevExt->pParams = pParams;

            // extract necessary info from pParams
            if (pParams->pWriterHandle == NULL)
            {
                pDevExt->dri_dro_link = FALSE;
                pDevExt->numChans = 2;
                pDevExt->RRingHandle  = PA_DSPLINK_writerHandle;
                printf("DRO configured to connect to connect to ARM.\n");
               #ifdef ENABLE_DSP_DSP_RINGIO
                printf("\n!! DRO can't yet talk to ARM if ENABLE_DSP_DSP_RINGIO is set!! \n");
               #endif
            }
            else
            {
                pDevExt->dri_dro_link = TRUE;
                pDevExt->RRingHandle  = (RingIO_Handle)(*pParams->pWriterHandle);
                pDevExt->numChans = pParams->numChans;
                TRACE_GEN((&TR_MOD, "DRO (0x%x) configured to connect %d channels to DRI (0x%x).\n", 
                           device, pDevExt->numChans, pDevExt->RRingHandle));
                // printf("DRO configured to connect %d channels to DRI (0x%x).\n", pDevExt->numChans, pDevExt->RRingHandle);
            }

            // set syncState to none to force reset in first issue call after
            // open which is needed to init internal bufConfig view.
            pDevExt->syncState   = SYNC_NONE;
            pDevExt->syncRequested = FALSE;

            break;

        }
        case PAF_SIO_CONTROL_IDLE:

            // do nothing if not running
            if (pDevExt->state != DRO_STATE_IDLE)
            {
                TRACE_VERBOSE((&TR_MOD, "DRO_ctrl (0x%x)  Idle Reset.", device));
                DRO_FTABLE_flush(device);
                DRO_FTABLE_reset(device,NULL);
                status = RingIO_sendNotify (pDevExt->RRingHandle, (RingIO_NotifyMsg)NOTIFY_DATA_END);
                pDevExt->state = DRO_STATE_IDLE;                
            }         
            break;
            
        case PAF_SIO_CONTROL_SET_BUFSTATUSADDR:
            pDevExt->pBufStatus = (PAF_OutBufStatus *) arg;
            break;

        case PAF_SIO_CONTROL_SET_ENCSTATUSADDR:
            pDevExt->pEncStatus = (PAF_EncodeStatus *) arg;
            break;        
                
        case PAF_SIO_CONTROL_ENABLE_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRO_ctrl (0x%x).%d PAF_SIO_CONTROL_ENABLE_STATS", device, __LINE__));
            DRO_initStats (device, (float) arg);
            return 0;

        case PAF_SIO_CONTROL_GET_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRO_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_STATS", device, __LINE__));
            // pass pointer to local stats structure
            *((Ptr *) arg) = pDevExt->pStats;
            return 0;

        case PAF_SIO_CONTROL_DISABLE_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRO_ctrl (0x%x).%d PAF_SIO_CONTROL_DISABLE_STATS", device, __LINE__));
            // not handled
            return 0;

            //PAF_SIO_CONTROL_UNMUTE
            //PAF_SIO_CONTROL_MUTE
          
        default:
            break;
    }
    return status;
} // DRO_ctrl

// -----------------------------------------------------------------------------

Int DRO_idle (DEV_Handle device, Bool flush)
{
    DRO_DeviceExtension   *pDevExt = (DRO_DeviceExtension *) device->object;
    Int                     status = 0;

    TRACE_VERBOSE((&TR_MOD, "DRO_idle (0x%x).", device));
    status = DRO_FTABLE_flush (device);

    status = DRO_FTABLE_reset (device,NULL);
    
    if (status)
        return status;

    pDevExt->state = DRO_STATE_IDLE;
    
    return 0;
} // DRO_idle

// -----------------------------------------------------------------------------

Int DRO_open (DEV_Handle device, String name)
{
    DRO_DeviceExtension *pDevExt;
    DEV_Device            *entry;
    (void)name;  // not used

    TRACE_GEN((&TR_MOD, "DRO_open (0x%x).", device));

    // only one frame interface supported
    if (device->nbufs != 1)
        return SYS_EALLOC;

    if ((pDevExt = MEM_alloc (device->segid, sizeof (DRO_DeviceExtension), 0)) == MEM_ILLEGAL) {
        SYS_error ("DRO MEM_alloc", SYS_EALLOC);
        return SYS_EALLOC;
    }
  
    pDevExt->lengthofFrame = 0;
    pDevExt->pBufStatus    = NULL;
    pDevExt->pEncStatus    = NULL;
    pDevExt->pStats        = NULL;
    device->object         = (Ptr) pDevExt;
    pDevExt->device        = device;

    // use dev match to fetch function table pointer for DRO
    name = DEV_match ("/DRO", &entry);
    if (entry == NULL) {
        SYS_error ("DRO", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns         = (DRO_Fxns *) entry->fxns;
    pDevExt->syncState     = SYNC_NONE;// to identify first time call
    pDevExt->syncRequested = FALSE;

    return 0;
} // DRO_open

// -----------------------------------------------------------------------------
// Although this is void it is still needed since BIOS calls all DEV inits on
// startup.

Void DRO_init (Void)
{
} // DRO_init

// -----------------------------------------------------------------------------

Int DRO_reset (DEV_Handle device, PAF_OutBufConfig * pBufConfig)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    TRACE_GEN((&TR_MOD, "DRO_reset (0x%x).", device));
    
     if (pBufConfig) 
     {
         TRACE_GEN((&TR_MOD, "DRO_reset (0x%x) pBufConfig.", device));
     
        pBufConfig->pntr          = pBufConfig->base;
        pBufConfig->head          = pBufConfig->base;
        
        pBufConfig->lengthofData  = 0;
        pBufConfig->sizeofElement = pDevExt->pParams->sio.wordSize;
        pBufConfig->stride        = pDevExt->pParams->numChans;
        pBufConfig->sizeofBuffer  = pBufConfig->allocation;  
        pDevExt->lengthofFrame    = pBufConfig->lengthofFrame;
        
        // initialize internal view with external buf config
        pDevExt->bufConfig = *pBufConfig;
    }

    pDevExt->syncState     = SYNC_NONE;
    pDevExt->secondary_out = NULL;
     
    return 0;
} //DRO_reset

Int DRO_flush (DEV_Handle device)
{
    TRACE_VERBOSE((&TR_MOD, "DRO_flush (0x%x).", device));

    while (!QUE_empty (device->todevice))
        QUE_enqueue (device->fromdevice, QUE_dequeue (device->todevice));

     while (!QUE_empty (device->fromdevice))
        QUE_enqueue (&((SIO_Handle) device)->framelist, QUE_dequeue (device->fromdevice));

     return 0;
} // DRO_flush

// -----------------------------------------------------------------------------

