
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// LINK interface to DRO driver
//
//
//

#include <stdlib.h>
#include <stdio.h>

#include <std.h>
#include <log.h>
#include <swi.h>
#include <sys.h>
#include <sio.h>
#include <tsk.h>
#include <pool.h>
#include <gbl.h>
#include <string.h>

#include <failure.h>
#include <dsplink.h>
#include <platform.h>
#include <notify.h>

#include <ringio.h>
#include <sma_pool.h>

#include <paftyp.h>

#include "dro.h"
#include "dro_ringio.h"
#include "droerr.h"

#include <csl_dat.h>
#include <pafhjt.h>

#include <ti/sysbios/hal/Cache.h>

#include "statStruct.h"

enum
{
    DRO_STATE_IDLE,
    DRO_STATE_RUNNING
};
enum
{
    DRO_PRIMARY_OUT,
    DRO_SECONDARY_OUT
};

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.

#define TRACE_MASK_NONE     0
#define TRACE_MASK_TERSE    (1)      // only flag errors
#define TRACE_MASK_GENERAL  (2)      // describe normal behavior
#define TRACE_MASK_VERBOSE  (4)      // detailed trace of operation
#define TRACE_MASK_DATA     (8)      // See the data

#define CURRENT_TRACE_MASK  1

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
 #define TRACE_DATA(a) LOG_printf a
#else
 #define TRACE_DATA(a)
#endif



//-----------------------------------------------------
//#define FILL_WITH_SINE
// If defined, DRO fills the ring with a sine wave, not what was received.
// Was last used with stereo only.
// 
#ifdef FILL_WITH_SINE
  #include <math.h>
  #define PI      3.1415926f

// pass size in bytes, assumed 8x frame size.
static void droFillSine (float * fp, int size)
{
    static unsigned int j = 0;
    int               srv = 48000;
    float             sri = 1.0f / 48000.0f;
    int              samp = j % srv;
    float            time = samp * sri;
    float            tinc = sri;
    float          omega1 = 2 * PI * 500.0f;
    float          omega2 = 2 * PI * 2000.0f;
    int                 i;
    int             lsize = size/8;
    float              *p = fp;

    // droFillSine is hard coded to stereo float samples.  
    TRACE_VERBOSE((&TR_MOD, "droFillSine: %d samples at 0x%x", lsize, p));
    for (i = 0; i < lsize; i++) 
    {
        *p++ = sinf (omega1 * time);
        *p++ = sinf (omega2 * time);
        // *p++ = ((short) (32767 *  sinf (omega1 * time)) << 16) & 0xFFFF0000;
        // *p++ = ((short) (32767 *  sinf (omega2 * time)) << 16) & 0xFFFF0000;
        time += tinc;
    }
    j = samp + (lsize);

    return;
} //droFillSine
#endif  // FILL_WITH_SINE


// -----------------------------------------------------------------------------
inline void droFillData (char* destptr,
                         PAF_OutBufConfig * pBufConfig,
                         Uint16 dri_dro_link,  // true if working on the same DSP, no cache invalidate required.
                         Uint32 copySize )
{
    Char *readPtr = (char *)pBufConfig->pntr.pVoid;

   #ifdef FILL_WITH_SINE
    droFillSine((float*)destptr, copySize);
   #else
    if (dri_dro_link)
    {
        TRACE_VERBOSE((&TR_MOD, "droFillData (0x%x) size %d.  Dest 0x%x.", pBufConfig, copySize, destptr));
        memcpy (destptr, readPtr, copySize);  // could use DAT_copy to lower cache traffic
    }
    else
    {
        Int datid;
        // memcpy replaced with DAT_copy.
        // must handle caching for off chip destinations
        TRACE_GEN((&TR_MOD, "droFillData.%d DRO-ARM link", __LINE__));
        Cache_inv(readPtr,copySize,Cache_Type_ALL,TRUE);
        datid = DAT_copy (readPtr, destptr,copySize);
        DAT_wait(datid);
        Cache_inv(destptr,copySize,Cache_Type_ALL,TRUE);
    }
   #endif // FILL_WITH_SINE

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
    {
        int *wp = (int*)destptr;

        TRACE_DATA((&TR_MOD, "droFillData: copySize: %d. readPtr: 0x%x, destptr: 0x%x", 
                       copySize, readPtr, destptr));
        TRACE_DATA((&TR_MOD, "droFillData: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", wp[0], wp[16], wp[99]));
    }
#endif

    return;
}


Int DRO_RingIO_Issue (DRO_DeviceExtension *pDevExt,
                        PAF_OutBufConfig * pBufConfig,
                        Uint32 size)

{
    Int     wrRingStatus = RINGIO_SUCCESS;
    Int     status = SYS_OK;
    Uint32  writerAcqSize = 0;
    Char    * writerBuf;
    Uint16 type;
    Uint32 param,bytesFlushed;
    Int validDataLevel;

    TRACE_VERBOSE((&TR_MOD, "DRO_RingIO_Issue (0x%x).%d", pDevExt->device, __LINE__));
    
    if (pDevExt->state == DRO_STATE_IDLE)
        status = RingIO_flush (pDevExt->RRingHandle, TRUE, &type, &param, &bytesFlushed);
    
    while (size != 0) 
    {
        writerAcqSize = size;
        wrRingStatus  = RingIO_acquire (pDevExt->RRingHandle, (RingIO_BufPtr *) &writerBuf, &writerAcqSize);
        TRACE_VERBOSE((&TR_MOD, "DRO_RingIO_Issue (0x%x) acquired %d", pDevExt->device, writerAcqSize));

        if((wrRingStatus == RINGIO_EBUFFULL) && (writerAcqSize <= 0))
        {
            validDataLevel = RingIO_getValidSize (pDevExt->RRingHandle);
            statStruct_LogRingLevel(pDevExt->RRingHandle, validDataLevel);
            TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue.%d error RINGIO_EBUFFULL (0x%x. Valid %d)", __LINE__, wrRingStatus, validDataLevel));

            if(pDevExt->secondary_out == DRO_SECONDARY_OUT)
            {
                status = RingIO_flush (pDevExt->RRingHandle, TRUE, &type, &param, &bytesFlushed);
                TRACE_VERBOSE((&TR_MOD, "DRO_RingIO_Issue flushed %d", bytesFlushed));
                if (status != RINGIO_SUCCESS)
                {
                    TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue.%d error  (0x%x)", __LINE__, wrRingStatus));
                    SET_FAILURE_REASON (status);                                
                }   
                return SYS_OK;
            }
            else
            {
                // TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue.%d error RINGIO_EBUFFULL 0x%x", __LINE__, wrRingStatus));
                return RINGIO_EBUFFULL;
            }
        }   // error case, but we are done.

        if (writerAcqSize == size) // we'll finish up with this pass.
        {
            if (wrRingStatus != RINGIO_SUCCESS) 
            {
                TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue.%d: unexpected: error 0x%x", __LINE__, wrRingStatus));
            }

            droFillData (writerBuf, pBufConfig, pDevExt->dri_dro_link, writerAcqSize);

            validDataLevel = RingIO_getValidSize (pDevExt->RRingHandle);
            statStruct_LogRingLevel(pDevExt->RRingHandle, validDataLevel);

            wrRingStatus = RingIO_release (pDevExt->RRingHandle, writerAcqSize);
            if (wrRingStatus != RINGIO_SUCCESS)
            {
                TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue (0x%x) error 0x%x. valid %d.", pDevExt->device, wrRingStatus, validDataLevel));
                SET_FAILURE_REASON (wrRingStatus);          
            } 
            else
            {
                TRACE_GEN((&TR_MOD, "DRO_RingIO_Issue (0x%x) released %d, OK. valid %d. size matches.", pDevExt->device, writerAcqSize, validDataLevel));
            }
            // we only use the first section of this buffer.
            pBufConfig->pntr.pVoid = pBufConfig->base.pVoid;
            return SYS_OK;
        }   // and we're done.

        // size must not be equal to writerAcqSize
        if ((wrRingStatus == RINGIO_SUCCESS) || (wrRingStatus == RINGIO_EBUFWRAP) || (wrRingStatus == RINGIO_EBUFFULL))
        {
            Int validDataLevel;
            if (writerAcqSize <= 0)
            {
                TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue.%d: unexpected: writerAcqSize is %d.", __LINE__, writerAcqSize));
                return RINGIO_EWRONGSTATE;
            }

            droFillData (writerBuf, pBufConfig, pDevExt->dri_dro_link, writerAcqSize);

            validDataLevel = RingIO_getValidSize (pDevExt->RRingHandle);
            size -= writerAcqSize;
            wrRingStatus = RingIO_release (pDevExt->RRingHandle, writerAcqSize);
            if (wrRingStatus != RINGIO_SUCCESS)
            {
                TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue (0x%x) error 0x%x. valid %d.", pDevExt->device, wrRingStatus, validDataLevel));
                SET_FAILURE_REASON (wrRingStatus);          
            } 
            else
            {
                TRACE_GEN((&TR_MOD, "DRO_RingIO_Issue (0x%x) released %d, OK. valid %d.  size mismatch.", pDevExt->device, writerAcqSize, validDataLevel));
            }

            // update the buffer pointer as there is more to copy.
            pBufConfig->pntr.pSmInt += writerAcqSize;
            continue;  // there's more to do.
        }
        else // other errors.
        {
            status = RINGIO_EFAILURE;
            TRACE_TERSE((&TR_MOD, "DRO_RingIO_Issue (0x%x).%d error 0x%x", pDevExt->device, __LINE__, status));
            SET_FAILURE_REASON (status);
            return status;
        } 
    } // while           

    return status;
} //DRO_RingIO_Reclaim

// -----------------------------------------------------------------------------
