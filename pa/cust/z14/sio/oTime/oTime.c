
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// oTime is a dummy input driver whose timing is based on DAP output.
// When the DAP output driver cycles its buffer, it can post the semaphore
// that oTime_reclaim is waiting on.
// The audio frame of oTime is always empty.
// Used to provide master timing to the Z-topology output thread.
//


#include <stdio.h>
#include <string.h>

#include <paftyp.h>

#include "oTime.h"
#include "oTime_params.h"


Int   OTIME_ctrl (DEV_Handle device, Uns code, Arg Arg);
Int   OTIME_idle (DEV_Handle device, Bool Flush);
Int   OTIME_issue (DEV_Handle device);
Int   OTIME_open (DEV_Handle device, String Name);
Int   OTIME_reclaim (DEV_Handle device);
Int   OTIME_requestFrame (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   OTIME_reset (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   OTIME_waitForData (DEV_Handle device, PAF_InpBufConfig *pBufConfig, XDAS_UInt32 count);


// oTimever function table.
OTIME_Fxns OTIME_FXNS = {
    NULL,              // close not used in PA/F systems
    OTIME_ctrl,
    OTIME_idle,
    OTIME_issue,
    OTIME_open,
    NULL,              // ready not used in PA/F systems
    OTIME_reclaim,
    OTIME_requestFrame,
    OTIME_reset,
    OTIME_waitForData
};

// macros assume pDevExt is available and pDevExt->pFxns is valid

#define OTIME_FTABLE_requestFrame(_a,_b)        (*pDevExt->pFxns->requestFrame)(_a,_b)
#define OTIME_FTABLE_reset(_a,_b)               (*pDevExt->pFxns->reset)(_a,_b)
#define OTIME_FTABLE_waitForData(_a,_b,_c)      (*pDevExt->pFxns->waitForData)(_a,_b,_c)
#define OTIME_FTABLE_ctrl(_a,_b,_c)             (*pDevExt->pFxns->ctrl)(_a,_b,_c)

#define OTIMEERR_UNSPECIFIED	0x7001


// .............................................................................
// syncState

enum
{
    SYNC_NONE,
    SYNC_PCM_FORCED
};

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
//
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// #define ENABLE_TRACING
#ifdef ENABLE_TRACING
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif



// -----------------------------------------------------------------------------
// There is only one oTime object.   oTime is not currently reentrant.
// It currently depends on this global to sync with DAP.
// Currently assume only one DAP output.
// 
static SEM_Obj semObj;

SEM_Handle g_oTimeSem = NULL;
int g_oTimeDapActive = 0;   // set in dap_dmax.c

const oTime_Params OTIME_Primary = {
    sizeof (oTime_Params),  /* size */
    "/OTIME",               /* name */
    0,                      /* Channel Number (moduleNum) */
    NULL,                   /* pConfig (unused) */
    4,                      /* wordSize (in bytes, unused) */
    32,                     /* precision (unused) */
    NULL,                   /* control (unused) */
    2,                      /* no_channel: number of channels */
    NULL,                   // put g_oTimeSem address here?
    {0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3}
};

// -----------------------------------------------------------------------------

Int OTIME_issue (DEV_Handle device)
{
    OTIME_DeviceExtension *pDevExt = (OTIME_DeviceExtension *) device->object;
    Int                   status = 0;
    DEV_Frame          *srcFrame;
    PAF_InpBufConfig *pBufConfig;

    TRACE((&TR_MOD, "OTIME_issue (0x%x) line %d", device, __LINE__ ));

    // check if queue is empty!
    if (QUE_empty(device->todevice))
    {
        TRACE((&TR_MOD, "OTIME_issue (0x%x) line %d: todevice queue is empty.", device, __LINE__));
        return SYS_EINVAL;
    }

    srcFrame   = QUE_get (device->todevice);
    TRACE((&TR_MOD, "OTIME_issue (0x%x).%d:  got srcFrame 0x%x.", device, __LINE__, srcFrame));
    pBufConfig = (PAF_InpBufConfig *) srcFrame->addr;
    if (!pBufConfig || !pBufConfig->pBufStatus)
    {
        TRACE((&TR_MOD, "OTIME_issue.%d: (0x%x)  bad pBufConfig.", device, __LINE__));
        return SYS_EINVAL;
    }

    QUE_put (device->fromdevice, srcFrame);

    // CJP
    switch(srcFrame->arg)
    {   // srcFrame->arg has the sync request.  See DIB.
        case PAF_SIO_REQUEST_AUTO:
            TRACE((&TR_MOD, "OTIME_issue (0x%x) PAF_SIO_REQUEST_AUTO", device));
            break;
        case PAF_SIO_REQUEST_NEWFRAME:
            TRACE((&TR_MOD, "OTIME_issue (0x%x) PAF_SIO_REQUEST_NEWFRAME", device));
            break;
        case PAF_SIO_REQUEST_SYNC:
            TRACE((&TR_MOD, "OTIME_issue (0x%x) PAF_SIO_REQUEST_SYNC", device));
            pDevExt->syncRequested = TRUE;
            break;  // don't return because issue only sets sizes.
        default:
            TRACE((&TR_MOD, "OTIME_issue (0x%x) ?? 0x%x", device, srcFrame->arg));
            break;
    }

    // if first issue after open then init internal buf config view
    if (pDevExt->syncState == SYNC_NONE) 
    {
        TRACE((&TR_MOD, "OTIME_issue.%d: (0x%x) syncState == SYNC_NONE.", __LINE__, device));
        status = OTIME_FTABLE_reset (device, pBufConfig);
        if (status)
        {
            TRACE((&TR_MOD, "OTIME_issue.%d: (0x%x) returns error 0x%x.", __LINE__, device, status));
            return status;
        }
        TRACE((&TR_MOD, "OTIME_issue.%d: (0x%x) syncState = SYNC_PCM_FORCED.", __LINE__, device));
        pDevExt->syncState = SYNC_PCM_FORCED;
    }

    status = OTIME_FTABLE_requestFrame (device, &pDevExt->bufConfig);

    TRACE((&TR_MOD, "OTIME_issue (0x%x) line %d returns 0x%x", device, __LINE__, status ));
    return status;
}// OTIME_issue

// -----------------------------------------------------------------------------
// This function uses the local definition of frameLength and lengthofData in
// pDevExt to request the next frame of data.

Int OTIME_requestFrame (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
  #if 1
    // nothing necessary here.
    return 0;
  #else
    OTIME_DeviceExtension *pDevExt = (OTIME_DeviceExtension *) device->object;
    int                   status = 0;
    int xferSize;

    // flush previous frame
    pBufConfig->pntr.pVoid   = pBufConfig->head.pVoid;

    xferSize = pDevExt->pcmFrameLength * pBufConfig->sizeofElement * pDevExt->numChans;
    // TRACE((&TR_MOD, "OTIME_requestFrame: pcmFrameLength %d.  sizeofElement: %d.  chansToTransfer %d.",
    //        pDevExt->pcmFrameLength, pBufConfig->sizeofElement, pDevExt->numChans));
    TRACE((&TR_MOD, "OTIME_requestFrame (0x%x): xferSize: %d", device, xferSize));

    pDevExt->frameLength     = xferSize; // pBufConfig->frameLength is apparently used by OTIME_reclaim.
    pDevExt->lengthofData    = pDevExt->pcmFrameLength * pDevExt->numChans;
    pBufConfig->frameLength  = xferSize; // ;
    pBufConfig->lengthofData = pDevExt->pcmFrameLength * pDevExt->numChans; // in elements, used in dwr_inp.c

    return status;
  #endif
} // OTIME_requestFrame

// -----------------------------------------------------------------------------

Int OTIME_waitForData (DEV_Handle device, PAF_InpBufConfig * pBufConfig, XDAS_UInt32 count)
{
  #if 1
    return 0;  // nothing necessary here
  #else
    OTIME_DeviceExtension *pDevExt = (OTIME_DeviceExtension *) device->object;
    int  status;

    while (1)
    {
        Int validDataLevel = RingIO_getValidSize (pDevExt->RingHandle);
        TRACE((&TR_MOD, "OTIME_waitForData (0x%x): %d valid, wanted %d.", device, validDataLevel, count));

        // check that decoding still requested -- allows for status
        // register to be updated via IOS to cancel autoProcessing
        if (pDevExt->pDecodeStatus) 
        {
            if (pDevExt->pDecodeStatus->sourceSelect == PAF_SOURCE_NONE)
            {
                TRACE((&TR_MOD, "OTIME_waitForData: return OTIMEERR_SYNC."));
                return OTIMEERR_SYNC;
            }
        }

        //TRACE((&TR_MOD, "OTIME_waitForData: count: %d, pcmFrameLength: %d, sizeofElement: %d.",
        //        count, pDevExt->pcmFrameLength, pBufConfig->sizeofElement));

        status = OTIME_RingIO_Reclaim (pDevExt, pBufConfig, count); // (pDevExt->frameLength * pBufConfig->sizeofElement));
        if (status)
        {
            TRACE((&TR_MOD, "OTIME_waitForData (0x%x).%d: return 0x%x.", device, __LINE__, status));
            // to do:  Support not getting all of the data requested.
            return status;
        }
        else 
        {
            TRACE((&TR_MOD, "OTIME_waitForData (0x%x).%d: reclaimed %d OK.", device, __LINE__, count));
            return 0;
        }
    }
    // return 0;
  #endif
} // OTIME_waitForData

// -----------------------------------------------------------------------------
// Although interface allows for arbitrary BufConfigs we only support 1 -- so
// we can assume the one on the fromdevice is the one we want


Int OTIME_reclaim (DEV_Handle device)
{
    OTIME_DeviceExtension *pDevExt = (OTIME_DeviceExtension *) device->object;
    DEV_Frame          *dstFrame;

    // TRACE((&TR_MOD, "OTIME_reclaim (0x%x) line %d", device, __LINE__ ));
    dstFrame = (DEV_Frame *) QUE_head (device->fromdevice);

    if (dstFrame == (DEV_Frame *) device->fromdevice)
    {
        TRACE((&TR_MOD, "OTIME_reclaim (0x%x) line %d.  Null frame.", device, __LINE__ ));
        return OTIMEERR_UNSPECIFIED;
    }
    if (!dstFrame->addr)
    {
        TRACE((&TR_MOD, "OTIME_reclaim (0x%x) line %d.  Null address.", device, __LINE__ ));
        return OTIMEERR_UNSPECIFIED;
    }

    if (pDevExt->syncRequested)
    {
        pDevExt->syncRequested = FALSE;
        pDevExt->sourceProgram = PAF_SOURCE_PCM;  // type is always PCM.
        g_oTimeDapActive = 0;
        TRACE((&TR_MOD, "OTIME_reclaim (0x%x).%d: return after syncRequested.", device, __LINE__));
        return 0;
    }

    if (!g_oTimeSem)
    {
        g_oTimeDapActive = 0;
        TRACE((&TR_MOD, "OTIME_reclaim (0x%x).%d: return because g_oTimeSem is null.", device, __LINE__));
        return 0;
    }

    if (!g_oTimeDapActive)
    {
    	TRACE((&TR_MOD, "OTIME_reclaim (0x%x).%d  skipping waiting on sem", device, __LINE__));
    	pDevExt->sourceProgram = PAF_SOURCE_PCM;
    	return 0;
    }
    TRACE((&TR_MOD, "OTIME_reclaim (0x%x).%d  Waiting on sem", device, __LINE__));
    // wait to be signaled by DAP output
    SEM_pend (g_oTimeSem, SYS_FOREVER);
    pDevExt->sourceProgram = PAF_SOURCE_PCM;
    TRACE((&TR_MOD, "OTIME_reclaim (0x%x).%d returns zero", device, __LINE__));
    return 0;
} // OTIME_reclaim

//--------------------------------------------------------------------------------
Int OTIME_initStats (DEV_Handle device, float seed)
{
    OTIME_DeviceExtension   *pDevExt = (OTIME_DeviceExtension *) device->object;
    int iseed = (unsigned int)(seed+0.1);
    // PAF_SIO_Stats *pStats;
    
    TRACE((&TR_MOD, "OTIME_initStats (0x%x).%d.", device, __LINE__));
    // allocate structure only once to avoid fragmentation
    if (!pDevExt->pStats) {
        pDevExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDevExt->pStats)
            return SYS_EALLOC;
    }
    if(iseed == 48000)
            pDevExt->pStats->dpll.dv = 3.637978807091713e-007;
    else if(iseed == 44100)
            pDevExt->pStats->dpll.dv = 3.959703462896842e-007;
    else if(iseed == 32000)
            pDevExt->pStats->dpll.dv = 5.4569682106375695e-007;

    return SYS_OK;
} //OTIME_initStats

Int OTIME_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    OTIME_DeviceExtension   *pDevExt = (OTIME_DeviceExtension *) device->object;
    PAF_SIO_InputStatus *tmpStatus = (PAF_SIO_InputStatus *) arg;

    // TRACE((&TR_MOD, "OTIME_ctrl (0x%x). code: 0x%x.  arg: 0x%x", device, code, arg));

    switch (code) {

        case PAF_SIO_CONTROL_SET_DECSTATUSADDR:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_DECSTATUSADDR", device, __LINE__));
            pDevExt->pDecodeStatus = (PAF_DecodeStatus *) arg;
            return 0;

        case PAF_SIO_CONTROL_SET_PCMFRAMELENGTH:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_PCMFRAMELENGTH", device, __LINE__));
            pDevExt->pcmFrameLength = (XDAS_Int32) arg;
            return 0;

        case PAF_SIO_CONTROL_SET_SOURCESELECT:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_SOURCESELECT", device, __LINE__));
            pDevExt->sourceSelect = (XDAS_Int8) arg;
            return 0;

        case PAF_SIO_CONTROL_GET_SOURCEPROGRAM:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_SOURCEPROGRAM", device, __LINE__));
            if (!arg)
            {
                TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d: return OTIMEERR_UNSPECIFIED", device, __LINE__));
                return OTIMEERR_UNSPECIFIED;
            }                
            *((XDAS_Int8 *) arg) = pDevExt->sourceProgram;
            return 0;

        case PAF_SIO_CONTROL_SET_IALGADDR:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_IALGADDR", device, __LINE__));
            pDevExt->pSioIalg = (PAF_SIO_IALG_Obj *) arg;
            return 0;

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        case PAF_SIO_CONTROL_OPEN:
        {
            const oTime_Params *pParams;

            pParams = (const oTime_Params *) arg;
            pDevExt->pParams = pParams;

            // Could get g_oTimeSem from params
            // For now it's allocated in open.

            // set syncState to none to force reset in first issue call after
            // open which is needed to init internal bufConfig view.
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d: syncState = SYNC_NONE.", device, __LINE__));
            pDevExt->syncState  = SYNC_NONE;
            pDevExt->linkLock   = FALSE;
            pDevExt->syncRequested = FALSE;
            return 0;
        }

        case PAF_SIO_CONTROL_GET_NUM_REMAINING:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_NUM_REMAINING", device, __LINE__));
            if (!arg)
            {
                TRACE((&TR_MOD, "OTIME_ctrl.%d: return OTIMEERR_UNSPECIFIED", __LINE__));
                return OTIMEERR_UNSPECIFIED;
            }
            return 0;

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:
            tmpStatus->lock = TRUE;
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d: PAF_SIO_CONTROL_GET_INPUT_STATUS. lock: 0x%x", device, __LINE__, tmpStatus->lock));
            return 0;

        case PAF_SIO_CONTROL_ENABLE_STATS:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_ENABLE_STATS", device, __LINE__));
            OTIME_initStats (device, (float) arg);
            return 0;

        case PAF_SIO_CONTROL_GET_STATS:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_STATS", device, __LINE__));
            // pass pointer to local stats structure
            //  should this be a ptr to const ?
            *((Ptr *) arg) = pDevExt->pStats;
            return 0;

        case PAF_SIO_CONTROL_DISABLE_STATS:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_DISABLE_STATS", device, __LINE__));
            // not handled
            return 0;

        case PAF_SIO_CONTROL_GET_NUM_EVENTS:
        {
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_NUM_EVENTS", device, __LINE__));
            return 0;
        }
        case PAF_SIO_CONTROL_TRACE:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d:  PAF_SIO_CONTROL_TRACE is unsupported", device, __LINE__));
            return 0;

        default:
            TRACE((&TR_MOD, "OTIME_ctrl (0x%x).%d:  code 0x%x Unsupported", device, __LINE__, code));
            return 0;
    }
} // OTIME_ctrl
// -----------------------------------------------------------------------------

Int OTIME_idle (DEV_Handle device, Bool flush)
{
    OTIME_DeviceExtension   *pDevExt = (OTIME_DeviceExtension *) device->object;
    Int                     status = 0;

    TRACE((&TR_MOD, "OTIME_idle (0x%x), line %d: Reset.", device, __LINE__));
    g_oTimeDapActive = 0;

    while (!QUE_empty (device->todevice))
        QUE_enqueue (device->fromdevice, QUE_dequeue (device->todevice));

    while (!QUE_empty (device->fromdevice))
        QUE_enqueue (&((SIO_Handle) device)->framelist, QUE_dequeue (device->fromdevice));

    status = OTIME_FTABLE_reset (device, NULL);
    if (status)
        return status;

    return 0;

} // OTIME_idle

// -----------------------------------------------------------------------------

Int OTIME_open (DEV_Handle device, String name)
{
    OTIME_DeviceExtension *pDevExt;
    DEV_Device            *entry;
    (void)name;  // not used

    TRACE((&TR_MOD, "OTIME_open (0x%x).", device));

    // only one frame interface supported
    if (device->nbufs != 1)
        return SYS_EALLOC;

    if ((pDevExt = MEM_alloc (device->segid, sizeof (OTIME_DeviceExtension), 0)) == MEM_ILLEGAL)
    {
        SYS_error ("OTIME MEM_alloc", SYS_EALLOC);
        return SYS_EALLOC;
    }
  
    pDevExt->pcmFrameLength = 0;
    pDevExt->sourceSelect   = PAF_SOURCE_NONE;
    pDevExt->pInpBufStatus  = NULL;
    pDevExt->pDecodeStatus  = NULL;
    pDevExt->pStats         = NULL;
    device->object          = (Ptr) pDevExt;
    pDevExt->syncRequested  = FALSE;
    pDevExt->device         = device;

    // use dev match to fetch function table pointer for OTIME
    name = DEV_match ("/OTIME", &entry);
    if (entry == NULL) {
        SYS_error ("OTIME", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns     = (OTIME_Fxns *) entry->fxns;
    pDevExt->syncState = SYNC_NONE;
    g_oTimeDapActive   = 0;

    SEM_new (&semObj, 1);	// start with count of 1 so we don't wait on output before starting input.
    g_oTimeSem = (SEM_Handle)&semObj;

    return 0;
} // OTIME_open

// -----------------------------------------------------------------------------
// Although this is void it is still needed since BIOS calls all DEV inits on
// startup.

Void OTIME_init (Void)
{
	// TRACE((&TR_MOD, "OTIME_init.%d.", __LINE__));
} // OTIME_init

// -----------------------------------------------------------------------------

Int OTIME_reset (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
    OTIME_DeviceExtension *pDevExt = (OTIME_DeviceExtension *) device->object;

    TRACE((&TR_MOD, "OTIME_reset (0x%x), line %d: Reset.", device, __LINE__));
    g_oTimeDapActive = 0;

    if (pBufConfig)
    {
        TRACE((&TR_MOD, "OTIME_reset (0x%x), line %d: pBufConfig. allocation: %d", 
                       device, __LINE__, pBufConfig->allocation));
        pBufConfig->lengthofData  = 0;
        pBufConfig->sizeofElement = pDevExt->pParams->sio.wordSize;
        pBufConfig->stride        = pDevExt->pParams->numChans;

        // hack -- save status context for use in close
        pDevExt->pInpBufStatus                = pBufConfig->pBufStatus;
        pBufConfig->pBufStatus->lastFrameFlag = 0;

        // initialize internal view with external buf config
        pDevExt->bufConfig = *pBufConfig;
    }
    pDevExt->syncState     = SYNC_NONE;
    pDevExt->pcmTimeout    = 0;
    pDevExt->zeroCount     = 0;
    pDevExt->sourceProgram = PAF_SOURCE_UNKNOWN;

    if (pDevExt->pInpBufStatus)
        pDevExt->pInpBufStatus->zeroRun = 0;

    SEM_reset (g_oTimeSem, 0);

    return 0;

} //OTIME_reset


// -----------------------------------------------------------------------------
