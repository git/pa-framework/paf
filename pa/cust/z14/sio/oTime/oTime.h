
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// RingIO Data oTimever declarations
//
// MID 2021 RingIO Input oTimever

#ifndef OTIME_H
#define OTIME_H

#include <std.h>

#include <dev.h>
#include <sio.h>
#include <xdas.h>

#include <inpbuf.h>
#include <pafdec.h>
#include <pafsio.h>
#include <pafsio_ialg.h>
#include <paftyp.h>

#include "oTime_params.h"

// .............................................................................

// .............................................................................
//Function table defs

typedef Int (*OTIME_TrequestFrame)(DEV_Handle, PAF_InpBufConfig *);
typedef Int (*OTIME_Treset)(DEV_Handle, PAF_InpBufConfig *);
typedef Int (*OTIME_TwaitForData)(DEV_Handle, PAF_InpBufConfig *, XDAS_UInt32);

typedef struct OTIME_Fxns {
    //common (must be same as DEV_Fxns)
    DEV_Tclose      close;
    DEV_Tctrl       ctrl;
    DEV_Tidle       idle;
    DEV_Tissue      issue;
    DEV_Topen       open;
    DEV_Tready      ready;
    DEV_Treclaim    reclaim;

    //OTIME specific
    OTIME_TrequestFrame       requestFrame;
    OTIME_Treset              reset;
    OTIME_TwaitForData        waitForData;

} OTIME_Fxns;

extern OTIME_Fxns OTIME_FXNS;

extern Void OTIME_init (Void);

// .............................................................................

typedef struct OTIME_DeviceExtension {

    DEV_Handle           device;
    OTIME_Fxns            *pFxns;

    XDAS_UInt32          zeroCount;
    XDAS_Int8            sourceSelect;
    XDAS_Int8            sourceProgram;
    XDAS_Int8            syncState;

    // second buffer info
    XDAS_Int32           frameLength;
    XDAS_Int32           lengthofData;
    XDAS_Int32           pcmFrameLength;
    XDAS_UInt32          pcmTimeout;
    PAF_SIO_Stats        *pStats;

    //hack
    PAF_InpBufStatus    *pInpBufStatus;
    PAF_DecodeStatus    *pDecodeStatus;

    PAF_SIO_IALG_Obj    *pSioIalg;
    PAF_InpBufConfig     bufConfig;

    const oTime_Params    *pParams;

    // RingIO specifics
    // SEM_Obj            readerSemObj;   // Reader SEM object
    XDAS_Int8              linkLock;   // Indicates DSPLINK handshake status
                                  // 
    XDAS_Int8               numChans;   // from oTime_Params
    XDAS_Int8          syncRequested;   // Set by OTIME_issue(PAF_SIO_REQUEST_SYNC), cleared by subsequent reclaim.

} OTIME_DeviceExtension;

// .............................................................................

#endif /* OTIME_H */
