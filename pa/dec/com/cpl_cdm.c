
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library common downmix module implementation 
//
//
//

#include <cpl.h>
#include <paftyp_a.h>
#include <paftyp.h>
#include <pafdec.h>
#include <math.h>
#include <stdasp.h>

void CDMTo_Mono_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char FromAux = pCDMConfig->channelConfigurationFrom.part.aux;
	float clev = pCDMConfig->clev;
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	char dualMonoMode  = pCDMConfig->sourceDual;	
	
	switch(FromSat)
	{
		case PAF_CC_SAT_STEREO: // C = M3DB(L + R) or for Dual Mono with MonoMix c = M6DB(L+R)
   			if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 0)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = ONE;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 2)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = ONE;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 3)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M6DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M6DB;				
			}
			else
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			}
			break;
			
		case PAF_CC_SAT_PHANTOM1://C = 3db(L + R) + Ls*slev
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] =slev;
			break;
						
		case PAF_CC_SAT_PHANTOM2://C = 3db(L + R) + 3db*slev*(Ls + Rs)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			break;

		case PAF_CC_SAT_PHANTOM3: // C = 3db*(L+R) + 3db*slev*(Ls+Rs) + slev*blev(Lb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = slev*blev;
			break;

		case PAF_CC_SAT_PHANTOM4: // C = 3db*(L+R) + 3db*slev*(Ls+Rs) + 3db*slev*blev(Lb+Rb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RBAK] = M3DB*slev*blev;
			break;

		case PAF_CC_SAT_SURROUND0: //C = 2*M3DB*clev*C + M3DB(L+R)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			break;    

		case PAF_CC_SAT_SURROUND1: //C = 2*M3DB*clev*C + 3db(L + R) + Ls*slev
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = slev;
			break;    

		case PAF_CC_SAT_SURROUND2: //C = 2*M3DB*clev*C + 3db(L + R) + M3DB*slev*(Ls+Rs)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			break;    

		case PAF_CC_SAT_SURROUND3: // C = C + 3db*(L+R) + 3db*slev*(Ls+Rs) + slev*blev(Lb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = slev*blev;
			break;

		case PAF_CC_SAT_SURROUND4: //C = C + 3db(L + R) + M3DB*slev*(Ls + Rs) + M3DB*slev*blev*(Lb+Rb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			//pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RBAK] = M3DB*slev*blev;
			break;
	}
	
}

/*****************************************************************************/	
void CDMTo_Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{		
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char FromAux = pCDMConfig->channelConfigurationFrom.part.aux;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
	float clev = pCDMConfig->clev;
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	char dualMonoMode  = pCDMConfig->sourceDual;
		
	switch(FromSat)
	{
		case PAF_CC_SAT_MONO: // L = M3DB*C, R = M3DB*C
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
			break;
			
		case PAF_CC_SAT_STEREO: 
		/* What if 
		input = LtRt and downmix = something STEREO else 
		input = LoRo and downmix = something STEREO else
		input = StereoMono and downmix = something STEREO else  
		input = StereoDual and downmix = something STEREO else 
		Check in all sucj conditions what is Downmix.part.aux?
		*/
   		//	if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 0) //L = L, R = R
		//	{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
		//	}
   		//	else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1) //L = R =  M3DB*L,
   		    if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1) //L = R =  M3DB*L,
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R = L
				
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				if(!pCDMConfig->inplace)
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 2)//L = R = M3DB*R
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ZERO; //As it was earlier made "ONE"
				
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L
				
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"										

				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = 0;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
				}
				
				
															
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 3) // L = R = M6DB(L + R)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M6DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M6DB;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L	
								
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"										
				
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M6DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M6DB;
				}
				

			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = M3DB(L + R)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L
				
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
				}

									
			}
		//	else
			//{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
		//	}
			break;
			
		case PAF_CC_SAT_PHANTOM1:
			/* If Request is LtRt, then Downmix LtRt
			 else in all other conditions, it should be LoRo
			 And Similar for other input channel config.
			 */
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt-=Ls*3db   Rt+=Ls*3db
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB*(L+R) + slev*Ls
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			        
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = slev;
				}
			}
			else // Lo+=Ls*M3DB*slev   Ro+=Ls*M3DB*slev

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;									
			}
			break;
						
		case PAF_CC_SAT_PHANTOM2:

			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt-=3db*(Ls+Rs)   Rt+=3db*(Ls+Rs)

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB(L+R) + M3DB*slev(Ls+Rs)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			        
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
				}
			}			
			else // Lo+=Ls*slev   Ro+=Rs*slev

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;			
			}
			break;

		case PAF_CC_SAT_PHANTOM3:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L - 3db*(Ls+Rs) - Lb; Rt = R + 3db*(Ls+Rs) + Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = ONE;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 3db(L+R) + 3db*slev(Ls+Rs) + slev*blev(Lb)
			{
			    pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = slev*blev;
			    }

			}
			else // Lo = L + slev*(Ls) + 3db*slev*blev(Lb); Ro = R + slev*(Rs) + 3db*slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
            }
			break;

		case PAF_CC_SAT_PHANTOM4:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L - 3db*(Ls+Rs) - 3db*(Lb+Rb); Rt = R + 3db*(Ls+Rs) + 3db*(Lb+Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 3db(L+R) + 3db*slev(Ls+Rs) + 3db*slev*blev(Lb+Rb)
			{
			    pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = M3DB*slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB*slev*blev;
			    
				}

			}
			else // Lo = L + slev*(Ls) + slev*blev(Lb); Ro = R + slev*(Rs) + slev*blev(Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = slev*blev;
            }
			break;

		case PAF_CC_SAT_SURROUND0: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt+= M3DB*C Rt+= M3DB*C   
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO)//L=R=M3DB*(L+R)+2*M3DB*clev*C
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L	
				
                pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"										
				
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = 2*M3DB*clev;
				}
			}
			else//Lo+= clev*C, Ro+= clev*C   
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;			
			}
			break;    

		case PAF_CC_SAT_SURROUND1: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - Ls*3db Rt+= 3db*C + Ls*3db

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB(L+R) + 2*M3DB*clev*C + slev*Ls
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = 2*M3DB*clev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = slev;
			
				}
			}
			else // Lo+= clev*C + Ls*M3DB*slev   Ro+= clev*C + Ls*M3DB*slev

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;				
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
			}
			break;    

		case PAF_CC_SAT_SURROUND2: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - 3db*(Ls+RS) Rt+= 3db*C + 3db*(Ls+RS)

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=3db(L+R) + 2*M3DB*clev*C + M3DB*slev*(Ls+Rs)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"

				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = 2*M3DB*clev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
			
				}
			}
			else // Lo+= clev*C + slev*Ls   Ro+= clev*C + slev*Rs

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;				
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			}
			break;  
			  
		case PAF_CC_SAT_SURROUND3:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L + 3db*(C) - 3db*(Ls+Rs) - Lb; Rt = R + 3db*(C) + 3db*(Ls+Rs) + Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = ONE;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 2*M3DB*clev*C + 3db(L+R) + 3db*slev(Ls+Rs) + slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = 2*M3DB*clev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = slev*blev;
				}
			}
			else // Lo = L + clev*(C) + slev*(Ls) + 3db*slev*blev(Lb); Ro = R + clev*(C) + slev*(Rs) + 3db*slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
            }
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - 3db*(Ls+RS) - 3db*(Lb+Rb) Rt+= 3db*C + 3db*(Ls+RS) + 3db*(Lb+Rb)

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = -M3DB;

				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=3db(L+R) + 2*M3DB*clev*C + 3db(Ls+Rs) + 3db(Lb+Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
				if(!pCDMConfig->inplace)
				{
					pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = M3DB;
					pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = 2*M3DB*clev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev;
					pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB*slev;
			
				}	
			}
			else // Lo+= clev*C + 3db*Ls + 3db*blev*Lb   Ro+= clev*C + 3db*Rs + 3db*blev*Rb

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*blev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB*blev;
			}
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Phantom1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float clev = pCDMConfig->clev;
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM2://L=L R=R Ls=3db(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;

		case PAF_CC_SAT_PHANTOM3: // Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_PHANTOM4: // Ls = 3db*(Ls+Rs) + 3db*blev*(Lb+Rb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB*blev;
			break;

		case PAF_CC_SAT_SURROUND1: //L+= clev*C R+= clev*C Ls=Ls
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;			
			break;    

		case PAF_CC_SAT_SURROUND2: //L+= clev*C R+= clev*C Ls=3db(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		    pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;  

		case PAF_CC_SAT_SURROUND3: // L = L + clev*(C); R = R + clev*(C); Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L+= clev*C R+= clev*C Ls=3db(Ls+Rs)+ 3db(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB;	
	}
	return;
}

/*****************************************************************************/	
void CDMTo_Phantom2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
		
	float clev = pCDMConfig->clev;	
	float blev = 1.0f;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM1://L=L R=R Ls=Rs=3db*Ls   
			//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls						
			
			if(!pCDMConfig->inplace)
				pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = M3DB;
			break;

		case PAF_CC_SAT_PHANTOM3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)// L=L R=R Ls' =  Ls - 0.707*Lb  Rs' = Rs + 0.707*Lb
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else // L=L R=R Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_PHANTOM4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)// L=L R=R Ls' = Ls - 0.707 * ( Lb + Rb ) Rs' = Rs + 0.707 * ( Lb + Rb )
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;

				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;
			}
			else // Ls = Ls + blev*(Lb); Rs = Rs + blev*(Rb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;

		case PAF_CC_SAT_SURROUND1: //L+= clev*C R+= clev*C Ls=Rs=3db*Ls  
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls
			
			if(!pCDMConfig->inplace)
				pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = M3DB;
			break;    

		case PAF_CC_SAT_SURROUND2: //L+= clev*C R+= clev*C Ls = Ls Rs = Rs
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			break;    

		case PAF_CC_SAT_SURROUND3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//L' = L + clev*C R' = R + clev*C Ls' =  Ls - 0.707*Lb Rs' = Rs + 0.707*Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else // L = L + clev*(C); R = R + clev*(C); Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT) //L' = L + clev*C R' = R + clev*C Ls' = Ls - 0.707 * ( Lb + Rb )  Rs' = Rs + 0.707 * ( Lb + Rb ) 
			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;
												
			//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;

			}
			else//L+= clev*C R+= clev*C Ls = Ls + Lb Rs = Rs + Rb
			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
				
			//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;
	}
	return;
}
/*****************************************************************************/	
void CDMTo_Phantom3_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{	
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float clev = pCDMConfig->clev;

	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM4: // Lb = 3db*(Lb+Rb)
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_LBAK][CDM_RBAK] = M3DB;
			break;

		case PAF_CC_SAT_SURROUND3: // L = L + clev*(C); R = R + clev*(C)
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			break;

		case PAF_CC_SAT_SURROUND4: //L+=clev*C  R += clev*C Ls=Ls Rs=Rs Lb=3db*(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;			
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_LBAK][CDM_RBAK] = M3DB;
						
			break;    
	}
	return;
}

/*****************************************************************************/	
void CDMTo_Phantom4_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float clev = pCDMConfig->clev;

	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM3: // Lb = Rb = 3db*(Lb)
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = ONE;
			pCDMConfig->coeff[PAF_RBAK][CDM_RBAK] = ZERO;
			if(!pCDMConfig->inplace)
				pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = M3DB;
			break;

		case PAF_CC_SAT_SURROUND3: //L+=clev*C  R += clev*C Ls=Ls Rs=Rs Lb=Rb=3db*Lb
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
		        pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
		        pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;			
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			
			pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = ONE;////After getting Lb, Rb = Lb
			if(!pCDMConfig->inplace)
			{	
				pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = M3DB;
			}
			break;    				

		case PAF_CC_SAT_SURROUND4: //L+=clev*C  R += clev*C Ls=Ls Rs=Ls Lb=Lb Rb=Rb
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;			
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
	
		//	pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = ONE;
			
		//	pCDMConfig->coeff[PAF_RBAK][CDM_RBAK] = ONE;
			break;    
	}
	return;
}

/*****************************************************************************/	
void CDMTo_3Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND1://L+=M3DB*slev*Ls R+=M3DB*slev*Ls C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;

		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;

		case PAF_CC_SAT_SURROUND2: //L+= slev*Ls R+= slev*Rs C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;    

		case PAF_CC_SAT_SURROUND3: // L = L + slev*(Ls) + 3db*slev*blev*(Lb); R = R + slev(Rs) + 3db*slev*blev*(Lb)
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L+= M3DB*Ls + slev*blev*Lb R+= 3db*Rs + slev*blev*Rb  C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = slev*blev;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Surround1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND2: //L= L R = R C = C Ls=3db*(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;    

		case PAF_CC_SAT_SURROUND3: // Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L= L R = R C = C Ls=3db*(Ls+Rs)+3db*blev*(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB*blev;
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Surround2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
		
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND1: //L= L R = R C = C Ls=Rs=3db*Ls
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls						
			if(!pCDMConfig->inplace)
				pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = M3DB;
			break;    

		case PAF_CC_SAT_SURROUND3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//Ls� = Ls - 0.707 * Lb Rs� = Rs + 0.707 * Lb
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else// Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//Ls� = Ls - 0.707 * ( Lb + Rb ) Rs� = Rs + 0.707 * ( Lb + Rb )  
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;

				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;
			}			
			else//L= L R = R C = C Ls+=blev*Lb Rs+=blev*Rb
			{
				//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
					
				//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
					
				//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
					
				//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
					pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
					
				//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
					pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;
	}
	return;
}
/*****************************************************************************/	
void CDMTo_Surround3_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	//float clev = pCDMConfig->clev;
	//float slev = pCDMConfig->slev;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND4: //L= L R = R C = C Ls=Ls Rs=Ls Lb=3db*(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_LBAK][CDM_RBAK] = M3DB;			
			break;    
	}
	return;
}
/*****************************************************************************/	
void CDMTo_Surround4_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{

	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND3: //L= L R = R C = C Ls=Ls Rs=Rs Lb=Rb=3db*Lb
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			
			pCDMConfig->coeff[PAF_LBAK][CDM_LBAK] = M3DB;
			
			pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = ONE;////After getting Lb, Rb = Lb

			if(!pCDMConfig->inplace)
			{
				pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] = M3DB;
			}
			break;    
	}
	return;
}

#if CDM_LEFT != 0 || CDM_RGHT != 1 || CDM_CNTR != 2 && \
    CDM_LSUR != 3 || CDM_RSUR != 4 || CDM_LBAK != 5 && \
	CDM_RBAK != 6 || CDM_SUBW != 7
#error CdmChToPafChMap is not valid because CDM_XXXX has changed
#endif

const char CdmChToPafChMap[CDM_CHAN_N]=
{ 
    PAF_LEFT,
    PAF_RGHT,
    PAF_CNTR,
    PAF_LSUR,
    PAF_RSUR,
    PAF_LBAK,
    PAF_RBAK,
    PAF_LFE,
};

/*************************************************************************/
/*
This function requires that the follwoing field has been populated
	- ToMask
	- coeff
*/
void CPL_cdm_downmixApply_(void *pv,PAF_AudioData * pIn[],PAF_AudioData * pOut[],CPL_CDM *pCDMConfig,int sampleCount)
{    
    PAF_ChannelMask ToMask = pCDMConfig->ToMask;
    Int cdmChOut,first,cdmChIn,pafChOut,j;
	PAF_AudioData *pDataIn;
	PAF_AudioData *pDataOut;

    for(cdmChOut=0;cdmChOut<CDM_CHAN_N;cdmChOut++)
    {
		pafChOut = CdmChToPafChMap[cdmChOut];
    	if(!(ToMask & (1 << pafChOut))) continue;
		pDataOut = pOut[pafChOut];
        first = 1;
		cdmChIn = cdmChOut; // start with the same channel to avoid
		                    // overwriting incase of inplace operation
    	for (j = 0; j < CDM_CHAN_N; j++,cdmChIn++)
		{
			if(cdmChIn >= CDM_CHAN_N)
					cdmChIn = 0;
            if (pCDMConfig->coeff[pafChOut][cdmChIn])
			{
		       pDataIn = pIn[CdmChToPafChMap[cdmChIn]];
			   if(first)
			   { // first time, out = scale * in
				  first = 0;
                  if((pCDMConfig->coeff[pafChOut][cdmChIn] == ONE) &&
					  (pDataIn == pDataOut))
						  continue;

				  CPL_CALL(vecScale)(pCDMConfig->coeff[pafChOut][cdmChIn],
									pDataIn,pDataOut,sampleCount);
			   }
			   else // otherwise out = out + scale * in
                 CPL_CALL(vecLinear2)(1.0f, pDataOut, 
                        pCDMConfig->coeff[pafChOut][cdmChIn],
						pDataIn, pDataOut, sampleCount);
			}
		}
    }
}

/*************************************************************************/
/*
This function requires that the follwoing field has already been populated
	- ToMask
	- coeff
*/

#ifndef _TMS320C6X
#define	_fabsf		fabs_c
float fabs_c(float src1);
#define log10f  log10
#endif

void CPL_cdm_samsiz_(void *pv,PAF_AudioFrame *pAudioFrame,CPL_CDM *pCDMConfig)
{        
    PAF_AudioSize * restrict samsiz = pAudioFrame->data.samsiz;
    
    PAF_ChannelMask ToMask = pCDMConfig->ToMask;
	PAF_ChannelMask FromMask = pCDMConfig->FromMask;

    char i, j; 
    
    PAF_AudioData SigmaCoeff;
	float samsiz_scale[PAF_HEAD_N];
	for(i=0; i<PAF_HEAD_N; i++)
	{
		if(FromMask & (1 << i))
			samsiz_scale[i] = PAF_ASP_dB2ToLinear(samsiz[i]);
		else
			samsiz_scale[i] = 0.0f;
	}

         
    for(i=0;i<PAF_HEAD_N;i++)
    {
        samsiz[i] = 0;
    	if((ToMask & (1 << i)))
    	{
    		SigmaCoeff=0;
    		for(j=0;j<CDM_CHAN_N;j++)
			  SigmaCoeff += _fabsf(pCDMConfig->coeff[i][j] * samsiz_scale[CdmChToPafChMap[j]]);

            if(pCDMConfig->inplace)
				samsiz_scale[i] = SigmaCoeff;
    		if(SigmaCoeff)
//    		 samsiz[i] = (int)ceil(2*20*log10(SigmaCoeff));
    		 samsiz[i] = 2*20*log10f(SigmaCoeff);
			
/*			//MID2194 FIX
			if(i == PAF_RSUR)
			{
				if(pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] == ONE)
					samsiz[PAF_RSUR] = samsiz[PAF_LSUR];
			}

			if(i == PAF_RBAK)
			{
				if(pCDMConfig->coeff[PAF_RBAK][CDM_LBAK] == ONE)
					samsiz[PAF_RBAK] = samsiz[PAF_LBAK];
			}

			//MID1975 FIX
			if(i == PAF_RGHT)
			{
				if(pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] == ONE)
					samsiz[PAF_RGHT] = samsiz[PAF_LEFT];
			}
*/
    	}
    }
}

/************************************************************************/
/*
This function requires that the follwoing field has been populated by the decoder
	- LfeDmxInclude
	- channelConfigurationFrom
	- channelConfigurationRequest
	- sourceDual
*/
void  CPL_cdm_downmixConfig_(void *pv,CPL_CDM *pCDMConfig)
{    
   PAF_ChannelConfiguration downmix, decode , request ;      
   
   char DecodeSat,RequestSat,dualmonomode,RequestAux;
   
   char progval,downval,requestval,dualFlag=0;
      
    /*
    This array is to decide the downmix channel config depending on decode channel config and request channel config.
    column denotes request channel config
    row denotes program channel config
    */
    static const SmInt downmix_array[12][12]={
    	{0,0,0,0,0,0,0,0,0,0,0,0},  //PAF_CC_SAT_UNKNOWN,PAF_CC_SAT_NONE
    	{0,1,2,2,2,2,2,1,1,1,1,1},  //PAF_CC_SAT_MONO
    	{0,1,2,2,2,2,2,2,2,2,2,2},  //PAF_CC_SAT_STEREO
    	{0,1,2,3,4,4,4,2,3,4,4,4},  //PAF_CC_SAT_PHANTOM1
    	{0,1,2,3,4,4,4,2,3,4,4,4},  //PAF_CC_SAT_PHANTOM2
    	{0,1,2,3,4,5,6,2,3,4,5,6},  //PAF_CC_SAT_PHANTOM3
    	{0,1,2,3,4,5,6,2,3,4,5,6},  //PAF_CC_SAT_PHANTOM4
    	{0,1,2,2,2,2,2,7,7,7,7,7},  //PAF_CC_SAT_3STEREO
    	{0,1,2,3,4,4,4,7,8,9,9,9},  //PAF_CC_SAT_SURROUND1
    	{0,1,2,3,4,4,4,7,8,9,9,9},  //PAF_CC_SAT_SURROUND2
    	{0,1,2,3,4,5,6,7,8,9,10,11},//PAF_CC_SAT_SURROUND3
    	{0,1,2,3,4,5,6,7,8,9,10,11} //PAF_CC_SAT_SURROUND4
    };
    
    static const SmInt request_cc[]={
    	0,//PAF_CC_SAT_UNKNOWN
    	0,//PAF_CC_SAT_NONE
    	1,//PAF_CC_SAT_MONO
    	2,//PAF_CC_SAT_STEREO
    	3,//PAF_CC_SAT_PHANTOM1
    	4,//PAF_CC_SAT_PHANTOM2
    	5,//PAF_CC_SAT_PHANTOM3
    	6,//PAF_CC_SAT_PHANTOM4
    	7,////PAF_CC_SAT_3STEREO
    	8,//PAF_CC_SAT_SURROUND1
    	9,//PAF_CC_SAT_SURROUND2
    	10,//PAF_CC_SAT_SURROUND3
    	11//PAF_CC_SAT_SURROUND4    	    	
    };
    
    static const SmInt downmix_cc[]={
    	(char)PAF_CC_SAT_NONE, //0
    	(char)PAF_CC_SAT_MONO, //1
    	(char)PAF_CC_SAT_STEREO, //2
    	(char)PAF_CC_SAT_PHANTOM1, //3
    	(char)PAF_CC_SAT_PHANTOM2, //4
    	(char)PAF_CC_SAT_PHANTOM3, //5
    	(char)PAF_CC_SAT_PHANTOM4, //6
    	(char)PAF_CC_SAT_3STEREO, //7
    	(char)PAF_CC_SAT_SURROUND1, //8
    	(char)PAF_CC_SAT_SURROUND2, //9
    	(char)PAF_CC_SAT_SURROUND3, //10
    	(char)PAF_CC_SAT_SURROUND4 //11
    };
    decode = pCDMConfig->channelConfigurationFrom;
    request = pCDMConfig->channelConfigurationRequest;
    DecodeSat=pCDMConfig->channelConfigurationFrom.part.sat;
    RequestSat=pCDMConfig->channelConfigurationRequest.part.sat;
    RequestAux=pCDMConfig->channelConfigurationRequest.part.aux;
    dualmonomode = pCDMConfig->sourceDual;
    
	// Setting up the inplce outplace flag; inplace-1, outplace-0
	pCDMConfig->inplace = 1;
    // MID 357 - Do not operate if channel configuration request is not known
    if (RequestSat > PAF_CC_SAT_SURROUND4) {
    	pCDMConfig->channelConfigurationTo = decode;
    	return;
    }
    
#if 0
    switch(DecodeSat)
     	{
		
		case PAF_CC_SAT_MONO:
			progval = 1;
			break;

		case PAF_CC_SAT_STEREO:
			progval = 2;
			break;

		case PAF_CC_SAT_PHANTOM1:
			progval = 3;
			break;

		case PAF_CC_SAT_PHANTOM2:
			progval = 4;
			break;

		case PAF_CC_SAT_PHANTOM3:
			progval = 5;
			break;

		case PAF_CC_SAT_PHANTOM4:
			progval = 6;
			break;

		case PAF_CC_SAT_3STEREO:
			progval = 7;
			break;

		case PAF_CC_SAT_SURROUND1:
			progval = 8;
			break;

		case PAF_CC_SAT_SURROUND2:
			progval = 9;	
			break;	
		
		case PAF_CC_SAT_SURROUND3:
			progval = 10;	
			break;	
					
		case PAF_CC_SAT_SURROUND4:
			progval = 11;	
			break;	
		
		case PAF_CC_SAT_UNKNOWN:
		case PAF_CC_SAT_NONE:
		default: 
		  	progval = 0;
	  		break;		  		  		      	
     	}              
#else
	progval = DecodeSat - 1;
	if(DecodeSat == PAF_CC_SAT_UNKNOWN)
	 progval = 0;
#endif	

    downmix.legacy = decode.legacy;
    
    requestval = request_cc[RequestSat];
    
    downval = downmix_array[progval][requestval];

    downmix.part.sat = downmix_cc[downval];


    if((dualmonomode == 1 || dualmonomode == 2 || dualmonomode == 3) &&
    	decode.part.aux == PAF_CC_AUX_STEREO_DUAL ) 
    {	   
       switch(RequestSat)
       {
       	case PAF_CC_SAT_MONO: 
       	case PAF_CC_SAT_3STEREO:
       	case PAF_CC_SAT_SURROUND1:
       	case PAF_CC_SAT_SURROUND2:
       	case PAF_CC_SAT_SURROUND3:
       	case PAF_CC_SAT_SURROUND4:
       	case PAF_CC_SAT_UNKNOWN:
       		downmix.part.sat = PAF_CC_SAT_MONO;
       		downmix.part.aux = PAF_CC_AUX_STEREO_UNKNOWN;
       		dualFlag = 1;
		break;

       	case PAF_CC_SAT_STEREO: 
       	case PAF_CC_SAT_PHANTOM1:
       	case PAF_CC_SAT_PHANTOM2:
       	case PAF_CC_SAT_PHANTOM3:
       	case PAF_CC_SAT_PHANTOM4:
       		downmix.part.aux = PAF_CC_AUX_STEREO_MONO;
       		break;       		
       }		       		
    }

    if(downmix.part.sat == PAF_CC_SAT_STEREO)
    {

    	switch(DecodeSat)
    	{
    		case PAF_CC_SAT_PHANTOM1:
    		case PAF_CC_SAT_PHANTOM2:
    		case PAF_CC_SAT_PHANTOM3:
    		case PAF_CC_SAT_PHANTOM4:    	
    		case PAF_CC_SAT_SURROUND1:
    		case PAF_CC_SAT_SURROUND2:
    		case PAF_CC_SAT_SURROUND3:
    		case PAF_CC_SAT_SURROUND4:
    			if(RequestAux == PAF_CC_AUX_STEREO_LTRT) 
    				downmix.part.aux = PAF_CC_AUX_STEREO_LTRT;
    			else
    				downmix.part.aux = PAF_CC_AUX_STEREO_STEREO;
    			break;    	
			
    		case PAF_CC_SAT_MONO:
				downmix.part.aux = PAF_CC_AUX_STEREO_MONO;
				break;

    		case PAF_CC_SAT_3STEREO:
				downmix.part.aux = PAF_CC_AUX_STEREO_STEREO;
				break;
    	}

    	if(RequestAux == PAF_CC_AUX_STEREO_MONO) 
	    	downmix.part.aux = PAF_CC_AUX_STEREO_MONO;    			

    }

    if(downmix.part.sat == PAF_CC_SAT_SURROUND2 || downmix.part.sat == PAF_CC_SAT_PHANTOM2 )
    {
    	downmix.part.aux = PAF_CC_AUX_SURROUND2_UNKNOWN;
    	switch(DecodeSat)
    	{
    		case PAF_CC_SAT_PHANTOM3:
    		case PAF_CC_SAT_PHANTOM4:
    		case PAF_CC_SAT_SURROUND3:
    		case PAF_CC_SAT_SURROUND4:
    			if(RequestAux == PAF_CC_AUX_SURROUND2_LTRT) 
    				downmix.part.aux = PAF_CC_AUX_SURROUND2_LTRT;
    			break;    	

    		case PAF_CC_SAT_SURROUND2:
    		case PAF_CC_SAT_PHANTOM2:
				if(decode.part.aux == PAF_CC_AUX_SURROUND2_LTRT)
					downmix.part.aux = PAF_CC_AUX_SURROUND2_LTRT;
    			break;
    	}
    }
    
    if(pCDMConfig->LfeDmxInclude)
     downmix.part.sub = PAF_CC_SUB_ZERO;
     
    if(decode.legacy == PAF_CC_UNKNOWN)
        downmix = decode;
    
    if(RequestSat == PAF_CC_SAT_NONE)
    	downmix.part.sat=PAF_CC_SAT_NONE;    
    else if(request.legacy == PAF_CC_UNKNOWN && !dualFlag)
    	downmix = decode;
    
    if(downmix.part.sat != PAF_CC_SAT_STEREO && downmix.part.sat != PAF_CC_SAT_SURROUND2 && downmix.part.sat != PAF_CC_SAT_PHANTOM2  )
      downmix.part.aux=PAF_CC_AUX_STEREO_UNKNOWN;    	
    
	if( (downmix.part.sat == decode.part.sat) &&(downmix.part.sat != PAF_CC_SAT_STEREO)  )
		 	downmix.part.aux=decode.part.aux;
        
/* Bud fix MID 1777
The current implementation checks if decode and downmix are same to populate the aux field as itself.
The issue is the when the decoded sat is surround and downmix sat is phantom the aux field is populated 
as unknown although there should be no change in the aux field.
*/
	if(((decode.part.sat == PAF_CC_SAT_SURROUND2) && (downmix.part.sat == PAF_CC_SAT_PHANTOM2)) ||
		((decode.part.sat == PAF_CC_SAT_SURROUND3) && (downmix.part.sat == PAF_CC_SAT_PHANTOM3))||
		((decode.part.sat == PAF_CC_SAT_SURROUND4) && (downmix.part.sat == PAF_CC_SAT_PHANTOM4)))
	{
			downmix.part.aux=decode.part.aux;
	}
    pCDMConfig->channelConfigurationTo = downmix;
}

/************************************************************************/
/*
This function requires that the follwoing field has been populated by the decoder
	- channelConfigurationFrom
	- channelConfigurationTo
	- sourceDual
	- clev
	- slev
	- LFEDmxVolume
*/
void CPL_cdm_downmixSetUp_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	
	PAF_ChannelMask FromMask, ToMask;
	
	char FromSub,ToSub;	
	
	PAF_ChannelConfiguration FromConfig, ToConfig;
	
	XDAS_Int8 ToSat;
	
	
	PAF_AudioData LFEDmxVolume = pCDMConfig->LFEDmxVolume;
	
	char i,j;
/*
	coeff[][0] = coeff multiplied with L;
	coeff[][1] = coeff multiplied with R;
	coeff[][2] = coeff multiplied with C;
	coeff[][3] = coeff multiplied with Ls;
	coeff[][4] = coeff multiplied with Rs;
	coeff[][5] = coeff multiplied with Lb;
	coeff[][6] = coeff multiplied with Rb;
	coeff[][7] = coeff multiplied with Sub;
	coeff[PAF_LEFT][] = coeff for L; 
	coeff[PAF_RGHT][] = coeff for R; 
	coeff[PAF_CNTR][] = coeff for C; 
	coeff[PAF_LSUR][] = coeff for Ls; 
	coeff[PAF_RSUR][] = coeff for Rs; 
	coeff[PAF_LBAK][] = coeff for Lb; 
	coeff[PAF_RBAK][] = coeff for Rb; 
	coeff[PAF_SUBW][] = coeff for Sub; , this is not required
*/


	FromConfig = pCDMConfig->channelConfigurationFrom ;
	ToConfig = pCDMConfig->channelConfigurationTo ; 
	ToSat = pCDMConfig->channelConfigurationTo.part.sat;
	FromSub = pCDMConfig->channelConfigurationFrom.part.sub;
	ToSub = pCDMConfig->channelConfigurationTo.part.sub;
					
	FromMask = pAudioFrame->fxns->channelMask (pAudioFrame, FromConfig);
	ToMask = pAudioFrame->fxns->channelMask (pAudioFrame, ToConfig);
	
	pCDMConfig->FromMask = FromMask;
	pCDMConfig->ToMask = ToMask;
	for(i=0;i<PAF_HEAD_N;i++)
	{	
    	for(j=0;j<CDM_CHAN_N;j++)
    	{
    		pCDMConfig->coeff[i][j]=0;
    	} 
	}
	for(i=0;i<PAF_HEAD_N;i++)
	{          
        if(i<=2) 
         j = i;
        else
         j = i-5;
     // PAF_MAXNUMCHAN is 16.
          
         if ((ToMask & (1 << i)) && (FromMask & (1 << i)))
          pCDMConfig->coeff[i][j]=ONE;
	}
    	    	  
    	switch(ToSat)
	{
		case PAF_CC_SAT_MONO:
			CDMTo_Mono(pv,pAudioFrame,pCDMConfig);
		break;
		
		case PAF_CC_SAT_STEREO:
			CDMTo_Stereo(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM1:
			CDMTo_Phantom1(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM2:
			CDMTo_Phantom2(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM3:
			CDMTo_Phantom3(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM4:
			CDMTo_Phantom4(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_3STEREO:
			CDMTo_3Stereo(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND1:
			CDMTo_Surround1(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND2:
			CDMTo_Surround2(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND3:
			CDMTo_Surround3(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND4:
			CDMTo_Surround4(pv,pAudioFrame,pCDMConfig);
		break;
	}

	if(FromSub == 1 && ToSub == 0)
	{ 
	  if (ToMask & (1 << PAF_LEFT))
	   pCDMConfig->coeff[PAF_LEFT][CDM_SUBW] =  M3DB*LFEDmxVolume;
	   
	  if (ToMask & (1 << PAF_RGHT))
	   pCDMConfig->coeff[PAF_RGHT][CDM_SUBW] =  M3DB*LFEDmxVolume;

	  if ((ToMask & (1 << PAF_CNTR)) && !(ToMask & (1 << PAF_LEFT)) && !(ToMask & (1 << PAF_RGHT)))
	   pCDMConfig->coeff[PAF_CNTR][CDM_SUBW] =  LFEDmxVolume;
	  
	}
}


