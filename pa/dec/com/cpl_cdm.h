
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library common downmix module data structures
//
//
//

#ifndef CPL_CDM_
#define CPL_CDM_

#include <paftyp.h>
#include <pafdec.h>


#define M3DB  0.707106781
#define M6DB  0.5
#define ONE 1.0
#define ZERO 0.

#define CDM_LEFT 0
#define CDM_RGHT 1
#define CDM_CNTR 2
#define CDM_LSUR 3
#define CDM_RSUR 4
#define CDM_LBAK 5
#define CDM_RBAK 6
#define CDM_SUBW  7
#define CDM_CHAN_N 8

typedef struct CPL_CDM{	
	XDAS_Int8 LfeDmxInclude;
	XDAS_Int8 inplace;
	PAF_ChannelConfiguration channelConfigurationFrom;
	PAF_ChannelConfiguration channelConfigurationRequest;
	PAF_ChannelConfiguration channelConfigurationTo;
	PAF_ChannelMask FromMask;
	PAF_ChannelMask ToMask;
	XDAS_Int8 sourceDual;
	PAF_AudioData clev;
	PAF_AudioData slev;
    	PAF_AudioData LFEDmxVolume;
	PAF_AudioData coeff[PAF_HEAD_N][CDM_CHAN_N];
}CPL_CDM;

#endif
