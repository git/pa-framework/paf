
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library common downmix modules function prototypes
//
//
//

#ifndef CPL_CDM_FXNS_
#define CPL_CDM_FXNS_

void CPL_cdm_downmixSetUp_(void *pv, PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CPL_cdm_samsiz_(void *pv,PAF_AudioFrame *pAudioFrame,CPL_CDM *pCDMConfig);
void CPL_cdm_downmixApply_(void *pv,PAF_AudioData * pIn[],PAF_AudioData * pOut[],CPL_CDM *pCDMConfig,int sampleCount);
void CPL_cdm_downmixConfig_(void *pv,CPL_CDM *pCDMConfig);
void CDMTo_Mono_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom3_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom4_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_3Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround3_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround4_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);

void CPL_cdm_downmixSetUp(void *pv, PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CPL_cdm_samsiz(void *pv,PAF_AudioFrame *pAudioFrame,CPL_CDM *pCDMConfig);
void CPL_cdm_downmixApply(void *pv,PAF_AudioData * pIn[],PAF_AudioData * pOut[],CPL_CDM *pCDMConfig,int sampleCount);
void CPL_cdm_downmixConfig(void *pv,CPL_CDM *pCDMConfig);
void CDMTo_Mono(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Stereo(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom1(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom2(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom3(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Phantom4(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_3Stereo(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround1(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround2(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround3(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
void CDMTo_Surround4(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);

#endif
