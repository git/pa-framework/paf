
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library fifo implementation 
//
//
//

#include <stdio.h>
#include <string.h>
#include "cpl_fifo.h"

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000) || ((PAF_DEVICE&0xFF000000) == 0xDD000000)
#include <pafhjt.h>
#else  /* PAF_DEVICE = 0xD8000000 || 0xDD000000*/
#ifdef _TMS320C6700_PLUS // only for Antara 
#include <dmax_dat.h>
#endif
#endif /* PAF_DEVICE = 0xD8000000 */

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// CIRC_CHECK(offset,circ_size)
#define CIRC_CHECK(o,c) ( ((o)<0)?((o)+(c)): (((o)>=(c))?((o)-(c)):(o)) )

#ifdef DEBUG_PRINTF
void DBG_prnt(CPL_Fifo *pFifo, char * s)
{
    if(pFifo)
    {
		printf("%s: pStore=0x%x size=%d r=%d curSize=%d\n",s,
						pFifo->pStore,
						pFifo->size,pFifo->r,pFifo->curSize);
    }
	else
	{
		printf("%s:\n",s);		
	}

}
#endif

void *cpl_memcpy(void *s1, void *s2, register size_t n)
{
#if (PAF_DEVICE&0xFF000000) == 0xD8000000
	memcpy(s1,s2,n);
#else /* PAF_DEVICE = 0xD8000000 */

#ifdef _TMS320C6700_PLUS // only for Antara
	if(((int)s1&0x80000000) || ((int)s2&0x80000000))
	{
		int id;
		id = DAT_copy(s2,s1,n);
		DAT_wait(id);

	}
	else
		memcpy(s1,s2,n);
#else
	memcpy(s1,s2,n);
#endif

#endif /* PAF_DEVICE = 0xD8000000 */

	return s1;
}

int CPL_fifoInit_(CPL_Fifo * pFifo)
{
   DBGPRNT(pFifo,"CPL_fifoInit");
   pFifo->r = 0;
   pFifo->curSize = 0;
   return 0;
}

// 
// return size of fifo
// 
int CPL_fifoSize_(const CPL_Fifo *pFifo)
{
  DBGPRNT(pFifo,"CPL_fifoSize");
  return pFifo->size;
}

// 
// return the number of bytes available for reading
// 
int CPL_fifoSizeFull_(const CPL_Fifo *pFifo)
{
  DBGPRNT(pFifo,"CPL_fifoSizeFull");
  return pFifo->curSize;
}

// 
// return the number of bytes available for writing
// 
int CPL_fifoSizeFree_(const CPL_Fifo *pFifo)
{
   DBGPRNT(pFifo,"CPL_fifoSizeFree");
   return pFifo->size - CPL_fifoSizeFull(pFifo);
}

//
// return bytes that can read linearly
//
int CPL_fifoSizeFullLinear_(const CPL_Fifo *pFifo)
{
  int l,a;

  DBGPRNT(pFifo,"CPL_fifoSizeFullLinear");
  l = pFifo->size - pFifo->r;
  a = CPL_fifoSizeFull(pFifo);

  if( a <= l ) return a;
  else return l;
}

//
// return bytes that can written linearly
//
int CPL_fifoSizeFreeLinear_(const CPL_Fifo *pFifo)
{
   int e;

   DBGPRNT(pFifo,"CPL_fifoSizeFreeLinear");
   e = pFifo->r + CPL_fifoSizeFull(pFifo);

   if(e >= pFifo->size)
   {
       e -= pFifo->size;
       return pFifo->r - e;
   }
   else
      return pFifo->size - e;
}

//
// read 'size' number of bytes into pBuf from fifo
// and return the actual number of bytes copied
// 
// return value can be lessthan 'size' when bytes available are lessthan 
// the bytes requested. 
// 
int CPL_fifoRead_(CPL_Fifo * pFifo, void *pBuf, int size)
{
    int a;
    int s1,s2;
    unsigned char *p=pFifo->pStore;
    int lr; // linearRead

    DBGPRNT(pFifo,"CPL_fifoRead");
	if(size < 0) return 0;

    a = CPL_fifoSizeFull(pFifo);
    size = (size>a)?a:size;

    lr = CPL_fifoSizeFullLinear(pFifo);
    if( lr >= size )// available linearly
        cpl_memcpy(pBuf,&p[pFifo->r],size);
    else
    { // circular
        s1 = (pFifo->size - pFifo->r);
        s2 = size - s1;
        cpl_memcpy(pBuf,&p[pFifo->r],s1);
        cpl_memcpy(&((unsigned char*)pBuf)[s1],p,s2);
    }

    pFifo->r += size;
    if(pFifo->r >= pFifo->size)
        pFifo->r -= pFifo->size;
    pFifo->curSize -= size;
    return size;
}

//
// write 'size' number of bytes into fifo from pBuf
// and return the actual number of bytes copied
//
// return value can be lessthan 'size' when space available writing is 
// less than the bytes requested to be written.
//  
int CPL_fifoWrite_(CPL_Fifo * pFifo, void *pBuf, int size)
{
    int f;
    int s1,s2;
    unsigned char *p=pFifo->pStore;
    int lw; // linearWrite
    int e;

    DBGPRNT(pFifo,"CPL_fifoWrite");
	if(size < 0) return 0;

    e = pFifo->r + pFifo->curSize;
    if(e >= pFifo->size) e -= pFifo->size;

    f = CPL_fifoSizeFree(pFifo);
    size = (size>f)?f:size;

    lw = CPL_fifoSizeFreeLinear(pFifo);
    if( lw >= size )
        cpl_memcpy(&p[e],pBuf,size);
    else
    {
        s1 = (pFifo->size - e);
        s2 = size - s1;
        cpl_memcpy(&p[e],pBuf,s1);
        cpl_memcpy(p,&((unsigned char*)pBuf)[s1],s2);
    }

    pFifo->curSize += size;
    return size;
}

//
// CPL_fifoReadNoPosMv and CPL_fifoReadDone together are alternative to
// using CPL_fifoRead API.
// Advantage being Application can retain data tail in fifo.
// 
// read 'size' number of bytes into pBuf from fifo
// and return the actual number of bytes copied
//
// return value can be lessthan 'size' when bytes available are lessthan 
// the bytes requested.
// 
// This function does not change the status of fifo i.e. if the function 
// is called again, function gives the same data.
// confirm the read using CPL_fifoReadDone.
// 
// Intended Usage is when user wants to read more data but retain the tail 
// in the fifo.
// 
int CPL_fifoReadNoPosMv_(CPL_Fifo * pFifo, void *pBuf, int size)
{
    int a;
    int s1,s2;
    unsigned char *p=pFifo->pStore;
    int lr; // linearRead

    DBGPRNT(pFifo,"CPL_fifoReadNoSChg");
	if(size < 0) return 0;

    a = CPL_fifoSizeFull(pFifo);
    size = (size>a)?a:size;

    lr = CPL_fifoSizeFullLinear(pFifo);
    if( lr >= size )// available linearly
        cpl_memcpy(pBuf,&p[pFifo->r],size);
    else
    { // circular
        s1 = (pFifo->size - pFifo->r);
        s2 = size - s1;
        cpl_memcpy(pBuf,&p[pFifo->r],s1);
        cpl_memcpy(&((unsigned char*)pBuf)[s1],p,s2);
    }

    //pFifo->r += size;
    //if(pFifo->r >= pFifo->size)
    //    pFifo->r -= pFifo->size;
    //pFifo->curSize -= size;
    return size;
}


// 
// CPL_fifoGetReadPtr and CPL_fifoReadDone together are alternative to
// using CPL_fifoRead API.
// Advantage being Application gets a pointer where the data is available
// linearly.
// 
// Get the ReadPtr (where data is avaiable linearly)
// Returns the number of bytes that can be read from '*p'
// Return value can lessthan 'size', when bytes available for reading 
// in fifo are lessthan the 'size'
// 
// NOTE: CPL_fifoGetReadPtr considers that data is not read.
//       Application has to use CPL_fifoReadDone for fifo 
//       to consider that bytes are read.
// 
int CPL_fifoGetReadPtr_(CPL_Fifo * pFifo, int size, void **p)
{
    int a;
    unsigned char *pStore;
    int lr; // linearRead

    DBGPRNT(pFifo,"CPL_fifoReadPtr");
	if(size < 0)
	{
		*p = NULL;
		return 0;
	}

    a = CPL_fifoSizeFull(pFifo);
    size = (size>a)?a:size;

    lr = CPL_fifoSizeFullLinear(pFifo);
    if( lr >= size  )
    { // available linearly
        pStore = pFifo->pStore;
        *p = &pStore[pFifo->r]; // give the address from which size
                                // number of bytes can be read
    }
    else
    {
       CPL_fifoMoveReadLocation(pFifo,0);
       pStore = pFifo->pStore;
       *p = &pStore[0]; // give the address from which size
                                // number of bytes can be read
    }
    return size;    
}

//
// Intended to be used after CPL_fifoGetReadPtr
// 
// Considers that 'size' number of bytes are read from fifo
// Returns the number of bytes considered read.
// Return value can lessthan 'size', when bytes available for reading 
// in fifo are lessthan the 'size'
// 
// Note: can also be used a dummy reading (or skip reading) function
//
int CPL_fifoReadDone_(CPL_Fifo * pFifo, int size)
{
    int a;

    DBGPRNT(pFifo,"CPL_fifoReadDone");
	if(size < 0) return 0;

    a = CPL_fifoSizeFull(pFifo);
    size = (size>a)?a:size;

    pFifo->r += size;
    if(pFifo->r >= pFifo->size) pFifo->r -= pFifo->size;

    pFifo->curSize -= size;

    return size;
}

// 
// CPL_fifoGetWritePtr and CPL_fifoWrote together are alternative to
// using CPL_fifoWrite API.
// Advantage being Application gets a pointer where the data can be written
// linearly.
// 
// Get the WritePtr (where data can be written linearly)
// Returns the number of bytes that can be written at '*p'
// Return value can lessthan 'size', when bytes available for writing 
// in fifo are lessthan the 'size'
// 
// NOTE: CPL_fifoGetWritePtr considers that data is not written.
//       Application has to use CPL_fifoWrote for fifo 
//       to consider that bytes are actually written.
// 
int CPL_fifoGetWritePtr_(CPL_Fifo * pFifo, int size, void **p)
{
    int f;
    unsigned char *pStore;
    int lw; // linearwrite
    int e;

    DBGPRNT(pFifo,"CPL_fifoWritePtr");
	if(size < 0)
	{
		*p = NULL;
		return 0;
	}

    f = CPL_fifoSizeFree(pFifo);
    size = (size>f)?f:size;

    lw = CPL_fifoSizeFreeLinear(pFifo);
    if( lw >= size )
    { // available linearly
        e = pFifo->r + pFifo->curSize;
        if(e >= pFifo->size)
            e -= pFifo->size;
        pStore = pFifo->pStore;
        *p = &pStore[e]; // give the address where size
                                // number of bytes can be written
    }
    else
    {
       CPL_fifoMoveReadLocation(pFifo,pFifo->size-pFifo->curSize);
       pStore = pFifo->pStore;
       *p = &pStore[0]; // give the address where size
                        // number of bytes can be written
    }
    return size;    
}

//
// Intended to be used after CPL_fifoGetWritePtr
// 
// Considers that 'size' number of bytes are written to fifo
// 
// Returns the number of bytes considered written.
// Return value can lessthan 'size', when bytes available for writting 
// in fifo are lessthan the 'size'
// 
int CPL_fifoWrote_(CPL_Fifo * pFifo, int size)
{
    int f;

    DBGPRNT(pFifo,"CPL_fifoWrote");
	if(size < 0) return 0;

    f = CPL_fifoSizeFree(pFifo);
    size = (size>f)?f:size;

    pFifo->curSize += size;
    return size;
}


//
// This function moves the read position (read index) to a specified 
// location in fifo.
// 
// This function is used by CPL_fifoGetReadPtr and CPL_fifoGetWritePtr
// to move that read index such that pointers can be made available to the
// user where data can be read or written linerly.
// 
// Algorithm:
// a) determine if left shift is closer or right shift is better
// b) check is if free space available, if not available, unwrite upto 16 
//    bytes, writeback after shift is complete.
// c) do the shifting until target is reached.
// 
int CPL_fifoMoveReadLocation_(CPL_Fifo * pFifo, int location)
{
   unsigned char *p;
   int f;
   int rotate;
   int rotateRight=0;
   int ltarget;
   int rtarget;
   int target;
   unsigned char scr[16];
   int scrUsed=0;

   DBGPRNT(pFifo,"CPL_fifoMoveReadLocation");
   if(location >= pFifo->size) location -= pFifo->size;
   if(location < 0) location += pFifo->size;

   if(pFifo->r == location) return 0;

   p = pFifo->pStore;

   ltarget = (location < pFifo->r) ?(pFifo->r - location):pFifo->r+(pFifo->size-location);
   rtarget = (location > pFifo->r) ?(location-pFifo->r):location+(pFifo->size-pFifo->r);

   if(ltarget <= rtarget)
         rotateRight = 0;
   else
         rotateRight = 1;

   target = (rotateRight)?rtarget:ltarget;

   f = CPL_fifoSizeFree(pFifo);
   if( f < target )
   { // no or too less space to rotate
     // unwrite and write back later
        int r,e,s1,s2;

        scrUsed = (target - f);
		scrUsed = (scrUsed>sizeof(scr))?sizeof(scr):scrUsed;
        r = pFifo->r + pFifo->curSize - scrUsed;
        if(r >= pFifo->size) r -= pFifo->size;
        e = r + scrUsed;
        s1 = scrUsed;
        s2 = 0;
        if(e >= pFifo->size)
        {
            s1 = pFifo->size - r;
            s2 = scrUsed - s1;
        }
        cpl_memcpy(scr,&p[r],s1);
        if(s2>0)cpl_memcpy(&scr[s1],p,s2);
        pFifo->curSize -= scrUsed;
        f = CPL_fifoSizeFree(pFifo);
   }

   while(pFifo->r != location)
   {
       if(rotateRight)
       { // rotate right
           rotate = rtarget;
           rotate = (rotate>f)?f:rotate;
           CPL_rotateRight(pFifo->pStore,pFifo->size,pFifo->r,pFifo->curSize,rotate);
           rtarget   -= rotate;
           pFifo->r += rotate;
           if(pFifo->r >= pFifo->size) pFifo->r -= pFifo->size;
       }
       else
       { // move left
           rotate = ltarget;
           rotate = (rotate>f)?f:rotate;
           CPL_rotateLeft(pFifo->pStore,pFifo->size,pFifo->r,pFifo->curSize,rotate);
           ltarget   -= rotate;
           pFifo->r -= rotate;
           if(pFifo->r < 0) pFifo->r += pFifo->size;
       }
   };

   if(scrUsed)
     CPL_fifoWrite(pFifo,scr,scrUsed);

   return 0;
}

//
// This function moves the memory to left.
// 
// This function is used by CPL_fifoMoveReadLocation
// Function achives this by three memcopies
// 
void CPL_rotateLeft_(unsigned char * restrict p,int circ_size, int offset, int size, int rotate)
{
   int dstOffset;
   int srcOffset;
   int i,copied=0,c;

   DBGPRNT(NULL,"CPL_rotateLeft");

   srcOffset = offset;
   srcOffset = CIRC_CHECK(srcOffset,circ_size);

   dstOffset = srcOffset - rotate;
   dstOffset = CIRC_CHECK(dstOffset,circ_size);

   // would need three copies in worst case
   for(i=0;i<3;i++)
   {
       if(srcOffset >= circ_size) srcOffset -= circ_size;
       if(dstOffset >= circ_size) dstOffset -= circ_size;
       c = MIN(circ_size-srcOffset,circ_size-dstOffset);
       c = MIN(c,size-copied);
       if(c)cpl_memcpy(&p[dstOffset],&p[srcOffset],c);
       dstOffset += c;
       srcOffset += c;
	   copied    += c;
   }
}

//
// This function moves the memory to right
// 
// This function is used by CPL_fifoMoveReadLocation
// Function achives this by three memmoves
// 
void CPL_rotateRight_(unsigned char * restrict p,int circ_size, int offset, int size, int rotate)
{
   int dstOffset;
   int srcOffset;
   int i,copied=0,c,count[3],dst[3],src[3];

   DBGPRNT(NULL,"CPL_rotateRight");
   srcOffset = offset;
   srcOffset = CIRC_CHECK(srcOffset,circ_size);

   dstOffset = srcOffset + rotate;
   dstOffset = CIRC_CHECK(dstOffset,circ_size);

   // would need three moves in worst case
   for(i=0;i<3;i++)
   {
       if(srcOffset >= circ_size) srcOffset -= circ_size;
       if(dstOffset >= circ_size) dstOffset -= circ_size;
       c = MIN(circ_size-srcOffset,circ_size-dstOffset);
       c = MIN(c,size-copied);
	   count[i] = c;
	   dst[i] = dstOffset;
	   src[i] = srcOffset;
       dstOffset += c;
       srcOffset += c;
	   copied    += c;
   }
   for(i=2;i>=0;i--)
   {
     if(count[i]) memmove(&p[dst[i]],&p[src[i]],count[i]);
   }
}

