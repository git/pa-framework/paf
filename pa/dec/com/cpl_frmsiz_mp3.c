
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// mp3 frame size calculation
//
//
// 

#include "cpl_frmsiz_mp3.h"

//
//  mp3 header description
//
//  aaaaaaaa aaabbccd eeeeffgh iijjklmm
//
//
// Sign Length(bits) Position(bits) Description
// ---- ------------ -------------- -----------
//
// a    11           (31-21)        Frame sync (all bits must be set)
//
// b    2            (20,19)        MPEG Audio version ID
//                                  00 - MPEG Version 2.5 (later extension of
//                                       MPEG 2)
//                                  01 - reserved
//                                  10 - MPEG Version 2 (ISO/IEC 13818-3)
//                                  11 - MPEG Version 1 (ISO/IEC 11172-3)
//
// c    2            (18,17)        Layer description
//                                  00 - reserved
//                                  01 - Layer III
//                                  10 - Layer II
//                                  11 - Layer I
//
// d    1            (16)           Protection bit
//                                  0 - Protected by CRC (16bit CRC follows
//                                        header)
//                                  1 - Not protected
//
// e    4            (15,12)        Bitrate index
//                                  bits|V1,L1|V1,L2|V1,L3|V2,L1|V2, L2 & L3
//                                  0000|free |free |free |free |free
//                                  0001|32   |32   |32   |32   |8
//                                  0010|64   |48   |40   |48   |16
//                                  0011|96   |56   |48   |56   |24
//                                  0100|128  |64   |56   |64   |32
//                                  0101|160  |80   |64   |80   |40
//                                  0110|192  |96   |80   |96   |48
//                                  0111|224  |112  |96   |112  |56
//                                  1000|256  |128  |112  |128  |64
//                                  1001|288  |160  |128  |144  |80
//                                  1010|320  |192  |160  |160  |96
//                                  1011|352  |224  |192  |176  |112
//                                  1100|384  |256  |224  |192  |128
//                                  1101|416  |320  |256  |224  |144
//                                  1110|448  |384  |320  |256  |160
//                                  1111|bad  |bad  |bad  |bad  |bad
//
//                                  NOTES: All values are in kbps
//                                  V1 - MPEG Version 1
//                                  V2 - MPEG Version 2 and Version 2.5
//                                  L1 - Layer I
//                                  L2 - Layer II
//                                  L3 - Layer III
//
//                                  "free" means free format. The free bitrate
//                                  must remain constant, an must be lower 
//                                  than the maximum allowed bitrate. Decoders
//                                  are not required to support decoding of 
//                                  free bitrate streams.
//                                  "bad" means that the value is unallowed.
//
//                                  MPEG files may feature variable bitrate 
//                                  (VBR). Each frame may then be created with
//                                   a different bitrate. It may be used in 
//                                  all layers. Layer III decoders must 
//                                  support this method. Layer I & II decoders
//                                   may support it.
//
//                                  For Layer II there are some combinations 
//                                  of bitrate and mode which are not allowed.
//                                  Here is a list of allowed combinations.
//
//                                  bitrate|single |stereo|intensity|dual
//                                         |channel|      |stereo   |channel
//                                  free   |yes    |yes   |yes      |yes
//                                  32     |yes    |no    |no       |no
//                                  48     |yes    |no    |no       |no
//                                  56     |yes    |no    |no       |no
//                                  64     |yes    |yes   |yes      |yes
//                                  80     |yes    |no    |no       |no
//                                  96     |yes    |yes   |yes      |yes
//                                  112    |yes    |yes   |yes      |yes
//                                  128    |yes    |yes   |yes      |yes
//                                  160    |yes    |yes   |yes      |yes
//                                  192    |yes    |yes   |yes      |yes
//                                  224    |no     |yes   |yes      |yes
//                                  256    |no     |yes   |yes      |yes
//                                  320    |no     |yes   |yes      |yes
//                                  384    |no     |yes   |yes      |yes
//
// f    2            (11,10)        Sampling rate frequency index
//                                  bits     MPEG1     MPEG2     MPEG2.5
//                                  00     44100 Hz     22050 Hz     11025 Hz
//                                  01     48000 Hz     24000 Hz     12000 Hz
//                                  10     32000 Hz     16000 Hz     8000 Hz
//                                  11     reserv.     reserv.     reserv.
//
// g    1            (9)            Padding bit
//                                  0 - frame is not padded
//                                  1 - frame is padded with one extra slot
//
//                                  Padding is used to exactly fit the bitrate.
//                                  As an example: 128kbps 44.1kHz layer II uses
//                                  a lot of 418 bytes and some of 417 bytes 
//                                  long frames to get the exact 128k bitrate.
//                                  For Layer I slot is 32 bits long, for Layer
//                                  II and Layer III slot is 8 bits long.
//
// h    1            (8)            Private bit. This one is only informative.
//
// i    2            (7,6)          Channel Mode
//                                  00 - Stereo
//                                  01 - Joint stereo (Stereo)
//                                  10 - Dual channel (2 mono channels)
//                                  11 - Single channel (Mono)
//
//                                  Note: Dual channel files are made of two 
//                                  independant mono channel. Each one uses 
//                                  exactly half the bitrate of the file. Most 
//                                  decoders output them as stereo, but it might
//                                  not always be the case.
//                                  One example of use would be some speech in 
//                                  two different languages carried in the same
//                                  bitstream, and then an appropriate decoder 
//                                  would decode only the choosen language.
//
// j    2            (5,4)          Mode extension (Only used in Joint stereo)
//                                  Mode extension is used to join informations
//                                  that are of no use for stereo effect, thus 
//                                  reducing needed bits. These bits are 
//                                  dynamically determined by an encoder in 
//                                  Joint stereo mode, and Joint Stereo can be 
//                                  changed from one frame to another, or even 
//                                  switched on or off.
//
//                                  Complete frequency range of MPEG file is 
//                                  divided in subbands There are 32 subbands. 
//                                  For Layer I & II these two bits determine 
//                                  frequency range (bands) where intensity 
//                                  stereo is applied. For Layer III these two 
//                                  bits determine which type of joint stereo 
//                                  is used (intensity stereo or m/s stereo).
//                                  Frequency range is determined within 
//                                  decompression algorithm.
//
//                                  Layer I and II        Layer III
//                                  --------------        ---------
//                                  value|Layer I & II    |intensity |MS
//                                       |                |stereo    |stereo
//                                  00   |bands 4 to 31   |off       |off
//                                  01   |bands 8 to 31   |on        |off
//                                  10   |bands 12 to 31  |off       |on
//                                  11   |bands 16 to 31  |on        |on
//
// k    1            (3)            Copyright
//                                  0 - Audio is not copyrighted
//                                  1 - Audio is copyrighted
//                                  The copyright has the same meaning as the 
//                                  copyright bit on CDs and DAT tapes, i.e. 
//                                  telling that it is illegal to copy the 
//                                  contents if the bit is set.
//
// l    1            (2)           Original
//                                  0 - Copy of original media
//                                  1 - Original media
//
//                                  The original bit indicates, if it is set, 
//                                  that the frame is located on its original 
//                                  media.
//
// m    2            (1,0)          Emphasis
//                                  00 - none
//                                  01 - 50/15 ms
//                                  10 - reserved
//                                  11 - CCIT J.17


//   v<version>s<frequecy index>
#define v1s0(a) (144*(a/44.10f))
#define v1s1(a) (144*(a/48.00f))
#define v1s2(a) (144*(a/32.00f))

#define v2s0(a) (72*(a/22.05f))
#define v2s1(a) (72*(a/24.00f))
#define v2s2(a) (72*(a/16.00f))

#define v2_5s0(a) (72*(a/11.025f))
#define v2_5s1(a) (72*(a/12.00f))
#define v2_5s2(a) (72*(a/8.00f))

// table of framesize [freq index][mpeg version][bitrate index]
const short CPL_frmsiz_MP3[3][3][16] =
{
    { // sampling frequency index 0
        {       0,v1s0( 32),v1s0( 40),v1s0( 48),
         v1s0( 56),v1s0( 64),v1s0( 80),v1s0( 96),
         v1s0(112),v1s0(128),v1s0(160),v1s0(192),
         v1s0(224),v1s0(256),v1s0(320),-1}, // V1
        {       0,v2s0(  8),v2s0( 16),v2s0( 24),
         v2s0( 32),v2s0( 40),v2s0( 48),v2s0( 56),
         v2s0( 64),v2s0( 80),v2s0( 96),v2s0(112),
         v2s0(128),v2s0(144),v2s0(160),-1}, // V2
        {       0,v2_5s0(  8),v2_5s0( 16),v2_5s0( 24),
         v2_5s0( 32),v2_5s0( 40),v2_5s0( 48),v2_5s0( 56),
         v2_5s0( 64),v2_5s0( 80),v2_5s0( 96),v2_5s0(112),
         v2_5s0(128),v2_5s0(144),v2_5s0(160),-1}, // V2.5
	},
    { // sampling frequency index 1
        {       0,v1s1( 32),v1s1( 40),v1s1( 48),
         v1s1( 56),v1s1( 64),v1s1( 80),v1s1( 96),
         v1s1(112),v1s1(128),v1s1(160),v1s1(192),
         v1s1(224),v1s1(256),v1s1(320),-1}, // V1
        {       0,v2s1(  8),v2s1( 16),v2s1( 24),
         v2s1( 32),v2s1( 40),v2s1( 48),v2s1( 56),
         v2s1( 64),v2s1( 80),v2s1( 96),v2s1(112),
         v2s1(128),v2s1(144),v2s1(160),-1}, // V2
        {       0,v2_5s1(  8),v2_5s1( 16),v2_5s1( 24),
         v2_5s1( 32),v2_5s1( 40),v2_5s1( 48),v2_5s1( 56),
         v2_5s1( 64),v2_5s1( 80),v2_5s1( 96),v2_5s1(112),
         v2_5s1(128),v2_5s1(144),v2_5s1(160),-1}, // V2.5
	},
    { // sampling frequency index 2
        {       0,v1s2( 32),v1s2( 40),v1s2( 48),
         v1s2( 56),v1s2( 64),v1s2( 80),v1s2( 96),
         v1s2(112),v1s2(128),v1s2(160),v1s2(192),
         v1s2(224),v1s2(256),v1s2(320),-1}, // V1
        {       0,v2s2(  8),v2s2( 16),v2s2( 24),
         v2s2( 32),v2s2( 40),v2s2( 48),v2s2( 56),
         v2s2( 64),v2s2( 80),v2s2( 96),v2s2(112),
         v2s2(128),v2s2(144),v2s2(160),-1}, // V2
        {       0,v2_5s2(  8),v2_5s2( 16),v2_5s2( 24),
         v2_5s2( 32),v2_5s2( 40),v2_5s2( 48),v2_5s2( 56),
         v2_5s2( 64),v2_5s2( 80),v2_5s2( 96),v2_5s2(112),
         v2_5s2(128),v2_5s2(144),v2_5s2(160),-1}, // V2.5
	},
};

// 
// function to do error check and framesize calculation
// for mp3
//
// return frame size in bytes or error (0)
//

int CPL_calcFSizeMP3(unsigned char* p, int s)
{
    int frameSize;
    unsigned char IDex,layeridx; //,ID,crc;
    unsigned char bitrateidx,samplerateidx,padding;
    unsigned char emphasis; //copyright,original;
    int versionidx;
    
    if(s<4) return 0; // not sufficient bytes to find frameSize

    layeridx = ((p[1]&0x6)>>1)&0x3;

    IDex = (p[1]>>3)&0x3; // mpeg version ID: 0-mpeg 2.5, 1-reserved
	                      // 2- mpeg 2, 3- mpeg 1

	// mpeg version index (for indexing into table) 
	// 0 - mpeg 1, 1 - mpeg 2, 2 - mpeg 2.5, -1 - error
    versionidx = (IDex==0x3)?0:(IDex==0x2)?1:(IDex==0x0)?2:-1;

    bitrateidx = (p[2]>>4)&0xf;
    samplerateidx = (p[2]& 0xc)>>2;
    padding = ((p[2]& 0x2)>>1);
    emphasis = p[3]&0x3;

    if(samplerateidx > 2) return 0;
    if(bitrateidx == 0 || bitrateidx == 15) return 0;
    if(layeridx != 1) return 0; // not version 3
    if(emphasis == 2) return 0; // reserved emphasis
	if(versionidx < 0) return 0;
    
    frameSize = CPL_frmsiz_MP3[samplerateidx][versionidx][bitrateidx];
    frameSize += padding;

    return frameSize;
}

