
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library  function table structure
//
//
//

#if (PAF_DEVICE&0xFF000000) == 0xD8000000 // ----------------------------------

#include <cpl.h>

#else  // PAF_DEVICE = 0xD8000000 ---------------------------------------------

#ifndef CPL_FXNS_
#define CPL_FXNS_

typedef struct CPL_TII_Fxns_Hidden {
    void (*pCPL_cdm_downmixSetUp)(void *pv, PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
    void (*pCPL_cdm_samsiz)(void *pv,PAF_AudioFrame *pAudioFrame,CPL_CDM *pCDMConfig);
    void (*pCPL_cdm_downmixApply)(void *pv,PAF_AudioData * pIn[],PAF_AudioData * pOut[],CPL_CDM *pCDMConfig,int sampleCount);
    void (*pCPL_cdm_downmixConfig)(void *pv,CPL_CDM *pCDMConfig);
	void (*pCDMTo_Mono)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Stereo)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Phantom1)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Phantom2)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Phantom3)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Phantom4)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_3Stereo)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Surround1)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Surround2)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Surround3)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);
	void (*pCDMTo_Surround4)(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig);		
    
    void (*fft)(int n, float * ptr_x, const float * ptr_w, float * ptr_y, 
               const unsigned char * brev, int n_min, int offset, int n_max);
    void (*vecAdd)(float *pIn1, float *pIn2, float *pOut, int n);
    void (*vecClip)(float *pIn, float max, float min, float *pOut, int n);
    float (*vecClip1)(float );
    void (*vecLinear2)(float c1, float *pIn1, float c2, float *pIn2, float *pOut, int n);
    void (*vecScale)(float c, float *pIn, float *pOut, int n);
    void (*vecSet)(float val, float *pOut, int n);
    void (*ivecCopy)(int *pIn, int *pOut, int n);
    void (*svecSet)(int val, short *pOut, int n);
    void (*ivecSet)(int val, int *pOut, int n);    
    void (*imaskScale)(int mask, float scale, int * restrict pIn, float * restrict pOut, int stride, int n);
    void (*smaskScale)(int mask, float scale, short * restrict pIn, float * restrict pOut, int stride, int n);
    void (*vecStrideScale)(float scale, float * restrict pIn, 
        float * restrict pOut, int stride, int n);
	void (*Modulation_new)( float *input,float *off_inp, float *restrict output,int n,
	                     const float *costable , const float *sintable, int off);
    void (*Demodulation_new)(float *input, float *restrict output,int n,
                 const float *costable,const float *sintable,int sign,int off,int flag);
     void (*window_aac_ac3)(const float *dnmixptr1,const float *delayptr1, 
					const float *win,int N,int mul,  float *restrict pcmptr1,
                    float *restrict pcmptr2, int sign );
                     
    Int (*setAudioFrame)(IALG_Handle handle, PAF_AudioFrame *pAudioFrame, Int cnt, PAF_DecodeStatus * pDecodeStatus);
    void (*vecLinear2Incr)(float c1, float Incr, float * restrict pIn1, float c2, float * restrict pIn2, float * restrict pOut, int nSamples);
    void (*vecLinear2Common)(float c, float * restrict pIn1, float * restrict pIn2, float * restrict pOut, int nSamples);
    void (*vecScaleAcc)(float c, float * restrict pIn, float * restrict pOut, int n);
    void (*vecScaleIncr)(float c, float Incr, float * restrict pIn, float * restrict pOut, int n);
    void (*vecSetArr)(float val, float ** restrict pOut, int n, int a);
    float (*GetPAFSampleRate)(int);
    int  (*GetNumSat)(int); 
    void (*intDelayProc)(WaDelay *, float *, float *, int );
    void (*floatDelayProc)(WaDelay *, float *, float *, int );
    void (*DelaySet)(WaDelay *, int);
    void (*DelayInit)(WaDelay *, int , int , int );
    void (*sumDiff)(float *p1, float *p2, int n);
    int (*delay)(PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, Int apply, Int channel, Int unit);
    Int (*delayDAT)(PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, PAF_AudioData * restrict swap, Int apply, Int channel, Int unit);

	int (*fifoInit) (CPL_Fifo * pFifo);
	int (*fifoSize) (const CPL_Fifo *pFifo);
	int (*fifoSizeFull) (const CPL_Fifo *pFifo);
	int (*fifoSizeFree) (const CPL_Fifo *pFifo);
	int (*fifoSizeFullLinear) (const CPL_Fifo *pFifo);
	int (*fifoSizeFreeLinear) (const CPL_Fifo *pFifo);
	int (*fifoRead) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoWrite) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoReadNoPosMv) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoGetReadPtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoReadDone) (CPL_Fifo * pFifo, int size);
	int (*fifoGetWritePtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoWrote) (CPL_Fifo * pFifo, int size);
	int (*fifoMoveReadLocation) (CPL_Fifo * pFifo, int location);
	void (*rotateLeft) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);
	void (*rotateRight) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);

} CPL_TII_Fxns_Hidden;
//extern CPL_TII_Fxns CPL_TII_ICPL;

#endif /* CPL_FXNS_ */

#endif // PAF_DEVICE = 0xD8000000 ---------------------------------------------
