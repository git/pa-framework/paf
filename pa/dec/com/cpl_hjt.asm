
*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
    .global _CPL_cdm_downmixSetUp
_CPL_cdm_downmixSetUp:
    B       L0
    MVK       0,     A0
    NOP     4
    .global _CPL_cdm_samsiz
_CPL_cdm_samsiz:
    B       L0
    MVK       1,     A0
    NOP     4
    .global _CPL_cdm_downmixApply
_CPL_cdm_downmixApply:
    B       L0
    MVK       2,     A0
    NOP     4
    .global _CPL_cdm_downmixConfig
_CPL_cdm_downmixConfig:
    B       L0
    MVK       3,     A0
    NOP     4
    .global _CDMTo_Mono
_CDMTo_Mono:
    B       L0
    MVK       4,     A0
    NOP     4
    .global _CDMTo_Stereo
_CDMTo_Stereo:
    B       L0
    MVK       5,     A0
    NOP     4
    .global _CDMTo_Phantom1
_CDMTo_Phantom1:
    B       L0
    MVK       6,     A0
    NOP     4
    .global _CDMTo_Phantom2
_CDMTo_Phantom2:
    B       L0
    MVK       7,     A0
    NOP     4
    .global _CDMTo_Phantom3
_CDMTo_Phantom3:
    B       L0
    MVK       8,     A0
    NOP     4
    .global _CDMTo_Phantom4
_CDMTo_Phantom4:
    B       L0
    MVK       9,     A0
    NOP     4
    .global _CDMTo_3Stereo
_CDMTo_3Stereo:
    B       L0
    MVK      10,     A0
    NOP     4
    .global _CDMTo_Surround1
_CDMTo_Surround1:
    B       L0
    MVK      11,     A0
    NOP     4
    .global _CDMTo_Surround2
_CDMTo_Surround2:
    B       L0
    MVK      12,     A0
    NOP     4
    .global _CDMTo_Surround3
_CDMTo_Surround3:
    B       L0
    MVK      13,     A0
    NOP     4
    .global _CDMTo_Surround4
_CDMTo_Surround4:
    B       L0
    MVK      14,     A0
    NOP     4
    .global _CPL_fft
_CPL_fft:
    B       L0
    MVK      15,     A0
    NOP     4
    .global _CPL_vecAdd
_CPL_vecAdd:
    B       L0
    MVK      16,     A0
    NOP     4
    .global _CPL_vecLinear2
_CPL_vecLinear2:
    B       L0
    MVK      19,     A0
    NOP     4
    .global _CPL_vecScale
_CPL_vecScale:
    B       L0
    MVK      20,     A0
    NOP     4
    .global _CPL_vecSet
_CPL_vecSet:
    B       L0
    MVK      21,     A0
    NOP     4
    .global _CPL_ivecCopy
_CPL_ivecCopy:
    B       L0
    MVK      22,     A0
    NOP     4
    .global _CPL_svecSet
_CPL_svecSet:
    B       L0
    MVK      23,     A0
    NOP     4
    .global _CPL_ivecSet
_CPL_ivecSet:
    B       L0
    MVK      24,     A0
    NOP     4
    .global _CPL_imaskScale
_CPL_imaskScale:
    B       L0
    MVK      25,     A0
    NOP     4
    .global _CPL_smaskScale
_CPL_smaskScale:
    B       L0
    MVK      26,     A0
    NOP     4
    .global _CPL_vecStrideScale
_CPL_vecStrideScale:
    B       L0
    MVK      27,     A0
    NOP     4
    .global _CPL_Modulation_new
_CPL_Modulation_new:
    B       L0
    MVK      28,     A0
    NOP     4
    .global _CPL_Demodulation_new
_CPL_Demodulation_new:
    B       L0
    MVK      29,     A0
    NOP     4
    .global _CPL_window_aac_ac3
_CPL_window_aac_ac3:
    B       L0
    MVK      30,     A0
    NOP     4
    .global _CPL_setAudioFrame
_CPL_setAudioFrame:
    B       L0
    MVK      31,     A0
    NOP     4
    .global _CPL_vecLinear2Incr
_CPL_vecLinear2Incr:
    B       L0
    MVK      32,     A0
    NOP     4
    .global _CPL_vecLinear2Common
_CPL_vecLinear2Common:
    B       L0
    MVK      33,     A0
    NOP     4
    .global _CPL_vecScaleAcc
_CPL_vecScaleAcc:
    B       L0
    MVK      34,     A0
    NOP     4
    .global _CPL_vecScaleIncr
_CPL_vecScaleIncr:
    B       L0
    MVK      35,     A0
    NOP     4
    .global _CPL_vecSetArr
_CPL_vecSetArr:
    B       L0
    MVK      36,     A0
    NOP     4
    .global _CPL_GetPAFSampleRate
_CPL_GetPAFSampleRate:
    B       L0
    MVK      37,     A0
    NOP     4
    .global _CPL_GetNumSat
_CPL_GetNumSat:
    B       L0
    MVK      38,     A0
    NOP     4
    .global _CPL_intDelayProc
_CPL_intDelayProc:
    B       L0
    MVK      39,     A0
    NOP     4
    .global _CPL_floatDelayProc
_CPL_floatDelayProc:
    B       L0
    MVK      40,     A0
    NOP     4
    .global _CPL_DelaySet
_CPL_DelaySet:
    B       L0
    MVK      41,     A0
    NOP     4
    .global _CPL_DelayInit
_CPL_DelayInit:
    B       L0
    MVK      42,     A0
    NOP     4
    .global _CPL_sumDiff
_CPL_sumDiff:
    B       L0
    MVK      43,     A0
    NOP     4
    .global _CPL_delay
_CPL_delay:
    B       L0
    MVK      44,     A0
    NOP     4
    .global _CPL_delayDAT
_CPL_delayDAT:
    B       L0
    MVK      45,     A0
    NOP     4
	.global _CPL_fifoInit
_CPL_fifoInit:
	B		L0
    MVK      46,     A0
    NOP     4
	.global _CPL_fifoSize
_CPL_fifoSize:
	B		L0
    MVK      47,     A0
    NOP     4
	.global _CPL_fifoSizeFull
_CPL_fifoSizeFull:
	B		L0
    MVK      48,     A0
    NOP     4
	.global _CPL_fifoSizeFree
_CPL_fifoSizeFree:
	B		L0
    MVK      49,     A0
    NOP     4
	.global _CPL_fifoSizeFullLinear
_CPL_fifoSizeFullLinear:
	B		L0
    MVK      50,     A0
    NOP     4
	.global _CPL_fifoSizeFreeLinear
_CPL_fifoSizeFreeLinear:
	B		L0
    MVK      51,     A0
    NOP     4
	.global _CPL_fifoRead
_CPL_fifoRead:
	B		L0
    MVK      52,     A0
    NOP     4
	.global _CPL_fifoWrite
_CPL_fifoWrite:
	B		L0
    MVK      53,     A0
    NOP     4
	.global _CPL_fifoReadNoPosMv
_CPL_fifoReadNoPosMv:
	B		L0
    MVK      54,     A0
    NOP     4
	.global _CPL_fifoGetReadPtr
_CPL_fifoGetReadPtr:
	B		L0
    MVK      55,     A0
    NOP     4
	.global _CPL_fifoReadDone
_CPL_fifoReadDone:
	B		L0
    MVK      56,     A0
    NOP     4
	.global _CPL_fifoGetWritePtr
_CPL_fifoGetWritePtr:
	B		L0
    MVK      57,     A0
    NOP     4
	.global _CPL_fifoWrote
_CPL_fifoWrote:
	B		L0
    MVK      58,     A0
    NOP     4
	.global _CPL_fifoMoveReadLocation
_CPL_fifoMoveReadLocation:
	B		L0
    MVK      59,     A0
    NOP     4
	.global _CPL_rotateLeft
_CPL_rotateLeft:
	B		L0
    MVK      60,     A0
    NOP     4
	.global _CPL_rotateRight
_CPL_rotateRight:
	B		L0
    MVK      61,     A0
    NOP     4
    .global _CPL_TII_ICPL_HIDDEN
L0: 
    MVKL    _CPL_TII_ICPL_HIDDEN, A1
    MVKH    _CPL_TII_ICPL_HIDDEN, A1
    LDW     *A1[A0], A2
    NOP     4
    B       A2
    NOP     5
