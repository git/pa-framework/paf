
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library function table
//
//
//

#include "cpl_cdm.h"
#include "cpl_fft.h"
#include "cpl_vecops.h"
#include "cpl_moddemod.h"
#include "cpl_window.h"
#include "cpl_dec.h"
#include "cpl_delay.h"
#include "cpl_fifo.h"

#include "cpl_fxns.h"

#include "cpl_cdm_fxns.h"

CPL_TII_Fxns_Hidden CPL_TII_ICPL_HIDDEN = {
    CPL_cdm_downmixSetUp_,
    CPL_cdm_samsiz_,
    CPL_cdm_downmixApply_,
    CPL_cdm_downmixConfig_,
	CDMTo_Mono_,
	CDMTo_Stereo_,
	CDMTo_Phantom1_,
	CDMTo_Phantom2_,
	CDMTo_Phantom3_,
	CDMTo_Phantom4_,
	CDMTo_3Stereo_,
	CDMTo_Surround1_,
	CDMTo_Surround2_,
	CDMTo_Surround3_,
	CDMTo_Surround4_,

    CPL_fft_,
    CPL_vecAdd_,
    NULL,
    NULL,
    CPL_vecLinear2_,
    CPL_vecScale_,
    CPL_vecSet_,
    CPL_ivecCopy_,
    CPL_svecSet_,
    CPL_ivecSet_,
    CPL_imaskScale_,
    CPL_smaskScale_,
    CPL_vecStrideScale_,
	CPL_Modulation_new_,
	CPL_Demodulation_new_,
	CPL_window_aac_ac3_,

    CPL_setAudioFrame_,
    CPL_vecLinear2Incr_,
    CPL_vecLinear2Common_,
    CPL_vecScaleAcc_,
    CPL_vecScaleIncr_,
    CPL_vecSetArr_,
    CPL_GetPAFSampleRate_,
    CPL_GetNumSat_,
    CPL_intDelayProc_,
    CPL_floatDelayProc_,
    CPL_DelaySet_,
    CPL_DelayInit_,
    CPL_sumDiff_,
    CPL_delay_,
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD7000000)
    CPL_delayDAT_,
#else
	NULL,
#endif

	CPL_fifoInit_,
	CPL_fifoSize_,
	CPL_fifoSizeFull_,
	CPL_fifoSizeFree_,
	CPL_fifoSizeFullLinear_,
	CPL_fifoSizeFreeLinear_,
	CPL_fifoRead_,
	CPL_fifoWrite_,
	CPL_fifoReadNoPosMv_,
	CPL_fifoGetReadPtr_,
	CPL_fifoReadDone_,
	CPL_fifoGetWritePtr_,
	CPL_fifoWrote_,
	CPL_fifoMoveReadLocation_,
	CPL_rotateLeft_,
	CPL_rotateRight_,
};

