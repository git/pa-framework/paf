
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library -- common modulation across AAC and AC3
//
//
//

#include "cpl_moddemod.h"


void CPL_Modulation_new_(float *input, float *off_inp,float *restrict output,int n,const float *costable,
                         const float *sintable, int off)
{
		float *restrict tcrptr, *restrict tciptr  ;
		const float *cosptr, *sinptr;
		float ar, ai, cr, ci;
		int count,x;
			
    	x = 2*off;
		tcrptr = off_inp;
		tciptr = input;
		cosptr = costable;
		sinptr = sintable;
	    
		for (count = 0; count < n/2; count++)
		{
			cr = *cosptr++;
			ci = *sinptr++;
			ar = *tcrptr;
			ai = *tciptr;				
			*output++ = ai * cr + ar * ci;
			*output++ = ar * cr - ai * ci;	
			tcrptr -= x;
			tciptr += x;
		}

}


void CPL_Demodulation_new_(float *input, float *restrict output,int n,
                 const float *costable, const float *sintable,
                 int sign , int off, int flag)
{
 	const float *fftrptr;
	float *restrict tcrptr1,*restrict tcrptr2;
	const float *cosptr, *sinptr;
	float ar, ai, cr, ci;
	int count,x,y;

	    
    	fftrptr = input;				
		cosptr = costable;
		sinptr = sintable;  
        tcrptr1 = output;		
        if(flag)
		{			
			 tcrptr2 = output+n-1; 
			 x = 2;
			 y = -2;
        }
        else
        {
			 tcrptr2 = output+ (off * n/2); 
			 x = 1;
			 y = 1;         
        }  
		for (count = 0; count < n/2; count++)
		{
			cr = *cosptr++;
			ci = *sinptr++;
			ai = *fftrptr++;
			ar = *fftrptr++;

			//tcrptr[count*2] =  ar * cr - ai * ci;
			//tcrptr[t- (count*2)] = ( ai * cr + ar * ci) * sign;
			
			*tcrptr1 =  ar * cr - ai * ci;			
			*tcrptr2 = ( ai * cr + ar * ci) * sign; 
			tcrptr2 += y;
			tcrptr1 += x;

		}

}


