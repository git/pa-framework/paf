
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// 
// 
//  Common PA Library -- Antara Single precision FFT for complex input
// 
// 


#include <cpl.h>
#include <paftyp_a.h>
#include <paftyp.h>
#include <pafdec.h>
#include <math.h>

void CPL_fft_asm (int n, float *ptr_x, const float *ptr_w, float *ptr_y,
				  const unsigned char *brev, int n_min, int offset, int n_max);

#if (PAF_DEVICE&0xFF000000) == 0xD8000000

void CPL_fft_ (int n, float *ptr_x, const float *ptr_w, float *ptr_y, const unsigned char *brev, int n_min, int offset, int n_max)
{
	unsigned int oldmask;

	// Disable the Interrupts Before Calling the Function in ROM 
	oldmask = _disable_interrupts ();

	CPL_fft_asm (n, ptr_x, ptr_w, ptr_y, brev, n_min, offset, n_max);

	// Enable the Interrupts. 
	_restore_interrupts (oldmask);

}

#else /* PAF_DEVICE = 0xD8000000 */

#include <hwi.h>
#include <tsk.h>

void CPL_fft_ (int n, float *ptr_x, const float *ptr_w, float *ptr_y, const unsigned char *brev, int n_min, int offset, int n_max)
{
	unsigned int oldmask;

	// Disable the Interrupts Before Calling the Function in ROM 
	TSK_disable ();
	oldmask = HWI_disable ();

	CPL_fft_asm (n, ptr_x, ptr_w, ptr_y, brev, n_min, offset, n_max);

	// Enable the Interrupts. 
	HWI_restore (oldmask);
	TSK_enable ();

}

#endif /* PAF_DEVICE = 0xD8000000 */
