
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Decode Wrapper functionality for Legacy decoders to work with off-chip input buffer.
//
//
//

#include <std.h>
#include <log.h>
#include  <std.h>
#include <xdas.h>
#include <alg.h> /* for SIO */
#include <pafdec.h>
#include <paftyp.h>
#include <dwr_inp.h>
#include <pcm.h>
#include "cpl.h"

#if (PAF_DEVICE&0xFF000000) == 0xD8000000
#include <pafhjt.h>
#else  /* PAF_DEVICE = 0xD8000000 */
#ifdef _TMS320C6700_PLUS // only for Antara 
#include <dmax_dat.h>
#endif
#endif /* PAF_DEVICE = 0xD8000000 */

extern void *cpl_memcpy(void *s1, void *s2, register size_t n);

#define MAX_INPUT_FRAME PAF_MAXNUMCHAN*1024

Void DWR_TII_activate(IALG_Handle handle)
{
    DWR_TII_Obj *dwr      = (Void *)handle;
    IALG_Handle decHandle = (IALG_Handle)(dwr->decHandle);
 
    COM_TII_snatchCommon (handle, dwr->pCommonLock);
    decHandle->fxns->algActivate (decHandle);
}

Int DWR_TII_alloc(const IALG_Params *algParams,
                  IALG_Fxns **pf, IALG_MemRec memTab[])
{
    int num;
    IDWR_Params *dwrParams = (IDWR_Params *)algParams;
    const IALG_Fxns *decTable = dwrParams->decTable;

    memTab[0].size      = ((sizeof(DWR_TII_Obj)+3)/4*4) + ((sizeof(IDWR_Active)+3)/4*4);
    memTab[0].alignment = 4;
    memTab[0].space     = IALG_SARAM;
    memTab[0].attrs     = IALG_PERSIST;

    memTab[1].size      = (sizeof(PAF_IALG_Common)+ 3)/4 * 4;
    memTab[1].alignment = 4;
    memTab[1].space     = IALG_SARAM;
    memTab[1].attrs     = PAF_IALG_COMMONN(1);

    memTab[2].size      = (MAX_INPUT_FRAME + 127)/128 * 128;
    memTab[2].alignment = 128;
    memTab[2].space     = IALG_SARAM;
    memTab[2].attrs     = PAF_IALG_COMMONN(1);

    num = decTable->algAlloc (dwrParams->decParam, pf , &memTab[3]);

    return num+3;
}
  
void DWR_TII_deactivate(IALG_Handle handle)
{
    DWR_TII_Obj *dwr      = (Void *)handle;
    IALG_Handle decHandle = (IALG_Handle)(dwr->decHandle);

    COM_TII_deactivateCommon ((IALG_Handle)handle, dwr->pCommonLock);
    decHandle->fxns->algDeactivate (decHandle);
}


Int DWR_TII_initObj(IALG_Handle handle,
                    const IALG_MemRec memTab[], IALG_Handle p,
                    const IALG_Params *algParams)
{
    DWR_TII_Obj     *dwr       = (Void *)handle;
    IALG_Handle      decHandle;	
    IDWR_Params     *dwrParams  = (IDWR_Params *)algParams;
    const IALG_Fxns *decTable   = dwrParams->decTable;


    // Set memory pointers.
    dwr->pActive     = (DWR_TII_Active*)((Uns)dwr + ((sizeof(DWR_TII_Obj)+3)/4*4));
    dwr->pCommonLock = memTab[1].base;

    // initialize input buffer info
    dwr->commonInp.pntr.pVoid   = (XDAS_UInt8*) memTab[2].base;
    dwr->commonInp.base         = dwr->commonInp.pntr;
    dwr->commonInp.sizeofBuffer = memTab[2].size;

    dwr->decHandle        = memTab[3].base;
    dwr->decHandle->fxns  = (IALG_Fxns *)decTable;

    decHandle = (IALG_Handle)(dwr->decHandle);
    decHandle->fxns->algInit (decHandle, &memTab[3], p, dwrParams->decParam);	
    return IALG_EOK;
}

Int DWR_TII_control(IALG_Handle handle, IALG_Cmd cmd, IALG_Status *status)
{
    DWR_TII_Obj *dwr = (Void *)handle;
    IALG_Handle decHandle = (IALG_Handle)(dwr->decHandle);

    switch(cmd)
    {
        case PCM_MINSAMGEN:
            return 8;

        case PCM_MAXSAMGEN:
            return 256;
        default:
            return decHandle->fxns->algControl(decHandle, cmd, status);
    }
}

Int
DWR_TII_decode(IDWR_Handle handle, ALG_Handle sioHandle,
               PAF_DecodeInStruct *pDecodeInStruct,
               PAF_DecodeOutStruct *pDecodeOutStruct)
{
    PAF_UnionPointer pntr, base;
    XDAS_Int32 sizeofBuffer;

    PAF_InpBufConfig *pConfig;
    DWR_TII_Obj *dwr = (Void *)handle;
    IPCM_Handle decHandle = (IPCM_Handle)dwr->decHandle;
    Int result=0;

    pConfig = (PAF_InpBufConfig *)((Void *)dwr->pActive->pInpBufConfig);

    // save locally
    pntr         = pConfig->pntr;
    base         = pConfig->base;
    sizeofBuffer = pConfig->sizeofBuffer;

    pConfig->pntr         = dwr->commonInp.pntr;
    pConfig->base         = dwr->commonInp.base;
    pConfig->sizeofBuffer = (XDAS_Int32)dwr->commonInp.sizeofBuffer;

    result = decHandle->fxns->decode (decHandle,sioHandle,pDecodeInStruct,pDecodeOutStruct);
    if(result != 0){
        asm(" NOP");
    }

    // restore
    pConfig->pntr         = pntr;
    pConfig->base         = base;
    pConfig->sizeofBuffer = sizeofBuffer;

    return result;
}

Int
DWR_TII_info(IDWR_Handle handle, ALG_Handle sioHandle, PAF_DecodeControl *pDecodeControl, PAF_DecodeStatus *pDecodeStatus)
{      
    PAF_UnionPointer base;
    PAF_UnionPointer pntr;
    XDAS_Int32       sizeofBuffer;
    XDAS_UInt32      bufEnd;
    XDAS_UInt32      length;

    PAF_InpBufConfig *pConfig;
    DWR_TII_Obj *dwr = (Void *)handle;
    IPCM_Handle decHandle = (IPCM_Handle)dwr->decHandle;
    Int result=0;
    

    dwr->pActive->pInpBufConfig = pDecodeControl->pInpBufConfig;

    pConfig = (PAF_InpBufConfig *)((Void *)dwr->pActive->pInpBufConfig);

    // Assume that writes into pDWR buffer will never wrap since this check
    // guarantees that length to copy is always smaller than the DWR buffer size.
    // And after we force pDWR->pntr to always be at the base.
    length = pConfig->lengthofData * pConfig->sizeofElement;
    if (length > MAX_INPUT_FRAME)
        return IALG_EFAIL;
    dwr->commonInp.pntr = dwr->commonInp.base;

    // if data doesn't wrap input circular buffer then perform single linear copy
    // otherwise split copy into two parts. 
    bufEnd = (XDAS_UInt32) pConfig->base.pVoid + pConfig->sizeofBuffer;
    if (((XDAS_UInt32) pConfig->pntr.pVoid + length) <= bufEnd) {
        cpl_memcpy (dwr->commonInp.pntr.pVoid, pConfig->pntr.pVoid, length);
    }
    else{
        XDAS_UInt32 length1,length2;
        void* next;
        length1 = bufEnd - (XDAS_UInt32) pConfig->pntr.pVoid;
        length2 = length - length1;
        next    = (Void*)((XDAS_UInt32) dwr->commonInp.pntr.pVoid + length1);
        cpl_memcpy (dwr->commonInp.pntr.pVoid, pConfig->pntr.pVoid, length1);
        cpl_memcpy (next, pConfig->base.pVoid, length2);
    }  

    // save locally 
    pntr         = pConfig->pntr;
    base         = pConfig->base;
    sizeofBuffer = pConfig->sizeofBuffer;

    pConfig->pntr         = dwr->commonInp.pntr;
    pConfig->base         = dwr->commonInp.base;
    pConfig->sizeofBuffer = (XDAS_Int32)dwr->commonInp.sizeofBuffer;

    result = decHandle->fxns->info (decHandle, sioHandle, pDecodeControl, pDecodeStatus);

    // restore
    pConfig->pntr         = pntr;
    pConfig->base         = base;
    pConfig->sizeofBuffer = sizeofBuffer;

    return result;
}

Int
DWR_TII_reset(IDWR_Handle handle, ALG_Handle sioHandle,
              PAF_DecodeControl *pDecodeControl,
              PAF_DecodeStatus *pDecodeStatus)
{
    PAF_UnionPointer    pntr, base;
    XDAS_Int32          sizeofBuffer;
    PAF_InpBufConfig   *pConfig;
    DWR_TII_Obj        *dwr = (Void *)handle;
    IPCM_Handle         decHandle = (IPCM_Handle)dwr->decHandle;
    Int                 result;


    pConfig = (PAF_InpBufConfig *) ((Void*) pDecodeControl->pInpBufConfig);

    // reset copy buffer info
    dwr->commonInp.pntr = dwr->commonInp.base;

    // save locally 
    pntr         = pConfig->pntr;
    base         = pConfig->base;
    sizeofBuffer = pConfig->sizeofBuffer;

    pConfig->pntr         = dwr->commonInp.pntr;
    pConfig->base         = dwr->commonInp.base;
    pConfig->sizeofBuffer = (XDAS_Int32) dwr->commonInp.sizeofBuffer;

    result = decHandle->fxns->reset (decHandle,sioHandle,pDecodeControl,pDecodeStatus);

    // restore
    pConfig->pntr         = pntr;
    pConfig->base         = base;
    pConfig->sizeofBuffer = sizeofBuffer;

    return(result);
}
