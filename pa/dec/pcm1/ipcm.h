
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PCM algorithm interface declarations
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the PCM algorithm.
 */
#ifndef IPCM_
#define IPCM_

#include <alg.h>
#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "cpl.h"
#include "icom.h"
#include "pafdec.h"

/*
 *  ======== IPCM_Obj ========
 *  Every implementation of IPCM *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IPCM_Obj {
    struct IPCM_Fxns *fxns;    /* function list: standard, public, private */
} IPCM_Obj;

/*
 *  ======== IPCM_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IPCM_Obj *IPCM_Handle;

/*
 *  ======== IPCM_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IPCM_Status {
    Int size;
    XDAS_Int8 mode;  // 4
    XDAS_Int8 ramp;  // 5
    XDAS_Int8 scaleVolume;  // 6
    XDAS_Int8 LFEDownmixVolume;  // 7
    XDAS_Int8 Unused1[4];        // 8, 9, A, B
    XDAS_Int16 CntrMixLev;        // offset 0xC (12)
    XDAS_Int16 SurrMixLev;        // offset 0xE (15)
    XDAS_Int8 LFEDownmixInclude;  // offset 0x10 (16)
    XDAS_Int8 unused[3];          // offset (17, 18, 19)
    XDAS_Int8 Unused2[4];         // offset 0x14 (20)
    PAF_ChannelConfiguration channelConfigurationProgram;// offset 0x18 (24)
} IPCM_Status;

/*
 *  ======== IPCM_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IPCM_Config {
    Int size;
    XDAS_Int16 minimumFrameLength;
    XDAS_Int16 maximumFrameLength;
    void *pDummy; // CPL_TII_Fxns *pCPL_fxns;
} IPCM_Config;

/*
 *  ======== IPCM_Active ========
 *  Active structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm but which must maintain value
 *  only while the algorithm is running.
 */

typedef struct IPCM_Active {
    Int size;
    Int flag;
    PAF_DecodeStatus *pDecodeStatus;
    const PAF_InpBufConfig *pInpBufConfig;
    XDAS_Int8 rampState;
    XDAS_Int8 unused[3];
    PAF_ChannelMask programMask;
    PAF_ChannelMask programWarp;
} IPCM_Active;

/*
 *  ======== IPCM_Scrach ========
 *  Scrach structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm and which must maintain value
 *  while the algorithm is running during a single invocation.
 */
typedef struct IPCM_Scrach {
    Int size;
    CPL_CDM PCM_CDMConfig;
} IPCM_Scrach;

/*
 *  ======== IPCM_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a PCM object.
 *
 *  Every implementation of IPCM *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IPCM_Params {
    Int size;
    const IPCM_Status *pStatus;
    const IPCM_Config *pConfig;
    const IPCM_Active *pActive;
    const IPCM_Scrach *pScrach;
} IPCM_Params;

/*
 *  ======== IPCM_PARAMS ========
 *  Default instance creation parameters (defined in ipcm.c)
 */
extern const IPCM_Params IPCM_PARAMS;

/*
 *  ======== IPCM_Fxns ========
 *  All implementation's of PCM must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is PCM_<vendor>_IPCM, where
 *  <vendor> is the vendor name.
 */
typedef struct IPCM_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IPCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*info)(IPCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*decode)(IPCM_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);
    /* private */
    Int         (*phase[4])(IPCM_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *, PAF_ChannelMask);
    Int         (*inputCheck)(IPCM_Handle, Int);
    Int         (*inputAudio)(IPCM_Handle, PAF_AudioData *, PAF_AudioData, Int, Int);
} IPCM_Fxns;

/*
 *  ======== IPCM_Cmd ========
 *  The Cmd enumeration defines the control commands for the PCM
 *  control method.
 */
typedef enum IPCM_Cmd {
    IPCM_NULL                   = ICOM_NULL,
    IPCM_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IPCM_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IPCM_GETSTATUS              = ICOM_GETSTATUS,
    IPCM_SETSTATUS              = ICOM_SETSTATUS,
    IPCM_MINSAMGEN,
    IPCM_MININFO,
    IPCM_MAXSAMGEN
} IPCM_Cmd;

#endif  /* IPCM_ */
