
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Buffer function definitions
//
//      THIS FILE IS NOT USED. IT IS PRESENT ONLY FOR HISTORICAL REASONS.
//
//
//

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <paftype.h>
#include <pafbuf.h>

PAF_BufferPointer *
PAF_BufferPointerConstructor(PAF_BufferPointer *this, PAF_BufferPointerFunctions *pFxns, Void *pBase, Int length, Int size, Int precision)
{
    this->fxns = pFxns;
    this->head.pVoid = pBase;
    this->head.pVoid = pBase;
    this->tail.pVoid = pBase;
    this->base.pVoid = pBase;
    this->length = length;
    this->size = size;
    this->precision = precision;
    this->headBits = 0;
    this->tailBits = 0;

    return this;
}

LgInt
PAF_BUF_getBits(PAF_BufferPointer *this, Int nBits)
{
    LgInt value;
    Int sz = 8 * this->size;

    if (this->head.pVoid == this->tail.pVoid)
        /* internal error! */
        return 0;
    if (sz <= 16)
        /* internal error! */
        return 0;
    else if (nBits > sz)
        /* internal error! */
        return 0;
    else if (this->tailBits + nBits < sz) {
        value = (this->fxns->gotWord (this) << this->tailBits) & (~0 << 2*sz-nBits);
        this->tailBits += nBits;
    }
    else {
        value = this->fxns->gotWord (this) << this->tailBits;
        value |= (LgUns )this->fxns->getWord (this) >> sz-this->tailBits;
        value &= (~0 << 2*sz-nBits);
        this->tailBits += nBits - sz;
    }

    return value;
}

LgInt
PAF_BUF_getBitsSigned(PAF_BufferPointer *this, Int nBits)
{
    return PAF_BUF_getBits(this, nBits) >> 32-nBits;
}

LgInt
PAF_BUF_getBitsUnsigned(PAF_BufferPointer *this, Int nBits)
{
    return (LgUns )PAF_BUF_getBits(this, nBits) >> 32-nBits;
}

Int
PAF_BUF_getCheck(PAF_BufferPointer *this)
{
    if (this->base.pVoid == NULL)
        return 1;
    else if (this->head.pVoid == NULL
        || this->head.pVoid < this->base.pVoid)
        return 2;
    else if (this->tail.pVoid == NULL
        || this->tail.pVoid < this->base.pVoid)
        return 3;
    else if (this->length <= 0)
        return 4;

    switch (this->size) {
      case 1:
        if (this->head.pSmInt > this->base.pSmInt + this->length)
            return 2;
        else if (this->tail.pSmInt > this->base.pSmInt + this->length)
            return 3;
        else if ((XDAS_UInt8 )this->precision > 8)
            return 6;
        break;
      case 2:
        if (this->head.pMdInt > this->base.pMdInt + this->length)
            return 2;
        else if (this->tail.pMdInt > this->base.pMdInt + this->length)
            return 3;
        else if ((XDAS_UInt8 )this->precision > 16)
            return 6;
        break;
      case 4:
        if (this->head.pLgInt > this->base.pLgInt + this->length)
            return 2;
        else if (this->tail.pLgInt > this->base.pLgInt + this->length)
            return 3;
        else if ((XDAS_UInt8 )this->precision > 32)
            return 6;
        break;
      default:
        return 5;
    }
    return 0;
}

Int
PAF_BUF_getCount(PAF_BufferPointer *this)
{
    LgInt count;

    switch (this->size) {
      case 1:
        if ((count = this->head.pSmInt - this->tail.pSmInt) < 0)
            count += this->length;
        break;
      case 2:
        if ((count = this->head.pMdInt - this->tail.pMdInt) < 0)
            count += this->length;
        break;
      case 4:
        if ((count = this->head.pLgInt - this->tail.pLgInt) < 0)
            count += this->length;
        break;
      default:
        count = 0;
    }
    return count;
}

LgInt
PAF_BUF_getWord(PAF_BufferPointer *this)
{
    LgInt value;

    if (this->head.pVoid == this->tail.pVoid)
        /* internal error! */
        return 0;

    switch (this->size) {
      case 1:
        value = (*this->tail.pSmInt++ << 24) & (~0 << 32-this->precision);
        if (this->tail.pSmInt == this->base.pSmInt + this->length)
            this->tail.pSmInt = this->base.pSmInt;
        break;
      case 2:
        value = (*this->tail.pMdInt++ << 16) & (~0 << 32-this->precision);
        if (this->tail.pMdInt == this->base.pMdInt + this->length)
            this->tail.pMdInt = this->base.pMdInt;
        break;
      case 4:
        value = *this->tail.pLgInt++ & (~0 << 32-this->precision);
        if (this->tail.pLgInt == this->base.pLgInt + this->length)
            this->tail.pLgInt = this->base.pLgInt;
        break;
      default:
        /* internal error! */
        return 0;
    }
    return value;
}

LgInt
PAF_BUF_gotWord(PAF_BufferPointer *this)
{
    LgInt value;

    switch (this->size) {
      case 1:
        value = (*this->tail.pSmInt << 24) & (~0 << 32-this->precision);
        break;
      case 2:
        value = (*this->tail.pMdInt << 16) & (~0 << 32-this->precision);
        break;
      case 4:
        value = *this->tail.pLgInt & (~0 << 32-this->precision);
        break;
      default:
        /* internal error! */
        return 0;
    }
    return value;
}

Void
PAF_BUF_putBits(PAF_BufferPointer *this, LgInt value, Int nBits)
{
    /* TBD */
}

Void
PAF_BUF_putBitsSigned(PAF_BufferPointer *this, LgInt value, Int nBits)
{
    PAF_BUF_putBits(this, value << 32-nBits, nBits);
}

Void
PAF_BUF_putBitsUnsigned(PAF_BufferPointer *this, LgInt value, Int nBits)
{
    PAF_BUF_putBitsSigned(this, value, nBits);
}

Void
PAF_BUF_putWord(PAF_BufferPointer *this, LgInt value)
{
    Int shift;
    LgInt upper;

    PAF_UnionPointer save = this->head;

    if ((shift = 31-this->precision) >= 0) {
        upper = value + (1 << shift);
        if (value > 0 && upper < 0)
            upper = 0x7fffffff;
        value = upper & (~0 << shift+1);
    }

    switch (this->size) {
      case 1:
        *this->head.pSmInt++ = value >> 24;
        if (this->head.pSmInt == this->base.pSmInt + this->length)
            this->head.pSmInt = this->base.pSmInt;
        break;
      case 2:
        *this->head.pMdInt++ = value >> 16;
        if (this->head.pMdInt == this->base.pMdInt + this->length)
            this->head.pMdInt = this->base.pMdInt;
        break;
      case 4:
        *this->head.pLgInt++ = value;
        if (this->head.pLgInt == this->base.pLgInt + this->length)
            this->head.pLgInt = this->base.pLgInt;
        break;
      default:
        /* internal error! */
        return;
    }

    if (this->head.pVoid == this->tail.pVoid) {
        /* internal error! */
        this->head = save;
        return;
    }
}

PAF_BufferPointerFunctions PAF_BUF_FXNS = {
    PAF_BUF_getBits, 
    PAF_BUF_getBitsSigned, 
    PAF_BUF_getBitsUnsigned, 
    PAF_BUF_getCheck, 
    PAF_BUF_getCount, 
    PAF_BUF_getWord, 
    PAF_BUF_gotWord, 
    PAF_BUF_putBits, 
    PAF_BUF_putBitsSigned, 
    PAF_BUF_putBitsUnsigned, 
    PAF_BUF_putWord, 
};

