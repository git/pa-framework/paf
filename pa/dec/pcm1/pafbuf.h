
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Buffer type and function declarations
//
//      THIS FILE IS NOT USED. IT IS PRESENT ONLY FOR HISTORICAL REASONS.
//
//
//

#ifndef PAFBUF_
#define PAFBUF_

typedef union PAF_UnionPointer {
    Void       *pVoid;
    XDAS_Int8  *pSmInt;
    XDAS_Int16 *pMdInt;
    XDAS_Int32 *pLgInt;
} PAF_UnionPointer;

struct PAF_BufferPointerFunctions;

typedef struct PAF_BufferPointer {
    struct PAF_BufferPointerFunctions *fxns;
    PAF_UnionPointer base;
    PAF_UnionPointer head;
    PAF_UnionPointer tail;
    XDAS_Int16 length;
    XDAS_Int8 size;
    XDAS_Int8 precision;
    XDAS_Int16 headBits;
    XDAS_Int16 tailBits;
} PAF_BufferPointer;

typedef struct PAF_BufferPointerFunctions {
    LgInt (*getBits)(PAF_BufferPointer *pBuffer, Int);
    LgInt (*getBitsSigned)(PAF_BufferPointer *pBuffer, Int);
    LgInt (*getBitsUnsigned)(PAF_BufferPointer *pBuffer, Int);
    Int (*getCheck)(PAF_BufferPointer *pBuffer);
    Int (*getCount)(PAF_BufferPointer *pBuffer);
    LgInt (*getWord)(PAF_BufferPointer *pBuffer);
    LgInt (*gotWord)(PAF_BufferPointer *pBuffer);
    Void (*putBits)(PAF_BufferPointer *pBuffer, LgInt value, Int);
    Void (*putBitsSigned)(PAF_BufferPointer *pBuffer, LgInt value, Int);
    Void (*putBitsUnsigned)(PAF_BufferPointer *pBuffer, LgInt value, Int);
    Void (*putWord)(PAF_BufferPointer *pBuffer, LgInt value);
} PAF_BufferPointerFunctions;

PAF_BufferPointer *
PAF_BufferPointerConstructor(PAF_BufferPointer *, PAF_BufferPointerFunctions *, Void *, Int, Int, Int);

extern PAF_BufferPointerFunctions PAF_BUF_FXNS;

typedef struct PAF_BufferSegment {
    PAF_BufferPointer *pBufferPointer;
    XDAS_Int16 wordCount;
} PAF_BufferSegment;

#ifndef lengthof
#define lengthof(X) (sizeof(X)/sizeof(*(X)))
#endif

#endif  /* PAFBUF_ */
