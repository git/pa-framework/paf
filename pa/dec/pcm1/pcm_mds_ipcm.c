
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) PCM algorithm functionality implementation


/*
 *  PCM Module implementation - MDS implementation of a PCM algorithm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>
#include <math.h>

#include <ipcm.h>
#include <pcm_mds.h>
#include <pcm_mds_priv.h>
#include <pcmerr.h>

#include <inpbuf.h>
#include <paftyp.h>

#include <cpl.h>
#include "pcm_mds_ipcm.h"

#ifndef _TMS320C6X
float ldexpf(float a, int b);
#define restrict
#else
#include <mathf.h>
#endif /* _TMS320C6X */

#if PAF_AUDIODATATYPE_FIXED
#warn fixed-point audio data type not supported
#endif /* PAF_AUDIODATATYPE_FIXED */

// Local symbol definitions

#if PAF_AUDIODATATYPE_FIXED
#define ZERO 0
#else
#define ZERO 0.
#endif

#include <logp.h>

#ifdef TRACE_ENABLE
  #define TRACE(a)  LOG_printf a
  #define LINE_END "\n"
#else
  #define LINE_END
  #define TRACE(a)
#endif

// A bit of a hack.  In bypass mode, deformatter can set this.
PAF_ChannelConfiguration gBypassConfig;



/*
 *  ======== PCM_MDS_decode ========
 *  MDS's implementation of the decode operation.
 */

Int
PCM_MDS_decode(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    PAF_ChannelMask programMask = pcm->pActive->programMask;
    PAF_ChannelMask programWarp = pcm->pActive->programWarp;
    PAF_ChannelMask decodeMask = programMask;

    Int sampleCount = pDecodeInStruct->sampleCount;

    Int i;

    Int errno;

    //
    // Check parameters.
    //

    TRACE((&trace, "PCM_MDS_decode: start." LINE_END));

    /* Check sample count against Decode Algorithm */
    if (sampleCount < pcm->pConfig->minimumFrameLength)
        return PCMERR_DECODE_PARAM;
    else if (sampleCount > pcm->pConfig->maximumFrameLength)
        return PCMERR_DECODE_PARAM;

    /* Check warp according to sample count */
    if (programMask && ! programWarp)
        return PCMERR_DECODE_INPUT;
    else if (sampleCount <= pAudioFrame->data.nSamples)
        /* okay */;
    else if (sampleCount <= 2 * pAudioFrame->data.nSamples) {
        if ((programWarp & (programWarp >> 1)) != 0
            || (programWarp & (1 << pAudioFrame->data.nChannels-1)) != 0)
            return PCMERR_DECODE_INPUT;
    }
    else if (sampleCount <= 4 * pAudioFrame->data.nSamples) {
        if ((programWarp & (programWarp >> 1)) != 0
            || (programWarp & (programWarp >> 2)) != 0
            || (programWarp & (programWarp >> 3)) != 0
            || (programWarp & (7 << pAudioFrame->data.nChannels-3)) != 0)
            return PCMERR_DECODE_INPUT;
    }
    else
        return PCMERR_DECODE_PARAM;

#if 0 /* Not used, for now. --Kurt */
    /* Check sample count against Input Buffer */
    if (pBufferSegment->wordCount < 2 * sampleCount)
        return PCMERR_DECODE_INPUT;
#endif

    // Implement decoding phases (nominal):
    // 1. Input
    // 2. Null
    // 3. Ramp Art
    // 4. Downmix

    for (i=0; i < lengthof (handle->fxns->phase); i++) {
    	TRACE((&trace, "PCM_MDS_decode: phase %d." LINE_END, i));
        if (handle->fxns->phase[i]
            && (errno = handle->fxns->phase[i](handle, pDecodeInStruct, pDecodeOutStruct, decodeMask)))
            return errno;
    }

    //
    // Set output flags and exit.
    //

    pDecodeOutStruct->outputFlag = 1;
    pDecodeOutStruct->errorFlag = 0;
    pDecodeOutStruct->pAudioFrame = pAudioFrame;
    CPL_setAudioFrame((IALG_Handle)handle, pAudioFrame, sampleCount,pcm->pActive->pDecodeStatus);

    TRACE((&trace, "PCM_MDS_decode: end." LINE_END));
    return (0);
}

/*
 *  ======== PCM_MDS_info ========
 *  MDS's implementation of the info operation.
 */

Int
PCM_MDS_info(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeControl *pDecodeControl, PAF_DecodeStatus *pDecodeStatus)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeControl->pAudioFrame;

    Int frameLength = pDecodeControl->frameLength;

    PAF_ChannelConfiguration request
        = pDecodeStatus->channelConfigurationRequest;
    PAF_ChannelConfiguration program
        = pDecodeStatus->channelConfigurationProgram;

    PAF_ChannelConfiguration decode;

    CPL_CDM *PCM_CDMConfig = &(pcm->pScrach->PCM_CDMConfig);

    if (gBypassConfig.full != 0LL)
    {
        TRACE((&trace, "PCM_MDS_info: update bypass config." LINE_END));
    	program = gBypassConfig;
    }

    decode = program;
    if (pcm->pStatus->channelConfigurationProgram.part.aux == PAF_CC_AUX_STEREO_DUAL)
        decode.part.aux = PAF_CC_AUX_STEREO_DUAL;
     // INCONSISTENT USE/REPORT, NOT LIKE AC3, FOR NOW --Jayant, see below the program format

    PCM_CDMConfig->LfeDmxInclude = 0;
    if(pcm->pStatus->LFEDownmixInclude) {
        if (decode.part.sub == PAF_CC_SUB_ONE && request.part.sub == PAF_CC_SUB_ZERO)
            PCM_CDMConfig->LfeDmxInclude = 1;
    }
    PCM_CDMConfig->channelConfigurationFrom = decode;
    PCM_CDMConfig->channelConfigurationRequest = request;
    PCM_CDMConfig->sourceDual = pDecodeStatus->sourceDual;

    CPL_CALL(cdm_downmixConfig)(NULL,PCM_CDMConfig);

    pDecodeStatus->channelConfigurationDecode = decode;
    // Do not perform downmix if decBypass is set
    if (pDecodeStatus->decBypass)
    {
    	TRACE((&trace, "PCM_MDS_info.%d: Bypass." LINE_END, __LINE__));
        pDecodeStatus->channelConfigurationDownmix = decode;
    }
    else
        pDecodeStatus->channelConfigurationDownmix = PCM_CDMConfig->channelConfigurationTo;

    pDecodeStatus->frameLength = frameLength;

    // Update sample rate and pre-emphasis status as per control.
    pDecodeStatus->sampleRate = pDecodeControl->sampleRate;
    pDecodeStatus->emphasis = pDecodeControl->emphasis;

    TRACE((&trace, "PCM_MDS_info: compute format." LINE_END));
    pDecodeStatus->programFormat =
        pAudioFrame->fxns->programFormat (pAudioFrame,
            program,
            pDecodeStatus->sourceDual);
            // INCONSISTENT USE/REPORT, NOT LIKE AC3, FOR NOW --Kurt
    TRACE((&trace, "PCM_MDS_info: programFormat.mask: 0x%x.  programFormat.form: 0x%x." LINE_END,
    		pDecodeStatus->programFormat.mask, pDecodeStatus->programFormat.form));

    pcm->pActive->pDecodeStatus = pDecodeStatus;
    pcm->pActive->pInpBufConfig = pDecodeControl->pInpBufConfig;

    CPL_setAudioFrame((IALG_Handle)handle, pAudioFrame, frameLength,pcm->pActive->pDecodeStatus);

    return ((Int)0);
}

/*
 *  ======== PCM_MDS_warpMask ========
 *  
 */

static inline PAF_ChannelMask
PCM_MDS_warpMask (PAF_AudioFrame *pAudioFrame, PAF_ChannelMask x)
{
    PAF_AudioData **sample = pAudioFrame->data.sample;

    PAF_AudioData *base = NULL;

    Int i;
    Int n, a, b;

    PAF_ChannelMask y = 0;

    /* Warp is ordered, contiguous allocation of audio frame channel buffers */
    /* Warp mask is a bit-mask version of same, 0 for no warp or a bad warp  */

    n = pAudioFrame->data.nSamples;
    for (i=0; i < pAudioFrame->data.nChannels; i++) {
        if ((1 << i) & x) {
            if (! sample[i])
                return 0; /* (x && ! y) indicates bad warp */
            if (! base)
                base = sample[i];
            a = sample[i] - base;
            b = a / n;
            if (a == b * n && 0 <= b && b < 8 * sizeof (PAF_ChannelMask)) {
                y |= (1 << b);
            }
        }
    }

    return y;
}

/*
 *  ======== PCM_MDS_reset ========
 *  MDS's implementation of the reset operation.
 */
Int
PCM_MDS_reset(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeControl *pDecodeControl, PAF_DecodeStatus *pDecodeStatus)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame =
        pDecodeControl->pAudioFrame;

    PAF_ChannelConfiguration program =
        pcm->pStatus->channelConfigurationProgram;

    PAF_ChannelMask programMask =
        pAudioFrame->fxns->channelMask (pAudioFrame, program);

    PAF_ChannelMask programWarp =
        PCM_MDS_warpMask (pAudioFrame, programMask);

    pDecodeStatus->channelConfigurationProgram = program;

    pcm->pActive->rampState = 0;

    pcm->pActive->programMask = programMask;
    pcm->pActive->programWarp = programWarp;

    return 0;
}

/*
 *  ======== PCM_MDS_input ========
 *  MDS's implementation of the input operation.
 */

Int
PCM_MDS_input(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    Int sampleCount = pDecodeInStruct->sampleCount;
    Int nChannels = pAudioFrame->data.nChannels;

    PAF_AudioSize * restrict samsiz = pAudioFrame->data.samsiz;

    Int scaleVolume = pcm->pStatus->scaleVolume;
    PAF_AudioData scale =
        scaleVolume ? pAudioFrame->fxns->dB2ToLinear (scaleVolume) : 1.0;

    Int i;

    Int errno;

    //
    // Load audio data from Input Buffer according to channel map.
    //

    if (errno = handle->fxns->inputCheck(handle, sampleCount))
        return errno;

    for (i=0; i < nChannels; i++) {
        Int from = pcm->pActive->pDecodeStatus->channelMap.from[i];
        Int to = pcm->pActive->pDecodeStatus->channelMap.to[i];
        if (to >= 0 && (decodeMask & (1 << to)) != 0)
        {
            if (from < -1)
            {
            	TRACE((&trace, "PCM_MDS_input: ch %d.  from is %d." LINE_END,  i, from));
            }
            else if (from == -1)
            {
            	TRACE((&trace, "PCM_MDS_input: ch %d.  from is -1." LINE_END, i));;
                handle->fxns->inputAudio(handle, pAudioFrame->data.sample[to], ZERO, from, sampleCount);
            }
            else
            {
            	TRACE((&trace, "PCM_MDS_input: ch %d.  to: %d. from: %d." LINE_END, i, to, from));;
                handle->fxns->inputAudio(handle, pAudioFrame->data.sample[to], scale, from, sampleCount);
            }
        }
        else
        {
        	TRACE((&trace, "PCM_MDS_input: ch %d.  to: %d. from %d.  decodeMask 0x%x 'unmasks' this channel." LINE_END, i, to, from, decodeMask));;
        }
        samsiz[i] = 0;
    }

    return 0;
}

/*
 *  ======== PCM_MDS_inputCheck ========
 *  MDS's implementation of the inputCheck operation.
 */

Int
PCM_MDS_inputCheck(IPCM_Handle handle, int count)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    const PAF_InpBufConfig *pConfig = pcm->pActive->pInpBufConfig;

    Int size = pConfig->sizeofElement;
    Int lengthofData = count * pConfig->stride;
    Int sizeofData = lengthofData * size;


    // deliver zero data during extended zero run
    if (pConfig->deliverZeros)
        return 0;

    // Check for input (format) errors.
    if (pConfig->pntr.pSmInt == NULL)
        return PCMERR_INPUT_POINTERNULL;
    else if (pConfig->pntr.pSmInt < pConfig->base.pSmInt
        || pConfig->pntr.pSmInt >= pConfig->base.pSmInt + pConfig->sizeofBuffer)
        return PCMERR_INPUT_POINTERRANGE;

    switch (size) {
      case 1:
      case 2:
      case 4:
      case 8:
        break;
      default:
        return PCMERR_INPUT_ELEMENTSIZE;
    }

    if (sizeofData > pConfig->sizeofBuffer)
        return PCMERR_INPUT_FRAMESIZE;

    return 0;
}

/*
 *  ======== PCM_MDS_inputAudioFloat ========
 *  MDS's implementation of the inputAudioFloat operation.
 *
 *  Assumes that the buffer is of a length that is a multiple of the stride
 *  and that the offset is less than the stride.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
Int
PCM_MDS_inputAudioFloat(IPCM_Handle handle, float * restrict pWord, float scale, Int offset, Int count)
{
// #ifdef _TMS320C6X
// #warn is this implementation optimal?
// #endif
    PCM_MDS_Obj *pcm = (Void *)handle;

    const PAF_InpBufConfig *pConfig = pcm->pActive->pInpBufConfig;
    PAF_DecodeStatus *pDecodeStatus = pcm->pActive->pDecodeStatus;
    PAF_UnionPointer pntr;

    Int count1 = count;
    Int count2;
    Int size = pConfig->sizeofElement;
    Int prec = pConfig->precision;
    Int stride = pConfig->stride;
    Int sizeofData = count * stride * size;
    Int wrap;
    Int i;
    LgInt mask;


    if (pConfig->deliverZeros || ! pConfig->pntr.pSmInt || scale == ZERO) {
    	TRACE((&trace, "PCM_MDS_inputAudioFloat: deliverZeros.  pWord: 0x%x" LINE_END, pWord));
        while (count1--)
            *pWord++ = 0;
    }
    else {
        pntr.pSmInt = pConfig->pntr.pSmInt + sizeofData;
        if (pntr.pSmInt >= pConfig->base.pSmInt + pConfig->sizeofBuffer)
            pntr.pSmInt -= pConfig->sizeofBuffer;
        pntr.pSmInt -= count1 * stride * size;
        wrap = pntr.pSmInt - pConfig->base.pSmInt;
        if (wrap >= 0) {
            count2 = 0;
        }
        else {
            count1 = - wrap / size / stride;
            count2 = count - count1;
            pntr.pSmInt += pConfig->sizeofBuffer;
        }

        switch (size) {
#if PAF_IROM_BUILD == 0xD610A003
          case 1:
            scale = ldexpf (scale, 1 - 8);
            mask = ~0 << 32-prec >> 24;
            for (i=offset; count1--; i+=stride)
                *pWord++ = (pntr.pSmInt[i] & mask) * scale;
            pntr.pSmInt = pConfig->base.pSmInt;
            for (i=offset; count2--; i+=stride)
                *pWord++ = (pntr.pSmInt[i] & mask) * scale;
            break;
#else
          case 1:
          case 8:
              return PCMERR_INPUT_ELEMENTSIZE;
#endif
          case 2:
            TRACE((&trace, "PCM_MDS_inputAudioFloat.%d:  case 2.  pWord: 0x%x.  pntr: 0x%x" LINE_END,
                        __LINE__, pWord, &pntr.pMdInt[offset]));
            scale = ldexpf (scale, 1 - 16);
            mask = ~0 << 32-prec >> 16;
            CPL_CALL(smaskScale)(mask, scale, &pntr.pMdInt[offset], pWord, stride, count1);
            pntr.pSmInt = pConfig->base.pSmInt;
            CPL_CALL(smaskScale)(mask, scale, &pntr.pMdInt[offset], pWord + count1, stride, count2);
            break;
          case 4:
            // Do not perform fixed to float and scale if decBypass is set
            if (pDecodeStatus->decBypass) 
            {
            	TRACE((&trace, "PCM_MDS_inputAudioFloat: Bypass case 4.  pWord: 0x%x.  pntr: 0x%x, *pntr: 0x%x" LINE_END,
            			pWord, pntr.pFloat, *pntr.pLgInt));
                for (i=offset; count1--; i+=stride)
                *(int *)pWord++ = (pntr.pLgInt[i]);

                // in case of wrap
                pntr.pLgInt = pConfig->base.pLgInt;
                for (i=offset; count2--; i+=stride)
                *(int *)pWord++ = (pntr.pLgInt[i]);
            }
            else if (prec <= 32) {
                TRACE((&trace, "PCM_MDS_inputAudioFloat.%d:  prec %d, pWord: 0x%x.  pntr: 0x%x" LINE_END,
            			__LINE__, prec, pWord, pntr.pLgInt));

                scale = ldexpf (scale, 1 - 32);
                mask = ~0 << 32-prec;
                CPL_CALL(imaskScale)(mask, scale, &pntr.pLgInt[offset], pWord, stride, count1);
                pntr.pSmInt = pConfig->base.pSmInt;
                CPL_CALL(imaskScale)(mask, scale, &pntr.pLgInt[offset], pWord + count1, stride, count2);
            }
            else {
                TRACE((&trace, "PCM_MDS_inputAudioFloat.%d:  prec %d. pWord: 0x%x.  pntr: 0x%x" LINE_END,
            			__LINE__, prec, pWord, pntr.pLgInt));

                CPL_CALL(vecStrideScale)(scale, &pntr.pFloat[offset], pWord, stride, count1);
                pntr.pSmInt = pConfig->base.pSmInt;
                CPL_CALL(vecStrideScale)(scale, &pntr.pFloat[offset], pWord + count1, stride, count2);
            }
            break;
#if PAF_IROM_BUILD == 0xD610A003
          case 8:
            for (i=offset; count1--; i+=stride)
                *pWord++ = pntr.pDouble[i] * scale;
            pntr.pSmInt = pConfig->base.pSmInt;
            for (i=offset; count2--; i+=stride)
                *pWord++ = pntr.pDouble[i] * scale;
            break;
#endif
        }
    }

    return 0;
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCM_MDS_inputAudioDouble ========
 *  MDS's implementation of the inputAudioDouble operation.
 *
 *  Assumes that the buffer is of a length that is a multiple of the stride
 *  and that the offset is less than the stride.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE
Int PCM_MDS_inputAudioDouble(IPCM_Handle handle, double * restrict pWord, double scale, Int offset, Int count)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    const PAF_InpBufConfig *pConfig = pcm->pActive->pInpBufConfig;
    PAF_UnionPointer pntr;

    Int count1 = count;
    Int count2;
    Int size = pConfig->sizeofElement;
    Int prec = pConfig->precision;
    Int stride = pConfig->stride;
    Int sizeofData = count * stride * size;
    Int wrap;
    Int i;
    LgInt mask;


    if (pConfig->deliverZeros || ! pConfig->pntr.pSmInt || scale == ZERO) {
        CPL_CALL(vecSet)(0.0f, pWord, count1);
    }
    else {
        pntr.pSmInt = pConfig->pntr.pSmInt + sizeofData;
        if (pntr.pSmInt >= pConfig->base.pSmInt + pConfig->sizeofBuffer)
            pntr.pSmInt -= pConfig->sizeofBuffer;
        pntr.pSmInt -= count1 * stride * size;
        wrap = pntr.pSmInt - pConfig->base.pSmInt;
        if (wrap >= 0) {
            count2 = 0;
        }
        else {
            count1 = - wrap / size / stride;
            count2 = count - count1;
            pntr.pSmInt += pConfig->sizeofBuffer;
        }

        switch (size) {
          case 1:
            scale = ldexp (scale, 1 - 8);
            mask = ~0 << 32-prec >> 24;
            CPL_CALL(smaskScale)(mask, scale, &pntr.pSmInt[offset], pWord, stride, count1);
            pntr.pSmInt = pConfig->base.pSmInt;
            CPL_CALL(smaskScale)(mask, scale, &pntr.pSmInt[offset], pWord + count1, stride, count2);
            break;
          case 2:
            scale = ldexp (scale, 1 - 16);
            mask = ~0 << 32-prec >> 16;
            CPL_CALL(imaskScale)(mask, scale, &pntr.pMdInt[offset], pWord, stride, count1);
            pntr.pSmInt = pConfig->base.pSmInt;
            CPL_CALL(imaskScale)(mask, scale, &pntr.pMdInt[offset], pWord + count1, stride, count2);
            break;
          case 4:
            if (prec <= 32) {
                scale = ldexp (scale, 1 - 32);
                mask = ~0 << 32-prec;
                for (i=offset; count1--; i+=stride)
                    *pWord++ = (pntr.pLgInt[i] & mask) * scale;
                pntr.pSmInt = pConfig->base.pSmInt;
                for (i=offset; count2--; i+=stride)
                    *pWord++ = (pntr.pLgInt[i] & mask) * scale;
            }
            else {
                for (i=offset; count1--; i+=stride)
                    *pWord++ = pntr.pFloat[i] * scale;
                pntr.pSmInt = pConfig->base.pSmInt;
                for (i=offset; count2--; i+=stride)
                    *pWord++ = pntr.pFloat[i] * scale;
            }
            break;
          case 8:
            for (i=offset; count1--; i+=stride)
                *pWord++ = pntr.pDouble[i] * scale;
            pntr.pSmInt = pConfig->base.pSmInt;
            for (i=offset; count2--; i+=stride)
                *pWord++ = pntr.pDouble[i] * scale;
            break;
        }
    }

    return 0;
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCM_MDS_rampart ========
 *  MDS's implementation of the rampart operation.
 */

Int
PCM_MDS_rampart(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;
    PAF_DecodeStatus *pDecodeStatus = pcm->pActive->pDecodeStatus;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    PAF_ChannelMask programMask = pcm->pActive->programMask;

    Int iChannel, iSample;

    Int nChannels = pAudioFrame->data.nChannels;
    Int sampleCount = pDecodeInStruct->sampleCount;

    PAF_AudioData ** restrict sample = pAudioFrame->data.sample;

    PAF_AudioData scinc = 1. / sampleCount;
    PAF_AudioData scale;

    // Do not perform ramp if decBypass is set
    if (pDecodeStatus->decBypass)
    {
    	TRACE((&trace, "PCM_MDS_rampart.%d: Bypass." LINE_END, __LINE__));
        return 0;
    }

    if (pcm->pStatus->ramp) {
        switch (pcm->pActive->rampState) {
          case 0:
            for (iChannel=0; iChannel < nChannels; iChannel++) {
                if (programMask & (1 << iChannel)) {
                    scale = ZERO;
                    for (iSample=0; iSample < sampleCount; iSample++) {
                        sample[iChannel][iSample] *= scale;
                        scale += scinc;
                    }
                }
            }
            pcm->pActive->rampState = 1;
            break;
          case 1:
            break;
        }
    }

    return 0;
}

/*
 *  ======== PCM_MDS_downmix ========
 *  MDS's implementation of the downmix operation.
 */

Int
PCM_MDS_downmix(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    CPL_CDM *PCM_CDMConfig = &(pcm->pScrach->PCM_CDMConfig);

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    Int sampleCount = pDecodeInStruct->sampleCount;

    PAF_ChannelConfiguration program
        = pcm->pActive->pDecodeStatus->channelConfigurationProgram;
    PAF_ChannelConfiguration request
        = pcm->pActive->pDecodeStatus->channelConfigurationRequest;
    PAF_ChannelConfiguration decode =
        pcm->pActive->pDecodeStatus->channelConfigurationDecode;
    PAF_ChannelConfiguration downmix =
        pcm->pActive->pDecodeStatus->channelConfigurationDownmix;

    if (decode.legacy != downmix.legacy) {
        PCM_CDMConfig->channelConfigurationFrom = decode;
        PCM_CDMConfig->channelConfigurationTo = downmix;
        PCM_CDMConfig->sourceDual = pcm->pActive->pDecodeStatus->sourceDual;
        PCM_CDMConfig->clev = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->CntrMixLev);
        PCM_CDMConfig->slev = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->SurrMixLev);
        PCM_CDMConfig->LFEDmxVolume = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->LFEDownmixVolume);

        CPL_CALL(cdm_downmixSetUp)(NULL,pAudioFrame,PCM_CDMConfig);
        CPL_CALL(cdm_samsiz)(NULL,pAudioFrame,PCM_CDMConfig);
        CPL_CALL(cdm_downmixApply)(NULL,pAudioFrame->data.sample,pAudioFrame->data.sample,PCM_CDMConfig,sampleCount);
    }

    return 0;
}
