
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) PCM algorithm functionality implementation
//
//
//

#ifndef PCM_MDS_IPCM_3_
#define PCM_MDS_IPCM_3_

#include <paftyp_a.h>

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE

#define P10dB 3.162278
#define P7dB  2.238721
#define M3dB  0.707107  /* 1/sqrt(2) */
#define M6dB  0.5       /* 1/2 */
#define M9dB  0.353553  /* 1/2/sqrt(2) */

#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT

#define P10dB 3.162278F
#define P7dB  2.238721F
#define M3dB  0.707107F /* 1/sqrt(2) */
#define M6dB  0.5F      /* 1/2 */
#define M9dB  0.353553F /* 1/2/sqrt(2) */

#else /* PAF_AUDIODATATYPE == ... */

#error fixed point audio data type not supported by this implementation

#endif /* PAF_AUDIODATATYPE == ... */

#endif  /* PCM_MDS_IPCM_3_ */
