
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PCM error numbers
//
//
//

#ifndef PCMERR_
#define PCMERR_

#include <acpbeta.h>

#define PCMERR_DECODE_                  ((STD_BETA_PCM<<8)+0x00)
#define PCMERR_PHASE1_                  ((STD_BETA_PCM<<8)+0x10)
#define PCMERR_PHASE2_                  ((STD_BETA_PCM<<8)+0x20)
#define PCMERR_PHASE3_                  ((STD_BETA_PCM<<8)+0x30)
#define PCMERR_PHASE4_                  ((STD_BETA_PCM<<8)+0x40)

#define PCMERR_DECODE_UNSPECIFIED       (PCMERR_DECODE_+0x00)
#define PCMERR_DECODE_PARAM             (PCMERR_DECODE_+0x01)
#define PCMERR_DECODE_INPUT             (PCMERR_DECODE_+0x02)
#define PCMERR_DECODE_CRC               (PCMERR_DECODE_+0x03)

#define PCMERR_INPUT_UNSPECIFIED        (PCMERR_PHASE1_+0x00)
#define PCMERR_INPUT_POINTERNULL        (PCMERR_PHASE1_+0x01)
#define PCMERR_INPUT_POINTERRANGE       (PCMERR_PHASE1_+0x02)
#define PCMERR_INPUT_ELEMENTSIZE        (PCMERR_PHASE1_+0x03)
#define PCMERR_INPUT_FRAMESIZE          (PCMERR_PHASE1_+0x04)
#define PCMERR_INPUT_RESULTRANGE        (PCMERR_PHASE1_+0x05)
#define PCMERR_INPUT_PRECISION          (PCMERR_PHASE1_+0x06)
#define PCMERR_INPUT_NODATA             (PCMERR_PHASE1_+0x07)

#define PCMERR_RAMPART_UNSPECIFIED      (PCMERR_PHASE3_+0x00)

#define PCMERR_DOWNMIX_UNSPECIFIED      (PCMERR_PHASE4_+0x00)
#define PCMERR_DOWNMIX_PARAM            (PCMERR_PHASE4_+0x01)
#define PCMERR_DOWNMIX_UNKNOWN          (PCMERR_PHASE4_+0x02)

#endif  /* PCMERR_ */
