
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PCE error numbers
//
//
//

#ifndef PCEERR_
#define PCEERR_

#include <acpbeta.h>

#define PCEERR_ENCODE_                  ((STD_BETA_PCE<<8)+0x00)
#define PCEERR_PHASE1_                  ((STD_BETA_PCE<<8)+0x10)
#define PCEERR_PHASE2_                  ((STD_BETA_PCE<<8)+0x20)
#define PCEERR_PHASE3_                  ((STD_BETA_PCE<<8)+0x30)
#define PCEERR_PHASE4_                  ((STD_BETA_PCE<<8)+0x40)

#define PCEERR_ENCODE_UNSPECIFIED       (PCEERR_ENCODE_+0x00)
#define PCEERR_ENCODE_PARAM             (PCEERR_ENCODE_+0x01)
#define PCEERR_ENCODE_OUTPUT            (PCEERR_ENCODE_+0x04)

#define PCEERR_VOLUME_                  (PCEERR_PHASE1_)
#define PCEERR_VOLUME_UNSPECIFIED       (PCEERR_VOLUME_+0x00)

#define PCEERR_OUTPUT_UNSPECIFIED       (PCEERR_PHASE3_+0x00)
#define PCEERR_OUTPUT_POINTERNULL       (PCEERR_PHASE3_+0x01)
#define PCEERR_OUTPUT_POINTERRANGE      (PCEERR_PHASE3_+0x02)
#define PCEERR_OUTPUT_ELEMENTSIZE       (PCEERR_PHASE3_+0x03)
#define PCEERR_OUTPUT_FRAMESIZE         (PCEERR_PHASE3_+0x04)
#define PCEERR_OUTPUT_RESULTRANGE       (PCEERR_PHASE3_+0x05)

#endif  /* PCEERR_ */
