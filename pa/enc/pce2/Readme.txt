
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
//

[1]

alpha writeVOLOffsetMasterN(0x7fff)

alpha readPCEExceptionDetectMode
alpha readPCEExceptionDetectFlag
alpha readPCEExceptionDetectMute

alpha writePCEExceptionDetectEnable
alpha writePCEExceptionDetectFlagOff
alpha writePCEExceptionDetectMute

alpha writePCEExceptionDetectEnable
alpha writePCEExceptionDetectFlagOff
alpha writePCEExceptionDetectUnmute

alpha readPCEExceptionDetectFlag


Note that the register debug windows don't show the correct status.
This may be cache effect; break point shows things correctly.


[]
Int
PCE_TII_outputAudioFloat()
...
...
	float detectAccAL;//volatile,ActiveLow 
	unsigned int iSampleCast;//volatile
        volatile unsigned int maskedISample;//If not volatile, compiler throws off the loop!!
	//	
	//31 30  ..0
	//floatExceptionMask=0x7F800000=0xFF<< 23;
	unsigned int floatExceptionMask=0x7F800000;// 0x7FFFFFFF;//
    volatile unsigned int floatOperatinStatus;    
...
...

//
    unsigned int maskFADCR; //.L1&.L2,.S1,.S2
    unsigned int maskFMCR;  //.M1&.M2
    unsigned int maskFAUCR;
// They also contain fields to warn if src1 and src2
// are NaN or denormalized numbers, and if the result overflows, underflows, is
// inexact, infinite, or invalid. There are also fields to warn if a divide by 0 was
// performed, or if a compare was attempted with a NaN source. Table 2-13 lists
// the additional registers used. The OVER, UNDER, INEX, INVAL, DENn,
// NANn, INFO, UNORD and DIV0 bits within these registers will not be modified
// by a conditional instruction whose condition is false.
//
// For the C67x+ DSP, the ADDSP, ADDDP, SUBSP, and SUBDP instructions
// executing in the .S functional unit use the rounding mode from and set the
// warning bits in FADCR. The warning bits in FADCR are the logical-OR of the
// warnings produced on the .L functional unit and the warnings produced by
// the ADDSP/ADDDP/SUBSP/SUBDP instructions on the .S functional unit
// (but not other instructions executing on the .S functional unit).
//
// The floating-point auxiliary register (FAUCR) contains fields that specify
// underflow or overflow, the rounding mode, NaNs, denormalized numbers, and
// inexact results for instructions that use the .S functional units.
//
	pStatus =(PCE_TII_Status *) pce->pStatus;
    maskFADCR = 0x00330033; //.L1&.L2,.S1,.S2
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 110011   = 0x0033
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 00110011 = 0x0033
    
    maskFMCR = 0x00330033;  //.M1&.M2
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 110011   = 0x0033
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 00110011 = 0x0033
    
    maskFAUCR = 0x04330433;
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved DIV0 UNORD UND INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 0x0433 
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved DIV0 UNORD UND INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 0x0433 
...
...
            // The generated loop should contain floating point instructions
            // Depending on optimisation options , this may change.. better code in serial assembly and hook
            while (count--) {
    
    			fSampleVal = *pWord++;		

    			//1.0f/2147483648.0f  - f is highly required
    			//Max + 2^31-1 = 2147483647
    			//Max - 2^31 = 2147483648
    			//iSampleCast=*((unsigned int *) &fSampleVal);                
                iSampleCast=_ftoi(fSampleVal+0.001f); //Force float operation in .L : SPINT
    
    			//start with all 1s
    			//mulitply 1 & masked pattern
    			//Any time all 0s are formed - detected the pattern in  iSampleCast
    			//
    			maskedISample=iSampleCast+1;
                 //detectAccAL+=fSampleVal;
            
            }//count--

//---

The above loop has become an ii=1 loop.

Ensure no optimsation throws off this loop. Else code in serial assembly and plug in.
 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 777
;*      Loop opening brace source line   : 777
;*      Loop closing brace source line   : 794
;*      Known Minimum Trip Count         : 8                    
;*      Expected Minimum Trip Count      : 256
;*      Expected Maximum Trip Count      : 256
;*      Known Max Trip Count Factor      : 8
;*      Loop Carried Dependency Bound(^) : 1
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1*       1*    
;*      .M units                     0        0     
;*      .X cross paths               1*       0     
;*      .T address paths             1*       1*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           1        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 11 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L27:    ; PIPED LOOP PROLOG

           SPLOOPD 1       ;11               ; (P) 
||         MV      .L1X    B1,A5
||         MVC     .S2     B4,ILC

;** --------------------------------------------------------------------------*
$C$L28:    ; PIPED LOOP KERNEL
$C$DW$L$_PCE_TII_outputAudioFloat$4$B:
           LDW     .D1T2   *A5++,B4          ; |791| (P) <0,0>  ^ 
           NOP             2

           SPMASK          S1
||         MVKL    .S1     0x3a83126f,A6

           SPMASK          S1
||         MVKH    .S1     0x3a83126f,A6

           ADDSP   .L1X    A6,B4,A4          ; |791| (P) <0,5>  ^ 
           NOP             3
           ADD     .S1     1,A4,A3           ; |791| (P) <0,9>  ^ 

           SPKERNEL 10,0
||         STW     .D2T1   A3,*+SP(4)        ; |791| <0,10>  ^ 

$C$DW$L$_PCE_TII_outputAudioFloat$4$E:
;** --------------------------------------------------------------------------*
$C$L29:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 846,column 9,is_stmt
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 838,column 3,is_stmt
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 837,column 6,is_stmt
           ZERO    .L2     B4                ; |846| 
           STW     .D2T2   B4,*+SP(8)        ; |846| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 849,column 9,is_stmt
           MVC     .S2     FADCR,B4          ; |849| 
           LDW     .D2T2   *+SP(8),B5        ; |849| 
           MVKL    .S1     0x330033,A3
           MVKH    .S1     0x330033,A3
           NOP             1
           AND     .L2X    A3,B4,B4          ; |849| 
           OR      .L2     B5,B4,B4          ; |849| 
           STW     .D2T2   B4,*+SP(8)        ; |849| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 850,column 9,is_stmt
           MVC     .S2     FMCR,B4           ; |850| 
           LDW     .D2T2   *+SP(8),B5        ; |850| 
           AND     .L2X    A3,B4,B4          ; |850| 
           NOP             3
           OR      .L2     B5,B4,B4          ; |850| 
           STW     .D2T2   B4,*+SP(8)        ; |850| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 851,column 9,is_stmt
           MVC     .S2     FAUCR,B4          ; |851| 
           LDW     .D2T2   *+SP(8),B5        ; |851| 
           MVKL    .S2     0x4330433,B9
           MVKH    .S2     0x4330433,B9
           AND     .L2     B9,B4,B4          ; |851| 
           NOP             1
           OR      .L2     B5,B4,B4          ; |851| 
           STW     .D2T2   B4,*+SP(8)        ; |851| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 852,column 9,is_stmt
           LDW     .D2T2   *+SP(8),B2        ; |852| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 853,column 13,is_stmt
           MVK     .S1     57,A4             ; |853| 
           MVK     .L1     1,A3              ; |853| 
           NOP             2
   [ B2]   STB     .D1T1   A3,*+A17[A4]      ; |853| 
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 859,column 3,is_stmt

           MV      .L1     A4,A3             ; |859| 
||         MVK     .S1     58,A4             ; |859| 

           LDB     .D1T1   *+A17[A4],A4      ; |859| 
           NOP             4
           CMPEQ   .L1     A4,1,A0           ; |859| 

   [!A0]   BNOP    .S1     $C$L33,4          ; |859| 
|| [ A0]   LDB     .D1T1   *+A17[A3],A3      ; |859| 

           CMPEQ   .L1     A3,1,A0           ; |859| 
           ; BRANCHCC OCCURS {$C$L33}        ; |859| 
;** --------------------------------------------------------------------------*
   [!A0]   BNOP    .S1     $C$L33,5          ; |859| 
           ; BRANCHCC OCCURS {$C$L33}        ; |859| 
;** --------------------------------------------------------------------------*
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 864,column 4,is_stmt
	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 865,column 4,is_stmt

           ZERO    .L2     B4
||         MV      .S2     B1,B16            ; |865| 
||         SUB     .D2     B1,8,B1

	.dwpsn	file "t:\pa\enc\pce2\pce_tii_output.c",line 872,column 10,is_stmt
           SHR     .S2     B0,1,B5           ; |872| 

           SUB     .L2     B5,4,B9
||         MV      .L1X    B4,A3

           MVC     .S2     B9,ILC



[]

Testing of the saturation.

Detect potential saturation using CSR:SAT bit

alpha "writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(0),readPCEClipDetectFlag"
alpha writeDECCommandRestart
alpha readVOLInternalStatusMaster
alpha readPCEClipDetectFlag

alpha writePCEClipDetectFlagOff
alpha readPCEClipDetectFlag


Flag Off

alpha "writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(12),readPCEClipDetectFlag"
alpha writeDECCommandRestart
alpha readVOLExternalStatusMaster
alpha readVOLInternalStatusMaster
alpha readPCEClipDetectFlag
alpha writePCEClipDetectFlagOff
alpha readPCEClipDetectFlag

Flag Off ( 6dB / bit ??? ; Don't know how it slips!!!)

alpha "writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(13),readPCEClipDetectFlag"
alpha writeDECCommandRestart
alpha readVOLInternalStatusMaster
alpha readPCEClipDetectFlag
alpha writePCEClipDetectFlagOff
alpha readPCEClipDetectFlag

Flag On ( Good)
---



[]

Before cleaning the code for anomaly detection.

	if((0x01==pStatus->pceExceptionDetect)) 
       // && (0x00==pStatus->pceExceptionFlag) )
	{
		//
		//
		tmp=count;
		pTmp = pWord;
	
		//pStatus->pceExceptionFlag=0;
	
		//Floating Point Exception  SPRU733A                                      
		//val=-1s*2^e-127* 1.f, 0<e<255                                           
		//Cases where val=+Inf,-Inf,NaN,QnaN,SnaN & e=255 is flagged as exception.
		//
		//Two approaches:
		//1. Use mask 0x7F800000 (ie b30-b23|b22...b0|  255<<23) to detect.
        //
		//2. register FADCR (Flags .L Unit exception)
		//            FMCR  (Flags .M Unit exception)
		//            FAUCR (Flags .S Unit exception)
		//     
		// Use Task disable for correct functonality, that other tasks will not set these bits
		//
		detectAccAL=1.0;

		// Functionality of floating point exception detected.
		// More optimisation expected..

    
    	//if((0x01==pStatus->pceExceptionDetect)) 
        // && (0x00==pStatus->pceExceptionFlag) )
    	{
        //TSK_disable();
        //Set to zero
        FADCR &= maskFADCR^0xFFFFFFFF ;
        FMCR  &= maskFMCR ^0xFFFFFFFF ;
        FAUCR &= maskFAUCR^0xFFFFFFFF ; 
        }
        detectAccAL=0;
		#pragma MUST_ITERATE(8,,8)
		#pragma PROB_ITERATE(256,256)
            // The generated loop should contain floating point instructions
            // Depending on optimisation options , this may change.. better code in serial assembly and hook
            while (count--) {
    
    			fSampleVal = *pWord++;		

    			//1.0f/2147483648.0f  - f is highly required
    			//Max + 2^31-1 = 2147483647
    			//Max - 2^31 = 2147483648
    			//*(unsigned int *) &fSampleVal=(*((unsigned int *) &fSampleVal)|0x007FFFFE); //force most of fractional part full                
                iSampleCast=_ftoi(fSampleVal+0.00000023f);//0.00000023 = 2^-22
                 //Force float operation in .L : SPINT
    
    			//start with all 1s
    			//mulitply 1 & masked pattern
    			//Any time all 0s are formed - detected the pattern in  iSampleCast
    			//
    			maskedISample=iSampleCast+1;
                //detectAccAL+=fSampleVal;
            
            }//count--



        //reverting the mdoified values
	    count = tmp;
		pWord = pTmp ;
		//
		//	

	
    	//if((0x01==pStatus->pceExceptionDetect)) 
        // && (0x00==pStatus->pceExceptionFlag) )
    	{
        floatOperationStatus=0;
        
        //Set to zero
        floatOperationStatus|=FADCR & maskFADCR; //.L1 &.L2
        floatOperationStatus|=FMCR  & maskFMCR;  //.M1 &.M2  
        floatOperationStatus|=FAUCR& maskFAUCR;  //.S1 &.S2
        if(floatOperationStatus !=0x00000000){
            pStatus->pceExceptionFlag=0x01;
        }
        //TSK_enable(); 
        }

	
		if((pStatus->pceExceptionMute==0x01) && (pStatus->pceExceptionFlag==0x01))
		{
			//1)Handle the current buffer playback.
			//
			//
			tmp=count;
			pTmp = pWord;
			#pragma MUST_ITERATE(8,,8)
			#pragma PROB_ITERATE(256,256)	
	        while (count--) {
	
				 *pWord++=0;		
			        
	        }//count--
			//reverting the mdoified values
		    count = tmp;
			pWord = pTmp ;
			//
			//
			//Other approaches
			//Multiply samples by 0
			//may make a multipler 0
            
			
			//2)Let all future buufers be muted, until exlpictly unmutes.
            //  Future calls will be hooking zero samples and will never enter here again
			//  
        	//pEncodeStatus->command2=0x01; //CRASHES?????? Need more debug...
            //Little ambiguity! User written value(writeENCCommandUnmute)will now be automatically reverted!
            //writeENCCommandUnmute will get honoured only if writePCEExceptionDetectUnmute is explicitly sent.
            // This is to prevent anomalies craeting noise which may damage the speakers. 
		}


	}//encExceptionDetect!=0	


[]

alpha writePCEDELModeDisable
alpha writeSYSRecreationModeDont
alpha writeDECASPGearControlNil
alpha "writeVOLRampTimeN(0)"

---

>alpha "writePCEClipDetectFlagOff,writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(0)" &&  alpha "writeDECCommandRestart" &&  alpha "readPCEClipDetectFlag"
Saturation Not detected : writeVOLControlMasterN(0)

>alpha "writePCEClipDetectFlagOff,writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(0)" &&  alpha "writeDECCommandRestart" &&  alpha "readPCEClipDetectFlag"
Saturation Not detected : writeVOLControlMasterN(3)

>alpha "writePCEClipDetectFlagOff,writeVOLOffsetMasterN(0x7FFF),writeVOLControlMasterN(4)" &&  alpha "writeDECCommandRestart" &&  alpha "readPCEClipDetectFlag"
Saturation Detected : writeVOLControlMasterN(4)

Note:-
This could indicate that the analog headphone-o/p of the Laptop at full volume (0dB) 
is lesser than the DAC voltage(line in?) reference by -2dB ie 0.7943.

(The line-o/p needs to be 1.25 times the headphone o/p voltage.)

---

pWord = 1.#INF = 0x7F800000
