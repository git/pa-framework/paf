
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder SLD functionality implementation
//
// SLD encode phase
//
//
//

#include <std.h>
#include <paftyp.h>
#include <stdasp.h>
#include <csl_dat.h>	// DAT_copy
#include <pafenc.h>
#include <pce_tii.h>
#include <pce_tii_priv.h>

#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
#include <dmax_dat.h>
#endif /* PAF_DEVICE */

#define SOS .34713 /* speed of sound in m/ms */

#ifndef _TMS320C6X
#define restrict
#include "c67x_cintrins.h"
#endif /* _TMS320C6X */

// -----------------------------------------------------------------------------
#include <logp.h>

  // #define ENABLE_TRACE

#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
 #define LINE_END "\n"
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------


/*
 *  ======== PCE_TII_delayPhase ========
 *  TII's implementation of the delay operation.
 */

Int 
PCE_TII_delayPhase(
    IPCE_Handle handle, 
    PAF_EncodeInStruct *pEncodeInStruct, 
    PAF_EncodeOutStruct *pEncodeOutStruct, 
    IPCE_StatusPhase *pStatusPhase,
    IPCE_ConfigPhase *pConfigPhase, 
    PAF_IALG_Common *pConfigCommon, 
    PAF_ActivePhase *pActivePhase, 
    PAF_ScrachPhase *pScrachPhase,
    PAF_ChannelMask_HD streamMask)
{
    PCE_TII_Obj *pce = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pEncodeInStruct->pAudioFrame;

    IPCE_ConfigPhaseDelay *pConfigDelay = (IPCE_ConfigPhaseDelay *)pConfigPhase; 

    PAF_DelayState * restrict state = pConfigDelay->state;

    PAF_ScrachPhaseDelay *pScrachDelay = (PAF_ScrachPhaseDelay *)pScrachPhase;

    Int unit = pce->pStatus->del.unit; 

    Int errno = 0;
    Int errch;

    Int iC, iS;

    if (pEncodeOutStruct->bypassFlag)
    {
    	TRACE((&trace, "%s.%d:  Bypass." LINE_END, __FUNCTION__, __LINE__));
    	return 0;
    }

    TRACE((&trace, "running PCE_TII_delayPhase func"));

    /* Compute delay using time or location based method. */
    if(!(unit & 0x80)) {
        /* Time-based method. */
        volatile XDAS_UInt16 * restrict delay = pce->pStatus->del.delay;

        for(iC=0, iS=0;
            iC < pce->pStatus->del.numc && iS < pce->pStatus->del.nums;
            iC++) {
            if(pConfigDelay->mask.bits & (1 << iC))
            {
            	state[iS++].delay = delay[iC] + pce->pStatus->del.masterDelay;
            }
        }
    }
    else {
        /* Location-based delay */
        volatile XDAS_UInt16 * restrict delay = pce->pStatus->del.delay;
        Uns locmax;

        locmax = delay[0];
        for(iC=1; iC < pce->pStatus->del.numc; iC++)
            if(locmax < delay[iC])
                locmax = delay[iC];

        for(iC=0, iS=0;
            iC < pce->pStatus->del.numc && iS < pce->pStatus->del.nums;
            iC++) {
            if(pConfigDelay->mask.bits & (1 << iC))
                state[iS++].delay = locmax - delay[iC];
        }
    }

    unit &= 0x7f;

    /* Perform delay for channels in the configuration; 
       reset delay for others. */

    for(iC=0, iS=0;
        iC < pce->pStatus->del.numc && iS < pce->pStatus->del.nums;
        iC++)
    {
    	TRACE((&trace, "PCE_TII_delay:  iC: %d" LINE_END, iC));
        if(pConfigDelay->mask.bits & (1 << iC))
        {
            if(!pScrachDelay)
                errch = handle->fxns->delay(pAudioFrame, &state[iS++],
                            streamMask & (1 << iC), iC, unit,
                            pce->pConfig->scale[iC],
                            pConfigDelay->sizeofElement);
            else
                errch = handle->fxns->delayDAT(pAudioFrame, &state[iS++],
#ifndef PCE_ALIGN_128
                            (SmInt *)((Int)pScrachDelay+sizeof(PAF_ScrachPhaseDelay)), 
#else
                            (SmInt *)(((Int)pScrachDelay+sizeof(PAF_ScrachPhaseDelay)+0x7F)&0xFFFFFF80), 
#endif
                            streamMask & (1 << iC), iC, unit,
                            pce->pConfig->scale[iC],
                            pConfigDelay->sizeofElement);
            if(errch && !errno)
                errno = errch + iC;
            if(streamMask & (1 << iC))
            {
	            pce->pActive->bitstreamMask.bits |= (1 << iC); 
	            TRACE((&trace, "PCE_TII_delay:  Set streamMask 0x%x, bit %d" LINE_END, pce->pActive->bitstreamMask.bits, iC));
            }
            else
            {
            	TRACE((&trace, "PCE_TII_delay:  Not setting streamMask, bit %d" LINE_END, iC));
            }
        }
    }

    return errno;
}

Int
PCE_TII_delay(
    PAF_AudioFrame * restrict pAudioFrame,
    PAF_DelayState * restrict state,
    Int apply,
    Int channel,
    Int unit,
    PAF_AudioData scale,
    Int sizeofElement)
{
    static const Float sampleRateScale[] = {
        0.0,                             // unused
        0.0010,                          // milliseconds (Q0)
        0.0005,                          // milliseconds (Q1)
        0.0010 / (SOS * 100.0),          // milliseconds per centimeter
        0.0010 / (SOS * 3.28084),        // milliseconds per foot
        0.0010 / (SOS * 1.093613),       // milliseconds per yard
        0.0010 / (SOS * 1.0),            // milliseconds per meter
        0.0001,                          // milliseconds (tenths)
    };

    PAF_AudioData * restrict data = pAudioFrame->data.sample[channel];
    SmInt * restrict outptr = (SmInt *)pAudioFrame->data.sample[channel];

    if(!apply || !data) {
        // Reset delay
        state->count = 0;
        state->pntr = state->base;
        TRACE((&trace, "PCE_TII_delay:  Not applying (0x%x, 0x%x)" LINE_END, apply, data));
    }
    else {
        // Apply delay
        Uns sampleCount = pAudioFrame->sampleCount;
        Uns startCount;
        Uns delay;
        Uns i;
        XDAS_Int32 value; 

        // Compute delay according to units and saturate to buffer length
        if(unit >= lengthof(sampleRateScale))
            unit = 0;

        if(!unit)
            delay = state->delay;
        else
            delay = sampleRateScale[unit] *
                    pAudioFrame->fxns->sampleRateHz(pAudioFrame,  
                        pAudioFrame->sampleRate, 
                        PAF_SAMPLERATEHZ_STD) *
                    state->delay;

        if(delay >= state->length)
            delay = state->length;

        // Adjust scale for 24 bit precision 
        scale *= 1 << 24 - 1;

        if(delay == 0) {
            // zero delay -- save to buffer & output the same
            SmInt * restrict pntr = (SmInt *)state->pntr;
            Int base = (Int)state->base;
            Int eob = base + (state->length-1) * sizeofElement;

            TRACE((&trace, "zero delay"));

            for(i=0;i<sampleCount;++i) {
                // Compute the value and save and also copy to output buffer
                value = _sshl(_spint(*data++ * scale),8) >> 8;
                *outptr = *pntr = value & 0x00FF;
                *(outptr+1) = *(pntr+1) = (value >> 8) & 0x00FF;
                *(outptr+2) = *(pntr+2) = (value >> 16) & 0x00FF;
               
                outptr += sizeofElement;

                // Check for delay buffer pointer overflow 
                if((Int)pntr == eob)
                    pntr = (SmInt *)base;
                else
                    pntr+= sizeofElement; 
            }
            state->pntr = (PAF_AudioData *)pntr;
            if(state->count < state->length)
                state->count += sampleCount;
        } 
        else {
            // non-zero delay -- implement in zero and non-zero phases
            SmInt * restrict pntr = (SmInt *)state->pntr;
            SmInt * restrict next;
            Int base = (Int)state->base;
            Int eob = base + (state->length-1) * sizeofElement;

            TRACE((&trace, "non zero delay %d" LINE_END, delay));

            if(state->count < delay) {
                if((startCount = delay - state->count) > sampleCount)
                    startCount = sampleCount;
                state->count += sampleCount;
            }
            else {
                startCount = 0;
                if(state->count < state->length)
                    state->count += sampleCount;
            }

            for(i=0;i<startCount;++i) {
                // Compute the value and save
                value = _sshl(_spint(*data++ * scale),8) >> 8;
                *pntr = value & 0x00FF;
                *(pntr+1) = (value >> 8) & 0x00FF;
                *(pntr+2) = (value >> 16) & 0x00FF;

                // output zero's 
                *outptr = 0;
                *(outptr+1) = 0;
                *(outptr+2) = 0;
                outptr += sizeofElement;

                // Check for delay buffer pointer overflow 
                if((Int)pntr == eob)
                    pntr = (SmInt *)base;
                else
                    pntr += sizeofElement;
            }

            next = pntr - delay * sizeofElement;
            if((Int)next < (Int)state->base)
                next += state->length * sizeofElement;

            for(;i<sampleCount;++i) {
                // Save the sample value
                value = _sshl(_spint(*data++ * scale),8) >> 8;
 
                // Copy delayed output samples
                *outptr = *next;
                *(outptr+1) = *(next+1);
                *(outptr+2) = *(next+2);
                outptr += sizeofElement;

                // Compute the value and save
                *pntr = value & 0x00FF;
                *(pntr+1) = (value >> 8) & 0x00FF;
                *(pntr+2) = (value >> 16) & 0x00FF;


                // Check for delay buffer pointer overflow 
                if((Int)next == eob)
                    next = (SmInt *)base;
                else
                    next += sizeofElement;

                if((Int)pntr == eob)
                    pntr = (SmInt *)base;
                else
                    pntr += sizeofElement; 
            }

            state->pntr = (PAF_AudioData *)pntr;
        } 
    }
    return 0;
}

#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
Int
PCE_TII_delayDAT(
    PAF_AudioFrame * restrict pAudioFrame,
    PAF_DelayState * restrict state,
    SmInt * restrict swap,  
    Int apply,
    Int channel,
    Int unit,
    PAF_AudioData scale,
    Int sizeofElement)
{
    static const Float sampleRateScale[] = {
        0.0,                             // unused
        0.0010,                          // milliseconds (Q0)
        0.0005,                          // milliseconds (Q1)
        0.0010 / (SOS * 100.0),          // milliseconds per centimeter
        0.0010 / (SOS * 3.28084),        // milliseconds per foot
        0.0010 / (SOS * 1.093613),       // milliseconds per yard
        0.0010 / (SOS * 1.0),            // milliseconds per meter
        0.0001,                          // milliseconds (tenths)
    };

    PAF_AudioData * restrict data = pAudioFrame->data.sample[channel];
    SmInt * restrict outptr = (SmInt *)pAudioFrame->data.sample[channel];
    Int datid;

    if(!apply || !data) {
        // Reset delay
        state->count = 0;
        state->pntr = state->base;
    }
    else {
        // Apply delay
        Uns sampleCount = pAudioFrame->sampleCount;
        Uns sampleLength = sampleCount * sizeofElement;
        Uns totlen, buflen, dellen, len, off;
        Uns startCount;
        Uns delay;
        Uns i;
        XDAS_Int32 value; 
        Int wraplen;
        Int eob;

        // Compute delay according to units and saturate to buffer length
        if(unit >= lengthof(sampleRateScale))
            unit = 0;

        if(!unit)
            delay = state->delay;
        else
            delay = sampleRateScale[unit] *
                    pAudioFrame->fxns->sampleRateHz(pAudioFrame,  
                        pAudioFrame->sampleRate, 
                        PAF_SAMPLERATEHZ_STD) *
                    state->delay;

        if(delay >= state->length)
            delay = state->length;

        TRACE((&trace, "PCE_TII_delayDAT: delay is %d.  channel %d, scale %f." LINE_END, delay, channel, scale));

        // Adjust scale for 24 bit precision 
        scale *= 1 << 24 - 1;

        if(delay == 0) {
            // zero delay -- save to buffer & output the same
            for(i=0;i<sampleCount;++i) {
                // Compute the value and save and also copy to output buffer
                value = _sshl(_spint(*data++ * scale),8) >> 8;
                *outptr = *swap = value & 0x00FF;
                *(outptr+1) = *(swap+1) = (value >> 8) & 0x00FF;
                *(outptr+2) = *(swap+2) = (value >> 16) & 0x00FF;
               
                outptr += sizeofElement;
                swap += sizeofElement; 
            }

            swap -= sampleLength; 
            wraplen = state->length * sizeofElement - ((Int)state->pntr - (Int)state->base);
            datid = DAT_copy(swap, (SmInt *)state->pntr, wraplen < sampleLength ? wraplen : sampleLength);
            DAT_wait(datid);
            if(wraplen < sampleLength) {
                datid = DAT_copy(swap+wraplen, (SmInt *)state->base, sampleLength - wraplen);
                DAT_wait(datid);
            }
            state->pntr = (PAF_AudioData *)((Int)state->pntr + sampleLength);
            eob = (Int)state->base + state->length * sizeofElement;
            if((Int)state->pntr >= eob)
                state->pntr = (PAF_AudioData *)((Int)state->pntr - state->length * sizeofElement);
            if(state->count < state->length)
                state->count += sampleCount;
        } 
        else {
            // non-zero delay -- implement in zero and non-zero phases
            SmInt * restrict next;

            if(state->count < delay) {
                if((startCount = delay - state->count) > sampleCount)
                    startCount = sampleCount;
                state->count += sampleCount;
            }
            else {
                startCount = 0;
                if(state->count < state->length)
                    state->count += sampleCount;
            }

            for(i=0;i<startCount;++i) {
                // Compute the value and save
                value = _sshl(_spint(*data++ * scale),8) >> 8;
                *swap = value & 0x00FF;
                *(swap+1) = (value >> 8) & 0x00FF;
                *(swap+2) = (value >> 16) & 0x00FF;

                // output zero's 
                *outptr = 0;
                *(outptr+1) = 0;
                *(outptr+2) = 0;

                outptr += sizeofElement;
                swap += sizeofElement;
            }

            for(;i<sampleCount;++i) {
                // Save the sample value
                value = _sshl(_spint(*data++ * scale),8) >> 8;
 
                // Compute the value and save
                *swap = value & 0x00FF;
                *(swap+1) = (value >> 8) & 0x00FF;
                *(swap+2) = (value >> 16) & 0x00FF;

                swap += sizeofElement; 
            }
#ifdef PCE_ALIGN_128
            datid = DAT_copy((SmInt*)pAudioFrame->data.sample[channel],
                (SmInt*)pAudioFrame->data.sample[channel],
                sizeof(PAF_AudioData)*sampleCount);
            DAT_wait(datid);
#endif
            swap -= sampleLength;
            if(sampleCount - startCount) {
                next = (SmInt *)((Int)state->pntr + startCount * sizeofElement - delay * sizeofElement);
                if((Int)next < (Int)state->base)
                    next = (SmInt *)((Int)next + state->length * sizeofElement);
                totlen = (sampleCount-startCount) * sizeofElement;
                wraplen = state->length * sizeofElement - ((Int)next - (Int)state->base);
                buflen = state->length * sizeofElement;
                dellen = delay * sizeofElement;
                if((Int)next == (Int)state->pntr && startCount == delay) {
                    datid = DAT_copy((SmInt*)swap, outptr, totlen);
                    DAT_wait(datid);
                }
                else {
                    if( totlen <= dellen) {
                        datid = DAT_copy(next, outptr, totlen > wraplen ? wraplen : totlen);
                        DAT_wait(datid);
                        if(totlen > wraplen) {
                            datid = DAT_copy((SmInt*)state->base, outptr+wraplen, 
                                     totlen <= buflen ? totlen - wraplen : buflen - wraplen);
                            DAT_wait(datid);
                        }
                        if(totlen > buflen) {
                            datid = DAT_copy((SmInt*)swap, outptr+buflen, totlen - buflen);
                            DAT_wait(datid);
                        }
                    }
                    else {
                        len = dellen > wraplen ? wraplen : dellen;
                        datid = DAT_copy(next, outptr, len);
                        DAT_wait(datid);
                        off = len;
                        if(dellen > wraplen) {
                            datid = DAT_copy((SmInt*)state->base, outptr+off,dellen - wraplen); 
                            DAT_wait(datid);
                            off = dellen;
                        }
                        datid = DAT_copy((SmInt*)swap, outptr+off, totlen - dellen);
                        DAT_wait(datid);
                    }
                }
            }
            totlen = sampleLength; 
            wraplen = state->length * sizeofElement - ((Int)state->pntr - (Int)state->base);
            buflen = state->length * sizeofElement;
            datid = DAT_copy(swap, (SmInt *)state->pntr, totlen > wraplen ? wraplen : totlen);
            DAT_wait(datid);
            if(totlen > wraplen) {
                totlen -= wraplen;
                off = wraplen;
                while(totlen) {
                    datid = DAT_copy(swap+off, (SmInt *)state->base, totlen > buflen ? buflen : totlen);
                    DAT_wait(datid);
                    totlen -= totlen > buflen ? buflen : totlen;
                    off += totlen > buflen ? buflen : totlen;
                } 
            }

            state->pntr = (PAF_AudioData *)((Int)state->pntr + sampleLength);
            eob = (Int)state->base + state->length * sizeofElement;
            while((Int)state->pntr >= eob)
                state->pntr = (PAF_AudioData *)((Int)state->pntr - state->length * sizeofElement);
        } 
    }
    return 0;
}
#endif /* PAF_DEVICE */

/*
 *  ======== PCE_TII_resetDelay ========
 *  TII's implementation of the delay reset operation.
 */

Int 
PCE_TII_delayReset(
    IPCE_Handle handle, 
    ALG_Handle sioHandle, 
    PAF_EncodeControl *pEncodeControl,
    PAF_EncodeStatus *pEncodeStatus,
    IPCE_StatusPhase *pStatusPhase,
    IPCE_ConfigPhase *pConfigPhase, 
    PAF_IALG_Common *pConfigCommon, 
    PAF_ActivePhase *pActivePhase,
    PAF_ScrachPhase *pScrachPhase) 
{
    PCE_TII_Obj *pce = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pEncodeControl->pAudioFrame;

    IPCE_ConfigPhaseDelay *pConfigDelay = (IPCE_ConfigPhaseDelay *)pConfigPhase; 

    PAF_DelayState * restrict state = pConfigDelay->state;

    PAF_ScrachPhaseDelay *pScrachDelay = (PAF_ScrachPhaseDelay *)pScrachPhase;

    Int errno = 0;
    Int errch;

    Int iC, iS;

    /* Reset delay state */
    for(iC=0, iS=0;
        iC < pce->pStatus->del.numc && iS < pce->pStatus->del.nums;
        iC++) {
        if(pConfigDelay->mask.bits & (1 << iC)) {
            if(!pScrachDelay)
                errch = handle->fxns->delay(pAudioFrame, &state[iS++],
                            0, iC, 0, 0, pConfigDelay->sizeofElement);
            else
                errch = handle->fxns->delayDAT(pAudioFrame, &state[iS++],
                            (SmInt *)((Int)pScrachDelay+sizeof(PAF_ScrachPhaseDelay)), 
                            0, iC, 0, 0, pConfigDelay->sizeofElement);
            if(errch && !errno)
                errno = errch + iC;
        }
    }

    return errno;
}

/*
 *  ======== PCE_TII_delayInit ========
 *  TII's implementation of the delay init operation.
 */

Int
PCE_TII_delayInit(
    IPCE_Handle handle, 
    IPCE_StatusPhase *pStatusPhase, 
    IPCE_ConfigPhase *pConfigPhase, 
    PAF_IALG_Common *pConfigCommon, 
    PAF_ActivePhase *pActivePhase,
    PAF_ScrachPhase *pScrachPhase, 
    IPCE_ConfigPhase *paramsConfigPhase, 
    PAF_IALG_Common *paramsConfigCommon, 
    PAF_ActivePhase *paramsActivePhase,
    PAF_ScrachPhase *paramsScrachPhase)
{
    PCE_TII_Obj *pce = (Void *)handle;

    IPCE_ConfigPhaseDelay *pConfigDelay = (IPCE_ConfigPhaseDelay *)pConfigPhase;
    IPCE_ConfigPhaseDelay *paramsConfigDelay = (IPCE_ConfigPhaseDelay *)paramsConfigPhase;

    Int i;

    TRACE((&trace, "PCE_TII_delayInit:  pConfigDelay 0x%x, paramsConfigDelay 0x%x" LINE_END, pConfigDelay, paramsConfigDelay));

    if(pConfigDelay && paramsConfigDelay)
    {
        pConfigDelay->size = paramsConfigDelay->size;
        pConfigDelay->mask.bits = paramsConfigDelay->mask.bits;
        pConfigDelay->sizeofElement = paramsConfigDelay->sizeofElement;
        pConfigDelay->state = (PAF_DelayState *)((Int)pConfigDelay + 
                                   sizeof(IPCE_ConfigPhaseDelay));

        for(i=0; i < pce->pStatus->del.nums; i++)
            pConfigDelay->state[i] = paramsConfigDelay->state[i];

        if(pConfigCommon && paramsConfigCommon) {
            Int base;
            pConfigCommon->size = 0;
            pConfigCommon->flag = 0;

            base = (Int)pConfigCommon + sizeof(PAF_IALG_Common);
            for(i=0; i < pce->pStatus->del.nums; i++) {
#ifdef PCE_ALIGN_128
                base=(base+0x7F)&0xFFFFFF80;
#endif
                pConfigDelay->state[i].base = (PAF_AudioData *)base;
                base = base + paramsConfigDelay->state[i].length *
                              paramsConfigDelay->sizeofElement; 
            }
            
        }
    }

    return 0; 
}
 
