
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder algoritm IALG implementation
//
//
//

/*
 *  PCE Module IALG implementation - TII's implementation of the
 *  IALG interface for the PCM Encoder algoritm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>

#include <ialg.h>
#include <ipce.h>
#include <pce_tii.h>
#include <pce_tii_priv.h>

#include <logp.h>

// #define ENABLE_TRACE
#ifdef ENABLE_TRACE
 // trace to LOG to observe startup behavior.
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif


/*
 *  ======== PCE_TII_activate ========
 */
  /* COM_TII_activate */

/*
 *  ======== PCE_TII_alloc ========
 */
Int PCE_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IPCE_Params *params = (Void *)algParams;

    Int i, n;

    if (params == NULL) {
        TRACE((&trace, "PCE_TII_alloc: Null params, use defaults."));
        params = &IPCE_PARAMS;  /* set default parameters */
    }

    /* Request memory for PCE object */
    memTab[0].size = (sizeof(PCE_TII_Obj)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;
    TRACE((&trace, "PCE_TII_alloc.%d: memTab[0] persist size 0x%x.", __LINE__, memTab[0].size));

    /* Request memory for PCE status */
    memTab[1].size = (sizeof(PCE_TII_Status)+3)/4*4;
    memTab[1].alignment = 4;
    memTab[1].space = IALG_SARAM;
    memTab[1].attrs = IALG_PERSIST;
    TRACE((&trace, "PCE_TII_alloc.%d: memTab[1] persist size 0x%x.", __LINE__, memTab[1].size));

    /* Request memory for PCE config */
    memTab[2].size = (sizeof(PCE_TII_Config)+3)/4*4; 
    memTab[2].alignment = 4;
    memTab[2].space = IALG_SARAM;
    memTab[2].attrs = IALG_PERSIST;
    TRACE((&trace, "PCE_TII_alloc.%d: memTab[2] persist size 0x%x.", __LINE__, memTab[2].size));

    /* Request memory for PCE active */
    memTab[3].size = (sizeof(PCE_TII_Active)+3)/4*4;
    memTab[3].alignment = 4;
    memTab[3].space = IALG_SARAM;
    memTab[3].attrs = IALG_PERSIST;
    TRACE((&trace, "PCE_TII_alloc.%d: memTab[3] persist size 0x%x.", __LINE__, memTab[3].size));

    /* Request memory for PCE active */
    memTab[4].size = (sizeof(PCE_TII_Scrach)+3)/4*4;
    memTab[4].alignment = 4;
    memTab[4].space = IALG_SARAM;
    memTab[4].attrs = IALG_PERSIST;
    TRACE((&trace, "PCE_TII_alloc.%d: memTab[4] persist size 0x%x.", __LINE__, memTab[4].size));

    /* Request memory for phase Configs */
    for (i=0, n=5; i < IPCE_PHASES; i++) {
        if(params->pConfig->pConfigPhase[i]) {
            Int size = params->pConfig->pConfigPhase[i]->pConfigPhaseData.size;
            if (size) {
                memTab[n].size = size;
                memTab[n].alignment = 4;
                memTab[n].space = IALG_SARAM;
                TRACE((&trace, "PCE_TII_alloc.%d: memTab[%d] persist size 0x%x.", __LINE__, n, memTab[n].size));
                memTab[n++].attrs = IALG_PERSIST;
            }
        }
    }

    /* Request memory for phase common memory */
    for (i=0; i < IPCE_PHASES; i++) {
        if(params->pConfig->pConfigCommon[i]) {
            Int size = params->pConfig->pConfigCommon[i]->size;
            if (size) {
#ifdef PCE_ALIGN_128
                if(i==1)
                    size+=(params->pStatus->del.nums+1)*128;
#endif
                memTab[n].size = size;
                memTab[n].alignment = 4;
                memTab[n].space = IALG_SARAM;
                TRACE((&trace, "PCE_TII_alloc.%d: memTab[%d] common size 0x%x.", __LINE__, n, memTab[n].size));
                memTab[n++].attrs = (IALG_MemAttrs)params->pConfig->pConfigCommon[i]->flag;
            }
        }
    }

    /* Request memory for phase active memory */
    for (i=0; i < IPCE_PHASES; i++) {
        if(params->pActive->pActivePhase[i]) {
            Int size = params->pActive->pActivePhase[i]->pActivePhaseData.size;
            if (size) {
                memTab[n].size = size;
                memTab[n].alignment = 4;
                memTab[n].space = IALG_SARAM;
                TRACE((&trace, "PCE_TII_alloc.%d: memTab[%d] active size 0x%x.", __LINE__, n, memTab[n].size));
                memTab[n++].attrs = IALG_PERSIST;
            }
        }
    }

    /* Request memory for phase scrach memory */
    for (i=0; i < IPCE_PHASES; i++) {
        if(params->pScrach->pScrachPhase[i]) {
            Int size = params->pScrach->pScrachPhase[i]->pScrachPhaseData.size;
            if (size) {
#ifdef PCE_ALIGN_128
                if(i==1||i==2)
                    size+=128;
#endif
                memTab[n].size = size;
                memTab[n].alignment = 4;
                memTab[n].space = IALG_SARAM;
                TRACE((&trace, "PCE_TII_alloc.%d: memTab[%d] scratch size 0x%x.", __LINE__, n, memTab[n].size));
                // n==11 is the scratch buffer which has to be larger for ASRC.
                // if (n==11) printf("PCE scratch buffer size is %d bytes.\n", size);
                memTab[n++].attrs = IALG_SCRATCH;
            }
        }
    }
    return (n);
}

/*
 *  ======== PCE_TII_deactivate ========
 */
  /* COM_TII_deactivate */

/*
 *  ======== PCE_TII_free ========
 */
  /* COM_TII_free */

/*
 *  ======== PCE_TII_initObj ========
 */
Int PCE_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    PCE_TII_Obj *pce = (Void *)handle;
    const IPCE_Params *params = (Void *)algParams;

    Int i, n;

    if (params == NULL) {
        params = &IPCE_PARAMS;  /* set default parameters */
    }

    pce->pStatus = (PCE_TII_Status *)memTab[1].base;
    pce->pConfig = (PCE_TII_Config *)memTab[2].base;
    pce->pActive = (PCE_TII_Active *)memTab[3].base;
    pce->pScrach = (PCE_TII_Scrach *)memTab[4].base;
    TRACE((&trace, "PCE_TII_initObj.%d: pStatus 0x%x.  pConfig 0x%x.", __LINE__, pce->pStatus, pce->pConfig ));
    TRACE((&trace, "PCE_TII_initObj.%d: pActive 0x%x.  pScrach 0x%x.", __LINE__, pce->pActive, pce->pScrach ));

    if (params->pStatus)
        *pce->pStatus = *params->pStatus;
    else
        pce->pStatus->size = sizeof(pce->pStatus);

    if (params->pConfig) 
    {
        pce->pConfig->size = params->pConfig->size;
        pce->pConfig->frameLength = params->pConfig->frameLength;
        for (i=0, n=5; i < IPCE_PHASES; i++) {
            if(params->pConfig->pConfigPhase[i]) { 
                Int size = params->pConfig->pConfigPhase[i]->pConfigPhaseData.size;
                if (size) {
                    pce->pConfig->pConfigPhase[i] = memTab[n++].base;
                    pce->pConfig->pConfigPhase[i]->pConfigPhaseData.size = size;
                }
            }
            else
                pce->pConfig->pConfigPhase[i] = 0;
        }

        for (i=0; i < IPCE_PHASES; i++) {
            if(params->pConfig->pConfigCommon[i]) { 
                Int size = params->pConfig->pConfigCommon[i]->size;
                if (size) {
                    pce->pConfig->pConfigCommon[i] = memTab[n++].base;
                    pce->pConfig->pConfigCommon[i]->size = size;
                    pce->pConfig->pConfigCommon[i]->flag = 0;
                }
                else
                    pce->pConfig->pConfigCommon[i] = 0;
            }
        }
    }
    else
        pce->pConfig->size = sizeof(pce->pConfig);


    if (params->pActive) 
    {
        pce->pActive->size = params->pActive->size;
        for (i=0; i < IPCE_PHASES; i++) {
            if(params->pActive->pActivePhase[i]) { 
                Int size = params->pActive->pActivePhase[i]->pActivePhaseData.size;
                if (size) {
                    pce->pActive->pActivePhase[i] = memTab[n++].base;
                    pce->pActive->pActivePhase[i]->pActivePhaseData.size = size;
                }
            }
            else
                pce->pActive->pActivePhase[i] = 0;
        }
    }
    else
        pce->pActive->size = sizeof(pce->pActive);

    if (params->pScrach) 
    {
        pce->pScrach->size = params->pScrach->size;
        TRACE((&trace, "PCE_TII_initObj.%d: pce->pScrach->size: 0x%x.", __LINE__, pce->pScrach->size ));
        for (i=0; i < IPCE_PHASES; i++) {
            if(params->pScrach->pScrachPhase[i]) 
            {
                Int size = params->pScrach->pScrachPhase[i]->pScrachPhaseData.size;
                if (size) {
                    pce->pScrach->pScrachPhase[i] = memTab[n++].base;
                    pce->pScrach->pScrachPhase[i]->pScrachPhaseData.size = size;
                    TRACE((&trace, "PCE_TII_initObj: pce->pScrach->pScrachPhase[%d]: 0x%x. size 0x%x", i, pce->pScrach->pScrachPhase[i], size ));
                }
            }
            else
                pce->pScrach->pScrachPhase[i] = 0;
        }
    }
    else
        pce->pScrach->size = sizeof(pce->pScrach);

    for(i=0; i < IPCE_PHASES; i++)
    {
        TRACE((&trace, "PCE_TII_initObj: (IPCE_Handle)handle)->fxns->phaseInit[%d]: 0x%x",
        		i, ((IPCE_Handle)handle)->fxns->phaseInit[i]));

        if(((IPCE_Handle)handle)->fxns->phaseInit[i])
            ((IPCE_Handle)handle)->fxns->phaseInit[i]((IPCE_Handle)handle,
                                  (IPCE_StatusPhase *)&pce->pStatus->phase[i],
                                  pce->pConfig->pConfigPhase[i],
                                  pce->pConfig->pConfigCommon[i],
                                  pce->pActive->pActivePhase[i],
                                  pce->pScrach->pScrachPhase[i],
                                  params->pConfig->pConfigPhase[i],
                                  params->pConfig->pConfigCommon[i],
                                  params->pActive->pActivePhase[i],
                                  params->pScrach->pScrachPhase[i]);
    } 

    return (IALG_EOK);
}

/*
 *  ======== PCE_TII_control ========
 */
Int PCE_TII_control(IALG_Handle handle, IALG_Cmd cmd, IALG_Status *status)
{
    Int control;

    control = COM_TII_control(handle, cmd, status);

    if (control < 0) {
        PCE_TII_Obj *pce = (Void *)handle;

        switch (cmd)
        {
          case IPCE_MININFO:
            return 384;
          case IPCE_MINSAMGEN:
            return PCE_TII_minimumSampleGeneration(pce);
          case IPCE_MAXSAMGEN:
            return PCE_TII_maximumSampleGeneration(pce);
          default:
            return ((Int)-1);
        }
    }
    else {
        return (control);
    }
}

/*
 *  ======== PCE_TII_moved ========
 */
  /* COM_TII_moved */

/*
 *  ======== PCE_TII_numAlloc ========
 */
Int PCE_TII_numAlloc()
{
    return  4 +
            IPCE_PHASES +      /* for IPCE_PHASES phase config's */
            IPCE_PHASES +      /* for IPCE_PHASES common memories */ 
            IPCE_PHASES;       /* for IPCE_PHASES phase active memories */ 
}
