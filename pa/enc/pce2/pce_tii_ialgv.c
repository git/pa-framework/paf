
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder algoritm IALG table definitions
//
//
//

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the PCE_TII module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the PCE is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <std.h>

#include <ialg.h>
#include <ipce.h>

#include <pce_tii.h>
#include <pce_tii_priv.h>
#include <com_tii_priv.h>

#if IPCE_PHASES != 6 
#error internal error
#endif

#define IALGFXNS \
    &PCE_TII_IALG,       /* module ID */                         \
    PCE_TII_activate,    /* activate */                          \
    PCE_TII_alloc,       /* alloc */                             \
    PCE_TII_control,     /* control */			         \
    PCE_TII_deactivate,  /* deactivate */                        \
    PCE_TII_free,        /* free */                              \
    PCE_TII_initObj,     /* init */                              \
    PCE_TII_moved,       /* moved */                             \
    PCE_TII_numAlloc     /* numAlloc */                          \

/*
 *  ======== PCE_TII_IPCE ========
 *  This structure defines TII's implementation of the IPCE interface
 *  for the PCE_TII module.
 */
const IPCE_Fxns PCE_TII_IPCE = {       /* module_vendor_interface */
    IALGFXNS,
    PCE_TII_reset,
    PCE_TII_info,
    PCE_TII_encode,
    /* Phase Init functions */
    PCE_TII_volumeInit,                /* phase 0 init: volume    */
    PCE_TII_delayInit,                 /* phase 1 init: delay     */ 
    NULL,                              /* phase 2 init: output    */
    NULL,                              /* phase 3 init: unused    */ 
    NULL,                              /* phase 4 init: unused    */ 
    NULL,                              /* phase 5 init: unused    */
    /* Phase Reset functions */
    PCE_TII_volumeReset,               /* phase 0 reset: volume   */
    PCE_TII_delayReset,                /* phase 1 reset: delay    */ 
    NULL,                              /* phase 2 reset: output   */
    NULL,                              /* phase 3 reset: unused   */
    NULL,                              /* phase 4 reset: unused   */
    NULL,                              /* phase 5 reset: unused   */
    /* Phase Info functions */
    PCE_TII_volumeInfo,                /* phase 0 info: volume    */
    NULL,                              /* phase 1 info: delay     */
    PCE_TII_outputInfo,                /* phase 2 info: output    */
    NULL,                              /* phase 3 info: unused    */
    NULL,                              /* phase 4 info: unused    */
    NULL,                              /* phase 5 info: unused    */
    /* Phase functions */
    PCE_TII_volumePhase,               /* phase 0: volume         */
    PCE_TII_delayPhase,                /* phase 1: delay          */
    PCE_TII_outputPhase,               /* phase 2: output         */
    NULL,                              /* phase 3: unused         */
    NULL,                              /* phase 4: unused         */  
    NULL,                              /* phase 5: unused         */
//    COM_TII_activateCommon,
//    COM_TII_deactivateCommon,
    PCE_TII_outputCheck,
#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE
    PCE_TII_outputAudioDouble,
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
    PCE_TII_outputAudioFloat,
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT32
    PCE_TII_outputAudioLgInt,
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT16
    PCE_TII_outputAudioMdInt,
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT8
    PCE_TII_outputAudioSmInt,
#else
#error unsupported option
#endif
    PCE_TII_outputCopy,
    PCE_TII_outputFinal,
    PCE_TII_delay,
#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
    PCE_TII_delayDAT,
#else /* PAF_DEVICE */
    NULL,
#endif /* PAF_DEVICE */
    PCE_TII_outputMdInsert
};

/*
 *  ======== PCE_TII_IALG ========
 *  This structure defines TII's implementation of the IALG interface
 *  for the PCE_TII module.
 */
#ifdef _TMS320C6X
#ifdef __TI_EABI__
asm("PCE_TII_IALG .set PCE_TII_IPCE");
#else /* _EABI_*/
asm("_PCE_TII_IALG .set _PCE_TII_IPCE");
#endif /* _EABI_*/
#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns PCE_TII_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif

