
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.  
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder algoritm functionality implementation
//
//   Part 2: Output encode phase
//
//
//

#include <std.h>
#include <csl_dat.h>    // DAT_copy

#include <ipce.h>
#include <pce_tii.h>
#include <pce_tii_priv.h>
#include <pceerr.h>

#include "paftyp.h"

#if 0 // defined (ASYNC_SINGLE_ZSA) || defined (ASYNC_DUAL_ZAA)
 #define TASK_PROTECT_DAT_COPY
 #include <tsk.h>
#endif

// -----------------------------------------------------------------------------

#define DISABLE_TRACE    0
#define ENABLE_LOG_TRACE 1

#define TRACE_MODE    DISABLE_TRACE

#include <logp.h>
#if (TRACE_MODE == ENABLE_LOG_TRACE)
  #define TRACE(a) LOG_printf a
  #define TRACE2(a) LOG_printf a
  #define LINE_END
#else
 #define TRACE(a)
 #define TRACE2(a)
  #define LINE_END
#endif


extern __cregister volatile unsigned int CSR;
extern __cregister volatile unsigned int FADCR;
extern __cregister volatile unsigned int FAUCR;
extern __cregister volatile unsigned int FMCR;
//Program Files\Texas Instruments\C6000 Code Generation Tools 6.1.13\include\c6x.h

#ifndef _TMS320C6X
#include "c67x_cintrins.h"
#endif

#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
#include <dmax_dat.h>
#endif /* PAF_DEVICE */

// If Davinci then omit choose un-omptimized conversion. For some
// reason the intrinsic _fixfi is not producing valid values. So
// attempts to simply define spint as fixfi in the makefile doesn't
// produce audible output. Hence, for now, we simply omit optimization.
#define ENC_OPT
#if ((PAF_DEVICE&0xFF000000) == 0xDD000000)
#undef ENC_OPT
#endif

// Local symbol definitions
#if PAF_AUDIODATATYPE_FIXED
#define ZERO 0
#else
#define ZERO 0.
#endif

/* For MD insertion */
#define MDFORMAT_SYNC_MARKER_SAMP_DELTA         ( 64 )      // distance in samples between successive sync markers
#define MDFORMAT_SYNC_MARKER                    ( 0xA5 )    // sync marker
#define MDFORMAT_SYNC_MARKER_NBYTES             ( 1 )       // number of bytes used for marker
#define MDFORMAT_SYNC_PHASE_NBYTES              ( 1 )       // number of bytes used for phase
#define MDFORMAT_SYNC_DATA_NBYTES               ( MDFORMAT_SYNC_MARKER_NBYTES + MDFORMAT_SYNC_PHASE_NBYTES )

#define MDFORMAT_NUM_PRV_MD_NBYTES              ( 1 )   // sizeof (XDAS_Uint8)
#define MDFORMAT_PAF_PRV_MD_OFFSET_NBYTES       ( 2 )   // sizeof (XDAS_Uint16)
#define MDFORMAT_PAF_PRV_MD_SIZE_NBYTES         ( 2 )   // sizeof (XDAS_Uint16)
#define MDFORMAT_PAF_PRV_MD_HDR_SIZE_NBYTES     ( MDFORMAT_PAF_PRV_MD_OFFSET_NBYTES + MDFORMAT_PAF_PRV_MD_SIZE_NBYTES )

#define F2INT_PREC        24
#define F2INT(x, prec)    ( _sshl(_spint(x * (1<<(prec-1))), 32-prec) & 0xFFFFFF00 )

/* Get IO buffer info */
static Int getIobInfo(
    IPCE_Handle handle, 
    PAF_AudioFrame *pAudioFrame,
    PAF_ChannelMask_HD streamMask, 
    PAF_ActivePhaseOutput *pActivePhaseOutput, 
    PAF_AudioData **chPtrsFrom, 
    XDAS_Int32 **chPtrsTo, 
    XDAS_UInt8 *pNumChs, 
    XDAS_Int32 *pStride,
    XDAS_Int32 *pPrec
);

/* Format audio frame */
static Int fmtAudioFrame(
    PAF_AudioFrame *pAudioFrame, 
    PAF_AudioData **chPtrsFrom , 
    XDAS_UInt8 numChs,
    XDAS_Int32 prec
);

/* Copy formatted AFB to OB */
static Int copyFmtAfbToOb(
    PAF_AudioData **chPtrsFrom, 
    XDAS_Int32 **chPtrsTo, 
    XDAS_UInt8 numChs, 
    XDAS_Int32 sampleCount, 
    XDAS_Int32 stride
);


/*
 *  ======== PCE_TII_outputPhase ========
 *  TII's implementation of the output operation.
 */

Int
PCE_TII_outputPhase(IPCE_Handle handle,
                    PAF_EncodeInStruct *pEncodeInStruct,
                    PAF_EncodeOutStruct *pEncodeOutStruct,
                    IPCE_StatusPhase *pStatusPhase,
                    IPCE_ConfigPhase *pConfigPhase,
                    PAF_IALG_Common *pConfigCommon,
                    PAF_ActivePhase *pActivePhase,
                    PAF_ScrachPhase *pScrachPhase,
                    PAF_ChannelMask_HD streamMask)
{
    PCE_TII_Obj *pce = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pEncodeInStruct->pAudioFrame;

    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_EncodeStatus *pEncodeStatus = pActivePhaseOutput->pEncodeStatus;

    Int sampleCount = pAudioFrame->sampleCount;
    Int nChannels = pAudioFrame->data.nChannels;

    Int i;

    Int errno;

    PAF_ChannelMask_HD encodeMask
        = pAudioFrame->fxns->channelMask (pAudioFrame,
            pEncodeStatus->channelConfigurationEncode);

    TRACE((&trace, "PCE_TII_outputPhase.%d" LINE_END, __LINE__));

    // Implement status.

    {
        PAF_ChannelConfiguration stream
            = pAudioFrame->channelConfigurationStream;
        pEncodeStatus->channelConfigurationStream = stream;
        pEncodeStatus->listeningFormat =
            pAudioFrame->fxns->programFormat (pAudioFrame, stream, 0);

    }
    PAF_PROCESS_COPY (pEncodeStatus->sampleProcess, pAudioFrame->sampleProcess);

    if (handle->fxns->outputCheck
        && (errno = handle->fxns->outputCheck(handle, pActivePhase, sampleCount, 0)))
        return errno;

    if ((!pEncodeStatus->encBypass) && (pce->pStatus->mdInsert))
    {
        PAF_ChannelConfiguration cc;

#if (PAF_MAXNUMCHAN <= 16)
        cc.full = PAFCC_SIXTEEN_CHANNEL_CONFIG_FULL;
#elif (PAF_MAXNUMCHAN <= 32)
        cc.full = PAFCC_THIRTYTWO_CHANNEL_CONFIG_FULL;
#endif
        streamMask = pAudioFrame->fxns->channelMask(pAudioFrame, cc);
    }

    if (handle->fxns->outputAudio)
    {

        for (i=0; i < nChannels; i++) {
            Int from = pEncodeStatus->channelMap.from[i];
            Int to = pEncodeStatus->channelMap.to[i];
            if (to >= 0) {
                if (from < -1)
                {
                    TRACE2((&trace, "PCE_TII_outputPhase.%d, at i=%d, from is negative (%d)" LINE_END, __LINE__, i, from));
                }
                else if ( (from == -1)||((pEncodeStatus->command2 & 0x01)==0x01))
                {   // if encoder is muted.
                    TRACE2((&trace, "PCE_TII_outputPhase.%d, i=%d, muted.  outputAudio to 0x%x" LINE_END, __LINE__, i, to));
                    handle->fxns->outputAudio(handle, pActivePhase, pAudioFrame->data.sample[0], ZERO, to);
                    //writeENCCommandMute = pEncodeStatus->command2
                    //MID#1702 Added feature to mute/unmute audio output.
                    //pa/i/d710e001/pa/enc/pce2/pce_tii_output.c , Revision: 1.9.2.1 (7x branch)
                    //
                }
                else if (! (streamMask & (1 << from)))
                {   // not valid data/active channel, must write zeros
                    TRACE2((&trace, "PCE_TII_outputPhase i:%d, inactive. outputAudio from 0x%x to 0x%x" LINE_END, i, from, to));
                    handle->fxns->outputAudio(handle, pActivePhase, pAudioFrame->data.sample[from], ZERO, to);
                }
                else if (pce->pActive->bitstreamMask.bits & (1 << from))
                {
                    // bitstreamMask.bits is set in delay function. Delay should not be enabled in bypass mode.
                    // doesn't affect stereo because stereo is not delayed.
                    // Delay should not be enabled if bypassed.

                    TRACE2((&trace, "PCE_TII_outputPhase. i=%d. 'delayed' outputCopy from 0x%x to 0x%x" LINE_END, i, from, to));
                    TRACE2((&trace, "PCE_TII_outputPhase.%d, pce->pActive->bitstreamMask.bits 0x%x. i: %d" LINE_END, __LINE__, pce->pActive->bitstreamMask.bits, i));
                    handle->fxns->outputCopy(handle, pActivePhase, pAudioFrame->data.sample[from], to);
                }
                else
                {

                    TRACE2((&trace, "PCE_TII_outputPhase.  i=%d, active outputAudio from 0x%x to 0x%x with scale %f." LINE_END,
                                    i, from, to, pce->pConfig->scale[from]));
                    handle->fxns->outputAudio(handle, pActivePhase, pAudioFrame->data.sample[from], pce->pConfig->scale[from], to);
                }
            }
        }
    }

    if ((!pEncodeStatus->encBypass) && (pce->pStatus->mdInsert))
    {
        if (handle->fxns->outputMdInsert
            && (errno = handle->fxns->outputMdInsert(handle, pActivePhase, pAudioFrame, streamMask)))
            return errno;
    }

    if (handle->fxns->outputFinal
        && (errno = handle->fxns->outputFinal(handle, pActivePhase, sampleCount, 0)))
        return errno;

    return 0;
}

/*
 *  ======== PCE_TII_outputCheck ========
 *  TII's implementation of the outputCheck operation.
 */

Int
PCE_TII_outputCheck(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, int count, int strideOverride)
{
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    Int size = pOutCfg->sizeofElement;
    Int stride = strideOverride > 0 ? strideOverride : pOutCfg->stride;
    Int lengthofData = count * stride;
    Int sizeofData = lengthofData * size;

    pOutCfg->putStride = stride;

    // Check for input (format) errors.

    if (pOutCfg->pntr.pSmInt == NULL)
        return PCEERR_OUTPUT_POINTERNULL;
    else if (pOutCfg->pntr.pSmInt < pOutCfg->base.pSmInt
       || pOutCfg->pntr.pSmInt > pOutCfg->base.pSmInt + pOutCfg->sizeofBuffer)
        return PCEERR_OUTPUT_POINTERRANGE;

    switch (size) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 8:
        break;
      default:
        return PCEERR_OUTPUT_ELEMENTSIZE;
    }

    if (sizeofData > pOutCfg->sizeofBuffer)
        return PCEERR_OUTPUT_FRAMESIZE;

    // Set input pointer & counts.

    pOutCfg->pntr.pSmInt += sizeofData;
    pOutCfg->lengthofData += lengthofData;
    pOutCfg->putCount = count;

    // Check for output (overflow) errors.

    if (pOutCfg->pntr.pSmInt > pOutCfg->base.pSmInt + pOutCfg->sizeofBuffer)
        return PCEERR_OUTPUT_RESULTRANGE;

        // Others TBD. --Kurt

    return 0;
}

/*
 *  ======== PCE_TII_outputFinal ========
 *  MDS's implementation of the outputFinal operation.
 */

Int
PCE_TII_outputFinal(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, int count, int strideOverride)
{
#if 1
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    Int size = pOutCfg->sizeofElement;
    Int stride = strideOverride > 0 ? strideOverride : pOutCfg->stride;
    Int lengthofData = count * stride;
    Int sizeofData = lengthofData * size;

#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))

    PAF_ScrachPhaseOutput *pScrachOutput = (PAF_ScrachPhaseOutput *)(((PCE_TII_Obj *)handle)->pScrach->pScrachPhase[2]);
#ifndef PCE_ALIGN_128
    SmInt * pByte = (SmInt *)((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput));
#else
    SmInt * pByte = (SmInt *)(((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput)+0x7F)&0xFFFFFF80);
#endif
#endif

    // Check for input (format) errors.

    if (pOutCfg->pntr.pSmInt == NULL)
        return PCEERR_OUTPUT_POINTERNULL;
    else if (pOutCfg->pntr.pSmInt < pOutCfg->base.pSmInt
       || pOutCfg->pntr.pSmInt > pOutCfg->base.pSmInt + pOutCfg->sizeofBuffer)
        return PCEERR_OUTPUT_POINTERRANGE;

    switch (size) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 8:
        break;
      default:
        return PCEERR_OUTPUT_ELEMENTSIZE;
    }

    if (sizeofData > pOutCfg->sizeofBuffer)
        return PCEERR_OUTPUT_FRAMESIZE;

    // Set input pointer & counts.

    pOutCfg->pntr.pSmInt -= sizeofData;
    pOutCfg->lengthofData = 0;
    pOutCfg->putCount = 0;

    // Check for output (overflow) errors.

    if (pOutCfg->pntr.pSmInt < pOutCfg->base.pSmInt)
        return PCEERR_OUTPUT_RESULTRANGE;

#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))

    if(pScrachOutput)
    {
        Int datid;

        TRACE((&trace, "PCE_TII_outputFinal.%d: Scratch copy: from 0x%x, to 0x%x, size %d." LINE_END,
                __LINE__, pByte, pOutCfg->pntr.pSmInt, sizeofData));
#ifdef TASK_PROTECT_DAT_COPY
        TSK_disable();  // Added due to case of 2 or more audio tasks observed interrupting following
                        // call to DAT_copy().  Remove this when DAT_copy is fully re-entrant.
#endif
        datid = DAT_copy(pByte, pOutCfg->pntr.pSmInt, sizeofData);
        DAT_wait(datid);
        //memcpy (pOutCfg->pntr.pSmInt, pByte, sizeofData);
#ifdef TASK_PROTECT_DAT_COPY
        TSK_enable();   // Restore task activity. Remove this when DAT_copy is fully re-entrant.
#endif
    }
#endif
        // Others TBD. --Kurt
#endif
    return 0;
}

/*
 *  ======== PCE_TII_outputMdInsert ========
 *  MDS's implementation of the outputMdInsert operation.
 */
Int
PCE_TII_outputMdInsert(
    IPCE_Handle handle, 
    PAF_ActivePhase *pActivePhase, 
    PAF_AudioFrame *pAudioFrame,
    PAF_ChannelMask_HD streamMask 
)
{
    PCE_TII_Obj *pce = (Void *)handle;
    PAF_ActivePhaseOutput *pActivePhaseOutput;
    PAF_AudioData *chPtrsFrom[PAF_MAXNUMCHAN_HD];
    XDAS_Int32 *chPtrsTo[PAF_MAXNUMCHAN_HD];
    XDAS_UInt8 numChs;
    XDAS_Int32 stride;
    XDAS_Int32 prec;
    XDAS_UInt8 numChMd;
    Int errno;
    
    int i;
    int chIdx,sampIdx;

    TRACE((&trace, "PCE_TII_outputMdInsert.%d" LINE_END, __LINE__));

    pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    /* Get I/O buffer info (AFB and OB) */
    getIobInfo(handle, pAudioFrame, streamMask, pActivePhaseOutput, &chPtrsFrom[0], &chPtrsTo[0], &numChs, &stride, &prec);
    numChMd = (numChs < pce->pStatus->maxNumChMd) ? numChs : pce->pStatus->maxNumChMd;

    /* Format audio frame (insert metadata into AFB) */
    /* Precision for float-to-fixed conversion can be passed in from application via 
       DAP Tx OB precision parameter. In this case, precision must be <= 24 for formatting 
       to operate correctly. fmtAudioFrame the precision is hard-coded to 24 to avoid 
       integration issues (e.g. DAP Tx OB precision set to 32). */
    //if (errno = fmtAudioFrame(pAudioFrame, chPtrsFrom, numChMd, prec))
    if (errno = fmtAudioFrame(pAudioFrame, chPtrsFrom, numChMd, F2INT_PREC))
        return errno;

    /* Copy formatter AFB to OB */
    copyFmtAfbToOb(chPtrsFrom, chPtrsTo, numChs, pAudioFrame->sampleCount, stride);


//test to suppress QNaNs on music_changing_configs_objects.mat
//decoder does not overwrite specific channels and int is passed as float ??
//try to delete such dangerous pattern...


    if ((pAudioFrame->bsMetadata_type == PAF_bsMetadata_Evolution) ||
        (pAudioFrame->bsMetadata_type == PAF_bsMetadata_DdpEvolution))
    {
		sampIdx = 0;
		chIdx = 1;
        for (i = 0; i < pAudioFrame->pafPrivateMetadata[0].size; i++){ //Todo: Private metadata is always 0 by now, so hard coded as 0 
			pAudioFrame->data.sample[chIdx++][sampIdx] = 0.;

            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }

		}


	}

    return 0;
}

/*
 *  ======== PCE_TII_outputAudioSmInt ========
 *  TII's implementation of the outputAudioSmInt operation.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT8
#warn implementation sub-optimal
#warn scale not implemented
Int
PCE_TII_outputAudioSmInt(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, SmInt *pWord, SmInt scale, int offset)
{
#ifndef ENC_OPT
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    SmInt x;
    LgInt value;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
    while (count--) {

        x = (pWord ? *pWord++ : 0);
        {
            Int shift;
            LgInt upper;

            value = x << 8*(sizeof(value) - sizeof(x));
            if ((shift = 31-prec) >= 0) {
                upper = value + (1 << shift);
                if (value > 0 && upper < 0)
                    upper = 0x7fffffff;
                value = upper & (~0 << shift+1);
            }
        }

        switch (size) {
          case 1:
            *pntr.pSmInt = value >> 24;
            break;
          case 2:
            *pntr.pMdInt = value >> 16;
            break;
          case 4:
            if (prec <= 32)
                *pntr.pLgInt = value;
            else
                *pntr.pFloat = (double )value / 2147483648.;
            break;
          case 8:
            *pntr.pDouble = (double )value / 2147483648.;
            break;
        }
        pntr.pSmInt += stride * size;
    }

    return 0;
#else /* ENC_OPT */
#error not supported
#endif /* ENC_OPT */
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCE_TII_outputAudioMdInt ========
 *  TII's implementation of the outputAudioMdInt operation.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT16
#warn implementation sub-optimal
#warn scale not implemented
Int
PCE_TII_outputAudioMdInt(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, MdInt *pWord, MdInt scale, int offset)
{
#ifndef ENC_OPT
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    MdInt x;
    LgInt value;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
    while (count--) {

        x = (pWord ? *pWord++ : 0);
        {
            Int shift;
            LgInt upper;

            value = x << 8*(sizeof(value) - sizeof(x));
            if ((shift = 31-prec) >= 0) {
                upper = value + (1 << shift);
                if (value > 0 && upper < 0)
                    upper = 0x7fffffff;
                value = upper & (~0 << shift+1);
            }
        }

        switch (size) {
          case 1:
            *pntr.pSmInt = value >> 24;
            break;
          case 2:
            *pntr.pMdInt = value >> 16;
            break;
          case 4:
            if (prec <= 32)
                *pntr.pLgInt = value;
            else
                *pntr.pFloat = (double )value / 2147483648.;
            break;
          case 8:
            *pntr.pDouble = (double )value / 2147483648.;
            break;
        }
        pntr.pSmInt += stride * size;
    }

    return 0;
#else /* ENC_OPT */
#error not supported
#endif /* ENC_OPT */
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCE_TII_outputAudioLgInt ========
 *  TII's implementation of the outputAudioLgInt operation.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT32
#warn implementation sub-optimal
#warn scale not implemented
Int
PCE_TII_outputAudioLgInt(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, LgInt *pWord, LgInt scale, int offset)
{
#ifndef ENC_OPT
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    LgInt x;
    LgInt value;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
    while (count--) {

        x = (pWord ? *pWord++ : 0);
        {
            Int shift;
            LgInt upper;

            value = x << 8*(sizeof(value) - sizeof(x));
            if ((shift = 31-prec) >= 0) {
                upper = value + (1 << shift);
                if (value > 0 && upper < 0)
                    upper = 0x7fffffff;
                value = upper & (~0 << shift+1);
            }
        }

        switch (size) {
          case 1:
            *pntr.pSmInt = value >> 24;
            break;
          case 2:
            *pntr.pMdInt = value >> 16;
            break;
          case 4:
            if (prec <= 32)
                *pntr.pLgInt = value;
            else
                *pntr.pFloat = (double )value / 2147483648.;
            break;
          case 8:
            *pntr.pDouble = (double )value / 2147483648.;
            break;
        }
        pntr.pSmInt += stride * size;
    }

    return 0;
#else /* ENC_OPT */
#error not supported
#endif /* ENC_OPT */
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCE_TII_outputAudioFloat ========
 *  TII's implementation of the outputAudioFloat operation.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
Int
PCE_TII_outputAudioFloat(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, float *pWord, float scale, int offset)
{
#ifndef ENC_OPT
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    float x;
    LgInt value;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
    while (count--) {

        x = (pWord && scale ? *pWord++ * scale : 0);

        if (prec <= 32) {
            Int shift;
            LgInt upper;

            if (x >= 1.-1./2147483648.)
                value = 0x7fffffff;
            else if (x <= -1.)
                value = 0x80000000;
            else
                value = round(x * 2147483648.);

            if ((shift = 31-prec) >= 0) {
                upper = value + (1 << shift);
                if (value > 0 && upper < 0)
                    upper = 0x7fffffff;
                value = upper & (~0 << shift+1);
            }
        }

        switch (size) {
          case 1:
            *pntr.pSmInt = value >> 24;
            break;
          case 2:
            *pntr.pMdInt = value >> 16;
            break;
          case 4:
            if (prec <= 32)
                *pntr.pLgInt = value;
            else
                *pntr.pFloat = x;
            break;
          case 8:
            *pntr.pDouble = x;
            break;
        }
        pntr.pSmInt += stride * size;
    }

    return 0;
#else /* ENC_OPT */
// #ifdef _TMS320C6X
// #warn implementation not quite optimal
// #endif
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;
    PAF_EncodeStatus *pEncodeStatus = pActivePhaseOutput->pEncodeStatus;
    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;
    PAF_UnionPointer pntr;
    PCE_TII_Obj *pce = (Void *)handle;
    PCE_TII_Status *pStatus;



    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;
    const LgUns one = 1;
    Int tmp;
    float *pTmp;

    float fSampleVal;

    // Variables are declared volatile for debugging.  
    // If not volatile, the loop get optimized and register variables are 
    // not getting updated by CCS-watch.
    // 
    // float detectAccAL;//volatile,ActiveLow
    unsigned int iSampleCast;//volatile
    volatile unsigned int maskedISample;//Don't change.If not volatile, compiler throws off the loop!!
    //
    //31 30  ..0
    //floatExceptionMask=0x7F800000=0xFF<< 23;
    // unsigned int floatExceptionMask=0x7F800000;// 0x7FFFFFFF;//
    volatile unsigned int floatOperationStatus;

    unsigned int maskFADCR; //.L1&.L2,.S1,.S2
    unsigned int maskFMCR;  //.M1&.M2
    unsigned int maskFAUCR;
//
// Reference: spru733.pdf
// TMS320C67x/C67x+ DSP CPU and Instruction Set,Reference Guide
//



#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))

    PAF_ScrachPhaseOutput *pScrachOutput;
    SmInt * pByte;
    PCE_TII_Obj *pPCEobj;

    pPCEobj = (PCE_TII_Obj *)handle;

    // pScrachOutput = (PAF_ScrachPhaseOutput *)(((PCE_TII_Obj *)handle)->pScrach->pScrachPhase[2]);
    pScrachOutput = (PAF_ScrachPhaseOutput *)(pPCEobj->pScrach->pScrachPhase[2]);

    TRACE((&trace, "pScrachOutput: 0x%x.  address is 0x%x.  pPCEobj: 0x%x\n",
                pScrachOutput,
                &pPCEobj->pScrach->pScrachPhase[2],
                pPCEobj ));

    if(pScrachOutput)
    {
#ifndef PCE_ALIGN_128
        pByte   = (SmInt *)((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput));
#else
        pByte   = (SmInt *)(((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput)+0x7F)&0xFFFFFF80);
#endif
        pByte  += count * stride * size;
    }
    else
        pByte = pOutCfg->pntr.pSmInt;

    pntr.pSmInt = pByte + (offset - count * stride) * size;
#else
    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
#endif

    pStatus =(PCE_TII_Status *) pce->pStatus;
    maskFADCR = 0x00330033; //.L1&.L2,.S1,.S2
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 110011   = 0x0033
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 00110011 = 0x0033

    maskFMCR = 0x00330033;  //.M1&.M2
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 110011   = 0x0033
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved RMODE UNDER INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 00110011 = 0x0033

    maskFAUCR = 0x04330433;
    //31 27 26 25 24 23 22 21 20 19 18 17 16
    //Reserved DIV0 UNORD UND INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 0x0433
    //15 11 10 9 8 7 6 5 4 3 2 1 0
    //Reserved DIV0 UNORD UND INEX OVER INFO INVAL DEN2 DEN1 NAN2 NAN1 = 0x0433


    if (! pWord)
        scale = 0.;

    if((0x01==pStatus->pceExceptionDetect))
       // && (0x00==pStatus->pceExceptionFlag) )
    {
        //
        //
        tmp=count;
        pTmp = pWord;

        //pStatus->pceExceptionFlag=0;

        //Floating Point Exception  SPRU733A
        //val=-1s*2^e-127* 1.f, 0<e<255
        //Cases where val=+Inf,-Inf,NaN,QnaN,SnaN & e=255 is flagged as exception.
        //
        //Two approaches:
        //1. Use mask 0x7F800000 (ie b30-b23|b22...b0|  255<<23) to detect.
        //
        //2. register FADCR (Flags .L Unit exception)
        //            FMCR  (Flags .M Unit exception)
        //            FAUCR (Flags .S Unit exception)
        //
        // Use Task disable for correct functonality, that other tasks will not set these bits
        //
        //Among the two approaches 2 is less costly as the loop will not have any conditional statements, disabling pipeline.
        //
        //Tested by loading 0x7F800000 pattern into the pWord buffer.
        //

#ifdef TASK_PROTECT_DAT_COPY
        TSK_disable();  // Added due to case of 2 or more audio tasks accessing registers. Is this needed?
#endif
        //Set to zero
        FADCR &= maskFADCR^0xFFFFFFFF ;
        FMCR  &= maskFMCR ^0xFFFFFFFF ;
        FAUCR &= maskFAUCR^0xFFFFFFFF ;
        // detectAccAL=0;
        #pragma MUST_ITERATE(8,,8)
        #pragma PROB_ITERATE(256,256)
            // The generated loop should contain floating point instructions
            // Depending on optimisation options , this may change.. better code in serial assembly and hook
            while (count--) {

                fSampleVal = *pWord++;


                //*(unsigned int *) &fSampleVal=(*((unsigned int *) &fSampleVal)|0x007FFFFE); //force most of fractional part full
                 iSampleCast=_ftoi(fSampleVal+0.00000023f);//0.00000023 = 2^-22
                 //This is to force float operation in .L : SPINT

                //start with all 1s
                //mulitply 1 & masked pattern
                //Any time all 0s are formed - detected the pattern in  iSampleCast
                //
                maskedISample=iSampleCast+1;
                //detectAccAL+=fSampleVal;

            }//count--




        //reverting the mdoified values
        count = tmp;
        pWord = pTmp ;
        //
        //


        floatOperationStatus=0;

        //Set to zero
        floatOperationStatus|=FADCR & maskFADCR; //.L1 &.L2
        floatOperationStatus|=FMCR  & maskFMCR;  //.M1 &.M2
        floatOperationStatus|=FAUCR& maskFAUCR;  //.S1 &.S2
        if(floatOperationStatus !=0x00000000){
            pStatus->pceExceptionFlag=0x01;
        }
#ifdef TASK_PROTECT_DAT_COPY
        TSK_enable();  // Restore for case of 2 or more audio tasks accessing registers. Is this needed?
#endif

        if((pStatus->pceExceptionMute==0x01) && (pStatus->pceExceptionFlag==0x01))
        {
            //1)Handle the current buffer playback.
            //
            //
            tmp=count;
            pTmp = pWord;
            #pragma MUST_ITERATE(8,,8)
            #pragma PROB_ITERATE(256,256)
            while (count--) {

                 *pWord++=0;

            }//count--
            //reverting the mdoified values
            count = tmp;
            pWord = pTmp ;
            pStatus->pceExceptionMute=0x0;
            //
            //
            //Other approaches
            //Multiply samples by 0
            //may make a multipler 0


            //2)Let all future buffers be muted, until user exlpictly unmutes.
            //  Future calls will be hooking zero samples and will never enter here again
            //
            //pEncodeStatus->command2=0x01; //CRASHES?????? Need more debug...
            //Little ambiguity! User written value(writeENCCommandUnmute)will now be automatically reverted!
            //writeENCCommandUnmute will get honoured only if writePCEExceptionDetectUnmute is explicitly sent.
            // This is to prevent anomalies creating noise which may damage the speakers.
        }


    }//encExceptionDetect!=0

    //
#ifdef TASK_PROTECT_DAT_COPY
    TSK_disable();  // Added due to case of 2 or more audio tasks accessing registers. Is this needed?
#endif
    CSR &= (one << 9)^0xFFFFFFFF ;//9th bit of CSR reigister = Saturate bit
    //Defined in C6000 Code Generation Tools 6.1.13\include\c6x.h
    //

    switch (size) {
      case 1:
#if (PAF_IROM_BUILD == 0xD610A003) || (PAF_IROM_BUILD == 0xD610A004) || (PAF_IROM_BUILD == 0xD710E001)|| (PAF_IROM_BUILD == 0xD8000001)
        return PCEERR_OUTPUT_UNSPECIFIED;
#else
        scale *= one << prec-1;
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pSmInt = scale
                ? _sshl(_spint(*pWord++ * scale), 32-prec) >> 24
                : 0;
            pntr.pSmInt += stride;
        }
#endif
        break;
      case 2:
        scale *= one << prec-1;
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pMdInt = scale
                ? _sshl(_spint(*pWord++ * scale), 32-prec) >> 16
                : 0;
            pntr.pMdInt += stride;
        }
        break;
      case 3:
        {
            SmInt * restrict pntrPrime;

            if (pEncodeStatus->encBypass)
            {   // in 3 byte bypass mode, the input buffer is assumed to contain fixed point data,
                // 24 bits, left justified, to be output with no processing.
                Int   * restrict pInt      = (Int *)pWord;
                Int value;
                pntrPrime = pntr.pSmInt;

                TRACE2((&trace, "outputAudio: 3 byte bypass from 0x%x to 0x%x." LINE_END, pWord, pntr.pLgInt));
    #pragma MUST_ITERATE(8,,8)
    #pragma PROB_ITERATE(256,256)
                while( count-- ) {
                    value = *pInt++;
                    *pntrPrime = _extu(value,16,24);
                    *(pntrPrime+1) = _extu(value,8,24);
                    *(pntrPrime+2) = _extu(value,0,24);
                    pntrPrime += stride * size;
                }
                break;   // all done here.
            }

            if (scale) 
            {   // input is assumed to be floating point, needing to be scaled.
                Int value;
                pntrPrime = pntr.pSmInt;
                scale *= one << prec-1;

        #pragma MUST_ITERATE(8,,8)
        #pragma PROB_ITERATE(256,256)
                while( count-- ) {
                    value = _sshl(_spint(*pWord++ * scale),32-prec);
                    *pntrPrime = _extu(value,16,24);
                    *(pntrPrime+1) = _extu(value,8,24);
                    *(pntrPrime+2) = _extu(value,0,24);
                    pntrPrime += stride * size;
                }
                pntr.pSmInt = pntrPrime;
                break;     // all done here.
            }

            // otherwise zero the data.
            pntrPrime = pntr.pSmInt;
            scale *= one << prec-1;
    #pragma MUST_ITERATE(8,,8)
    #pragma PROB_ITERATE(256,256)
            while( count-- )
            {
                *pntrPrime = 0;
                *(pntrPrime+1) = 0;
                *(pntrPrime+2) = 0;
                pntrPrime += stride * size;
            }
            pntr.pSmInt = pntrPrime;
            break;
        }

      case 4:
        if (pEncodeStatus->encBypass)
        {
           unsigned int *pInt;
           pInt = (unsigned int *)pWord;
           TRACE2((&trace, "outputAudio: 4 byte bypass from 0x%x to 0x%x." LINE_END, pWord, pntr.pLgInt));
           // the scale variable is ignored.
           while (count--)
           {   // the data is treated here as 32 bit ints, whether float or not.
               *pntr.pLgInt = *pInt++;
               pntr.pLgInt += stride;
           }
           break;  // all done here.
        }

        if (prec > 32)
        {
            #if (PAF_IROM_BUILD == 0xD610A003) || (PAF_IROM_BUILD == 0xD610A004) || (PAF_IROM_BUILD == 0xD710E001)|| (PAF_IROM_BUILD == 0xD8000000)
            return PCEERR_OUTPUT_UNSPECIFIED;
        #else
        #pragma MUST_ITERATE(8,,8)
        #pragma PROB_ITERATE(256,256)
            while (count--) {
                *pntr.pFloat = scale ? *pWord++ * scale : 0;
                pntr.pFloat += stride;
            }
        #endif
            break;  // all done here.
        }
        if (scale) 
        {
            Int i;

            TRACE2((&trace, "outputAudio: not bypass, scale %f, from 0x%x to 0x%x." LINE_END, scale, pWord, pntr.pLgInt));

#ifdef TASK_PROTECT_DAT_COPY
            TSK_disable();  // Added due to case of 2 or more audio tasks accessing registers. Is this needed?
#endif

            CSR &= (one << 9)^0xFFFFFFFF ;  //9th bit of CSR reigister = Saturate bit
                                            //Defined in C6000 Code Generation Tools 6.1.13\include\c6x.h

            if (pStatus->mdInsert)
            {
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
                for (i=0; count--; i+=stride)
                    *pWord++ = *pWord * scale;
            }
            else
            {
                scale *= one << prec-1;

                // Is 1.25 cycles/sample optimal? --Kurt
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
                for (i=0; count--; i+=stride)
                    pntr.pLgInt[i] = _sshl(_spint(*pWord++ * scale), 32-prec);

                if( (CSR&(one << 9))!=0 )
                {
                    //if((pceClipDetect&0x10)==0x10)//bitfileds
                    pStatus->pceClipDetect=0x01;
                }
            }

#ifdef TASK_PROTECT_DAT_COPY
            TSK_enable();  // Restore for case of 2 or more audio tasks accessing registers. Is this needed?
#endif
            break;  // all done here
        }   // end scale case.

        {   // no scale
            Int i;

            TRACE2((&trace, "outputAudio: not bypass, no scale, from 0x%x to 0x%x." LINE_END, pWord, pntr.pLgInt));

            // Is 1.00 cycles/sample optimal? --Kurt
            for (i=0; count--; i+=stride)
                pntr.pLgInt[i] = 0;
        }
        break;

      case 8:
#if (PAF_IROM_BUILD == 0xD610A003) || (PAF_IROM_BUILD == 0xD610A004) || (PAF_IROM_BUILD == 0xD710E001)|| (PAF_IROM_BUILD == 0xD8000000)
        return PCEERR_OUTPUT_UNSPECIFIED;
#else
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pDouble = scale ? *pWord++ * scale : 0;
            pntr.pDouble += stride;
        }
#endif
        break;
    }  // end switch (size)

    //
    if( (CSR&(one << 9))!=0 )
    {
        //if((pceClipDetect&0x10)==0x10)//bitfileds
        pStatus->pceClipDetect=0x01;
    }

#ifdef TASK_PROTECT_DAT_COPY
    TSK_enable();  // Restore for case of 2 or more audio tasks accessing registers. Is this needed?
#endif
    //

    return 0;
#endif/* ENC_OPT */
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCE_TII_outputAudioDouble ========
 *  TII's implementation of the outputAudioDouble operation.
 */

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE
Int
PCE_TII_outputAudioDouble(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, double *pWord, double scale, int offset)
{
#ifndef ENC_OPT
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    double x;
    LgInt value;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;
    while (count--) {

        x = (pWord && scale ? *pWord++ * scale : 0);

        if (prec <= 32) {
            Int shift;
            LgInt upper;

            if (x >= 1.-1./2147483648.)
                value = 0x7fffffff;
            else if (x <= -1.)
                value = 0x80000000;
            else
                value = round(x * 2147483648.);

            if ((shift = 31-prec) >= 0) {
                upper = value + (1 << shift);
                if (value > 0 && upper < 0)
                    upper = 0x7fffffff;
                value = upper & (~0 << shift+1);
            }
        }

        switch (size) {
          case 1:
            *pntr.pSmInt = value >> 24;
            break;
          case 2:
            *pntr.pMdInt = value >> 16;
            break;
          case 4:
            if (prec <= 32)
                *pntr.pLgInt = value;
            else
                *pntr.pFloat = x;
            break;
          case 8:
            *pntr.pDouble = x;
            break;
        }
        pntr.pSmInt += stride * size;
    }

    return 0;
#else /* ENC_OPT */
#warn implementation not quite optimal
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    PAF_UnionPointer pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int prec = pOutCfg->precision;
    Int stride = pOutCfg->putStride;

    const LgUns one = 1;

    pntr.pSmInt = pOutCfg->pntr.pSmInt + (offset - count * stride) * size;

    if (! pWord)
        scale = 0.;

    switch (size) {
      case 1:
        scale *= one << prec-1;
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pSmInt = pWord && scale
                ? _sshl(_dpint(*pWord++ * scale), 32-prec) >> 24
                : 0;
            pntr.pSmInt += stride;
        }
        break;
      case 2:
        scale *= one << prec-1;
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pMdInt = pWord && scale
                ? _sshl(_dpint(*pWord++ * scale), 32-prec) >> 16
                : 0;
            pntr.pMdInt += stride;
        }
        break;
      case 3:
        {
        SmInt * restrict pntrPrime = pntr.pSmInt;
        Int value;
        scale *= one << prec-1;
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while( count-- ) {
            value = _dpint(*pWord++ * scale);
            *pntrPrime = (value) & 0x000000FF;
            *(pntrPrime+1) = (value >> 8) & 0x000000FF;
            *(pntrPrime+2) = (value >> 16) & 0x000000FF;
            pntrPrime += stride * size;
        }
        pntr.pSmInt = pntrPrime;
        }
        break;
      case 4:
        if (prec <= 32) {
            if (pWord && scale) {
                Int i;
                scale *= one << prec-1;
                // Is 2.50 cycles/sample optimal? --Kurt
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
                for (i=0; count--; i+=stride)
                    pntr.pLgInt[i] = _sshl(_dpint(*pWord++ * scale), 32-prec);
            }
            else {
                Int i;
                // Is 1.00 cycles/sample optimal? --Kurt
                for (i=0; count--; i+=stride)
                    pntr.pLgInt[i] = 0;
            }
        }
        else {
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
            while (count--) {
                *pntr.pFloat = pWord && scale ? *pWord++ * scale : 0;
                pntr.pFloat += stride;
            }
        }
        break;
      case 8:
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
        while (count--) {
            *pntr.pDouble = pWord && scale ? *pWord++ * scale : 0;
            pntr.pDouble += stride;
        }
        break;
    }

    return 0;
#endif /* ENC_OPT */
}
#endif /* PAF_AUDIODATATYPE */

/*
 *  ======== PCE_TII_outputCopy ========
 *  TII's implementation of the outputCopy operation.
 */

Int
PCE_TII_outputCopy(IPCE_Handle handle, PAF_ActivePhase *pActivePhase, float *pWord, int offset)
{
    SmInt * restrict pSample = (SmInt *)pWord;

    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_OutBufConfig *pOutCfg = pActivePhaseOutput->pOutBufConfig;

    SmInt * restrict pntr;

    Int count = pOutCfg->putCount;
    Int size = pOutCfg->sizeofElement;
    Int stride = pOutCfg->putStride;

    Int i, j;

    PAF_ScrachPhaseOutput *pScrachOutput = (PAF_ScrachPhaseOutput *)(((PCE_TII_Obj *)handle)->pScrach->pScrachPhase[2]);
    SmInt * pByte;

    if(pScrachOutput)
    {
#ifndef PCE_ALIGN_128
        pByte   = (SmInt *)((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput));
#else
        pByte   = (SmInt *)(((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput)+0x7F)&0xFFFFFF80);
#endif
        pByte  += count * stride * size;
    }
    else
        pByte = pOutCfg->pntr.pSmInt;

    pntr = pByte + (offset - count * stride) * size;

    if(size == 3)
    {
        TRACE((&trace, "outputCopy, size 3, from 0x%x, to 0x%x." LINE_END, pSample, pntr));
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
    for (i=0, j=0; count--; i+=stride * size, j+=size) {
        pntr[i] = pSample[j];
        pntr[i+1] = pSample[j+1];
        pntr[i+2] = pSample[j+2];
    }
    }
    else if(size == 4)
    {
        TRACE((&trace, "outputCopy, size 4, from 0x%x, to 0x%x.  size %d.  stride %d," LINE_END,
                pSample, pntr, size, stride));
#pragma MUST_ITERATE(8,,8)
#pragma PROB_ITERATE(256,256)
         // better to use int* here?
        for (i=0, j=0; count--; i+=stride * size, j+=size) {
            pntr[i]   = pSample[j];
            pntr[i+1] = pSample[j+1];
            pntr[i+2] = pSample[j+2];
            pntr[i+3] = pSample[j+3];
        }
    }
    else
        return 1;   // Define proper error -- Girish

    return 0;
}

/*
 *  ======== PCE_TII_outputInfo ========
 *  TII's implementation of the outputInfo operation.
 */

Int
PCE_TII_outputInfo(IPCE_Handle handle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus, PAF_ActivePhase *pActivePhase, PAF_ScrachPhase *pScrachPhase)
{
    PAF_ActivePhaseOutput *pActivePhaseOutput = (PAF_ActivePhaseOutput *)pActivePhase;

    PAF_AudioFrame *pAudioFrame = pEncodeControl->pAudioFrame;

    PAF_ChannelConfiguration request
        = pEncodeStatus->channelConfigurationRequest;
    PAF_ChannelConfiguration stream
        = pAudioFrame->channelConfigurationStream;
    PAF_ChannelConfiguration program
        = request;

    PAF_ChannelMask_HD encodeMask
        = pAudioFrame->fxns->channelMask (pAudioFrame, program);

    Int chn = pAudioFrame->data.nChannels;
    Int chm;
    Int i;

    chm = 0;
    for (i=0; i < chn; i++) {
        if (encodeMask & (1 << i))
            chm++;
    }

    pEncodeStatus->channelCount = chm;

    pEncodeStatus->sampleRate = pAudioFrame->sampleRate;

    pEncodeStatus->channelConfigurationStream = stream;
    pEncodeStatus->channelConfigurationEncode = program;

    pEncodeStatus->programFormat =
        pAudioFrame->fxns->programFormat (pAudioFrame, program, 0);
    pEncodeStatus->listeningFormat =
        pAudioFrame->fxns->programFormat (pAudioFrame, stream, 0);

    pEncodeStatus->frameLength = pAudioFrame->sampleCount;

#if 0 /* TP>> internal error: bad type:  TYPE::type_qualified() --Kurt */
    PAF_PROCESS_COPY (pEncodeStatus->sampleProcess, pAudioFrame->sampleProcess);
#else
    {
        Int i;
        volatile PAF_SampleProcess *to = pEncodeStatus->sampleProcess;
        PAF_SampleProcess *from = pAudioFrame->sampleProcess;
        for (i=0; i < PAF_SAMPLEPROCESS_N; i++)
            *to++ = *from++;
    }
#endif

    pActivePhaseOutput->pEncodeStatus = pEncodeStatus;
    pActivePhaseOutput->pOutBufConfig = pEncodeControl->pOutBufConfig;

    return 0;
}

/* Get IO buffer info */
static Int getIobInfo(
    IPCE_Handle handle, 
    PAF_AudioFrame *pAudioFrame,
    PAF_ChannelMask_HD streamMask, 
    PAF_ActivePhaseOutput *pActivePhaseOutput, 
    PAF_AudioData **chPtrsFrom, 
    XDAS_Int32 **chPtrsTo, 
    XDAS_UInt8 *pNumChs, 
    XDAS_Int32 *pStride,
    XDAS_Int32 *pPrec
)
{
    PAF_EncodeStatus *pEncodeStatus;
    PAF_OutBufConfig *pOutCfg;
    Int count;
    Int size;
    Int stride;
    Int lengthofData;
    Int sizeofData;
    PAF_UnionPointer pntr;
    Uint8 numChs;
    Int from, to; 
    Int i;
#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
    PAF_ScrachPhaseOutput *pScrachOutput;
    SmInt * pByte;
    PCE_TII_Obj *pPceObj;
#endif

    pEncodeStatus = pActivePhaseOutput->pEncodeStatus;
    pOutCfg = pActivePhaseOutput->pOutBufConfig;

    count = pOutCfg->putCount;
    size = pOutCfg->sizeofElement;
    stride = pOutCfg->putStride;
    lengthofData = count * stride;
    sizeofData = lengthofData * size;

#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
    pPceObj = (PCE_TII_Obj *)handle;

    pScrachOutput = (PAF_ScrachPhaseOutput *)(pPceObj->pScrach->pScrachPhase[2]);

    if (pScrachOutput)
    {
#ifndef PCE_ALIGN_128
        pByte   = (SmInt *)((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput));
#else
        pByte   = (SmInt *)(((Int)pScrachOutput+sizeof(PAF_ScrachPhaseOutput)+0x7F)&0xFFFFFF80);
#endif
        pByte  += sizeofData;
    }
    else
    {
        pByte = pOutCfg->pntr.pSmInt;
    }

    pntr.pSmInt = pByte - sizeofData;
#else
    pntr.pSmInt = pOutCfg->pntr.pSmInt - sizeofData;
#endif

    for (i = 0; i < PAF_MAXNUMCHAN_HD; i++)
    {
        chPtrsTo[i] = NULL;
    }

    for (i = 0; i < PAF_MAXNUMCHAN_HD; i++)
    {
        from = pEncodeStatus->channelMap.from[i];
        to = pEncodeStatus->channelMap.to[i];
        if ((to >= 0) && (from >= 0) && 
            (streamMask & (1 << from)))
        {
            chPtrsTo[from] = &pntr.pLgInt[to];
        }
    }

    numChs = 0;
    for (i = 0; i < PAF_MAXNUMCHAN_HD; i++)
    {
        if (((streamMask >> i) & 0x1) && 
            (chPtrsTo[i] != NULL))
        {
            chPtrsFrom[numChs] = &pAudioFrame->data.sample[i][0];
            chPtrsTo[numChs] = chPtrsTo[i];
            numChs++;
        }
    }

    *pPrec = pOutCfg->precision;
    *pStride = stride;
    *pNumChs = numChs;

    return 0;
}

/* Format audio frame */
static Int fmtAudioFrame(
    PAF_AudioFrame *pAudioFrame,
    PAF_AudioData **chPtrsFrom, 
    XDAS_UInt8 numChMd,
    XDAS_Int32 prec
)
{
    XDAS_Int32 *chPtrsI[PAF_MAXNUMCHAN_HD];
    XDAS_Int32 sampleCount;
    XDAS_UInt8 *pSrcData;
    XDAS_UInt16 numBytesXmtAvail; // bytes for transmit
    XDAS_UInt16 numBytesXmt; // bytes to transmit
    XDAS_UInt8 chIdx;
    XDAS_Int16 sampIdx;
    XDAS_Int32 tempL;
    PAF_PrivateMetadata *pPrivateMd;
    XDAS_UInt16 i, j;

    sampleCount = pAudioFrame->sampleCount; // Number of samples in the audio frame
                                            // (in each channel).

    for (chIdx = 0; chIdx < numChMd; chIdx++)
    {
        chPtrsI[chIdx] = (XDAS_Int32 *)chPtrsFrom[chIdx];
    }

    /*-----------------------------*
     * Insert control channel data *
     *-----------------------------*/
    /* Insert sync channel data */
    for (sampIdx = 0; sampIdx < sampleCount; sampIdx++)
    {
        chPtrsI[0][sampIdx] = F2INT(chPtrsFrom[0][sampIdx], prec);
    }
    for (sampIdx = 0; sampIdx < sampleCount; sampIdx += MDFORMAT_SYNC_MARKER_SAMP_DELTA)
    {
        /* Insert marker */
        chPtrsI[0][sampIdx] |= (MDFORMAT_SYNC_MARKER & 0xFF);
        /* Insert phase */
        chPtrsI[0][sampIdx+1] |= ((sampIdx/MDFORMAT_SYNC_MARKER_SAMP_DELTA) & 0xFF);
    }

    /* Initialize sample index */
    sampIdx = MDFORMAT_SYNC_DATA_NBYTES;

    /* Insert number of channels used for metadata */
    chPtrsI[0][sampIdx++] |= (numChMd & 0xFF);

    /* Insert bit-stream metadata */
    /* bitstream metadata update flag */
    chPtrsI[0][sampIdx++] |= (TRUE & 0xFF);

    //if (pafBsMdUpd == XDAS_TRUE)
    //{
        /* 1)  metadata type */
        pSrcData = (Uint8 *)&pAudioFrame->bsMetadata_type;
        for (i = 0; i < sizeof(PAF_bsMetadataTypes); i++)
        {
            chPtrsI[0][sampIdx++] |= (*pSrcData & 0xFF);
            pSrcData++;
        }

        /* 2) sample rate */
        chPtrsI[0][sampIdx++] |= (pAudioFrame->sampleRate & 0xFF);

        /* 3) channel configuration */
        pSrcData = (Uint8 *)&pAudioFrame->channelConfigurationStream;
        for (i = 0; i < sizeof(PAF_ChannelConfiguration); i++)
        {
            chPtrsI[0][sampIdx++] |= (*pSrcData & 0xFF);
            pSrcData++;
        }

        /* 4) offset */
        pSrcData = (Uint8 *)&pAudioFrame->bsMetadata_offset;
        for (i = 0; i < sizeof(XDAS_UInt16); i++)
        {
            chPtrsI[0][sampIdx++] |= (*pSrcData & 0xFF);
            pSrcData++;
        }
    //}

    /* Check enough bytes available for transmission of private metadata */
    /* Compute number of bytes available for transmit */
    numBytesXmtAvail = sampleCount * (numChMd - 1);

    numBytesXmt = 0;

    /* Add byte count for private metadata */
    // W.C. data to transmit:
    //      PAF_AudioFrame      : XDAS_Uint8 numPrivateMetadata                     => 1 bytes (note: initially assume filtering is enabled, so this can only be (0/1)
    //      PAF_PrivateMetadata : XDAS_UInt8 offset                                 => 1 bytes  (note: transmit offset as sample offset only)
    //                          : XDAS_UInt16 size                                  => 2 bytes (2 bytes since max. size is 4096 bytes)
    //                          : XDAS_UInt8 *pMdBuf                                => size bytes (up to 4096)
    if ((pAudioFrame->bsMetadata_type == PAF_bsMetadata_Evolution) ||
        (pAudioFrame->bsMetadata_type == PAF_bsMetadata_DdpEvolution))
    {
        /* Only transmit private metadata in object-audio mode */
        numBytesXmt += MDFORMAT_NUM_PRV_MD_NBYTES;
        numBytesXmt += pAudioFrame->numPrivateMetadata * MDFORMAT_PAF_PRV_MD_HDR_SIZE_NBYTES;
        for (i = 0; i < pAudioFrame->numPrivateMetadata; i++)
        {
            numBytesXmt += pAudioFrame->pafPrivateMetadata[i].size;
        }
    }

    if (numBytesXmt > numBytesXmtAvail)
    {
        TRACE((&trace, "mdFormat:  Metadata is too large.  %d vs %d.\n", numBytesXmt, numBytesXmtAvail));

        return 3;
    }

    /* ------------------------*
     * Insert private metadata *
     * ------------------------*/
    /* Only transmit private metadata in object-audio mode */
    if ((pAudioFrame->bsMetadata_type == PAF_bsMetadata_Evolution) ||
        (pAudioFrame->bsMetadata_type == PAF_bsMetadata_DdpEvolution))
    {
        /* Initialize channel and sample indices */
        chIdx = 1;      /* channel index */
        sampIdx = 0;    /* sample index */

        /* number of private metadata */
        tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
        chPtrsI[chIdx++][sampIdx] = tempL | (pAudioFrame->numPrivateMetadata & 0xFF);
        if (chIdx >= numChMd)
        {
            chIdx = 1;
            sampIdx++;
        }

        for (i = 0; i < pAudioFrame->numPrivateMetadata; i++)
        {
            pPrivateMd = &pAudioFrame->pafPrivateMetadata[i];

            /* offset */
            tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
            chPtrsI[chIdx++][sampIdx] = tempL | (pPrivateMd->offset & 0xFF);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }
            tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
            chPtrsI[chIdx++][sampIdx] = tempL | ((pPrivateMd->offset>>8) & 0xFF);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }

            /* size */
            tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
            chPtrsI[chIdx++][sampIdx] = tempL | (pPrivateMd->size & 0xFF);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }
            tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
            chPtrsI[chIdx++][sampIdx] = tempL | ((pPrivateMd->size>>8) & 0xFF);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }

            /* de-serialized private metadata frame */
            pSrcData = pPrivateMd->pMdBuf;
            for (j = 0; j < pPrivateMd->size; j++)
            {
                tempL = F2INT(chPtrsFrom[chIdx][sampIdx], prec);
                chPtrsI[chIdx++][sampIdx] = tempL | (*pSrcData & 0xFF);
                pSrcData++;

                if (chIdx >= numChMd)
                {
                    chIdx = 1;
                    sampIdx++;
                }
            }
        }
    }

    return 0;
}

/* Copy formatted AFB to OB */
static Int copyFmtAfbToOb(
    PAF_AudioData **chPtrsFrom, 
    XDAS_Int32 **chPtrsTo, 
    XDAS_UInt8 numChs, 
    XDAS_Int32 sampleCount, 
    XDAS_Int32 stride
)
{
    XDAS_UInt8 chIdx;
    XDAS_Int16 sampIdx;
    XDAS_Int32 *chPtrFromI;
    XDAS_Int32 *chPtrTo;

    for (chIdx = 0; chIdx < numChs; chIdx++)
    {
        chPtrFromI = (XDAS_Int32 *)chPtrsFrom[chIdx];
        chPtrTo = chPtrsTo[chIdx];
        for (sampIdx = 0; sampIdx < sampleCount; sampIdx++)
        {
            *chPtrTo = *chPtrFromI++;
            chPtrTo += stride;
        }
    }

    return 0;
}
