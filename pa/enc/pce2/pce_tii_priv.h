
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder algoritm internal interface declarations
//
//
//

/*
 *  Internal vendor specific (TII) interface header for PCE
 *  algorithm. Only the implementation source files include
 *  this header; this header is not shipped as part of the
 *  algorithm.
 *
 *  This header contains declarations that are specific to
 *  this implementation and which do not need to be exposed
 *  in order for an application to use the PCM Encoder algoritm.
 */
#ifndef PCE_TII_PRIV_
#define PCE_TII_PRIV_

#include <ialg.h>
#include <ipce.h>
#include <log.h>

#include "paftyp.h"
#include "com_tii_priv.h"
#include "pafhjt.h"

#ifdef PCE_ALIGN_128
#undef DAT_copy
#define DAT_copy     DAT_cacheop_and_copy
#endif

typedef struct PCE_TII_Obj {
    IALG_Obj alg;            /* MUST be first field of all XDAS algs */
    int mask;
    PCE_TII_Status *pStatus; /* public interface (not shared) */
    PCE_TII_Config *pConfig; /* private interface (not shared) */
    PCE_TII_Active *pActive; /* private interface (stream-shared) */
    PCE_TII_Scrach *pScrach; /* private interface */
} PCE_TII_Obj;

#define PCE_TII_activate COM_TII_activate

#define PCE_TII_deactivate COM_TII_deactivate

extern Int PCE_TII_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);

#define PCE_TII_free COM_TII_free

extern Int PCE_TII_control(IALG_Handle, IALG_Cmd, IALG_Status *);

extern Int PCE_TII_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
                
#define PCE_TII_moved COM_TII_moved

extern Int PCE_TII_numAlloc();
                
extern Int PCE_TII_encode(IPCE_Handle, ALG_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *);

extern Int PCE_TII_info(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);

extern Int PCE_TII_reset(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);

/* Phase Init function declarations */
extern Int PCE_TII_volumeInit(IPCE_Handle, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *);

extern Int PCE_TII_delayInit(IPCE_Handle, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *);

/* Phase Reset function declarations */
extern Int PCE_TII_volumeReset(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *);

extern Int PCE_TII_delayReset(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *);

/* Phase Info function declarations */
extern Int PCE_TII_volumeInfo(IPCE_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, PAF_ActivePhase *, PAF_ScrachPhase *);

extern Int PCE_TII_outputInfo(IPCE_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, PAF_ActivePhase *, PAF_ScrachPhase *);

/* Phase function declarations */
extern Int PCE_TII_volumePhase(IPCE_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, PAF_ChannelMask_HD);

extern Int PCE_TII_delayPhase(IPCE_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, PAF_ChannelMask_HD);

extern Int PCE_TII_outputPhase(IPCE_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, PAF_ChannelMask_HD);

extern Int PCE_TII_outputCheck(IPCE_Handle, PAF_ActivePhase *, int, int);

extern Int PCE_TII_outputAudioSmInt(IPCE_Handle, PAF_ActivePhase *, SmInt *, SmInt, int);
extern Int PCE_TII_outputAudioMdInt(IPCE_Handle, PAF_ActivePhase *, MdInt *, MdInt, int);
extern Int PCE_TII_outputAudioLgInt(IPCE_Handle, PAF_ActivePhase *, LgInt *, LgInt, int);
extern Int PCE_TII_outputAudioFloat(IPCE_Handle, PAF_ActivePhase *, float *, float, int);
extern Int PCE_TII_outputAudioDouble(IPCE_Handle, PAF_ActivePhase *, double *, double, int);

extern Int PCE_TII_outputCopy(IPCE_Handle, PAF_ActivePhase *, float *, int);
extern Int PCE_TII_outputFinal(IPCE_Handle handle, PAF_ActivePhase *,int count, int strideOverride);

extern Int PCE_TII_delay(PAF_AudioFrame *, PAF_DelayState *, Int, Int, Int, PAF_AudioData, Int);
extern Int PCE_TII_delayDAT(PAF_AudioFrame *, PAF_DelayState *, SmInt *, Int, Int, Int, PAF_AudioData, Int);

extern Int PCE_TII_outputMdInsert(IPCE_Handle handle, PAF_ActivePhase *, PAF_AudioFrame *, PAF_ChannelMask_HD);

Int inline PCE_TII_minimumSampleGeneration(PCE_TII_Obj *pce)
{
    return 1;
}

Int inline PCE_TII_maximumSampleGeneration(PCE_TII_Obj *pce)
{
    return ((Int)pce->pConfig->frameLength);
}

#endif  /* PCE_TII_PRIV_ */













