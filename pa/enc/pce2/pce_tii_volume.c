
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) PCM Encoder algoritm functionality implementation
//
//   Part 1: Volume encode phase
//
//
//

#include <std.h>

#include <ipce.h>
#include <pce_tii.h>
#include <pce_tii_priv.h>
#include <pceerr.h>

#include "paftyp.h"
#include "stdasp.h"

// -----------------------------------------------------------------------------
// #define ENABLE_TRACE

#ifdef ENABLE_TRACE
 #include <logp.h>
 #define TRACE(a) LOG_printf a
 #define LINE_END "\n"
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------

/*
 *  ======== satvol ========
 *  Local implementation of volume saturation operation.
 */

inline Int 
satvol (Int x)
{
    return x < -0x8000 ? -0x8000 : x > 0x7fff ? 0x7fff : x;
}

/*
 *  ======== PCE_TII_volumePhase ========
 *  TII's implementation of the volume operation.
 */

Int 
PCE_TII_volumePhase(IPCE_Handle handle,
                    PAF_EncodeInStruct *pEncodeInStruct,
                    PAF_EncodeOutStruct *pEncodeOutStruct,
                    IPCE_StatusPhase *pStatusPhase,
                    IPCE_ConfigPhase *pConfigPhase,
                    PAF_IALG_Common *pConfigCommon,
                    PAF_ActivePhase *pActivePhase,
                    PAF_ScrachPhase *pScrachPhase,
                    PAF_ChannelMask_HD streamMask)
{
    PCE_TII_Obj *pce = (Void *)handle;

    PAF_ActivePhaseVolume *pActivePhaseVolume = (PAF_ActivePhaseVolume *)pActivePhase;

    PAF_VolumeStatus *pVolumeStatus = pActivePhaseVolume->pVolumeStatus;

    PAF_AudioFrame *pAudioFrame = pEncodeInStruct->pAudioFrame;

    IPCE_ConfigPhaseVolume *pConfigVolume = (IPCE_ConfigPhaseVolume *)pConfigPhase;

    // Int chn = pAudioFrame->data.nChannels;

    Int i;

    const Int minTrimStatus = ~0 << (8 * sizeof (PAF_AudioSize) - 1);

    Int maxTrimStatus = minTrimStatus;

    Int limitTrimStatus;


    PAF_AudioSize trim[PAF_MAXNUMCHAN_AF];
    PAF_AudioSize resizeInternal[PAF_MAXNUMCHAN_AF];
    PAF_AudioSize resizeExternal[PAF_MAXNUMCHAN_AF];
    // If requested volume ramp time is non-zero and volume ramp
    // is active (volume master control has changed and current
    // volume level needs to ramp up/down to it), update volume
    // status accordingly:

    if (pEncodeOutStruct->bypassFlag)
    {
    	TRACE((&trace, "%s.%d:  Bypass." LINE_END, __FUNCTION__, __LINE__));
    	return 0;
    }
    if (pConfigVolume->volumeRamp != pVolumeStatus->master.control &&
        pVolumeStatus->rampTime > 0) {

        Int rate = pAudioFrame->sampleRate;
        Int cnt = pAudioFrame->sampleCount;

        float frameDbQ1;
        float sampleTime;

        // Calculate audio frame duration in msec
        //   = (sample count / 8) * (8 samples / (x samples/sec)),
        //
        // then convert to required change in 0.5 dB steps (volume is Q1),
        // using given ramp rate, for duration of current audio frame
        //   = 2.0 * frame duration (msec) / ramp rate (msec/dB):
        sampleTime = 1.0f/pAudioFrame->fxns->sampleRateHz(pAudioFrame, rate, PAF_SAMPLERATEHZ_STD);
        frameDbQ1 = ((float )(cnt>>3) * sampleTime * 8000.0f * 2.0f) /
                    (float ) pVolumeStatus->rampTime;

        if (pConfigVolume->volumeRamp > (float ) pVolumeStatus->master.control) {
            // ramp volume "down," limiting to avoid "overshoot":
            pConfigVolume->volumeRamp =
                pConfigVolume->volumeRamp - (float ) pVolumeStatus->master.control >= frameDbQ1
                ? pConfigVolume->volumeRamp -= frameDbQ1
                : pVolumeStatus->master.control;
        }
        else {
            // ramp volume "up," limiting to avoid "overshoot":
            pConfigVolume->volumeRamp =
                (float ) pVolumeStatus->master.control - pConfigVolume->volumeRamp >= frameDbQ1
                ? pConfigVolume->volumeRamp += frameDbQ1
                : pVolumeStatus->master.control;
        }

        // Note: Update frequency using internal volume implementation
        //       is effectively occurring every 0.5 dB change, limited
        //       by updates being performed on audio frame boundaries
        //       only. I.e. more than one 0.5 dB change per sampleCount
        //       is summed and performed as one change per audio frame.
        //       Float volumeRamp is converted to (Q1) Int in usage below.
        //
        //       Volume ramp update frequency could be limited to once
        //       per n dB while using external volume implementation
        //       to lessen transmission load. (?) Alternate method is
        //       to perform volume ramp in an ASP, but scaling of
        //       each audio data sample is required (for fixed point
        //       audio data, but not floating point?) with cost of
        //       additional MIPS.  --Tom
    }
    else    // ramp inactive -- use volume master control level:
        pConfigVolume->volumeRamp = pVolumeStatus->master.control;

    // Compute volume status as per type & mode:

    if (pVolumeStatus->implementation & 0x01) {

        // Compute internal channel volumes without misalignment.

        for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
            if (streamMask & (1 << i)) {
                trim[i] = satvol ((Int )pConfigVolume->volumeRamp + pVolumeStatus->trim[i].control + pAudioFrame->data.samsiz[i]);
                if (maxTrimStatus < trim[i] + pVolumeStatus->trim[i].offset)
                    maxTrimStatus = trim[i] + pVolumeStatus->trim[i].offset;
            }
            else {
                trim[i] = minTrimStatus;
            }
        }
        limitTrimStatus = maxTrimStatus - pVolumeStatus->master.offset;
        if (limitTrimStatus > 0) {
            for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
                if (streamMask & (1 << i)) {
                    trim[i] = satvol ((Int )trim[i] - limitTrimStatus);
                }
            }
        }
        else
            limitTrimStatus = 0;

    }
    else {

        // Set internal channel volumes directly.

        for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
            if (streamMask & (1 << i))   
            trim[i] = satvol ((Int )pConfigVolume->volumeRamp + pVolumeStatus->trim[i].control);
            else
                trim[i] = minTrimStatus;
        }
        limitTrimStatus = 0;
    }

    // Set resize parameters as per type & mode:

        for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
            if (streamMask & (1 << i)) { 
#if PAF_AUDIODATATYPE_FIXED
        resizeInternal[i] = 0;
#else /* PAF_AUDIODATATYPE_FIXED */
        resizeInternal[i] = satvol (- (Int )pAudioFrame->data.samsiz[i]);
#endif /* PAF_AUDIODATATYPE_FIXED */
        resizeExternal[i] = 0;
        if (pVolumeStatus->implementation & 0x02)
            resizeInternal[i] = satvol ((Int )resizeInternal[i] + trim[i]);
        else
            resizeExternal[i] = satvol ((Int )resizeExternal[i] + trim[i]);
        if (pVolumeStatus->implementation & 0x04) {
            resizeInternal[i] = satvol ((Int )resizeInternal[i] + pVolumeStatus->trim[i].offset);
            resizeExternal[i] = satvol ((Int )resizeExternal[i] - pVolumeStatus->trim[i].offset);
        }
    }
    }

    // Set scales as per type & mode:

#if ! PAF_AUDIODATATYPE_FIXED
        for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
        if (streamMask & (1 << i)) {
            if ((pVolumeStatus->implementation & 0x08)
                && resizeInternal[i] != 0)
                pce->pConfig->scale[i] = 
                    pAudioFrame->fxns->dB2ToLinear (resizeInternal[i]);
            else
                pce->pConfig->scale[i] = 1.;
        }
        else
            pce->pConfig->scale[i] = 0.;
    }
#else /* ! PAF_AUDIODATATYPE_FIXED */
#error fixed-point audio data type not supported by this implementation
#endif /* ! PAF_AUDIODATATYPE_FIXED */

    // Set volume status registers for resizing as per type & mode:

    pVolumeStatus->master.instat 
        = satvol ((Int )pConfigVolume->volumeRamp - limitTrimStatus);
    for (i=PAF_LEFT; i < PAF_MAXNUMCHAN_AF; i++){
        if (streamMask & (1 << i)) {
            pVolumeStatus->trim[i].instat = resizeInternal[i];
            pVolumeStatus->trim[i].exstat = resizeExternal[i];
        }
        else {
            pVolumeStatus->trim[i].instat = minTrimStatus;
            pVolumeStatus->trim[i].exstat = minTrimStatus;
        }
    }

    // Return success.

    return 0;
}

/*
 *  ======== PCE_TII_volumeInit ========
 *  TII's implementation of the volume init operation.
 */

Int
PCE_TII_volumeInit(IPCE_Handle handle, IPCE_StatusPhase *pStatusPhase, IPCE_ConfigPhase *pConfigPhase, PAF_IALG_Common *pConfigCommon, PAF_ActivePhase *pActivePhase, PAF_ScrachPhase *pScrachPhase, IPCE_ConfigPhase *paramsConfigPhase, PAF_IALG_Common *paramsConfigCommon, PAF_ActivePhase *paramsActivePhase, PAF_ScrachPhase *paramsScrachPhase)
{
    IPCE_ConfigPhaseVolume *pConfigVolume = (IPCE_ConfigPhaseVolume *)pConfigPhase;

    IPCE_ConfigPhaseVolume *paramsConfigVolume = (IPCE_ConfigPhaseVolume *)paramsConfigPhase;

    if(pConfigVolume && paramsConfigVolume) {
        pConfigVolume->size = paramsConfigVolume->size;
        pConfigVolume->volumeRamp = paramsConfigVolume->volumeRamp;
    }

    if(pConfigCommon && paramsConfigCommon) {
        pConfigCommon->size = paramsConfigCommon->size;
        pConfigCommon->flag = 0;
    }

    return 0; 
} 

/*
 *  ======== PCE_TII_volumeReset ========
 *  TII's implementation of the delay reset operation.
 */

Int 
PCE_TII_volumeReset(
    IPCE_Handle handle, 
    ALG_Handle sioHandle, 
    PAF_EncodeControl *pEncodeControl,
    PAF_EncodeStatus *pEncodeStatus,
    IPCE_StatusPhase *pStatusPhase,
    IPCE_ConfigPhase *pConfigPhase, 
    PAF_IALG_Common *pConfigCommon, 
    PAF_ActivePhase *pActivePhase,
    PAF_ScrachPhase *pScrachPhase)
{
    IPCE_ConfigPhaseVolume *pConfigVolume = (IPCE_ConfigPhaseVolume *)pConfigPhase;

    PAF_VolumeStatus *pVolumeStatus = pEncodeControl->pVolumeStatus;

    // Set volume ramp level to 20 dB less than master volume level or
    // -40 dB, whichever is lower:
    PAF_AudioSize initLevel = pVolumeStatus->master.control - (2*20);
    pConfigVolume->volumeRamp = (float ) (initLevel > -(2*40) ? -(2*40) : initLevel);

    return 0;
}

/*
 *  ======== PCE_TII_volumeInfo ========
 *  TII's implementation of the outputInfo operation.
 */

Int
PCE_TII_volumeInfo(IPCE_Handle handle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus, PAF_ActivePhase *pActivePhase, PAF_ScrachPhase *pScrachPhase)
{
    PAF_ActivePhaseVolume *pActivePhaseVolume = (PAF_ActivePhaseVolume *)pActivePhase;

    pActivePhaseVolume->pVolumeStatus = pEncodeControl->pVolumeStatus;

    return 0;
}

