
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Output Buffer alpha codes
//
//
//

#ifndef _OUTBUF_A
#define _OUTBUF_A

#include <paftyp_a.h>
#include <acpbeta.h>

#define  readOBMode       0xc200+STD_BETA_OB,0x0400
#define writeOBModeEnable 0xca00+STD_BETA_OB,0x0401
#define writeOBModeIEC    0xca00+STD_BETA_OB,0x0402

#define  readOBSioSelect 0xc200+STD_BETA_OB, 0x0500
#define  rb32OBSioSelect 0xc000+STD_BETA_OB, 0x0581
#define writeOBSioSelectN(NN) 0xca00+STD_BETA_OB,(0x0500+(NN))

#define  readOBSampleRate 0xc200+STD_BETA_OB,0x0600
#define wroteOBSampleRateUnknown 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_UNKNOWN
#define wroteOBSampleRateNone 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_NONE
#define wroteOBSampleRate8000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_8000HZ
#define wroteOBSampleRate11025Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_11025HZ
#define wroteOBSampleRate12000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_12000HZ
#define wroteOBSampleRate16000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_16000HZ
#define wroteOBSampleRate22050Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_22050HZ
#define wroteOBSampleRate24000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_24000HZ
#define wroteOBSampleRate32000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_32000HZ
#define wroteOBSampleRate44100Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_44100HZ
#define wroteOBSampleRate48000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_48000HZ
#define wroteOBSampleRate64000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_64000HZ
#define wroteOBSampleRate88200Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_88200HZ
#define wroteOBSampleRate96000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_96000HZ
#define wroteOBSampleRate128000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_128000HZ
#define wroteOBSampleRate176400Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_176400HZ
#define wroteOBSampleRate192000Hz 0xca00+STD_BETA_OB,0x0600+PAF_SAMPLERATE_192000HZ

#define  readOBAudio 0xc200+STD_BETA_OB,0x0700
#define wroteOBAudioQuiet 0xca00+STD_BETA_OB,0x0700
#define wroteOBAudioSound 0xca00+STD_BETA_OB,0x0701
#define wroteOBAudioFlush 0xca00+STD_BETA_OB,0x0702
#define wroteOBAudioQuietMuted 0xca00+STD_BETA_OB,0x0710
#define wroteOBAudioSoundMuted 0xca00+STD_BETA_OB,0x0711
#define wroteOBAudioFlushMuted 0xca00+STD_BETA_OB,0x0712

#define  readOBClock 0xc200+STD_BETA_OB,0x0800
#define writeOBClockExternal 0xca00+STD_BETA_OB,0x0800
#define writeOBClockInternal 0xca00+STD_BETA_OB,0x0801

#define  readOBFlush 0xc200+STD_BETA_OB,0x0900
#define writeOBFlushDisable 0xca00+STD_BETA_OB,0x0900
#define writeOBFlushEnable 0xca00+STD_BETA_OB,0x0901

#define   readOBRateTrackMode          0xc200+STD_BETA_OB,0x0A00
#define  writeOBRateTrackDisable       0xca00+STD_BETA_OB,0x0A00
#define  writeOBRateTrackEnable        0xca00+STD_BETA_OB,0x0A01

#define   readOBMarkerInsertionMode        0xc200+STD_BETA_OB,0x0B00
#define  writeOBMarkerInsertionModeDisable 0xca00+STD_BETA_OB,0x0B00
#define  writeOBMarkerInsertionModeEnable  0xca00+STD_BETA_OB,0x0B01

#define   readOBMaxNumBufOverride         0xc200+STD_BETA_OB,0x0C00
#define writeOBMaxNumBufOverrideN(NN)     0xca00+STD_BETA_OB,(0x0C00 + (NN))
#define writeOBMaxNumBufOverrideNone      0xca00+STD_BETA_OB,0x0C00

#define  readOBUnknownNumBufOverride       0xc200+STD_BETA_OB,0x0D00
#define writeOBUnknownNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x0D00 + (NN))
#define writeOBUnknownNumBufOverrideNone   0xca00+STD_BETA_OB,0x0D00

#define  readOBNoneNumBufOverride       0xc200+STD_BETA_OB,0x0E00
#define writeOBNoneNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x0E00 + (NN))
#define writeOBNoneNumBufOverrideNone   0xca00+STD_BETA_OB,0x0E00

#define  readOBPassNumBufOverride       0xc200+STD_BETA_OB,0x0F00
#define writeOBPassNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x0F00 + (NN))
#define writeOBPassNumBufOverrideNone   0xca00+STD_BETA_OB,0x0F00

#define  readOBSngNumBufOverride       0xc200+STD_BETA_OB,0x1000
#define writeOBSngNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1000 + (NN))
#define writeOBSngNumBufOverrideNone   0xca00+STD_BETA_OB,0x1000

#define  readOBAutoNumBufOverride       0xc200+STD_BETA_OB,0x1100
#define writeOBAutoNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1100 + (NN))
#define writeOBAutoNumBufOverrideNone   0xca00+STD_BETA_OB,0x1100

#define  readOBBitstreamNumBufOverride       0xc200+STD_BETA_OB,0x1200
#define writeOBBitstreamNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1200 + (NN))
#define writeOBBitstreamNumBufOverrideNone   0xca00+STD_BETA_OB,0x1200

#define  readOBDtsallNumBufOverride       0xc200+STD_BETA_OB,0x1300
#define writeOBDtsallNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1300 + (NN))
#define writeOBDtsallNumBufOverrideNone   0xca00+STD_BETA_OB,0x1300

#define  readOBPcmautoNumBufOverride       0xc200+STD_BETA_OB,0x1400
#define writeOBPcmautoNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1400 + (NN))
#define writeOBPcmautoNumBufOverrideNone   0xca00+STD_BETA_OB,0x1400

#define  readOBPcmNumBufOverride       0xc200+STD_BETA_OB,0x1500
#define writeOBPcmNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1500 + (NN))
#define writeOBPcmNumBufOverrideNone   0xca00+STD_BETA_OB,0x1500

#define  readOBPc8NumBufOverride       0xc200+STD_BETA_OB,0x1600
#define writeOBPc8NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1600 + (NN))
#define writeOBPc8NumBufOverrideNone   0xca00+STD_BETA_OB,0x1600

#define  readOBAc3NumBufOverride       0xc200+STD_BETA_OB,0x1700
#define writeOBAc3NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1700 + (NN))
#define writeOBAc3NumBufOverrideNone   0xca00+STD_BETA_OB,0x1700

#define  readOBDtsNumBufOverride       0xc200+STD_BETA_OB,0x1800
#define writeOBDtsNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1800 + (NN))
#define writeOBDtsNumBufOverrideNone   0xca00+STD_BETA_OB,0x1800

#define  readOBAacNumBufOverride       0xc200+STD_BETA_OB,0x1900
#define writeOBAacNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1900 + (NN))
#define writeOBAacNumBufOverrideNone   0xca00+STD_BETA_OB,0x1900

#define  readOBMpegNumBufOverride       0xc200+STD_BETA_OB,0x1A00
#define writeOBMpegNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1A00 + (NN))
#define writeOBMpegNumBufOverrideNone   0xca00+STD_BETA_OB,0x1A00

#define  readOBDts12NumBufOverride       0xc200+STD_BETA_OB,0x1B00
#define writeOBDts12NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1B00 + (NN))
#define writeOBDts12NumBufOverrideNone   0xca00+STD_BETA_OB,0x1B00

#define  readOBDts13NumBufOverride       0xc200+STD_BETA_OB,0x1C00
#define writeOBDts13NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1C00 + (NN))
#define writeOBDts13NumBufOverrideNone   0xca00+STD_BETA_OB,0x1C00

#define  readOBDts14NumBufOverride       0xc200+STD_BETA_OB,0x1D00
#define writeOBDts14NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1D00 + (NN))
#define writeOBDts14NumBufOverrideNone   0xca00+STD_BETA_OB,0x1D00

#define  readOBDts16NumBufOverride       0xc200+STD_BETA_OB,0x1E00
#define writeOBDts16NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1E00 + (NN))
#define writeOBDts16NumBufOverrideNone   0xca00+STD_BETA_OB,0x1E00

#define  readOBWma9proNumBufOverride       0xc200+STD_BETA_OB,0x1F00
#define writeOBWma9proNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x1F00 + (NN))
#define writeOBWma9proNumBufOverrideNone   0xca00+STD_BETA_OB,0x1F00

#define  readOBMp3NumBufOverride       0xc200+STD_BETA_OB,0x2000
#define writeOBMp3NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2000 + (NN))
#define writeOBMp3NumBufOverrideNone   0xca00+STD_BETA_OB,0x2000

#define  readOBDsd1NumBufOverride       0xc200+STD_BETA_OB,0x2100
#define writeOBDsd1NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2100 + (NN))
#define writeOBDsd1NumBufOverrideNone   0xca00+STD_BETA_OB,0x2100

#define  readOBDsd2NumBufOverride       0xc200+STD_BETA_OB,0x2200
#define writeOBDsd2NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2200 + (NN))
#define writeOBDsd2NumBufOverrideNone   0xca00+STD_BETA_OB,0x2200

#define  readOBDsd3NumBufOverride       0xc200+STD_BETA_OB,0x2300
#define writeOBDsd3NumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2300 + (NN))
#define writeOBDsd3NumBufOverrideNone   0xca00+STD_BETA_OB,0x2300

#define  readOBDdpNumBufOverride       0xc200+STD_BETA_OB,0x2400
#define writeOBDdpNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2400 + (NN))
#define writeOBDdpNumBufOverrideNone   0xca00+STD_BETA_OB,0x2400

#define  readOBDtshdNumBufOverride       0xc200+STD_BETA_OB,0x2500
#define writeOBDtshdNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2500 + (NN))
#define writeOBDtshdNumBufOverrideNone   0xca00+STD_BETA_OB,0x2500

#define  readOBThdNumBufOverride       0xc200+STD_BETA_OB,0x2600
#define writeOBThdNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2600 + (NN))
#define writeOBThdNumBufOverrideNone   0xca00+STD_BETA_OB,0x2600

#define  readOBDxpNumBufOverride       0xc200+STD_BETA_OB,0x2700
#define writeOBDxpNumBufOverrideN(NN)  0xca00+STD_BETA_OB,(0x2700 + (NN))
#define writeOBDxpNumBufOverrideNone   0xca00+STD_BETA_OB,0x2700

#define  readOBStatus 0xc508,STD_BETA_OB
#define  readOBControl \
         readOBMode, \
         readOBSioSelect, \
         readOBClock, \
         readOBFlush

#ifdef HSE
// DFI
#define  readDFOFileSelect             0xc300+STD_BETA_OB,0x000C
#define  writeDFOFileSelectN(NN)       0xcb00+STD_BETA_OB,0x000C,NN
#define  readDFOMediaSelect            0xc300+STD_BETA_OB,0x000E
#define  writeDFOMediaSelectN(NN)      0xcb00+STD_BETA_OB,0x000E,NN
#endif //HSE

/***********************************************************************
 * MID 1102
 * Alpha codes to read/write DITCSR registers.
 * These 'alpha' codes don't write-to or read-from the OB status structure. 
 * So, OB's STD_BETA is not used! 
 * They directly write-to or read-from the DITCSR registers.
 *
 * The following addresses will be directly referred in the alpha codes below.
 *
 * #define DITCSRA0	0x46000100
 * #define DITCSRA1	0x46000104
 * #define DITCSRA2	0x46000108
 * #define DITCSRA3	0x4600010c
 * #define DITCSRA4	0x46000110
 * #define DITCSRA5	0x46000114
 *
 * #define DITCSRB0	0x46000118
 * #define DITCSRB1	0x4600011c
 * #define DITCSRB2	0x46000120
 * #define DITCSRB3	0x46000124
 * #define DITCSRB4	0x46000128
 * #define DITCSRB5	0x4600012c
*/

#define readOBDitCsrA0		0xf704,0x0004,0x0100,0x4600
#define writeOBDitCsrA0(N)	0xff04,0x0004,0x0100,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrA1		0xf704,0x0004,0x0104,0x4600
#define writeOBDitCsrA1(N)	0xff04,0x0004,0x0104,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrA2		0xf704,0x0004,0x0108,0x4600
#define writeOBDitCsrA2(N)	0xff04,0x0004,0x0108,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrA3		0xf704,0x0004,0x010c,0x4600
#define writeOBDitCsrA3(N)	0xff04,0x0004,0x010c,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrA4		0xf704,0x0004,0x0110,0x4600
#define writeOBDitCsrA4(N)	0xff04,0x0004,0x0110,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrA5		0xf704,0x0004,0x0114,0x4600
#define writeOBDitCsrA5(N)	0xff04,0x0004,0x0114,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB0		0xf704,0x0004,0x0118,0x4600
#define writeOBDitCsrB0(N)	0xff04,0x0004,0x0118,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB1		0xf704,0x0004,0x011c,0x4600
#define writeOBDitCsrB1(N)	0xff04,0x0004,0x011c,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB2		0xf704,0x0004,0x0120,0x4600
#define writeOBDitCsrB2(N)	0xff04,0x0004,0x0120,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB3		0xf704,0x0004,0x0124,0x4600
#define writeOBDitCsrB3(N)	0xff04,0x0004,0x0124,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB4		0xf704,0x0004,0x0128,0x4600
#define writeOBDitCsrB4(N)	0xff04,0x0004,0x0128,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)
#define readOBDitCsrB5		0xf704,0x0004,0x012c,0x4600
#define writeOBDitCsrB5(N)	0xff04,0x0004,0x012c,0x4600,((N)&0xFFFF),(((N)>>16)&0xFFFF)

#endif /* _OUTBUF_A */
