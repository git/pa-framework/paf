
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Input Buffer Declarations
//
//
//

#ifndef INPBUF_
#define INPBUF_

#include  <std.h>
#include <xdas.h>
#include <paftyp.h>

typedef volatile struct PAF_InpBufStatus {
// common entries (i.e. DIB)
    Int         size;
    XDAS_Int8   mode;
    XDAS_Int8   sioSelect;
    XDAS_Int8   lockOverride;
    XDAS_Int8   reserved1[1];
    XDAS_Int8   sampleRateOverride;
    XDAS_Int8   sampleRateData;
    XDAS_Int8   sampleRateMeasured;
    XDAS_Int8   sampleRateStatus;
    XDAS_Int8   nonaudio;
    XDAS_Int8   emphasisData;
    XDAS_Int8   emphasisOverride;
    XDAS_Int8   emphasisStatus;
    XDAS_Int8   lock;
    XDAS_Int8   scanAtHighSampleRateMode;
    XDAS_Int8   zeroRun;
    XDAS_Int8   rateTrackMode;
    XDAS_Int8   precisionDefault;
    XDAS_Int8   precisionDetect;
    XDAS_Int8   precisionOverride;
    XDAS_Int8   precisionInput;
    XDAS_UInt32 zeroRunTrigger;
    XDAS_UInt32 zeroRunRestart;
    XDAS_UInt32 unknownTimeout;

#ifdef HSE
// DFI entries 
    XDAS_Int16  fileSelect;
    XDAS_Int16  mediaSelect;
    XDAS_UInt32 fileSize;
    XDAS_UInt32 filePosition;
    XDAS_UInt16 playRate1;  // in kilo-bytes per sec
    XDAS_UInt16 playRate2;  // in kilo-bytes per sec
    void*       algDec;     // current decoder handle
#endif //HSE

    XDAS_UInt32 lastFrameMask;
    XDAS_UInt8  lastFrameFlag;
    XDAS_UInt8  reportDTS16AsDTSForLargeSampleRate;
    XDAS_UInt8  useIECSubType;
    XDAS_UInt8  reserved2[1];
#ifdef HSE
// DFI entries 
    XDAS_Int16  dirSelect;
#endif //HSE
} PAF_InpBufStatus;

// Depending on runtime usage the amount of memory allocated
// for the buffer will be restricted to a smaller amount (sizeofBuffer)
typedef struct PAF_InpBufConfig {
    PAF_UnionPointer base;				// base address of input buffer
    PAF_UnionPointer pntr;				// beginning address of valid data in buffer
    PAF_UnionPointer head;				// end address of valid data in buffer
    PAF_UnionPointer futureHead;		// end address of valid data after next DMA completes
    XDAS_Int32 sizeofBuffer;			// amount of buffer memory available
    XDAS_Int8 sizeofElement;			// number of bytes per input data word
    XDAS_Int8 precision;				// number of valid bits per input data word
    XDAS_Int8 stride;					// number of input data words per sample
    XDAS_Int8 deliverZeros;				// used to inform PCM decoder when to ignore data
    XDAS_Int32 frameLength;				// number of words in current frame
    XDAS_Int32 lengthofData;			// number of words needed to contain one frame
    PAF_InpBufStatus   *pBufStatus;		// pointer to status (see f/alpha/inpuf_a.h)
    XDAS_Int32 allocation;				// amount of buffer memory allocated
} PAF_InpBufConfig;

#endif  /* INPBUF_ */



