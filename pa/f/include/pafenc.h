
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Encoder Declarations
//
//
//

#ifndef PAFENC_
#define PAFENC_

#include <outbuf.h>
#include <paftyp.h>
#include <pafvol.h>

typedef struct PAF_EncodeControl {
    Int size;
    PAF_AudioFrame *pAudioFrame;
    PAF_VolumeStatus *pVolumeStatus;
    PAF_OutBufConfig *pOutBufConfig;
    XDAS_Int16 frameLength;
    XDAS_Int8 encActive;
    XDAS_Int8 unused;
} PAF_EncodeControl;

typedef volatile struct PAF_EncodeStatus {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 command2;
    XDAS_Int8 sampleRate;
    XDAS_Int8 channelCount;
    XDAS_Int8 Unused1[4]; // formerly channelConfigurationRequest. Can be reused in the future.
    XDAS_Int8 Unused2[4]; // formerly channelConfigurationStream. Can be reused in the future.
    XDAS_Int8 Unused3[4]; // formerly channelConfigurationEncode. Can be reused in the future.
    XDAS_Int8 Unused4[4]; // formerly programFormat. Can be reused in the future.
    XDAS_Int8 Unused5[4]; // formerly listeningFormat. Can be reused in the future.
    XDAS_Int16 frameLength;//offset 28
	XDAS_Int8 encBypass;
    XDAS_Int8 select;    
    PAF_ChannelConfiguration channelConfigurationRequest;  //offset 32 0x20
    PAF_ChannelConfiguration channelConfigurationStream;
    PAF_ChannelConfiguration channelConfigurationEncode;
    XDAS_Int8 Unused6[8];//can be used, added for allignment
    PAF_ChannelMap_HD channelMap; // not constant size  offset 64, 0x40
    //PAF_ChannelMap_HD;//This may be required for support of concurrently PAF_MAXMUMCHAN = 32 
    //To support channels like PAF_LHSI & PAF_RHSI where PAF_MAXMUMCHAN = 16 &  PAF_MAXMUMCHAN_AF = 32 ,this may not be required.
    //
    PAF_ProgramFormat programFormat;
    PAF_ProgramFormat listeningFormat;
    PAF_SampleProcess sampleProcess[PAF_SAMPLEPROCESS_N]; // not constant size

} PAF_EncodeStatus;

typedef struct PAF_EncodeInStruct {
    XDAS_Int8 outputFlag;
    XDAS_Int8 errorFlag;
    XDAS_Int16 unused;
    PAF_AudioFrame *pAudioFrame;
} PAF_EncodeInStruct;

typedef struct PAF_EncodeOutStruct {
    XDAS_Int8 outputFlag;
    XDAS_Int8 errorFlag;
    XDAS_Int16 sampleCount;
    XDAS_Int8 bypassFlag;
} PAF_EncodeOutStruct;

#endif  /* PAFENC_ */
