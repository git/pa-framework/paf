
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Sample Process Symbol Definitions
//
//
//

#ifndef PAFSP_
#define PAFSP_

typedef XDAS_Int32 PAF_SampleProcess;

// Define PAF_PROCESS_* linearly and PAF_PROCESS_N as the total bit count.

#define PAF_PROCESS_STD 0
#define PAF_PROCESS_PL (PAF_PROCESS_STD+0)
#define PAF_PROCESS_NEO (PAF_PROCESS_STD+1)   
/* Addded PAF_PROCESS_ASA to indicate ASA processing */
#define PAF_PROCESS_ASA (PAF_PROCESS_STD+2)
#define PAF_PROCESS_DEX (PAF_PROCESS_STD+3)
#define PAF_PROCESS_SURRBASS (PAF_PROCESS_STD+4)
#define PAF_PROCESS_BACKBASS (PAF_PROCESS_STD+5)
#define PAF_PROCESS_MONOBACK (PAF_PROCESS_STD+6)
#define PAF_PROCESS_SURRPROC (PAF_PROCESS_STD+7)

#define PAF_PROCESS_ALT 8
#define PAF_PROCESS_BACKPROC (PAF_PROCESS_ALT+0)
#define PAF_PROCESS_PL2X (PAF_PROCESS_ALT+1)
#define PAF_PROCESS_RVB (PAF_PROCESS_ALT+2)
#define PAF_PROCESS_MTX (PAF_PROCESS_ALT+3)
#define PAF_PROCESS_GEQ (PAF_PROCESS_ALT+4)
#define PAF_PROCESS_SRC (PAF_PROCESS_ALT+5)
#define PAF_PROCESS_DMX (PAF_PROCESS_ALT+6)
#define PAF_PROCESS_DEM (PAF_PROCESS_ALT+7)

#define PAF_PROCESS_OEM 16
 #define PAF_PROCESS_OAR     (PAF_PROCESS_OEM+0)   // Dolby ATMOS Object Audio Renderer
 #define PAF_PROCESS_CAR     (PAF_PROCESS_OEM+1)   // Dolby ATMOS Channel Audio Renderer
 #define PAF_PROCESS_DTSX    (PAF_PROCESS_OEM+2)   // DTS:X Object Audio Renderer
 #define PAF_PROCESS_NEUX    (PAF_PROCESS_OEM+3)   // DTS Neural:X Upmixer
 #define PAF_PROCESS_ARC     (PAF_PROCESS_OEM+4)   // Asynchronous sample rate converter
 #define PAF_PROCESS_BM      (PAF_PROCESS_OEM+5)   // Legacy Bass management
 #define PAF_PROCESS_BMDA    (PAF_PROCESS_OEM+6)   // Dolby ATMOS bass manager

#define PAF_PROCESS_CUS 24

#define PAF_PROCESS_N 32

#define PAF_SAMPLEPROCESS_N \
    ((PAF_PROCESS_N+8*sizeof(PAF_SampleProcess)-1)/(8*sizeof(PAF_SampleProcess)))

#define PAF_PROCESS_TST(SP,NN) \
    ((SP)[PAF_PROCESS_##NN/(8*sizeof(PAF_SampleProcess))] \
        & (1<<PAF_PROCESS_##NN%(8*sizeof(PAF_SampleProcess))))

#define PAF_PROCESS_SET(SP,NN) \
    ((SP)[PAF_PROCESS_##NN/(8*sizeof(PAF_SampleProcess))] \
        |= (1<<PAF_PROCESS_##NN%(8*sizeof(PAF_SampleProcess))))

#define PAF_PROCESS_CLR(SP,NN) \
    ((SP)[PAF_PROCESS_##NN/(8*sizeof(PAF_SampleProcess))] \
        &= ~(1<<PAF_PROCESS_##NN%(8*sizeof(PAF_SampleProcess))))

#define PAF_PROCESS_ZERO(SP) \
    do { \
        Int i; \
        for (i=0; i < PAF_SAMPLEPROCESS_N; i++) \
            (SP)[i] = 0; \
    } while (0)

#define PAF_PROCESS_COPY(TO,FR) \
    do { \
        Int i; \
        for (i=0; i < PAF_SAMPLEPROCESS_N; i++) \
            (TO)[i] = (FR)[i]; \
    } while (0)

#endif /* PAFSP_ */
