
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework System Declarations
//
//
//

#ifndef PAFSYS_
#define PAFSYS_

#include <paftyp.h>
#define NUM_TASK_ERROR_REPORT 5

typedef volatile struct PAF_SystemStatus {
    Int size;
    XDAS_UInt8 mode;
    XDAS_UInt8 listeningMode;
    XDAS_UInt8 recreationMode;
    XDAS_UInt8 speakerMain;
    XDAS_UInt8 speakerCntr;
    XDAS_UInt8 speakerSurr;
    XDAS_UInt8 speakerBack;
    XDAS_UInt8 speakerSubw;
    XDAS_UInt8 channelConfigurationRequestType;
    XDAS_UInt8 switchImage;
    XDAS_UInt8 imageNum;
    XDAS_UInt8 imageNumMax;
    XDAS_UInt32 reserved;		//can be used in future
    XDAS_UInt16 cpuLoad;
    XDAS_UInt16 peakCpuLoad;
    XDAS_UInt8 speakerWide;
    XDAS_UInt8 speakerHead;
    XDAS_UInt8 speakerTopfront;
    XDAS_UInt8 speakerToprear;
    XDAS_UInt8 speakerTopmiddle;
    XDAS_UInt8 speakerFrontheight;
    XDAS_UInt8 speakerRearheight;
    XDAS_UInt8 unused2;//To make 64 bit alligned
    PAF_ChannelConfiguration channelConfigurationRequest;
#ifdef FULL_SPEAKER
    XDAS_UInt8 speakerScreen;//0x28
    XDAS_UInt8 speakerSurr1;//0x29
    XDAS_UInt8 speakerSurr2;//0x2a
    XDAS_UInt8 speakerRearSurr1;//0x2b
    XDAS_UInt8 speakerRearSurr2;//0x2c
    XDAS_UInt8 speakerCntrSurr;//0x2d
    XDAS_UInt8 speakerLRCntr;//0x2e
    XDAS_UInt8 speakerLRCntrSurr;//0x2f
#endif
} PAF_SystemStatus;

typedef volatile struct PAF_ErrorStatus {
    Int size;
    XDAS_UInt8  mode;
    XDAS_UInt8  errRst[2*NUM_TASK_ERROR_REPORT];
    XDAS_UInt8  unused;
    XDAS_UInt32 prevErr[2*NUM_TASK_ERROR_REPORT];
    XDAS_UInt32 curErr[2*NUM_TASK_ERROR_REPORT];
} PAF_ErrorStatus;

#include <pafsys_a.h>

#endif  /* PAFSYS_ */
