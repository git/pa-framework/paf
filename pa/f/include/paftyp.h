
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework General Constant and Type Declarations
//
//
//
#ifndef FULL_SPEAKER
#define FULL_SPEAKER
#endif

#ifndef PAFTYP_
#define PAFTYP_

// Basic includes:

#include <std.h>
#include  <std.h>
#include <xdas.h>
#include "paftyp_a.h"
#include "pafcc.h"
#include "pafsp.h"

// Audio frame data types:

// PAF_AudioData is the type of data carried in the stream.

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE
typedef double PAF_AudioData;
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
typedef float PAF_AudioData;
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT32
typedef XDAS_Int32 PAF_AudioData;
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT16
typedef XDAS_Int16 PAF_AudioData;
#elif PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_INT8
typedef XDAS_Int8 PAF_AudioData;
#endif

// PAF_AudioSize is the size of the data carried in the stream,
// where "size" is defined variously depending upon the type:

typedef XDAS_Int16 PAF_AudioSize;

// PAF_AudioFrameData is a fixed structure which defines the possible
// data-carrying capacity of the audio frame.

typedef struct PAF_AudioFrameData {
    XDAS_Int16 nChannels; /* max M */
    XDAS_Int16 nSamples; /* max N */
    PAF_AudioData **sample; /* sample[M][N] */
    PAF_AudioSize *samsiz; /* samsiz[M] */
} PAF_AudioFrameData;

// ............................................................................

// PAF_ChannelMask indicates which channels in the audio frame data
// carry content (1) and which are to be ignored (0).

#if PAF_MAXNUMCHAN <= 8
typedef XDAS_Int8 PAF_ChannelMask;
#elif PAF_MAXNUMCHAN <= 16
typedef XDAS_Int16 PAF_ChannelMask;
#elif PAF_MAXNUMCHAN <= 32
typedef XDAS_Int32 PAF_ChannelMask;
#else
#error unsupported option
#endif

typedef struct PAF_ChannelMaskList {
    PAF_ChannelMask length;
    PAF_ChannelMask *pMask;
} PAF_ChannelMaskList;

typedef struct PAF_ChannelConfigurationMaskTable {
    PAF_ChannelMaskList sat;
    PAF_ChannelMaskList sub;
} PAF_ChannelConfigurationMaskTable;

typedef XDAS_Int32 PAF_ChannelMask_HD;

// ............................................................................

// PAF_ChannelForm indicates extra info about channels in the audio frame.

typedef XDAS_Int16 PAF_ChannelForm;

// PAF_ProgramFormat combines channel mask and format.

typedef struct PAF_ProgramFormat {
    PAF_ChannelMask mask;
    PAF_ChannelForm form;
} PAF_ProgramFormat;

// PAF_SampleRateHz represents the sample rate in Hz (float or fixed Q0):
//   Sample Rate Unknown:       48000
//   Sample Rate None:          0
//   Sample Rate Unrecognized:  -1

#if PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_DOUBLE \
    || PAF_AUDIODATATYPE == PAF_AUDIODATATYPE_FLOAT
#define PAF_SAMPLERATEHZ_FLOAT
typedef float PAF_SampleRateHz;
#else
#define PAF_SAMPLERATEHZ_INT32
typedef LgInt PAF_SampleRateHz; /* truncated */
#endif

// ............................................................................

// PAF_AudioFunctions are the methods defined for the audio frame.

struct PAF_AudioFrame;
struct PAF_DelayState;

typedef struct PAF_AudioFunctions {
    PAF_AudioData (*dB2ToLinear)(Int);
    PAF_ChannelMask_HD (*channelMask) // (MID 1933) formerly PAF_ChannelMask (*)
        (struct PAF_AudioFrame *, PAF_ChannelConfiguration);
    PAF_ProgramFormat (*programFormat)
        (struct PAF_AudioFrame *, PAF_ChannelConfiguration, Int);
    PAF_SampleRateHz (*sampleRateHz) (struct PAF_AudioFrame *, Int, Int);
    Int (*delay)(struct PAF_AudioFrame *, struct PAF_DelayState *, Int, Int, Int);
} PAF_AudioFunctions;


// Attaching metadata to a frame:
//  metadata can be PAF information
//      PAF_AudioFrame : XDAS_Int8 sampleRate
//      PAF_AudioFrame : channelConfigurationStream (number of channels can be derived from this)
//  metadata can be "private" to be exchanged between two components.
//  Particularly necessary for dual DSP systems.
//

// Components using metadata typically treat the data privately.
// A normal legacy component must  process a frame if and only if
//  the metadata has BsMetadata_channelData type.
// Other components must look for their private metadata type.
typedef enum 
{
    PAF_bsMetadata_none         = -1,
    PAF_bsMetadata_channelData  =  0, // normal, no metadata.
    PAF_bsMetadata_Evolution    =  1, // Dolby ATMOS (non DDP)
    PAF_bsMetadata_DdpEvolution =  2, // DDP ATMOS
    PAF_bsMetadata_reserved_lar,      // 
    PAF_bsMetadata_reserved_2,        // DTS
    PAF_bsMetadata_reserved_3,        // others to be assigned
    PAF_bsMetadata_reserved_4
} PAF_bsMetadataTypes;

#define PAF_MAX_NUM_PRIVATE_MD  ( 4 )   // spec says it could be 8, but we limit it to 4.
#define PAF_MAX_PRIVATE_MD_SZ   ( 4096 )

/* Contains de-serialized private metadata */
typedef struct PAF_PrivateMetadata
{
    XDAS_UInt16 offset;     /* offset of this private metadata into audio frame */
    XDAS_UInt16 size;       /* Byte count of the buffer pointed to by pMdBuf */
    XDAS_UInt8 *pMdBuf;     /* Pointer to output metadata buffer */
} PAF_PrivateMetadata;

// ............................................................................
// PAF_AudioFrame is the audio frame as a whole.
// ............................................................................
typedef struct PAF_AudioFrame {
    const PAF_AudioFunctions *fxns;
    XDAS_Int8 mode;
    XDAS_Int8 sampleDecode;
    XDAS_Int8 sampleRate;
    XDAS_Int8 unused[3];
    XDAS_Int16 sampleCount;  /* valid N */
    PAF_AudioFrameData data; /* data[M][N] */
    PAF_ChannelConfiguration channelConfigurationRequest;
    PAF_ChannelConfiguration channelConfigurationStream; /* valid M */
    PAF_ChannelConfigurationMaskTable *pChannelConfigurationMaskTable;
    PAF_SampleProcess sampleProcess[PAF_SAMPLEPROCESS_N];
    struct PAF_AudioFrame *root;

    PAF_bsMetadataTypes bsMetadata_type;        /* non zero if metadata is attached. */
    XDAS_Bool           pafBsMetadataUpdate;    /* indicates whether bit-stream metadata update */
    XDAS_UInt8          numPrivateMetadata;     /* number of valid private metadata (0 or 1 if metadata filtering enabled) */
    XDAS_UInt16         bsMetadata_offset;      /* offset into audio frame for change in bsMetadata_type field */
    XDAS_UInt16         privateMetadataBufSize; /* max byte count of the private metadata buffers */

    PAF_PrivateMetadata pafPrivateMetadata[PAF_MAX_NUM_PRIVATE_MD]; /* private metadata */

} PAF_AudioFrame;

// PAF_AudioBuffer is a fixed structure which defines the possible
// data-carrying capacity of an audio buffer.

typedef struct PAF_AudioBuffer {
    XDAS_Int32 nSamples; /* max N */
    PAF_AudioData *sample; /* sample[N] */
} PAF_AudioBuffer;

// PAF_DelayState is the state vector for the delay member function.

typedef struct PAF_DelayState {
    XDAS_UInt16 delay;
    XDAS_UInt16 count;
    XDAS_UInt16 length;
    XDAS_UInt16 unused;
    PAF_AudioData *base;
    PAF_AudioData *pntr;
} PAF_DelayState;

#define PAF_BS_AUDIO_MODE_SWITCH_FROM_FRAME_COUNT   ( 6 )    /* frame count for from switch in ch- <-> obj-based audio switching */
#define PAF_BS_AUDIO_MODE_SWITCH_TO_FRAME_COUNT     ( 5 )    /* frame count for to switch in ch- <-> obj-based audio switching */

// Switching states for ch-based <-> obj-based audio mode switch within a bit stream
typedef enum
{
    PAF_BsAudioModeSwitchState_Init         = -1,   // init mode
    PAF_BsAudioModeSwitchState_Run          =  0,   // running (normal) mode
    PAF_BsAudioModeSwitchState_SwitchFrom   =  1,   // switching from mode
    PAF_BsAudioModeSwitchState_SwitchTo     =  2    // switching to mode
} PAF_BsAudioModeSwitchState;

// ............................................................................

// PAF_ChannelMap provides a mapping from I/O buffers to/from the audio
// frame data buffers.
typedef struct PAF_ChannelMap {
    XDAS_Int8 from[PAF_MAXNUMCHAN];
    XDAS_Int8   to[PAF_MAXNUMCHAN];
} PAF_ChannelMap;

typedef struct PAF_ChannelMap_HD {
    XDAS_Int8 from[PAF_MAXNUMCHAN_HD];
    XDAS_Int8   to[PAF_MAXNUMCHAN_HD];
} PAF_ChannelMap_HD;

// ............................................................................

// Standard operations:

#ifdef round
#undef round
#endif
inline long
round (double d) 
{
    return d > 0. ? d + 0.5 : d - 0.5;
}

#ifndef lengthof
#define lengthof(X) (sizeof(X)/sizeof(*(X)))
#endif /* lengthof */

typedef union PAF_UnionPointer {
    Void       *pVoid;
    XDAS_Int8  *pSmInt;
    XDAS_Int16 *pMdInt;
    XDAS_Int32 *pLgInt;
    float      *pFloat;
    double     *pDouble;
} PAF_UnionPointer;

#endif  /* PAFTYP_ */
