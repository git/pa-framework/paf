
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Volume Declarations
//
//
//

#ifndef PAFVOL_
#define PAFVOL_

#include <paftyp.h>

typedef struct PAF_VolumeQuad {
    PAF_AudioSize control;
    PAF_AudioSize offset;
    PAF_AudioSize instat;
    PAF_AudioSize exstat;
} PAF_VolumeQuad;

typedef struct PAF_VolumeStatus {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 channelCount;
    XDAS_Int8 implementation;
    XDAS_Int8 unused1;
    XDAS_UInt16 rampTime;
    XDAS_Int16 unused2;
    XDAS_Int32 unused3;
    PAF_VolumeQuad master;
    PAF_VolumeQuad trim[PAF_MAXNUMCHAN_AF];
    //Max index, PAF_MAXNUMCHAN_AF (changed from PAF_MAXNUMCHAN ) is required for support 32 concurrent channels
    //ie. to support channels like, PAF_LHSI & PAF_RHSI(out of 16). This required to set scale" 
    //
} PAF_VolumeStatus;

#endif  /* PAFVOL_ */
