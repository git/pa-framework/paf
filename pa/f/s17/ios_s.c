
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha Code Shortcuts for Standard Input/Output Switching (IOS)
//
//
//

#include <acptype.h>
#include <pafstd_a.h>
#include <pafenc_a.h>

#define  rb32ENCCommand 0xc025,0x0581
#define  ob32ENCCommandMuted 0x0081,0x0000
#define  ob32ENCCommandUnmuted 0x0082,0x0000

#define writeIOSAwait(RB32,WB32) 0xcd0b,5+2,0x0204,200,1,WB32,RB32

#define IOS_MUTE_S \
    writeENCCommandMute, \
   writeIOSAwait (rb32ENCCommand, ob32ENCCommandMuted), \
    execSTDReady

#pragma DATA_SECTION(ios_mute_s0, ".none")
const ACP_Unit ios_mute_s0[] = {
    0xc900 + 0 - 1,
    IOS_MUTE_S,
};

const ACP_Unit ios_mute_s[] = {
    0xc900 + sizeof(ios_mute_s0)/2 - 1,
    IOS_MUTE_S,
};

#define IOS_UNMUTE_S \
    writeENCCommandUnmute, \
   writeIOSAwait (rb32ENCCommand, ob32ENCCommandUnmuted), \
    execSTDReady

#pragma DATA_SECTION(ios_unmute_s0, ".none")
const ACP_Unit ios_unmute_s0[] = {
    0xc900 + 0 - 1,
    IOS_UNMUTE_S,
};

const ACP_Unit ios_unmute_s[] = {
    0xc900 + sizeof(ios_unmute_s0)/2 - 1,
    IOS_UNMUTE_S,
};

