
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef _AFP6
#define _AFP6    1
#ifdef BIOS6_NONLEGACY
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/heaps/HeapMem.h>
#else
#include <std.h>
#include <tsk.h>
#include <log.h>
#endif

#include <acp.h>

#include <dmax.h>
#include <dmax_struct.h>

#include <dcs6.h>

#include <afp6_params.h>

/* AFP6 Function Table */
typedef struct AFP6_Obj *AFP6_Handle;
typedef struct AFP6_Fxns{
    void (*log)(AFP6_Handle,void*,char*);
    void* (*memAlloc)(AFP6_Handle,void*,Uint32,Uint32);
    void (*task)(AFP6_Handle);
    void (*process)(AFP6_Handle);    
    void (*acpInit)(void);
    void (*acpExit)(void);
}AFP6_Fxns;

/* AFP6 Object */
typedef struct AFP6_Obj {
    Uint32 size;
    const AFP6_Fxns *fxns;
    const AFP6_Params *params;
    DCS6_Handle dcs6;
    const DCS6_Fxns *dcs6Fxns;
    const DCS6_Config *dcs6Config;
    dMAX_Handle dmax;
    ACP_Handle acp;
    ACP_Params *acpParams;
    Uint16 *rxBuf;
    Uint16 *txBuf;
    Int *pBufSeg;
    Int *pObjSeg;
    void *logObj;
}AFP6_Obj;
#endif

