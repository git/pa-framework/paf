
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#include <afp_uart.h>

#include <acp_mds.h>
#include <acp_mds_priv.h>

#include <logp.h>
#define ENABLE_ALPHA_ERROR_TRACE
#ifdef ENABLE_ALPHA_ERROR_TRACE
 #define AE_TRACE(a) LOG_printf a
#else
 #define AE_TRACE(a)
#endif

void AFPUartLog(
    AFPUart_Handle handle,
    void *log,
    char *msg)
{
    if(log)
#ifdef BIOS6_NONLEGACY
        Log_print0((Uint32)log,msg);
#else
        LOG_printf(log,msg);
#endif
}

void* AFPUartMemAlloc(
    AFPUart_Handle handle,
    void *memSeg,
    Uint32 size,
    Uint32 align)
{
#ifdef BIOS6_NONLEGACY
    return (void*)Memory_alloc((IHeap_Handle)*(Int32*)memSeg,size,align,NULL);
#else
    return (void*)MEM_alloc(*(Int32*)memSeg,size,align);
#endif
}

void* AFPUartSemAlloc(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_create(0,NULL,NULL);
#else
    return SEM_create(0,NULL);
#endif
}

Uint32 AFPUartSemPend(
    AFPUart_Handle handle,
    void *sem)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_pend(sem,BIOS_WAIT_FOREVER);
#else
    return SEM_pend(sem,SYS_FOREVER);
#endif
}

void AFPUartSemPost(
    AFPUart_Handle handle,
    void *sem)
{
#ifdef BIOS6_NONLEGACY
    Semaphore_post(sem);
#else
    SEM_post(sem);
#endif
}

void AFPUartIntDisable(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    handle->intMask=Hwi_disable();
#else
    handle->intMask=HWI_disable();
#endif
}

void AFPUartIntRestore(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Hwi_restore(handle->intMask);
#else
    HWI_restore(handle->intMask);
#endif
}

void AFPUartTaskDisable(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_disable();
#else
    TSK_disable();
#endif
}

void AFPUartTaskEnable(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_enable();
#else
    TSK_enable();
#endif
}

void AFPUartTaskSleep(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_sleep(1);
#else
    TSK_sleep(1);
#endif
}

void AFPUartIsr(
    AFPUart_Handle handle)
{
    CSL_UartRegsOvly CSL_UART_REGS=handle->devHandle;
    Uint32 iir,intid,lsr;
    Uint32 cnt,rxBufSpace;
    Uint32 i;

    iir=CSL_UART_REGS->IIR;
    while(!(iir&CSL_UART_IIR_IPEND_MASK)){
        intid=(iir&CSL_UART_IIR_INTID_MASK)>>CSL_UART_IIR_INTID_SHIFT;
        if(intid==CSL_UART_IIR_INTID_RLS){
            lsr=CSL_UART_REGS->LSR;
            handle->rxBuf[handle->rxWIdx]=CSL_UART_REGS->RBR;
            if(lsr&CSL_UART_LSR_OE_MASK){
                handle->errcnt++;
                handle->errflg=lsr;
            }
        }
        else if(intid==CSL_UART_IIR_INTID_RDA||intid==CSL_UART_IIR_INTID_CTI){
            CSL_UART_REGS->THR=AFPUART_XOFF;
            cnt=handle->rxfiflen;
            lsr=CSL_UART_REGS->LSR;
            while(cnt&&lsr&CSL_UART_LSR_DR_MASK){
                handle->rxBuf[handle->rxWIdx]=CSL_UART_REGS->RBR;
                if(handle->rxBuf[handle->rxWIdx]==AFPUART_XOFF)
                    handle->hostOvrn=1;
                else if(handle->rxBuf[handle->rxWIdx]==AFPUART_XON)
                    handle->hostOvrn=0;
                else if(handle->rxBuf[handle->rxWIdx]=='\n'&&
                    handle->rxBuf[handle->rxWIdx?handle->rxWIdx-1:handle->rxSize-1]=='\r'){
                    handle->rxLineCnt++;
                    AFPUartSemPost(handle,handle->sem);
                }
                if(!(handle->rxBuf[handle->rxWIdx]==AFPUART_XOFF||
                    handle->rxBuf[handle->rxWIdx]==AFPUART_XON)){
                    handle->rxWIdx++;
                    if(handle->rxWIdx>=handle->rxSize)
                        handle->rxWIdx=0;
                }
                cnt--;
                lsr=CSL_UART_REGS->LSR;
            }
            rxBufSpace=handle->rxWIdx>handle->rxRIdx?
                handle->rxSize-(handle->rxWIdx-handle->rxRIdx):
                handle->rxRIdx-handle->rxWIdx-1;
            if(rxBufSpace>handle->rxfiflen<<1)
                CSL_UART_REGS->THR=AFPUART_XON;
            else
                handle->rxOvrn=1;
        }
        else if(intid==CSL_UART_IIR_INTID_THRE){
            if(!handle->txUdrn&&!handle->hostOvrn&&handle->txLineCnt){
                for(i=0;handle->txLineCnt&&i<handle->rxfiflen;++i){
                    CSL_UART_REGS->THR=handle->txBuf[handle->txRIdx];
                    if(handle->txBuf[handle->txRIdx]=='\n'&&
                        handle->txBuf[handle->txRIdx?handle->txRIdx-1:handle->txSize-1]=='\r')
                        handle->txLineCnt--;
                    handle->txRIdx++;
                    if(handle->txRIdx>=handle->txSize)
                        handle->txRIdx=0;
                }
            }
            else{
                    handle->txUdrn=1;
            }
        }
        iir=CSL_UART_REGS->IIR;
    }
}

void AFPUartConfigureInt(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Hwi_Params hwiParams;

    Hwi_Params_init(&hwiParams);
    hwiParams.arg=(xdc_UArg)handle;
    Hwi_create(handle->cpuInt,(Hwi_FuncPtr)AFPUartIsr,&hwiParams,NULL);
    Hwi_enableInterrupt(handle->cpuInt);
#else
    HWI_Attrs hwiAttrs;

    hwiAttrs=HWI_ATTRS;
    hwiAttrs.arg=(Arg)handle;
    HWI_dispatchPlug(handle->cpuInt,(Fxn)AFPUartIsr,-1,&hwiAttrs);
    C64_enableIER(1<<handle->cpuInt);
#endif
    ((Uint32*)&CSL_INTC_REGS->INTMUX1)[handle->cpuInt-4>>2]=
        ((Uint32*)&CSL_INTC_REGS->INTMUX1)[handle->cpuInt-4>>2]&
        ~(0xFF<<8*(handle->cpuInt&3))|
        (handle->dev==AFPUART_DEV_UART0?AFPUART_INT_UART0:
        handle->dev==AFPUART_DEV_UART1?AFPUART_INT_UART1:
        handle->dev==AFPUART_DEV_UART2?AFPUART_INT_UART2:0)<<
        8*(handle->cpuInt&3);
}

void AFPUartOpen(
    AFPUart_Handle handle)
{
    CSL_UartRegsOvly CSL_UART_REGS;

    AFPUartTaskDisable(handle);
    if(handle->dev==AFPUART_DEV_UART0){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0xFFF00FFF|0x00011000;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART0_DSP<<CSL_BOOTCFG_SUSPSRC_UART0_SHIFT;
    }
    else if(handle->dev==AFPUART_DEV_UART1){
        CSL_BOOTCFG_0_REGS->PINMUX11=
            CSL_BOOTCFG_0_REGS->PINMUX11&0xFFFF00FF|0x00001100;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART1_DSP<<CSL_BOOTCFG_SUSPSRC_UART1_SHIFT;
    }
    else if(handle->dev==AFPUART_DEV_UART2){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0x0FFFFFFF|0x20000000;
        CSL_BOOTCFG_0_REGS->PINMUX9=
            CSL_BOOTCFG_0_REGS->PINMUX9&0xFFFFFFF0|0x00000002;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART2_DSP<<CSL_BOOTCFG_SUSPSRC_UART2_SHIFT;
    }
    AFPUartTaskEnable(handle);

    if(handle->dev==AFPUART_DEV_UART0)
        CSL_UART_REGS=CSL_UART_0_REGS;
    else if(handle->dev==AFPUART_DEV_UART1)
        CSL_UART_REGS=CSL_UART_1_REGS;
    else if(handle->dev==AFPUART_DEV_UART2)
        CSL_UART_REGS=CSL_UART_2_REGS;
    handle->devHandle=(void*)CSL_UART_REGS;
    CSL_UART_REGS->PWREMU_MGMT=
        CSL_UART_PWREMU_MGMT_UTRST_RESET<<CSL_UART_PWREMU_MGMT_UTRST_SHIFT|
        CSL_UART_PWREMU_MGMT_URRST_RESET<<CSL_UART_PWREMU_MGMT_URRST_SHIFT;
    CSL_UART_REGS->DLL=handle->div&0xFF;
    CSL_UART_REGS->DLH=handle->div>>8&0xFF;
    CSL_UART_REGS->MDR=CSL_UART_MDR_RESETVAL;
    CSL_UART_REGS->FCR=
        CSL_UART_FCR_FIFOEN_ENABLE<<CSL_UART_FCR_FIFOEN_SHIFT;
    CSL_UART_REGS->FCR|=
        handle->rxfiftl<<CSL_UART_FCR_RXFIFTL_SHIFT|
        CSL_UART_FCR_DMAMODE1_DISABLE<<CSL_UART_FCR_DMAMODE1_SHIFT|
        CSL_UART_FCR_TXCLR_CLR<<CSL_UART_FCR_TXCLR_SHIFT|
        CSL_UART_FCR_RXCLR_CLR<<CSL_UART_FCR_RXCLR_SHIFT;
    CSL_UART_REGS->LCR=
        CSL_UART_LCR_DLAB_DISABLE<<CSL_UART_LCR_DLAB_SHIFT|
        CSL_UART_LCR_BC_DISABLE<<CSL_UART_LCR_BC_SHIFT|
        CSL_UART_LCR_PEN_DISABLE<<CSL_UART_LCR_PEN_SHIFT|
        CSL_UART_LCR_STB_1STOPBIT<<CSL_UART_LCR_STB_SHIFT|
        CSL_UART_LCR_WLS_BITS8<<CSL_UART_LCR_WLS_SHIFT;
    CSL_UART_REGS->MCR=CSL_UART_MCR_RESETVAL;
    CSL_UART_REGS->IER=
        CSL_UART_IER_ELSI_ENABLE<<CSL_UART_IER_ELSI_SHIFT|
        CSL_UART_IER_ETBEI_ENABLE<<CSL_UART_IER_ETBEI_SHIFT|
        CSL_UART_IER_ERBI_ENABLE<<CSL_UART_IER_ERBI_SHIFT;
    AFPUartConfigureInt(handle);
    CSL_UART_REGS->PWREMU_MGMT=
        CSL_UART_PWREMU_MGMT_UTRST_ENABLE<<CSL_UART_PWREMU_MGMT_UTRST_SHIFT|
        CSL_UART_PWREMU_MGMT_URRST_ENABLE<<CSL_UART_PWREMU_MGMT_URRST_SHIFT;
}

Uint32 AFPUartRead(
    AFPUart_Handle handle,
    Uint32 *len)
{
    CSL_UartRegsOvly CSL_UART_REGS=handle->devHandle;
    Uint32 rxRIdx,rxLineCnt,rxBufSpace;
    Uint32 i=0,j=0;

    AFPUartSemPend(handle,handle->sem);
    AFPUartIntDisable(handle);
    rxLineCnt=handle->rxLineCnt;
    rxRIdx=handle->rxRIdx;
    AFPUartIntRestore(handle);
    while(i<handle->arxSize&&j<rxLineCnt){
        handle->arxBuf[i++]=handle->rxBuf[rxRIdx++];
        if(rxRIdx>=handle->rxSize)
            rxRIdx=0;
        if(i>1&&handle->arxBuf[i-1]=='\n'&&handle->arxBuf[i-2]=='\r')
            j++;
    }
    AFPUartIntDisable(handle);
    handle->rxLineCnt-=j;
    handle->rxRIdx+=i;
    if(handle->rxRIdx>=handle->rxSize)
        handle->rxRIdx-=handle->rxSize;
    if(handle->rxOvrn){
        rxBufSpace=handle->rxWIdx>handle->rxRIdx?
            handle->rxSize-(handle->rxWIdx-handle->rxRIdx):
            handle->rxRIdx-handle->rxWIdx-1;
        if(rxBufSpace>handle->rxfiflen<<1){
            CSL_UART_REGS->THR=AFPUART_XON;
            handle->rxOvrn=0;
        }
    }
    AFPUartIntRestore(handle);
    *len=i;
    return 0;
}

Uint32 AFPUartProcessA2I(
    Uint8 *s)
{
    Uint32 res[2];
    Uint32 i;

    for(i=0;i<2;++i){
        if(s[i]>='0'&&s[i]<='9')
            res[i]=s[i]-'0';
        else if(s[i]>='A'&&s[i]<='F')
            res[i]=s[i]-'A'+10;
        else if(s[i]>='a'&&s[i]<='f')
            res[i]=s[i]-'a'+10;
        else
            res[i]=0;
    }
    return res[0]*16+res[1];
}

Uint32 AFPUartProcessXtoi(
    AFPUart_Handle handle,
    Uint8 *s,
    Uint8 *d)
{
    Uint32 len=0;

    s+=8;
    while(!(*(s+2)=='\r'&&*(s+3)=='\n')){
        *d++=AFPUartProcessA2I(s);
        s+=2;
        len++;
    }
    return len;
}

void AFPUartProcessI82A(
    Uint8 s,
    Uint8 *res)
{
    Uint32 sp[2];
    Uint32 i;

    sp[0]=s>>4;
    sp[1]=s&0xF;
    for(i=0;i<2;++i){
        if(sp[i]<=0x9)
            res[i]=sp[i]+'0';
        else if(sp[i]>=0xa&&sp[i]<=0xf)
            res[i]=sp[i]-10+'A';
        else
            res[i]='0';
    }
    return;
}

void AFPUartProcessI162A(
    Uint16 s,
    Uint8 *res)
{
    Uint32 sp[4];
    Uint32 i;

    sp[0]=s>>12;
    sp[1]=s>>8;
    sp[2]=s>>4;
    sp[3]=s&0xF;
    for(i=0;i<4;++i){
        if(sp[i]<=0x9)
            res[i]=sp[i]+'0';
        else if(sp[i]>=0xa&&sp[i]<=0xf)
            res[i]=sp[i]-10+'A';
        else
            res[i]='0';
    }
    return;
}

Uint32 AFPUartProcessItox(
    AFPUart_Handle handle,
    Uint8 *s,
    Uint8 *d,
    Uint32 slen)
{
    Uint8 cnt,cks;
    Uint16 adr=0;
    Uint32 dlen=0; 
    Uint32 i;

    while(slen){
        cnt=slen>handle->srecLim?handle->srecLim:slen;
        slen-=cnt;
        *d++='S';
        *d++='1';
        AFPUartProcessI82A(cnt+3,d);
        d+=2;
        AFPUartProcessI162A(adr,d);
        d+=4;
        cks=(cnt+3&0xFF)+(adr>>8&0xFF)+(adr&0xFF);
        for(i=0;i<cnt;++i){
            AFPUartProcessI82A(*s,d);
            d+=2;
            cks+=*s;
            s++;
        }
        AFPUartProcessI82A(~cks,d);
        d+=2;
        adr+=cnt;
        *d++='\r';
        *d++='\n';
        dlen+=2+2+4+(cnt<<1)+2+2;
    }
    return dlen;
}

Int32 AFPUartStrnCmp(
    char *s,
    char *d,
    Uint32 len)
{
    Int32 res=0;

    while(!res&&len--)
        res=*s++-*d++;
    return res;
}

void AFPUartStrnCat(
    char *d,
    char *s,
    Uint32 len)
{
    while(len--)
        *d++=*s++;
}

Uint32 AFPUartProcessCommand(
    AFPUart_Handle handle,
    Uint16* s,
    Uint32 slen,
    Uint32 si,
    Uint16* d,
    Uint32 dlen,
    Uint32 di,
    Uint32 cnt)
{
    Uint32 doff;
    Uint32 len,nel;
    Uint32 lene,nele;
    Uint32 lenp,nelp;
    Int32 i,j,k;

    doff=di;
    while(cnt){
        len=handle->acp->fxns->length(handle->acp,&s[si]);
        nel=handle->acp->fxns->htgnel(handle->acp,&s[si]);
        if(nel>dlen-(di+2)){
            d[di++]=0xc901;
            d[di++]=0xdead;
            AE_TRACE((&trace, "AFPUartProcessCommand.%d:  0xdead: Response too big.", __LINE__));
            AFPUartLog(handle,handle->logObj,"AFPUART: Response too big");
            return di-doff;
        }
        /* Add encapsulation to alpha command -- ACP limitation */
        if((s[si]&0xff00)==0xc900||s[si]==0xcd01){
            if((s[si]&0xff00)==0xc900)
                k=1;
            else if(s[si]==0xcd01)
                k=2;
            lene=len-k;
            nele=0;
            while(lene){
                lenp=handle->acp->fxns->length(handle->acp,&s[si+k]);
                nelp=handle->acp->fxns->htgnel(handle->acp,&s[si+k]);
                k+=lenp;
                lene-=lenp;
                nele+=nelp;
            }
            if(nele<256)
                nel=nele+1;
            else 
                nel=nele+2;
            i=0;
            if(nel>dlen-(di+2)){
                d[di++]=0xc901;
                d[di++]=0xdead;
                AE_TRACE((&trace, "AFPUartProcessCommand.%d:  0xdead: Response too big.", __LINE__));
                AFPUartLog(handle,handle->logObj,"AFPUART: Response too big");
                return di-doff;
            }
        }
        else if(len<256){
            i=-1;
            s[si+i]=0xc900+len;
        }
        else{
            i=-2;
            s[si+i]=0xcd01;
            s[si+i+1]=len;
        }
        if(handle->acp->fxns->sequence(handle->acp,&s[si+i],&d[di])){
            AFPUartLog(handle,handle->logObj,"AFPUART: Sequence error");
            AE_TRACE((&trace, "AFPUartProcessCommand.%d:  0xdead: Sequence error.", __LINE__));
            if(nel)
                d[di]=0xdead;
            cnt-=len;
            si+=len;
            di++;
        }
        else{
            if(i){
                if((d[di]&0xff00)==0xc900)
                    j=1;
                else if(d[di]==0xcd01) 
                    j=2;
                for(k=0;k<nel;++k)
                    d[di+k]=d[di+k+j];
            }
            cnt-=len;
            si+=len;
            di+=nel;
        }
    }
    return di-doff;
}

Uint32 AFPUartProcess(
    AFPUart_Handle handle,
    Uint32 *len)
{
    Uint32 i=0,j=0;
    Int32 comLen,payLen,resLen;
    Int32 resIdx;

    while(i<*len&&i<handle->arxSize-1){
        if(!AFPUartStrnCmp((char*)&handle->arxBuf[i],"SC",2)){
            if(!AFPUartStrnCmp((char*)&handle->arxBuf[i],"SC0380017B\r\n",12)||
                !AFPUartStrnCmp((char*)&handle->arxBuf[i],"SC0381017A\r\n",12)){
                AFPUartStrnCat((char*)&handle->atxBuf[j],"SS03F0010B\r\n",12);
                i+=12;
                j+=12;
            }
            else{
                AFPUartStrnCat((char*)&handle->atxBuf[j],"\r\n",2);
                i+=12;
                j+=2;
            }
        }
        else if(!AFPUartStrnCmp((char*)&handle->arxBuf[i],"S0030000FC\r\n",12)){
            AFPUartStrnCat((char*)&handle->atxBuf[j],"S0030000FC\r\n",12);
            i+=12;
            j+=12;
        }
        else if(!AFPUartStrnCmp((char*)&handle->arxBuf[i],"S9030000FC\r\n",12)){
            AFPUartStrnCat((char*)&handle->atxBuf[j],"S9030000FC\r\n",12);
            i+=12;
            j+=12;
        }
        else if(!AFPUartStrnCmp((char*)&handle->arxBuf[i],"S1",2)){
            comLen=AFPUartProcessXtoi(handle,&handle->arxBuf[i],
                &handle->comBuf[handle->comWIdx]);
            i+=(comLen<<1)+12;
            handle->comLen+=comLen;
            handle->comWIdx+=comLen;
            if(!handle->payLen){
                handle->payLen=handle->acp->fxns->length(handle->acp,
                    (Uint16*)&handle->comBuf[4])<<1;
                if(handle->comBuf[5]==0xc9)
                    handle->comRIdx=6;
                else if(handle->comBuf[4]==0x01&&handle->comBuf[5]==0xcd)
                    handle->comRIdx=8;
                else
                    handle->comRIdx=4;
            }
            payLen=0;
            while(handle->payLen+payLen<handle->comLen){
                payLen=handle->acp->fxns->length(handle->acp,
                    (Uint16*)&handle->comBuf[4+handle->payLen])<<1;
                if(handle->payLen+payLen<=handle->comLen){
                    handle->payLen+=payLen;
                    payLen=0;
                }
            }
            if(handle->comLen==handle->payLen){
                resLen=AFPUartProcessCommand(handle,
                    (Uint16*)handle->comBuf,handle->comSize>>1,handle->comRIdx>>1,
                    (Uint16*)handle->resBuf,handle->resSize>>1,4>>1,
                    handle->comLen-handle->comRIdx+4>>1)<<1;
                if(resLen){
                    if(resLen<256){
                        handle->resBuf[2]=resLen>>1;
                        handle->resBuf[3]=0xc9;
                        resIdx=-2;
                    }
                    else{
                        handle->resBuf[0]=0x01;
                        handle->resBuf[1]=0xcd;
                        handle->resBuf[2]=resLen>>1&0xFF;
                        handle->resBuf[3]=resLen>>1>>8;
                        resIdx=-4;
                    }
                    j+=AFPUartProcessItox(handle,&handle->resBuf[4+resIdx],
                        &handle->atxBuf[j],resLen-resIdx);
                }
                else{
                    AFPUartStrnCat((char*)&handle->atxBuf[j],"S105000000C9FC\r\n",16);
                    j+=16;
                }
                handle->comLen=handle->payLen=0;
                handle->comWIdx=4;
            }
        }
        else{
            while(!(handle->arxBuf[i]=='\n'&&handle->arxBuf[i-1]=='\r'))
                handle->atxBuf[j++]=handle->arxBuf[i++];
            handle->atxBuf[j++]=handle->arxBuf[i++];
        }
    }
    *len=j;
    return 0;
}

Uint32 AFPUartWrite(
    AFPUart_Handle handle,
    Uint32 *len)
{
    CSL_UartRegsOvly CSL_UART_REGS=handle->devHandle;
    Uint32 txWIdx,txLineCnt=0;
    Uint32 i=0;

    if(*len){
        AFPUartIntDisable(handle);
        txWIdx=handle->txWIdx;
        AFPUartIntRestore(handle);
        while(i<*len){
            handle->txBuf[txWIdx++]=handle->atxBuf[i++];
            if(txWIdx>=handle->txSize)
                txWIdx=0;
            if(handle->atxBuf[i-1]=='\n'&&handle->atxBuf[i-2]=='\r')
                txLineCnt++;
        }
        AFPUartIntDisable(handle);
        handle->txLineCnt+=txLineCnt;
        handle->txWIdx+=*len;
        if(handle->txWIdx>=handle->txSize)
            handle->txWIdx-=handle->txSize;
        if(handle->txUdrn){
            if(handle->hostOvrn){
                AFPUartIntRestore(handle);
                while(handle->hostOvrn)
                    AFPUartTaskSleep(handle);
                AFPUartIntDisable(handle);
            }
            CSL_UART_REGS->THR=handle->txBuf[handle->txRIdx];
            handle->txRIdx++;
            if(handle->txRIdx>=handle->txSize)
                handle->txRIdx=0;
            handle->txUdrn=0;
        }
        AFPUartIntRestore(handle);
    }
    return 0;
}

void AFPUartTaskSuspend(
    AFPUart_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_setPri(Task_self(),-1);
    Task_yield();
#else
    TSK_setpri(TSK_self(),-1);
    TSK_yield();
#endif
}

void AFPUartTask(
    AFPUart_Handle handle)
{
    Uint32 len;

    if(!handle)
        handle=&AFPUART_OBJ;
    handle->rxBuf=(Uint8*)AFPUartMemAlloc(handle,handle->pBufSeg,
        handle->rxSize,4);
    handle->txBuf=(Uint8*)AFPUartMemAlloc(handle,handle->pBufSeg,
        handle->txSize,4);
    handle->arxBuf=(Uint8*)AFPUartMemAlloc(handle,handle->apBufSeg,
        handle->arxSize,4);
    handle->atxBuf=(Uint8*)AFPUartMemAlloc(handle,handle->apBufSeg,
        handle->atxSize,4);
    if(!handle->rxBuf||!handle->txBuf||!handle->arxBuf||!handle->atxBuf){
        AFPUartLog(handle,handle->logObj,"AFPUART: Buffer allocation failed");
        AFPUartTaskSuspend(handle);
    }
    if(!(handle->sem=AFPUartSemAlloc(handle))){
        AFPUartLog(handle,handle->logObj,"AFPUART: Semaphore allocation failed");
        AFPUartTaskSuspend(handle);
    }
    handle->rxfiflen=handle->rxfiftl==AFPUART_RXFIFTL_1?1:
        handle->rxfiftl==AFPUART_RXFIFTL_4?4:
        handle->rxfiftl==AFPUART_RXFIFTL_8?8:
        handle->rxfiftl==AFPUART_RXFIFTL_14?14:14;

    handle->comBuf=(Uint8*)AFPUartMemAlloc(handle,handle->crBufSeg,
        handle->comSize,4);
    handle->resBuf=(Uint8*)AFPUartMemAlloc(handle,handle->crBufSeg,
        handle->resSize,4);
    if(!handle->comBuf||!handle->resBuf){
        AFPUartLog(handle,handle->logObj,
            "AFPUART: Command/Response buffer allocation failed");
        AFPUartTaskSuspend(handle);
    }

    ACP_MDS_init();
    if(!(handle->acp=ACP_create(&ACP_MDS_IACP,handle->acpParams))){
        AFPUartLog(handle,handle->logObj,
            "AFPUART: ACP instance creation failed");
        AFPUartTaskSuspend(handle);
    }
    {
        IALG_Status *pStatus;
        extern struct{
           int size;
        }pafIdentification;
        ((ALG_Handle)handle->acp)->fxns->algControl((IALG_Handle)(handle->acp),
            ACP_GETSTATUSADDRESS1,(IALG_Status*)&pStatus);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_UART,
            pStatus);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_BETATABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.betaTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_PHITABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.phiTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_SIGMATABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.sigmaTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_IDENTITY,
            (IALG_Status*)&pafIdentification);
        AFPUartLog(handle,handle->logObj,"AFPUART: ACP initialized");
    }

    /* Open device */
    AFPUartOpen(handle);
    AFPUartLog(handle,handle->logObj,"AFPUART: AFPUART initialized");

    /* Receive, process and transmit data */
    while(1){
        if(AFPUartRead(handle,&len))
            AFPUartLog(handle,handle->logObj,
                "AFPUART: Error in reading data");
        else if(AFPUartProcess(handle,&len))
            AFPUartLog(handle,handle->logObj,
                "AFPUART: Error in processing data");
        else if(AFPUartWrite(handle,&len))
            AFPUartLog(handle,handle->logObj,
                "AFPUART: Error in writing data");
    }
}

#ifdef BIOS6_NONLEGACY
extern HeapMem_Handle SDRAMHeap;
#else
extern Int SDRAM;
#endif

AFPUart_Obj AFPUART_OBJ={
    sizeof(AFPUart_Obj),
    AFPUART_DEV_UART2,
    AFPUART_RXFIFTL_14,
    AFPUART_DIV_B19200,
    2048,
    2048,
#ifdef BIOS6_NONLEGACY
    (void*)&SDRAMHeap,
#else
    (void*)&SDRAM,
#endif
    2048,
    2048,
#ifdef BIOS6_NONLEGACY
    (void*)&SDRAMHeap,
#else
    (void*)&SDRAM,
#endif
#ifdef BIOS6_NONLEGACY
    NULL,
#else
    NULL,
#endif
    7,
    0,0,0,
    0,
    0,0,0,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    0,0,
    0,0,
    0,0,
    0,
    0,0,
    1024,860,
#ifdef BIOS6_NONLEGACY
    (void*)&SDRAMHeap,
#else
    (void*)&SDRAM,
#endif
    32,
    NULL,
    NULL,
    0,0,
    4,4,
    (ACP_Params*)&ACP_PARAMS,
    NULL,
};
