
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Notify Information Change functionality
//
//
//

#include <da830lib.h>

#include <paftyp.h>

#include <std.h>
#include <alg.h>

#include <acp.h>
#include <acp_mds.h>

// ----------------------------------------------------------------------------

typedef struct NIC_GPIO_Status_t
{
	unsigned int size;
	unsigned char mode;
	unsigned char state;
	unsigned char deassert;
	unsigned char unused;
} NIC_GPIO_Status;

NIC_GPIO_Status nic = {
	sizeof (NIC_GPIO_Status),								// size
	1,														// mode
	0,														// state
	0,														// deassert
	0,														// unused
};

// ----------------------------------------------------------------------------

ACP_Handle nicAcp = NULL;
unsigned int nicGpioBank;
unsigned int nicGpioMask;
unsigned int nicActive = 0;

// ----------------------------------------------------------------------------

unsigned int (*nicChecks[]) (void);
void nicGpioInit (void);
void nicGpioSetState (unsigned int state);

// ----------------------------------------------------------------------------

void customSystemStreamIdleNIC (void) {
	int i;
	
    if (!nicAcp) {

		// Initialize ACP handle
        ACP_MDS_init ();
        nicAcp = ACP_create (&ACP_MDS_IACP, &ACP_PARAMS);
        if (!nicAcp)
            return;

		// Attach NIC status structure
        nicAcp->fxns->attach (nicAcp, ACP_SERIES_STD, STD_BETA_ASSERT, (IALG_Status *) &nic);
        
        // Initialize GPIO pin for NIC
        nicGpioInit ();
        nicGpioSetState (nic.state);
    }

	if (!nic.mode) {
		// Feature is disabled
		if (nic.state) {
			nic.state = 0;
			nicGpioSetState (nic.state);
		}
		nicActive = 0;
		nic.deassert = 0;
	}
	else {
		// Feature is enabled
		if (nic.deassert) {
			// De-assert, if requested
			if (nic.state) {
				nic.state = 0;
				nicGpioSetState (nic.state);
			}
			nicActive = 0;
			nic.deassert = 0;
		}
		else if (nic.state)
			// Do nothing, if already asserted
			;
		else {
			if (nicActive) {
				// Perform checks, assert if required
				i = 0;
				while (nicChecks[i]) {
					if (nicChecks[i]())
						break;
					i++;
				}
				if (nicChecks[i]) {
					nic.state = 1;
					nicGpioSetState (nic.state);
				}
			}
			else {
				// Perform checks, ignore results
				i = 0;
				while (nicChecks[i]) {
					nicChecks[i]();
					i++;
				}
				// Activate from next call
				nicActive = 1;
			}
		}
	}
}

// ----------------------------------------------------------------------------

int nicGetStat (ACP_Unit * from)
{
	ACP_Unit to[5];
	int readVal = -1;
	unsigned char type = ((from[1] >> 8) & 0x7);

	if (!(nicAcp->fxns->sequence (nicAcp, from, to)))
	{
		switch (type)
		{
			case 2:
				readVal = (to[2] & 0xFF);
				break;

			case 3:
				readVal = to[3];
				break;

			case 4:
				readVal = to[3] | (to[4] << 16);
				break;

			default:
				break;
		}
	}

	return readVal;
}

// Start of customizable code -------------------------------------------------

#include <pafdec_a.h>

unsigned int nicCheck1 () {
    static int prevStat = 0;
    int retVal;

    static ACP_Unit from[] = {0xc902, readDECSourceProgram};

    retVal = nicGetStat (from);
    if (retVal != prevStat) {
        prevStat = retVal;
        return 1;
    }
    return 0;
}

unsigned int nicCheck2 () {
    static int prevStat = 0;
    int retVal;

    static ACP_Unit from[] = {0xc902, readDECSampleRate};

    retVal = nicGetStat (from);
    if (retVal != prevStat) {
        prevStat = retVal;
        return 1;
    }
    return 0;
}

// more...

// NOTE: Checks can be added as more functions
//       or a single function can perform multiple checks

unsigned int (*nicChecks[]) (void) = {
	nicCheck1,
	nicCheck2,
	// more...
	NULL,
};

// Use GP0[12] pin on DA830 EVM, which maps to user LED1 (DS1)
// This LED is active low

#define GP_MOD_NUM 0
#define GP_PIN_NUM 12
#define ACTIVE_LOW 1

void nicGpioInit (void) {
	gpioInitWrite (GP_MOD_NUM, GP_PIN_NUM, ACTIVE_LOW);
}

void nicGpioSetState (unsigned int state) {
	gpioWrite (GP_MOD_NUM, GP_PIN_NUM, ACTIVE_LOW ^ state);
}

// End of customizable code ---------------------------------------------------
