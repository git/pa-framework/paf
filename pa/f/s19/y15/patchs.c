
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//
// Framework Declarations
//

#include <as1-f2-patchs.h>
#include <asp0.h>
#include <asp1.h>

//
// Decoder Definitions
//

#include <pcm.h>
#include <pcm_mds.h>

#include <dwr_inp.h>

const PAF_ASP_LinkInit decLinkInitY15[] =
{
    PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_decLinkInit[] =
{
    decLinkInitY15,
};

//
// Audio Stream Processing Declarations & Definitions
//

#include <ass.h>
#include <ass_tii.h>

#include <dm.h>
#include <dm_tii.h>

#include <ml.h>
#include <ml_mds.h>

#include <aspstd.h>

const PAF_ASP_LinkInit aspLinkInitAllY15[] =
{
    PAF_ASP_LINKINIT(STD,ASS,TII),
    PAF_ASP_LINKINIT (STD, DM, TII),
    PAF_ASP_LINKINIT (STD, ML, MDS),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitNilY15[] =
{
    PAF_ASP_LINKINIT(STD,ASS,TII),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitStdY15[] =
{
    PAF_ASP_LINKINIT(STD,ASS,TII),
    PAF_ASP_LINKINIT (STD, DM, TII),
    PAF_ASP_LINKINIT (STD, ML, MDS),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitCusY15[] =
{
	PAF_ASP_LINKINIT(STD,ASS,TII),
    PAF_ASP_LINKNONE,
};
static const PAF_ASP_LinkInit aspLinkInitAllY15_2[] =
{
    PAF_ASP_LINKNONE,
};

#define aspLinkInitNilY15_2 NULL

static const PAF_ASP_LinkInit aspLinkInitStdY15_2[] =
{
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitCusY15_2[] =
{
    PAF_ASP_LINKNONE,
};


const PAF_ASP_LinkInit *const patchs_aspLinkInit[2][GEARS] =
{
    {
     aspLinkInitAllY15,
     aspLinkInitNilY15,
     aspLinkInitStdY15,
     aspLinkInitCusY15,
     },
     {
     aspLinkInitAllY15_2,
     aspLinkInitNilY15_2,
     aspLinkInitStdY15_2,
     aspLinkInitCusY15_2,
     },
};

//
// Encoder Definitions
//

#include <pce.h>
#include <pce_tii.h>
extern const IPCE_Params IPCE_PARAMS_CUS;
extern const IPCE_Params IPCE_PARAMS_THX_EXT; // use if extended channels needed 
extern const IPCE_Params IPCE_PARAMS_THX; // use if 7.1 channels needed
extern const IPCE_Params IPCE_PARAMS_CUS_SEC; 
const PAF_ASP_LinkInit encLinkInitY15[] =
{
    PAF_ASP_LINKINITPARAMS (STD, PCE, TII, &IPCE_PARAMS_THX_EXT),
    PAF_ASP_LINKNONE,
};
static const PAF_ASP_LinkInit encLinkInitY15Secondary[] =
{
    PAF_ASP_LINKINITPARAMS(STD,PCE,TII,&IPCE_PARAMS_CUS_SEC),
    PAF_ASP_LINKNONE,
};


const PAF_ASP_LinkInit *const patchs_encLinkInit[] =
{
    encLinkInitY15,
    encLinkInitY15Secondary,
};

//
// Audio Stream Patch Definition
//

extern const PAF_SIO_ParamsN patchs_devinp[];
extern const PAF_SIO_ParamsN patchs_devout[];

const PAF_AST_Patchs patchs_PAy =
{
    patchs_devinp,
    patchs_devout,
    patchs_decLinkInit,
    patchs_aspLinkInit,
    patchs_encLinkInit,
};

// EOF
