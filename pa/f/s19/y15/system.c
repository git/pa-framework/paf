
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//
// IROM-Inclusion Definitions
//

/* required */
#define PAF_AST_PARAMS_PAU

/* optional */
#define PAF_AST_PARAMS_PAI
#define PAF_AST_PARAMS_PAH
#define PAF_AST_PARAMS_PAD
#define PAF_AST_PARAMS_PAY
#if (PAF_IROM_BUILD != 0xD610A004) || (PAF_IROM_BUILD != 0xD710E001)
#define PAF_AST_PARAMS_PAZ /* not working? */
#endif

/* required */
#define PAF_AST_PARAMS_PRIMARY
#if defined (PAF_AST_PARAMS_PAD) || defined (PAF_AST_PARAMS_PAY) || defined (PAF_AST_PARAMS_PAZ)
#define PAF_AST_PARAMS_SECONDARY
#endif

//
// File includes
//

#include <std.h>

#include <acp.h>
#include <pafsys.h>

#include <ss0.h>

#include <as1-f2.h>
#include <as1-f2-params.h>
#include <as1-f2-patchs.h>
//
// audioStream1Idle
//
//  Control audio streams
//

extern PAF_SST_FxnsMain     systemStreamMain;
extern PAF_SST_FxnsCompute  systemStream1Compute;
extern PAF_SST_FxnsTransmit systemStream1Transmit;
extern PAF_SST_FxnsCompute  systemStream2Compute;
extern PAF_SST_FxnsTransmit systemStream2Transmit;
extern PAF_SST_FxnsCompute  systemStream3Compute;
extern PAF_SST_FxnsTransmit systemStream3Transmit;
#ifdef THX
extern PAF_SST_FxnsCompute  systemStream4Compute;
extern PAF_SST_FxnsTransmit systemStream4Transmit;
#endif
extern PAF_SST_FxnsCompute  systemStream5Compute;
extern PAF_SST_FxnsTransmit systemStream5Transmit;

const struct {
    PAF_SST_FxnsMain *main;
    Int count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[5];
} systemStreamPrimaryFxns =
{
    &systemStreamMain,
    5,
    {
        { &systemStream1Compute, &systemStream1Transmit, },
        { &systemStream2Compute, &systemStream2Transmit, },
        { &systemStream3Compute, &systemStream3Transmit, },
#ifdef THX
 		{ &systemStream4Compute, &systemStream4Transmit, },
#else
        { NULL, NULL, },
#endif
        { &systemStream5Compute, &systemStream5Transmit, },
    },
};

const PAF_SST_Params systemStreamParams_PA[1] =
{
    {
        1, 0, 1, 1,
        (const PAF_SST_Fxns *)&systemStreamPrimaryFxns,
    },
};

#ifdef PAF_AST_PARAMS_PAU
extern const PAF_SST_Params systemStreamParams_PAu[1];
asm("_systemStreamParams_PAu .set _systemStreamParams_PA");
#endif /* PAF_AST_PARAMS_PAU */

#ifdef PAF_AST_PARAMS_PAI
extern const PAF_SST_Params systemStreamParams_PAi[1];
asm("_systemStreamParams_PAi .set _systemStreamParams_PA");
#endif /* PAF_AST_PARAMS_PAI */

#ifdef PAF_AST_PARAMS_PAD
extern const PAF_SST_Params systemStreamParams_PAd[1];
asm("_systemStreamParams_PAd .set _systemStreamParams_PA");
#endif /* PAF_AST_PARAMS_PAD */

#ifdef PAF_AST_PARAMS_PAY
extern const PAF_SST_Params systemStreamParams_PAy[1];
asm("_systemStreamParams_PAy .set _systemStreamParams_PA");
#endif /* PAF_AST_PARAMS_PAY */

/* .......................................................................... */

#ifdef PAF_AST_PARAMS_PAH

const PAF_SST_Params systemStreamParams2[1] =
{
    {
        1,  // streams
        1,  // stream1
        2,  // streamN
        2,  // ss
        (const PAF_SST_Fxns *)&systemStreamPrimaryFxns,
    },
};

PAF_SystemStatus systemStreamStatus2[1] = {
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_ALL,
        0,
        PAF_SYS_RECREATIONMODE_AUTO,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_BASS,
        PAF_SYS_CCRTYPE_STANDARD,
        0, 0, 0,
        0, 								// Unused         
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
		0, 0, 0, 0,						//unused2[4]
		PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
    },
};

PAF_SST_Config systemStreamConfig2[1] =
{
    {
        NULL,
        &systemStreamStatus2[0],
    },
};

#endif /* PAF_AST_PARAMS_PAH */

/* .......................................................................... */

#ifdef PAF_AST_PARAMS_PAZ

const struct {
    PAF_SST_FxnsMain *main;
    Int count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[1];
} systemStreamSecondaryFxns =
{
    &systemStreamMain,
    1,
    {
        { &systemStream1Compute, &systemStream1Transmit, },
    },
};

const PAF_SST_Params systemStreamParams_PAz[2] =
{
    {
        2, 0, 2, 1,
        (const PAF_SST_Fxns *)&systemStreamSecondaryFxns,
    },
    {
        2, 0, 2, 2,
        (const PAF_SST_Fxns *)&systemStreamPrimaryFxns,
    },
};

#endif /* PAF_AST_PARAMS_PAZ */

/* .......................................................................... */

#if defined (PAF_AST_PATCHS_PA17) \
    || defined (PAF_AST_PATCHS_PAU) \
    || defined (PAF_AST_PATCHS_PAI) \
    || defined (PAF_AST_PATCHS_PAH) \
    || defined (PAF_AST_PATCHS_PAD) \
    || defined (PAF_AST_PATCHS_PAY)

#define systemStreamParams systemStreamParams_PA

PAF_SystemStatus systemStreamStatus[1] = {
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_ALL,
        0,
        PAF_SYS_RECREATIONMODE_AUTO,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_BASS,
        PAF_SYS_CCRTYPE_STANDARD,       // channelConfigurationRequestType
        0, 0, 0,                        // switchImage, imageNum, imageNumMax
        0, 								// Unused         
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
		0, 0, 0, 0,						//unused2[4]
		PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
    },
};

PAF_SST_Config systemStreamConfig[1] =
{
    {
        NULL,
        &systemStreamStatus[0],
    },
};

#endif /* defined (...) */

/* .......................................................................... */

#if defined (PAF_AST_PATCHS_PAZ)

#define systemStreamParams systemStreamParams_PAz

PAF_SystemStatus systemStreamStatus[2] = {
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_DEC,
        0,
        PAF_SYS_RECREATIONMODE_STEREO,
        2 + PAF_SYS_SPEAKERSIZE_LARGE,
        0,
        0,
        0,
        0,
        PAF_SYS_CCRTYPE_STANDARD,
        0, 0, 0,
        0, 								// Unused         
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
		0, 0, 0, 0,						//unused2[4]
		PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
    },
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_ALL,
        0,
        PAF_SYS_RECREATIONMODE_AUTO,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_BASS,
        PAF_SYS_CCRTYPE_STANDARD,
        0, 0, 0,
        0, 								// Unused         
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
		0, 0, 0, 0,						//unused2[4]
		PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
    },
};

PAF_SST_Config systemStreamConfig[2] =
{
    {
        NULL,
        &systemStreamStatus[0],
    },
    {
        NULL,
        &systemStreamStatus[1],
    },
};

#endif /* PAF_AST_PATCHS_PAZ */

/* .......................................................................... */

void
audioStream1Idle(void)
{
    Int i;

    for (i=0; i < lengthof (systemStreamConfig); i++)
        systemStreamParams[i].fxns->main
            (&systemStreamParams[i], &systemStreamConfig[i]);
}

/* .......................................................................... */

#if defined (PAF_AST_PARAMS_PAH)

void
audioStream2Idle(void)
{
    systemStreamParams[0].fxns->main
        (&systemStreamParams2[0], &systemStreamConfig2[0]);
}

#endif /* PAF_AST_PARAMS_PAH */
