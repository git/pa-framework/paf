
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Function Definitions
//
//     Audio Framework is Audio Streams 1-N for IROM.
//
//
//

#include <stdio.h>
#include <std.h>
#include <alg.h>
#include <ialg.h>
#include <sio.h>
#include <mem.h>
#include <tsk.h>
#include <string.h> // memset
#include <sts.h>
#include <clk.h>

#include <ztop.h>
#include <arc_ext.h>

#include "statStruct.h"  // A concise structure for debugging

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, you might set the mask to 0, but I'd always leave it at 1.
#ifdef AS_INPUTB_TRACE_MASK
  // app can define this in ztop.h
  #define CURRENT_TRACE_MASK    AS_INPUTB_TRACE_MASK  
#else
  #define CURRENT_TRACE_MASK  3   // terse and general only
#endif

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // half dozen lines per frame
#define TRACE_MASK_VERBOSE  4   // trace full operation
#define TRACE_MASK_DATA     8   // Show data
#define TRACE_MASK_TIME    0x10  // Timing related traces

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
 #define TRACE_DATA(a) LOG_printf a
#else
 #define TRACE_DATA(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
// consolidate list of processing strings, indexed by PAF_SOURCE
static char *procName[] =
{
        "", //PAF_SOURCE_UNKNOWN
        "", //PAF_SOURCE_NONE
        "AS%d: Pass processing ...",   //PAF_SOURCE_PASS
        "AS%d: SNG processing ...",    //PAF_SOURCE_SNG
        "AS%d: Auto processing ...",   //PAF_SOURCE_AUTO
        "AS%d: Auto processing ...",   //PAF_SOURCE_BITSTREAM
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTSALL
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCMAUTO
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCM
        "AS%d: PCN processing ...",    //PAF_SOURCE_PC8
        "AS%d: AC3 processing ...",    //PAF_SOURCE_AC3
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS
        "AS%d: AAC processing ...",    //PAF_SOURCE_AAC
        "AS%d: MPG processing ...",    //PAF_SOURCE_MPEG
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS12
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS13
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS14
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS16
        "AS%d: WMP processing ...",    //PAF_SOURCE_WMA9PRO
        "AS%d: MP3 processing ...",    //PAF_SOURCE_MP3
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD1
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD2
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD3
        "AS%d: DDP processing ...",    //PAF_SOURCE_DDP
        "AS%d: DTSHD processing ...",  //PAF_SOURCE_DTSHD
        "AS%d: THD processing ...",    //PAF_SOURCE_THD
        "AS%d: DXP processing ...",    //PAF_SOURCE_DXP
        "AS%d: WMA processing ...",    //PAF_SOURCE_WMA
};

#else
 #define TRACE_VERBOSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_TIME)
 #define LOG_TIME
 #define TRACE_TIME(a) LOG_printf a
 #define TIME_MOD  trace // this could be different
 Int dtime2()
 {
     static Int old_time = 0;
     Int time = TSK_time();
     Int delta_time = time - old_time;
     old_time = time;
     return( delta_time);
 }
 
 static char *stateName[11] =
 {
     "INIT",
     "INFO1",
     "AGAIN",
     "INFO2",
     "CONT",
     "TIME",
     "DECODE",
     "STREAM",
     "ENCODE",
     "FINAL",
     "QUIT"
 };

#else
 #define TRACE_TIME(a)
#endif

// .............................................................................

#include "AS_common.h"

#define __TASK_NAME__  "AS_InputA"

// -----------------------------------------------------------------------------
// Audio Stream Task
//
//   Name:      AS_InputA_Task
//   Purpose:   BIOS Task Function for Audio Framework Number 2.
//   From:      BIOS
//   Uses:      See code.
//   States:    x
//   Return:    Returns void on initialization failure.
//              Otherwise, does not return.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//              * State information on processing.
//              * Memory allocation errors.
//              * Error number macros.
//              * Line number macros.
//

#if 1  // Need local definitions when files are not in library?
 LINNO_DECL (AS_InputA_Task); /* Line number macros */
 ERRNO_DECL (AS_InputA_Task); /* Error number macros */
#else
 volatile far int linno_AS_InputA_Task;

 inline void f_errno_AS_InputA_Task (int x)
 {
    extern volatile far int errno_AS_InputA_Task, errst_AS_InputA_Task;
    errno_AS_InputA_Task = x;
    if (! errst_AS_InputA_Task) errst_AS_InputA_Task = x;
 }
 volatile far int errno_AS_InputA_Task, errst_AS_InputA_Task;
#endif

Int PAF_AST_computeRateRatio (const PAF_AST_Params *pP, PAF_AST_Config *pC, double *arcRatio);

void
AS_InputA_Task (Int betaPrimeValue, const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ)
{
    // Task data
    PAF_AST_Config PAF_AST_config;      /* Local configuration */
    PAF_AST_Config *pC;                 /* Local configuration pointer */

    // Local data
    Int as = betaPrimeValue + 1;        /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int i;                              /* phase */
    Int errno;                          /* error number */
    Int zMD, zMI, zX;

    TRACE_TERSE((&TR_MOD, "AS_InputA task waiting."));

    // Ensure that all idle threads run before framework threads.
    // Each thread that starts sets a bit in gStartupOrder.
    while (ZTOP_IDLE_COMPLETE != (ZTOP_IDLE_COMPLETE & gStartupOrder))
        TSK_sleep(1);

    //
    // Audio Framework Configuration (*pC):
    //
    //   Set default.
    //

    pC = &PAF_AST_config;
    pC->as = as;

    TRACE_TERSE((&TR_MOD, "AS_InputA task started with betaPrimeValue %d.", betaPrimeValue));
    printf(" in A: ");

    //
    // Audio Framework Parameters & Patch (*pP, *pQ):
    //

    if (! pP) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: AS%d: No Parameters defined. Exiting.", __TASK_NAME__, __FUNCTION__, __LINE__));
        LINNO_RPRT (AS_InputA_Task, -1);
        return;
    }

    if (! pQ) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: AS%d: No Patchs defined. Exiting.", __TASK_NAME__, __FUNCTION__, __LINE__));
        LINNO_RPRT (AS_InputA_Task, -1);
        return;
    }

    //
    // Initialize message log trace and line number reporting
    //

    for (z=STREAM1; z < STREAMN; z++)
        TRACE_TERSE((&TR_MOD, "AS_InputA.%d: AS%d: initiated", __LINE__, as+z));
    LINNO_RPRT (AS_InputA_Task, -1);

    //
    // Determine stream and decoder indices associated with the master input
    //
    zMI = pP->zone.master;
    pC->masterDec = zMI;
    pC->masterStr = zMI;
    for (zX = DECODE1; zX < DECODEN; zX++) {
        if (pP->inputsFromDecodes[zX] == zMI) {
            pC->masterDec = zX;
            pC->masterStr = pP->streamsFromDecodes[zX];
            break;
        }
    }
    zMD = pC->masterDec;
    pC->masterStr = pC->masterStr;

    // Initialize as per parameterized phases:
    //
    //   In standard form these are:
    //   - Malloc: Memory Allocation
    //   - Config: Configuration Initialization
    //   - AcpAlg: ACP Algorithm Initialization and Local Attachment
    //   - Common: Common Algorithm Initialization
    //   - AlgKey: Algorithm Keying
    //   - Device: I/O Device Initialization
    //   - Unused: (available)
    //   - Unused: (available)
    //

    

    LINNO_RPRT (AS_InputA_Task, -2);
    for (i=0; i < lengthof (pP->fxns->initPhase); i++) {
        Int linno;
        if (pP->fxns->initPhase[i]) {
            if (linno = pP->fxns->initPhase[i] (pP, pQ, pC)) {
                LINNO_RPRT (AS_InputA_Task, linno);
                return;
            }
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: initialization phase - null", __LINE__, as+pC->masterStr));
        }
        TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: initialization phase - %d completed", __LINE__,as+pC->masterStr, i));
        LINNO_RPRT (AS_InputA_Task, -i-3);
    }

#ifdef _WIN32
    {
        extern PAF_AST_Config *pPAFConfig;                 /* PAF configuration pointer */
        
        pPAFConfig = pC;
    }
#endif

    //
    // End of Initialization -- final memory usage report.
    //
    if (pP->fxns->memStatusPrint)
        pP->fxns->memStatusPrint(HEAP_INTERNAL,HEAP_EXTERNAL,HEAP_INTERNAL1);

    gStartupOrder |= ZTOP_INPUT_A_THREAD_BIT;

    for (z=STREAM1; z < STREAMN; z++)
        TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: running", __LINE__, as+z));

    errno = 0;
    //
    // Main processing loop
    //
    for (;;) {

        Int sourceSelect;
        XDAS_Int8 sourceProgram;

        TRACE_GEN((&TR_MOD, "AS_InputA Task.%d (begin Main loop) (errno 0x%x)", __LINE__, errno));
        TRACE_TIME((&TIME_MOD, "AS_InputA... + %d = %d (begin Main loop)", dtime3(), TSK_time()));

        // since not decoding indicate such
        pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_NONE);

        // any error forces idling of input
        if (errno) {
            for (z=INPUT1; z < INPUTN; z++)
                if (pC->xInp[z].hRxSio)
                    SIO_idle (pC->xInp[z].hRxSio);

            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: AS%d: errno = 0x%x", __LINE__, as+pC->masterStr, errno));
            ERRNO_RPRT (AS_InputA_Task, errno);
        }

        // Execute a TSK_sleep to ensure that any non-blocking code paths are broken
        // up to allow lower priority tasks to run. This may seem odd to be at the top
        // of the state machine but provides for a cleaner flow even though the very
        // first time we enter we do a sleep which is non-intuitive.
        TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: ... sleeping ...", __LINE__, as+pC->masterStr));
        TRACE_TIME((&TIME_MOD, "AS_InputA... + %d = %d (begin SLEEP)", dtime2(), TSK_time()));
        TSK_sleep (1);

        TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: Device selection ...", __LINE__, as+pC->masterStr));
        if (errno = pP->fxns->selectDevices (pP, pQ, pC))
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA: errno = 0x%04x at line %d. AS%d", errno, __LINE__, as+pC->masterStr));
            continue;
        }

        // if no master input selected then we don't know what may be at the input
        // so set to unkown and skip any remaining processing
        if (! pC->xInp[zMI].hRxSio) {
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_UNKNOWN;
            statStruct_LogLock(STATSTRUCT_INPUT_A, FALSE);
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: AS%d: No input selected...", __LINE__, as+pC->masterStr));
            continue;
        }

        statStruct_SetDibDeviceHandle(STATSTRUCT_INPUT_A, (int)pC->xInp[0].hRxSio);

        // if here then we have a valid input so query its status
        if (errno = pP->fxns->updateInputStatus (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufStatus, &pC->xInp[zMI].inpBufConfig))
        {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: continue as updateInputStatus returns 0x%x", __LINE__, errno));
            continue;
        }

        // If master decoder is not enabled, or the input is unlocked, then do nothing
        if (!pC->xDec[zMD].decodeStatus.mode || !pC->xInp[zMI].inpBufStatus.lock)
        {
            statStruct_LogLock(STATSTRUCT_INPUT_A, FALSE);
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: Not locked, continue", __LINE__));
            continue;
        }

        // if special continuous mode then jump straight to decodeProcessing
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS)
        {
            errno = pP->fxns->decodeProcessing (pP, pQ, pC, NULL);
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: DEC_MODE_CONTINUOUS: continue", __LINE__));
            continue;
        }

        // If no source selected then do nothing
        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_NONE) {
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_NONE;
            statStruct_LogLock(STATSTRUCT_INPUT_A, FALSE);
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d.  AS%d: no source selected, continue", __LINE__, as+pC->masterStr)); 
            continue;
        }

        // here we'll assume we are locked for purposes of priority settinig
        statStruct_LogLock(STATSTRUCT_INPUT_A, TRUE);

        // If we want pass processing then proceed directly
        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_PASS) {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d, AS%d: Pass processing ...\n", __LINE__, as+pC->masterStr));
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_PASS;
            pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_PASS);
            if (pP->fxns->passProcessing)
                errno = pP->fxns->passProcessing (pP, pQ, pC, NULL);
            else {
                TRACE_TERSE((&TR_MOD, "AS_InputA.%d: Pass Processing not supported, errno 0x%x", __LINE__, as+pC->masterStr, ASPERR_PASS));
                errno = ASPERR_PASS;
            }
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: continue", __LINE__));
            continue;
        }

        // .....................................................................
        // At this point we have an enabled input and want to decode something.
        // If no decoder selected then do nothing. Need to reset the sourceProgram, since
        // when no decoder is selected there are no calls to IB

        if (errno = pP->fxns->autoProcessing (pP, pQ, pC, pC->xDec[zMD].decodeStatus.sourceSelect, pC->xDec[zMD].decAlg[PAF_SOURCE_PCM]))
        {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: autoProcessing returns 0x%x, continue", __LINE__, errno));
            continue;
        }
        // query for input type
        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM, (Arg )&sourceProgram))
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: SIO_ctrl returns 0x%x, then 0x%x, continue", __LINE__, errno, ASPERR_AUTO_PROGRAM));
            errno = ASPERR_AUTO_PROGRAM;
            continue;
        }
        pC->xDec[zMD].decodeStatus.sourceProgram = sourceProgram;

        // if input is unclassifiable then do nothing
        if (sourceProgram == PAF_SOURCE_UNKNOWN)
        {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: Source program unknown. continue", __LINE__));
            continue;
        }

        // now that we have some input classification, and possibly an outstanding
        // input frame, we determine whether or not to call decodeProcessing and with
        // what decAlg.
        sourceSelect = PAF_SOURCE_NONE;
        switch (pC->xDec[zMD].decodeStatus.sourceSelect) {

            // If autodetecting, decoding everything, and input is something
            // (i.e. bitstream or PCM) then decode.
            case PAF_SOURCE_AUTO:
                if (sourceProgram >= PAF_SOURCE_PCM)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only PCM, and input is PCM then decode.
            case PAF_SOURCE_PCMAUTO:
                if (sourceProgram == PAF_SOURCE_PCM)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only bitstreams, and input is a bitstream then decode.
            case PAF_SOURCE_BITSTREAM:
                if (sourceProgram >= PAF_SOURCE_AC3)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only DTS, and input is DTS then decode.
            case PAF_SOURCE_DTSALL:
                switch (sourceProgram) {
                    case PAF_SOURCE_DTS11:
                    case PAF_SOURCE_DTS12:
                    case PAF_SOURCE_DTS13:
                    case PAF_SOURCE_DTS14:
                    case PAF_SOURCE_DTS16:
                    case PAF_SOURCE_DTSHD:
                        sourceSelect = sourceProgram;
                        break;
                }
                break;

            // All others, e.g., force modes, fall through to here.
            // If user made specific selection then program must match select.
            // (NB: this compare relies on ordering of PAF_SOURCE)
            default:
                sourceSelect = pC->xDec[zMD].decodeStatus.sourceSelect;
                if ((sourceSelect >= PAF_SOURCE_PCM) && (sourceSelect <= PAF_SOURCE_N)) {
                    if (sourceProgram != sourceSelect)
                        sourceSelect = PAF_SOURCE_NONE;
                }
                break;
        }

        // if we didn't find any matches then skip
        if (sourceSelect == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA.%d: no matching source type, continue", __LINE__));
            continue;
        }

        // set to unknown so that we can ensure, for IOS purposes, that sourceDecode = NONE
        // iff we are in this top level state machine and specifically not in decodeProcessing
        pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_UNKNOWN);

        TRACE_VERBOSE((&TR_MOD, procName[sourceProgram], as+pC->masterStr));

        if (errno = pP->fxns->decodeProcessing(pP, pQ, pC, pC->xDec[zMD].decAlg[sourceSelect]))
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: decodeProcessing returns 0x%x, continue", __LINE__, errno));
        }

    }  // End of main processing loop for (;;) .

} //AS_InputA_Task

// -----------------------------------------------------------------------------
// AST Processing Function - Decode Processing
//
//   Name:      PAF_AST_decodeProcessing
//   Purpose:   Audio Stream Task Function for processing audio data for
//              output on a continuous basis, including detection of the
//              input type.
//   From:      AS_InputA_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization (via children).
//              * State information on processing (via children).
//              * Decode warnings.
//

// When "writeDECModeContinuous" is used for zMI input/decode:
// PAF_AST_decodeProcessing() loop may be (is designed to be) exited:
// (a) if "writeDECCommandRestart" is used
//    (or "writeDECCommandAbort", but this performs no cleanup whatsoever, and so its use is discouraged)
// (b) if "writeDECSourceSelectNone" is used
// [ the latter events lead to QUIT state, simply for exiting (errme = errno = ASPERR_QUIT)
// (c) if an error occurs in
//     INIT
//     CONT ("subsequent block state", which "Establish[es] secondary timing")
//         -> PAF_AST_decodeCont(): "Await slave inputs"
//     STREAM (errno |= PAF_COMPONENT_ASP)
//     ENCODE (errno |= PAF_COMPONENT_ENCODE)
// [ the latter errors lead to "switch_break:"
//         -> PAF_AST_decodeComplete(), which always returns 0 (no error) ]
//
// [ Notably, in FINAL ("frame-finalization state")
//         -> PAF_AST_decodeFinalTest() is *not* called,
//   and so any other (asynchronous) changes in pC->xDec[zMD].decodeStatus.sourceSelect are ignored. ]
// [ For completeness, note also: "default" state, internal check (errme = errno = ASPERR_UNKNOWNSTATE) ]
//
// States in which error can't occur:
//     AGAIN ("subsequent initial state")
//
// States in which (some) errors must be handled:
//     INFO1 ("first frame state")
//         -> PAF_AST_decodeInfo(): pass on ASPERR_INFO_RATECHANGE, ASPERR_INFO_PROGRAM ("bad" internal error)
//            -> *DONE* must "catch" ASPERR_RECLAIM from SIO_reclaim (pC->xInp[zMI].hRxSio) -- note zMI only **
//               ?*? but what about ASPERR_RESYNC from same call ?*?
//            -> *for now, at least, pass on error from pP->fxns->updateInputStatus ()*
//            -> *DONE* must "catch" error from (zMI) dec->fxns->info() **
//         -> PAF_AST_decodeInfo1(): pass on any errors which occur here:
//            - pP->fxns->streamChainFunction (... PAF_ASP_CHAINFRAMEFXNS_RESET)
//            - enc->fxns->info()
//            - pP->fxns->setCheckRateX()
//            - pP->fxns->startOutput()
//            - "Start slave inputs if necessary"
//     INFO2 ("subsequent frame state")
//         -> PAF_AST_decodeInfo(): (see above)
//         -> PAF_AST_decodeInfo2(): pass on any errors which occur here:
//            - pP->fxns->setCheckRateX()
//     TIME ("timing state")
//         -> PAF_AST_decodeTime(): "Special timing consideations for AC-3"
//         -> performs SIO_issue (... PAF_SIO_REQUEST_FULLFRAME) & SIO_reclaim() *for zMI only*
//         -> now, DIB_issue [PAF_SIO_REQUEST_FULLFRAME] would only return SYS_EINVAL for "bad" internal error
//            (*OK* don't try to recover from this*)
//         -> much more likely would be SIO_reclaim() error (ASPERR_RECLAIM)
//         -> *DONE* must "catch" (just) ASPERR_RECLAIM error -- note zMI only,
//            possibly in PAF_AST_decodeProcessing() itself **
//     DECODE ("decode state")
//         -> PAF_AST_decodeDecode(): pass on error from
//            - PAF_SIO_CONTROL_GET_NUM_REMAINING ("bad" internal error)
//            - dec->fxns->reset()
//            - PAF_SIO_CONTROL_SET_PCMFRAMELENGTH
//         -> *DONE* must catch error from (zMI) dec->fxns->decode()
//         -> *?* must catch ASPERR_ISSUE from (zMI) SIO_issue()

Int
PAF_AST_decodeProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlgMaster)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int frame, block;
    Int errno;                          /* error number */
    Int getVal;
    enum { INIT, INFO1, AGAIN, INFO2, CONT, DECODE, STREAM, ENCODE, FINAL, QUIT } state;
    ALG_Handle alg[DECODEN_MAX];
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;
    Int resyncLoopCount, remainingSlaves;

    (void)as;   // avoid compiler warning:  This is used for debugging.
    (void)zMS;


    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS) {

        if (errno = SIO_idle (pC->xInp[zMI].hRxSio))
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA.%d continuous, master AS%d: returning errno 0x%x",
                                            __LINE__, zMI, errno));
            return errno;
        }


        // indicates (primary) input not running
        pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_NONE;

        // will be changed after next reclaim, to PAF_SOURCE_UNKNOWN or other
        pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_NONE;

        // in support of PAF_SIO_REQUEST_AUTO
        decAlgMaster = pC->xDec[zMD].decAlg[PAF_SOURCE_PCM];
    }

    if (! decAlgMaster)
    {
        TRACE_TERSE((&TR_MOD, "AS_InputA.%d !decAlgMaster returning ASPERR_ALGORITHM (0x%x)",
                                                    __LINE__, ASPERR_ALGORITHM));
        return ASPERR_ALGORITHM;
    }

    for (z=DECODE1; z < DECODEN; z++)
        alg[z] = pC->xDec[z].decAlg[PAF_SOURCE_PCM];
    alg[zMD] = decAlgMaster;

    //
    // Receive, process, and transmit the data in single-frame buffers
    //

    state = INIT;
    errno = 0; /* error number */

    for (;;) {

        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: sourceSelect == PAF_SOURCE_NONE", __LINE__));
            state = QUIT;
        }
            

        // Process commands (decode)

        if (getVal = pP->fxns->decodeCommand (pP, pQ, pC)) 
        {
            if (state != INIT)   // no need to restart/abort if not yet started
            {
                if (getVal == ASPERR_QUIT)
                {
                    state = QUIT;
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: state = QUIT", __LINE__));
                }
                else if (getVal == ASPERR_ABORT)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: return getVal", __LINE__));
                    return getVal;
                }
                else
                    /* ignore */;
            }
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: state == INIT", __LINE__));
        }

        // Process commands (encode)

        if (getVal = pP->fxns->encodeCommand (pP, pQ, pC)) {
            /* ignore */;
        }

        TRACE_TIME((&TIME_MOD,         "... + %d = %d ->", dtime2(), TSK_time()));
        TRACE_TIME((&TIME_MOD,         "                 state = %s", stateName[state]));

        // Process state (decode)

        switch (state) {

            case INIT: // first initial state

                // reset audio frame pointers to original values
                // (may be needed if error occured)
                for (z=STREAM1; z < STREAMN; z++) {
                    int ch;
                    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
                        if (pC->xStr[z].audioFrameChannelPointers[ch])
                            pC->xStr[z].audioFrameChannelPointers[ch] = pC->xStr[z].origAudioFrameChannelPointers[ch];
                    }

                    // (MID 1933) reset nChannels -- temp. changed for PCE2
///                    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
                }

                if (errno = pP->fxns->decodeInit (pP, pQ, pC, alg))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA.%d: INIT: errno 0x%x after decodeInit", __LINE__, errno));
                    break;
                }

                // initialize value (just in case)
                resyncLoopCount = 0;

                frame = 0;
                statStruct_UpdateFrameCount(STATSTRUCT_INPUT_A, frame, 1);
                block = 0;
                state = INFO1;
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: state: INIT->INFO", __LINE__));
                continue;

            case INFO1: // first frame state

                // Establish primary timing
                if (errno = pP->fxns->decodeInfo (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA.%d: INFO1: errno 0x%x after decodeInfo, primary timing", __LINE__, errno));
                    break;
                }

                // Don't start output untill major access unit is found.
                if( (pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_THD ||
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_DXP ||
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_DTSHD) &&
                                (pC->xStr[zMS].pAudioFrame->sampleRate == PAF_SAMPLERATE_UNKNOWN)) {
                    int z;
                    for (z=DECODE1; z < DECODEN; z++) {
                        Int zI = pP->inputsFromDecodes[z];
                        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) 
                        {
                            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: INFO1, SIO_issue", __LINE__));
                            if (SIO_issue (pC->xInp[zI].hRxSio, 
                                           &pC->xInp[zI].inpBufConfig,
                                           sizeof (pC->xInp[zI].inpBufConfig), 
                                           PAF_SIO_REQUEST_NEWFRAME))
                            {
                                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: INFO1, return (ASPERR_ISSUE)", __LINE__));
                                return (ASPERR_ISSUE);
                            }
                        }
                    }
                    frame++;
                    state = INFO1;
                    continue;
                }

                // Establish secondary timing
                if (errno = pP->fxns->decodeInfo1 (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: INFO1, errno 0x%x.  break after decodeInfo1", __LINE__, errno));
                    break;
                }

                state = DECODE;
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: state: INFO1->DECODE", __LINE__));
                continue;

            case AGAIN: // subsequent initial state

                block = 0;
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: state: AGAIN->INFO2", __LINE__));
                state = INFO2;
                continue;

            case INFO2: // subsequent frame state

                // Establish primary timing
                if (errno = pP->fxns->decodeInfo (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: INFO2 break on decodeInfo. errno 0x%x", __LINE__, errno));
                    break;
                }

                if (errno = pP->fxns->decodeInfo2 (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing. %d: INFO2 break on decodeInfo2. errno 0x%x", __LINE__, errno));
                    break;
                }

                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: INFO2->CONT", __LINE__));
                state = CONT;
                continue;

            case CONT: // subsequent block state

                remainingSlaves = 0x7FFFFFFF;

                if (pC->xInp[zMI].hRxSio)
                {   
                    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                        // primary input not currently running
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE) 
                    {
                        TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT", __LINE__));

                        for (z=DECODE1; z < DECODEN; z++) 
                        {
                            Int remaining;
                            Int zI = pP->inputsFromDecodes[z];
                            if (z == zMD
                                || ! pC->xInp[zI].hRxSio
                                || ! pC->xDec[z].decodeStatus.mode)
                            {
                                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT: continue", __LINE__));
                                continue;
                            }

                            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio, PAF_SIO_CONTROL_GET_NUM_REMAINING, (Arg )&remaining)) {
                                errno |= PAF_COMPONENT_IO;
                                {
                                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT: break", __LINE__));
                                    break;
                                }
                            }

                            if (remaining < remainingSlaves) // new minimum?
                                remainingSlaves = remaining;
                        }
                        remainingSlaves /= 2;  // adjust for two channels
                    }
                }   

                // Establish secondary timing
                if (errno = pP->fxns->decodeCont (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: decodeProcessing.%d: state: CONT: decodeCont returns errno 0x%x", __LINE__, errno));
                    break;
                }

                if (pC->xInp[zMI].hRxSio)
                {
                    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                        // primary input not currently running
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE) 
                    {

                        TRACE_TIME((&TIME_MOD, "AS_InputA: primary input not yet restarted -- remainingSlaves = %d, resyncLoopCount = %d", 
                                     remainingSlaves, resyncLoopCount));

                        // if remainingSlaves is (significantly) negative,
                        // need to wait until *next* block to re-sync timing
                        if (remainingSlaves >= -50) 
                        {
                            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT", __LINE__));
                            resyncLoopCount = 0;

                            // indicates primary input is (about to be) re-started
                            pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_UNKNOWN;

                            TRACE_TIME((&TIME_MOD, "AS_InputA: restarting (primary) input"));

                            if (errno = SIO_issue (pC->xInp[zMI].hRxSio,
                                                           &pC->xInp[zMI].inpBufConfig,
                                                           sizeof (pC->xInp[zMI].inpBufConfig),
                                                   PAF_SIO_REQUEST_AUTO)) 
                            {
                                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT.  SIO_issue err 0x%x", __LINE__, errno));
                                errno |= PAF_COMPONENT_IO;
                                break;
                            }
                        }
                        else if (++resyncLoopCount > 2) 
                        {
                            TRACE_TIME((&TIME_MOD, "AS_InputA: FAILED to restart (primary) input"));
                            errno = ASPERR_UNKNOWNSTATE;
                            errno |= PAF_COMPONENT_IO;
                            break;
                        }
                    } // DEC_MODE_CONTINUOUS && PAF_SOURCE_NONE
                }

                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT->DECODE", __LINE__));
                state = DECODE;
                continue;

            case DECODE: // decode state

                if (errno = pP->fxns->decodeDecode (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: CONT.  decodeDecode err 0x%x", __LINE__, errno));
                    break;
                }
#ifdef HSE
                // Hack for wma decoder to work. Needs to be discussed
                if( pC->xDec[0].decodeInStruct.pAudioFrame->sampleCount == 0)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: DECODE->AGAIN", __LINE__));
                    state = AGAIN;
                    continue;
                }
#endif
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: DECODE->STREAM", __LINE__));
                state = STREAM;
                continue;

            case STREAM: // stream state

                if (errno = pP->fxns->decodeStream (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: STREAM.  decodeStream err 0x%x", __LINE__, errno));
                    break;
                }

                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: STREAM->ENCODE", __LINE__));
                state = ENCODE;
                continue;

            case ENCODE: // encode state

                if (errno = pP->fxns->decodeEncode (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: ENCODE.  decodeEncode err 0x%x", __LINE__, errno));
                    break;
                }

                block++;

                // Check result to determine next state
                for (z=DECODE1; z < DECODEN; z++) 
                {
                    if (pC->xDec[z].decodeStatus.mode) 
                    {
                        if (pC->xDec[z].decodeOutStruct.errorFlag)
                            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing: %s: AS%d: decode warning (0x%02x)", 
                                          as+z, pC->xDec[z].decodeOutStruct.errorFlag));
                    }
                }

                // reset audio frame pointers (may have been adjusted by ARC or the like)
                for (z=STREAM1; z < STREAMN; z++) 
                {
                    int ch;
                    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
                        if (pC->xStr[z].audioFrameChannelPointers[ch])
                            pC->xStr[z].audioFrameChannelPointers[ch] = pC->xStr[z].origAudioFrameChannelPointers[ch];
                    }

                    // (MID 1933) reset nChannels -- temp. changed for PCE2
///                    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
                }

                state = pC->xDec[zMD].decodeOutStruct.outputFlag & 1 ? FINAL : CONT;
                if (state == FINAL)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: ENCODE->FINAL", __LINE__));
                }
                else
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: ENCODE->CONT", __LINE__));
                }
                continue;

            case FINAL: // frame-finalization state

                // Check for final frame, and if indicated:
                // - Update audio flag to cause output buffer flush rather than
                //   the default truncate in "decode complete" processing.
                // - Exit state machine to "decode complete" processing.

                if (!(pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS)) 
                {
                    if (pP->fxns->decodeFinalTest (pP, pQ, pC, alg, frame, block)) 
                    {
                        for (z=OUTPUT1; z < OUTPUTN; z++)
                        {
                            if ((pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_SOUND)
                            {
                                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: FINAL: SOUND -> QUIET", __LINE__));
                                pC->xOut[z].outBufStatus.audio++; // SOUND -> QUIET
                            }
                        }
                        break;
                    }
                }

                frame++;
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: FINAL->AGAIN", __LINE__));
                state = AGAIN;
                continue;

            case QUIT: // exit state

                // Quit:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.

                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: QUIT", __LINE__));
                errno = ASPERR_QUIT;
                break;

            default: // unknown state

                // Unknown:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.

                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeProcessing.%d: state: unknown, 0x%x", __LINE__, state));
                errno = ASPERR_UNKNOWNSTATE;
                break;

        }  // End of switch (state).

        TRACE_VERBOSE((&TR_MOD, "AS_InputA: Calling decode complete"));
        if (pP->fxns->decodeComplete (pP, pQ, pC, alg, frame, block))
            /* ignored? */;

        TRACE_TIME((&TIME_MOD, "AS_InputA: ... + %d = ?? (final? %d)", dtime2(), state == FINAL));

        return errno;
    }  // End of for (;;) to Receive, process, and transmit the data.
} //PAF_AST_decodeProcesing

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Common
//
//   Name:      PAF_AST_decodeInfo
//   Purpose:   Decoding Function for processing information in a manner that
//              is common for both initial and subsequent frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInfo (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/decode/stream counter */
    Int errno;                          /* error number */
    Int sioErr;                         /* error number, SIO */
    Int zD, zI, zS, zX;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    // Set decode control: sample rate, emphasis
    for (z=INPUT1; z < INPUTN; z++) {
        zD = z;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->inputsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        if (pC->xInp[z].hRxSio) {
            //determine associated decoder
            int rateO;
            rateO = (int)pP->pAudioFrameFunctions->sampleRateHz (NULL, pC->xInp[z].inpBufStatus.sampleRateStatus, PAF_SAMPLERATEHZ_STD);
            statStruct_LogSampleRate(STATSTRUCT_INPUT_A, rateO);

            if (pC->xInp[z].inpBufStatus.sampleRateStatus != pC->xDec[zD].decodeControl.sampleRate) {
                if (pC->xDec[zD].decodeControl.sampleRate == PAF_SAMPLERATE_UNKNOWN) {
                    pC->xDec[zD].decodeControl.sampleRate = pC->xInp[z].inpBufStatus.sampleRateStatus;
                }
                else
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA.%d: AS%d: return error ASPERR_INFO_RATECHANGE", __LINE__, as+pC->masterStr));
                    statStruct_LogFullSRateChange(STATSTRUCT_INPUT_A);
                    return (ASPERR_INFO_RATECHANGE);
            }
            }
            pC->xDec[zD].decodeControl.emphasis =
                pC->xDec[zD].decodeStatus.sourceDecode != PAF_SOURCE_PCM
                ? PAF_IEC_PREEMPHASIS_NO // fix for Mantis ID #119
                : pC->xInp[z].inpBufStatus.emphasisStatus;
        }
        else {
            pC->xDec[zD].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;
            pC->xDec[zD].decodeControl.emphasis = PAF_IEC_PREEMPHASIS_UNKNOWN;
        }
    }

    // Wait for info input
    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo: AS%d: awaiting frame %d -- sync+info+data", as+pC->masterStr, frame));

    if (pC->xInp[zMI].hRxSio) {
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
            // only perform reclaim if input really operating
            if (!(pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                  pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE)) {

                sioErr = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
                if (sioErr != sizeof (pC->xInp[zMI].inpBufConfig)) {
                    // only attempt error recovery for DIBERR_SYNC
                    if (sioErr != -DIBERR_SYNC ||
                        // attempted error recovery failed?
                        (errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, zMI, -sioErr)) )
                    {
                        TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_RECLAIM.  sioErr: 0x%x. errno 0x%x.", __LINE__, sioErr, errno));
                        return ASPERR_RECLAIM;
                }
                }
                else {
                    if (pC->xDec[zMD].decodeStatus.mode) {
                        if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
                                      (Arg )&pC->xDec[zMD].decodeStatus.sourceProgram))
                        {
                            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_INFO_PROGRAM.  errno 0x%x.", __LINE__, errno));
                            return ASPERR_INFO_PROGRAM;
                        }

                        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                            // actual source detected? (* relies on ordering of PAF_SOURCE_* *)
                            pC->xDec[zMD].decodeStatus.sourceProgram >= PAF_SOURCE_PCM)

                            // algorithm not supported for detected source type?
                            if ( (!(pC->xDec[zMD].decodeStatus.sourceProgram < pP->pDecAlgKey->length) ||
                                  (! pC->xDec[zMD].decAlg[pC->xDec[zMD].decodeStatus.sourceProgram])) &&
                                 // attempted error recovery failed?
                                 (errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, zMI, ASPERR_INFO_PROGRAM)) )
                            {
                                TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_INFO_PROGRAM.  errno 0x%x.", __LINE__, errno));
                                return ASPERR_INFO_PROGRAM;
                            }

                    }
                }
            }
        } //DEC_MODE_CONTINUOUS
        else {

            sioErr = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
            if (sioErr != sizeof (pC->xInp[zMI].inpBufConfig))
            {
                TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_RECLAIM.  sioErr: 0x%x. errno 0x%x.", __LINE__, sioErr, errno));
                return ASPERR_RECLAIM;
        }
        }
    } //pC->xInp[zMI].hRxSio

    // Decode info
    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;   // avoid compiler warning:  This is used for debugging.

        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {

            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeInfo: processing frame %d -- info", __LINE__, as+zS, frame));
            statStruct_UpdateFrameCount(STATSTRUCT_INPUT_A, frame, 0);

            if (errno = pP->fxns->updateInputStatus (pC->xInp[zI].hRxSio,
                                                     &pC->xInp[zI].inpBufStatus, &pC->xInp[zI].inpBufConfig))
            {
                TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error errno 0x%x.", __LINE__, errno));
                return errno;
            }
            if (dec->fxns->info
                && (errno = dec->fxns->info (dec, NULL,
                                             &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus))) {
                if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) 
                {
                    // if error recovery successful, retry info w/ PCM decoder w/ zero input
                    if ( !(errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, z, errno))) {
                        DEC_Handle dec = (DEC_Handle )decAlg[z];

                        if (dec->fxns->info
                            && (errno = dec->fxns->info (dec, NULL,
                                                         &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                        {
                            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error errno 0x%x.", __LINE__, errno));
                            return errno;
                        }
                    }
                }
                else
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error errno 0x%x.", __LINE__, errno));
                    return errno;
                }
            }
            // increment decoded frame count
            pC->xDec[z].decodeStatus.frameCount += 1;
        }
    } // z=DECODE1 to DECODEN

    // query IB for latest sourceProgram (needed if we started decoding due to a force mode)
    if (pC->xDec[zMD].decodeStatus.mode && (pC->xDec[zMD].decodeStatus.mode != DEC_MODE_CONTINUOUS)) {
        Int sourceProgram;
        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                              PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
                              (Arg )&sourceProgram))
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_AUTO_PROGRAM. errno 0x%x.", __LINE__, errno));
            return ASPERR_AUTO_PROGRAM;
        }
        pC->xDec[zMD].decodeStatus.sourceProgram = sourceProgram;
    }

    if ((DECODEN - DECODE1) > 1) // currently z14 supports only 1 stream for InputA -- see '#if 0' section below
    {
        TRACE_TERSE((&TR_MOD, "AS_InputA: > 1 stream not yet supported (return arbitrary error ASPERR_AUTO_PROGRAM)", __LINE__));
        return ASPERR_AUTO_PROGRAM; // same error as above
    }
#if 0 // Need to add this for more than one stream
    // Above does not determine sourceProgram for other than master decoder, added this to do others also. 2013-05-22
    for (z=DECODE1; z < DECODEN; z++)
    {
        if (z != zMD)
        {
            if (pC->xDec[z].decodeStatus.mode && (pC->xDec[z].decodeStatus.mode != DEC_MODE_CONTINUOUS))
            {
                Int sourceProgram;
                if (errno = SIO_ctrl (pC->xInp[z].hRxSio,
                                      PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
                                      (Arg )&sourceProgram))
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA.%d: return error ASPERR_AUTO_PROGRAM. errno 0x%x.", __LINE__, errno));
                    return ASPERR_AUTO_PROGRAM;
                }
                pC->xDec[z].decodeStatus.sourceProgram = sourceProgram;
            }
        }
    }
#endif

    // since now decoding update decode status for all enabled decoders
    for (z=DECODE1; z < DECODEN; z++) {
        if (pC->xDec[z].decodeStatus.mode && (pC->xDec[z].decodeStatus.mode != DEC_MODE_CONTINUOUS)) {
            pC->xDec[z].decodeStatus.sourceDecode = pC->xDec[z].decodeStatus.sourceProgram;
            if (pC->xDec[z].decodeStatus.sourceSelect == PAF_SOURCE_SNG)
                pC->xDec[z].decodeStatus.sourceDecode = PAF_SOURCE_SNG;
        }
    }

    // TODO: move this to start of this function so that it doesn't affect IO timing
    // Initialize audio frame(s)
    //    Re-initialize audio frame if there is an assocatiated decode and
    //    that decode doesn't have a valid input or is turned off
    for (z=STREAM1; z < STREAMN; z++) {
        Int reset = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                zI = pP->inputsFromDecodes[zX];
                if (!pC->xDec[zX].decodeStatus.mode || !pC->xInp[zI].hRxSio)
                    reset = 1;
            }
        }
        if (reset) {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo: AS%d: initializing block %d -- info", as+z, frame));
            pP->fxns->initFrame1 (pP, pQ, pC, z, 0);
        }
        else
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo: AS%d: initializing block %d -- info <ignored>", as+z, frame));
    }

    return 0;
} //PAF_AST_decodeInfo

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Initial
//
//   Name:      PAF_AST_decodeInfo1
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to initial frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInfo1 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zMD = pC->masterDec;
    double arcRatio; // KR032013
    (void)as;   // avoid compiler warning:  This is used for debugging.

    // init arcRatio -- before startOutput to prevent IO timing errors
    //           and -- before stream reset so ASPs get a good initial value for arcRatio
    // arcRatio = 1; // Make sure it is initialized else what if PAF_AST_computeRateRatio returns without updating arcRatio KR032013
    if (errno = PAF_AST_computeRateRatio (pP, pC, &arcRatio)) // KR032013
    {
        TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: PAF_AST_computeRateRatio returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    pP->fxns->controlRate(pC->xInp[0].hRxSio, pC->xOut[0].hTxSio, pC->acp, arcRatio); // computes inputsPerOutputQ24  KR032013

    if (errno = pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_RESET, 1, frame))
    {
        TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: streamChainFunction returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            if (enc->fxns->info
                && (errno = enc->fxns->info(enc, NULL,
                                            &pC->xEnc[z].encodeControl, &pC->xEnc[z].encodeStatus)))
            {
                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: info returns errno 0x%x ", __LINE__, errno));
                return errno;
            }
        }
    }


    if (errno = pP->fxns->setCheckRateX (pP, pQ, pC, 0))
    {
        // ignore if rateX has changed since we haven't, but are about to,
        // start the output. If we didn't ignore this case then the state machine
        // would restart unnecessarily, e.g. in the case of SRC, resulting in
        // added latency.
        if (errno != ASPERR_INFO_RATECHANGE)
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: setCheckRateX returns errno 0x%x, not RATECHANGE", __LINE__, errno));
            return errno;
    }
        else
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: RATECHANGE returns RATECHANGE, ignoring", __LINE__));
        }
    }

    if (errno = pP->fxns->startOutput (pP, pQ, pC, arcRatio)) // KR032013
    {
        if (errno == 0x105) {
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: startOutput returns RING BUFFER FULL (0x%x)", __LINE__, errno));
            statStruct_LogFullRing(STATSTRUCT_INPUT_A);
        }
        else
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: startOutput returns errno 0x%x", __LINE__, errno));
        }
        return errno;
    }

    // Start slave inputs if necessary
    for (z=DECODE1; z < DECODEN; z++) {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS;   // used for debugging
        if (z == zMD
            || ! pC->xInp[z].hRxSio
            || ! pC->xDec[z].decodeStatus.mode)
            continue;

        TRACE_TERSE((&TR_MOD, "AS_InputA: decodeInfo1.%d: About to request SIO SYNC[%d]",__LINE__, zI));
        if (errno = SIO_issue (pC->xInp[zI].hRxSio, &pC->xInp[zI].inpBufConfig,
                       sizeof (pC->xInp[zI].inpBufConfig), PAF_SIO_REQUEST_SYNC))  // sync only, not data
        {
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: SIO_REQUEST_SYNC returns errno 0x%x, we return ASPERR_ISSUE (0x%x)",
                            __LINE__, errno, ASPERR_ISSUE));
            return (ASPERR_ISSUE);
        }
        if ( (errno = SIO_reclaim (pC->xInp[zI].hRxSio, (Ptr )&pC->xInp[zI].pInpBuf, NULL))
            != sizeof (pC->xInp[zI].inpBufConfig))
        {   // will return OK because last call was REQUEST_SYNC
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo1.%d: SIO_reclaim returns wrong size (%d), we return ASPERR_RECLAIM (0x%x)",
                            __LINE__, errno, ASPERR_RECLAIM));
            return (ASPERR_RECLAIM);
        }

        TRACE_VERBOSE((&TR_MOD, "AS_InputA: decodeInfo1: AS%d: input started", as+zS));
    }

    return 0;
} //PAF_AST_decodeInfo1

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Subsequent
//
//   Name:      PAF_AST_decodeInfo2
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//

Int
PAF_AST_decodeInfo2 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;
    Int errno;
    Int inpNumEvents;
    Int outNumEvents;
    (void)zMS;

    // assume output for comparison is indexed same as master stream (zMS)
    // only perform this calculation if both input and output are active and
    // store in the master decoder (zMD) status
    if (pC->xInp[zMI].hRxSio && pC->xOut[zMS].hTxSio)
    {
        const Int oldMask = HWI_disable ();

        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUM_EVENTS, (Arg) &inpNumEvents)) 
        {
            errno |= PAF_COMPONENT_IO;
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo2.%d: SIO_ctrl errno 0x%x", __LINE__, errno));
            HWI_restore (oldMask);
            return errno;
        }

        if (errno = SIO_ctrl (pC->xOut[zMS].hTxSio, PAF_SIO_CONTROL_GET_NUM_EVENTS, (Arg) &outNumEvents))
        {
            errno |= PAF_COMPONENT_IO;
            TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo2.%d: SIO_ctrl errno 0x%x", __LINE__, errno));
            HWI_restore (oldMask);
            return errno;
        }

        HWI_restore (oldMask);

        pC->xDec[zMD].decodeStatus.bufferDrift = outNumEvents - inpNumEvents;
        TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo2: outNumEvents: %u,  inpNumEvents: %u.  bufferDrift: %d", 
                      outNumEvents, inpNumEvents, pC->xDec[zMD].decodeStatus.bufferDrift));
    }

    errno = pP->fxns->setCheckRateX (pP, pQ, pC, 1);
    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeInfo2.%d: return 0x%x", __LINE__, errno));
    return errno;
} //PAF_AST_decodeInfo2

// -----------------------------------------------------------------------------
// AST Decoding Function - Continuation Processing
//
//   Name:      PAF_AST_decodeCont
//   Purpose:   Decoding Function for processing that occurs subsequent to
//              information processing but antecedent to timing processing
//              for frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeCont (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int zI, zS;
    Int zMD = pC->masterDec;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    // Await slave inputs
    for (z=DECODE1; z < DECODEN; z++) {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;   // avoid compiler warning:  This is used for debugging.

        if (z == zMD
            || ! pC->xInp[zI].hRxSio
            || ! pC->xDec[z].decodeStatus.mode)
            continue;

        TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeCont: AS%d: awaiting frame %d -- data", as+zS, frame));
        if (SIO_reclaim (pC->xInp[zI].hRxSio, (Ptr )&pC->xInp[zI].pInpBuf, NULL)
            != sizeof (pC->xInp[zI].inpBufConfig))
            return (ASPERR_RECLAIM);
    }

    return 0;
} //PAF_AST_decodeCont

// -----------------------------------------------------------------------------
// AST Decoding Function - Decode Processing
//
//   Name:      PAF_AST_decodeDecode
//   Purpose:   Decoding Function for processing of input data by the
//              Decode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeDecode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/stream counter */
    Int errno;                          /* error number */
    Int ch;
    Int zMD = pC->masterDec;     // index of master decoder
    Int zMI = pP->zone.master;   // master input (DIB/DRI)
    Int remaining = 0x7FFFFFFF;
    Int shortenedFrame = 0;
    (void)as;   // avoid compiler warning:  This is used for debugging.


    // Clear samsiz for all channels - MID 208.
    for (z=STREAM1; z < STREAMN; z++) {
        for (ch=0; ch < PAF_MAXNUMCHAN_AF; ch++) {
            pC->xStr[z].pAudioFrame->data.samsiz[ch] = 0;
        }
    }

    if (pC->xInp[zMI].hRxSio &&
        pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
        // still performing auto-detection, i.e., still using PAF_SIO_REQUEST_AUTO?
        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN &&
        // actual source detected? (* relies on ordering of PAF_SOURCE_* *)
        pC->xDec[zMD].decodeStatus.sourceProgram >= PAF_SOURCE_PCM) {

        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUM_REMAINING, (Arg )&remaining))
        {
            // ** change to ASPERR_INFO_NUM_REMAINING, or similar **
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: SIO GET_NUM_REMAINING returns 0x%x", __LINE__, errno));
            return errno;
        }
        remaining /= 2;  // adjust for two channels

        // opportunity to adjust timing of secondary to better match primary?
        if ( FRAMELENGTH + MINFRAMELENGTH <= remaining && remaining < 2*FRAMELENGTH) {
            shortenedFrame = remaining - FRAMELENGTH;

            // assure adjusted framelength is multiple of 8 (ASP granularity)
            shortenedFrame /= 8;
            shortenedFrame *= 8;
            TRACE_TIME((&TIME_MOD, "AS_InputA: PAF_AST_decodeDecode: AS%d: adjustment frame length = %d", as+zMD, shortenedFrame));
        }
    }

    // Decode data
    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS;  // used for debugging

        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) 
        {
            // CJP
            // and I have sync
            // could put a valid bit in the PAF_AudioFrame

            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeDecode: processing block %d -- decode", __LINE__, as+zS, block));

            TRACE_VERBOSE((&TR_MOD, "AS_InputA: AS%d: decodeDecode: decoding from 0x%x (base) 0x%x (ptr)",
                           as+zS,
                           pC->xInp[z].pInpBuf->base.pVoid,
                           pC->xInp[z].pInpBuf->head.pVoid));

            if (dec->fxns->decode
                && (errno = dec->fxns->decode (dec, NULL,
                                               &pC->xDec[z].decodeInStruct, &pC->xDec[z].decodeOutStruct))) 
            {
                if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) 
                {
                    // if error recovery successful, retry decoding w/ PCM decoder w/ zero input
                    if ( !(errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, z, errno))) 
                    {
                        DEC_Handle dec = (DEC_Handle )decAlg[z];

                        if (dec->fxns->info
                            && (errno = dec->fxns->info (dec, NULL,
                                                         &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                        {
                            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: fxns->info returns 0x%x", __LINE__, errno));
                            return errno;
                        }
                        if (dec->fxns->decode
                            && (errno = dec->fxns->decode (dec, NULL,
                                                           &pC->xDec[z].decodeInStruct, &pC->xDec[z].decodeOutStruct)))
                        {
                            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: fxns->decode returns 0x%x", __LINE__, errno));

                            return errno;
                        }
                    }
                }
                else // not continuous
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: fxns->decode returns 0x%x", __LINE__, errno));
                    return errno;
                }
            }


#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
 {
     PAF_AudioFrame *pAudioFrame = pC->xDec[z].decodeInStruct.pAudioFrame;
     int *wp;
     wp = (int*)pAudioFrame->data.sample[0];
     TRACE_DATA((&TR_MOD, "AS_InputA: AS%d decodeDecode: decoded to ch 0 0x%x. line %d", z, wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch0)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[1];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 1 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch1)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[2];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 2 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch2)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[8];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 8 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch8)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[9];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 9 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch9)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[10];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 10 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch10)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[11];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 11 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch11)", wp[0], wp[1], wp[2]));
     wp = (int*)pAudioFrame->data.sample[12];
     TRACE_DATA((&TR_MOD, "AS_InputA: decodeDecode: decoded to ch 12 0x%x. line %d", wp, __LINE__));
     TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch12)", wp[0], wp[1], wp[2]));
 }
#endif


            if (pC->xDec[z].decodeOutStruct.outputFlag & 1) 
            {
                Int frameLength;

                frameLength = pP->fxns->computeFrameLength (decAlg[z],
                                                            FRAMELENGTH, pC->xDec[z].decodeStatus.bufferRatio);

// ............................................................................

                if (shortenedFrame > 0)
                    frameLength = shortenedFrame;
                else if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                         // per above calculations, this will only be true if ready for decoder change
                         remaining < FRAMELENGTH + MINFRAMELENGTH) {
                    DEC_Handle dec;

                    if (decAlg[z]->fxns->algDeactivate)
                        decAlg[z]->fxns->algDeactivate (decAlg[z]);

                    pC->xDec[z].decodeStatus.sourceDecode = pC->xDec[z].decodeStatus.sourceProgram;

                    decAlg[z] = pC->xDec[z].decAlg[pC->xDec[z].decodeStatus.sourceDecode];
                    dec = (DEC_Handle )decAlg[z];

                    TRACE_TIME((&TIME_MOD, "AS_InputA: PAF_AST_decodeDecode: AS%d: changing decoder", as+z));

                    if (decAlg[z]->fxns->algActivate)
                        decAlg[z]->fxns->algActivate (decAlg[z]);
                    if (dec->fxns->reset
                        && (errno = dec->fxns->reset (dec, NULL,
                                                      &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                    {
                        TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: fxns->reset returns 0x%x", __LINE__, errno));
                        return errno;
                    }
                }

// ............................................................................

                pC->xDec[z].decodeControl.frameLength = frameLength;
                pC->xDec[z].decodeInStruct.sampleCount = frameLength;
                if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                      PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: SIO SET_PCMFRAMELENGTH returns 0x%x", __LINE__, errno));
                    return errno;
                }

                // only perform issue if input really operating
                // if not, must wait until appropriate time to restart
                if ( !(pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                       // assures initial reclaim
                       pC->xDec[z].decodeStatus.sourceProgram == PAF_SOURCE_NONE) )
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: calling SIO_issue[%d]", __LINE__, zI));
                    if (errno = SIO_issue (pC->xInp[zI].hRxSio, &pC->xInp[zI].inpBufConfig,
                                   sizeof (pC->xInp[zI].inpBufConfig),
                                   pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                                   pC->xDec[z].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN ?
                                   PAF_SIO_REQUEST_AUTO :
                                   PAF_SIO_REQUEST_NEWFRAME))
                    {
                        TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode.%d: SIO_issue returns 0x%x, we return ASPERR_ISSUE (0x%x)", __LINE__, errno, ASPERR_ISSUE));
                        return (ASPERR_ISSUE);
                    }
                }
            } // outputFlag & 1
        } // hRxSio && decodeStatus.mode
        else
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeDecode: processing block %d -- decode <ignored>", __LINE__, as+zS, block));
    } // z=DECODE1 to DECODEN

    // Set up audio frames not decoded into
    //    Re-initialize audio frame if there is an assocatiated decode and
    //    that decode doesn't have a valid input or is turned off
    for (z=STREAM1; z < STREAMN; z++) {
        Int zX;
        Int reset = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                Int zI = pP->inputsFromDecodes[zX];
                if (!pC->xDec[zX].decodeStatus.mode || !pC->xInp[zI].hRxSio)
                    reset = 1;
            }
        }
        if (reset) {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode: AS%d: initializing block %d -- decode", as+z, frame));
            pP->fxns->initFrame1 (pP, pQ, pC, z, 0);
        }
        else
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_decodeDecode: AS%d: initializing block %d -- decode <ignored>", as+z, frame));
    }
    return 0;
} //PAF_AST_decodeDecode

// -----------------------------------------------------------------------------
// AST Decoding Function - Stream Processing
//
//   Name:      PAF_AST_decodeStream
//   Purpose:   Decoding Function for processing of audio frame data by the
//              ASP Algorithms.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent/child.
//

Int
PAF_AST_decodeStream (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int errno;
    double arcRatio;  // KR032013

    // get latest arc ratio before applying stream
    if (errno = PAF_AST_computeRateRatio (pP, pC, &arcRatio))
        return errno;

    // indirectly conditioned on "PAF_FST == 4"
    if (pC->xInp[0].inpBufStatus.rateTrackMode
     && pC->xOut[0].outBufStatus.rateTrackMode)
        if (pP->fxns->controlRate
            && (errno = pP->fxns->controlRate (pC->xInp[0].hRxSio, pC->xOut[0].hTxSio, pC->acp, arcRatio)) )
            return errno;

    return pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_APPLY, 1, block);
} //PAF_AST_decodeStream

// -----------------------------------------------------------------------------
// AST Decoding Function - Encode Processing
//
//   Name:      PAF_AST_decodeEncode
//   Purpose:   Decoding Function for processing of audio frame data by the
//              Encode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeEncode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* encode/output counter */
    Int errno;                          /* error number */
    Int zX, zE, zS;
    (void)as;   // avoid compiler warning:  This is used for debugging.


    // Await output buffers (but not first time)
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        // determine encoder associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];
        (void)zS;

        if (pC->xOut[z].hTxSio) {
            // update length (e.g. ARC may have changed)
            pC->xOut[z].outBufConfig.lengthofFrame = pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- idle", __LINE__, as+zS, block));
            errno = SIO_reclaim (pC->xOut[z].hTxSio,(Ptr *) &pC->xOut[z].pOutBuf, NULL);
            if (errno < 0 ) 
            {
                SIO_idle (pC->xOut[z].hTxSio);
                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: AS%d: SIO_reclaim returns error %d", as+zS, -errno));
                return -errno; // SIO negates error codes
            }
        }
        else {
            TRACE_GEN((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode.%d: AS%d: processing block %d -- idle <ignored>", __LINE__, as+zS, block));
        }
    }

    // Encode data
    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        Int zS = pP->streamsFromEncodes[z];   (void)zS;
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            if(select != pC->xEnc[z].encodeControl.encActive){
                pC->xEnc[z].encodeControl.encActive = select;
                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: return error line %d", __LINE__));
                return (-1);
            }
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- encode", __LINE__, as+zS, block));

            // (MID 1933) temp. workaround for PCE2
            pC->xEnc[z].encodeInStruct.pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;

          #if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
            {
                PAF_AudioFrame *pAudioFrame = pC->xEnc[z].encodeInStruct.pAudioFrame;
                int *wp;
                wp = (int*)pAudioFrame->data.sample[0];
                TRACE_DATA((&TR_MOD, "AS_InputA: AS%d: PAF_AST_decodeEncode: encoding from ch 0 0x%x. line %d", z, wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch0)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[1];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 1 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch1)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[2];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 2 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch2)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[8];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 8 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch8)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[9];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 9 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch9)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[10];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 10 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch10)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[11];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 11 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch11)", wp[0], wp[1], wp[2]));
                wp = (int*)pAudioFrame->data.sample[12];
                TRACE_DATA((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: encoding from ch 12 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "AS_InputA: [0]: 0x%x, [1]: 0x%x, [2]: 0x%x (ch12)", wp[0], wp[1], wp[2]));
            }
          #endif

            if (enc->fxns->encode)
            {
                if  (errno = enc->fxns->encode (enc, NULL, &pC->xEnc[z].encodeInStruct, &pC->xEnc[z].encodeOutStruct))
                {
                    if (errno != PCEERR_OUTPUT_POINTERNULL)
                    {
                        TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: return error %d line %d", errno, __LINE__));
                        return errno;
                    }
                }
              #if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
                else
                {
                    int *wp = (int*)pC->xOut[z].pOutBuf->pntr.pVoid;
                    TRACE_DATA((&TR_MOD, "AS_InputA: AS%d: PAF_AST_decodeEncode: encoded to 0x%x. line %d", z, wp, __LINE__));
                    TRACE_DATA((&TR_MOD, "AS_InputA: %[0]: 0x%x, [16]: 0x%x, [99]: 0x%x", wp[0], wp[16], wp[99]));
                }
              #endif
            }
        }
        else {
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- encode <ignored>",
            		__LINE__, as+pP->streamsFromEncodes[z], block));
        }
    }

    // Transmit data
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        Int zS;
        // determine encoder associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];
        (void)zS;   // avoid compiler warning:  This is used for debugging.

        if (pC->xOut[z].hTxSio) {
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- output", __LINE__, as+zS, block));
            errno = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof (pC->xOut[z].outBufConfig), 0);
            if (errno)
            {
                SIO_idle (pC->xOut[z].hTxSio);
                if (errno == 0x105)     // 0x105 == RINGIO_EBUFFULL
                {
                    statStruct_LogFullRing(STATSTRUCT_INPUT_A);
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode.%d: SIO_idle returned RINGIO_EBUFFULL (0x%x)", __LINE__, errno));
                }

                if (errno > 0)
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: return error 0x%x line %d", errno, __LINE__));
                    return (ASPERR_ISSUE + (z << 4));
                }
                else if (errno < 0)
                {
                    TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_decodeEncode: return error neg 0x%x line %d", -errno, __LINE__));
                    return -errno; // SIO negates error codes
                }
            }
        }
        else {
            TRACE_GEN((&TR_MOD, "AS_InputA.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- output <ignored>", __LINE__, as+zS, block));
        }
    }

    return 0;
} //PAF_AST_decodeEncode

// -----------------------------------------------------------------------------
// AST Selection Function - Device Selection
//
//   Name:      PAF_AST_selectDevices
//   Purpose:   Audio Stream Task Function for selecting the devices used
//              for input and output.
//   From:      AS_InputA_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_selectDevices (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/output counter */
    Int errno = 0;                      /* error number */
    Int errme;                          /* error number, local */
    Int device;
    Int zMD = pC->masterDec;
    (void)as;   // avoid compiler warning:  This is used for debugging.


    // Select output devices
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if ((device = pC->xOut[z].outBufStatus.sioSelect) >= 0) {
            TRACE_GEN((&TR_MOD, "AS_InputA: PAF_AST_selectDevices: AS%d: output device %d selecting ...", as+z, device));

            /* check for valid index into device array */
            if (device >= pQ->devout->n)
                device = 0; /* treat as device None */

            errme = pP->fxns->deviceSelect (&pC->xOut[z].hTxSio, SIO_OUTPUT, HEAP_OUTBUF, (Ptr )pQ->devout->x[device]);
            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                if (! errno)
                    errno = ASPERR_DEVOUT + errme;
                pC->xOut[z].outBufStatus.sioSelect = 0x80;
            }
            else {
                Int zE;

                pC->xOut[z].outBufStatus.sioSelect = device | 0x80;
                // register outBufStatus and encodeStatus pointers with output devices
                // This enables proper IEC encapsulation.
                if (pC->xOut[z].hTxSio) {
                    // set max # of output buffers (use override if necessary)
                    if (pC->xOut[z].outBufStatus.maxNumBufOverride == 0)
                        SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_MAX_NUMBUF,(Arg) pP->poutNumBufMap[z]->maxNumBuf);
                    else
                        SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_MAX_NUMBUF,(Arg) pC->xOut[z].outBufStatus.maxNumBufOverride);

                    // register PAF_SIO_IALG object address
                    SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_IALGADDR,(Arg) pC->xOut[z].outChainData.head->alg);
                    SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_BUFSTATUSADDR, (Arg) &pC->xOut[z].outBufStatus);
                    for (zE=ENCODE1; zE < ENCODEN; zE++) {
                        if (pP->outputsFromEncodes[zE] == z) {
                            SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_ENCSTATUSADDR, (Arg) &pC->xEnc[zE].encodeStatus);
                            break;
                        }
                    }
                }
            }
        }

        // if device selected and valid then enable stat tracking if
        // required and start clocking
        if ((pC->xOut[z].outBufStatus.sioSelect < 0) && (pC->xOut[z].hTxSio)) {
            // enable status tracking (needed for ARC)
            if (pC->xOut[z].outBufStatus.rateTrackMode) {
                Int outputRate = pC->xOut[z].outBufStatus.sampleRate;
                float rateO = pP->pAudioFrameFunctions->sampleRateHz (NULL, outputRate, PAF_SAMPLERATEHZ_STD);
                errno = SIO_ctrl (pC->xOut[z].hTxSio, (Uns) PAF_SIO_CONTROL_ENABLE_STATS, rateO);
                if (errno)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d, errno 0x%x", __LINE__, errno));
                    return errno;
                }

            }
            errme = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_OUTPUT_START_CLOCKS, 0);
            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                SIO_idle (pC->xOut[z].hTxSio);
                if (!errno)
                    errno = ASPERR_DEVOUT + errme;
            }
        }
    }

    // Select input devices
    for (z=INPUT1; z < INPUTN; z++) {
        if ((device = pC->xInp[z].inpBufStatus.sioSelect) >= 0) {
            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices: AS%d: input device %d selecting ...", as+z, device));

            // check for valid index into device array
            if (device >= pQ->devinp->n)
                device = 0; /* treat as device None */

            errme = pP->fxns->deviceSelect (&pC->xInp[z].hRxSio, SIO_INPUT,
                                            HEAP_INPBUF, (Ptr )pQ->devinp->x[device]);

            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                if (! errno)
                    errno = ASPERR_DEVINP + errme;
                pC->xInp[z].inpBufStatus.sioSelect = 0x80;
            }
            else {
                pC->xInp[z].inpBufStatus.sioSelect = device | 0x80;
                // register decodeStatus pointer with input devices
                // This allows, e.g., autoProcessing to exit when sourceSelect = none
                // Use zMIs decodeStatus for all inputs
                if (pC->xInp[z].hRxSio) {
                    // register PAF_SIO_IALG object address
                    SIO_ctrl (pC->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_IALGADDR,(Arg) pC->xInp[z].inpChainData.head->alg);
                    SIO_ctrl (pC->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_DECSTATUSADDR, (Arg) &pC->xDec[zMD].decodeStatus);
                }
            }
        }

        // enable status tracking (needed for ARC)
        if (pC->xInp[z].inpBufStatus.rateTrackMode && (pC->xInp[z].inpBufStatus.sioSelect < 0) && (pC->xInp[z].hRxSio)) {
                Int inputRate;
                float rateI;
                errno = pP->fxns->updateInputStatus (pC->xInp[z].hRxSio,&pC->xInp[z].inpBufStatus, &pC->xInp[z].inpBufConfig);
                if (errno)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d: errno 0x%x", __LINE__, errno));
                    return errno;
                }
                inputRate = pC->xInp[z].inpBufStatus.sampleRateStatus;
                rateI = pP->pAudioFrameFunctions->sampleRateHz (NULL, inputRate, PAF_SAMPLERATEHZ_STD);
                errno = SIO_ctrl (pC->xInp[z].hRxSio, (Uns) PAF_SIO_CONTROL_ENABLE_STATS, rateI);
                if (errno)
                {
                    TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_selectDevices.%d: errno 0x%x", __LINE__, errno));
                    return errno;
        }
    }
    }

    return errno;
} //PAF_AST_selectDevices

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Start
//
//   Name:      PAF_AST_startOutput
//   Purpose:   Decoding Function for initiating output.
//   From:      AST Parameter Function -> decodeInfo1
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * SIO control errors.
//
#define DEC_OUTNUMBUF_MAP(X) \
      pP->poutNumBufMap[z]->map[(X) >= pP->poutNumBufMap[z]->length ? 0 : (X)]

Int
PAF_AST_startOutput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, double arcRatio) // KR032013
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* output counter */
    Int errno,nbufs;                    /* error number */
    Int zE, zS, zX;
    Int zMD = pC->masterDec;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio) {
            // determine associated encoder and stream
            zE = z;
            zS = z;     (void)zS;   // avoid compiler warning:  This is used for debugging.
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zE = zX;
                    zS = pP->streamsFromEncodes[zE];
                    break;
                }
            }
            (void) zS;  // avoid compiler warning.

            // Set sample count so that DOB knows how much data to send

            // if estimating output rate then assume be need the initial estimate
            // of framelength needs to be close, otherwise there will be an IO error
            // shortly after startOutput. Specifically, we start the output with
            // two buffers with size based on the arc ratio seed.
            if (pC->xOut[z].outBufStatus.rateTrackMode) {
                pC->xOut[z].outBufConfig.lengthofFrame =
                    pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount * arcRatio;
            }
            else {
                pC->xOut[z].outBufConfig.lengthofFrame =
                    pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;
            }

            if (pC->xOut[z].outBufStatus.markerMode == PAF_OB_MARKER_ENABLED) {
                pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
                pAlgConfig = &pObj->config;
                memset (pC->xOut[z].outBufConfig.base.pVoid, 0xAA, pAlgConfig->pMemRec[0].size);
            }

            // The index to DEC_OUTNUMBUF_MAP will always come from the primary/master
            // decoder. How should we handle the sourceProgram for multiple decoders?
            // Override as needed
            nbufs = DEC_OUTNUMBUF_MAP(pC->xDec[zMD].decodeStatus.sourceProgram);
            if (pC->xOut[z].outBufStatus.numBufOverride[pC->xDec[zMD].decodeStatus.sourceProgram] > 0)
                nbufs = pC->xOut[z].outBufStatus.numBufOverride[pC->xDec[zMD].decodeStatus.sourceProgram];
            SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_NUMBUF, nbufs);

            if (errno = SIO_issue (pC->xOut[z].hTxSio,
                                   &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0)) {
                SIO_idle (pC->xOut[z].hTxSio);
                return errno;
            }

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (errno =  SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_UNMUTE, 0))) {
                errno = (errno & 0xff) | ASPERR_MUTE;
                /* convert to sensical errno */
                TRACE_TERSE((&TR_MOD, "AS_InputA: PAF_AST_startOutput: AS%d: SIO control failed (unmute)", as+zS));
                return (errno);
            }
            else
                pC->xOut[z].outBufStatus.audio
                    = (pC->xOut[z].outBufStatus.audio & 0xf0) | PAF_OB_AUDIO_SOUND;

            TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_startOutput: AS%d: output started", as+zS ));
        }
    }

    return 0;
} //PAF_AST_startOutput

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Stop
//
//   Name:      PAF_AST_stopOutput
//   Purpose:   Decoding Function for terminating output.
//   From:      AST Parameter Function -> decodeProcessing
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * SIO control errors.
//

Int
PAF_AST_stopOutput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* output counter */
    Int errno = 0, getVal;
    Int zS, zX;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio) {
            // determine associated encoder and stream
            zS = z;     (void)zS;   // avoid compiler warning:  This is used for debugging.
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zS = pP->streamsFromEncodes[zX];
                    break;
                }
            }

            // Mute output before audio data termination in the usual case,
            // where such termination is due to decode error or user command.
            // Identification of this as the usual case is provided by the
            // "decode processing" state machine.

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_SOUND
                && (getVal = SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_MUTE, 0))) {
                if (! errno) {
                    errno = (getVal & 0xff) | ASPERR_MUTE;
                    /* convert to sensical errno */
                }
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_stopOutput:  AS%d: SIO control failed (mute)", as+zS));
            }

            TRACE_TIME((&TIME_MOD, "... + %d = %d (stopOutput -- begin PAF_SIO_CONTROL_IDLE)", dtime2(), TSK_time()));
            // Terminate audio data output, truncating (ignore) or flushing
            // (play out) final samples as per (1) control register set by
            // the user and (2) the type of audio data termination:

#if 0
            // This form is not used because driver support for truncating
            // data is not supported for internal clocks, although it is
            // for external clocks.
            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_IDLE,
                               pC->xOut[z].outBufStatus.flush
                               & (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                               ? 1 : 0);
            /* UNTESTED */
#else
            // This form should be used when driver support for truncating
            // data is supported for both internal and external clocks.
            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_IDLE,
                               pC->xOut[z].outBufStatus.flush ? 1 :
                               (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                               ? 1 : 0);
            /* TESTED */
#endif

            TRACE_TIME((&TIME_MOD, "... + %d = %d (stopOutput -- after PAF_SIO_CONTROL_IDLE)", dtime2(), TSK_time()));

            if (! errno)
                errno = getVal;

            // Mute output after audio data termination in a special case,
            // where such termination is due to processing of a final frame
            // or user command. Identification of this as a special case is
            // provided by the "decode processing" state machine.

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                && (getVal = SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_MUTE, 0))) {
                if (! errno) {
                    errno = (getVal & 0xff) | ASPERR_MUTE;
                    /* convert to sensical errno */
                }
                TRACE_VERBOSE((&TR_MOD, "AS_InputA: PAF_AST_stopOutput:  AS%d: SIO control failed (mute)", as+zS));
            }

            pC->xOut[z].outBufStatus.audio &= ~0x0f;

            // zero output buffers
            pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
            pAlgConfig = &pObj->config;
            memset (pC->xOut[z].outBufConfig.base.pVoid, 0, pAlgConfig->pMemRec[0].size);
        } //pC->xOut[z].hTxSio
    }//OUTPUT

    return (errno);
} //PAF_AST_stopOutput

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Change
//
//   Name:      PAF_AST_setCheckRateX
//   Purpose:   Decoding Function for reinitiating output.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeInfo2
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//

/* 0: set, 1: check, unused for now. --Kurt */
Int
PAF_AST_setCheckRateX (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int check)
{
    float rateX;
    PAF_SampleRateHz rateO /* std */, rateI /* inv */;
    Int z;                              /* output counter */
    Int zx;                             /* output re-counter */
    Int getVal;
    int inputRate, inputCount, outputRate, outputCount;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zE, zX;


    inputRate = pC->xInp[zMI].inpBufStatus.sampleRateStatus;
    inputCount = pC->xDec[zMD].decodeStatus.frameLength;
    rateI = pC->xStr[pC->masterStr].pAudioFrame->fxns->sampleRateHz
        (pC->xStr[pC->masterStr].pAudioFrame, inputRate, PAF_SAMPLERATEHZ_INV);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio && (pC->xOut[z].outBufStatus.clock & 0x01)) {

            // determine associated encodr
            zE = z;
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zE = zX;
                    break;
                }
            }

            outputRate = pC->xEnc[zE].encodeStatus.sampleRate;
            outputCount = pC->xEnc[zE].encodeStatus.frameLength;
            rateO = pC->xStr[pC->masterStr].pAudioFrame->fxns->sampleRateHz
                (pC->xStr[pC->masterStr].pAudioFrame, outputRate, PAF_SAMPLERATEHZ_STD);
            if (rateI > 0 && rateO > 0)
                rateX = rateO /* std */ * rateI /* inv */;
            else if (inputCount != 0)
                rateX = (float )outputCount / inputCount;
            else
                return ( ASPERR_INFO_RATERATIO );

            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_RATEX, (Arg) &rateX);
            if (getVal == DOBERR_RATECHANGE) {
                for (zx=OUTPUT1; zx < OUTPUTN; zx++)
                    if (pC->xOut[zx].hTxSio)
                        SIO_idle (pC->xOut[zx].hTxSio);

                // this forces an exit from the calling state machine which will
                // eventually call startOutput which calls setCheckRateX for all outputs
                // and so it is ok, in the presence of a rate change on any output, to
                // exit this loop /function early.
                return ASPERR_INFO_RATECHANGE;
            }
            else if( getVal != SYS_OK )
                return ((getVal & 0xff) | ASPERR_RATE_CHECK);
        }
    }

    return (0);
} //PAF_AST_setCheckRateX

// -----------------------------------------------------------------------------
// TODO: indices for need to be generalized.
// This tracing code should be conditional.  Are these taking 16k of on chip memory?

#define AS_INPUTA_DV_START_LOG_SIZE     256
#define AS_INPUTA_DV_LOG_SIZE           256

#pragma DATA_SECTION (AS_INPUTA_TX_DV_START_LOG, ".text:AS_INPUTA_TX_DV_START_LOG")
float AS_INPUTA_TX_DV_START_LOG[AS_INPUTA_DV_START_LOG_SIZE];
#pragma DATA_SECTION (AS_INPUTA_RX_DV_START_LOG, ".text:AS_INPUTA_RX_DV_START_LOG")
float AS_INPUTA_RX_DV_START_LOG[AS_INPUTA_DV_START_LOG_SIZE];
Int AS_INPUTA_DV_START_LOG_index=0;

#pragma DATA_SECTION (AS_INPUTA_TX_DV_LOG, ".text:AS_INPUTA_TX_DV_LOG")
float AS_INPUTA_TX_DV_LOG[AS_INPUTA_DV_LOG_SIZE];
#pragma DATA_SECTION (AS_INPUTA_RX_DV_LOG, ".text:AS_INPUTA_RX_DV_LOG")
float AS_INPUTA_RX_DV_LOG[AS_INPUTA_DV_LOG_SIZE];
Int AS_INPUTA_DV_LOG_index=0;

// global populated by output driver specifying measured output rate.
extern double gAS_Output_tx_dv;

Int
PAF_AST_computeRateRatio (const PAF_AST_Params *pP, PAF_AST_Config *pC, double *arcRatio)
{
    PAF_SIO_Stats *pRxStats, *pTxStats;
    Int errno;
    double arcRatio1;
    double tx_dv;

    arcRatio1 = 1;         // KR032013
    *arcRatio = arcRatio1; // KR032013
                           
#ifndef ASYNC_DUAL_ZAA
    // ARC is only used here in dual async mode.
    return 0;
#endif

    // need valid rx and tx and both need to have stats tracking enabled
    if (!pC->xInp[0].hRxSio || !pC->xOut[0].hTxSio)
        return 0;
    if (!pC->xInp[0].inpBufStatus.rateTrackMode || !pC->xOut[0].outBufStatus.rateTrackMode)
        return 0;

    errno = SIO_ctrl (pC->xInp[0].hRxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pRxStats);
    if (errno) {
        TRACE_TERSE((&TR_MOD, "AS_InputA: Error 0x%x, retrieving Rx xfer stats", errno));
        return errno;
    }

    errno = SIO_ctrl (pC->xOut[0].hTxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pTxStats);
    if (errno) {
        TRACE_TERSE((&TR_MOD, "AS_InputA: Error 0x%x, retrieving Tx xfer stats", errno));
        return errno;
    }
#ifdef Z_TOP
    // global set in AS_out.
    tx_dv = gAS_Output_tx_dv;	// CJP
#else
    tx_dv = pTxStats->dpll.dv;
#endif
    if ((pRxStats->dpll.dv == 0) || (tx_dv == 0)) {
    	// startup case
        // TODO: loop over all inputs?
        // we use the non-stream specific pAudioFrameFunctions since all streams are,
        // currently initialized with this value. (see PAF_AST_initFrame0)
        // Also, sampleRateHz ignores the audio frame pointer
        Int inputRate = pC->xInp[0].inpBufStatus.sampleRateStatus;
        float rateI = pP->pAudioFrameFunctions->sampleRateHz (NULL, inputRate, PAF_SAMPLERATEHZ_STD);
        Int outputRate = pC->xOut[0].outBufStatus.sampleRate;
        float rateO = pP->pAudioFrameFunctions->sampleRateHz (NULL, outputRate, PAF_SAMPLERATEHZ_STD);
        arcRatio1 = rateO / rateI;                          // KR032013
    }
    else
        arcRatio1 = pRxStats->dpll.dv / tx_dv;  // KR032013

    *arcRatio = arcRatio1;
    statStruct_LogArcRatio(STATSTRUCT_INPUT_A, arcRatio1);

    // trace buffers
    if (AS_INPUTA_DV_START_LOG_index<AS_INPUTA_DV_START_LOG_SIZE) {
        AS_INPUTA_RX_DV_START_LOG[AS_INPUTA_DV_START_LOG_index  ]=(float)pRxStats->dpll.dv;
        AS_INPUTA_TX_DV_START_LOG[AS_INPUTA_DV_START_LOG_index++]=(float)tx_dv;
    }
    if (AS_INPUTA_DV_LOG_index==AS_INPUTA_DV_LOG_SIZE) AS_INPUTA_DV_LOG_index=0;
    AS_INPUTA_RX_DV_LOG[AS_INPUTA_DV_LOG_index  ]=(float)pRxStats->dpll.dv;
    AS_INPUTA_TX_DV_LOG[AS_INPUTA_DV_LOG_index++]=(float)tx_dv;

    if ((arcRatio1 > MAX_ARC_RATIO_A) || (arcRatio1 < MIN_ARC_RATIO_A))
    {   // Input of 32k means 1.5.  Input of 96k means 0.5.  
        // Limit in other cases.

        int scaled_tx_dv = tx_dv * 1.0e12;

        TRACE_TERSE((&TR_MOD, "AS_InputA: arcRatio has gone wild. %d.%07d (%d)", 
                     (int) arcRatio1, (int) (1.e7 * (arcRatio1 - (int) arcRatio1)), scaled_tx_dv));
        *arcRatio = arcRatio1 = 1.0;
    }
    else
    {
        int scaled_tx_dv = tx_dv * 1.0e12;
        TRACE_TERSE((&TR_MOD, "AS_InputA: arcRatio1 %d.%07d (%d)", 
                   (int) arcRatio1, (int) (1.e7 * (arcRatio1 - (int) arcRatio1)), scaled_tx_dv));
    }

    return 0;
} //PAF_AST_computeRateRatio

// .............................................................................
// Audio Stream Task Parameter Functions - PA17
//
//   Name:      PAF_INA_params_fxnsPA17
//   Purpose:   Collect the functions that embody the implementation of
//              Audio Framework Number 2 for use as a jump table.
//   From:      PAF_AST_Params
//   Uses:      See contents.
//   States:    N.A.
//   Return:    N.A.
//   Trace:     None.
//

const PAF_AST_Fxns PAF_INA_params_fxnsPA17 =
{
    {
        PAF_AST_initPhaseMalloc,
        PAF_AST_initPhaseConfig,
        PAF_AST_initPhaseAcpAlg,
        PAF_AST_initPhaseCommon,
        PAF_AST_initPhaseAlgKey,
        PAF_AST_initPhaseDevice,
        NULL,
        NULL,
    },
    PAF_AST_initFrame0,
    PAF_AST_initFrame1,
    NULL, /* PAF_AST_passProcessing, */
    NULL, /* PAF_AST_passProcessingCopy, */
    PAF_AST_autoProcessing,
    PAF_AST_decodeProcessing,
    PAF_AST_decodeCommand,
    PAF_AST_encodeCommand,
    PAF_AST_decodeInit,
    PAF_AST_decodeInfo,
    PAF_AST_decodeInfo1,
    PAF_AST_decodeInfo2,
    PAF_AST_decodeCont,
    PAF_AST_decodeDecode,
    PAF_AST_decodeStream,
    PAF_AST_decodeEncode,
    PAF_AST_decodeFinalTest,
    PAF_AST_decodeComplete,
    PAF_AST_selectDevices,
    PAF_AST_sourceDecode,
    PAF_AST_startOutput,
    PAF_AST_stopOutput,
    PAF_AST_setCheckRateX,
    PAF_AST_streamChainFunction,
    PAF_DEC_deviceAllocate,
    PAF_DEC_deviceSelect,
    PAF_DEC_computeFrameLength,
    PAF_DEC_updateInputStatus,
    NULL, /* PAF_BUF_copy, */
    NULL, /*headerPrint*/
    NULL, /*allocPrint*/
    NULL, /*commonPrint*/
    NULL, /*bufMemPrint*/
    NULL, /*memStatusPrint*/
    // For ARC
    PAF_ARC_controlRateAsInputA,
};


// .............................................................................

// EOF
