
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Function Definitions
//
//     Audio Framework is Audio Streams 1-N for IROM.
//
//
//

#include <stdio.h>
#include <std.h>
#include <alg.h>
#include <ialg.h>
#include <sio.h>
#include <mem.h>
#include <tsk.h>
#include <string.h> // memset
#include <sts.h>
#include <clk.h>

#include <ztop.h>
#include <arc_ext.h>

// .............................................................................

// Debugging Trace Control, local to this file.
// 
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
    #define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, you might set the mask to 0, but I'd always leave it at 1.
#ifdef AS_COMMON_TRACE_MASK
  // app can define this in ztop.h
  #define CURRENT_TRACE_MASK  AS_COMMON_TRACE_MASK
#else
  #define CURRENT_TRACE_MASK  3   // terse and general only
#endif

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // half dozen lines per frame
#define TRACE_MASK_VERBOSE  4   // trace full operation
#define TRACE_MASK_TIME     8   // Timing related traces

// malloc failures need to be found and addressed immediately.
#define TRACE_PRINT_ERR(a,b,c) { printf("\n%s: %s.%d: PAF_ALG_alloc failed.  Check the trace.\n", a, b, c); LOG_disable(&trace); } // stop tracing

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
    #define TRACE_TERSE(a) LOG_printf a
#else
    #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
    #define TRACE_GEN(a) LOG_printf a
#else
    #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
    #define TRACE_VERBOSE(a) LOG_printf a
#else
    #define TRACE_VERBOSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_TIME)
    #define TRACE_TIME(a) LOG_printf a
#else
    #define TRACE_TIME(a)
#endif

#if (CURRENT_TRACE_MASK) // non-zero i.e. in-use
// list of stream names, indexed by audio stream number
static char *streamName[] =
{
    "",
    "OutputMaster",
    "OutputSlaveA",
    "OutputSlaveB",
    "InputA",
    "InputB",
};
#endif

// .............................................................................

#include "AS_common.h"
#include "statStruct.h"  // A concise structure for debugging

#include <AS_params.h>
#include <AS_patchs.h>
#include <AS_config.h>


// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Allocation
//
//   Name:      PAF_AST_initPhaseMalloc
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation of memory.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseMalloc (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int zMS = pC->masterStr;
    (void)as;   // avoid compiler warning:  This is used for debugging.
    (void)zMS;

    TRACE_VERBOSE((&TR_MOD, "%s: initialization phase - memory allocation", streamName[as+zMS]));

    if (! (pC->xInp = MEM_calloc (HEAP_INTERNAL, INPUTN * sizeof (*pC->xInp), 4))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    if (! (pC->xDec = MEM_calloc (HEAP_INTERNAL, DECODEN * sizeof (*pC->xDec), 4))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    if (! (pC->xStr = MEM_calloc (HEAP_INTERNAL, STREAMN * sizeof (*pC->xStr), 4))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    {
        Int z;                          /* stream counter */

        PAF_AudioFrame *fBuf;

        if (! (fBuf = (PAF_AudioFrame *)MEM_calloc (HEAP_INTERNAL, STREAMS * sizeof (*fBuf), 4))) {
            TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
            return __LINE__;
        }

        for (z=STREAM1; z < STREAMN; z++)
            pC->xStr[z].pAudioFrame = &fBuf[z-STREAM1];
    }

    if (! (pC->xEnc = MEM_calloc (HEAP_INTERNAL, ENCODEN * sizeof (*pC->xEnc), 4))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    if (! (pC->xOut = MEM_calloc (HEAP_INTERNAL, OUTPUTN * sizeof (*pC->xOut), 4))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed", streamName[as+zMS], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    return 0;
} //PAF_AST_initPhaseMalloc

// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Initialization from Configuration
//
//   Name:      PAF_AST_initPhaseConfig
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Other as per initFrame0 and initFrame1.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_initPhaseConfig (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int zMS = pC->masterStr;
    (void)as;   // avoid compiler warning:  This is used for debugging.
    (void)zMS;

    TRACE_VERBOSE((&TR_MOD, "%s: %s.%d: initialization phase - configuration ",
                   streamName[as+zMS], __FUNCTION__, __LINE__));

    // Unspecified elements have been initialized to zero during alloc.

    for (z=INPUT1; z < INPUTN; z++) {
        pC->xInp[z].inpBufStatus = *pP->pInpBufStatus;
        pC->xInp[z].inpBufConfig.pBufStatus = &pC->xInp[z].inpBufStatus;
    }

    for (z=DECODE1; z < DECODEN; z++) {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        pC->xDec[z].decodeControl.size = sizeof(pC->xDec[z].decodeControl);
        pC->xDec[z].decodeControl.pAudioFrame = pC->xStr[zS].pAudioFrame;
        pC->xDec[z].decodeControl.pInpBufConfig =
        (const PAF_InpBufConfig *) &pC->xInp[zI].inpBufConfig;
        pC->xDec[z].decodeStatus = *pP->z_pDecodeStatus[z];
        pC->xDec[z].decodeInStruct.pAudioFrame = pC->xStr[zS].pAudioFrame;
    }

    for (z=STREAM1; z < STREAMN; z++) {
        Int linno;
        if (linno = pP->fxns->initFrame0 (pP, pQ, pC, z))
            return linno;
        if (linno = pP->fxns->initFrame1 (pP, pQ, pC, z, -1))
            return linno;
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        Int zS = pP->streamsFromEncodes[z];
        pC->xEnc[z].encodeControl.size = sizeof(pC->xEnc[z].encodeControl);
        pC->xEnc[z].encodeControl.pAudioFrame = pC->xStr[zS].pAudioFrame;
        pC->xEnc[z].encodeControl.pVolumeStatus = &pC->xEnc[z].volumeStatus;
        pC->xEnc[z].encodeControl.pOutBufConfig = &pC->xOut[zO].outBufConfig;
        pC->xEnc[z].encodeStatus = *pP->z_pEncodeStatus[z];
        pC->xEnc[z].encodeControl.encActive = pC->xEnc[z].encodeStatus.select;
        pC->xEnc[z].volumeStatus = *pP->pVolumeStatus;
        pC->xEnc[z].encodeInStruct.pAudioFrame = pC->xStr[zS].pAudioFrame;
    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        pC->xOut[z].outBufStatus = *pP->pOutBufStatus;
    }

    return 0;
} //PAF_AST_initPhaseConfig

// -----------------------------------------------------------------------------
// AST Initialization Function - ACP Algorithm Instantiation
//
//   Name:      PAF_AST_initPhaseAcpAlg
//   Purpose:   Audio Stream Task Function for initialization of ACP by
//              instantiation of the algorithm.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on ACP Algorithm creation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseAcpAlg (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */

    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int betaPrimeOffset;
    ACP_Handle acp;
    Int zS, zX;

    TRACE_VERBOSE((&TR_MOD, "%s: %s.%d: initialization phase - ACP Algorithm",
                   streamName[as+pC->masterStr], __FUNCTION__, __LINE__));

    ACP_MDS_init();

    if (! (acp = (ACP_Handle )ACP_MDS_create (NULL))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: ACP algorithm instance creation  failed",
                     streamName[as+pC->masterStr], __FUNCTION__, __LINE__));
        return __LINE__;
    }
    pC->acp = acp;

    ((ALG_Handle) acp)->fxns->algControl ((ALG_Handle) acp,
                                          ACP_GETBETAPRIMEOFFSET, (IALG_Status *) &betaPrimeOffset);

    for (z=INPUT1; z < INPUTN; z++) {
        zS = z;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->inputsFromDecodes[zX] == z) {
                zS = pP->streamsFromDecodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_IB + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xInp[z].inpBufStatus);
        /* Ignore errors, not reported. */
    }

    for (z=DECODE1; z < DECODEN; z++) {
        zS = pP->streamsFromDecodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_DECODE + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xDec[z].decodeStatus);
        /* Ignore errors, not reported. */
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        zS = pP->streamsFromEncodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_ENCODE + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xEnc[z].encodeStatus);
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_VOLUME + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xEnc[z].volumeStatus);
        /* Ignore errors, not reported. */
    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        zS = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zS = pP->streamsFromEncodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_OB + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xOut[z].outBufStatus);
        /* Ignore errors, not reported. */
    }

    return 0;
} //PAF_AST_initPhaseAcpAlg

// -----------------------------------------------------------------------------
// AST Initialization Function - Common Memory and Algorithms
//
//   Name:      PAF_AST_initPhaseCommon
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation for common memory and by instantiation for
//              algorithms.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on PAF_ALG_alloc failure.
//              Source code line number on PAF_ALG_mallocMemory failure.
//              Source code line number on Decode Chain initialization failure.
//              Source code line number on ASP Chain initialization failure.
//              Source code line number on Encode Chain initialization failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

#include <pafsio_ialg.h>

#define inpLinkInit pP->i_inpLinkInit
#define outLinkInit pP->i_outLinkInit

Int
PAF_AST_initPhaseCommon (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* stream counter */
    Int g;                              /* gear */
    ACP_Handle acp = pC->acp;
    PAF_IALG_Config pafAlgConfig;
    IALG_MemRec common[3][PAF_IALG_COMMON_MEMN+1];

    //
    // Determine memory needs and instantiate algorithms across audio streams
    //

    PAF_ALG_setup (&pafAlgConfig, HEAP_INTERNAL, HEAP_EXTERNAL,HEAP_INTERNAL1,HEAP_CLEAR);

    if (pP->fxns->headerPrint)
        pP->fxns->headerPrint();

    for (z=STREAM1; z < STREAMN; z++) {
        Int zD, zE, zX;

        TRACE_VERBOSE((&TR_MOD, "%s: %s.%d: initialization phase - Common Algorithms",
                       streamName[as+z], __FUNCTION__, __LINE__));

        //
        // Determine common memory needs of Decode, ASP, and Encode Algorithms
        //

        PAF_ALG_init (common[z], lengthof (common[z]), COMMONSPACE);

        zD = -1;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        zE = -1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->streamsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }

        if (zD >= 0) {
            if (PAF_ALG_ALLOC (decLinkInit[zD-DECODE1], common[z])) {
                TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_alloc failed",
                             streamName[as+z], __FUNCTION__, __LINE__));
                TRACE_PRINT_ERR(streamName[as+z], __FUNCTION__, __LINE__);
                return __LINE__;
            }
            if (pP->fxns->allocPrint)
                pP->fxns->allocPrint ((const PAF_ALG_AllocInit *)(decLinkInit[z-DECODE1]),sizeof (*(decLinkInit[z-DECODE1])), &pafAlgConfig);
        }

        if (PAF_ALG_ALLOC (aspLinkInit[z-STREAM1][0], common[z])) {
            TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_alloc failed",
                         streamName[as+z], __FUNCTION__, __LINE__));
            TRACE_PRINT_ERR(streamName[as+z], __FUNCTION__, __LINE__);
            return __LINE__;
        }
        if (pP->fxns->allocPrint)
            pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(aspLinkInit[z-STREAM1][0]), sizeof (*(aspLinkInit[z-STREAM1][0])), &pafAlgConfig);

        if (zE >= 0) {
            if (PAF_ALG_ALLOC (encLinkInit[zE-ENCODE1], common[z])) {
                TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_alloc failed",
                             streamName[as+z], __FUNCTION__, __LINE__));
                TRACE_PRINT_ERR(streamName[as+z], __FUNCTION__, __LINE__);

                return __LINE__;
            }
            if (pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(encLinkInit[z-ENCODE1]), sizeof (*(encLinkInit[z-ENCODE1])), &pafAlgConfig);
        }

        //
        // Determine common memory needs of Logical IO drivers
        //

        // really need to loop over all inputs for this stream using the tables
        // inputsFromDecodes and streamsFromDecodes. But these don't exist for this
        // patch, and not needed for FS11, since there is only one input.
        if (INPUT1 <= z && z < INPUTN) {
            TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_initPhaseCommon.%d: alloc for %d",
                           streamName[as+z], __LINE__, z));
            if (PAF_ALG_ALLOC (inpLinkInit[z-INPUT1], common[z])) {
                TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_alloc failed",
                             streamName[as+z], __FUNCTION__, __LINE__));
                TRACE_PRINT_ERR(streamName[as+z], __FUNCTION__, __LINE__);
                return __LINE__;
            }
            if (pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(inpLinkInit[z-INPUT1]), sizeof (*(inpLinkInit[z-INPUT1])), &pafAlgConfig);
        }

        if (OUTPUT1 <= z && z < OUTPUTN) {
            if (PAF_ALG_ALLOC (outLinkInit[z-OUTPUT1], common[z])) {
                TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_alloc failed",
                             streamName[as+z], __FUNCTION__, __LINE__));
                printf("%s: %s.%d: PAF_ALG_alloc failed",
                       streamName[as+z], __FUNCTION__, __LINE__);

                return __LINE__;
            }
            if (pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(outLinkInit[z-INPUT1]), sizeof (*(outLinkInit[z-INPUT1])), &pafAlgConfig);
        }
    }
    {
        // Changes made to share scratch between zones
        // Assume maximum 3 zones and scratch common memory is at offset 0;
        int max=0;
        for (z=STREAM1; z < STREAMN; z++)
        {
            if (max<common[z][0].size)
                max=common[z][0].size;
        }
        common[STREAM1][0].size=max;
        for (z=STREAM1+1; z < STREAMN; z++)
            common[z][0].size=0;
    }     //
    // Provide common memory needs of Decode, ASP, and Encode Algorithms
    //

    for (z=STREAM1; z < STREAMN; z++) {
        Int zD, zE, zX;
        zD = -1;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        zE = -1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->streamsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }

        // 
        // CJP:  This is not likely required. It may hide some other problem.
        //  When I run ZAA the first time, I often fail on the alloc below.
        // When I stop in the debugger, I see that common is junk.
        // With a breakpoint and some fiddling, common is OK, and OK ever after.
        // This smells more like a compiler problem to me than the alternative,
        // an uninitialized variable.
        // I leave this comment in case others see it.
        // 
       #if 0
        if ((char*)common < (char*)0x11800000)  // lowest RAM address
        {
        	 TRACE_TERSE((&TR_MOD, "%s: %s.%d: ERROR:  common pointer is not in RAM.",
        	                         streamName[as+z], __FUNCTION__, __LINE__));
        	 TRACE_TERSE((&TR_MOD, "%s:: common pointer is 0x%x.",
        	                         streamName[as+z], common));
        	 printf("\n %s: %s.%d: ERROR:  common pointer (0x%x) is not in RAM. \n",
        			 streamName[as+z], __FUNCTION__, __LINE__, common);
        }
       #endif

        if (PAF_ALG_mallocMemory (common[z], &pafAlgConfig)) {
            // see comment above
            TRACE_TERSE((&TR_MOD, "%s: %s.%d: PAF_ALG_mallocMemory failed",
                         streamName[as+z], __FUNCTION__, __LINE__));
            TRACE_TERSE((&TR_MOD, "%s: z: %d.  Size 0x%x", streamName[as+z], z, common[z][0].size));
            TRACE_PRINT_ERR(streamName[as+z], __FUNCTION__, __LINE__);
            return __LINE__;
        }
        // share zone0 scratch with all zones 
        common[z][0].base=common[0][0].base;
        if (pP->fxns->commonPrint)
            pP->fxns->commonPrint (common[z], &pafAlgConfig);

        //
        // Instantiate Decode, ASP, and Encode Algorithms
        //

        if (zD >= 0) {
            PAF_ASP_Chain *chain =
            PAF_ASP_chainInit (&pC->xDec[zD].decChainData, pP->pChainFxns,
                               HEAP_INTERNAL, as+z, acp, &trace,
                               decLinkInit[zD-DECODE1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "%s: Decode chain initialization failed", streamName[as+z]));
                return __LINE__;
            }
        }

        pC->xStr[z].aspChain[0] = NULL;
        for (g=0; g < GEARS; g++) {
            PAF_ASP_Chain *chain =
            PAF_ASP_chainInit (&pC->xStr[z].aspChainData[g], pP->pChainFxns,
                               HEAP_INTERNAL, as+z, acp, &trace,
                               aspLinkInit[z-STREAM1][g], pC->xStr[z].aspChain[0], common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "%s: ASP chain %d initialization failed", streamName[as+z], g));
                return __LINE__;
            }
            else
                pC->xStr[z].aspChain[g] = chain;
        }

        if (zE >= 0) {
            PAF_ASP_Chain *chain =
            PAF_ASP_chainInit (&pC->xEnc[zE].encChainData, pP->pChainFxns,
                               HEAP_INTERNAL, as+z, acp, &trace,
                               encLinkInit[zE-ENCODE1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "%s: Encode chain initialization failed", streamName[as+z]));
                return __LINE__;
            }
        }

        //
        // Allocate non-common memories for Logical IO drivers
        //    Since these structures are used at run-time we allocate from external memory

        if (INPUT1 <= z && z < INPUTN) {
            PAF_ASP_Chain *chain;
            TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_initPhaseCommon.%d: chain init for %d",
                           streamName[as+z], __LINE__, z));
            chain = PAF_ASP_chainInit (&pC->xInp[z].inpChainData, pP->pChainFxns,
                                       HEAP_EXTERNAL, as+z, acp, &trace,
                                       inpLinkInit[z-INPUT1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "%s: Input chain initialization failed", streamName[as+z]));
                return __LINE__;
            }
        }

        if (OUTPUT1 <= z && z < OUTPUTN) {
            PAF_ASP_Chain *chain =
            PAF_ASP_chainInit (&pC->xOut[z].outChainData, pP->pChainFxns,
                               HEAP_EXTERNAL, as+z, acp, &trace,
                               outLinkInit[z-OUTPUT1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "%s: Output chain initialization failed", streamName[as+z]));
                return __LINE__;
            }
        }
    }

    return 0;
} //PAF_AST_initPhaseCommon

// -----------------------------------------------------------------------------
// AST Initialization Function - Algorithm Keys
//
//   Name:      PAF_AST_initPhaseAlgKey
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters for Algorithm Keys.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

// .............................................................................

Int
PAF_AST_initPhaseAlgKey (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int g;                              /* gear */
    Int s;                              /* key number */
    Int nkey;                           /* key index */
    Int keyfound;                       /* keyfound flag for table lookup */
    Int keyval;                         /* keyval is the hash returned from the key process */

    PAF_ASP_Link *that;
    PAF_ASP_LinkInit *pInit;

    (void)as;   // avoid compiler warning:  This is used for debugging.

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
#else
    volatile Uint32 ROM_revision = 0x0000000C;     /* ROM revision address */
#endif

    TRACE_VERBOSE((&TR_MOD, "%s: %s.%d: initialization phase - Algorithm Keys",
                   streamName[as], __FUNCTION__, __LINE__));

    for (z=DECODE1; z < DECODEN; z++) {
        for (s=0; s < pP->pDecAlgKey->length; s++) {
            if (pP->pDecAlgKey->code[s].full != 0
                && (that = PAF_ASP_chainFind (&pC->xDec[z].decChainData, pP->pDecAlgKey->code[s]))) {
                    pC->xDec[z].decAlg[s] = (ALG_Handle )that->alg;
                /* Cast in interface, for now --Kurt */
            }
            else
                pC->xDec[z].decAlg[s] = NULL;
        }
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        for (s=0; s < pP->pEncAlgKey->length; s++) {
            if (pP->pEncAlgKey->code[s].full != 0
                && (that = PAF_ASP_chainFind (&pC->xEnc[z].encChainData, pP->pEncAlgKey->code[s])))
                pC->xEnc[z].encAlg[s] = (ALG_Handle )that->alg;
            /* Cast in interface, for now --Kurt */
            else
                pC->xEnc[z].encAlg[s] = NULL;
        }
    }

    return 0;
} //PAF_AST_initPhaseAlgKey

// -----------------------------------------------------------------------------
// AST Initialization Function - I/O Devices
//
//   Name:      PAF_AST_initPhaseDevice
//   Purpose:   Audio Stream Task Function for initialization of I/O Devices.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on device allocation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseDevice (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/output counter */
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
    PAF_IALG_Config pafAlgConfig;

    (void)as;   // avoid compiler warning:  This is used for debugging.

    TRACE_VERBOSE((&TR_MOD, "%s: %s.%d: initialization phase - I/O Devices",
                   streamName[as], __FUNCTION__, __LINE__));

    if (pP->fxns->bufMemPrint)
        PAF_ALG_setup (&pafAlgConfig, HEAP_INTERNAL, HEAP_EXTERNAL,HEAP_INTERNAL1,HEAP_CLEAR);

    for (z=INPUT1; z < INPUTN; z++) {
        PAF_InpBufConfig *pConfig = &pC->xInp[z].inpBufConfig;

        pObj = (PAF_SIO_IALG_Obj *) pC->xInp[z].inpChainData.head->alg;
        pAlgConfig = &pObj->config;

        pC->xInp[z].hRxSio = NULL;

        pConfig->base.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->futureHead.pVoid = pAlgConfig->pMemRec[0].base;
        pConfig->allocation       = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement    = 2;
        pConfig->precision        = 16;

        if (pP->fxns->bufMemPrint)
            pP->fxns->bufMemPrint(z,pAlgConfig->pMemRec[0].size,PAF_ALG_memSpace(&pafAlgConfig,pAlgConfig->pMemRec[0].space),0);

    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        PAF_OutBufConfig *pConfig = &pC->xOut[z].outBufConfig;

        pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
        pAlgConfig = &pObj->config;

        pC->xOut[z].hTxSio = NULL;
        pConfig->base.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->allocation     = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement  = 3;
        pConfig->precision      = 24;
        if (pP->fxns->bufMemPrint)
            pP->fxns->bufMemPrint(z,pAlgConfig->pMemRec[0].size,PAF_ALG_memSpace(&pafAlgConfig,pAlgConfig->pMemRec[0].space),1);
    }

    return 0;
} //PAF_AST_initPhaseDevice

// -----------------------------------------------------------------------------
// AST Initialization Function Helper - Initialization of Audio Frame
//
//   Name:      PAF_AST_initFrame0
//   Purpose:   Audio Stream Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
//   From:      AST Parameter Function -> decodeInfo
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//              Source code line number on unsupported option.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Memory allocation errors.
//              * Unsupported option errors.
//

// MID 314
extern const char AFChanPtrMap[16+1][16];
extern PAF_ChannelConfigurationMaskTable PAF_ASP_stdCCMT_patch;

Int
PAF_AST_initFrame0 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int ch;
    Int aLen;
    Int aSize = sizeof(PAF_AudioData);
    Int aAlign = aSize < sizeof (int) ? sizeof (int) : aSize;
    Int maxFrameLength = pP->maxFramelength;
    Int zX;
    PAF_AudioData *aBuf;
    char i;

    (void)as;   // avoid compiler warning:  This is used for debugging.


    // Compute maximum framelength (needed for ARC support)
    maxFrameLength += 8 - maxFrameLength % 8;
    aLen = numchan[z] * maxFrameLength;

    //
    // Initialize audio frame elements directly
    //

    pC->xStr[z].pAudioFrame->fxns = pP->pAudioFrameFunctions;
    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN; ///
///    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
    pC->xStr[z].pAudioFrame->data.nSamples = FRAMELENGTH;
    pC->xStr[z].pAudioFrame->data.sample = pC->xStr[z].audioFrameChannelPointers;
    pC->xStr[z].pAudioFrame->data.samsiz = pC->xStr[z].audioFrameChannelSizes;
    pC->xStr[z].pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;

    //
    // Allocate memory for and initialize pointers to audio data buffers
    //
    //   The NUMCHANMASK is used to identify the channels for which data
    //   buffers can be allocated. Using this mask and switch statement
    //   rather than some other construct allows efficient code generation,
    //   providing just the code necessary (with significant savings).
    //

    if (pP->fxns->bufMemPrint)
        pP->fxns->bufMemPrint(z, aLen*aSize, HEAP_FRMBUF, 2);

    if (! (aBuf = (PAF_AudioData *)MEM_calloc (HEAP_FRMBUF, aLen*aSize, aAlign))) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: MEM_calloc failed",
                     streamName[as+z], __FUNCTION__, __LINE__));
        return __LINE__;
    }

    {
        Int i;

#pragma UNROLL(1)
        for (i=0; i < PAF_MAXNUMCHAN_AF; i++)
            pC->xStr[z].audioFrameChannelPointers[i] = NULL;
    }

    // MID 314
    if ((numchan[z] > 16) || (numchan[z] < 1)) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: unsupported option",
                     streamName[as+z], __FUNCTION__, __LINE__));
        return __LINE__;
    }
    else {
        for (i=0;i<numchan[z];i++) {
            char chan = AFChanPtrMap[numchan[z]][i];
            if (chan != -1)
                pC->xStr[z].audioFrameChannelPointers[chan] = aBuf + maxFrameLength*(i+1) - FRAMELENGTH;
        }
    }

    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
        if (pC->xStr[z].audioFrameChannelPointers[ch])
            pC->xStr[z].origAudioFrameChannelPointers[ch] = pC->xStr[z].audioFrameChannelPointers[ch];
    }

    //
    // Initialize decoder elements directly
    //

    for (zX = DECODE1; zX < DECODEN; zX++) {
        if (pP->streamsFromDecodes[zX] == z) {
#ifdef NOAUDIOSHARE
            pC->xDec[zX].decodeInStruct.audioShare.nSamples = 0;
            pC->xDec[zX].decodeInStruct.audioShare.sample = NULL;
#else /* NOAUDIOSHARE */
            pC->xDec[zX].decodeInStruct.audioShare.nSamples = aLen;
            pC->xDec[zX].decodeInStruct.audioShare.sample = aBuf;
#endif /* NOAUDIOSHARE */
        }
    }

    return 0;
} //PAF_AST_initFrame0

// -----------------------------------------------------------------------------
// AST Initialization Function Helper - Reinitialization of Audio Frame
// AST Decoding Function              - Reinitialization of Audio Frame
//
//   Name:      PAF_AST_initFrame1
//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
//   From:      AS_xxxxxx_Task or equivalent
//              AST Parameter Function -> decodeInfo
//              AST Parameter Function -> decodeDecode
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//

Int
PAF_AST_initFrame1 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z, Int apply)
{
    //
    // Reinitialize audio frame elements:
    //
    //   Channel Configurations during init             = Unknown
    //      "          "        during info or decode   = None
    //
    //   Sample Rate / Count    during init or info     = Unknown
    //     "     "   /   "      during decode           = unchanged
    //

    if (apply < 0) {
        pC->xStr[z].pAudioFrame->channelConfigurationRequest.full = PAF_CC_UNKNOWN;
        pC->xStr[z].pAudioFrame->channelConfigurationStream.full = PAF_CC_UNKNOWN;
    }
    else {
        pC->xStr[z].pAudioFrame->channelConfigurationRequest.full = PAF_CC_NONE;
        pC->xStr[z].pAudioFrame->channelConfigurationStream.full = PAF_CC_NONE;
    }

    if (apply < 1) {
        pC->xStr[z].pAudioFrame->sampleRate = PAF_SAMPLERATE_UNKNOWN;
        pC->xStr[z].pAudioFrame->sampleCount = 0;
    }

    return 0;
} //PAF_AST_initFrame1

// -----------------------------------------------------------------------------
// AST Processing Function - Pass-Through Processing
//
//   Name:      PAF_AST_passProcessing
//   Purpose:   Audio Stream Task Function for processing audio data as a
//              pass-through from the input driver to the output driver
//              for development and testing.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//              * State information on processing.
//

#pragma CODE_SECTION(PAF_AST_passProcessing,".text:_PAF_AST_passProcessing")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int hack)
{
    Int z;                              /* input/output counter */
    Int errno = 0;                      /* error number */
    Int getVal;
    Int rxNumChan, txNumChan;
    Int first;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;

    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */

    TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: initializing"));

    //
    // Determine that receive/transmit channels are compatible
    //

    // Can handle handle only master input
    for (z=INPUT1; z < INPUTN; z++) {
        if (z != zMI && pC->xInp[z].hRxSio)
            return(ASPERR_PASS + 0x01);
    }

    /* Number of receive/transmit channels */

    if (! pC->xInp[zMI].hRxSio)
        return(ASPERR_PASS + 0x11);
    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return(ASPERR_PASS + 0x12);
    if (rxNumChan > TX_NUMCHAN(zMI))
        return(ASPERR_PASS + 0x13);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (! pC->xOut[zMI].hTxSio)
            return(ASPERR_PASS + 0x10*(z+2) + 0x01);
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return(ASPERR_PASS + 0x10*(z+2) + 0x02);
        if (txNumChan > TX_NUMCHAN(zMI))
            return(ASPERR_PASS + 0x10*(z+2) + 0x03);
    }

    //
    // Set up receive/transmit
    //

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (SIO_idle (pC->xOut[z].hTxSio))
            return ASPERR_IDLE;
    }

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_SOURCESELECT, PAF_SOURCE_PCM))
        return(ASPERR_PASS + 0x14);

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, FRAMELENGTH))
        return(ASPERR_PASS + 0x15);

    for (z=OUTPUT1; z < OUTPUTN; z++)
        pC->xOut[z].outBufConfig.lengthofFrame = FRAMELENGTH;

    if (SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_SYNC))
        return ASPERR_PASS + 0x16;

    if (SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL) != sizeof (PAF_InpBufConfig))
        return ASPERR_PASS + 0x17;

    //
    // Receive and transmit the data in single-frame buffers
    //

    first = 1;
    while (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_PASS) {
        PAF_OutBufConfig *pOutBuf;
        PAF_InpBufConfig *pInpBuf;

        if (first) {
            first = 0;

            TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: starting output"));

            for (z=OUTPUT1; z < OUTPUTN; z++) {
                getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
                if (getVal > 0) {
                    errno = ASPERR_ISSUE;
                    break;
                }
                else if (getVal < 0) {
                    errno = -getVal;
                    break;
                }

                if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_UNMUTE, 0))
                    return(getVal & 0xff) | ASPERR_MUTE;
            }
            if (errno)
                break;

        }

        getVal = SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME);
        if (getVal > 0) {
            errno = ASPERR_ISSUE;
            break;
        }

        TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: awaiting frame -- input size %d", rxNumChan * FRAMELENGTH));

        getVal = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr) &pInpBuf, NULL);
        if (getVal < 0) {
            errno = -getVal;
            break;
        }

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_reclaim (pC->xOut[z].hTxSio, (Ptr) &pOutBuf, NULL);
            if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if ( errno )
            break;

        TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: copying frame"));

        if (errno = pP->fxns->passProcessingCopy (pP, pQ, pC))
            break;

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
            if (getVal > 0) {
                errno = ASPERR_ISSUE;
                break;
            }
            else if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if ( errno )
            break;
    }

    //
    // Close down receive/transmit
    //

    TRACE_TERSE((&TR_MOD, "PAF_AST_passProcessing: finalizing"));

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_MUTE, 0)) {
            if (! errno)
                errno = (getVal & 0xff) | ASPERR_MUTE;
            /* convert to sensical errno */
        }
    }

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++)
        SIO_idle (pC->xOut[z].hTxSio);

    return errno;

} //PAF_AST_passProcessing

// -----------------------------------------------------------------------------
// AST Processing Function Helper - Pass-Through Processing Patch Point
//
//   Name:      PAF_AST_passProcessingCopy
//   Purpose:   Pass-Through Processing Function for copying audio data
//              from the input buffer to the output buffer.
//   From:      AST Parameter Function -> passProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              x
//

#pragma CODE_SECTION(PAF_AST_passProcessingCopy,".text:_PAF_AST_passProcessingCopy")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessingCopy (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int z;                              /* output counter */
    Int errno;                          /* error number */
    Int i;
    Int rxNumChan, txNumChan;
    Int zMI = pP->zone.master;

    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */

    // Copy data from input channels to output channels one of two ways:

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return(ASPERR_PASS + 0x12);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return(ASPERR_PASS + 0x22);

        if ( txNumChan <= rxNumChan ) {

            // Copy one to one, ignoring later rx channels as needed.

            for ( i=0; i < txNumChan; i++ ) {
                errno = pP->fxns->copy( i, &pC->xInp[zMI].inpBufConfig, i, &pC->xOut[z].outBufConfig );
                if ( errno )
                    return errno;
            }
        }
        else {

            // Copy one to many, repeating earlier rx channels as needed.

            Int from, to;

            from = 0;
            to   = 0;
            while ( to < txNumChan ) {
                errno = pP->fxns->copy( from, &pC->xInp[zMI].inpBufConfig, to, &pC->xOut[z].outBufConfig );
                if ( errno )
                    return errno;

                from++;
                to++;
                if ( from == rxNumChan )
                    from = 0;
            }
        }
    }

    return 0;
} //PAF_AST_passProcessingCopy

// -----------------------------------------------------------------------------
// AST Processing Function - Auto Processing
//
//   Name:      PAF_AST_autoProcessing
//   Purpose:   Audio Stream Task Function for processing audio data to
//              determine the input type without output.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//

Int
PAF_AST_autoProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int inputTypeSelect, ALG_Handle pcmAlgMaster)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int errno = 0;                      /* error number */
    Int nbytes;
    Int frameLength;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;

    (void)as;   // avoid compiler warning:  This is used for debugging.
    (void)zMS;

    TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_autoProcessing.%d",
                   streamName[as+zMS], __LINE__));

    if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                          PAF_SIO_CONTROL_SET_SOURCESELECT,
                          DECSIOMAP (pC->xDec[zMD].decodeStatus.sourceSelect)))
    {
        TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_autoProcessing.%d: source select returns 0x%x",
                       streamName[as+zMS], __LINE__, errno));
        return errno;
    }
    frameLength = pP->fxns->computeFrameLength (pcmAlgMaster, FRAMELENGTH,
                                                pC->xDec[zMD].decodeStatus.bufferRatio);

    if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                          PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
    {
        TRACE_VERBOSE((&TR_MOD, "PAF_AST_autoProcessing.%d: SET_PCMFRAMELENGTH returns 0x%x, returning ASPERR_AUTO_LENGTH, 0x%x",
                       __LINE__, errno, ASPERR_AUTO_LENGTH));
        return ASPERR_AUTO_LENGTH;
    }

    if (errno = SIO_issue (pC->xInp[zMI].hRxSio,
                           &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig),
                           PAF_SIO_REQUEST_SYNC))
    {
        TRACE_VERBOSE((&TR_MOD, "PAF_AST_autoProcessing.%d: REQUEST_SYNC returns 0x%x, returning ASPERR_ISSUE, 0x%x",
                       __LINE__, errno, ASPERR_ISSUE));
        return ASPERR_ISSUE;
    }

    TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_autoProcessing.%d: awaiting sync",
                   streamName[as+zMS], __LINE__));

    // all of the sync scan work is done in this call. If the error returned
    // is DIBERR_SYNC then that just means we didn't find a sync, not a real I/O
    // error so we mask it off.
    nbytes = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
    if (nbytes == -DIBERR_SYNC)
    {
        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_autoProcessing.%d: SIO_reclaim returned 0x%x, ignoring",
                     streamName[as+zMS], __LINE__, nbytes));
        return 0;
    }

    if (nbytes != sizeof (PAF_InpBufConfig))
    {
        TRACE_TERSE((&TR_MOD, "PAF_AST_autoProcessing. SIO_reclaim returned %d, not %d, returning ASPERR_RECLAIM (0x%x)",
                     nbytes, sizeof (PAF_InpBufConfig), ASPERR_RECLAIM));
        return ASPERR_RECLAIM;
    }
    if (errno)
    {
        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_autoProcessing.%d: returning errno 0x%x",
                     streamName[as+zMS], __LINE__, errno));
    }
    return errno;
} //PAF_AST_autoProcessing

// -----------------------------------------------------------------------------
// AST Decoding Function - Decode Command Processing
//
//   Name:      PAF_AST_decodeCommand
//   Purpose:   Decoding Function for processing Decode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//

Int
PAF_AST_decodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int zS;
    (void)as;   // avoid compiler warning:  This is used for debugging.



    for (z=DECODE1; z < DECODEN; z++) {
        zS = pP->streamsFromDecodes[z];
        (void)zS;   // avoid compiler warning:  This is used for debugging.
        if (! (pC->xDec[z].decodeStatus.command2 & 0x80)) {
            switch (pC->xDec[z].decodeStatus.command2) {
                case 0: // command none - process
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    break;
                case 1: // command abort - leave now
                    TRACE_TERSE((&TR_MOD, "%s: PAF_AST_decodeCommand: decode command abort (0x%02x)",
                                 streamName[as+zS], 1));
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    return(ASPERR_ABORT);
                case 2: // command restart - leave later
                    TRACE_TERSE((&TR_MOD, "%s: PAF_AST_decodeCommand: decode command quit (0x%02x)",
                                 streamName[as+zS], 2));
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    return(ASPERR_QUIT);
                default: // command unknown - ignore
                    break;
            }
        }
    }

    return 0;
} //PAF_AST_decodeCommand

// -----------------------------------------------------------------------------
// AST Decoding Function - Encode Command Processing
//
//   Name:      PAF_AST_encodeCommand
//   Purpose:   Decoding Function for processing Encode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//              * SIO control errors.
//              * Error number macros.
//

Int
PAF_AST_encodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* encode counter */
    Int errno = 0;                      /* error number */
    Int zO, zS;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    for (z=ENCODE1; z < ENCODEN; z++) {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        (void)zS;   // avoid compiler warning:  This is used for debugging.

        if (! (pC->xEnc[z].encodeStatus.command2 & 0x80)) {
            switch (pC->xEnc[z].encodeStatus.command2) {
                case 0: // command none - process
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 1: // mute command
                    TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_encodeCommand: encode command mute (0x%02x)",
                                   streamName[as+zS], 1));
                    if ((pC->xOut[zO].outBufStatus.audio & 0x0f) != PAF_OB_AUDIO_QUIET
                        && pC->xOut[zO].hTxSio
                        && (errno = SIO_ctrl (pC->xOut[zO].hTxSio, PAF_SIO_CONTROL_MUTE, 0))) {
                        errno = (errno & 0xff) | ASPERR_MUTE;
                        /* convert to sensical errno */
                        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_encodeCommand: SIO control failed (mute)", streamName[as+zS]));
                        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_encodeCommand: errno = 0x%04x <ignored>", streamName[as+zS], errno));
                    }
                    else {
                        pC->xOut[zO].outBufStatus.audio |= PAF_OB_AUDIO_MUTED;
                    }
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 2: // unmute command
                    TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_encodeCommand: encode command unmute (0x%02x)", streamName[as+zS], 2));
                    if ((pC->xOut[zO].outBufStatus.audio & 0x0f) != PAF_OB_AUDIO_QUIET
                        && pC->xOut[zO].hTxSio
                        && (errno = SIO_ctrl (pC->xOut[zO].hTxSio, PAF_SIO_CONTROL_UNMUTE, 0))) {
                        errno = (errno & 0xff) | ASPERR_MUTE;
                        /* convert to sensical errno */
                        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_encodeCommand: SIO control failed (unmute)", streamName[as+zS]));
                        TRACE_TERSE((&TR_MOD, "%s: PAF_AST_encodeCommand: errno = 0x%04x <ignored>", streamName[as+zS], errno));
                    }
                    else {
                        pC->xOut[zO].outBufStatus.audio &= ~PAF_OB_AUDIO_MUTED;
                    }
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                default: // command unknown - ignore
                    break;
            }
        }
    }

//  ERRNO_RPRT (AS_InputA_Task, errno);

    return 0;
} //PAF_AST_encodeCommand

// -----------------------------------------------------------------------------
// AST Decoding Function - Reinitialization of Decode
//
//   Name:      PAF_AST_decodeInit
//   Purpose:   Decoding Function for reinitializing the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInit (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[])
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zI, zO, zS;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    (void)as;   // avoid compiler warning:  This is used for debugging.

    // reset frameCount
    for (z=DECODE1; z < DECODEN; z++)
        if (pC->xDec[z].decodeStatus.mode)
            pC->xDec[z].decodeStatus.frameCount = 0;

        // reset drift register. Do this here so that register keeps its value until
        // decodeProcessing is restarted. Mostly useful in tightly coupled testing
        // where, e.g., a custom system stream can read the register. But not useful
        // in general since the timing window is so small.
    pC->xDec[zMD].decodeStatus.bufferDrift = 0;

    // infoSize = alg[zMI]->fxns->algControl(alg[zMI], DEC_MININFO, NULL);
    // vs. FRAMEINFO? --Kurt

    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;   // avoid compiler warning:  This is used for debugging.

        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {
            Uns gear;
            Int frameLength;
            TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_decodeInit: initializing decode", streamName[as+zS]));
            if (decAlg[z]->fxns->algActivate)
                decAlg[z]->fxns->algActivate (decAlg[z]);
            if (dec->fxns->reset
                && (errno = dec->fxns->reset (dec, NULL,
                                              &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                return errno;
            gear = pC->xDec[z].decodeStatus.aspGearControl;
            pC->xDec[z].decodeStatus.aspGearStatus = gear < GEARS ? gear : 0;
            frameLength = pP->fxns->computeFrameLength (decAlg[z], FRAMELENGTH,
                                                        pC->xDec[z].decodeStatus.bufferRatio);
            pC->xDec[z].decodeControl.frameLength = frameLength;
            pC->xDec[z].decodeInStruct.sampleCount = frameLength;
            pC->xDec[z].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;

            if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
                if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                      PAF_SIO_CONTROL_SET_AUTOREQUESTSIZE, 2*frameLength))
                    return ASPERR_AUTO_LENGTH;    // ** need a different error code here? **
            }
            else if (z != zMD) {
                if (errno = SIO_idle (pC->xInp[zI].hRxSio))
                    return errno;
            }

            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                  PAF_SIO_CONTROL_SET_SOURCESELECT,
                                  DECSIOMAP (pC->xDec[z].decodeStatus.sourceSelect)))
                return errno;
            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                  PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
                return errno;
            if (errno = pP->fxns->updateInputStatus (pC->xInp[zI].hRxSio,
                                                     &pC->xInp[zI].inpBufStatus, &pC->xInp[zI].inpBufConfig))
                return errno;
        }
    }

    if (pC->xInp[zMI].hRxSio) {
        // if in master decoder is in continous mode and
        // not running then change sourceDecode to force start
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
            pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE)
            pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_UNKNOWN;

        errno = SIO_issue (pC->xInp[zMI].hRxSio,
                           &pC->xInp[zMI].inpBufConfig,
                           sizeof (pC->xInp[zMI].inpBufConfig),
                           pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                           pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN ?
                           PAF_SIO_REQUEST_AUTO :
                           PAF_SIO_REQUEST_NEWFRAME);
        if (errno)
            return errno;
    }

    // TODO: move this to start of this function so that it doesn't affect IO timing
    for (z=ENCODE1; z < ENCODEN; z++) {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_decodeInit: initializing encode", streamName[as+zS]));
            if (encAlg->fxns->algActivate)
                encAlg->fxns->algActivate (encAlg);
            if (enc->fxns->reset
                && (errno = enc->fxns->reset (enc, NULL,
                                              &pC->xEnc[z].encodeControl, &pC->xEnc[z].encodeStatus)))
                return errno;
        }
    }

    return 0;
} //PAF_AST_decodeInit

// -----------------------------------------------------------------------------
// AST Decoding Function - Frame-Final Processing
//
//   Name:      PAF_AST_decodeFinalTest
//   Purpose:   Decoding Function for determining whether processing of the
//              current frame is complete.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0 if incomplete, and 1 if complete.
//   Trace:     None.
//

Int
PAF_AST_decodeFinalTest (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int           zMD = pC->masterDec;
    Int  sourceSelect;
    Int sourceProgram;


    sourceSelect  = pC->xDec[zMD].decodeStatus.sourceSelect;
    sourceProgram = pC->xDec[zMD].decodeStatus.sourceProgram;

    if ((sourceSelect == PAF_SOURCE_NONE) || (sourceSelect == PAF_SOURCE_PASS))
        return 1;

    // The following allows for Force modes to switch without command deferral. This might
    // be better suited for inclusion in DIB_requestFrame, but for now will reside here.
    if ((sourceSelect == PAF_SOURCE_SNG) || (sourceSelect > PAF_SOURCE_BITSTREAM)) {
        if (sourceSelect == PAF_SOURCE_DTSALL) {
            if (sourceProgram != PAF_SOURCE_DTS11 &&
                sourceProgram != PAF_SOURCE_DTS12 &&
                sourceProgram != PAF_SOURCE_DTS13 &&
                sourceProgram != PAF_SOURCE_DTS14 &&
                sourceProgram != PAF_SOURCE_DTS16 &&
                sourceProgram != PAF_SOURCE_DTSHD)
                return 1;
        }
        else if (sourceSelect == PAF_SOURCE_PCMAUTO) {
            if (sourceProgram != PAF_SOURCE_PCM)
                return 1;
        }
        else {
            if (pC->xDec[zMD].decodeStatus.sourceSelect != pC->xDec[zMD].decodeStatus.sourceDecode)
                return 1;
        }
    }

    return 0;
} //PAF_AST_decodeFinalTest

// -----------------------------------------------------------------------------
// AST Decoding Function - Stream-Final Processing
//
//   Name:      PAF_AST_decodeComplete
//   Purpose:   Decoding Function for terminating the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeComplete (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as  = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                               /* decode/encode counter */
    (void)as;   // avoid compiler warning:  This is used for debugging.


#ifdef PAF_ASP_FINAL
    /* This material is currently not utilized */
#endif /* PAF_ASP_FINAL */
    for (z=DECODE1; z < DECODEN; z++) {
#ifdef PAF_ASP_FINAL
        DEC_Handle dec = (DEC_Handle )decAlg[z];
#endif /* PAF_ASP_FINAL */
        Int zI = pP->inputsFromDecodes[z];
        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing decode", as+z));
#ifdef PAF_ASP_FINAL
            if (dec->fxns->final)
                dec->fxns->final(dec, NULL, &pC->xDec[z].decodeControl,
                                 &pC->xDec[z].decodeStatus);
#endif /* PAF_ASP_FINAL */
            if (decAlg[z]->fxns->algDeactivate)
                decAlg[z]->fxns->algDeactivate (decAlg[z]);
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: processing decode <ignored>", as+z));
        }
    }

    pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_FINAL, 0, frame);

    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
#ifdef PAF_ASP_FINAL
            ENC_Handle enc = (ENC_Handle )encAlg;
#endif /* PAF_ASP_FINAL */
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing encode", as+z));
#ifdef PAF_ASP_FINAL
            if (enc->fxns->final)
                enc->fxns->final(enc, NULL, &pC->xEnc[z].encodeControl,
                                 &pC->xEnc[z].encodeStatus);
#endif /* PAF_ASP_FINAL */
            if (encAlg->fxns->algDeactivate)
                encAlg->fxns->algDeactivate (encAlg);
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing encode <ignored>", as+z));
        }
    }

    // wait for remaining data to be output
    pP->fxns->stopOutput (pP, pQ, pC);

    return 0;
} //PAF_AST_decodeComplete

// -----------------------------------------------------------------------------
// AST Selection Function - Source Selection
//
//   Name:      PAF_AST_sourceDecode
//   Purpose:   Audio Stream Task Function for selecting the sources used
//              for decoding of input to output.
//   From:      AS_xxxxxx_Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//

Int
PAF_AST_sourceDecode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int x)
{
    Int z;                              /* decode counter */

    for (z=DECODE1; z < DECODEN; z++)
        if (pC->xDec[z].decodeStatus.mode) {
            pC->xDec[z].decodeStatus.sourceDecode = x;
        }
    return 0;
} //PAF_AST_sourceDecode

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - Chain Processing
//
//   Name:      PAF_AST_streamChainFunction
//   Purpose:   Common Function for processing algorithm chains.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeStream
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_streamChainFunction (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int iChainFrameFxns, Int abortOnError, Int logArg)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* stream counter */
    Int errno;                          /* error number */
    Int dFlag, eFlag, gear;
    Int zX;
    Int zS;
    (void)as;           // avoid compiler warning, used in debugging

    for (zS = STREAM1; zS < STREAMN; zS++) {
        z = pP->streamOrder[zS];  // Select stream order from streamOrder parameter - MID 788

        // apply stream
        //      unless the stream is associated with a decoder and it is not running
        // or
        //      unless the stream is associated with an encoder and it is not running
        // Also gear control only works for streams with an associated decoder
        // if no such association exists then gear 0 (All) is used
        dFlag = 1;
        gear = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                dFlag = pC->xDec[zX].decodeStatus.mode;
                gear = pC->xDec[zX].decodeStatus.aspGearStatus;
                break;
            }
        }
        eFlag = 1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->streamsFromEncodes[zX] == z) {
                eFlag = pC->xEnc[zX].encodeStatus.mode;
                break;
            }
        }

        if (dFlag && eFlag) {
            PAF_ASP_Chain *chain = pC->xStr[z].aspChain[gear];
            PAF_AudioFrame *frame = pC->xStr[z].pAudioFrame;
            Int (*func) (PAF_ASP_Chain *, PAF_AudioFrame *) = chain->fxns->chainFrameFunction[iChainFrameFxns];
            TRACE_GEN((&TR_MOD,
                       iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (reset)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "%s: PAF_AST_streamChainFunction.%d: processing block %d -- audio stream (apply)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (final)"
                       : "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (?????)",
                       streamName[as+z], __LINE__, logArg));
            errno = (*func) (chain, frame);
            TRACE_VERBOSE((&TR_MOD, "%s: PAF_AST_streamChainFunction.%d: errno 0x%x.",
                           streamName[as+z], __LINE__, errno));
            if (errno && abortOnError)
                return errno;
        }
        else {
            TRACE_GEN((&TR_MOD,
                       iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (reset) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "%s: PAF_AST_streamChainFunction.%d: processing block %d -- audio stream (apply) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (final) <ignored>"
                       : "%s: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (?????) <ignored>",
                       streamName[as+z], __LINE__, logArg));
        }
    }

    return 0;
} //PAF_AST_streamChainFunction

// -----------------------------------------------------------------------------

Int
PAF_AST_decodeHandleErrorInput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int z, Int error)
{
    Int errno = 0;
    Int zMD = pC->masterDec;
    Int zI = pP->inputsFromDecodes[z];


    // only handle real errors, on primary input, for writeDECModeContinuous
    if ( !( error &&
            z == zMD &&
            pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS ))
        return error;

    TRACE_TIME((&TIME_MOD, "%s: PAF_AST_decodeHandleErrorInput: (primary) input error caught = %d", streamName[pC->as+z], error));
    TRACE_TIME((&TIME_MOD, "%s: PAF_AST_decodeHandleErrorInput: old sourceProgram = %d", streamName[pC->as+z], pC->xDec[z].decodeStatus.sourceProgram));

    if (pC->xInp[zI].hRxSio) {
        DEC_Handle dec;

        if (errno = SIO_idle (pC->xInp[zI].hRxSio))
            return errno;

        // indicates (primary) input not running
        pC->xDec[z].decodeStatus.sourceDecode = PAF_SOURCE_NONE;

        pC->xInp[zI].inpBufConfig.deliverZeros = 1;

        // will be changed after next reclaim, to PAF_SOURCE_UNKNOWN or other
        pC->xDec[z].decodeStatus.sourceProgram = PAF_SOURCE_NONE;

        if (decAlg[z]->fxns->algDeactivate)
            decAlg[z]->fxns->algDeactivate (decAlg[z]);

        decAlg[z] = pC->xDec[z].decAlg[PAF_SOURCE_PCM];
        dec = (DEC_Handle )decAlg[z];

        TRACE_TIME((&TIME_MOD, "%s: PAF_AST_decodeHandleErrorInput: resetting to PCM decoder",
                    streamName[pC->as+z]));

        if (decAlg[z]->fxns->algActivate)
            decAlg[z]->fxns->algActivate (decAlg[z]);

        if (dec->fxns->reset
            && (errno = dec->fxns->reset (dec, NULL,
                                          &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
            return errno;
    }

    return errno;
} //PAF_AST_decodeHandleErrorInput

// -----------------------------------------------------------------------------
