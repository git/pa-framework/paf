
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Function Definitions
//
//     Audio Framework is Audio Streams 1-N for IROM.
//
//
//

#ifndef AS_COMMON_
#define AS_COMMON_

// .............................................................................

#include <stdio.h>
#include <std.h>
#include <alg.h>
//#include <ialg.h>
//#include <sio.h>
//#include <mem.h>
//#include <tsk.h>
//#include <string.h> // memset
//#include <sts.h>
//#include <clk.h>

// .............................................................................

#include <paf_alg.h>
#include <paf_ialg.h>
#include <pafsio.h>
#include <paferr.h>

#include <acp.h>
#include <acp_mds.h>

#include <pcm.h>

#include <pce.h>
#include <pceerr.h>

#include <dob.h>
#include <doberr.h>
#include <outbuf.h>

#include <dib.h>
#include <diberr.h>
#include <inpbuf.h>

#include <paftyp.h>
#include <stdasp.h>
#include <acpbeta.h>
#include <ccm.h>

#include <as0.h>
#include <asp0.h>
#include <asperr.h>

#include <paf_alg_print.h>
#include "pafhjt.h"

// .............................................................................

#include <ztop.h>
//#include <arc_ext.h>

//#include "statStruct.h"  // A concise structure for debugging

// .............................................................................
//
// Audio Topology (Zone) Declarations
//

#define DECODEN_MAX 3
#define STREAMN_MAX 5
#define ENCODEN_MAX 3

//
// Audio Framework Configuration
//

#define GEARS   4   /* All, Nil, Std, Cus */
#define SERIES  4   /* Std, Alt, Oem, Cus */


#include <AS_params.h>
#include <AS_patchs.h>
#include <AS_config.h>
#include <paf_alg_algkey.h>

/* ---------------------------------------------------------------- */
/* For historical reasons, macro definitions (#define ...) are used */
/* to hide parameter references (pP->...) throughout this file, but */
/* only for select quantities and not for all parameter elements.   */
/*                                                                  */
/*             Parameter macro definitions start here.              */
/* ---------------------------------------------------------------- */

//
// Audio Topology (Zone) Definitions
//
//   The ZONE, a historical term here, is a letter identifying the Audio
//   Topology with a single letter or "*" for a variable quantization of
//   same.
//
//   The Zone Elements listed here indicate the cardinality of the corre-
//   sponding element:
//
//     INPUTS   Number of inputs.
//     DECODES  Number of decodes.
//     STREAMS  Number of streams.
//     ENCODES  Number of encodes.
//     OUTPUTS  Number of outputs.
//
//   The Zone Element Counts listed here indicate the first (1) and after-
//   last (N) values suitable for use in a loop:
//
//     INPUTS[1N]  for inputs.
//     DECODES[1N] for decodes.
//     STREAMS[1N] for streams.
//     ENCODES[1N] for encodes.
//     OUTPUTS[1N] for outputs.
//
//   The Zone Element Count Limits listed here establish array sizes:
//
//     DECODEN_MAX for decodes.
//     STREAMN_MAX for streams.
//     ENCODEN_MAX for encodes.
//
//   The Zone Master is important in multi-input frameworks:
//
//     MASTER   In a uni-input zone, the count of the input.
//              In a multi-input zone, the count of the primary which controls
//              the secondary or secondaries.
//

#ifndef ZONE

#define ZONE "*" /* 19.53 kB of 38.80 kB FW */

#define MASTER  pP->zone.master
#define INPUTS  pP->zone.inputs
#define DECODES pP->zone.decodes
#define STREAMS pP->zone.streams
#define ENCODES pP->zone.encodes
#define OUTPUTS pP->zone.outputs

#endif /* ZONE */

#define INPUT1  pP->zone.input1
#define INPUTN  pP->zone.inputN

#define DECODE1 pP->zone.decode1
#define DECODEN pP->zone.decodeN
// DECODEN_MAX defined above

#define STREAM1 pP->zone.stream1
#define STREAMN pP->zone.streamN
// STREAMN_MAX defined above

#define ENCODE1 pP->zone.encode1
#define ENCODEN pP->zone.encodeN
// ENCODEN_MAX defined above

#define OUTPUT1 pP->zone.output1
#define OUTPUTN pP->zone.outputN

// .............................................................................

//
// Heap & Memory Allocation Definitions
//

#define HEAP_INTERNAL *pP->heap.pIntern
#define HEAP_INTERNAL1 *pP->heap.pIntern1
#define HEAP_EXTERNAL *pP->heap.pExtern
#define HEAP_CLEAR pP->heap.clear

#define HEAP_INPBUF *pP->heap.pInpbuf
#define HEAP_OUTBUF *pP->heap.pOutbuf
#define HEAP_FRMBUF *pP->heap.pFrmbuf

#define COMMONSPACE pP->common.space

//
// Audio Stream Definitions
//

#define DEC_MODE_CONTINUOUS (1<<1)

#define RX_BUFSIZE(Z) pP->z_rx_bufsiz[Z]

#define TX_NUMCHAN(Z) pP->z_numchan[Z] /* as per stream numchan */
#define TX_BUFSIZE(Z) pP->z_tx_bufsiz[Z]

//
// Audio Data Representation Definitions
//

/* audio frame "width" in channels */

#define numchan pP->z_numchan

/* audio frame "length" in samples */

#define FRAMELENGTH pP->framelength

Int PAF_AST_decodeHandleErrorInput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int z, Int error);

// minimum audio frame "length" in samples (should be multiple of 8)
#define MINFRAMELENGTH 24

// if using ARC then may need extra room in audio buffers
// this value must be >= 1
/* uSDK - Initialize to 1 to get correct value for  pAudioFrame->sampleRate in ARC_TIH_reset - even if ARC is diabled in chain */
//double arcRatio;                                                                             // KR032013
Int PAF_AST_computeRateRatio (const PAF_AST_Params *pP, PAF_AST_Config *pC, double *arcRatio); // KR032013

//
// Decoder Definitions
//

#define decLinkInit pQ->i_decLinkInit

//
// Audio Stream Processing Definitions
//

#define aspLinkInit pQ->i_aspLinkInit

//
// Encoder Definitions
//

#define encLinkInit pQ->i_encLinkInit

/* ---------------------------------------------------------------- */
/*              Parameter macro definitions end here.               */
/* ---------------------------------------------------------------- */

//
// Standardized Definitions
//

#define DEC_Handle PCM_Handle /* works for all: SNG, PCM, AC3, DTS, AAC */
#define ENC_Handle PCE_Handle /* works for all: PCE */

#define DECSIOMAP(X)                                                \
    pP->pDecSioMap->map[(X) >= pP->pDecSioMap->length ? 0 : (X)]

// -----------------------------------------------------------------------------

Int PAF_AST_initPhaseMalloc (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_initPhaseConfig (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_initPhaseAcpAlg (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_initPhaseCommon (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_initPhaseAlgKey (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_initPhaseDevice (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);

Int PAF_AST_initFrame0 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z);
Int PAF_AST_initFrame1 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z, Int apply);
Int PAF_AST_passProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int hack);
Int PAF_AST_passProcessingCopy (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_autoProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int inputTypeSelect, ALG_Handle pcmAlgMaster);
// PAF_AST_decodeProcessing
Int PAF_AST_decodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_encodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC);
Int PAF_AST_decodeInit (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[]);
// PAF_AST_decodeInfo
// PAF_AST_decodeInfo1
// PAF_AST_decodeInfo2
// PAF_AST_decodeCont
// PAF_AST_decodeDecode
// PAF_AST_decodeStream
// PAF_AST_decodeEncode
Int PAF_AST_decodeFinalTest (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block);
Int PAF_AST_decodeComplete (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block);
// PAF_AST_selectDevices
Int PAF_AST_sourceDecode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int x);
// PAF_AST_startOutput
// PAF_AST_stopOutput
// PAF_AST_setCheckRateX
Int PAF_AST_streamChainFunction (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int iChainFrameFxns, Int abortOnError, Int logArg);

Int PAF_AST_decodeHandleErrorInput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int z, Int error);

#endif // AS_COMMON_
