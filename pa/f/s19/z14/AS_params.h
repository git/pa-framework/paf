
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Parameter Declarations
//
//
//

#ifndef AS_PARAMS_
#define AS_PARAMS_

#include <std.h>
#include <alg.h>
#include <ialg.h>
#include <sio.h>

#include <asp0.h>

#include <acp.h>

#include <paftyp.h>
#include <inpbuf.h>
#include <pafdec.h>
#include <pafenc.h>
#include <pafvol.h>
#include <outbuf.h>

//#include <AS_common.h>

//
// Source Select Arrays -- algorithm keys & sio map
//

typedef struct PAF_ASP_AlgKey {
    Int length;
    PAF_ASP_AlphaCode code[PAF_SOURCE_N /* ostensibly */];
} PAF_ASP_AlgKey;

typedef struct PAF_ASP_SioMap {
    Int length;
    SmInt map[PAF_SOURCE_N /* ostensibly */];
} PAF_ASP_SioMap;

typedef struct PAF_ASP_outNumBufMap {
	Int maxNumBuf;
    Int length;
    SmInt map[PAF_SOURCE_N];
} PAF_ASP_outNumBufMap;

//
// Audio Framework Functions
//

struct PAF_AST_Params;
struct PAF_AST_Patchs;
struct PAF_AST_Config;

typedef struct PAF_AST_Fxns {
    Int (*initPhase[8]) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*initFrame0) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
    Int (*initFrame1) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int, Int);
    Int (*passProcessing) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
    Int (*passProcessingCopy) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*autoProcessing) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int, ALG_Handle);
    Int (*decodeProcessing) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle);
    Int (*decodeCommand) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*encodeCommand) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*decodeInit) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *);
    Int (*decodeInfo) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeInfo1) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeInfo2) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeCont) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeDecode) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeStream) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeEncode) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeFinalTest) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*decodeComplete) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, ALG_Handle *, Int, Int);
    Int (*selectDevices) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*sourceDecode) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
    Int (*startOutput) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, double); // KR032013
    Int (*stopOutput) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *);
    Int (*setCheckRateX) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
    Int (*streamChainFunction) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int, Int, Int);
    Int (*deviceAllocate) (SIO_Handle *, int, int, int, Ptr);
    Int (*deviceSelect) (SIO_Handle *, int, int, Ptr);
    Int (*computeFrameLength) (ALG_Handle, Int, Int);
    Int (*updateInputStatus) (SIO_Handle, PAF_InpBufStatus *, PAF_InpBufConfig *);
    Int (*copy) (Uns, PAF_InpBufConfig *, Uns, PAF_OutBufConfig *);

#ifdef HSE
    Int (*setPri) (const struct PAF_AST_Params *, const struct PAF_AST_Patchs *, struct PAF_AST_Config *, Int);
#endif

    // For RAM_report
    Void (*headerPrint)();
    Int  (*allocPrint)(const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p);
    Void (*commonPrint)(IALG_MemRec common[], PAF_IALG_Config *p);
    Void (*bufMemPrint)(Int z,Int size, Int heap,Int bufType);
    Void (*memStatusPrint)(Int internal,Int external,Int internal1);

    // For ARC
  //Int (*controlRate) (SIO_Handle, SIO_Handle, ACP_Handle);         // KR032013
    Int (*controlRate) (SIO_Handle, SIO_Handle, ACP_Handle, double); // KR032013

} PAF_AST_Fxns;

//
// Audio Framework Parameters
//

typedef struct PAF_AST_Params {
    const PAF_AST_Fxns *fxns;
    struct {
        SmInt master;
        SmInt inputs;
        SmInt input1;
        SmInt inputN;
        SmInt decodes;
        SmInt decode1;
        SmInt decodeN;
        SmInt streams;
        SmInt stream1; /* unused */
        SmInt streamN; /* unused */
        SmInt encodes;
        SmInt encode1;
        SmInt encodeN;
        SmInt outputs;
        SmInt output1;
        SmInt outputN;
    } zone;
    const SmInt *inputsFromDecodes;
    const SmInt *outputsFromEncodes;
    struct {
        int *pIntern;
        int *pExtern;
        int *pInpbuf;
        int *pOutbuf;
        int *pFrmbuf;
        int *pIntern1;
        int clear; 
    } heap;
    struct {
        const IALG_MemSpace *space;
    } common;
    const LgInt *z_rx_bufsiz;
    const LgInt *z_tx_bufsiz;
    const SmInt *z_numchan;
    MdInt framelength;
    const PAF_AudioFunctions *pAudioFrameFunctions;
    const struct PAF_ASP_ChainFxns *pChainFxns;
    const PAF_InpBufStatus *pInpBufStatus;
    const PAF_DecodeStatus * const *z_pDecodeStatus;
    const PAF_OutBufStatus *pOutBufStatus;
    const PAF_EncodeStatus * const *z_pEncodeStatus;
    const PAF_VolumeStatus *pVolumeStatus;
    const PAF_ASP_AlgKey *pDecAlgKey;
    const PAF_ASP_AlgKey *pEncAlgKey;
    const PAF_ASP_SioMap *pDecSioMap;
    const SmInt *streamsFromDecodes;
    const SmInt *streamsFromEncodes;
    const MdInt maxFramelength;
    const SmInt *streamOrder;
    const PAF_ASP_LinkInit * const (*i_inpLinkInit);
    const PAF_ASP_LinkInit * const (*i_outLinkInit);
    const PAF_ASP_outNumBufMap *  const (*poutNumBufMap);
} PAF_AST_Params;

#endif  /* AS_PARAMS_ */

