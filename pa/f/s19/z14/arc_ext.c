
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Function Definitions
//
//     Audio Framework is Audio Streams 1-N for IROM.
//
//
//

// .............................................................................
// Audio Stream Task / Asynchronous Rate Conversion (ARC) - Control
//
//   Name:      PAF_ARC_controlRate
//   Purpose:   Control ARC conversion rate
//   From:      AST Parameter Function -> controlRate
//   Uses:      See code.
//   States:    None.
//   Return:    0 on success.
//              Other on SIO Control failure (using SIO error numbers).
//   Trace:     None.
//

#include <std.h>

#include <as0.h>
#include <pafdec.h>
#include <pafenc.h>
#include <asperr.h> /* ASPERR_*OUT_* */
#include <pafsio.h>
#include "pafhjt.h"

#include <arc_ext.h>

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 0.
#define CURRENT_TRACE_MASK  0

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // describe startup behavior
#define TRACE_MASK_VERBOSE  4   // trace full operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

#ifndef ALL_SYNCHRONOUS_ZSS
Int PAF_ARC_controlRateAsInputA(SIO_Handle hRxSio, SIO_Handle hTxSio, ACP_Handle acp, double arcRatio); // KR032013
#endif //ALL_SYNCHRONOUS_ZSS
Int PAF_ARC_controlRateAsInputB(SIO_Handle hRxSio, SIO_Handle hTxSio, ACP_Handle acp, double arcRatio); // KR032013
Int PAF_ARC_controlRateAsOutput(SIO_Handle hRxSio, SIO_Handle hTxSio, ACP_Handle acp, double arcRatio); // KR032013

// -----------------------------------------------------------------------------

#include <math.h>    /* ldexp() */
#include <arc_a.h>
#if defined (ALL_SYNCHRONOUS_ZSS)
    #include "alpha/pa_zss_evmda830_io_a.h"
#elif defined (ASYNC_SINGLE_ZSA)
    #include "alpha/pa_zsa_evmda830_io_a.h"
#elif defined (ASYNC_DUAL_ZAA)
    #include "alpha/pa_zaa_evmda830_io_a.h"
#else
    #error Unknown synchronicity mode.
#endif

/// can't use "PAF_AST_InpBuf *xInp" -- needs #include "as1-f2-config.h"
/// use "    LOG_Obj *log,"?

// -----------------------------------------------------------------------------

#ifndef ALL_SYNCHRONOUS_ZSS

#ifdef USE_EXT_RAM
    #pragma CODE_SECTION (PAF_ARC_controlRateAsInputA, ".text:PAF_ARC_controlRateAsInputA")
#endif // USE_EXT_RAM
Int
PAF_ARC_controlRateAsInputA(
    SIO_Handle hRxSio,
    SIO_Handle hTxSio,
    ACP_Handle acp,
    double arcRatio)  // KR032013
{
    Int errno;

    if (hRxSio && hTxSio) {
        PAF_SIO_Stats *pRxStats, *pTxStats;

        XDAS_UInt32 inputsPerOutputQ24 = (XDAS_UInt32) ldexp( arcRatio, 24);

        static const ACP_Unit
            readARCOutputsRemainingQ24_s[] = { readARCOutputsRemainingQ24 },
            wroteARCInputsPerOutputQ24_s[] = { wroteARCInputsPerOutputQ24 };

        ACP_Unit selectStream4[2]       = { streamInputA, };
        ACP_Unit selectStreamDefault[2] = { defaultStream, };

        ACP_Unit y4[4];

        if (errno = SIO_ctrl (hRxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pRxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: Error retrieving Rx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        if (errno = SIO_ctrl (hTxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pTxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: Error retrieving Tx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        // choose the stream of this instance
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, selectStream4, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: stream select error"
                " (0x%04x)", errno));
            return errno;
        }

        y4[0] = wroteARCInputsPerOutputQ24_s[0];
        y4[1] = wroteARCInputsPerOutputQ24_s[1];
        y4[2] = (MdInt) inputsPerOutputQ24;
        y4[3] = (MdInt)(inputsPerOutputQ24 >> 16);

        // write the control value
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, y4, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: Error sending ARC rate ratio"
                " (0x%04x)", errno));
            return errno;
        }

        // read back the control value.  Not sure why.
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, readARCOutputsRemainingQ24_s, y4)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: Error retrieving ARC timing"
                " (0x%04x)", errno));
            return errno;
        }

        // restore default stream
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, selectStreamDefault, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputA: restore default stream select error"
                " (0x%04x)", errno));
            return errno;
        }
    }

    return 0;
}

#endif //ALL_SYNCHRONOUS_ZSS
// -----------------------------------------------------------------------------

#ifdef USE_EXT_RAM
    #pragma CODE_SECTION (PAF_ARC_controlRateAsInputB, ".text:PAF_ARC_controlRateAsInputB")
#endif // USE_EXT_RAM
Int
PAF_ARC_controlRateAsInputB(
    SIO_Handle hRxSio,
    SIO_Handle hTxSio,
    ACP_Handle acp,
    double arcRatio)  // KR032013
{
    Int errno;

    if (hRxSio && hTxSio) {
        PAF_SIO_Stats *pRxStats, *pTxStats;
        XDAS_UInt32 inputsPerOutputQ24 = (XDAS_UInt32) ldexp( arcRatio, 24);

        static const ACP_Unit
            readARCOutputsRemainingQ24_s[] = { readARCOutputsRemainingQ24 },
            wroteARCInputsPerOutputQ24_s[] = { wroteARCInputsPerOutputQ24 };

        ACP_Unit selectStream5[2]       = { streamInputB, };
        ACP_Unit selectStreamDefault[2] = { defaultStream, };

        ACP_Unit y4[4];

        if (errno = SIO_ctrl (hRxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pRxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: Error retrieving Rx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        if (errno = SIO_ctrl (hTxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pTxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: Error retrieving Tx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        // select this stream
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB.%d: Null apply function.", __LINE__));
        }
        else
        {
        	errno = acp->fxns->apply (acp, selectStream5, NULL);
        	if (errno) {
        		TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: stream select error (0x%04x)", errno));
        		return errno;
        	}
        }

        y4[0] = wroteARCInputsPerOutputQ24_s[0];
        y4[1] = wroteARCInputsPerOutputQ24_s[1];
        y4[2] = (MdInt) inputsPerOutputQ24;
        y4[3] = (MdInt)(inputsPerOutputQ24 >> 16);
        // write the control value
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, y4, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: Error sending ARC rate ratio"
                " (0x%04x)", errno));
            return errno;
        }
        // read back control value
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, readARCOutputsRemainingQ24_s, y4)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: Error retrieving ARC timing"
                " (0x%04x)", errno));
            return errno;
        }
        // restore the default stream
        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, selectStreamDefault, NULL)) { // restore default
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsInputB: restore default stream select error"
                " (0x%04x)", errno));
            return errno;
        }

    }
    return 0;
}

// -----------------------------------------------------------------------------

#ifdef USE_EXT_RAM
    #pragma CODE_SECTION (PAF_ARC_controlRateAsOutput, ".text:PAF_ARC_controlRateAsOutput")
#endif // USE_EXT_RAM
Int
PAF_ARC_controlRateAsOutput(
    SIO_Handle hRxSio,
    SIO_Handle hTxSio,
    ACP_Handle acp,
    double arcRatio)  // KR032013
{
    Int errno;

    if (hRxSio && hTxSio) {
        PAF_SIO_Stats *pRxStats, *pTxStats;
        XDAS_UInt32 inputsPerOutputQ24 = (XDAS_UInt32) ldexp( arcRatio, 24);

        static const ACP_Unit
            readARCOutputsRemainingQ24_s[] = { readARCOutputsRemainingQ24 },
            wroteARCInputsPerOutputQ24_s[] = { wroteARCInputsPerOutputQ24 };

        ACP_Unit selectStream1[2]       = { streamOutputMaster, };
        ACP_Unit selectStreamDefault[2] = { defaultStream, };

        ACP_Unit y4[4];

        if (errno = SIO_ctrl (hRxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pRxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: Error retrieving Rx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        if (errno = SIO_ctrl (hTxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pTxStats)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: Error retrieving Tx xfer stats"
                " (0x%04x)", errno));
            return errno;
        }

        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, selectStream1, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: stream select error"
                " (0x%04x)", errno));
            return errno;
        }

        y4[0] = wroteARCInputsPerOutputQ24_s[0];
        y4[1] = wroteARCInputsPerOutputQ24_s[1];
        y4[2] = (MdInt) inputsPerOutputQ24;
        y4[3] = (MdInt)(inputsPerOutputQ24 >> 16);

        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, y4, NULL)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: Error sending ARC rate ratio"
                " (0x%04x)", errno));
            return errno;
        }

        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, readARCOutputsRemainingQ24_s, y4)) {
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: Error retrieving ARC timing"
                " (0x%04x)", errno));
            return errno;
        }

        if (acp->fxns->apply == NULL)
        {
        	TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput.%d: Null apply function.", __LINE__));
        }
        else if (errno = acp->fxns->apply (acp, selectStreamDefault, NULL)) { // restore default
            TRACE_TERSE((&TR_MOD, "PAF_ARC_controlRateAsOutput: restore default stream select error"
                " (0x%04x)", errno));
            return errno;
        }

    }

    return 0;
}
