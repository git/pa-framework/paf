
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework -- pre-BIOS initialization
//
//
//

#include <std.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef DSPLINK
#include <hal_interrupt.h>
#endif /* DSPLINK */

// macros like ENABLE_GPP_DRI to be defined at build time
#ifndef ENABLE_DSP_DSP_RINGIO  // nothing has been set, default to legacy.
  #define ENABLE_GPP_DRI
  #define ENABLE_GPP_DRO
#endif

#define DSPLINK_DSP_ONLY_MODE (defined(DSPLINK) && (!defined(ENABLE_GPP_DRI) || !defined(ENABLE_GPP_DRO) ) )

#include <ztop.h>
volatile Int gStartupOrder = 0;

#ifndef dMAX_CFG
extern void dmax_init ();
#endif /* dMAX_CFG */

#ifndef ALL_SYNCHRONOUS_ZSS
  extern Void AS_InputA_Idle (void);
#endif //ALL_SYNCHRONOUS_ZSS
extern Void AS_Output_Idle (void);
extern Void ACP_main_std ();
extern Void ACP_main_cus ();


// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

#define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------


/*
 *  ======== main ========
 */

#ifndef dMAX_CFG
#include <dmax.h>
#else
#include <dmax_struct.h>
#include <dmax_params.h>
#endif /* dMAX_CFG */

#include <pafhjt.h>

extern void dat_enable ();

#if(PAF_DEVICE) == 0xD8000001
extern const PAFHJT_t *pafhjt;
#else
const PAFHJT_t *pafhjt;
#endif

#ifdef DSPLINK
#if DSPLINK_DSP_ONLY_MODE
  extern void dspOnlyLinkInit (void);
#else
extern void AFP_LINK_initIsr (Ptr arg);
extern volatile unsigned int DSPLINK_initFlag;
extern Bool PA_DSPLINK_isInitialized;
#endif /* DSPLINK_DSP_ONLY_MODE */
#endif /* DSPLINK */

#ifndef SIMULATOR
void board_init (void);
#endif /* SIMULATOR */

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
  /* Include the legacy csl 2.0 dat header */
  #include <csl_dat.h>

  /* Include EDMA3 low level driver specific implementation APIs for dat */
  //#include <csl2_dat_edma3lld.h>
  /** EDMA3 Driver Handle, which is used to call all the Driver APIs.
   * It gets initialized during EDMA3 Initialization.
   */
  //extern EDMA3_DRV_Handle hEdma;
  //extern EDMA3_DRV_Handle DAT_EDMA3LLD_hEdma;

  #ifdef RAM_REPORT

    #ifndef ALL_SYNCHRONOUS_ZSS
      #include <AS_InputA-params.h>
    #endif //ALL_SYNCHRONOUS_ZSS
    #include <AS_InputB-params.h>
    #include <AS_Output-params.h>

    #include <paf_alg_print.h>

    #ifndef ALL_SYNCHRONOUS_ZSS
      extern PAF_AST_Fxns PAF_INA_params_fxnsPA17;
    #endif //ALL_SYNCHRONOUS_ZSS
    extern PAF_AST_Fxns PAF_INB_params_fxnsPA17;
    extern PAF_AST_Fxns PAF_OUT_params_fxnsPA17;

  #endif // RAM_REPORT
#endif // PAF_DEVICE


extern void printBannerString(void);

main (Int argc, String argv[])
{
    pafhjt = &PAFHJT_RAM;

    printBannerString();  // kept in a separate file to speed recompile

#ifndef SIMULATOR
    board_init ();
#endif /* SIMULATOR */

    // Patch for the CUStom alpha codes.
    ACP_main_cus ();

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)

#if DSPLINK_DSP_ONLY_MODE
    dspOnlyLinkInit();

#else
#ifdef DSPLINK
#ifdef PA_NO_BOOT
    /* register the init ISR */
    HAL_intRegister (5, 4, (Fxn) & AFP_LINK_initIsr, 0);
#else
    DSPLINK_init ();
    PA_DSPLINK_isInitialized = TRUE;
#endif
#endif /* DSPLINK */
#endif /* DSPLINK_DSP_ONLY_MODE */

#if 0
    // Initialize EDMA
    if (!DAT_EDMA3LLD_init (NULL))
	{
		printf ("Error initializing EDMA3 low level driver\n");
		exit (3);
	}
	else
	{
		DAT_EDMA3LLD_hEdma = hEdma;
	}
	edma_DAT_open (0, 0, 0);
#endif
#ifdef RAM_REPORT
  #ifndef ALL_SYNCHRONOUS_ZSS
    PAF_INA_params_fxnsPA17.headerPrint    = PAF_ALG_headerPrint;
    PAF_INA_params_fxnsPA17.allocPrint     = PAF_ALG_allocPrint;
    PAF_INA_params_fxnsPA17.commonPrint    = PAF_ALG_commonPrint;
    PAF_INA_params_fxnsPA17.bufMemPrint    = PAF_ALG_bufMemPrint;
    PAF_INA_params_fxnsPA17.memStatusPrint = PAF_ALG_memStatusPrint;
  #endif //ALL_SYNCHRONOUS_ZSS

    PAF_INB_params_fxnsPA17.headerPrint    = PAF_ALG_headerPrint;
    PAF_INB_params_fxnsPA17.allocPrint     = PAF_ALG_allocPrint;
    PAF_INB_params_fxnsPA17.commonPrint    = PAF_ALG_commonPrint;
    PAF_INB_params_fxnsPA17.bufMemPrint    = PAF_ALG_bufMemPrint;
    PAF_INB_params_fxnsPA17.memStatusPrint = PAF_ALG_memStatusPrint;

    PAF_OUT_params_fxnsPA17.headerPrint    = PAF_ALG_headerPrint;
    PAF_OUT_params_fxnsPA17.allocPrint     = PAF_ALG_allocPrint;
    PAF_OUT_params_fxnsPA17.commonPrint    = PAF_ALG_commonPrint;
    PAF_OUT_params_fxnsPA17.bufMemPrint    = PAF_ALG_bufMemPrint;
    PAF_OUT_params_fxnsPA17.memStatusPrint = PAF_ALG_memStatusPrint;
#endif
#ifndef dMAX_CFG
    // CSL_init (); // temporarily removed due to build issues - need to see if this is required.
    dmax_init ();

    // Open the DAT module
    DAT_open (dMAX_DATANY, dMAX_MEMCPY_PRI4 + 8, 7);
    DAT_open (dMAX_DATANY, dMAX_MEMCPY_PRI4 + 8, 0);
    DAT_open (dMAX_DATANY, dMAX_MEMCPY_PRI4 + 8, 0);
    DAT_open (dMAX_DATANY, dMAX_MEMCPY_PRI4 + 8, 0);
#else
    dMAX_init (dMAX_HANDLE);
#if 0
    // Open the DAT module
    {
        int i;

        for (i = 0; i < 4; ++i)
            DAT_open (NONE_EVENT, dMAX_MEMCPY_PRI4, 0);
    }
#endif
#endif /* dMAX_CFG */
#endif /* PAF_DEVICE */

    TRACE((&trace, "main.c completed."));
}

// -----------------------------------------------------------------------------
// This function is used to copy BIOS data from SDRAM to RAM_BIOSROM to avoid
// conflict with secondary boot loader or boot loader patch.

extern const unsigned int RamBiosromData_start;				// Start address of RAM_BIOSROM_DATA segment

void copyBiosRomData (void)
{
	unsigned char *src, *dst;
	unsigned int i;

	const unsigned int *START_ADR = &RamBiosromData_start;

	src = (unsigned char *) START_ADR;
	dst = (unsigned char *) 0x11800020;
	for (i = 0; i < 0xFE0; i++)
		*dst++ = *src++;
}

// -----------------------------------------------------------------------------
