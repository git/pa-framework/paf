
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

//
// IROM-Inclusion Definitions
//

//
// File includes
//

#include <std.h>

#include <acp.h>
#include <pafsys.h>

#include <ss0.h>

#include <AS_common.h>
#include <AS_params.h>
#include <AS_patchs.h>
#include <ztop.h>

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

#define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------

//
// System Stream 1: AS_InputA_Idle
//

extern PAF_SST_FxnsMain     systemStreamMain;
extern PAF_SST_FxnsCompute  systemStream1Compute;
extern PAF_SST_FxnsTransmit systemStream1Transmit;
extern PAF_SST_FxnsCompute  systemStream2Compute;
extern PAF_SST_FxnsTransmit systemStream2Transmit;
extern PAF_SST_FxnsCompute  systemStream3Compute;
extern PAF_SST_FxnsTransmit systemStream3Transmit;
#ifdef THX
extern PAF_SST_FxnsCompute  systemStream4Compute;
extern PAF_SST_FxnsTransmit systemStream4Transmit;
#endif // THX
extern PAF_SST_FxnsCompute  systemStream5Compute;
extern PAF_SST_FxnsTransmit systemStream5Transmit;

//#define DISABLE_SYSTEM_STREAM

const struct {
    PAF_SST_FxnsMain *main;
    Int count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[5];
} systemStream1Fxns =
#ifdef DISABLE_SYSTEM_STREAM
{
    &systemStreamMain,
    5,
    {
        { NULL, NULL, }, // { &systemStream1Compute, &systemStream1Transmit, },
        { NULL, NULL, }, // { &systemStream2Compute, &systemStream2Transmit, },
        { NULL, NULL, }, // { &systemStream3Compute, &systemStream3Transmit, },
        { NULL, NULL, }, // THX
        { NULL, NULL, }, // { &systemStream5Compute, &systemStream5Transmit, },
    },
};
#else
{
    &systemStreamMain,
    5,
    {
        { &systemStream1Compute, &systemStream1Transmit, }, // Decoder channel configuration
        { NULL, NULL, }, // { &systemStream2Compute, &systemStream2Transmit, }, // Bass Management output configuration
        { NULL, NULL, }, // { &systemStream3Compute, &systemStream3Transmit, }, // S/PDIF Deemphasis -- needs update: ss0.c
#ifdef THX
        { &systemStream4Compute, &systemStream4Transmit, },
#else
        { NULL, NULL, },
#endif
        { NULL, NULL, }, // { &systemStream5Compute, &systemStream5Transmit, }, // CPU load
    },
};
#endif
/* .......................................................................... */

const PAF_SST_Params systemStream1Params[1] =
{
    {
        1,  // streams
        0,  // stream1
        1,  // streamN
        AS_InputA_betaPrimeValue+1, // This is ss,  betaPrimeValue = ss -1 
        (const PAF_SST_Fxns *)&systemStream1Fxns,
    },
};

PAF_SystemStatus systemStream1Status[1] = {
    {
        sizeof (PAF_SystemStatus),
        PAF_SYS_MODE_ALL,
        0,
        PAF_SYS_RECREATIONMODE_AUTO,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        2 + PAF_SYS_SPEAKERSIZE_SMALL,
        1 + PAF_SYS_SPEAKERSIZE_BASS,
        PAF_SYS_CCRTYPE_STANDARD,       // channelConfigurationRequestType
        0, 0, 0,                        // switchImage, imageNum, imageNumMax
        { PAF_CC_SAT_UNKNOWN, 0, 0, 0}, // channelConfigurationRequest.part
          // "Only the first member of a union can be initialized"
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
    },
};

PAF_SST_Config systemStream1Config[1] =
{
    {
        NULL,
        &systemStream1Status[0],
    },
};

/* .......................................................................... */
// This function is called repeatedly in the SYS/BIOS idle loop.
void AS_InputA_Idle(void)
{
    Int i;

    for (i=0; i < lengthof (systemStream1Config); i++)
    {
        systemStream1Params[i].fxns->main(&systemStream1Params[i], &systemStream1Config[i]);
    }

    // Ensure that all idle threads run before framework threads.
    // Each thread that starts sets a bit in gStartupOrder.
    gStartupOrder |= ZTOP_INPUT_A_IDLE_BIT;
}
