
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

// PAF_DEVICE_VERSION Symbol Definitions

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

//
// Framework Declarations
//

#include <asp1.h>
#include <stdasp.h>
#include <AS_common.h>
#include <AS_params.h>
#include <pafsio_ialg.h>
#include <ztop.h>
#include <io.h>

#define PAF_INB_params_fxnsPA PAF_INB_params_fxnsPA17

#define MAX_ARC_RATIO   (48./32.)
#define MAX_FRAMELENGTH ((ZTOP_FRAMESIZE * MAX_ARC_RATIO)+32)

//
// Audio Data Representation Definitions
//
//   External declarations to patched IROM provide standard functionality.
//

/* audio frame "width" in channels for z_numchan */

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (PAF_INB_params_numchan, ".text:PAF_INB_params_numchan")
#endif // USE_EXT_RAM
const SmInt PAF_INB_params_numchan[STREAMN_MAX] =
{
    IO_NUM_CHANS_INPUTB,        // stream betaPrimevalue+1
    0,                          // stream betaPrimevalue+2
    0,                          // stream betaPrimevalue+3
    0,                          // stream betaPrimevalue+4
    0,                          // stream betaPrimevalue+5
};

///
// Audio Stream Processing Function Table Definition
//

const PAF_AudioFunctions PAF_INB_params_audioFrameFunctions =
{
    &PAF_ASP_dB2ToLinear,
    &PAF_ASP_channelMask,
    &PAF_ASP_programFormat,
    &PAF_ASP_sampleRateHz,
    &PAF_ASP_delay,
};


//
// Source Select Array Declarations -- algorithm keys & sio map
//
//   External declarations to patched IROM provide standard functionality.
//

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (PAF_INB_params_decAlgKey, ".text:PAF_INB_params_decAlgKey")
#endif // USE_EXT_RAM
const PAF_ASP_AlgKey PAF_INB_params_decAlgKey =
{
    PAF_SOURCE_N,                       // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                  // PAF_SOURCE_UNKNOWN
    0,                                  // PAF_SOURCE_NONE
    0,                                  // PAF_SOURCE_PASS
    PAF_ASP_ALPHACODE (STD, SNG),       // PAF_SOURCE_SNG
    0,                                  // PAF_SOURCE_AUTO
    0,                                  // PAF_SOURCE_BITSTREAM
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTSALL
    PAF_ASP_ALPHACODE (STD, PCM),       // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCM),       // PAF_SOURCE_PCM
    PAF_ASP_ALPHACODE (STD, PCN),       // PAF_SOURCE_PC8 /* unused */
    PAF_ASP_ALPHACODE (STD, DDP),       // PAF_SOURCE_AC3
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTS
    PAF_ASP_ALPHACODE (STD, AAC),       // PAF_SOURCE_AAC
    PAF_ASP_ALPHACODE (STD, MPG),       // PAF_SOURCE_MPEG /* unused */
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTS12
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTS13
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTS14
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTS16
    0,                                  // PAF_SOURCE_WMA9PRO
    0,                                  // PAF_SOURCE_MP3
    0,                                  // PAF_SOURCE_DSD1
    0,                                  // PAF_SOURCE_DSD2
    0,                                  // PAF_SOURCE_DSD3
    PAF_ASP_ALPHACODE (STD, DDP),       // PAF_SOURCE_DDP
    PAF_ASP_ALPHACODE (STD, DTSHD),     // PAF_SOURCE_DTSHD
    PAF_ASP_ALPHACODE (STD, THD),       // PAF_SOURCE_THD
    PAF_ASP_ALPHACODE (STD, DXP),       // PAF_SOURCE_DXP
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (PAF_INB_params_encAlgKey, ".text:PAF_INB_params_encAlgKey")
#endif // USE_EXT_RAM
const PAF_ASP_AlgKey PAF_INB_params_encAlgKey =
{
    PAF_SOURCE_N,                       // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                  // PAF_SOURCE_UNKNOWN
    0,                                  // PAF_SOURCE_NONE
    0,                                  // PAF_SOURCE_PASS
    0,                                  // PAF_SOURCE_SNG
    0,                                  // PAF_SOURCE_AUTO
    0,                                  // PAF_SOURCE_BITSTREAM
    0,                                  // PAF_SOURCE_DTSALL
    0,                                  // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCE),       // PAF_SOURCE_PCM
    0,                                  // PAF_SOURCE_PC8
    0,                                  // PAF_SOURCE_AC3
    0,                                  // PAF_SOURCE_DTS
    0,                                  // PAF_SOURCE_AAC
    0,                                  // PAF_SOURCE_MPEG
    0,                                  // PAF_SOURCE_DTS12
    0,                                  // PAF_SOURCE_DTS13
    0,                                  // PAF_SOURCE_DTS14
    0,                                  // PAF_SOURCE_DTS16
    0,                                  // PAF_SOURCE_WMA9PRO
    PAF_ASP_ALPHACODE (STD, MPE),       // PAF_SOURCE_MP3
    0,                                  // PAF_SOURCE_DSD1
    0,                                  // PAF_SOURCE_DSD2
    0                                   // PAF_SOURCE_DSD3
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (PAF_INB_params_decSioMap, ".text:PAF_INB_params_decSioMap")
#endif // USE_EXT_RAM
const PAF_ASP_SioMap PAF_INB_params_decSioMap =
{
    PAF_SOURCE_N,                       // length
    PAF_SOURCE_UNKNOWN,                 // PAF_SOURCE_UNKNOWN   -> ...
    PAF_SOURCE_NONE,                    // PAF_SOURCE_NONE      -> ...
    PAF_SOURCE_PASS,                    // PAF_SOURCE_PASS      -> ...
    PAF_SOURCE_PCM,                     // PAF_SOURCE_SNG       -> PCM
    PAF_SOURCE_AUTO,                    // PAF_SOURCE_AUTO      -> ...
    PAF_SOURCE_BITSTREAM,               // PAF_SOURCE_BITSTREAM -> ...
    PAF_SOURCE_DTSALL,                  // PAF_SOURCE_UNUSED1   -> ...
    PAF_SOURCE_PCMAUTO,                 // PAF_SOURCE_UNUSED2   -> ...
    PAF_SOURCE_PCM,                     // PAF_SOURCE_PCM       -> ...
    PAF_SOURCE_PC8,                     // PAF_SOURCE_PC8       -> ...
    PAF_SOURCE_AC3,                     // PAF_SOURCE_AC3       -> ...
    PAF_SOURCE_DTS,                     // PAF_SOURCE_DTS       -> ...
    PAF_SOURCE_AAC,                     // PAF_SOURCE_AAC       -> ...
    PAF_SOURCE_MPEG,                    // PAF_SOURCE_MPEG      -> ...
    PAF_SOURCE_DTS12,                   // PAF_SOURCE_DTS12     -> ...
    PAF_SOURCE_DTS13,                   // PAF_SOURCE_DTS13     -> ...
    PAF_SOURCE_DTS14,                   // PAF_SOURCE_DTS14     -> ...
    PAF_SOURCE_DTS16,                   // PAF_SOURCE_DTS16     -> ...
    PAF_SOURCE_WMA9PRO,                 // PAF_SOURCE_WMA9PRO -> ...
    PAF_SOURCE_MP3,                     // PAF_SOURCE_MP3 -> ...
    PAF_SOURCE_DSD1,                    // PAF_SOURCE_DSD1      -> ...
    PAF_SOURCE_DSD2,                    // PAF_SOURCE_DSD2      -> ...
    PAF_SOURCE_DSD3,                    // PAF_SOURCE_DSD3      -> ...
    PAF_SOURCE_DDP,                     // PAF_SOURCE_DDP       -> ...
    PAF_SOURCE_DTSHD,                   // PAF_SOURCE_DTSHD     -> ...
    PAF_SOURCE_THD,                     // PAF_SOURCE_THD       -> ...
    PAF_SOURCE_DXP,                     // PAF_SOURCE_DXP -> ...
};

//
// Mapping Declarations -- from *coders to *puts
//
//   External declarations to patched IROM provide standard functionality.
//
const SmInt PAF_AST_streamsFromDecodes_std_InputB[DECODEN_MAX] =
{
    0, 1, 2,
};

#define PAF_INB_streamsFromDecodes_std PAF_AST_streamsFromDecodes_std_InputB

const SmInt PAF_AST_streamsFromEncodes_std_InputB[ENCODEN_MAX] =
{
    0, 1, 2,
};

#define PAF_INB_streamsFromEncodes_std PAF_AST_streamsFromEncodes_std_InputB

const SmInt PAF_AST_inputsFromDecodes_std_InputB[DECODEN_MAX] =
{
    0, 1, 2, 
};

#define PAF_INB_inputsFromDecodes_std PAF_AST_inputsFromDecodes_std_InputB

extern const SmInt PAF_AST_outputsFromEncodes_std_InputB[ENCODEN_MAX];
asm("_PAF_AST_outputsFromEncodes_std_InputB .set _PAF_AST_inputsFromDecodes_std_InputB");

#define PAF_INB_outputsFromEncodes_std PAF_AST_outputsFromEncodes_std_InputB

//
// Setting of audio stream order
//

const SmInt PAF_AST_streamOrder_InputB[STREAMN_MAX] =
{
    0, 1, 2, 3, 4,
};

#define PAF_INB_streamOrder PAF_AST_streamOrder_InputB

//
// Audio Framework Initialization Function Table Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

extern const PAF_AST_Fxns PAF_INB_params_fxnsPA;



//
// Audio Framework Status Initialization Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_InpBufStatus PAF_INB_params_inpBufStatus_InputB =
{
    sizeof (PAF_InpBufStatus),          // size
    0,                                  // mode
    0,                                  // sioSelect
    0x80,                               // lockOverride = none
    0,                                  // unused
    PAF_SAMPLERATE_UNKNOWN,             // sampleRateOverride
    PAF_SAMPLERATE_NONE,                // sampleRateData
    PAF_SAMPLERATE_NONE,                // sampleRateMeasured
    PAF_SAMPLERATE_UNKNOWN,             // sampleRateStatus
    PAF_IEC_AUDIOMODE_UNKNOWN,          // nonaudio
    PAF_IEC_PREEMPHASIS_UNKNOWN,        // emphasisData
    0,                                  // emphasisOverride
    PAF_IEC_PREEMPHASIS_NO,             // emphasisStatus
    0,                                  // lock
    0,                                  // scanAtHighSampleRateMode
    0,                                  // zeroRun
    0,                                  // rateTrackMode
    24,                                 // precisionDefault
    -1,                                 // precisionDetect
    0,                                  // precisionOverride
    0,                                  // precisionInput
    100,                                // zeroRunTrigger
    100,                                // zeroRunRestart
    2048,                               // unknownTimeout
    0,                                  // lastFrameMask
    0,                                  // lastFrameFlag
    0,                                  // reportDTS16AsDTSForLargeSampleRate
    0, 0                                // reserved
};

#define PAF_INB_params_inpBufStatus PAF_INB_params_inpBufStatus_InputB

const PAF_DecodeStatus PAF_INB_params_decodeStatus_InputB =
{
    sizeof (PAF_DecodeStatus),          // size
    1,                                  // mode
    0,                                  // unused1
    0,                                  // command.action
    0,                                  // command.result
    PAF_SAMPLERATE_UNKNOWN,             // sampleRate
    PAF_SOURCE_NONE,                    // sourceSelect
    PAF_SOURCE_UNKNOWN,                 // sourceProgram
    PAF_SOURCE_UNKNOWN,                 // sourceDecode
    PAF_SOURCE_DUAL_STEREO,             // sourceDual
    4,                                  // sourceKaraoke: both
    0,                                  // aspGearControl: unused
    0,                                  // aspGearStatus: unused
    PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND2_UNKNOWN, 0,
                                        // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationProgram.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationDecode.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationDownmix.full
    0,                                  // programFormat.mask
    0,                                  // programFormat.form
    0,                                  // frameCount
    0x40,                               // karaoka: Vocal 1 Level
    0x40,                               // karaoka: Vocal 1 Pan
    0x40,                               // karaoka: Vocal 2 Level
    0xc0,                               // karaoka: Vocal 2 Pan
    0x40,                               // karaoka: Melody Level
    0x00,                               // karaoka: Melody Pan
    0,                                  // decBypass
    0,                                  // unused
    -3,                                 // channelMap.from[0]
    -3,                                 // channelMap.from[1]
    -3,                                 // channelMap.from[2]
    -3,                                 // channelMap.from[3]
    -3,                                 // channelMap.from[4]
    -3,                                 // channelMap.from[5]
    -3,                                 // channelMap.from[6]
    -3,                                 // channelMap.from[7]
    -3,                                 // channelMap.from[8]
    -3,                                 // channelMap.from[9]
    -3,                                 // channelMap.from[10]
    -3,                                 // channelMap.from[11]
    -3,                                 // channelMap.from[12]
    -3,                                 // channelMap.from[13]
    -3,                                 // channelMap.from[14]
    -3,                                 // channelMap.from[15]
    -3,                                 // channelMap.to[0]
    -3,                                 // channelMap.to[1]
    -3,                                 // channelMap.to[2]
    -3,                                 // channelMap.to[3]
    -3,                                 // channelMap.to[4]
    -3,                                 // channelMap.to[5]
    -3,                                 // channelMap.to[6]
    -3,                                 // channelMap.to[7]
    -3,                                 // channelMap.to[8]
    -3,                                 // channelMap.to[9]
    -3,                                 // channelMap.to[10]
    -3,                                 // channelMap.to[11]
    -3,                                 // channelMap.to[12]
    -3,                                 // channelMap.to[13]
    -3,                                 // channelMap.to[14]
    -3,                                 // channelMap.to[15]
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationOverride.full
    0,                                  // frameLength: reset later
    1,                                  // bufferRatio: unity
    PAF_IEC_PREEMPHASIS_UNKNOWN,        // emphasis
};

const PAF_DecodeStatus *const PAF_INB_params_decodeStatus[] =
{
    &PAF_INB_params_decodeStatus_InputB,
};

const PAF_OutBufStatus PAF_INB_params_outBufStatus =
{
    sizeof (PAF_OutBufStatus),          // size
    1,                                  // mode
    0,                                  // sioSelect
    PAF_SAMPLERATE_UNKNOWN,             // sampleRate
    0,                                  // audio
    PAF_OB_CLOCK_INTERNAL,              // clock
    PAF_OB_FLUSH_ENABLE,                // flush
    0,                                  // rateTrackMode
    0,                                  // PAF_OB_MARKER_ENABLED
};

const PAF_EncodeStatus PAF_INB_params_encodeStatus_InputB =
{
    sizeof (PAF_EncodeStatus),          // size
    1,                                  // mode
    0,                                  // unused1
    PAF_SAMPLERATE_UNKNOWN,             // sampleRate
    0,                                  // channelCount
    PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND2_UNKNOWN, 0,
                                        // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationStream.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,
                                        // channelConfigurationEncode.full
    0,                                  // programFormat.mask
    0,                                  // programFormat.form
    0,                                  // listeningFormat.mask
    0,                                  // listeningFormat.form
    0,                                  // frameLength
    1,                                  // encBypass    :  true for DRO-DRI link
    PAF_SOURCE_PCM,                     // select
    -3,                                 // channelMap.from[0]
    -3,                                 // channelMap.from[1]
    -3,                                 // channelMap.from[2]
    -3,                                 // channelMap.from[3]
    -3,                                 // channelMap.from[4]
    -3,                                 // channelMap.from[5]
    -3,                                 // channelMap.from[6]
    -3,                                 // channelMap.from[7]
    -3,                                 // channelMap.from[8]
    -3,                                 // channelMap.from[9]
    -3,                                 // channelMap.from[10]
    -3,                                 // channelMap.from[11]
    -3,                                 // channelMap.from[12]
    -3,                                 // channelMap.from[13]
    -3,                                 // channelMap.from[14]
    -3,                                 // channelMap.from[15]
    -3,                                 // channelMap.to[0]
    -3,                                 // channelMap.to[1]
    -3,                                 // channelMap.to[2]
    -3,                                 // channelMap.to[3]
    -3,                                 // channelMap.to[4]
    -3,                                 // channelMap.to[5]
    -3,                                 // channelMap.to[6]
    -3,                                 // channelMap.to[7]
    -3,                                 // channelMap.to[8]
    -3,                                 // channelMap.to[9]
    -3,                                 // channelMap.to[10]
    -3,                                 // channelMap.to[11]
    -3,                                 // channelMap.to[12]
    -3,                                 // channelMap.to[13]
    -3,                                 // channelMap.to[14]
    -3,                                 // channelMap.to[15]
    0,                                  // sampleProcess[0]
};

const PAF_EncodeStatus *const PAF_INB_params_encodeStatus[] =
{
    &PAF_INB_params_encodeStatus_InputB,
};

const PAF_VolumeStatus PAF_INB_params_volumeStatus =
{
    sizeof (PAF_VolumeStatus),          // size
    1,                                  // mode
    PAF_MAXNUMCHAN,                     // channelCount
    0x0f,                               // implementation
    0,                                  // unused1
    50,                                 // rampTime: 50 msec/dB (20 dB/sec)
    0,                                  // unused2
    0,                                  // unused3
    -2*20, 0, 0, 0,                     // master
    -2*0, 0, 0, 0,                      // trim
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2*0, 0, 0, 0,                      // 
    -2 * 0, 0, 0, 0,                    // trim - upper16
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //
    -2 * 0, 0, 0, 0,                    //

};

//
// Common Space Parameter Declarations and Definitions
//
//   Local definitions in RAM provide non-standard functionality.
//   The NULL pointer provides standard functionality.
//



/* baseline definition - NULL equivalent */
/* May be used for overrides of IALG_MemSpace */

static const IALG_MemSpace params_memspace_PAz_InputB[] = {
    PAF_IALG_NONE, // Scratch
    PAF_IALG_NONE, // Persistant
    PAF_IALG_NONE, // Write once
    PAF_IALG_NONE, // Common  1
    PAF_IALG_NONE, // Common  2
    PAF_IALG_NONE, // Common  3
    PAF_IALG_NONE, // Common  4
    PAF_IALG_NONE, // Common  5
    PAF_IALG_NONE, // Common  6
    PAF_IALG_NONE, // Common  7
    IALG_EXTERNAL, // Common  8
    PAF_IALG_NONE, // Common  9
    PAF_IALG_NONE, // Common 10
    PAF_IALG_NONE, // Common 11
    PAF_IALG_NONE, // Common 12
    PAF_IALG_NONE, // Common 13
    PAF_IALG_NONE, // Common 14
    PAF_IALG_NONE, // Common 15
};



//
// Heap Declarations
//

#include <pafhjt.h>

extern int IRAM;
extern int SDRAM;
extern int L3RAM;

// .............................................................................
// DIB memory requirements

const IALG_MemRec inpMemTab_InputB[] =
{
    // circular buffer written to by encoder and read by DRO.
    // DRO only uses the first section.
    {
        (2*ZTOP_FRAMESIZE*IO_NUM_CHANS_INPUTB*4),   // size
        128,            // alignment
        IALG_EXTERNAL,  // space
        IALG_PERSIST,   // attrs
        NULL,           // base
    },

    // IRAM scratch memory for autodetection and stereo PCM input
    //      High watermark needs are set by the latter:
    //      double buffer stereo 32bit PCM input 512 max frame size
    //      1 buffers * 512 samples/buffer * 2 words/sample * 4 bytes/word
    {
        8*1024,          // size
        128,             // alignment
        IALG_EXTERNAL,      // space  (was IALG_SARAM, moved to external to make memory for 3 streams.)
        IALG_SCRATCH,    // attrs
        NULL,            // base
    }
};

const PAF_SIO_IALG_Params inpSioAlgParams_InputB =
{
    1,          // numRec
    inpMemTab_InputB // *pMemRec
};

const PAF_ASP_LinkInit inpLinkInit_InputB[] =
{
    PAF_ASP_LINKINITPARAMS (STD,IB,TIH,&inpSioAlgParams_InputB),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit * const PAF_INB_inpLinkInit[] =
{
    inpLinkInit_InputB
};

// .............................................................................
// DOB memory requirements

const IALG_MemRec outMemTab_InputB[] =
{
    // SDRAM buffer
    {
        (2*ZTOP_FRAMESIZE*IO_NUM_CHANS_INPUTB*4),   // size
        128,            // alignment
        IALG_EXTERNAL,  // space
        IALG_PERSIST,   // attrs
        NULL,           // base
     }
};

const PAF_SIO_IALG_Params outSioAlgParams_InputB =
{
    1,          // numRec
    outMemTab_InputB // *pMemRec
};

const PAF_ASP_LinkInit outLinkInit_InputB[] =
{
    PAF_ASP_LINKINITPARAMS (STD,OB,TIH,&outSioAlgParams_InputB),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit * const PAF_INB_outLinkInit[] =
{
    outLinkInit_InputB
};

// .............................................................................
// sourceProgram mapped to DOB num of buffers
const PAF_ASP_outNumBufMap out3NumBufMap=
{
    2,                 // maxNumBuf
    PAF_SOURCE_N,      // length
    0,                 // PAF_SOURCE_UNKNOWN
    0,                 // PAF_SOURCE_NONE
    2,                 // PAF_SOURCE_PASS
    2,                 // PAF_SOURCE_SNG
    0,                 // PAF_SOURCE_AUTO
    0,                 // PAF_SOURCE_BITSTREAM
    0,                 // PAF_SOURCE_DTSALL
    0,                 // PAF_SOURCE_PCMAUTO
    2,                 // PAF_SOURCE_PCM
    0,                 // PAF_SOURCE_PC8
    2,                 // PAF_SOURCE_AC3
    2,                 // PAF_SOURCE_DTS
    2,                 // PAF_SOURCE_AAC
    0,                 // PAF_SOURCE_MPEG
    2,                 // PAF_SOURCE_DTS12
    2,                 // PAF_SOURCE_DTS13
    2,                 // PAF_SOURCE_DTS14
    2,                 // PAF_SOURCE_DTS16
    0,                 // PAF_SOURCE_WMA9PRO
    0,                 // PAF_SOURCE_MP3
    2,                 // PAF_SOURCE_DSD1,
    2,                 // PAF_SOURCE_DSD2,
    2,                 // PAF_SOURCE_DSD3,
    0,                 // PAF_SOURCE_DDP
    0,                 // PAF_SOURCE_DTSHD
    0,                 // PAF_SOURCE_THD
    0,                 // PAF_SOURCE_DXP

};

const PAF_ASP_outNumBufMap * const PAF_INB_outNumBufMap[] =
{
    &out3NumBufMap,
};

// .............................................................................

//
// Audio Stream Parameter Definitions
//
//   Global definition in RAM provides standard & non-standard functionality.
//

const PAF_AST_Params params_PAz_InputB =
{
    &PAF_INB_params_fxnsPA, // fxns
    { // zone
        0,      // master
        1,      // inputs
        0,      // input1
        1,      // inputN
        1,      // decodes
        0,      // decode1
        1,      // decodeN
        1,      // streams
        0,      // stream1
        1,      // streamN
        1,      // encodes
        0,      // encode1
        1,      // encodeN
        1,      // outputs
        0,      // output1
        1,      // outputN
    },
    PAF_INB_inputsFromDecodes_std,
    PAF_INB_outputsFromEncodes_std,
    { // heap
        &IRAM,  // pIntern
        &SDRAM, // pExtern
        &IRAM,  // pInpbuf
        &IRAM,  // pOutbuf
        &IRAM,  // pFrmbuf
        &L3RAM, // pIntern1
        1,      // clear
    },
    { // common
        params_memspace_PAz_InputB, // space
    },
    NULL,                                       // z_rx_bufsiz
    NULL,                                       // z_tx_bufsiz
    PAF_INB_params_numchan,                     // z_numchan
    ZTOP_FRAMESIZE,                             // framelength
    &PAF_INB_params_audioFrameFunctions,        // pAudioFrameFunctions
    &PAF_ASP_chainFxns,                         // pChainFxns
    &PAF_INB_params_inpBufStatus,               // pInpBufStatus
    PAF_INB_params_decodeStatus,                // z_pDecodeStatus
    &PAF_INB_params_outBufStatus,               // pOutBufStatus
    PAF_INB_params_encodeStatus,                // z_pEncodeStatus
    &PAF_INB_params_volumeStatus,               // pVolumeStatus
    &PAF_INB_params_decAlgKey,                  // pDecAlgKey
    &PAF_INB_params_encAlgKey,                  // pEncAlgKey
    &PAF_INB_params_decSioMap,                  // pDecSioMap
    PAF_INB_streamsFromDecodes_std,             // streamsFromDecodes
    PAF_INB_streamsFromEncodes_std,             // streamsFromEncodes
    MAX_FRAMELENGTH,                            // maxFramelength
    PAF_INB_streamOrder,                        // streamOrder
    PAF_INB_inpLinkInit,                        // i_inpLinkInit
    PAF_INB_outLinkInit,                        // i_outLinkInit
    PAF_INB_outNumBufMap                        // outNumBufMap
};
// EOF
