
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//

//
// Special symbols to exclude features (if defined)
//

#define NOAAC    // AAC
#define NOPL2X   // Dolby Pro Logic

// #define EXCLUDE_ALL_ASPS
#ifdef EXCLUDE_ALL_ASPS
 // for debugging.
 #define NODDP    // Dolby Digital
 #define NODM    // Downmix ASP
 #define NOLOU   // Loudness (GEQ) ASPs
#else  // normal case:  These are enabled.
  //#define NODDP    // Dolby Digital
 // #define NODM    // Downmix ASP
 // #define NOLOU   // Loudness (GEQ) ASPs
#endif
#define NOBM    // Bass Management ASP
#define NODEM   // De-emphasis ASP
#define NOGEQ   // Graphic EQ ASPs
#define NOML    // MIPS Load ASP
#define NOMTX   // Matrix ASP
#define NORVB   // Room Simulator ASP
#define NOSRC   // Synchronous Rate Conversion ASP

//#define AE0   // Audio Examples
//#define AE_1
//#define AE_2
//#define AE_3

//
// Framework Declarations
//

#include <AS_common.h>
#include <AS_patchs.h>
#include <asp0.h>
#include <asp1.h>

//
// Decoder Definitions
//

#include <pcm.h>
#include <pcm_mds.h>

#include <sng.h>
#include <sng_mds.h>

#ifndef NOAAC
#include <aac.h>
#include <aac_tii.h>
#endif // NOAAC

#ifndef NODDP
#include <ddp.h>
#include <ddp_tij.h>
#endif // NODDP

#include <dwr_inp.h>

const PAF_ASP_LinkInit decLinkInitZ14_InputB[] =
{
    PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
#if(PAF_DEVICE) == 0xD8000001
#ifndef NOAAC
    PAF_ASP_LINKINITPARAMS (STD, DWRAAC, TII, &IDWRAAC_PARAMS),
#endif // NOAAC
#ifndef NODDP
    PAF_ASP_LINKINITPARAMS (STD, DDP, TIJ, &IDDP_PARAMS_SDRAM),
#endif // NODDP
    PAF_ASP_LINKINIT (STD, SNG, MDS),
#endif // (PAF_DEVICE) == 0xD8000001
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit *const patchs_decLinkInitZ14_InputB[] =
{
    decLinkInitZ14_InputB,
};

//
// Audio Stream Processing Declarations & Definitions
//

#ifdef AE0
#include <ae.h>
#include <ae_mds.h>
#endif

#if defined (AE_1) || defined (AE_2) || defined (AE_3)
#include <ae.h>
#include <ae_tii.h>
#endif /* !defined(NOAE_1) || !defined(NOAE_2) || !defined(NOAE_3) */

#ifndef NOARC
#include <arc.h>
#include <arc_tih.h>
#endif  // NOARC

#ifndef NODEM
#include <dem.h>
#include <dem_mds.h>
#endif  // NODEM

#ifndef NOMTX
#include <mtx.h>
#include <mtx_wav.h>
#endif  // NOMTX

#ifndef NOPL2X
#include <pl2x.h>
#include <pl2x_tii.h>
#endif  // NOPL2X

#ifndef NORVB
#include <rvb.h>
#include <rvb_wav.h>
#endif  // NORVB

#ifndef NODM
#include <dm.h>
#include <dm_tii.h>
#endif  // NODM

#if ! defined (NOGEQ) || ! defined (NOLOU)
#include <geq.h>
#include <geq_tii.h>
#define LOU_TII_init GEQ_TII_init
#define LOU_TII_ILOU GEQ_TII_IGEQ
#endif  // NOGEQ NOLOU

#ifndef NOBM
#include <bm.h>
#include <bm_mds.h>
#endif  // NOBM

#ifndef NOML
#include <ml.h>
#include <ml_mds.h>
#endif  // NOML

#include <fil.h>
#include <fil_tii.h>

extern const IFIL_Params IBGC_PARAMS;
#define BGC_TII_init FIL_TII_init
#define BGC_TII_IBGC FIL_TII_IFIL

#ifndef NOSRC
#include <src.h>
#include <src_tih.h>
#define SUC_TIH_init SRC_TIH_init
#define SUC_TIH_ISUC SRC_TIH_ISRC
#endif  // NOSRC

#include <aspstd.h>

const PAF_ASP_LinkInit aspLinkInitAllZ14_InputB[] =
{
#ifdef AE0
    PAF_ASP_LINKINIT(CUS,AE,MDS),
#endif
#ifdef AE_1
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_DIRECT),
#endif
#ifdef AE_2
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_SIMPLE_DMA),
#endif
#ifdef AE_3
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_CONCURRENT_DMA),
#endif /* AE_3 */
#ifndef NOSRC
    PAF_ASP_LINKINITPARAMS (STD, SRC, TIH, &ISRC_PARAMS_DS_8CH_HBW),
#endif  // NOSRC
#ifndef NODEM
    PAF_ASP_LINKINIT (STD, DEM, MDS),
#endif  // NODEM

#if(PAF_DEVICE) == 0xD8000001
#ifndef NOPL2X
    PAF_ASP_LINKINIT (STD, PL2x, TII),
#endif  // NOPL2X
#ifndef NOMTX
    PAF_ASP_LINKINIT (STD, MTX, WAV),
#endif  // NOMTX
#ifndef NORVB
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
#endif  // NORVB
#ifndef NODM
    PAF_ASP_LINKINIT (STD, DM, TII),
#endif  // NODM
#ifndef NOBM
    PAF_ASP_LINKINITPARAMS(STD,BM,MDS,&IBM_PARAMS_256),
#endif  // NOBM
#ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
#endif  // NOGEQ
#ifndef NOLOU
    PAF_ASP_LINKINITPARAMS (STD, LOU, TII, &IGEQ_PARAMS_LOU),
#endif  // NOLOU
#ifdef BASS
    PAF_ASP_LINKINIT(STD,BASS,TIJ),
#endif /* BASS */
#ifndef NOARC
    PAF_ASP_LINKINIT(STD,ARC,TIH),
#endif  // NOARC
#ifndef NOML
    PAF_ASP_LINKINIT (STD, ML, MDS),
#endif  // NOML
#endif /* (PAF_DEVICE) == 0xD8000001 */
    PAF_ASP_LINKNONE,
};

#define aspLinkInitNilZ14_InputB NULL

const PAF_ASP_LinkInit aspLinkInitStdZ14_InputB[] =
{
#ifndef NOSRC
    PAF_ASP_LINKINITPARAMS (STD, SRC, TIH, &ISRC_PARAMS_DS_8CH_HBW),
#endif  // NOSRC
#ifndef NODEM
    PAF_ASP_LINKINIT (STD, DEM, MDS),
#endif  // NODEM
#if(PAF_DEVICE) == 0xD8000001
#ifndef NOPL2X
    PAF_ASP_LINKINIT (STD, PL2x, TII),
#endif  // NOPL2X
#ifndef NOMTX
     PAF_ASP_LINKINIT (STD, MTX, WAV),
#endif  // NOMTX
#ifndef NORVB
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
#endif  // NORVB
#ifndef NODM
    PAF_ASP_LINKINIT (STD, DM, TII),
#endif  // NODM
#ifndef NOBM
    PAF_ASP_LINKINITPARAMS(STD,BM,MDS,&IBM_PARAMS_256),
#endif  // NOBM
#ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
#endif  // NOGEQ
#ifndef NOLOU
    PAF_ASP_LINKINITPARAMS (STD, LOU, TII, &IGEQ_PARAMS_LOU),
#endif  // NOLOU
#ifdef BASS
    PAF_ASP_LINKINIT(STD,BASS,TIJ),
#endif /* BASS */
#ifndef NOARC
    PAF_ASP_LINKINIT(STD,ARC,TIH),
#endif  // NOARC
#ifndef NOML
    PAF_ASP_LINKINIT (STD, ML, MDS),
#endif  // NOML
#endif /* (PAF_DEVICE) == 0xD8000001 */
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit aspLinkInitCusZ14_InputB[] =
{
#ifdef AE0
    PAF_ASP_LINKINIT (CUS, AE, MDS),
#endif
#ifdef AE_1
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_DIRECT),
#endif /* AE_1 */
#ifdef AE_2
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_SIMPLE_DMA),
#endif /* AE_2 */
#ifdef AE_3
    PAF_ASP_LINKINITPARAMS(CUS,AE,TII,&IAE_PARAMS_CONCURRENT_DMA),
#endif /* AE_3 */
#ifndef NOARC
    PAF_ASP_LINKINIT(STD,ARC,TIH),
#endif  // NOARC
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_aspLinkInitZ14_InputB[1][GEARS] =
{
    {
     aspLinkInitAllZ14_InputB,
     aspLinkInitNilZ14_InputB,
     aspLinkInitStdZ14_InputB,
     aspLinkInitCusZ14_InputB,
    },
};

//
// Encoder Definitions
//

#include <pce.h>
#include <pce_tii.h>

extern const IPCE_Params IPCE_PARAMS_DOLBY;
const PAF_ASP_LinkInit encLinkInitZ14_InputB[] =
{
    PAF_ASP_LINKINITPARAMS(STD,PCE,TII,&IPCE_PARAMS_DOLBY),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_encLinkInitZ14_InputB[1] =
{
    encLinkInitZ14_InputB,
};

//
// Audio Stream Patch Definition
//

extern const PAF_SIO_ParamsN patchs_devinp[];
extern const PAF_SIO_ParamsN patchs_devout[];

const PAF_AST_Patchs patchs_PAz_InputB =
{
    patchs_devinp,
    patchs_devout,
    patchs_decLinkInitZ14_InputB,
    patchs_aspLinkInitZ14_InputB,
    patchs_encLinkInitZ14_InputB,
};

// EOF
