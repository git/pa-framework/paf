
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//

//
// IROM-Inclusion Definitions
//

//
// File includes
//

#include <std.h>

#include <acp.h>
#include <pafsys.h>

#include <ss3.h>

#include <AS_common.h>
#include <AS_params.h>
#include <AS_patchs.h>
#include <ztop.h>

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

#define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------

//
// System Stream 3: AS_InputB_Idle
//

extern PAF_SST3_FxnsMain     ss3_systemStreamMain;
extern PAF_SST3_FxnsCompute  ss3_systemStream1Compute;
extern PAF_SST3_FxnsTransmit ss3_systemStream1Transmit;
extern PAF_SST3_FxnsCompute  ss3_systemStream2Compute;
extern PAF_SST3_FxnsTransmit ss3_systemStream2Transmit;
extern PAF_SST3_FxnsCompute  ss3_systemStream3Compute;
extern PAF_SST3_FxnsTransmit ss3_systemStream3Transmit;
#ifdef THX
extern PAF_SST3_FxnsCompute  ss3_systemStream4Compute;
extern PAF_SST3_FxnsTransmit ss3_systemStream4Transmit;
#endif // THX
extern PAF_SST3_FxnsCompute  ss3_systemStream5Compute;
extern PAF_SST3_FxnsTransmit ss3_systemStream5Transmit;

//#define DISABLE_SYSTEM_STREAM

const struct {
    PAF_SST3_FxnsMain *main;
    Int count;
    struct {
        PAF_SST3_FxnsCompute  *compute;
        PAF_SST3_FxnsTransmit *transmit;
    } sub[5];
} ss3_systemStream1Fxns =
#ifdef DISABLE_SYSTEM_STREAM
{
    &ss3_systemStreamMain,
    5,
    {
        { NULL, NULL, }, // { &ss3_systemStream1Compute, &ss3_systemStream1Transmit, },
        { NULL, NULL, }, // { &ss3_systemStream2Compute, &ss3_systemStream2Transmit, },
        { NULL, NULL, }, // { &ss3_systemStream3Compute, &ss3_systemStream3Transmit, },
        { NULL, NULL, }, // THX
        { NULL, NULL, }, // { &ss3_systemStream5Compute, &ss3_systemStream5Transmit, },
    },
};
#else
{
    &ss3_systemStreamMain,
    5,
    {
        { &ss3_systemStream1Compute, &ss3_systemStream1Transmit, }, // Decoder channel configuration
        { &ss3_systemStream2Compute, &ss3_systemStream2Transmit, }, // Bass Management output configuration
        { NULL, NULL, }, // { &ss3_systemStream3Compute, &ss3_systemStream3Transmit, }, // S/PDIF Deemphasis -- needs update: ss3.c
#ifdef THX
        { &ss3_systemStream4Compute, &ss3_systemStream4Transmit, },
#else
        { NULL, NULL, },
#endif
        { NULL, NULL, }, // { &ss3_systemStream5Compute, &ss3_systemStream5Transmit, }, // CPU load
    },
};
#endif
/* .......................................................................... */

const PAF_SST3_Params ss3_systemStream1Params[1] =
{
    {
        1,  // streams
        0,  // stream1
        1,  // streamN
        AS_InputB_betaPrimeValue+1, // ss:  betaPrimeValue = ss -1 
        (const PAF_SST3_Fxns *)&ss3_systemStream1Fxns,
    },
};

PAF_SystemStatus ss3_systemStream1Status[1] = {
    {
        sizeof (PAF_SystemStatus),      // size
        PAF_SYS_MODE_ALL,               // mode
        0,                              // listeningMode
        PAF_SYS_RECREATIONMODE_AUTO,    // recreationMode
        2 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerMain
        1 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerCntr
        2 + PAF_SYS_SPEAKERSIZE_SMALL,  // speakerSurr
        0,                              // speakerBack
        1 + PAF_SYS_SPEAKERSIZE_BASS,   // speakerSubw
        PAF_SYS_CCRTYPE_STANDARD,       // channelConfigurationRequestType
        0, 0, 0,                        // switchImage, imageNum, imageNumMax
        { PAF_CC_SAT_UNKNOWN, 0, 0, 0}, // channelConfigurationRequest.part
          // "Only the first member of a union can be initialized"
        0, 0,                           // cpuLoad, peakCpuLoad
        0, 0, 0, 0,                     // speakerWide, speakerHead, unused[2]
    },
};

PAF_SST3_Config ss3_systemStream1Config[1] =
{
    {
        NULL,
        &ss3_systemStream1Status[0],
    },
};

/* .......................................................................... */
// This function is called repeatedly in the SYS/BIOS idle loop.
void AS_InputB_Idle(void)
{
    Int i;

    for (i=0; i < lengthof (ss3_systemStream1Config); i++)
    {
        ss3_systemStream1Params[i].fxns->main(&ss3_systemStream1Params[i], &ss3_systemStream1Config[i]);
    }
    // Ensure that all idle threads run before framework threads.
    // Each thread that starts sets a bit in gStartupOrder.
    gStartupOrder |= ZTOP_INPUT_B_IDLE_BIT;
}


