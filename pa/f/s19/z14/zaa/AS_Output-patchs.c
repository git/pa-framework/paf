
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

//
// Framework Declarations
//

#include <AS_common.h>
#include <AS_patchs.h>
#include <asp0.h>
#include <asp1.h>

//
// Decoder Definitions
//

#include <pcm.h>
#include <pcm_mds.h>

#include <sng.h>
#include <sng_mds.h>

#include <dwr_inp.h>

#include <ztop.h>

// primary input (dummy for timing) and output.
static const PAF_ASP_LinkInit decLinkInitZ14_Output_Master[] =
{
    PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
//    PAF_ASP_LINKINIT (STD, SNG, MDS),
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit decLinkInitZ14_Output_SlaveA[] =
{
    PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit decLinkInitZ14_Output_SlaveB[] =
{
    PAF_ASP_LINKINITPARAMS (STD, DWRPCM, TII, &IDWRPCM_PARAMS),
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit *const patchs_decLinkInitZ14_Output[NUM_AS_OUT_INPUT_STREAMS] =
{
    decLinkInitZ14_Output_Master,
    decLinkInitZ14_Output_SlaveA,
#if (NUM_AS_OUT_INPUT_STREAMS>2)
    decLinkInitZ14_Output_SlaveB,
#endif
};

//
// Audio Stream Processing Declarations & Definitions
//

// Special symbols to exclude features (if defined)

  // #define NOAMIX   // Z-topo Mixer
  #define NOASJ   // Audio Stream Join (Mix)

// #define EXCLUDE_ALL_ASPS
#ifdef EXCLUDE_ALL_ASPS
  #define NOGEQ   // Graphic EQ ASP
  #define NORVB   // Room Simulator ASP
#else
  // #define NOGEQ   // Graphic EQ ASP
  // #define NORVB   // Room Simulator ASP
#endif

#define NOBM    // Bass Management ASP
#define NODEM   // De-emphasis ASP
#define NODM    // Downmix ASP
#define NOLOU   // Loudness (GEQ) ASPs
#define NOML    // MIPS Load ASP
#define NOMTX   // Matrix ASP
#define NOSRC   // Synchronous Rate Conversion ASP

// Audio Examples -- reverse logic compared to above:
//#define AE0   /* optional -- not inserted if commented out */
//#define AE_1  /* optional -- not inserted if commented out */
//#define AE_2  /* optional -- not inserted if commented out */
//#define AE_3  /* optional -- not inserted if commented out */

#ifdef AE0
#include <ae.h>
#include <ae_mds.h>
#endif

#if defined (AE_1) || defined (AE_2) || defined (AE_3)
#include <ae.h>
#include <ae_tii.h>
#endif /* !defined(NOAE_1) || !defined(NOAE_2) || !defined(NOAE_3) */

#ifndef NODEM
#include <dem.h>
#include <dem_mds.h>
#endif  // NODEM

#ifndef NOMTX
#include <mtx.h>
#include <mtx_wav.h>
#endif  // NOMTX

#ifndef NORVB
#include <rvb.h>
#include <rvb_wav.h>
#endif  // NORVB

#ifndef NODM
#include <dm.h>
#include <dm_tii.h>
#endif  // NODM

#if ! defined (NOGEQ) || ! defined (NOLOU)
#include <geq.h>
#include <geq_tii.h>
#define LOU_TII_init GEQ_TII_init
#define LOU_TII_ILOU GEQ_TII_IGEQ
#endif  // NOGEQ NOLOU

#ifndef NOBM
#include <bm.h>
#include <bm_mds.h>
#endif  // NOBM

#ifndef NOAMIX
extern void* AMIX_MDS_IAMIX;
#define AMIX_MDS_init COM_TII_init
#endif  // NOAMIX

#ifndef NOASJ
extern void* ASJ_MDS_IASJ;
#define ASJ_MDS_init COM_TII_init
#endif  // NOASJ

#ifndef NOML
#include <ml.h>
#include <ml_mds.h>
#endif  // NOML

#include <fil.h>
#include <fil_tii.h>

extern const IFIL_Params IBGC_PARAMS;
#define BGC_TII_init FIL_TII_init
#define BGC_TII_IBGC FIL_TII_IFIL

#ifndef NOSRC
#include <src.h>
#include <src_tih.h>
#define SUC_TIH_init SRC_TIH_init
#define SUC_TIH_ISUC SRC_TIH_ISRC
#endif  // NOSRC

#include <aspstd.h>

static const PAF_ASP_LinkInit aspLinkInitAllZ14_Output_Master[] =
{
#ifndef NOAMIX
    PAF_ASP_LINKINIT(STD,AMIX,MDS),
#endif  // NOAMIX
#ifndef NOASJ
    PAF_ASP_LINKINIT(STD,ASJ,MDS),
#endif  // NOASJ
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitAllZ14_Output_slaveA[] =
{
#if defined (ALL_SYNCHRONOUS_ZSS) || defined (ASYNC_SINGLE_ZSA)
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ZSS ZSA
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitAllZ14_Output_slaveB[] =
{
#ifdef ASYNC_DUAL_ZAA
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ASYNC_DUAL_ZAA
    PAF_ASP_LINKNONE,
};

#define aspLinkInitNilZ14_Output_Master NULL
#define aspLinkInitNilZ14_Output_slaveA aspLinkInitNilZ14_Output_Master
#define aspLinkInitNilZ14_Output_slaveB aspLinkInitNilZ14_Output_Master

static const PAF_ASP_LinkInit aspLinkInitStdZ14_Output_Master[] =
{
#ifndef NOAMIX
    PAF_ASP_LINKINIT(STD,AMIX,MDS),
#endif  // NOAMIX
#ifndef NOASJ
    PAF_ASP_LINKINIT(STD,ASJ,MDS),
#endif  // NOASJ
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitStdZ14_Output_slaveA[] =
{
#if defined (ALL_SYNCHRONOUS_ZSS) || defined (ASYNC_SINGLE_ZSA)
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ZSS ZSA
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitStdZ14_Output_slaveB[] =
{
#ifdef ASYNC_DUAL_ZAA
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ASYNC_DUAL_ZAA
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitCusZ14_Output_Master[] =
{
#ifndef NOAMIX
    PAF_ASP_LINKINIT(STD,AMIX,MDS),
#endif  // NOAMIX
#ifndef NOASJ
    PAF_ASP_LINKINIT(STD,ASJ,MDS),
#endif  // NOASJ
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitCusZ14_Output_slaveA[] =
{
#if defined (ALL_SYNCHRONOUS_ZSS) || defined (ASYNC_SINGLE_ZSA)
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ZSS ZSA
    PAF_ASP_LINKNONE,
};

static const PAF_ASP_LinkInit aspLinkInitCusZ14_Output_slaveB[] =
{
#ifdef ASYNC_DUAL_ZAA
  #ifndef NORVB  // substitute for "Upmix"
    PAF_ASP_LINKINITPARAMS (STD, RVB, WAV, &IRVB_PARAMS_PSDELAY),
  #endif  // NORVB
  #ifndef NOGEQ
    PAF_ASP_LINKINITPARAMS (STD, GEQ, TII, &IGEQ_PARAMS_EQX_16CHN_UNICOEF),
  #endif  // NOGEQ
#endif //ASYNC_DUAL_ZAA
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_aspLinkInitZ14_Output[NUM_AS_OUT_INPUT_STREAMS][GEARS] =
{
    {
     aspLinkInitAllZ14_Output_Master,
     aspLinkInitNilZ14_Output_Master,
     aspLinkInitStdZ14_Output_Master,
     aspLinkInitCusZ14_Output_Master,
    },
    {
     aspLinkInitAllZ14_Output_slaveA,
     aspLinkInitNilZ14_Output_slaveA,
     aspLinkInitStdZ14_Output_slaveA,
     aspLinkInitCusZ14_Output_slaveA,
    },
#if (NUM_AS_OUT_INPUT_STREAMS>2)
    {
     aspLinkInitAllZ14_Output_slaveB,
     aspLinkInitNilZ14_Output_slaveB,
     aspLinkInitStdZ14_Output_slaveB,
     aspLinkInitCusZ14_Output_slaveB,
    },
#endif
};

//
// Encoder Definitions
//

#include <pce.h>
#include <pce_tii.h>

extern const IPCE_Params IPCE_PARAMS_DOLBY;

const PAF_ASP_LinkInit encLinkInitZ14_Output[] =
{
    PAF_ASP_LINKINITPARAMS(STD,PCE,TII,&IPCE_PARAMS_DOLBY),
    PAF_ASP_LINKNONE,
};

const PAF_ASP_LinkInit *const patchs_encLinkInitZ14_Output[] =
{
    encLinkInitZ14_Output,
};

//
// Audio Stream Patch Definition
//

extern const PAF_SIO_ParamsN patchs_devinp[];
extern const PAF_SIO_ParamsN patchs_devout[];

const PAF_AST_Patchs patchs_PAz_Output =
{
    patchs_devinp,
    patchs_devout,
    patchs_decLinkInitZ14_Output,
    patchs_aspLinkInitZ14_Output,
    patchs_encLinkInitZ14_Output,
};

// EOF
