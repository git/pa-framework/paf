
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// 
// Definitions governing DRI-DRO link.
// 

#ifndef _DRIO_H
#define _DRIO_H

#include "ztop.h"

#define DRIO_SIZE_OF_ELEMENT    4       // floats
#define DRIO_PRECISION          32      // floats

// .............................................................................

// describes DRI - DRO link "A" used out of AS_InputA
#define DRIO_A_NUM_CHANS        ZTOP_NUM_CHANS_MAX_INPUTA

#if (DRIO_A_NUM_CHANS == 0)
    #define DRIO_A_CH_0   -3
    #define DRIO_A_CH_1   -3
    #define DRIO_A_CH_2   -3
    #define DRIO_A_CH_3   -3
    #define DRIO_A_CH_4   -3
    #define DRIO_A_CH_5   -3
    #define DRIO_A_CH_6   -3
    #define DRIO_A_CH_7   -3
    #define DRIO_A_CH_8   -3
    #define DRIO_A_CH_9   -3
    #define DRIO_A_CH_A   -3
    #define DRIO_A_CH_B   -3
    #define DRIO_A_CH_B   -3
    #define DRIO_A_CH_C   -3
    #define DRIO_A_CH_D   -3
    #define DRIO_A_CH_E   -3
    #define DRIO_A_CH_F   -3
#elif (DRIO_A_NUM_CHANS == 2)
    #define DRIO_A_CH_0   0
    #define DRIO_A_CH_1   1
    #define DRIO_A_CH_2   -3
    #define DRIO_A_CH_3   -3
    #define DRIO_A_CH_4   -3
    #define DRIO_A_CH_5   -3
    #define DRIO_A_CH_6   -3
    #define DRIO_A_CH_7   -3
    #define DRIO_A_CH_8   -3
    #define DRIO_A_CH_9   -3
    #define DRIO_A_CH_A   -3
    #define DRIO_A_CH_B   -3
    #define DRIO_A_CH_C   -3
    #define DRIO_A_CH_D   -3
    #define DRIO_A_CH_E   -3
    #define DRIO_A_CH_F   -3
#elif (DRIO_A_NUM_CHANS == 6)
    #define DRIO_A_CH_0   0
    #define DRIO_A_CH_1   1
    #define DRIO_A_CH_2   2
    #define DRIO_A_CH_3   3
    #define DRIO_A_CH_4   4
    #define DRIO_A_CH_5   5
    #define DRIO_A_CH_6   -3
    #define DRIO_A_CH_7   -3
    #define DRIO_A_CH_8   -3
    #define DRIO_A_CH_9   -3
    #define DRIO_A_CH_A   -3
    #define DRIO_A_CH_B   -3
    #define DRIO_A_CH_C   -3
    #define DRIO_A_CH_D   -3
    #define DRIO_A_CH_E   -3
    #define DRIO_A_CH_F   -3
#elif (DRIO_A_NUM_CHANS == 8)
    #define DRIO_A_CH_0   0
    #define DRIO_A_CH_1   1
    #define DRIO_A_CH_2   2
    #define DRIO_A_CH_3   3
    #define DRIO_A_CH_4   4
    #define DRIO_A_CH_5   5
    #define DRIO_A_CH_6   6
    #define DRIO_A_CH_7   7
    #define DRIO_A_CH_8   -3
    #define DRIO_A_CH_9   -3
    #define DRIO_A_CH_A   -3
    #define DRIO_A_CH_B   -3
    #define DRIO_A_CH_C   -3
    #define DRIO_A_CH_D   -3
    #define DRIO_A_CH_E   -3
    #define DRIO_A_CH_F   -3
#elif (DRIO_A_NUM_CHANS == 12)
    #define DRIO_A_CH_0   0
    #define DRIO_A_CH_1   1
    #define DRIO_A_CH_2   2
    #define DRIO_A_CH_3   3
    #define DRIO_A_CH_4   4
    #define DRIO_A_CH_5   5
    #define DRIO_A_CH_6   6
    #define DRIO_A_CH_7   7
    #define DRIO_A_CH_8   8
    #define DRIO_A_CH_9   9
    #define DRIO_A_CH_A   10
    #define DRIO_A_CH_B   11
    #define DRIO_A_CH_C   -3
    #define DRIO_A_CH_D   -3
    #define DRIO_A_CH_E   -3
    #define DRIO_A_CH_F   -3
#else
  ERROR Unsupported channel number
#endif

#define DRIO_A_ONE_BUFFER  (ZTOP_FRAMESIZE*DRIO_SIZE_OF_ELEMENT*DRIO_A_NUM_CHANS)

// .............................................................................

// describes DRI - DRO link "B" used out of AS_InputB
#define DRIO_B_NUM_CHANS        ZTOP_NUM_CHANS_MAX_INPUTB

#if (DRIO_B_NUM_CHANS == 0)
    #define DRIO_B_CH_0   -3
    #define DRIO_B_CH_1   -3
    #define DRIO_B_CH_2   -3
    #define DRIO_B_CH_3   -3
    #define DRIO_B_CH_4   -3
    #define DRIO_B_CH_5   -3
    #define DRIO_B_CH_6   -3
    #define DRIO_B_CH_7   -3
    #define DRIO_B_CH_8   -3
    #define DRIO_B_CH_9   -3
    #define DRIO_B_CH_A   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_C   -3
    #define DRIO_B_CH_D   -3
    #define DRIO_B_CH_E   -3
    #define DRIO_B_CH_F   -3
#elif (DRIO_B_NUM_CHANS == 2)
    #define DRIO_B_CH_0   0
    #define DRIO_B_CH_1   1
    #define DRIO_B_CH_2   -3
    #define DRIO_B_CH_3   -3
    #define DRIO_B_CH_4   -3
    #define DRIO_B_CH_5   -3
    #define DRIO_B_CH_6   -3
    #define DRIO_B_CH_7   -3
    #define DRIO_B_CH_8   -3
    #define DRIO_B_CH_9   -3
    #define DRIO_B_CH_A   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_C   -3
    #define DRIO_B_CH_D   -3
    #define DRIO_B_CH_E   -3
    #define DRIO_B_CH_F   -3
#elif (DRIO_B_NUM_CHANS == 6)
    #define DRIO_B_CH_0   0
    #define DRIO_B_CH_1   1
    #define DRIO_B_CH_2   2
    #define DRIO_B_CH_3   3
    #define DRIO_B_CH_4   4
    #define DRIO_B_CH_5   5
    #define DRIO_B_CH_6   -3
    #define DRIO_B_CH_7   -3
    #define DRIO_B_CH_8   -3
    #define DRIO_B_CH_9   -3
    #define DRIO_B_CH_A   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_C   -3
    #define DRIO_B_CH_D   -3
    #define DRIO_B_CH_E   -3
    #define DRIO_B_CH_F   -3
#elif (DRIO_B_NUM_CHANS == 8)
    #define DRIO_B_CH_0   0
    #define DRIO_B_CH_1   1
    #define DRIO_B_CH_2   2
    #define DRIO_B_CH_3   3
    #define DRIO_B_CH_4   4
    #define DRIO_B_CH_5   5
    #define DRIO_B_CH_6   6
    #define DRIO_B_CH_7   7
    #define DRIO_B_CH_8   -3
    #define DRIO_B_CH_9   -3
    #define DRIO_B_CH_A   -3
    #define DRIO_B_CH_B   -3
    #define DRIO_B_CH_C   -3
    #define DRIO_B_CH_D   -3
    #define DRIO_B_CH_E   -3
    #define DRIO_B_CH_F   -3
#elif (DRIO_B_NUM_CHANS == 12)
    #define DRIO_B_CH_0   0
    #define DRIO_B_CH_1   1
    #define DRIO_B_CH_2   2
    #define DRIO_B_CH_3   3
    #define DRIO_B_CH_4   4
    #define DRIO_B_CH_5   5
    #define DRIO_B_CH_6   6
    #define DRIO_B_CH_7   7
    #define DRIO_B_CH_8   8
    #define DRIO_B_CH_9   9
    #define DRIO_B_CH_A   10
    #define DRIO_B_CH_B   11
    #define DRIO_B_CH_C   -3
    #define DRIO_B_CH_D   -3
    #define DRIO_B_CH_E   -3
    #define DRIO_B_CH_F   -3
#else
  ERROR Unsupported channel number
#endif

#define DRIO_B_ONE_BUFFER  (ZTOP_FRAMESIZE*DRIO_SIZE_OF_ELEMENT*DRIO_B_NUM_CHANS)

#endif /* _DRIO_H */
