
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//

#include <acptype.h>
#include <pafaip_a.h>
#include "alpha\pa_zaa_evmda830_io_a.h"
#include <arc_a.h>

#include <ztop.h>
#include <DRIO.h>
#include <io.h>

#if ! defined (ASYNC_DUAL_ZAA)
    #error This is atboot for ZAA only.
#endif

// -----------------------------------------------------------------------------
// .............................................................................


    #if ! defined (NUM_AS_OUT_INPUT_STREAMS)
        #error NUM_AS_OUT_INPUT_STREAMS not defined (ztop.h)
    #else
        #if NUM_AS_OUT_INPUT_STREAMS != 3
            #error NUM_AS_OUT_INPUT_STREAMS != 3 (ztop.h)
        #endif
    #endif

#include <amix_a.h>

    // dual async case, uses AMIX, not ASJ
#define CUS_ATBOOT_S \
    /* streamInputA is the first input task */ \
    streamInputA, \
    execPAZInNone, \
    execPAZOutNone, \
    writeDECChannelMapTo16(INPUTA_CH_0, INPUTA_CH_1, INPUTA_CH_2, INPUTA_CH_3, INPUTA_CH_4, INPUTA_CH_5, INPUTA_CH_6, INPUTA_CH_7, INPUTA_CH_8, INPUTA_CH_9, INPUTA_CH_A, INPUTA_CH_B, INPUTA_CH_C, INPUTA_CH_D, INPUTA_CH_E, INPUTA_CH_F), \
    writeENCChannelMapFrom16(INPUTA_CH_0, INPUTA_CH_1, INPUTA_CH_2, INPUTA_CH_3, INPUTA_CH_4, INPUTA_CH_5, INPUTA_CH_6, INPUTA_CH_7, INPUTA_CH_8, INPUTA_CH_9, INPUTA_CH_A, INPUTA_CH_B, INPUTA_CH_C, INPUTA_CH_D, INPUTA_CH_E, INPUTA_CH_F), \
    writeIBRateTrackEnable, /* for ARC:  This is really tracking the input. */ \
    writeOBRateTrackEnable, /* for ARC: Output is tracked in streamOutputMaster */ \
    execPAZInAnalog, \
    execPAZOut_DRO_A, \
    \
    /* streamInputB is the second input task */ \
    streamInputB, \
    execPAZInNone, \
    execPAZOutNone, \
    writeDECChannelMapTo16(INPUTB_CH_0, INPUTB_CH_1, INPUTB_CH_2, INPUTB_CH_3, INPUTB_CH_4, INPUTB_CH_5, INPUTB_CH_6, INPUTB_CH_7, INPUTB_CH_8, INPUTB_CH_9, INPUTB_CH_A, INPUTB_CH_B, INPUTB_CH_C, INPUTB_CH_D, INPUTB_CH_E, INPUTB_CH_F), \
    writeENCChannelMapFrom16(INPUTB_CH_0, INPUTB_CH_1, INPUTB_CH_2, INPUTB_CH_3, INPUTB_CH_4, INPUTB_CH_5, INPUTB_CH_6, INPUTB_CH_7, INPUTB_CH_8, INPUTB_CH_9, INPUTB_CH_A, INPUTB_CH_B, INPUTB_CH_C, INPUTB_CH_D, INPUTB_CH_E, INPUTB_CH_F), \
    writeVOLRampTimeN(1), \
    writeIBRateTrackEnable, /* for ARC:  This is really tracking the input. */ \
    writeOBRateTrackEnable, /* for ARC: Output is tracked in streamOutputMaster */ \
    execPAZInDigital, \
    execPAZOut_DRO_B, \
    \
    /* streamOutputMaster is the (dummy) master input to AMIX and the output */ \
    streamOutputMaster, \
    writeDECChannelMapTo16(MASTER_CH_0, MASTER_CH_1, MASTER_CH_2, MASTER_CH_3, MASTER_CH_4, MASTER_CH_5, MASTER_CH_6, MASTER_CH_7, MASTER_CH_8, MASTER_CH_9, MASTER_CH_A, MASTER_CH_B, MASTER_CH_C, MASTER_CH_D, MASTER_CH_E, MASTER_CH_F), \
    writeENCChannelMapFrom16(MASTER_CH_0, MASTER_CH_1, MASTER_CH_2, MASTER_CH_3, MASTER_CH_4, MASTER_CH_5, MASTER_CH_6, MASTER_CH_7, MASTER_CH_8, MASTER_CH_9, MASTER_CH_A, MASTER_CH_B, MASTER_CH_C, MASTER_CH_D, MASTER_CH_E, MASTER_CH_F), \
    execPAZInNone, \
    execPAZOutNone, \
    writeIBRateTrackEnable, /* for ARC:  Input is tracked in input thread. */ \
    writeOBRateTrackEnable, /* for ARC:  Output tracking is forced/hard coded in AS_Output.c */ \
    writeVOLRampTimeN(1), \
    writeVOLControlMasterN(0), /* full volume */ \
    execPAZOutAnalog, \
    execPAZInOTime, \
    \
    /* streamOutputSlaveA is the first slave input to AMIX */ \
    streamOutputSlaveA, \
    writeDECChannelMapTo16(SLAVEA_CH_0, SLAVEA_CH_1, SLAVEA_CH_2, SLAVEA_CH_3, SLAVEA_CH_4, SLAVEA_CH_5, SLAVEA_CH_6, SLAVEA_CH_7, SLAVEA_CH_8, SLAVEA_CH_9, SLAVEA_CH_A, SLAVEA_CH_B, SLAVEA_CH_C, SLAVEA_CH_D, SLAVEA_CH_E, SLAVEA_CH_F), \
    execPAZInNone, \
    execPAZIn_DRI_A, \
    \
    /* streamOutputSlaveB is the second slave input to AMIX */ \
    streamOutputSlaveB, \
    writeDECChannelMapTo16(SLAVEB_CH_0, SLAVEB_CH_1, SLAVEB_CH_2, SLAVEB_CH_3, SLAVEB_CH_4, SLAVEB_CH_5, SLAVEB_CH_6, SLAVEB_CH_7, SLAVEB_CH_8, SLAVEB_CH_9, SLAVEB_CH_A, SLAVEB_CH_B, SLAVEB_CH_C, SLAVEB_CH_D, SLAVEB_CH_E, SLAVEB_CH_F), \
    execPAZInNone, \
    execPAZIn_DRI_B, \
    \
    /* default is set in pa_zaa_evmda830_io_a.h */ \
    defaultStream
    
// -----------------------------------------------------------------------------

#pragma DATA_SECTION(cus_atboot_s0_patch, ".none")
const ACP_Unit cus_atboot_s0_patch[] = {
    0xc900 + 0 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s_patch[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (cus_atboot_s, ".text:cus_atboot_s")
#endif // USE_EXT_RAM
const ACP_Unit cus_atboot_s[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (cus_attime_s, ".text:cus_attime_s")
#endif // USE_EXT_RAM
const ACP_Unit cus_attime_s[] = {
    0xc900,
};
// EOF
