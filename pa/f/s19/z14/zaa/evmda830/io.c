
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

//
// Input / output device configuration definitions
//

#include <acptype.h>
#include <acpbeta.h>
#include <pafstd_a.h>
#include <pafdec_a.h>
#include <pafenc_a.h>
#include <paftyp.h>
#include <pafsio.h>

#include <alpha/pcm_a.h>

#include <inpbuf_a.h>
#include <outbuf_a.h>

#include "ztop.h"

#include "alpha\pa_zaa_evmda830_io_a.h"
#include <dap_e17.h>

#include "dri_params.h"
#include "dro_params.h"
#include "oTime_params.h"

#define  rb32DECSourceDecode 0xc024,0x0b81
#define  ob32DECSourceDecodeNone 0x0001,0x0000

#define  rb32IBSioSelect 0xc022,0x0581
#define  ob32IBSioSelect(X) (X)|0x0080,0x0000

#define  ob32OBSioSelect(X) (X)|0x0080,0x0000
#define  rb32DECSourceSelect_3 0xc024,0x09b1
#define  wb32DECSourceSelect_3 0xc024,0x09f1

#define writePA3Await(RB32,WB32) 0xcd0b,5+2,0x0204,200,10,WB32,RB32

/*
extern RingIO_Handle PA_DSPLINK_readerHandle;
extern RingIO_Handle PA_DSPLINK_writerHandle;
*/
extern RingIO_Handle DSP_DSPLINK_writerHandle_0;
extern RingIO_Handle DSP_DSPLINK_readerHandle_0;
extern RingIO_Handle DSP_DSPLINK_writerHandle_1;
extern RingIO_Handle DSP_DSPLINK_readerHandle_1;

#include "DRIO.h"

/* DRO to DRI link coming out of stream A */
const Dro_Params DRO_RINGIO_DRIO_A = {
    sizeof (Dro_Params),                /* size */
    "/DRO",                             /* name /DDLink */
    0,                                  /* Channel Number (moduleNum) */
    NULL,                               /* pConfig (unused) */
    DRIO_SIZE_OF_ELEMENT,               /* wordSize (in bytes) */
    DRIO_PRECISION,                     /* precision  */
    NULL,
    DRIO_A_NUM_CHANS,                   /* number of channels  */
    0,                                  /* 1-Secondary output, 0- primary output  */
    (Int*)&DSP_DSPLINK_writerHandle_0,  // RingIO_Handle   writerHandle;

    { DRIO_A_CH_0, DRIO_A_CH_1, DRIO_A_CH_2, DRIO_A_CH_3, DRIO_A_CH_4, DRIO_A_CH_5, DRIO_A_CH_6, DRIO_A_CH_7, 
      DRIO_A_CH_8, DRIO_A_CH_9, DRIO_A_CH_A, DRIO_A_CH_B, DRIO_A_CH_C, DRIO_A_CH_D, DRIO_A_CH_E, DRIO_A_CH_F}
};

 /* Ring IO Input from other DSP thread */
const Dri_Params DRI_RINGIO_DRIO_A = {
    sizeof (Dri_Params),    /* size */
    "/DRI",                 /* name  */
    0,                      /* Channel Number (moduleNum) */
    NULL,                   /* pConfig (unused) */
    DRIO_SIZE_OF_ELEMENT,   /* wordSize (in bytes) */
    DRIO_PRECISION,         /* precision  */
    NULL,                   /* control (unused) */
    DRIO_A_NUM_CHANS,         /* no_channel: number of channels */
    (Int*)&DSP_DSPLINK_readerHandle_0,    // RingIO_Handle   readerHandle;
    { DRIO_A_CH_0, DRIO_A_CH_1, DRIO_A_CH_2, DRIO_A_CH_3, DRIO_A_CH_4, DRIO_A_CH_5, DRIO_A_CH_6, DRIO_A_CH_7, 
      DRIO_A_CH_8, DRIO_A_CH_9, DRIO_A_CH_A, DRIO_A_CH_B, DRIO_A_CH_C, DRIO_A_CH_D, DRIO_A_CH_E, DRIO_A_CH_F}
};

/* DRO to DRI link coming out of stream B */
const Dro_Params DRO_RINGIO_DRIO_B = {
    sizeof (Dro_Params),                /* size */
    "/DRO",                             /* name /DDLink */
    0,                                  /* Channel Number (moduleNum) */
    NULL,                               /* pConfig (unused) */
    DRIO_SIZE_OF_ELEMENT,               /* wordSize (in bytes) */
    DRIO_PRECISION,                     /* precision  */
    NULL,
    DRIO_B_NUM_CHANS,                   /* number of channel  */
    0,                                  /* 1-Secondary output, 0- primary output  */
    (Int*)&DSP_DSPLINK_writerHandle_1,  // RingIO_Handle   writerHandle;
    { DRIO_B_CH_0, DRIO_B_CH_1, DRIO_B_CH_2, DRIO_B_CH_3, DRIO_B_CH_4, DRIO_B_CH_5, DRIO_B_CH_6, DRIO_B_CH_7, 
      DRIO_B_CH_8, DRIO_B_CH_9, DRIO_B_CH_A, DRIO_B_CH_B, DRIO_B_CH_C, DRIO_B_CH_D, DRIO_B_CH_E, DRIO_B_CH_F}
};

 /* Ring IO Input from other DSP thread */
const Dri_Params DRI_RINGIO_DRIO_B = {
    sizeof (Dri_Params),    /* size */
    "/DRI",                 /* name  */
    0,                      /* Channel Number (moduleNum) */
    NULL,                   /* pConfig (unused) */
    DRIO_SIZE_OF_ELEMENT,   /* wordSize (in bytes) */
    DRIO_PRECISION,         /* precision  */
    NULL,                   /* control (unused) */
    DRIO_B_NUM_CHANS,         /* no_channel: number of channels */
    (Int*)&DSP_DSPLINK_readerHandle_1,    // RingIO_Handle   readerHandle;
    { DRIO_B_CH_0, DRIO_B_CH_1, DRIO_B_CH_2, DRIO_B_CH_3, DRIO_B_CH_4, DRIO_B_CH_5, DRIO_B_CH_6, DRIO_B_CH_7, 
      DRIO_B_CH_8, DRIO_B_CH_9, DRIO_B_CH_A, DRIO_B_CH_B, DRIO_B_CH_C, DRIO_B_CH_D, DRIO_B_CH_E, DRIO_B_CH_F}
};


 /* Ring IO Input from ARM */
const Dri_Params DRI_RINGIO_GPP = {
    sizeof (Dri_Params),    /* size */
    "/DRI",                 /* name */
    0,                      /* Channel Number (moduleNum) */
    NULL,                   /* pConfig (unused) */
    2,                      /* wordSize (in bytes, unused) */
    -1,                     /* precision (unused) */
    NULL,                   /* control (unused) */
    2,                      /* no_channel: number of channels */
    (Int*)(&PA_DSPLINK_readerHandle),
    {0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3}
};




// -----------------------------------------------------------------------------
//
// Input device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVINP_N];
} patchs_devinp[1] =
{
    DEVINP_N,
    // These values reflect the definitions DEVINP_* in pa*io_a.h:
    NULL,       // InNone            DEVINP_NULL   0xf120   Sigma32
                // InDigital         DEVINP_DIR    0xf121   Sigma33
    (const PAF_SIO_Params *) &DAP_E17_RX_DIR,                 
                // InAnalog          DEVINP_ADC1   0xf122   Sigma34
    (const PAF_SIO_Params *) &DAP_E17_RX_ADC_48000HZ,         
                // InAnalogStereo    DEVINP_ADC_STE 0xf123  Sigma35
    (const PAF_SIO_Params *) &DAP_E17_RX_ADC_STEREO_48000HZ,  
                // InRingIO          DEVINP_DRI_GPP 0xf124  Sigma36
    (const PAF_SIO_Params *) &DRI_RINGIO_GPP,                 
                // InRingIO          DEVINP_DRI_A   0xf125  Sigma37
    (const PAF_SIO_Params *) &DRI_RINGIO_DRIO_A,               
                // InRingIO          DEVINP_DRI_B   0xf126  Sigma38
    (const PAF_SIO_Params *) &DRI_RINGIO_DRIO_B,               
                // InSing                           0xF127, Sigma39.
                // InOTime           DEVINP_OTIME   0xf128  Sigma40
    (const PAF_SIO_Params *) &OTIME_Primary
};

// .............................................................................
// execPAZInNone
#define CUS_SIGMA32_S \
    writeDECSourceSelectNone, \
    writeIBSioSelectN(DEVINP_NULL), \
    0xcdf0,execPAZInNone

#pragma DATA_SECTION(cus_sigma32_s0, ".none")
const ACP_Unit cus_sigma32_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA32_S,
};

#pragma DATA_SECTION(cus_sigma32_s, ".none")
const ACP_Unit cus_sigma32_s[] = {
    0xc900 + sizeof(cus_sigma32_s0)/2 - 1,
    CUS_SIGMA32_S,
};

// .............................................................................

// #define PCM_ONLY
#ifdef PCM_ONLY  // Force PCM decoder on digital input.  Normally set Auto-Detection

// execPAZInDigital
#define CUS_SIGMA33_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBUnknownTimeoutN(2*2048), \
    writeIBScanAtHighSampleRateModeDisable, \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideDisable, \
    writeIBPrecisionDefaultOriginal, \
    writeIBPrecisionOverrideDetect, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_DIR), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZInDigital

#else // Zxx

// execPAZInDigital
#define CUS_SIGMA33_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBUnknownTimeoutN(2*2048), \
    writeIBScanAtHighSampleRateModeDisable, \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideDisable, \
    writeIBPrecisionDefaultOriginal, \
    writeIBPrecisionOverrideDetect, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_DIR), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAZInDigital

#endif // Zxx

#pragma DATA_SECTION(cus_sigma33_s0, ".none")
const ACP_Unit cus_sigma33_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA33_S,
};

#pragma DATA_SECTION(cus_sigma33_s, ".none")
const ACP_Unit cus_sigma33_s[] = {
    0xc900 + sizeof(cus_sigma33_s0)/2 - 1,
    CUS_SIGMA33_S,
};

// .............................................................................
// execPAZInAnalog
#define CUS_SIGMA34_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAZInAnalog

#pragma DATA_SECTION(cus_sigma34_s0, ".none")
const ACP_Unit cus_sigma34_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA34_S,
};

#pragma DATA_SECTION(cus_sigma34_s, ".none")
const ACP_Unit cus_sigma34_s[] = {
    0xc900 + sizeof(cus_sigma34_s0)/2 - 1,
    CUS_SIGMA34_S,
};

// .............................................................................
// execPAZInAnalogStereo
#define CUS_SIGMA35_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC_STEREO), \
    writeDECSourceSelectPCM,                 \
    0xcdf0,execPAZInAnalogStereo

#pragma DATA_SECTION(cus_sigma35_s0, ".none")
const ACP_Unit cus_sigma35_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA35_S,
};

#pragma DATA_SECTION(cus_sigma35_s, ".none")
const ACP_Unit cus_sigma35_s[] = {
    0xc900 + sizeof(cus_sigma35_s0)/2 - 1,
    CUS_SIGMA35_S,
};

// .............................................................................
// execPAZIn_DRI_GPP
#define CUS_SIGMA36_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride16, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_DRI_GPP), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZIn_DRI_GPP

#pragma DATA_SECTION(cus_sigma36_s0, ".none")
const ACP_Unit cus_sigma36_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA36_S,
};

#pragma DATA_SECTION(cus_sigma36_s, ".none")
const ACP_Unit cus_sigma36_s[] = {
    0xc900 + sizeof(cus_sigma36_s0)/2 - 1,
    CUS_SIGMA36_S,
};

// .............................................................................
// execPAZIn_DRI_A
#define CUS_SIGMA37_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(DRIO_A_CH_0, DRIO_A_CH_1, DRIO_A_CH_2, DRIO_A_CH_3, DRIO_A_CH_4, DRIO_A_CH_5, DRIO_A_CH_6, DRIO_A_CH_7, DRIO_A_CH_8, DRIO_A_CH_9, DRIO_A_CH_A, DRIO_A_CH_B, DRIO_A_CH_C, DRIO_A_CH_D, DRIO_A_CH_E, DRIO_A_CH_F), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride16, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_DRI_A), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZIn_DRI_A

#pragma DATA_SECTION(cus_sigma37_s0, ".none")
const ACP_Unit cus_sigma37_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA37_S,
};

#pragma DATA_SECTION(cus_sigma37_s, ".none")
const ACP_Unit cus_sigma37_s[] = {
    0xc900 + sizeof(cus_sigma37_s0)/2 - 1,
    CUS_SIGMA37_S,
};
// .............................................................................
// execPAZIn_DRI_B
#define CUS_SIGMA38_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround2_1, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(DRIO_B_CH_0, DRIO_B_CH_1, DRIO_B_CH_2, DRIO_B_CH_3, DRIO_B_CH_4, DRIO_B_CH_5, DRIO_B_CH_6, DRIO_B_CH_7, DRIO_B_CH_8, DRIO_B_CH_9, DRIO_B_CH_A, DRIO_B_CH_B, DRIO_B_CH_C, DRIO_B_CH_D, DRIO_B_CH_E, DRIO_B_CH_F), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride16, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_DRI_B), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZIn_DRI_B

#pragma DATA_SECTION(cus_sigma38_s0, ".none")
const ACP_Unit cus_sigma38_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA38_S,
};

#pragma DATA_SECTION(cus_sigma38_s, ".none")
const ACP_Unit cus_sigma38_s[] = {
    0xc900 + sizeof(cus_sigma38_s0)/2 - 1,
    CUS_SIGMA38_S,
};


// .............................................................................
// execPAZInSing
#define CUS_SIGMA39_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectSing, \
    writeSNGSongMultiTone, \
    0xcdf0,execPAZInSing

// writeSNGSongToneSatSequence

#pragma DATA_SECTION(cus_sigma39_s0, ".none")
const ACP_Unit cus_sigma39_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA39_S,
};

#pragma DATA_SECTION(cus_sigma39_s, ".none")
const ACP_Unit cus_sigma39_s[] = {
    0xc900 + sizeof(cus_sigma39_s0)/2 - 1,
    CUS_SIGMA39_S,
};

// .............................................................................
// execPAZInOTime
#define CUS_SIGMA40_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride16, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_OTIME), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZInOTime

#pragma DATA_SECTION(cus_sigma40_s0, ".none")
const ACP_Unit cus_sigma40_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA40_S,
};

#pragma DATA_SECTION(cus_sigma40_s, ".none")
const ACP_Unit cus_sigma40_s[] = {
    0xc900 + sizeof(cus_sigma40_s0)/2 - 1,
    CUS_SIGMA40_S,
};




// -----------------------------------------------------------------------------
//
// Output device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVOUT_N];
} patchs_devout[1] =
{
    DEVOUT_N,
        // These values reflect the definitions DEVOUT_* in pa*io_a.h:
    NULL,       // OutNone                   0xf130      Sigma48
                // OutAnalog     DEVOUT_DAC  0xf131      Sigma49
    (const PAF_SIO_Params *) &DAP_E17_TX_DAC,
    // OutDigital DEVOUT_DIT  0xf132  Sigma50
    (const PAF_SIO_Params *) &DAP_E17_TX_DIT,               
    // OutAnalogSlave DEVOUT_DAC_SLAVE  0xf133  Sigma51
    (const PAF_SIO_Params *) &DAP_E17_TX_DAC_SLAVE,         // 
    // OutRio16 DEVOUT_RRIO_16bit  0xf134  Sigma52
    (const PAF_SIO_Params *) &DRO_RINGIO_16bit,        //
    // OutRio24 DEVOUT_RRIO_24bit  0xf135  Sigma53
    (const PAF_SIO_Params *) &DRO_RINGIO_24bit,        //
    // OutDroDsp0 DEVOUT_DRO_A  0xf136  Sigma54
    (const PAF_SIO_Params *) &DRO_RINGIO_DRIO_A, 
    // OutDroDsp0 DEVOUT_DRO_B  0xf137  Sigma55
    (const PAF_SIO_Params *) &DRO_RINGIO_DRIO_B, 
};

// .............................................................................
// execPAZOutNone
#define CUS_SIGMA48_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_NULL), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutNone

#pragma DATA_SECTION(cus_sigma48_s0, ".none")
const ACP_Unit cus_sigma48_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA48_S,
};

#pragma DATA_SECTION(cus_sigma48_s, ".none")
const ACP_Unit cus_sigma48_s[] = {
    0xc900 + sizeof(cus_sigma48_s0)/2 - 1,
    CUS_SIGMA48_S,
};

// .............................................................................
// execPAZOutAnalog
#define CUS_SIGMA49_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(1), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutAnalog

#pragma DATA_SECTION(cus_sigma49_s0, ".none")
const ACP_Unit cus_sigma49_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA49_S,
};

#pragma DATA_SECTION(cus_sigma49_s, ".none")
const ACP_Unit cus_sigma49_s[] = {
    0xc900 + sizeof(cus_sigma49_s0)/2 - 1,
    CUS_SIGMA49_S,
};

// .............................................................................
// execPAZOutDigital
#define CUS_SIGMA50_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DIT), \
    writeENCChannelMapTo16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutDigital

#pragma DATA_SECTION(cus_sigma50_s0, ".none")
const ACP_Unit cus_sigma50_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA50_S,
};

#pragma DATA_SECTION(cus_sigma50_s, ".none")
const ACP_Unit cus_sigma50_s[] = {
    0xc900 + sizeof(cus_sigma50_s0)/2 - 1,
    CUS_SIGMA50_S,
};

// .............................................................................
// execPAZOutAnalogSlave
#define CUS_SIGMA51_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_SLAVE), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutAnalogSlave

#pragma DATA_SECTION(cus_sigma51_s0, ".none")
const ACP_Unit cus_sigma51_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA51_S,
};

#pragma DATA_SECTION(cus_sigma51_s, ".none")
const ACP_Unit cus_sigma51_s[] = {
    0xc900 + sizeof(cus_sigma51_s0)/2 - 1,
    CUS_SIGMA51_S,
};

// .............................................................................
//execPAZOutRingIO16bit
#define CUS_SIGMA52_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_RRIO_16bit), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutRingIO16bit

#pragma DATA_SECTION(cus_sigma52_s0, ".none")
const ACP_Unit cus_sigma52_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA52_S,
};

#pragma DATA_SECTION(cus_sigma52_s, ".none")
const ACP_Unit cus_sigma52_s[] = {
    0xc900 + sizeof (cus_sigma52_s0) / 2 - 1,
    CUS_SIGMA52_S,
};


// .............................................................................
//execPAZOutRingIO24bit
#define CUS_SIGMA53_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_RRIO_24bit)), \
    writeENCChannelMapTo16(0, 1, -3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutRingIO24bit

#pragma DATA_SECTION(cus_sigma53_s0, ".none")
const ACP_Unit cus_sigma53_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA53_S,
};

#pragma DATA_SECTION(cus_sigma53_s, ".none")
const ACP_Unit cus_sigma53_s[] = {
    0xc900 + sizeof (cus_sigma53_s0) / 2 - 1,
    CUS_SIGMA53_S,
};


// .............................................................................
//execPAZOut_DRO_A
#define CUS_SIGMA54_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DRO_A), \
    writeENCChannelMapTo16(DRIO_A_CH_0, DRIO_A_CH_1, DRIO_A_CH_2, DRIO_A_CH_3, DRIO_A_CH_4, DRIO_A_CH_5, DRIO_A_CH_6, DRIO_A_CH_7, DRIO_A_CH_8, DRIO_A_CH_9, DRIO_A_CH_A, DRIO_A_CH_B, DRIO_A_CH_C, DRIO_A_CH_D, DRIO_A_CH_E, DRIO_A_CH_F), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOut_DRO_A

#pragma DATA_SECTION(cus_sigma54_s0, ".none")
const ACP_Unit cus_sigma54_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA54_S, 
};

#pragma DATA_SECTION(cus_sigma54_s, ".none")
const ACP_Unit cus_sigma54_s[] = {
    0xc900 + sizeof (cus_sigma54_s0) / 2 - 1,
    CUS_SIGMA54_S,
};

// .............................................................................
//execPAZOut_DRO_B
#define CUS_SIGMA55_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DRO_B), \
    writeENCChannelMapTo16(DRIO_B_CH_0, DRIO_B_CH_1, DRIO_B_CH_2, DRIO_B_CH_3, DRIO_B_CH_4, DRIO_B_CH_5, DRIO_B_CH_6, DRIO_B_CH_7, DRIO_B_CH_8, DRIO_B_CH_9, DRIO_B_CH_A, DRIO_B_CH_B, DRIO_B_CH_C, DRIO_B_CH_D, DRIO_B_CH_E, DRIO_B_CH_F), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOut_DRO_B

#pragma DATA_SECTION(cus_sigma55_s0, ".none")
const ACP_Unit cus_sigma55_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA55_S,
};

#pragma DATA_SECTION(cus_sigma55_s, ".none")
const ACP_Unit cus_sigma55_s[] = {
    0xc900 + sizeof (cus_sigma55_s0) / 2 - 1,
    CUS_SIGMA55_S,
};

// -----------------------------------------------------------------------------

// EOF
