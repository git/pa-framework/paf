
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- System Stream Function Definitions
//
//     System Stream Functions Main and 1-3.
//
//
//

// Specific ASP usage requires inter-file symbol definitions ...
#include "noasp.h"

// ... end of specific usage definitions.

#include <std.h>
#include <alg.h>
#include <logp.h>

#include <xdas.h>

#include <acp.h>
#include <acp_mds.h>

#include <acptype.h>
#include <acpbeta.h>
#include <acperr.h>

#include <ss1.h>
#include "pafhjt.h"

#ifdef RAM_REPORT
#include <paf_alg_print.h>
extern int IRAM;
extern int SDRAM;
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
extern int L3RAM;
#endif
#endif /* RAM_REPORT */

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// #define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------

//
// ss2_systemStream : audio stream control function (main)
//

LINNO_DEFN (ss2_systemStreamMain);
ERRNO_DEFN (ss2_systemStreamMain);

extern IALG_Status pafIdentification;

// this is called repeatedly from AS_Output_idle()
void
ss2_systemStreamMain (const PAF_SST2_Params *pP, PAF_SST2_Config *pC)
{
    Int ss = pP->ss;
    const PAF_SST2_Fxns *fxns= pP->fxns;
    PAF_SystemStatus *pStatus = pC->pStatus;

    TRACE((&trace, "ss2_systemStreamMain.%d", __LINE__));
    LINNO_RPRT (ss2_systemStreamMain, -1);

    // Initialize on first invocation:

    if (! pC->acp) {

        ALG_Handle acpAlg;
        ACP_Handle acp;

        Int betaPrimeValue = ss - 1;
        Int betaPrimeOffset;
        Int betaPrime;

        // Initialize algorithms

        // reate an ACP algorithm instance with trace enabled

        TRACE((&trace, "ss1 started"));
        ACP_MDS_init();

        if (! (acpAlg = (ALG_Handle )ACP_MDS_create (NULL))) {
            LOG_printf(&trace, "ss2_systemStreamMain: SS%d: ACP algorithm instance creation failed",
                ss);
            LINNO_RPRT (ss2_systemStreamMain, __LINE__);
            return;
        }

        acpAlg->fxns->algControl (acpAlg, ACP_GETBETAPRIMEOFFSET,
            (IALG_Status *) &betaPrimeOffset);
        betaPrime = betaPrimeOffset * betaPrimeValue;
        TRACE((&trace, "ss2_systemStreamMain: betaPrime(%d) = betaPrimeOffset(%d) * betaPrimeValue(%d)",
         				betaPrime, betaPrimeOffset, betaPrimeValue));

        acp = (ACP_Handle )acpAlg;
        acp->fxns->attach(acp, ACP_SERIES_STD, STD_BETA_SYSIDL+betaPrime, (IALG_Status *)pStatus);

        acpAlg->fxns->algControl (acpAlg, ACP_SETBETAPRIMEVALUE,
            (IALG_Status *) &betaPrimeValue);

        pC->acp = acp;

        TRACE((&trace, "ss2_systemStreamMain: SS%d: ACP processing initialized", ss));
        LINNO_RPRT (ss2_systemStreamMain, -2);

        // Attach PAF IDENTITY
        acp->fxns->attach (acp, ACP_SERIES_STD, STD_BETA_IDENTITY, (IALG_Status *) &pafIdentification);
        TRACE((&trace, "ss2_systemStreamMain.%d", __LINE__));
    }

    // Invoke sub-functions as per mode:

#ifdef RAM_REPORT
    {
     static int count=0;
     if(count<10) count++;
     else if(count==10){
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
    PAF_ALG_memStatusPrint(IRAM,SDRAM,L3RAM);
#else
             PAF_ALG_memStatusPrint(IRAM,SDRAM);
#endif
	     count++;
          }
    }
#endif /* RAM_REPORT */
    if (fxns) {
        Int i;

        LgUns x[PAF_SYSTEMSTREAMFXNS_XN];

        for (i=0; i < fxns->count; i++) {
            TRACE((&trace, "ss2_systemStreamMain.%d, i: %d", __LINE__, i));
            if ((pStatus->mode & (1 << i)) == 0)
                continue;
            if (! fxns->sub[i].compute)
                continue;
            if (fxns->sub[i].compute (pP, pC, x))
                continue;
            if (! fxns->sub[i].transmit)
                continue;
            if (fxns->sub[i].transmit (pP, pC, x))
                continue;
        }
    }
}

//
// ss2_systemStream1 : audio stream control functions (sub)
//
//   Process listening mode, recreation mode, and speaker configuration
//   to set Decode Channel Configuration Request and Override Select
//   Registers.
//

#ifndef NODEC

Int
ss2_systemStream1Compute (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    PAF_SystemStatus *pStatus = pC->pStatus;

    static const PAF_ChannelConfiguration cs[PAF_SYS_RECREATIONMODE_N] = {
        // PAF_SYS_RECREATIONMODE_NONE
        { PAF_CC_SAT_NONE, PAF_CC_SUB_ZERO, 0, 0, },
        // PAF_SYS_RECREATIONMODE_MONO (unused below)
        { PAF_CC_SAT_MONO, PAF_CC_SUB_ZERO, 0, 0, },
        // PAF_SYS_RECREATIONMODE_STEREO
        { PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, },
        // PAF_SYS_RECREATIONMODE_PHANTOM0_1
        { PAF_CC_SAT_STEREO, PAF_CC_SUB_ONE, 0, 0, },
        // PAF_SYS_RECREATIONMODE_SURROUND0_1
        { PAF_CC_SAT_3STEREO, PAF_CC_SUB_ONE, 0, 0, },
        // PAF_SYS_RECREATIONMODE_PHANTOM2_1
        { PAF_CC_SAT_PHANTOM2, PAF_CC_SUB_ONE, 0, 0, },
        // PAF_SYS_RECREATIONMODE_SURROUND2_1
        { PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, },
    };

    XDAS_UInt8 n;

    PAF_ChannelConfiguration ccr; /* Channel Configuration Request */
    PAF_ChannelConfiguration cco; /* Channel Configuration Override */

    // Listening mode: ignored.

    // Recreation mode: direct, select, or auto.

    switch (n = pStatus->recreationMode) {

      case PAF_SYS_RECREATIONMODE_DONT:
        ccr.full = -1;
        cco.full = -1;
        break;

      case PAF_SYS_RECREATIONMODE_DIRECT:
        ccr = pStatus->channelConfigurationRequest;
        cco.full = PAF_CC_UNKNOWN;
        break;

      case PAF_SYS_RECREATIONMODE_AUTO:
        ccr.full = 0;
        ccr.part.sat = PAF_CC_SAT_STEREO
                + (pStatus->speakerCntr & PAF_SYS_SPEAKERNUMB) * 5
                + (pStatus->speakerSurr & PAF_SYS_SPEAKERNUMB)
                + (pStatus->speakerBack & PAF_SYS_SPEAKERNUMB);
        ccr.part.sub = (pStatus->speakerSubw & PAF_SYS_SPEAKERNUMB);
        ccr.part.extMask =
            ((pStatus->speakerWide & PAF_SYS_SPEAKERNUMB) ? PAF_CC_EXTMASK_LwRw : 0)
          | ((pStatus->speakerHead & PAF_SYS_SPEAKERNUMB) ? PAF_CC_EXTMASK_LhRh : 0) ;
        pStatus->channelConfigurationRequest = ccr;
        cco.full = PAF_CC_UNKNOWN;
        break;

      case PAF_SYS_RECREATIONMODE_MONO:
        ccr.full = (pStatus->speakerCntr & PAF_SYS_SPEAKERNUMB)
            ? PAF_CC_MONO : PAF_CC_STEREO_MONO;
        pStatus->channelConfigurationRequest = ccr;
        cco.full = PAF_CC_UNKNOWN;
        break;

      default:
        ccr = cs[n < lengthof (cs) ? n : 0];
        pStatus->channelConfigurationRequest = ccr;
        cco.full = PAF_CC_UNKNOWN;
        break;
    }

    if (pStatus->channelConfigurationRequestType
        != PAF_SYS_CCRTYPE_STANDARD) {
        PAF_ChannelConfiguration ccs;

        ccs = cco;

        // CCO is computed CCR.
        cco = ccr;

        // CCR is as per request type:
        // DecodeBypass - PAF_CC_UNKNOWN (see above)
        // DecodeDirect - don't write, allow use as control register
        ccr.full = pStatus->channelConfigurationRequestType
              == PAF_SYS_CCRTYPE_DECODEDIRECT ? -1 : ccs.full;
    }

    x[0] = ccr.full; /* Channel Configuration Request */
    x[1] = cco.full; /* Channel Configuration Override */

    return 0;
}

Int
ss2_systemStream1Transmit (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    Int ss = pP->ss;

    ACP_Handle acp = pC->acp;

    PAF_ChannelConfiguration ccr; /* Channel Configuration Request */
    PAF_ChannelConfiguration cco; /* Channel Configuration Override */

    ccr.full = x[0]; /* Channel Configuration Request */
    cco.full = x[1]; /* Channel Configuration Override */

    // Send Request to Audio Stream 1 Decode Status.

    if (ccr.full != -1) {
        ACP_Unit from[9];

        Int errno;

        from[0] = 0xc908; // encapsulated: no.of following words

        // select stream
        from[1] = 0xcd09;
        from[2] = 0x0401; // stream2/Primary : SlaveA (not oTime)

        // writeDECChannelConfigurationRequest 0xcc24,0x0010
        from[3] = 0xcc24;
        from[4] = 0x0010;
        from[5] = ccr.full;
        from[6] = ccr.full >> 16;

        // select stream1 (default)
        from[7] = 0xcd09;
        from[8] = 0x0400;

        if (errno = acp->fxns->sequence(acp, from, NULL)) {
            LOG_printf(&trace, "ss2_systemStream1Transmit: SS%d: DEC Request sequence processing error (0x%04x)",
                ss, errno);
            ERRNO_RPRT (ss2_systemStreamMain, errno);
        }
    }

    // Send Override to Audio Stream 1 Decode Status.

    if (cco.full != -1) {
        ACP_Unit from[9];

        Int errno;

        from[0] = 0xc908; // encapsulated: no.of following words

        // select stream
        from[1] = 0xcd09;
        from[2] = 0x0401; // stream2/Primary : SlaveA (not oTime)

        // writeDECChannelConfigurationOverride 0xcc24,0x0030+2*PAF_MAXNUMCHAN
        from[3] = 0xcc24;
        from[4] = 0x0030+2*PAF_MAXNUMCHAN; // FOR NOW --Kurt
        from[5] = cco.full;
        from[6] = cco.full >> 16;

        // select stream1 (default)
        from[7] = 0xcd09;
        from[8] = 0x0400;

        if (errno = acp->fxns->sequence(acp, from, NULL)) {
            LOG_printf(&trace, "ss2_systemStream1Transmit: SS%d: DEC Override sequence processing error (0x%04x)",
                ss, errno);
            ERRNO_RPRT (ss2_systemStreamMain, errno);
        }
    }

    return 0;
}

#else /* NODEC */

asm(" .global _ss2_systemStream1Compute");
asm("_ss2_systemStream1Compute .set 0");

asm(" .global _ss2_systemStream1Transmit");
asm("_ss2_systemStream1Transmit .set 0");

#endif /* NODEC */

//
// ss2_systemStream2 : audio stream control functions (sub)
//
//   Process listening mode, recreation mode, and speaker configuration
//   to set Bass Management Output Configuration Select Register.
//

#define NOBM // zaa has BM in AS_InputB, not here in AS_Output

#ifndef NOBM
#define DOC_AUTO  0x0f  // 15, to leave some room
Int
ss2_systemStream2Compute (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    PAF_SystemStatus *pStatus = pC->pStatus;

    LgUns oc;

    // Determine BM Output Configuration Select (if auto mode):

    if(  pStatus->recreationMode == PAF_SYS_RECREATIONMODE_DONT
      || pStatus->recreationMode == PAF_SYS_RECREATIONMODE_DIRECT )
    {
        return 1;
    }
    // Set channel flags, including channels beyond Dolby spec:

    oc = (DOC_AUTO << 24) +
        ((pStatus->speakerSubw & PAF_SYS_SPEAKERFREQ_HI ? 1 : 0) << 0) +
        ((pStatus->speakerMain & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 1) +
        ((pStatus->speakerCntr & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 2) +
        ((pStatus->speakerSurr & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 3) +
        ((pStatus->speakerBack & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 4) +
        ((pStatus->speakerWide & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 5) +
        ((pStatus->speakerHead & PAF_SYS_SPEAKERFREQ_LO ? 1 : 0) << 6) ;

    x[0] = oc; /* Output Configuration */

    return 0;
}

Int
ss2_systemStream2Transmit (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    Int ss = pP->ss;

    ACP_Handle acp = pC->acp;

    LgUns oc;

    oc = x[0]; /* Output Configuration */

    // Send Select to Audio Stream 1 Bass Management Status if valid:

    {
        Int errno;
        ACP_Unit from[10];

        from[0] = 0xc909; // encapsulated: no.of following words

        // select stream
        from[1] = 0xcd09;
        from[2] = 0x0401; // stream2/Primary : SlaveA (not oTime)

        // writeBMOCSelectOCAuto(OCNO,AUTO) 0xcb40,0x000a,
        //     ((OCNO) << 8)&0xff00+(AUTO)&0x00ff
        from[3] = 0xcb40;
        from[4] = 0x000a;
        from[5] = (oc>>16) & 0xffff;

        // writeBMOCSelectChannels(CHANS) 0xca40,
        //     0x0800+((CHANS) & 0x00ff)
        from[6] = 0xca40;
        from[7] = 0x0800 + (oc & 0xff);

        // select stream1 (default)
        from[8] = 0xcd09;
        from[9] = 0x0400;

        if (errno = acp->fxns->sequence(acp, from, NULL)) {
            if (errno == ACPERR_APPLY_NOBETA) {
                // Return without reporting to trace Log if BM is not
                // part of the stream.
                return 1;
            }
            else {
                LOG_printf(&trace, "ss2_systemStream2Transmit: SS%d: BM sequence processing error (0x%04x)",
                    ss, errno);
                ERRNO_RPRT (ss2_systemStreamMain, errno);
            }
        }
    }

    return 0;
}

#else /* NOBM */

asm(" .global _ss2_systemStream2Compute");
asm("_ss2_systemStream2Compute .set 0");

asm(" .global _ss2_systemStream2Transmit");
asm("_ss2_systemStream2Transmit .set 0");

#endif /* NOBM */

//
// ss2_systemStream3 : audio stream control functions (sub)
//
//   Process S/PDIF pre-emphasis flag information and Deemphasis Filter
//   Mode Control information to set Deemphasis Filter Active Select
//   Register if needed.
//
//   Note that this implementation is quite arbitrary and could be done
//   another way, including completely via a shortcut! --Kurt
//

#if 0 // Not yet updated from ss0.c (stream0)

#ifndef NODEM

Int
ss2_systemStream3Compute (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    Int ss = pP->ss;

    ACP_Handle acp = pC->acp;

    Int errno;

    ACP_Unit from[3], to[3];

    // readDEMFilterMode 0xc250,0x0500
    from[0] = 0xc902;
    from[1] = 0xc250;
    from[2] = 0x0500;
    if (errno = acp->fxns->sequence(acp, from, to)) {
        if (errno == ACPERR_APPLY_NOBETA) {
            // Return without reporting to trace Log if DEM is not
            // part of the stream.
            return 1;
        }
        else {
            LOG_printf(&trace, "ss2_systemStream3Compute: SS%d: DEM sequence processing error (0x%04x)",
                ss, errno);
            ERRNO_RPRT (ss2_systemStreamMain, errno);
        }
    }

    // If Deemphasis Filter Mode Control is not "Auto", no further action.
    if ((to[2] & 0xff) != 1)
        return 1;

    // readDECEmphasis  0xc224,0x4700/0x5700
    from[0] = 0xc902;
    from[1] = 0xc224;
#if PAF_MAXNUMCHAN==8
    from[2] = 0x4700;
#elif PAF_MAXNUMCHAN==16
    from[2] = 0x5700;
#else
#error readDECEmphasis: unsupported option
#endif /* PAF_MAXNUMCHAN */
    if (errno = acp->fxns->sequence(acp, from, to)) {
        LOG_printf(&trace, "ss2_systemStream3Compute: SS%d: DEC sequence processing error (0x%04x)",
            ss, errno);
        ERRNO_RPRT (ss2_systemStreamMain, errno);
    }

    x[0] = (to[2] & 0xff) == 2 ? 1 : 0;

    return 0;
}

Int
ss2_systemStream3Transmit (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    Int ss = pP->ss;

    ACP_Handle acp = pC->acp;

    Int errno;

    ACP_Unit from[3];

    from[0] = 0xc902;
    from[1] = 0xca50;
    from[2] = 0x0600 + x[0];

    if (errno = acp->fxns->sequence(acp, from, NULL)) {
        if (errno == ACPERR_APPLY_NOBETA) {
            // Return without reporting to trace Log if DEM is not
            // part of the stream.
            return 1;
        }
        else {
            LOG_printf(&trace, "ss2_systemStream3Transmit: SS%d: DEM sequence processing error (0x%04x)",
                ss, errno);
            ERRNO_RPRT (ss2_systemStreamMain, errno);
        }
    }

    return 0;
}

#else /* NODEM */

asm(" .global _ss2_systemStream3Compute");
asm("_ss2_systemStream3Compute .set 0");

asm(" .global _ss2_systemStream3Transmit");
asm("_ss2_systemStream3Transmit .set 0");

#endif /* NODEM */

#endif // Not yet updated from ss0.c (stream0)

// ----

//
// ss2_systemStream5 : CPU Load Graph
//


#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)

#include <ti/bios/include/log.h>

#include <ti/sysbios/utils/Load.h>
#include <ti/sysbios/knl/Task.h>

#include <xdc/runtime/LoggerBuf.h>
#include <xdc/runtime/System.h>

#else // PAF_DEVICE = 0xD8000000

#include <std.h>
#include <clk.h>

#define LOAD_WINDOW 500

static LgUns minloop = 0xffffffff;
static LgUns window = 0;
static float cpuload = 0;

Void
ss2_systemStream5Init (void)
{
    window = CLK_countspms () * LOAD_WINDOW;
}

#endif

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)

Int
ss2_systemStream5Compute (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    PAF_SystemStatus *pStatus = pC->pStatus;

    Load_Stat stat;

    Load_getTaskLoad (Task_getIdleTask(), &stat);
    pStatus->cpuLoad = (100 - Load_calculateLoad (&stat)) * 256;
    if (pStatus->peakCpuLoad < pStatus->cpuLoad)
        pStatus->peakCpuLoad = pStatus->cpuLoad;

    return 0;
}

#else // PAF_DEVICE = 0xD8000000

Int
ss2_systemStream5Compute (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    PAF_SystemStatus *pStatus = pC->pStatus;

    static LgUns count = 0;
    static LgUns total = 0;
    static LgUns t0 = 0;
    LgUns t1 = 0;
    LgUns delta;

    count++;                                                /* bump number of times thru IDL loop */

    /*
     * Compute the time difference since the last call to LOAD_idlefxn.
     */
    t1 = CLK_gethtime ();
    delta = t1 - t0;
    t0 = t1;

    total += delta;                                         /* accumulate time */

    /*
     * 'minloop' keeps track of the shortest time thru the IDL loop.
     * Note that we check to make sure delta is not zero (delta
     * will be zero during IDL loop calibration)
     */
    if (delta != 0 && delta < minloop)
    {
        minloop = delta;
    }

    /*
     * Compute the CPU load if the time window has expired.
     * The first cpuload is wrong because of init timer value is random
     */
    if (total > window)
    {
        cpuload = 100.00 - ((float) (count * minloop) / ((float) total / 100.00));
        count = total = 0;
        minloop = 0xffffffff;
    }

    pStatus->cpuLoad = cpuload * 256;
    if (pStatus->peakCpuLoad < pStatus->cpuLoad)
        pStatus->peakCpuLoad = pStatus->cpuLoad;

    return 0;
}

#endif // PAF_DEVICE = 0xD8000000

Int
ss2_systemStream5Transmit (const PAF_SST2_Params *pP, PAF_SST2_Config *pC, LgUns x[])
{
    return 0;
}
