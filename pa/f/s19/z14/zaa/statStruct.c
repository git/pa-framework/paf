
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Status tracking and global priority control for async Z-topology
//
// Copyright (c) 2013, Texas Instruments, Inc.  All rights reserved.
// Copyright (c) 2013, Momentum Data Systems.  All rights reserved.
//
//

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// Define to enable local tracing features:
//  You can read most stat info from gStatStruct.
//  The CCS debugger can be set to update it continuously while running.
// #define STAT_DEBUG
#ifdef STAT_DEBUG
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// .............................................................................

#include <tsk.h>
#include "statStruct.h"
#include "ztop.h"

#include <ringiodefs.h>

extern RingIO_Handle   DSP_DSPLINK_writerHandle_0;
extern RingIO_Handle   DSP_DSPLINK_writerHandle_1;
extern RingIO_Handle   DSP_DSPLINK_readerHandle_0;
extern RingIO_Handle   DSP_DSPLINK_readerHandle_1;

// add this global to the watched expressions in the debugger.
// The debugger updates this in real time, or see it when you break.
// Or write a complex breakpoint trigger using it.
statStruct_t gStatStruct;


void statStruct_UpdateFrameCount(int taskID, int frameCount, int reset)
{
    int *pCurFrameCount;
    int *pLastFrameCount;
    int *pMaxFrameCount;
    int *pResetCount;
    int curResetCount;

    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            pCurFrameCount  = &gStatStruct.inA_CurFrameCount;
            pLastFrameCount = &gStatStruct.inA_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.inA_MaxFrameCount;
            pResetCount     = &gStatStruct.inA_ResetCount;
            if (gStatStruct.inA_task == 0) 
            {
                gStatStruct.inA_task = TSK_self();
            }
            gStatStruct.A_watchdogCount = 0;
            break;       
        case STATSTRUCT_INPUT_B:
        	if (reset)
        	{
        		// TRACE((&trace, "statStruct Reset on B.  Break here."));
        	}

            pCurFrameCount  = &gStatStruct.inB_CurFrameCount;
            pLastFrameCount = &gStatStruct.inB_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.inB_MaxFrameCount;
            pResetCount     = &gStatStruct.inB_ResetCount;
            if (gStatStruct.inB_task == 0) 
            {
                gStatStruct.inB_task = TSK_self();
            }
            gStatStruct.B_watchdogCount = 0;
            break;
        case STATSTRUCT_OUTPUT:
            pCurFrameCount  = &gStatStruct.out_CurFrameCount;
            pLastFrameCount = &gStatStruct.out_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.out_MaxFrameCount;
            pResetCount     = &gStatStruct.out_ResetCount;
            if (gStatStruct.out_task == 0) 
            {
                gStatStruct.out_task = TSK_self();
            }
            gStatStruct.O_watchdogCount = 0;
            break;
        default:  // should not get here.
            statStruct_resetPriorities();
            return;
    }   

  #if 0
    // watchdogs for debugging
    if (gStatStruct.inA_CurFrameCount == gStatStruct.inA_LastFrameCount) {
        gStatStruct.A_watchdogCount++;
        if (gStatStruct.A_watchdogCount > 12) {
            if (gStatStruct.inA_LastFrameCount > 3)
                TRACE(( &trace, "Why did A die?  Break here."));
        }
    }
    if (gStatStruct.inB_CurFrameCount == gStatStruct.inB_LastFrameCount) {
        gStatStruct.B_watchdogCount++;
        if (gStatStruct.B_watchdogCount > 32) {
            if (gStatStruct.inB_LastFrameCount > 32)
                TRACE(( &trace, "Why did B die?  Break here."));
        }
    }
    if (gStatStruct.out_CurFrameCount == gStatStruct.out_LastFrameCount) {
        gStatStruct.O_watchdogCount++;
        if (gStatStruct.O_watchdogCount > 12) {
            if (gStatStruct.out_LastFrameCount > 3)
                TRACE(( &trace, "Why did O die?  Break here."));
        }
    }
  #endif

    statStruct_resetPriorities();

    curResetCount = *pResetCount;
    if (reset) {
        *pCurFrameCount = 0;
        curResetCount++;
        if (taskID == STATSTRUCT_INPUT_A && curResetCount>3)
        	LOG_printf( &trace, "Why did A reset?  Break here.");
    }
    else {
        *pCurFrameCount = *pLastFrameCount = frameCount;
        if (frameCount > *pMaxFrameCount) {
            *pMaxFrameCount = frameCount;
        }
    }
    *pResetCount = curResetCount;

    // complex breakpoint trigger, if we reset after a long time, stop the trace.
    if ((*pLastFrameCount > 40000) && reset)
    {	
    	TRACE(( &trace, "Unexpected reset condition."));
    	TRACE(( &trace, "Check the trace logs."));
    	LOG_disable(&trace);  // stop tracing
    	if (reset)
    		TRACE(( &trace, "Dummy statement, set breakpoint here."));
    	return;
    }

    return;
}

void statStruct_LogFullRing(int taskID)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_FullRingCount++;
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_FullRingCount++;
            return;
        default:
            return;
    }
}


void statStruct_LogLock(int taskID, int locked)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_Locked = locked;
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_Locked = locked;
            return;
        default:
            return;
    }
}

void statStruct_LogFullSRateChange(int taskID)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_SRateChanges++;
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_SRateChanges++;
            return;
        default:
            return;
    }
}

void statStruct_LogEmptyRing(RingIO_Handle readerHandle)
{
    if (readerHandle == DSP_DSPLINK_readerHandle_0) {
        gStatStruct.inA_EmptyRingCount++;
        gStatStruct.inA_ringLevel = 0;

        return;
    }
    if (readerHandle == DSP_DSPLINK_readerHandle_1) {
        gStatStruct.inB_EmptyRingCount++;
        gStatStruct.inB_ringLevel = 0;
        return;
    }
}

void statStruct_LogArcRatio(int taskID, double arcRatio)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_arcRatio = arcRatio;
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_arcRatio = arcRatio;
            return;
        default:
            return;
    }
}


void statStruct_LogRingLevel(RingIO_Handle writerHandle, int ringLevel)
{
    if (writerHandle == DSP_DSPLINK_writerHandle_0) {
        gStatStruct.inA_ringLevel = ringLevel;
        return;
    }
    if (writerHandle == DSP_DSPLINK_writerHandle_1) {
        gStatStruct.inB_ringLevel = ringLevel;
        return;
    }
}

void statStruct_LogSampleRate(int taskID, int sRate)
{

    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_sRate = sRate;
            break;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_sRate = sRate;
            break;
        default:
            gStatStruct.out_sRate = sRate;
            break;
    }
}

void statStruct_SetDibDeviceHandle(int taskID, int handle)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            gStatStruct.inA_DIB_device_handle = handle;
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_DIB_device_handle = handle;
            return;
        default:
            return;
    }

}

void statStruct_LogDibSize(int handle, int size)
{
    if (handle == gStatStruct.inA_DIB_device_handle) {
        gStatStruct.inA_size = size;
        TRACE(( &trace, "statStruct_LogDibSize: A: %d. ", gStatStruct.inA_size));
        return;
    }
    if (handle == gStatStruct.inB_DIB_device_handle) {
        gStatStruct.inB_size = size;
        TRACE(( &trace, "statStruct_LogDibSize: B: %d. ", gStatStruct.inB_size));
        return;
    }
    TRACE(( &trace, "statStruct_LogDibSize: ? 0x%x: %d. ", handle, size));
    // we only log input, output is fixed.
    return;
}
//////////////////////////////////////////////////////////////////////////////
//  This function is responsible to dynamically adjust the priorites of the 
// three threads used in ZAA.  It may need to be tuned for alternate 
// hardware configurations.  In tests on the EVM, the A input is always active.
// If the A input goes away, different behavior may be required.
//  See discussion in the Z topology app note.
//////////////////////////////////////////////////////////////////////////////
void statStruct_resetPriorities(void)
{
	float rateA, rateB, rateO;
	float measuredSRateA, measuredSRateB, measuredSRateO;

    // according to the rate monotonic rule, the highest priority
    // goes to the thread with the shortest period.
    // period depends on sample rate and buffer size.

    // output rate is the reference, set in the DAP driver (dap_e17.c)
    // Here assumed fixed at 48k.
	measuredSRateO = (float)FIXED_OUTPUT_SAMPLE_RATE;	

    // The arcRatio measures the relative rates of the inputs.
	if ((gStatStruct.inA_arcRatio > MIN_ARC_RATIO_A) && (gStatStruct.inA_arcRatio < MAX_ARC_RATIO_A))
		measuredSRateA = measuredSRateO / gStatStruct.inA_arcRatio;
	else
		measuredSRateA = measuredSRateO;

	if ((gStatStruct.inB_arcRatio > MIN_ARC_RATIO_B) && (gStatStruct.inB_arcRatio < MAX_ARC_RATIO_B))
		measuredSRateB = measuredSRateO / gStatStruct.inB_arcRatio;
	else
		measuredSRateB = measuredSRateO;

    // If a channel is not running, set it to low priority.
    // A channel not running can be indicated by size of zero
    // or by an "unlocked" indiciation.
    // Not running needs to be indicated by a larger size in the sort below.
    if (gStatStruct.inA_size == 0) 
        gStatStruct.inA_size = 3000;  
    if (gStatStruct.inB_size == 0)
        gStatStruct.inB_size = 3000;  

    if (!gStatStruct.inA_Locked)
    {        
        gStatStruct.inA_size = 3000;  
        gStatStruct.inB_prio = HIGH_PRIO;
        gStatStruct.inA_prio = LOW_PRIO;
        gStatStruct.out_prio = MID_PRIO;
        TRACE(( &trace, "statStruct_resetPriorities: A appears stopped (unlocked)."));

    }
    else if (!gStatStruct.inB_Locked)
    {
        gStatStruct.inB_size = 3000;  
        gStatStruct.inA_prio = HIGH_PRIO;
        gStatStruct.inB_prio = LOW_PRIO;
        gStatStruct.out_prio = MID_PRIO;
        TRACE(( &trace, "statStruct_resetPriorities: B appears stopped (unlocked)."));
    }
    else  // apply rate monotonic rule to get priorities
    {
        typedef struct
        {
            int ID;
            int period;
        } sortStruct_t;

        int changes = 1;
        int i;
        sortStruct_t sorter[3];

        // Account for block size
        rateA = 1000000.0 * (float)gStatStruct.inA_size / measuredSRateA; // 
        rateB = 1000000.0 * (float)gStatStruct.inB_size / measuredSRateB; // 
        rateO = 1000000.0 * (float)ZTOP_FRAMESIZE / measuredSRateO;

        TRACE(( &trace, "statStruct_resetPriorities: A: %d.  B: %d.  Out: %d.",
                   (int)rateA, (int)rateB, (int)rateO));

        sorter[0].ID = STATSTRUCT_INPUT_A;  sorter[0].period = rateA;
        sorter[1].ID = STATSTRUCT_INPUT_B;  sorter[1].period = rateB;
        sorter[2].ID = STATSTRUCT_OUTPUT;   sorter[2].period = rateO;
    
        // simple bubble sort:  There are only 3 to sort.
        // Order from shortest period (= highest priority) to longest
        while (changes) 
        {
            sortStruct_t temp;
            changes = 0;
            for (i=0; i<2; i++) 
            {
                if (sorter[i].period > sorter[i+1].period) {
                    temp.ID = sorter[i].ID;
                    temp.period = sorter[i].period;
                    sorter[i].ID = sorter[i+1].ID;
                    sorter[i].period = sorter[i+1].period;
                    sorter[i+1].ID = temp.ID;
                    sorter[i+1].period = temp.period;
                    changes = 1;
                }
            }
            // TRACE(( &trace, "statStruct_resetPriorities: 0: %d.  1: %d.  2: %d.", sorter[0].ID, sorter[1].ID, sorter[2].ID));
        }

        switch (sorter[0].ID) {
            case STATSTRUCT_INPUT_A:
                gStatStruct.inA_prio = HIGH_PRIO;
                break;
            case STATSTRUCT_INPUT_B:
                gStatStruct.inB_prio = HIGH_PRIO;
                break;
            default:
                gStatStruct.out_prio = HIGH_PRIO;
                break;
        }

        if (sorter[0].period == sorter[1].period) {
            switch (sorter[1].ID) {
                case STATSTRUCT_INPUT_A:
                    gStatStruct.inA_prio = HIGH_PRIO;
                    break;
                case STATSTRUCT_INPUT_B:
                    gStatStruct.inB_prio = HIGH_PRIO;
                    break;
                default:
                    gStatStruct.out_prio = HIGH_PRIO;
                    break;
            }
        }
        else
        {
            switch (sorter[1].ID) {
                case STATSTRUCT_INPUT_A:
                    gStatStruct.inA_prio = MID_PRIO;
                    break;
                case STATSTRUCT_INPUT_B:
                    gStatStruct.inB_prio = MID_PRIO;
                    break;
                default:
                    gStatStruct.out_prio = MID_PRIO;
                    break;
            }
        }

        if (sorter[1].period == sorter[2].period) {
            switch (sorter[2].ID) {
                case STATSTRUCT_INPUT_A:
                    gStatStruct.inA_prio = MID_PRIO;
                    break;
                case STATSTRUCT_INPUT_B:
                    gStatStruct.inB_prio = MID_PRIO;
                    break;
                default:
                    gStatStruct.out_prio = MID_PRIO;
                    break;
            }
        }
        else
        {
            switch (sorter[2].ID) {
                case STATSTRUCT_INPUT_A:
                    gStatStruct.inA_prio = LOW_PRIO;
                    break;
                case STATSTRUCT_INPUT_B:
                    gStatStruct.inB_prio = LOW_PRIO;
                    break;
                default:
                    gStatStruct.out_prio = LOW_PRIO;
                    break;
            }
        }
    }
    TRACE(( &trace, "statStruct_resetPriorities: A: %d.  B: %d.  O: %d.",
       gStatStruct.inA_prio, gStatStruct.inB_prio, gStatStruct.out_prio));

    if (gStatStruct.inA_task != 0)
        TSK_setpri(gStatStruct.inA_task, gStatStruct.inA_prio);
    if (gStatStruct.inB_task != 0)
        TSK_setpri(gStatStruct.inB_task, gStatStruct.inB_prio);
    if (gStatStruct.out_task != 0)
        TSK_setpri(gStatStruct.out_task, gStatStruct.out_prio);
}



