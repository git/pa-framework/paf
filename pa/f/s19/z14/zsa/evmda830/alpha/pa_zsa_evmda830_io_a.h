
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

#ifndef _PAZSA_EVMDA830_IO_A
#define _PAZSA_EVMDA830_IO_A

#include <acpbeta.h>
#include <zsa_a.h>

#define stream1     0xcd09,0x0400
#define stream2     0xcd09,0x0401
#define stream3     0xcd09,0x0402
#define stream4     0xcd09,0x0403
#define stream5     0xcd09,0x0404

#define streamOutputMaster stream1  // AS_Output_Task
#define streamOutputSlaveA stream2  // AS_Output_Task
#define streamOutputSlaveB stream3  // AS_Output_Task
#define streamInputA       stream4  // AS_InputA_Task
#define streamInputB       stream5  // AS_InputB_Task

#define defaultStream      streamOutputMaster

// execPAZInOutError : if returned, input/output "none" selected
#define  execPAZInOutError      0xf1ff

// -----------------------------------------------------------------------------
// IB SIO Select Register is set by the execPAZIn* shortcuts

#define  execPAZInNone          0xf120
#define  execPAZInDigital       0xf121
#define  execPAZInAnalog        0xf122
#define  execPAZInAnalogStereo  0xf123
#define  execPAZIn_DRI_GPP      0xf124
#define  execPAZIn_DRI_A        0xf125
#define  execPAZIn_DRI_B        0xf126
#define  execPAZInSing          0xf127
#define  execPAZInOTime         0xf128


// These values reflect the definition of devinp[]
#define DEVINP_NULL             0
#define DEVINP_DIR              1
#define DEVINP_ADC1             2
#define DEVINP_ADC_STEREO       3
#define DEVINP_DRI_GPP          4
#define DEVINP_DRI_A            5
#define DEVINP_DRI_B            6
#define DEVINP_OTIME            7
#define DEVINP_N                8

#define wroteIBSioCommandNone           0xca00+STD_BETA_IB,0x0500+DEVINP_NULL
#define wroteIBSioCommandDigital        0xca00+STD_BETA_IB,0x0500+DEVINP_DIR
#define wroteIBSioCommandAnalog         0xca00+STD_BETA_IB,0x0500+DEVINP_ADC1
#define wroteIBSioCommandAnalogStereo   0xca00+STD_BETA_IB,0x0500+DEVINP_ADC_STEREO
#define wroteIBSioCommandRingIO         0xca00+STD_BETA_IB,0x0500+DEVINP_DRI_GPP
#define wroteIBSioCommand_DRI_A         0xca00+STD_BETA_IB,0x0500+DEVINP_DRI_A
#define wroteIBSioCommand_DRI_B         0xca00+STD_BETA_IB,0x0500+DEVINP_DRI_B
#define wroteIBSioCommand_OTIME         0xca00+STD_BETA_IB,0x0500+DEVINP_OTIME


#define wroteIBSioSelectNone            0xca00+STD_BETA_IB,0x0580+DEVINP_NULL
#define wroteIBSioSelectDigital         0xca00+STD_BETA_IB,0x0580+DEVINP_DIR
#define wroteIBSioSelectAnalog          0xca00+STD_BETA_IB,0x0580+DEVINP_ADC1
#define wroteIBSioSelectAnalogStereo    0xca00+STD_BETA_IB,0x0580+DEVINP_ADC_STEREO
#define wroteIBSioSelectRingIO          0xca00+STD_BETA_IB,0x0580+DEVINP_DRI_GPP
#define wroteIBSioSelect_DRI_A          0xca00+STD_BETA_IB,0x0580+DEVINP_DRI_A
#define wroteIBSioSelect_DRI_B          0xca00+STD_BETA_IB,0x0580+DEVINP_DRI_B
#define wroteIBSioSelect_OTIME          0xca00+STD_BETA_IB,0x0580+DEVINP_OTIME


// -----------------------------------------------------------------------------
// OB SIO Select Register is set by the execPAZOut* shortcuts

#define  execPAZOutNone                 0xf130
#define  execPAZOutAnalog               0xf131 //8 channel output analog (24bit)
#define  execPAZOutDigital              0xf132 //8 channel output analog (24bit)
#define  execPAZOutAnalogSlave          0xf133 //8 channel output analog (24bit) slave to input
#define  execPAZOutRingIO16bit          0xf134
#define  execPAZOutRingIO24bit          0xf135
#define  execPAZOut_DRO_A               0xf136
#define  execPAZOut_DRO_B               0xf137

// These values reflect the definition of devout[]
#define DEVOUT_NULL             0
#define DEVOUT_DAC              1
#define DEVOUT_DIT              2
#define DEVOUT_DAC_SLAVE        3
#define DEVOUT_RRIO_16bit       4
#define DEVOUT_RRIO_24bit       5
#define DEVOUT_DRO_A            6
#define DEVOUT_DRO_B            7
#define DEVOUT_N                8

#define wroteOBSioCommandNone               0xca00+STD_BETA_OB,0x0500+DEVOUT_NULL      
#define wroteOBSioCommandAnalog             0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC       
#define wroteOBSioCommandDigital            0xca00+STD_BETA_OB,0x0500+DEVOUT_DIT       
#define wroteOBSioCommandAnalogSlave        0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_SLAVE 
#define wroteOBSioCommandRRINGIO_16bit      0xca00+STD_BETA_OB,0x0500+DEVOUT_RRIO_16bit
#define wroteOBSioCommandRRINGIO_24bit      0xca00+STD_BETA_OB,0x0500+DEVOUT_RRIO_24bit
#define wroteOBSioCommand_DRO_A             0xca00+STD_BETA_OB,0x0500+DEVOUT_DRO_A 
#define wroteOBSioCommand_DRO_B             0xca00+STD_BETA_OB,0x0500+DEVOUT_DRO_B 

#define wroteOBSioSelectNone                0xca00+STD_BETA_OB,0x0580+DEVOUT_NULL      
#define wroteOBSioSelectAnalog              0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC       
#define wroteOBSioSelectDigital             0xca00+STD_BETA_OB,0x0580+DEVOUT_DIT       
#define wroteOBSioSelectAnalogSlave         0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_SLAVE 
#define wroteOBSioSelectRRINGIO_16bit       0xca00+STD_BETA_OB,0x0580+DEVOUT_RRIO_16bit
#define wroteOBSioSelectRRINGIO_24bit       0xca00+STD_BETA_OB,0x0580+DEVOUT_RRIO_24bit
#define wroteOBSioSelect_DRO_A              0xca00+STD_BETA_OB,0x0580+DEVOUT_DRO_A 
#define wroteOBSioSelect_DRO_B              0xca00+STD_BETA_OB,0x0580+DEVOUT_DRO_B 

// -----------------------------------------------------------------------------

#endif // _PAZSA_EVMDA830_IO_A
