
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Input/Output channel configuration definitions
//

#include "ztop.h"

#define IO_SIZE_OF_ELEMENT      4  // 4 bytes, float

#define IO_NUM_CHANS_INPUTA     ZTOP_NUM_CHANS_MAX_INPUTA   // AS_InputA
#define IO_NUM_CHANS_INPUTB     ZTOP_NUM_CHANS_MAX_INPUTB   // AS_InputB
#define IO_NUM_CHANS_MASTER     ZTOP_NUM_CHANS_MAX_MASTER   // AS_Output
#define IO_NUM_CHANS_SLAVEA     ZTOP_NUM_CHANS_MAX_SLAVEA   // AS_Output
#define IO_NUM_CHANS_SLAVEB     ZTOP_NUM_CHANS_MAX_SLAVEB   // AS_Output

// .............................................................................

#if (IO_NUM_CHANS_INPUTA == 0)
    #define INPUTA_CH_0   -3
    #define INPUTA_CH_1   -3
    #define INPUTA_CH_2   -3
    #define INPUTA_CH_3   -3
    #define INPUTA_CH_4   -3
    #define INPUTA_CH_5   -3
    #define INPUTA_CH_6   -3
    #define INPUTA_CH_7   -3
    #define INPUTA_CH_8   -3
    #define INPUTA_CH_9   -3
    #define INPUTA_CH_A   -3
    #define INPUTA_CH_B   -3
    #define INPUTA_CH_C   -3
    #define INPUTA_CH_D   -3
    #define INPUTA_CH_E   -3
    #define INPUTA_CH_F   -3
#elif (IO_NUM_CHANS_INPUTA == 2)
    #define INPUTA_CH_0   PAF_LEFT
    #define INPUTA_CH_1   PAF_RGHT
    #define INPUTA_CH_2   -3
    #define INPUTA_CH_3   -3
    #define INPUTA_CH_4   -3
    #define INPUTA_CH_5   -3
    #define INPUTA_CH_6   -3
    #define INPUTA_CH_7   -3
    #define INPUTA_CH_8   -3
    #define INPUTA_CH_9   -3
    #define INPUTA_CH_A   -3
    #define INPUTA_CH_B   -3
    #define INPUTA_CH_C   -3
    #define INPUTA_CH_D   -3
    #define INPUTA_CH_E   -3
    #define INPUTA_CH_F   -3
#elif (IO_NUM_CHANS_INPUTA == 6)
    #define INPUTA_CH_0   PAF_LEFT
    #define INPUTA_CH_1   PAF_RGHT
    #define INPUTA_CH_2   PAF_LSUR
    #define INPUTA_CH_3   PAF_RSUR
    #define INPUTA_CH_4   PAF_CNTR
    #define INPUTA_CH_5   PAF_SUBW
    #define INPUTA_CH_6   -3
    #define INPUTA_CH_7   -3
    #define INPUTA_CH_8   -3
    #define INPUTA_CH_9   -3
    #define INPUTA_CH_A   -3
    #define INPUTA_CH_B   -3
    #define INPUTA_CH_C   -3
    #define INPUTA_CH_D   -3
    #define INPUTA_CH_E   -3
    #define INPUTA_CH_F   -3
#elif (IO_NUM_CHANS_INPUTA == 8)
    #define INPUTA_CH_0   PAF_LEFT
    #define INPUTA_CH_1   PAF_RGHT
    #define INPUTA_CH_2   PAF_LSUR
    #define INPUTA_CH_3   PAF_RSUR
    #define INPUTA_CH_4   PAF_CNTR
    #define INPUTA_CH_5   PAF_SUBW
    #define INPUTA_CH_6   PAF_LBAK
    #define INPUTA_CH_7   PAF_RBAK
    #define INPUTA_CH_8   -3
    #define INPUTA_CH_9   -3
    #define INPUTA_CH_A   -3
    #define INPUTA_CH_B   -3
    #define INPUTA_CH_C   -3
    #define INPUTA_CH_D   -3
    #define INPUTA_CH_E   -3
    #define INPUTA_CH_F   -3
#elif (IO_NUM_CHANS_INPUTA == 12)
    #define INPUTA_CH_0   PAF_LEFT
    #define INPUTA_CH_1   PAF_RGHT
    #define INPUTA_CH_2   PAF_LSUR
    #define INPUTA_CH_3   PAF_RSUR
    #define INPUTA_CH_4   PAF_CNTR
    #define INPUTA_CH_5   PAF_SUBW
    #define INPUTA_CH_6   PAF_LBAK
    #define INPUTA_CH_7   PAF_RBAK
    #define INPUTA_CH_8   PAF_LWID
    #define INPUTA_CH_9   PAF_RWID
    #define INPUTA_CH_A   PAF_LHED
    #define INPUTA_CH_B   PAF_RHED
    #define INPUTA_CH_C   -3
    #define INPUTA_CH_D   -3
    #define INPUTA_CH_E   -3
    #define INPUTA_CH_F   -3
#else
  ERROR Unsupported INPUTA channel number
#endif

// .............................................................................

#if (IO_NUM_CHANS_INPUTB == 0)
    #define INPUTB_CH_0   -3
    #define INPUTB_CH_1   -3
    #define INPUTB_CH_2   -3
    #define INPUTB_CH_3   -3
    #define INPUTB_CH_4   -3
    #define INPUTB_CH_5   -3
    #define INPUTB_CH_6   -3
    #define INPUTB_CH_7   -3
    #define INPUTB_CH_8   -3
    #define INPUTB_CH_9   -3
    #define INPUTB_CH_A   -3
    #define INPUTB_CH_B   -3
    #define INPUTB_CH_C   -3
    #define INPUTB_CH_D   -3
    #define INPUTB_CH_E   -3
    #define INPUTB_CH_F   -3
#elif (IO_NUM_CHANS_INPUTB == 2)
    #define INPUTB_CH_0   PAF_LEFT
    #define INPUTB_CH_1   PAF_RGHT
    #define INPUTB_CH_2   -3
    #define INPUTB_CH_3   -3
    #define INPUTB_CH_4   -3
    #define INPUTB_CH_5   -3
    #define INPUTB_CH_6   -3
    #define INPUTB_CH_7   -3
    #define INPUTB_CH_8   -3
    #define INPUTB_CH_9   -3
    #define INPUTB_CH_A   -3
    #define INPUTB_CH_B   -3
    #define INPUTB_CH_C   -3
    #define INPUTB_CH_D   -3
    #define INPUTB_CH_E   -3
    #define INPUTB_CH_F   -3
#elif (IO_NUM_CHANS_INPUTB == 6)
    #define INPUTB_CH_0   PAF_LEFT
    #define INPUTB_CH_1   PAF_RGHT
    #define INPUTB_CH_2   PAF_LSUR
    #define INPUTB_CH_3   PAF_RSUR
    #define INPUTB_CH_4   PAF_CNTR
    #define INPUTB_CH_5   PAF_SUBW
    #define INPUTB_CH_6   -3
    #define INPUTB_CH_7   -3
    #define INPUTB_CH_8   -3
    #define INPUTB_CH_9   -3
    #define INPUTB_CH_A   -3
    #define INPUTB_CH_B   -3
    #define INPUTB_CH_C   -3
    #define INPUTB_CH_D   -3
    #define INPUTB_CH_E   -3
    #define INPUTB_CH_F   -3
#elif (IO_NUM_CHANS_INPUTB == 8)
    #define INPUTB_CH_0   PAF_LEFT
    #define INPUTB_CH_1   PAF_RGHT
    #define INPUTB_CH_2   PAF_LSUR
    #define INPUTB_CH_3   PAF_RSUR
    #define INPUTB_CH_4   PAF_CNTR
    #define INPUTB_CH_5   PAF_SUBW
    #define INPUTB_CH_6   PAF_LBAK
    #define INPUTB_CH_7   PAF_RBAK
    #define INPUTB_CH_8   -3
    #define INPUTB_CH_9   -3
    #define INPUTB_CH_A   -3
    #define INPUTB_CH_B   -3
    #define INPUTB_CH_C   -3
    #define INPUTB_CH_D   -3
    #define INPUTB_CH_E   -3
    #define INPUTB_CH_F   -3
#elif (IO_NUM_CHANS_INPUTB == 12)
    #define INPUTB_CH_0   PAF_LEFT
    #define INPUTB_CH_1   PAF_RGHT
    #define INPUTB_CH_2   PAF_LSUR
    #define INPUTB_CH_3   PAF_RSUR
    #define INPUTB_CH_4   PAF_CNTR
    #define INPUTB_CH_5   PAF_SUBW
    #define INPUTB_CH_6   PAF_LBAK
    #define INPUTB_CH_7   PAF_RBAK
    #define INPUTB_CH_8   PAF_LWID
    #define INPUTB_CH_9   PAF_RWID
    #define INPUTB_CH_A   PAF_LHED
    #define INPUTB_CH_B   PAF_RHED
    #define INPUTB_CH_C   -3
    #define INPUTB_CH_D   -3
    #define INPUTB_CH_E   -3
    #define INPUTB_CH_F   -3
#else
  ERROR Unsupported INPUTB channel number
#endif

// .............................................................................

#if (IO_NUM_CHANS_MASTER == 0)
    #define MASTER_CH_0   -3
    #define MASTER_CH_1   -3
    #define MASTER_CH_2   -3
    #define MASTER_CH_3   -3
    #define MASTER_CH_4   -3
    #define MASTER_CH_5   -3
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 2)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   -3
    #define MASTER_CH_3   -3
    #define MASTER_CH_4   -3
    #define MASTER_CH_5   -3
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 6)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 8)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   PAF_LBAK
    #define MASTER_CH_7   PAF_RBAK
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 12)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   PAF_LBAK
    #define MASTER_CH_7   PAF_RBAK
    #define MASTER_CH_8   PAF_LWID
    #define MASTER_CH_9   PAF_RWID
    #define MASTER_CH_A   PAF_LHED
    #define MASTER_CH_B   PAF_RHED
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#else
  ERROR Unsupported MASTER channel number
#endif

// .............................................................................

#if (IO_NUM_CHANS_SLAVEA == 0)
    #define SLAVEA_CH_0   -3
    #define SLAVEA_CH_1   -3
    #define SLAVEA_CH_2   -3
    #define SLAVEA_CH_3   -3
    #define SLAVEA_CH_4   -3
    #define SLAVEA_CH_5   -3
    #define SLAVEA_CH_6   -3
    #define SLAVEA_CH_7   -3
    #define SLAVEA_CH_8   -3
    #define SLAVEA_CH_9   -3
    #define SLAVEA_CH_A   -3
    #define SLAVEA_CH_B   -3
    #define SLAVEA_CH_C   -3
    #define SLAVEA_CH_D   -3
    #define SLAVEA_CH_E   -3
    #define SLAVEA_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEA == 2)
    #define SLAVEA_CH_0   PAF_LEFT
    #define SLAVEA_CH_1   PAF_RGHT
    #define SLAVEA_CH_2   -3
    #define SLAVEA_CH_3   -3
    #define SLAVEA_CH_4   -3
    #define SLAVEA_CH_5   -3
    #define SLAVEA_CH_6   -3
    #define SLAVEA_CH_7   -3
    #define SLAVEA_CH_8   -3
    #define SLAVEA_CH_9   -3
    #define SLAVEA_CH_A   -3
    #define SLAVEA_CH_B   -3
    #define SLAVEA_CH_C   -3
    #define SLAVEA_CH_D   -3
    #define SLAVEA_CH_E   -3
    #define SLAVEA_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEA == 6)
    #define SLAVEA_CH_0   PAF_LEFT
    #define SLAVEA_CH_1   PAF_RGHT
    #define SLAVEA_CH_2   PAF_LSUR
    #define SLAVEA_CH_3   PAF_RSUR
    #define SLAVEA_CH_4   PAF_CNTR
    #define SLAVEA_CH_5   PAF_SUBW
    #define SLAVEA_CH_6   -3
    #define SLAVEA_CH_7   -3
    #define SLAVEA_CH_8   -3
    #define SLAVEA_CH_9   -3
    #define SLAVEA_CH_A   -3
    #define SLAVEA_CH_B   -3
    #define SLAVEA_CH_C   -3
    #define SLAVEA_CH_D   -3
    #define SLAVEA_CH_E   -3
    #define SLAVEA_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEA == 8)
    #define SLAVEA_CH_0   PAF_LEFT
    #define SLAVEA_CH_1   PAF_RGHT
    #define SLAVEA_CH_2   PAF_LSUR
    #define SLAVEA_CH_3   PAF_RSUR
    #define SLAVEA_CH_4   PAF_CNTR
    #define SLAVEA_CH_5   PAF_SUBW
    #define SLAVEA_CH_6   PAF_LBAK
    #define SLAVEA_CH_7   PAF_RBAK
    #define SLAVEA_CH_8   -3
    #define SLAVEA_CH_9   -3
    #define SLAVEA_CH_A   -3
    #define SLAVEA_CH_B   -3
    #define SLAVEA_CH_C   -3
    #define SLAVEA_CH_D   -3
    #define SLAVEA_CH_E   -3
    #define SLAVEA_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEA == 12)
    #define SLAVEA_CH_0   PAF_LEFT
    #define SLAVEA_CH_1   PAF_RGHT
    #define SLAVEA_CH_2   PAF_LSUR
    #define SLAVEA_CH_3   PAF_RSUR
    #define SLAVEA_CH_4   PAF_CNTR
    #define SLAVEA_CH_5   PAF_SUBW
    #define SLAVEA_CH_6   PAF_LBAK
    #define SLAVEA_CH_7   PAF_RBAK
    #define SLAVEA_CH_8   PAF_LWID
    #define SLAVEA_CH_9   PAF_RWID
    #define SLAVEA_CH_A   PAF_LHED
    #define SLAVEA_CH_B   PAF_RHED
    #define SLAVEA_CH_C   -3
    #define SLAVEA_CH_D   -3
    #define SLAVEA_CH_E   -3
    #define SLAVEA_CH_F   -3
#else
  ERROR Unsupported SLAVEA channel number
#endif

// .............................................................................

#if (IO_NUM_CHANS_SLAVEB == 0)
    #define SLAVEB_CH_0   -3
    #define SLAVEB_CH_1   -3
    #define SLAVEB_CH_2   -3
    #define SLAVEB_CH_3   -3
    #define SLAVEB_CH_4   -3
    #define SLAVEB_CH_5   -3
    #define SLAVEB_CH_6   -3
    #define SLAVEB_CH_7   -3
    #define SLAVEB_CH_8   -3
    #define SLAVEB_CH_9   -3
    #define SLAVEB_CH_A   -3
    #define SLAVEB_CH_B   -3
    #define SLAVEB_CH_C   -3
    #define SLAVEB_CH_D   -3
    #define SLAVEB_CH_E   -3
    #define SLAVEB_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEB == 2)
    #define SLAVEB_CH_0   PAF_LEFT
    #define SLAVEB_CH_1   PAF_RGHT
    #define SLAVEB_CH_2   -3
    #define SLAVEB_CH_3   -3
    #define SLAVEB_CH_4   -3
    #define SLAVEB_CH_5   -3
    #define SLAVEB_CH_6   -3
    #define SLAVEB_CH_7   -3
    #define SLAVEB_CH_8   -3
    #define SLAVEB_CH_9   -3
    #define SLAVEB_CH_A   -3
    #define SLAVEB_CH_B   -3
    #define SLAVEB_CH_C   -3
    #define SLAVEB_CH_D   -3
    #define SLAVEB_CH_E   -3
    #define SLAVEB_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEB == 6)
    #define SLAVEB_CH_0   PAF_LEFT
    #define SLAVEB_CH_1   PAF_RGHT
    #define SLAVEB_CH_2   PAF_LSUR
    #define SLAVEB_CH_3   PAF_RSUR
    #define SLAVEB_CH_4   PAF_CNTR
    #define SLAVEB_CH_5   PAF_SUBW
    #define SLAVEB_CH_6   -3
    #define SLAVEB_CH_7   -3
    #define SLAVEB_CH_8   -3
    #define SLAVEB_CH_9   -3
    #define SLAVEB_CH_A   -3
    #define SLAVEB_CH_B   -3
    #define SLAVEB_CH_C   -3
    #define SLAVEB_CH_D   -3
    #define SLAVEB_CH_E   -3
    #define SLAVEB_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEB == 8)
    #define SLAVEB_CH_0   PAF_LEFT
    #define SLAVEB_CH_1   PAF_RGHT
    #define SLAVEB_CH_2   PAF_LSUR
    #define SLAVEB_CH_3   PAF_RSUR
    #define SLAVEB_CH_4   PAF_CNTR
    #define SLAVEB_CH_5   PAF_SUBW
    #define SLAVEB_CH_6   PAF_LBAK
    #define SLAVEB_CH_7   PAF_RBAK
    #define SLAVEB_CH_8   -3
    #define SLAVEB_CH_9   -3
    #define SLAVEB_CH_A   -3
    #define SLAVEB_CH_B   -3
    #define SLAVEB_CH_C   -3
    #define SLAVEB_CH_D   -3
    #define SLAVEB_CH_E   -3
    #define SLAVEB_CH_F   -3
#elif (IO_NUM_CHANS_SLAVEB == 12)
    #define SLAVEB_CH_0   PAF_LEFT
    #define SLAVEB_CH_1   PAF_RGHT
    #define SLAVEB_CH_2   PAF_LSUR
    #define SLAVEB_CH_3   PAF_RSUR
    #define SLAVEB_CH_4   PAF_CNTR
    #define SLAVEB_CH_5   PAF_SUBW
    #define SLAVEB_CH_6   PAF_LBAK
    #define SLAVEB_CH_7   PAF_RBAK
    #define SLAVEB_CH_8   PAF_LWID
    #define SLAVEB_CH_9   PAF_RWID
    #define SLAVEB_CH_A   PAF_LHED
    #define SLAVEB_CH_B   PAF_RHED
    #define SLAVEB_CH_C   -3
    #define SLAVEB_CH_D   -3
    #define SLAVEB_CH_E   -3
    #define SLAVEB_CH_F   -3
#else
  ERROR Unsupported SLAVEB channel number
#endif

// .............................................................................
