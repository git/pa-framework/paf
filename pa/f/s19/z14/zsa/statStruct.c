
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Status tracking and global priority control for async Z-topology
//
// Copyright (c) 2013, Texas Instruments, Inc.  All rights reserved.
// Copyright (c) 2013, Momentum Data Systems.  All rights reserved.
//
//

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// Define to enable local tracing features:
//  You can read most stat info from gStatStruct.
//  The CCS debugger can be set to update it continuously while running.
// #define STAT_DEBUG
#ifdef STAT_DEBUG
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// .............................................................................

#include <tsk.h>
#include "statStruct.h"

#include <ringiodefs.h>

extern RingIO_Handle   DSP_DSPLINK_writerHandle_0;
extern RingIO_Handle   DSP_DSPLINK_writerHandle_1;
extern RingIO_Handle   DSP_DSPLINK_readerHandle_0;
extern RingIO_Handle   DSP_DSPLINK_readerHandle_1;

// add this global to the watched expressions in the debugger.
// The debugger updates this in real time, or see it when you break.
// Or write a complex breakpoint trigger using it.
statStruct_t gStatStruct;


void statStruct_UpdateFrameCount(int taskID, int frameCount, int reset)
{
    int *pCurFrameCount;
    int *pLastFrameCount;
    int *pMaxFrameCount;
    int *pResetCount;
    int curResetCount;

    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            break;       
        case STATSTRUCT_INPUT_B:
        	if (reset)
        	{
        		// TRACE((&trace, "statStruct Reset on B.  Break here."));
        	}

            pCurFrameCount  = &gStatStruct.inB_CurFrameCount;
            pLastFrameCount = &gStatStruct.inB_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.inB_MaxFrameCount;
            pResetCount     = &gStatStruct.inB_ResetCount;
            if (gStatStruct.inB_task == 0) 
            {
                gStatStruct.inB_task = TSK_self();
            }
            gStatStruct.B_watchdogCount = 0;
            break;
        case STATSTRUCT_OUTPUT:
            pCurFrameCount  = &gStatStruct.out_CurFrameCount;
            pLastFrameCount = &gStatStruct.out_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.out_MaxFrameCount;
            pResetCount     = &gStatStruct.out_ResetCount;
            if (gStatStruct.out_task == 0) 
            {
                gStatStruct.out_task = TSK_self();
            }
            gStatStruct.O_watchdogCount = 0;
            break;
        default:  // should not get here.
            statStruct_resetPriorities();
            return;
    }   

  #if 0
    // watchdogs for debugging:  
    // You can enable this to get a break point 
    // in case a task stops after some time.
    if (gStatStruct.inB_CurFrameCount == gStatStruct.inB_LastFrameCount) {
        gStatStruct.B_watchdogCount++;
        if (gStatStruct.B_watchdogCount > 32) {
            if (gStatStruct.inB_LastFrameCount > 32)
                TRACE(( &trace, "Why did B die?  Break here."));
        }
    }
    if (gStatStruct.out_CurFrameCount == gStatStruct.out_LastFrameCount) {
        gStatStruct.O_watchdogCount++;
        if (gStatStruct.O_watchdogCount > 12) {
            if (gStatStruct.out_LastFrameCount > 3)
                TRACE(( &trace, "Why did O die?  Break here."));
        }
    }
  #endif

    statStruct_resetPriorities();

    curResetCount = *pResetCount;
    if (reset) {
        *pCurFrameCount = 0;
        curResetCount++;
    }
    else {
        *pCurFrameCount = *pLastFrameCount = frameCount;
        if (frameCount > *pMaxFrameCount) {
            *pMaxFrameCount = frameCount;
        }
    }
    *pResetCount = curResetCount;

    // complex breakpoint trigger, if we reset after a long time, stop the trace.
    if ((*pLastFrameCount > 40000) && reset)
    {	
    	TRACE(( &trace, "Unexpected reset condition."));
    	TRACE(( &trace, "Check the trace logs."));
    	LOG_disable(&trace);  // stop tracing
    	if (reset)
    		TRACE(( &trace, "Dummy statement, set breakpoint here."));
    	return;
    }

    return;
}

void statStruct_LogFullRing(int taskID)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_FullRingCount++;
            return;
        default:
            return;
    }
}


void statStruct_LogLock(int taskID, int locked)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_Locked = locked;
            return;
        default:
            return;
    }
}

void statStruct_LogFullSRateChange(int taskID)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_SRateChanges++;
            return;
        default:
            return;
    }
}

void statStruct_LogEmptyRing(RingIO_Handle readerHandle)
{
    if (readerHandle == DSP_DSPLINK_readerHandle_0) {
        return;
    }
    if (readerHandle == DSP_DSPLINK_readerHandle_1) {
        gStatStruct.inB_EmptyRingCount++;
        return;
    }
}

void statStruct_LogArcRatio(int taskID, double arcRatio)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_arcRatio = arcRatio;
            return;
        default:
            return;
    }
}


void statStruct_LogRingLevel(RingIO_Handle writerHandle, int ringLevel)
{
    if (writerHandle == DSP_DSPLINK_writerHandle_1) {
        gStatStruct.inB_ringLevel = ringLevel;
        return;
    }
}

void statStruct_LogSampleRate(int taskID, int sRate)
{

    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            break;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_sRate = sRate;
            break;
        default:
            gStatStruct.out_sRate = sRate;
            break;
    }
}

void statStruct_SetDibDeviceHandle(int taskID, int handle)
{
    switch (taskID) {
        case STATSTRUCT_INPUT_A:
            return;
        case STATSTRUCT_INPUT_B:
            gStatStruct.inB_DIB_device_handle = handle;
            return;
        default:
            return;
    }

}

void statStruct_LogDibSize(int handle, int size)
{
    if (handle == gStatStruct.inB_DIB_device_handle) {
        gStatStruct.inB_size = size;
        TRACE(( &trace, "statStruct_LogDibSize: B: %d. ", gStatStruct.inB_size));
        return;
    }
    TRACE(( &trace, "statStruct_LogDibSize: ? 0x%x: %d. ", handle, size));
    // we only log input, output is fixed.
    return;
}

// It appears that dynamic priority is not required in the ZSA case.
// We need it in the ZAA case.  But when thread B has a load,
// dynamic priority goes wrong in the DD input case.
// #define ENABLE_DYNAMIC_PRIORITY
void statStruct_resetPriorities(void)
{
#ifndef ENABLE_DYNAMIC_PRIORITY
#if 0
    // set priorities once.
    // Could just do this in pa.cfg.
    if (gStatStruct.inB_task != 0)
    {
        if (gStatStruct.inB_prio != MID_PRIO)
        {
            gStatStruct.inB_prio = MID_PRIO;
            TSK_setpri(gStatStruct.inB_task, gStatStruct.inB_prio);
        }
    }
    if (gStatStruct.out_task != 0)
    {
        if (gStatStruct.out_prio != HIGH_PRIO)
        {
            gStatStruct.out_prio = HIGH_PRIO;
            TSK_setpri(gStatStruct.out_task, gStatStruct.out_prio);
        }
    }
#endif 
#else
	float rateB, rateO;
	float measuredSRateB, measuredSRateO;

	measuredSRateO = 48000.0;	// output rate is the reference, set in the DAP driver (dap_e17.c)

	if ((gStatStruct.inB_arcRatio > 0.6) && (gStatStruct.inB_arcRatio < 1.2))
		measuredSRateB = measuredSRateO / gStatStruct.inB_arcRatio;
	else
		measuredSRateB = measuredSRateO;

    // if channel not running, set to low priority.
    if (gStatStruct.inB_size == 0)
        gStatStruct.inB_size = 3000;  

	// Account for block size
    rateB = 1000000.0 * (float)gStatStruct.inB_size / measuredSRateB; // 2 ch
	rateO = 1000000.0 * 256.0 / measuredSRateO;

    TRACE(( &trace, "statStruct_resetPriorities: B: %d.  Out: %d.",
               (int)rateB, (int)rateO));

    if (rateB > rateO)
    {
        gStatStruct.inB_prio = HIGH_PRIO;
        gStatStruct.out_prio = MID_PRIO;
    }
    else
    {
        gStatStruct.inB_prio = MID_PRIO;
        gStatStruct.out_prio = HIGH_PRIO;
    }

    TRACE(( &trace, "statStruct_resetPriorities: B: %d.  O: %d.",
       gStatStruct.inB_prio, gStatStruct.out_prio));

    if (gStatStruct.inB_task != 0)
        TSK_setpri(gStatStruct.inB_task, gStatStruct.inB_prio);
    if (gStatStruct.out_task != 0)
        TSK_setpri(gStatStruct.out_task, gStatStruct.out_prio);
#endif
}



