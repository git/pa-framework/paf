//
// $Source: /cvsstl/ti/pa/f/s19/z14/zss/config/pa.cfg,v $
// $Revision: 1.1 $
//
// DSP/BIOS TextConf (TCONF) text-based configuration file
// for DA830 / zss Topo/Feature Set
//
// Copyright 2004-2008, Texas Instruments, Inc.  All rights reserved.
//
// $Log: pa.cfg,v $
//   mv pa.cfg pa.cfg.

// =============================================================================

// This line must come first (w/ "bios_6_10_00_25")
xdc.loadPackage('ti.bios.tconf');

// .............................................................................

var verbose = true;

var ProjName = environment["ProjName"];

var bK001 = ProjName.match( /_zss_/)     ? true : false;
// use (algo.) ROM for zss
// ~PAF_DEVICE_VERSION
// match() returns null if no match
var bSim  = ProjName.match( /_simda830/) ? true : false;

var topo = ProjName.replace( /pa_([a-z][a-z,0-9]+)_.*/, "$1");

// .............................................................................

var useBiosRom = true;

if( useBiosRom) {
  Program.importRomAssembly('ti.sysbios.rom.da830.romimage');
  Program.loadRom = bSim;
}

var Memory = xdc.useModule('xdc.runtime.Memory');
Memory.defaultHeapInstance.$orig.size = 0x1000 + 0x1000;
// .............................................................................
// Adjust size of SEM_Obj to fix init size errors (e.g. via SEM_new).
// This is currently a work-around until the BIOS team arrives at another soln.

Semaphore = xdc.useModule('ti.sysbios.ipc.Semaphore');
Semaphore.common$.namedInstance = false;

// .............................................................................

if( verbose) {
  print(   "ProjName = " + ProjName);
  print(      "bK001 = " + bK001);
  print(       "bSim = " + bSim);
  print(       "topo = " + topo);
  print( "useBiosRom = " + useBiosRom);
  print( "Program.loadRom = " + Program.loadRom);
}

// .............................................................................

xdc.useModule('ti.sysbios.hal.Cache');
xdc.useModule('ti.sysbios.family.c64p.Hwi');

// .............................................................................
// Cache Settings

var Cache = xdc.module("ti.sysbios.family.c64p.Cache");
Cache["initSize"]["l2Size"] = Cache.L2Size_64K;
Cache["MAR128_159"] = 0x0000ffff;
Cache["MAR192_223"] = 0x0000ffff;

// .............................................................................
// LOG prints over UART

var ENABLE_RTAUART = environment["ENABLE_RTAUART"];
if (ENABLE_RTAUART == "true") {
  var Idle = xdc.useModule('ti.sysbios.knl.Idle');
  var SysMin = xdc.useModule('xdc.runtime.SysMin');
  var Startup = xdc.useModule('xdc.runtime.Startup');
  Idle.addFunc('&SysMinToUart_idlFxn');
  SysMin.outputFxn = "&SysMinToUart_copyIntoBuffer";
  Startup.firstFxns.$add("&SysMinToUart_init");
  var LoggerBuf = xdc.useModule('xdc.runtime.LoggerBuf');
  LoggerBuf.enableFlush = true;
  if( verbose) print(   "ENABLE_RTAUART = true");
}

// .............................................................................
// RTA:
if (1)
{
    // RTA in stop mode  
    var Agent = xdc.useModule('ti.sysbios.rta.Agent');
    // as recommended in EricScott videos for BIOS 6
    Agent.transport = Agent.Transport_STOP_MODE_ONLY;
    
    var windowInMs = bSim ? 10 : 1000; // need short window in simulation
    var Load = xdc.useModule('ti.sysbios.utils.Load');
    Load.windowInMs = windowInMs;  // Default (500 ms)
    Load.updateInIdle = true;
}   
else
{   // RTA over RTDX
    
    var ENABLE_CPULOAD = environment["ENABLE_CPULOAD"];
    if (ENABLE_CPULOAD == "true") {
      var Defaults = xdc.useModule('xdc.runtime.Defaults');
      Defaults.common$.namedInstance = true;
    
      if( verbose) print(   "ENABLE_CPULOAD = true");
      // Bring in the Agent
      //     This runs as a thread of priority one (every 1000 ms)
      //     creates a thread at this priority using a non-parameterized stack size
      //     period is interpreted as a TSK_sleep in this created thread.
      //     Only one agent for all transports (that is the current architecture but may change)
      //     Priority 0 is not currently allowed (see SDSCM00027813)
    
      var windowInMs = bSim ? 10 : 1000; // need short window in simulation
    
      var Agent = xdc.useModule('ti.sysbios.rta.Agent');
    
      // as recommended in EricScott videos for BIOS 6
      Agent.transport = Agent.Transport_STOP_MODE_ONLY;
    
    
      Agent.configureSystemLog = false;  // Disable all RTA System Log (diagnostics USER1, USER2)
      Agent.numSystemRecords = 64;  // Default (64) when above true - number of records in the RTASystemLog
    
      Agent.configureLoadLog = true;    // Enable Thread load (diagnostics USER4)
      Agent.numLoadRecords = 64;  // Default (64) when above true - number of records in the RTALoadLog
    
      Agent.periodInMs = windowInMs;
      Agent.priority = 1;
    
      Agent.transferBufferSize = 85; // Default (85 records * 48 bytes LOGs) =~ 4K
      Agent.transferBufferSection = ".transferBufferSectionPa";
    
      // could remove following lines if default window OK
      // Bring in Load module to modify its update window
      var Load = xdc.useModule('ti.sysbios.utils.Load');
      Load.windowInMs = windowInMs;  // Default (500 ms)
      Load.updateInIdle = true;
    
      /*
       *  Bring in and configure the necessary RTDX modules and Driver Table
       *  needed for RTA.
       */
      var RtdxModule = xdc.useModule('ti.rtdx.RtdxModule');
      RtdxModule.bufferSizeInWords = 4096; // * MUST be >= 48 bytes * Agent.transferBufferSize *
      RtdxModule.protocol =
        bSim ? RtdxModule.PROTOCOL_SIMULATION :
               RtdxModule.PROTOCOL_JTAG; // or RtdxModule.PROTOCOL_HIGH_SPEED (XDS60 only)
    
      var RtdxDvr = xdc.useModule('ti.rtdx.driver.RtdxDvr');
        RtdxDvr.numInputChannels = 2;
    
      var rtdx = RtdxDvr.create();
    
      var DriverTable = xdc.useModule('ti.sdo.io.DriverTable');
      DriverTable.addMeta("/rtdx", rtdx);
    }  // end RTA over RTDX
} // end RTA

// .............................................................................

/* Enable ECM Handler */
bios.ECM.ENABLE = 1;
/* ECM configuration - manually Reflect these settings in soc.h */
bios.HWI.instance("HWI_INT7").interruptSelectNumber = 0;
bios.HWI.instance("HWI_INT8").interruptSelectNumber = 1;
bios.HWI.instance("HWI_INT9").interruptSelectNumber = 2;
bios.HWI.instance("HWI_INT10").interruptSelectNumber = 3;

 /* USE EDMA3 Sample App */
xdc.loadPackage('ti.sdo.edma3.drv.sample');

// .............................................................................

bios.GBL.CLKOUT = 300.00;

// adjust "Clock.tickPeriod" to account for timer difference--
// simulator operates timer at 50 MHz, whereas DSP/BIOS believes it's at 24 MHz
/// bios.CLK.MICROSECONDS = 2083;

// enable DSP/BIOS components
bios.MEM.NOMEMORYHEAPS = false;
bios.GBL.ENABLEALLTRC = true;
bios.TSK.ENABLETSK = true;

// configure IRAM memory section
bios.IRAM = bios.MEM.create("IRAM");
bios.IRAM.createHeap = 1;
bios.IRAM.heapSize = 181*1024-0x100;

// ~39 KB currently used: DIB scratch buffer, audio frame buffer, Dolby delay
bios.IRAM.space = "code/data";
bios.IRAM["heapLabel"] = "IRAM";

// create SDRAM section
bios.SDRAM = bios.MEM.create("SDRAM");
bios.SDRAM.createHeap = 1;
bios.SDRAM.heapSize = 1024*1024+600*1024;
bios.SDRAM.space = "data";
bios.SDRAM["heapLabel"] = "SDRAM";

// create L3RAM section
bios.L3RAM = bios.MEM.create("L3RAM");
bios.L3RAM.createHeap = 1;
bios.L3RAM.heapSize = 42*1024; //= Y13 setting, others use 34*1024; check with CCS ROV Tool
bios.L3RAM.space = "data";
bios.L3RAM["heapLabel"] = "L3RAM";

bios.setMemCodeSections (prog, bios.SDRAM);
bios.setMemDataHeapSections (prog, bios.IRAM);
bios.setMemDataNoHeapSections (prog, bios.IRAM);

// ------------------------------------
// Create and configure trace LOGS
// ------------------------------------
bios.trace = bios.LOG.create("trace");
//bios.trace.bufSeg = prog.get("L3RAM");
//bios.trace.bufLen = 2048; /* _ti.bios.tconf.LOG[1] -> _trace */
//bios.trace.logType = "circular";

// For development/debugging:
bios.trace.bufSeg = prog.get("SDRAM");
bios.trace.bufLen = 32*1024;
bios.trace.logType = "circular"; // "fixed";   // 
bios.trace.exitFlush = true;

// Create and configure trace LOG
bios.dcs6Trace = bios.LOG.create("dcs6Trace");
bios.dcs6Trace.bufSeg = prog.get("L3RAM");
bios.dcs6Trace.bufLen = 2048; /* _ti.bios.tconf.LOG[1] -> _trace */
bios.dcs6Trace.logType = "circular";
bios.dcs6Trace.exitFlush = true;

// Create another log for I4 builds
if (ProjName.match( /_i4_/)) {
  bios.traceIO = bios.LOG.create("traceIO");
  bios.traceIO.bufSeg = prog.get("L3RAM");
  bios.traceIO.bufLen = 2048; /* _ti.bios.tconf.LOG[1] -> _trace */
  bios.traceIO.logType = "circular";
  bios.traceIO.exitFlush = true;
}

bios.LOG_system.bufLen = 64; /* _ti.bios.tconf.LOG[0] -> _LOG_system */
// not currently used -- will be used in future for RTA

bios.LOG.OBJMEMSEG = prog.get("SDRAM");    /* .log    */
bios.MEM.STACKSEG = prog.get("IRAM");      /* .stack  */
bios.MEM.STACKSIZE = 0x800;
bios.MEM.CONSTSEG = prog.get("L3RAM");     /* .const  */
bios.MEM.FARSEG = prog.get("SDRAM");       /* .far    */
bios.MEM.HWIVECSEG = prog.get("IRAM");     /* .vecs   */
bios.MEM.TEXTSEG = prog.get("SDRAM");      /* .text   */
bios.SWI.OBJMEMSEG = prog.get("IRAM");     /* .swi    */
bios.MEM.SWITCHSEG  = prog.get("SDRAM");   /* .switch */
bios.MEM.CIOSEG = prog.get("SDRAM");       /* .cio    */
bios.MEM.ARGSSEG = prog.get("SDRAM");      /* .args   */
bios.MEM.CINITSEG = prog.get("SDRAM");     /* .cinit  */

bios.TSK.STACKSEG = prog.get("SDRAM");		/* .defaultStackSection */

Program.sectMap[".vecs"] = {loadSegment: "IRAM"};

Program.sectMap[".none"] = {loadSegment: "SDRAM"};
Program.sectMap[".const:uboot"] = {loadSegment: "UBOOT"};

Program.sectMap[".rtdx_data"] = {loadSegment: "SDRAM"};
Program.sectMap[".transferBufferSectionPa"] = {loadSegment: "SDRAM"};
Program.sectMap[".rtdx_text"] = {loadSegment: "SDRAM"};

Program.sectMap[".taskStackSection"] = "SDRAM";

// force strings into SDRAM
Program.sectMap[".const:.string"] = new Program.SectionSpec();
Program.sectMap[".const:.string"] .loadSegment = "SDRAM";


// bios.MEM.SYSMEMSEG = prog.get("IRAM"); // .sysmem
/* "compiler uses .sysmem for the rts library's (malloc/free) heap section" */

// .............................................................................
// Task Definitions

var AS_Output_betaPrimeValue = 0;   // match these to ss in AS_*-system.c

//***************************************
// One alpha interval processing task:
//***************************************

TSK_aip2 = bios.TSK.create("TSK_aip");
TSK_aip2.fxn = "alphaIntervalProcessingTask";
TSK_aip2.priority = 4;
TSK_aip2.stackSize = 0x600;
TSK_aip2.stackMemSeg = prog.get("SDRAM");
TSK_aip2.arg0 = AS_Output_betaPrimeValue;  // ignored?

//***************************************
// One stream task, 
//  each with matching idle task.

TSK_Output = bios.TSK.create("TSK_Output");
TSK_Output.fxn = "AS_Output_Task";
TSK_Output.priority = 8;
TSK_Output.stackSize = 0xc00;
TSK_Output.stackMemSeg = prog.get("L3RAM");
TSK_Output.arg0 = AS_Output_betaPrimeValue;
TSK_Output.arg1 = $externPtr("params_PA_" + topo + "_Output");
TSK_Output.arg2 = $externPtr("patchs_PA_" + topo + "_Output");

IDL_Output = bios.IDL.create("IDL_Output");
IDL_Output.fxn = "AS_Output_Idle";

//***************************************
// Time Slice:  
// Periodic Task Yield: PRD object creation
if (0)
{
    var PRD_yield = prog.module("PRD").create("PRD_yield");
    PRD_yield.comment = "Periodic Task Yield";
    PRD_yield.period = 1;
    PRD_yield.mode = "continuous";
    PRD_yield.fxn = "PRD_Tsk_yield";
    PRD_yield.arg0 = 0;

    // Timer period register:  0.5ms
    prog.module("CLK").MICROSECONDS = 500;
}

//***************************************
IDL_dap = bios.IDL.create("IDL_dap");
IDL_dap.fxn = "DAP_watchDog";

bios.TSK.STACKSIZE = 0x200;

// .............................................................................
// SIO Drivers

bios.DAP = bios.UDEV.create("DAP");
bios.DAP.comment = "McASP SIO Driver";
bios.DAP.initFxn = "DAP_init";
bios.DAP.fxnTable = "DAP_FXNS";

//bios.DAB = bios.UDEV.create("DAB");
//bios.DAB.comment = "McBSP SIO Driver";
//bios.DAB.initFxn = "DAB_init";
//bios.DAB.fxnTable = "DAB_FXNS";

bios.DIB = bios.UDEV.create("DIB");
bios.DIB.comment = "Stacking Input Buffer Driver";
bios.DIB.initFxn = "DIB_init";
bios.DIB.fxnTable = "DIB_FXNS";

bios.DOB = bios.UDEV.create("DOB");
bios.DOB.comment = "Stacking Output Buffer Driver";
bios.DOB.initFxn = "DOB_init";
bios.DOB.fxnTable = "DOB_FXNS";

// .............................................................................
// Note that DSPLINK is not defined for simulator builds so no need to check in
// the positive if condition. If the false condition we need to include the UART
// AFP task only when not in the simulator.

var DSPLINK = environment["DSPLINK"];
if (DSPLINK == "true") {
  if( verbose) print(   "DSPLINK = true");
  bios.MSGQ.ENABLEMSGQ = true;
  bios.POOL.ENABLEPOOL = true;

  TSK_afp             = bios.TSK.create ("TSK_afp");
  TSK_afp.fxn         = "afpLinkTask";
  TSK_afp.priority    = 3;
  TSK_afp.stackSize   = 0x1000;
  TSK_afp.stackMemSeg = prog.get("SDRAM");
  TSK_afp.arg0        = 0;
  TSK_afp.arg1        = 0;

  var Idle = xdc.useModule ('ti.sysbios.knl.Idle');
  Idle.addFunc ('&nicLinkTask');
}
else if (!bSim) {
  if( verbose) print(   "DSPLINK = false, setting up AFPUart");
  TSK_afp             = bios.TSK.create ("TSK_afp");
  TSK_afp.fxn         = "AFPUartTask";
  TSK_afp.fxn         = "AFP6_task";
  TSK_afp.priority    = 3;
  TSK_afp.stackSize   = 0x1000;
  TSK_afp.stackMemSeg = prog.get("SDRAM");
  TSK_afp.arg0        = 0;
  TSK_afp.arg1        = 0;

  var Idle = xdc.useModule ('ti.sysbios.knl.Idle');
  Idle.addFunc ('&customSystemStreamIdleNIC');
}


// .............................................................................

Timer = xdc.useModule('ti.sysbios.timers.timer64.Timer');
Clock = xdc.useModule('ti.sysbios.knl.Clock');
Timer.timerSettings[1].master = true;
Timer.defaultHalf = Timer.Half_LOWER;
Clock.timerId = 1;

// .............................................................................

if( useBiosRom) {
  // BIOS ROM data should be loaded to this address
  /// Program.$capsule._romAsm.loadDataAddr = 0xC3800000; // bios_6_20_00_13_2 or earlier
  Program.loadFixedDataAddr = 0xC3800000; // bios_6_20_00_23 or later
}
var Text= xdc.useModule("xdc.runtime.Text"); 

Text.isLoaded = true;

print("Text.isLoaded = " + Text.isLoaded);



// get handle to xdc Startup module
var Startup = xdc.useModule('xdc.runtime.Startup');

// install "reset function"
Startup.resetFxn = '&copyBiosRomData';
