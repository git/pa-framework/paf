
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

#ifndef _PA_ZSS_EVMDA830_IO_A
#define _PA_ZSS_EVMDA830_IO_A

#include <acpbeta.h>
#include <zss_a.h>

#define stream1     0xcd09,0x0400
#define stream2     0xcd09,0x0401
#define stream3     0xcd09,0x0402
#define stream4     0xcd09,0x0403
#define stream5     0xcd09,0x0404

#define streamOutputMaster stream1  // AS_Output_Task
#define streamOutputSlave  stream2  // AS_Output_Task

#define defaultStream      streamOutputMaster

// execPAZInOutError : if returned, input/output "none" selected
#define  execPAZInOutError      0xf1ff

// -----------------------------------------------------------------------------
// IB SIO Select Register is set by the execPAZIn* shortcuts

#define  execPAZInNone          0xf120
#define  execPAZInDigital       0xf121
#define  execPAZInAnalog        0xf122
#define  execPAZInAnalogStereo  0xf123
#define  execPAZInSing          0xf127


// These values reflect the definition of devinp[]
#define DEVINP_NULL             0
#define DEVINP_DIR              1
#define DEVINP_ADC1             2
#define DEVINP_ADC_STEREO       3
#define DEVINP_N                4

#define wroteIBSioCommandNone           0xca00+STD_BETA_IB,0x0500+DEVINP_NULL
#define wroteIBSioCommandDigital        0xca00+STD_BETA_IB,0x0500+DEVINP_DIR
#define wroteIBSioCommandAnalog         0xca00+STD_BETA_IB,0x0500+DEVINP_ADC1
#define wroteIBSioCommandAnalogStereo   0xca00+STD_BETA_IB,0x0500+DEVINP_ADC_STEREO


#define wroteIBSioSelectNone            0xca00+STD_BETA_IB,0x0580+DEVINP_NULL
#define wroteIBSioSelectDigital         0xca00+STD_BETA_IB,0x0580+DEVINP_DIR
#define wroteIBSioSelectAnalog          0xca00+STD_BETA_IB,0x0580+DEVINP_ADC1
#define wroteIBSioSelectAnalogStereo    0xca00+STD_BETA_IB,0x0580+DEVINP_ADC_STEREO


// -----------------------------------------------------------------------------
// OB SIO Select Register is set by the execPAZOut* shortcuts

#define  execPAZOutNone                 0xf130
#define  execPAZOutAnalog               0xf131 //8 channel output analog (24bit)
#define  execPAZOutDigital              0xf132 //8 channel output analog (24bit)
#define  execPAZOutAnalogSlave          0xf133 //8 channel output analog (24bit) slave to input

// These values reflect the definition of devout[]
#define DEVOUT_NULL             0
#define DEVOUT_DAC              1
#define DEVOUT_DIT              2
#define DEVOUT_DAC_SLAVE        3
#define DEVOUT_N                4

#define wroteOBSioCommandNone               0xca00+STD_BETA_OB,0x0500+DEVOUT_NULL      
#define wroteOBSioCommandAnalog             0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC       
#define wroteOBSioCommandDigital            0xca00+STD_BETA_OB,0x0500+DEVOUT_DIT       
#define wroteOBSioCommandAnalogSlave        0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_SLAVE 

#define wroteOBSioSelectNone                0xca00+STD_BETA_OB,0x0580+DEVOUT_NULL      
#define wroteOBSioSelectAnalog              0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC       
#define wroteOBSioSelectDigital             0xca00+STD_BETA_OB,0x0580+DEVOUT_DIT       
#define wroteOBSioSelectAnalogSlave         0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_SLAVE 

// -----------------------------------------------------------------------------

#endif // _PA_ZSS_EVMDA830_IO_A
