
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

#include <acptype.h>
#include <pafaip_a.h>
#include "alpha\pa_zss_evmda830_io_a.h"

#include <ztop.h>
#include <io.h>

#if ! defined (ALL_SYNCHRONOUS_ZSS)
    #error ALL_SYNCHRONOUS_ZSS not defined.
#endif

// -----------------------------------------------------------------------------
#include <asj_a.h>

  // no ARC, only two channels
#define CUS_ATBOOT_S \
    /* streamOutputSlave is in fact the secondary input to ASJ */ \
    streamOutputSlave, \
    writeDECChannelMapTo16(SLAVE_CH_0, SLAVE_CH_1, SLAVE_CH_2, SLAVE_CH_3, SLAVE_CH_4, SLAVE_CH_5, SLAVE_CH_6, SLAVE_CH_7, SLAVE_CH_8, SLAVE_CH_9, SLAVE_CH_A, SLAVE_CH_B, SLAVE_CH_C, SLAVE_CH_D, SLAVE_CH_E, SLAVE_CH_F), \
    execPAZInNone, \
    execPAZInDigital, \
    \
    /* streamOutputMaster is the primary input to ASJ and the output */ \
    streamOutputMaster, \
    writeDECChannelMapTo16(MASTER_CH_0, MASTER_CH_1, MASTER_CH_2, MASTER_CH_3, MASTER_CH_4, MASTER_CH_5, MASTER_CH_6, MASTER_CH_7, MASTER_CH_8, MASTER_CH_9, MASTER_CH_A, MASTER_CH_B, MASTER_CH_C, MASTER_CH_D, MASTER_CH_E, MASTER_CH_F), \
    writeENCChannelMapFrom16(MASTER_CH_0, MASTER_CH_1, MASTER_CH_2, MASTER_CH_3, MASTER_CH_4, MASTER_CH_5, MASTER_CH_6, MASTER_CH_7, MASTER_CH_8, MASTER_CH_9, MASTER_CH_A, MASTER_CH_B, MASTER_CH_C, MASTER_CH_D, MASTER_CH_E, MASTER_CH_F), \
    execPAZInNone, \
    execPAZOutNone, \
    writeASJForm16 (0x10, 0x11, 0x12, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x18, 0x19, 0x0f, 0x0f, 0x1c, 0x0f, 0x0f, 0x0f), \
    writeVOLControlMasterN(0), /* full volume */ \
    writeVOLRampTimeN(1), \
    execPAZOutAnalog, \
    execPAZInAnalog
    // leave stream1 as default (streamOutputMaster)

// -----------------------------------------------------------------------------

#pragma DATA_SECTION(cus_atboot_s0_patch, ".none")
const ACP_Unit cus_atboot_s0_patch[] = {
    0xc900 + 0 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s_patch[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (cus_atboot_s, ".text:cus_atboot_s")
#endif // USE_EXT_RAM
const ACP_Unit cus_atboot_s[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (cus_attime_s, ".text:cus_attime_s")
#endif // USE_EXT_RAM
const ACP_Unit cus_attime_s[] = {
    0xc900,
};
// EOF
