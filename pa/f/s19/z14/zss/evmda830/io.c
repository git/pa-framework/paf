
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//

//
// Input / output device configuration definitions
//

#include <acptype.h>
#include <acpbeta.h>
#include <pafstd_a.h>
#include <pafdec_a.h>
#include <pafenc_a.h>
#include <paftyp.h>
#include <pafsio.h>

#include <alpha/pcm_a.h>

#include <inpbuf_a.h>
#include <outbuf_a.h>

#include "ztop.h"

#include "alpha\pa_zss_evmda830_io_a.h"
#include <dap_e17.h>

#define  rb32DECSourceDecode 0xc024,0x0b81
#define  ob32DECSourceDecodeNone 0x0001,0x0000

#define  rb32IBSioSelect 0xc022,0x0581
#define  ob32IBSioSelect(X) (X)|0x0080,0x0000

#define  ob32OBSioSelect(X) (X)|0x0080,0x0000
#define  rb32DECSourceSelect_3 0xc024,0x09b1
#define  wb32DECSourceSelect_3 0xc024,0x09f1

#define writePA3Await(RB32,WB32) 0xcd0b,5+2,0x0204,200,10,WB32,RB32

// -----------------------------------------------------------------------------
//
// Input device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVINP_N];
} patchs_devinp[1] =
{
    DEVINP_N,
    // These values reflect the definitions DEVINP_* in pa*io_a.h:
    NULL,       // InNone            DEVINP_NULL   0xf120   Sigma32
                // InDigital         DEVINP_DIR    0xf121   Sigma33
    (const PAF_SIO_Params *) &DAP_E17_RX_DIR,                 
                // InAnalog          DEVINP_ADC1   0xf122   Sigma34
    (const PAF_SIO_Params *) &DAP_E17_RX_ADC_48000HZ,         
                // InAnalogStereo    DEVINP_ADC_STE 0xf123  Sigma35
    (const PAF_SIO_Params *) &DAP_E17_RX_ADC_STEREO_48000HZ,  
                // InSing                           0xF127, Sigma39.
};

// .............................................................................
// execPAZInNone
#define CUS_SIGMA32_S \
    writeDECSourceSelectNone, \
    writeIBSioSelectN(DEVINP_NULL), \
    0xcdf0,execPAZInNone

#pragma DATA_SECTION(cus_sigma32_s0, ".none")
const ACP_Unit cus_sigma32_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA32_S,
};

#pragma DATA_SECTION(cus_sigma32_s, ".none")
const ACP_Unit cus_sigma32_s[] = {
    0xc900 + sizeof(cus_sigma32_s0)/2 - 1,
    CUS_SIGMA32_S,
};

// .............................................................................

// execPAZInDigital
#define CUS_SIGMA33_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBUnknownTimeoutN(2*2048), \
    writeIBScanAtHighSampleRateModeDisable, \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideDisable, \
    writeIBPrecisionDefaultOriginal, \
    writeIBPrecisionOverrideDetect, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_DIR), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAZInDigital

#pragma DATA_SECTION(cus_sigma33_s0, ".none")
const ACP_Unit cus_sigma33_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA33_S,
};

#pragma DATA_SECTION(cus_sigma33_s, ".none")
const ACP_Unit cus_sigma33_s[] = {
    0xc900 + sizeof(cus_sigma33_s0)/2 - 1,
    CUS_SIGMA33_S,
};

// .............................................................................
// execPAZInAnalog
#define CUS_SIGMA34_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAZInAnalog

#pragma DATA_SECTION(cus_sigma34_s0, ".none")
const ACP_Unit cus_sigma34_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA34_S,
};

#pragma DATA_SECTION(cus_sigma34_s, ".none")
const ACP_Unit cus_sigma34_s[] = {
    0xc900 + sizeof(cus_sigma34_s0)/2 - 1,
    CUS_SIGMA34_S,
};

// .............................................................................
// execPAZInAnalogStereo
#define CUS_SIGMA35_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC_STEREO), \
    writeDECSourceSelectPCM,                 \
    0xcdf0,execPAZInAnalogStereo

#pragma DATA_SECTION(cus_sigma35_s0, ".none")
const ACP_Unit cus_sigma35_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA35_S,
};

#pragma DATA_SECTION(cus_sigma35_s, ".none")
const ACP_Unit cus_sigma35_s[] = {
    0xc900 + sizeof(cus_sigma35_s0)/2 - 1,
    CUS_SIGMA35_S,
};

#if 0 // SNG isn't present
// .............................................................................
// execPAZInSing
#define CUS_SIGMA39_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectSing, \
    writeSNGSongMultiTone, \
    0xcdf0,execPAZInSing

// writeSNGSongToneSatSequence

#pragma DATA_SECTION(cus_sigma39_s0, ".none")
const ACP_Unit cus_sigma39_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA39_S,
};

#pragma DATA_SECTION(cus_sigma39_s, ".none")
const ACP_Unit cus_sigma39_s[] = {
    0xc900 + sizeof(cus_sigma39_s0)/2 - 1,
    CUS_SIGMA39_S,
};
#endif

// -----------------------------------------------------------------------------
//
// Output device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVOUT_N];
} patchs_devout[1] =
{
    DEVOUT_N,
        // These values reflect the definitions DEVOUT_* in pa*io_a.h:
    NULL,       // OutNone                   0xf130      Sigma48
                // OutAnalog     DEVOUT_DAC  0xf131      Sigma49
    (const PAF_SIO_Params *) &DAP_E17_TX_DAC,
    // OutDigital DEVOUT_DIT  0xf132  Sigma50
    (const PAF_SIO_Params *) &DAP_E17_TX_DIT,               
    // OutAnalogSlave DEVOUT_DAC_SLAVE  0xf133  Sigma51
    (const PAF_SIO_Params *) &DAP_E17_TX_DAC_SLAVE,         // 
};

// .............................................................................
// execPAZOutNone
#define CUS_SIGMA48_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_NULL), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutNone

#pragma DATA_SECTION(cus_sigma48_s0, ".none")
const ACP_Unit cus_sigma48_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA48_S,
};

#pragma DATA_SECTION(cus_sigma48_s, ".none")
const ACP_Unit cus_sigma48_s[] = {
    0xc900 + sizeof(cus_sigma48_s0)/2 - 1,
    CUS_SIGMA48_S,
};

// .............................................................................
// execPAZOutAnalog
#define CUS_SIGMA49_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(1), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutAnalog

#pragma DATA_SECTION(cus_sigma49_s0, ".none")
const ACP_Unit cus_sigma49_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA49_S,
};

#pragma DATA_SECTION(cus_sigma49_s, ".none")
const ACP_Unit cus_sigma49_s[] = {
    0xc900 + sizeof(cus_sigma49_s0)/2 - 1,
    CUS_SIGMA49_S,
};

// .............................................................................
// execPAZOutDigital
#define CUS_SIGMA50_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DIT), \
    writeENCChannelMapTo16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutDigital

#pragma DATA_SECTION(cus_sigma50_s0, ".none")
const ACP_Unit cus_sigma50_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA50_S,
};

#pragma DATA_SECTION(cus_sigma50_s, ".none")
const ACP_Unit cus_sigma50_s[] = {
    0xc900 + sizeof(cus_sigma50_s0)/2 - 1,
    CUS_SIGMA50_S,
};

// .............................................................................
// execPAZOutAnalogSlave
#define CUS_SIGMA51_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_SLAVE), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAZOutAnalogSlave

#pragma DATA_SECTION(cus_sigma51_s0, ".none")
const ACP_Unit cus_sigma51_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA51_S,
};

#pragma DATA_SECTION(cus_sigma51_s, ".none")
const ACP_Unit cus_sigma51_s[] = {
    0xc900 + sizeof(cus_sigma51_s0)/2 - 1,
    CUS_SIGMA51_S,
};

// -----------------------------------------------------------------------------

// EOF
