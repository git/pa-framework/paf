
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Input/Output channel configuration definitions
//
//

#include "ztop.h"

#define IO_SIZE_OF_ELEMENT      4  // 4 bytes, float

#define IO_NUM_CHANS_MASTER     ZTOP_NUM_CHANS_MAX_MASTER   // AS_Output
#define IO_NUM_CHANS_SLAVE      ZTOP_NUM_CHANS_MAX_SLAVE    // AS_Output

// .............................................................................

#if (IO_NUM_CHANS_MASTER == 0)
    #define MASTER_CH_0   -3
    #define MASTER_CH_1   -3
    #define MASTER_CH_2   -3
    #define MASTER_CH_3   -3
    #define MASTER_CH_4   -3
    #define MASTER_CH_5   -3
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 2)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   -3
    #define MASTER_CH_3   -3
    #define MASTER_CH_4   -3
    #define MASTER_CH_5   -3
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 6)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   -3
    #define MASTER_CH_7   -3
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 8)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   PAF_LBAK
    #define MASTER_CH_7   PAF_RBAK
    #define MASTER_CH_8   -3
    #define MASTER_CH_9   -3
    #define MASTER_CH_A   -3
    #define MASTER_CH_B   -3
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#elif (IO_NUM_CHANS_MASTER == 12)
    #define MASTER_CH_0   PAF_LEFT
    #define MASTER_CH_1   PAF_RGHT
    #define MASTER_CH_2   PAF_LSUR
    #define MASTER_CH_3   PAF_RSUR
    #define MASTER_CH_4   PAF_CNTR
    #define MASTER_CH_5   PAF_SUBW
    #define MASTER_CH_6   PAF_LBAK
    #define MASTER_CH_7   PAF_RBAK
    #define MASTER_CH_8   PAF_LWID
    #define MASTER_CH_9   PAF_RWID
    #define MASTER_CH_A   PAF_LHED
    #define MASTER_CH_B   PAF_RHED
    #define MASTER_CH_C   -3
    #define MASTER_CH_D   -3
    #define MASTER_CH_E   -3
    #define MASTER_CH_F   -3
#else
  ERROR Unsupported MASTER channel number
#endif

// .............................................................................

#if (IO_NUM_CHANS_SLAVE == 0)
    #define SLAVE_CH_0   -3
    #define SLAVE_CH_1   -3
    #define SLAVE_CH_2   -3
    #define SLAVE_CH_3   -3
    #define SLAVE_CH_4   -3
    #define SLAVE_CH_5   -3
    #define SLAVE_CH_6   -3
    #define SLAVE_CH_7   -3
    #define SLAVE_CH_8   -3
    #define SLAVE_CH_9   -3
    #define SLAVE_CH_A   -3
    #define SLAVE_CH_B   -3
    #define SLAVE_CH_C   -3
    #define SLAVE_CH_D   -3
    #define SLAVE_CH_E   -3
    #define SLAVE_CH_F   -3
#elif (IO_NUM_CHANS_SLAVE == 2)
    #define SLAVE_CH_0   PAF_LEFT
    #define SLAVE_CH_1   PAF_RGHT
    #define SLAVE_CH_2   -3
    #define SLAVE_CH_3   -3
    #define SLAVE_CH_4   -3
    #define SLAVE_CH_5   -3
    #define SLAVE_CH_6   -3
    #define SLAVE_CH_7   -3
    #define SLAVE_CH_8   -3
    #define SLAVE_CH_9   -3
    #define SLAVE_CH_A   -3
    #define SLAVE_CH_B   -3
    #define SLAVE_CH_C   -3
    #define SLAVE_CH_D   -3
    #define SLAVE_CH_E   -3
    #define SLAVE_CH_F   -3
#elif (IO_NUM_CHANS_SLAVE == 6)
    #define SLAVE_CH_0   PAF_LEFT
    #define SLAVE_CH_1   PAF_RGHT
    #define SLAVE_CH_2   PAF_LSUR
    #define SLAVE_CH_3   PAF_RSUR
    #define SLAVE_CH_4   PAF_CNTR
    #define SLAVE_CH_5   PAF_SUBW
    #define SLAVE_CH_6   -3
    #define SLAVE_CH_7   -3
    #define SLAVE_CH_8   -3
    #define SLAVE_CH_9   -3
    #define SLAVE_CH_A   -3
    #define SLAVE_CH_B   -3
    #define SLAVE_CH_C   -3
    #define SLAVE_CH_D   -3
    #define SLAVE_CH_E   -3
    #define SLAVE_CH_F   -3
#elif (IO_NUM_CHANS_SLAVE == 8)
    #define SLAVE_CH_0   PAF_LEFT
    #define SLAVE_CH_1   PAF_RGHT
    #define SLAVE_CH_2   PAF_LSUR
    #define SLAVE_CH_3   PAF_RSUR
    #define SLAVE_CH_4   PAF_CNTR
    #define SLAVE_CH_5   PAF_SUBW
    #define SLAVE_CH_6   PAF_LBAK
    #define SLAVE_CH_7   PAF_RBAK
    #define SLAVE_CH_8   -3
    #define SLAVE_CH_9   -3
    #define SLAVE_CH_A   -3
    #define SLAVE_CH_B   -3
    #define SLAVE_CH_C   -3
    #define SLAVE_CH_D   -3
    #define SLAVE_CH_E   -3
    #define SLAVE_CH_F   -3
#elif (IO_NUM_CHANS_SLAVE == 12)
    #define SLAVE_CH_0   PAF_LEFT
    #define SLAVE_CH_1   PAF_RGHT
    #define SLAVE_CH_2   PAF_LSUR
    #define SLAVE_CH_3   PAF_RSUR
    #define SLAVE_CH_4   PAF_CNTR
    #define SLAVE_CH_5   PAF_SUBW
    #define SLAVE_CH_6   PAF_LBAK
    #define SLAVE_CH_7   PAF_RBAK
    #define SLAVE_CH_8   PAF_LWID
    #define SLAVE_CH_9   PAF_RWID
    #define SLAVE_CH_A   PAF_LHED
    #define SLAVE_CH_B   PAF_RHED
    #define SLAVE_CH_C   -3
    #define SLAVE_CH_D   -3
    #define SLAVE_CH_E   -3
    #define SLAVE_CH_F   -3
#else
  ERROR Unsupported SLAVE channel number
#endif

// .............................................................................
