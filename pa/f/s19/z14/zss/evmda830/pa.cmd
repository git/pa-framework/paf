/*
* $Source: /cvsstl/ti/pa/f/s19/z14/zss/evmda830/pa.cmd,v $
* $Revision: 1.1 $
*
* Add Description here
*
*
* $Log: pa.cmd,v $
*/


SECTIONS {

   /* This directive ensures that the message sample can be used with a DSP
    * executable from which the symbol table has been stripped out.
    * In such scenario, the actual shared memory start address for the DRV
    * component must be used as the fill contents for this data section.
    */

    .data:DSPLINK_shmBaseAddress: fill=0xC3F05000 {} > SDRAM

   /* This directive creates an alias of start address of RAM_BIOSRAM_DATA
    * segment to RamBiosromData_start sysmbol which can be called in code through
    * "extern const unsigned int RamBiosromData_start". Thus code using this start
    * address, can avoid hard coding and get the address through RamBiosromData_start.
    */

    RamBiosromData:{_RamBiosromData_start = .;} >  RAM_BIOSROM_DATA
}
