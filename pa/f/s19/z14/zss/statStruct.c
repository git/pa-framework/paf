
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Status tracking and global priority control for async Z-topology
//
// Copyright (c) 2013, Texas Instruments, Inc.  All rights reserved.
// Copyright (c) 2013, Momentum Data Systems.  All rights reserved.
//
//

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// Define to enable local tracing features:
//  You can read most stat info from gStatStruct.
//  The CCS debugger can be set to update it continuously while running.
// #define STAT_DEBUG
#ifdef STAT_DEBUG
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// .............................................................................

#include <tsk.h>
#include "statStruct.h"

// add this global to the watched expressions in the debugger.
// The debugger updates this in real time, or see it when you break.
// Or write a complex breakpoint trigger using it.
statStruct_t gStatStruct;


void statStruct_UpdateFrameCount(int taskID, int frameCount, int reset)
{
    int *pCurFrameCount;
    int *pLastFrameCount;
    int *pMaxFrameCount;
    int *pResetCount;
    int curResetCount;

    switch (taskID) {
        case STATSTRUCT_OUTPUT:
            pCurFrameCount  = &gStatStruct.out_CurFrameCount;
            pLastFrameCount = &gStatStruct.out_LastFrameCount;
            pMaxFrameCount  = &gStatStruct.out_MaxFrameCount;
            pResetCount     = &gStatStruct.out_ResetCount;
            if (gStatStruct.out_task == 0) 
            {
                gStatStruct.out_task = TSK_self();
            }
            gStatStruct.O_watchdogCount = 0;
            break;
        default:  // should not get here.
            statStruct_resetPriorities();
            return;
    }   

  #if 0
    // watchdogs for debugging
    if (gStatStruct.out_CurFrameCount == gStatStruct.out_LastFrameCount) {
        gStatStruct.O_watchdogCount++;
        if (gStatStruct.O_watchdogCount > 12) {
            if (gStatStruct.out_LastFrameCount > 3)
                TRACE(( &trace, "Why did O die?  Break here."));
        }
    }
  #endif

    statStruct_resetPriorities();

    curResetCount = *pResetCount;
    if (reset) {
        *pCurFrameCount = 0;
        curResetCount++;
    }
    else {
        *pCurFrameCount = *pLastFrameCount = frameCount;
        if (frameCount > *pMaxFrameCount) {
            *pMaxFrameCount = frameCount;
        }
    }
    *pResetCount = curResetCount;

    // complex breakpoint trigger, if we reset after a long time, stop the trace.
    if ((*pLastFrameCount > 40000) && reset)
    {	
    	TRACE(( &trace, "Unexpected reset condition."));
    	TRACE(( &trace, "Check the trace logs."));
    	LOG_disable(&trace);  // stop tracing
    	if (reset)
    		TRACE(( &trace, "Dummy statement, set breakpoint here."));
    	return;
    }

    return;
}

void statStruct_LogLock(int taskID, int locked)
{ }

void statStruct_LogFullSRateChange(int taskID)
{ }


void statStruct_LogSampleRate(int taskID, int sRate)
{

    switch (taskID) {
        default:
            gStatStruct.out_sRate = sRate;
            break;
    }
}

void statStruct_SetDibDeviceHandle(int taskID, int handle)
{
    switch (taskID) {
        default:
            return;
    }

}

void statStruct_LogDibSize(int handle, int size)
{
    return;
}

void statStruct_resetPriorities(void)
{
}



