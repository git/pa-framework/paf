
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Status tracking and global priority control for async Z-topology
//
// Copyright (c) 2013, Texas Instruments, Inc.  All rights reserved.
// Copyright (c) 2013, Momentum Data Systems.  All rights reserved.
//
//

// -----------------------------------------------------------------------------
#include <stdio.h>
#include <std.h>
#include <alg.h>
#include <ialg.h>
#include <sio.h>
#include <mem.h>
#include <tsk.h>
#include <string.h> // memset
#include <sts.h>
#include <clk.h>


typedef struct statStruct
{
    // the order here is chosen to put the key things at the top
    // when viewing in the debugger
    int out_CurFrameCount;
    int out_ResetCount;
    int out_prio;
    int out_sRate;

    // below here are less critical things.
    int out_MaxFrameCount;
    int out_LastFrameCount;
    void* out_task;
    int   O_watchdogCount;

} statStruct_t;

#define STATSTRUCT_OUTPUT      2

#define HIGH_PRIO   9
#define MEDIUM_PRIO 8
#define MID_PRIO    8
#define LOW_PRIO    7

extern statStruct_t gStatStruct;
extern void statStruct_UpdateFrameCount(int taskID, int frameCount, int reset);
extern void statStruct_LogLock(int taskID, int locked);
extern void statStruct_LogFullSRateChange(int taskID);
extern void statStruct_LogSampleRate(int taskID, int sRate);
extern void statStruct_SetDibDeviceHandle(int taskID, int handle);
extern void statStruct_LogDibSize(int handle, int size);
extern void statStruct_resetPriorities(void);

