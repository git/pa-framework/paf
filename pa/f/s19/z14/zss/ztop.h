
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// 
// Definitions shared by multiple Z-Topology files
// 

#ifndef _ZTOP
#define _ZTOP

// defining CJP_PATCH_AROUND makes the threads ignore errors.
// It seems too agressive now, as it also stops sound.
// #define CJP_PATCH_AROUND

// stream offsets are these plus 1.
#define AS_Output_betaPrimeValue            0

// to be used everywhere; audio frame size
#define ZTOP_FRAMESIZE                      256

// Maximum number of channels per audio stream
#if defined (ALL_SYNCHRONOUS_ZSS)
    #define ZTOP_NUM_CHANS_MAX_MASTER       8       // AS_Output
    #define ZTOP_NUM_CHANS_MAX_SLAVE        6       // AS_Output
#else
    #error ALL_SYNCHRONOUS_ZSS not defined.
#endif

// These bit masks enable us to ensure startup order.
// All threads must be running before AIP can run.
#define   ZTOP_OUTPUT_IDLE_BIT              4
#define   ZTOP_IDLE_COMPLETE                (ZTOP_OUTPUT_IDLE_BIT)

#define   ZTOP_OUTPUT_THREAD_BIT            0x40
#define   ZTOP_THREADS_RUNNING              (ZTOP_OUTPUT_THREAD_BIT)

#define   ZTOP_STARTUP_READY_FOR_AIP        (ZTOP_IDLE_COMPLETE | ZTOP_THREADS_RUNNING)

extern volatile Int gStartupOrder;  // declared in main.c

// Tracing Control:
// For each of the framework files, tracing can be enabled at compile time 
//  by setting these bits in the mask -- copied here for reference:
//#define TRACE_MASK_TERSE        1   // only flag errors
//#define TRACE_MASK_GENERAL      2   // half dozen lines per frame
//#define TRACE_MASK_VERBOSE      4   // trace full operation
//#define TRACE_MASK_DATA         8   // Show data

#define AS_OUTPUT_TRACE_MASK    3
#define AS_COMMON_TRACE_MASK    1

#endif /* _ZTOP */
