
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Framework support function (declarations)
//
//
//

#ifndef AS0_
#define AS0_

#include <alg.h>
#include <sio.h>

#include <acp.h>
#include <pafdec.h>
#include <pafenc.h>
#include <pafsio.h>
#include "pafhjt.h"

extern Int
PAF_DEC_deviceAllocate(
    SIO_Handle *pHandle,
    int mode,
    int heapID,
    int bufSize,
    Ptr pBufCfg );

extern Int
PAF_DEC_deviceSelect(
    SIO_Handle *pHandle,
    int mode,
    int heapID,
    Ptr Params );

extern Int
PAF_DEC_computeFrameLength(ALG_Handle alg, Int frameLength, Int bufferRatio);

extern Int
PAF_DEC_updateInputStatus(SIO_Handle, PAF_InpBufStatus *, PAF_InpBufConfig *);

extern Int
PAF_ARC_controlRate(SIO_Handle hRxSio, SIO_Handle hTxSio, ACP_Handle acp, double arcRatio); 

extern Int
PAF_BUF_copy( Uns, PAF_InpBufConfig *, Uns, PAF_OutBufConfig *);

#endif /* AS0_ */
