
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Audio Stream Processing Algoritm Interface Declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of all Audio Stream Processing Algoritms.
 */
#ifndef IASP_
#define IASP_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include <paftyp.h>

/*
 *  ======== IASP_Obj ========
 *  Every implementation of IASP *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IASP_Obj {
    struct IASP_Fxns *fxns;    /* function list: standard, public, private */
} IASP_Obj;

/*
 *  ======== IASP_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IASP_Obj *IASP_Handle;

/*
 *  ======== IASP_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IASP_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 unused[3];
} IASP_Status;

/*
 *  ======== IASP_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IASP_Config {
    Int dummy;
} IASP_Config;

/*
 *  ======== IASP_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of an IASP object.
 *
 *  Every implementation of IASP *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IASP_Params {
    Int size;
    const IASP_Status *pStatus;
    IASP_Config config;
} IASP_Params;

/*
 *  ======== IASP_PARAMS ========
 *  Default instance creation parameters (defined in asp.c)
 */
extern const IASP_Params IASP_PARAMS;

/*
 *  ======== IASP_Fxns ========
 *  All implementation's of IASP must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is IASP_<vendor>_IASP<number>, where
 *  <vendor> is the vendor name and <number> is the instantiation (0 demo).
 */
typedef struct IASP_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IASP_Handle, PAF_AudioFrame *);
    Int         (*apply)(IASP_Handle, PAF_AudioFrame *);
    /* private */
} IASP_Fxns;

/*
 *  ======== IASP_Cmd ========
 *  The Cmd enumeration defines the control commands for the IASP
 *  control method.
 */
typedef enum IASP_Cmd {
    IASP_NULL,
    IASP_GETSTATUSADDRESS1,
    IASP_GETSTATUSADDRESS2,
    IASP_GETSTATUS,
    IASP_SETSTATUS
} IASP_Cmd;

#endif  /* IASP_ */

