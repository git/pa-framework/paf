
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha Code Shortcuts for Standard Listening Modes
//
//
//

#include <acptype.h>
#include <pafstd_a.h>
#include <pafsys_a.h>
#include <pafdec_a.h>
#include <pafvol_a.h>

#define LM_UNKNOWN_S \
    wroteSYSListeningModeUnknown, \
    execSTDReady

#pragma DATA_SECTION(lm_unknown_s0, ".none")
const ACP_Unit lm_unknown_s0[] = {
    0xc900 + 0 - 1,
    LM_UNKNOWN_S,
};

const ACP_Unit lm_unknown_s[] = {
    0xc900 + sizeof(lm_unknown_s0)/2 - 1,
    LM_UNKNOWN_S,
};

#define LM_STANDARD_S \
    writeDECASPGearControlAll, \
    writeSYSRecreationModeAuto, \
    writeVOLImplementationInternal, /* Okay only for now. --Kurt */ \
    wroteSYSListeningModeStandard, \
    execSTDReady

#pragma DATA_SECTION(lm_standard_s0, ".none")
const ACP_Unit lm_standard_s0[] = {
    0xc900 + 0 - 1,
    LM_STANDARD_S,
};

const ACP_Unit lm_standard_s[] = {
    0xc900 + sizeof(lm_standard_s0)/2 - 1,
    LM_STANDARD_S,
};

#define LM_PURE_S \
    writeDECASPGearControlNil, \
    writeSYSRecreationModeDont,writeDECChannelConfigurationRequestUnknown, \
    writeVOLImplementationInactive, \
    wroteSYSListeningModePure, \
    execSTDReady

#pragma DATA_SECTION(lm_pure_s0, ".none")
const ACP_Unit lm_pure_s0[] = {
    0xc900 + 0 - 1,
    LM_PURE_S,
};

const ACP_Unit lm_pure_s[] = {
    0xc900 + sizeof(lm_pure_s0)/2 - 1,
    LM_PURE_S,
};

