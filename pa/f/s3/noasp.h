
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha Stream Processing Exclusion File
//
//
//

#ifndef NOASP_
#define NOASP_

// #define NOASP /* define to exclude all ASP processing */

// #define NODEM /* define to exclude Deemphasis */
// #define NOPL /* define to exclude Dolby Pro Logic */
// #define NONEO /* define to exclude DTS Neo:6 */
// #define NOBM /* define to exclude Bass Management */
// #define NODEL /* define to exclude Speaker Location Delay */
// #define NOML /* define to exclude MIPS Load */
// #define NOASS /* define to exclude Audio Stream Split */
// #define NOSRC /* define to exclude Sample Rate Conversion */
#define NODH /* define to exclude Dolby Headphone */
#define NODVS /* define to exclude Dolby Virtual Speaker */

#ifdef _PATE_
#define NOASP
#endif /* _PATE_ */

#ifdef NOASP
#define NODEM
#define NOPL
#define NONEO
#define NOBM
#define NODEL
#define NOML
#define NOASS
#define NOSRC
#define NODH
#define NODVS

//THX ASPS

#define NOBC
#define NOBGC
#define NOREQ
#define NOTM
#define NOAD
#define NOASA


#endif /* NOASP */

#endif /* NOASP_ */
