
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Algorithm RAM_REPORT definitions.
//
//
#include <stdlib.h> // malloc
#include <stdio.h>  // printf
#include <paf_alg_print.h>

Int
PAF_ALG_allocPrint (const PAF_ALG_AllocInit *pInit, Int sizeofInit, PAF_IALG_Config *p)
{
    IALG_MemRec *memTab;
    Int commonSize[PAF_IALG_COMMON_MEMN+1];
    Int commonCount[PAF_IALG_COMMON_MEMN+1];
    Int commonSpace[PAF_IALG_COMMON_MEMN+1];
    Int i,n;

    for ( ; pInit && pInit->ialg_fxns; 
        pInit=(const PAF_ALG_AllocInit *)((char *)pInit+sizeofInit)) {
#pragma UNROLL(1)
        for (i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i) {
            commonSize[i]=0;
            commonCount[i]=0;
            commonSpace[i]=-1;
        }
        n = pInit->ialg_fxns->algNumAlloc != NULL ? pInit->ialg_fxns->algNumAlloc() 
                                                  : IALG_DEFMEMRECS;
        if((memTab = (IALG_MemRec *)malloc(n * sizeof(IALG_MemRec)))){
            n=pInit->ialg_fxns->algAlloc(pInit->ialg_prms,(IALG_Fxns ** )&(pInit->ialg_fxns),memTab);
            if(n<=0)
                return PAF_ALGERR_MEMTAB_COUNT;

            for(i=0;i<n;++i){
                if((memTab[i].attrs >= PAF_IALG_COMMON_MEM0 && memTab[i].attrs <= PAF_IALG_COMMON_MEMN)) {
                    commonSize[memTab[i].attrs] += memTab[i].size + 
		        (commonCount[memTab[i].attrs] ? memTab[i].alignment : 0);
                    commonCount[memTab[i].attrs]++;
                }
                if (commonSpace[memTab[i].attrs] < memTab[i].space) {
                    commonSpace[memTab[i].attrs] = memTab[i].space;
                }
            }
            free(memTab);

            printf("\n0x%-3x\t",((PAF_ASP_LinkInit*)pInit)->thisCode.part.beta);
            for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i){
                if (commonSize[i] == 0)
                    printf("%9d\t",0);
                else 
                    printf("%6d(%1d)\t", 
                        commonSize[i], PAF_ALG_memSpace (p, (IALG_MemSpace)commonSpace[i]));
            }
            commonSize[IALG_SCRATCH] = commonSize[IALG_PERSIST] = commonSize[IALG_WRITEONCE] = 0;
        }
        else
            return 1;
    }
    return 0;
}

Void
PAF_ALG_commonPrint (IALG_MemRec common[], PAF_IALG_Config *p)
{

    Int i;

	printf("\n\n");
	for(i=0;i<130;i++)
		printf ("--");

    printf ("\nCOMMON                         \t\t\t\t");
    for (i=PAF_IALG_COMMON_MEM0; i <= PAF_IALG_COMMON_MEMN; i++) {
        if (i <= IALG_WRITEONCE)
            printf("");
        else if (common[i].size == 0)
            printf("%9d\t",0);
        else 
            printf("%6d(%1d)\t",
                common[i].size, PAF_ALG_memSpace (p, common[i].space));
    }
}


Void
PAF_ALG_memStatusPrint(Int internal,Int external,Int internal1)
{
    MEM_Stat statbuf;

    printf("\n\nSegment\tSize\tUsed\tAvailable");
    MEM_stat(internal,&statbuf);
    printf("\nIRAM\t%d\t%d\t%d",statbuf.size,statbuf.used,statbuf.length);
    MEM_stat(external,&statbuf);
    printf("\nSDRAM\t%d\t%d\t%d",statbuf.size,statbuf.used,statbuf.length);
    MEM_stat(internal1,&statbuf);
    printf("\nL3RAM\t%d\t%d\t%d\n",statbuf.size,statbuf.used,statbuf.length);
}

Void
PAF_ALG_bufMemPrint(Int z,Int size, Int heap,Int bufType)
{
      printf("\n\n%s Device Buffer (Zone %d) = %d (%d)", 
              (bufType==0 ? "Input" : bufType==1 ? "Output" : "Audio Frame"), 
              z, size,heap);
	  printf("\n");
}

Void
PAF_ALG_headerPrint()
{
    Int i;

    printf("\n\nAlgorithm Memory requirement:\n");
    printf("  Key:  \n");
    printf("    Beta ID can be decoded from stdbeta.h, oembeta.h or cusbeta.h in pa\\sio\\acp1.\n");
    printf("    The number in parentheses is the heap.  (1) is IRAM.  (2) is SDRAM.  (3) is L3RAM.\n");
    printf("%8s\t","BetaId");
    for(i=PAF_IALG_COMMON_MEM0;i<=PAF_IALG_COMMON_MEMN;++i){
        if(i==0) printf("%8s\t","SCRATCH");
        else if(i==1) printf("%8s\t","PERSIST");
        else if(i==2) printf("%8s \t","WRITONCE");
        else if(i<10)
			printf("%-7s%d  \t","COMMON",(i==0 ? i : i-IALG_WRITEONCE));
		else
        	printf("%6s%2d \t","COMMON",(i==0 ? i : i-IALG_WRITEONCE));
    }
}

