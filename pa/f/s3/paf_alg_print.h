
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Algorithm RAM_REPORT declarations.
//
//
//

#ifndef PAF_ALG_PRINT_
#define PAF_ALG_PRINT_

#include <std.h>
#include <mem.h>
#include <alg.h>
#include <paf_ialg.h>
#include <paf_alg.h>
#include <asp0.h>

#define PAF_ALG_ALLOCPRINT(X,Y) \
    PAF_ALG_allocPrint ((const PAF_ALG_AllocInit *)(X), sizeof (*(X)), (Y))

extern 
Int
PAF_ALG_allocPrint (
    const PAF_ALG_AllocInit *pInit,
    Int sizeofInit, 
    PAF_IALG_Config *p);

extern
Void
PAF_ALG_commonPrint (
    IALG_MemRec common[], 
    PAF_IALG_Config *p);

extern
Void
PAF_ALG_memStatusPrint (
    Int internal,
    Int external,
    Int internal1);

extern
Void
PAF_ALG_bufMemPrint (
    Int z,
    Int size, 
    Int heap,
    Int bufType);

extern
Void
PAF_ALG_headerPrint();

#endif /* PAF_ALG_PRINT_ */
