
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Algorithm Creation private API declarations.
//
//
//

#ifndef PAF_ALG_PRIV_
#define PAF_ALG_PRIV_

/*
 *  ======== PAF_ALG_alloc_ ========
 */

extern
Int
PAF_ALG_alloc_ (
    const PAF_ALG_AllocInit *pInit,
    Int sizeofInit,
    IALG_MemRec *common);

/*
 *  ======== PAF_ALG_create_ ========
 */

extern
ALG_Handle
PAF_ALG_create_ (
    const IALG_Fxns *fxns,
    IALG_Handle p,
    const IALG_Params *params,
    const IALG_MemRec *common,
    PAF_IALG_Config *pafConfig);

/*
 *  ======== PAF_ALG_init_ ========
 */

extern
Void 
PAF_ALG_init_ (
    IALG_MemRec *memTab,
    Int n,
    const IALG_MemSpace *commonSpace);

/*
 *  ======== PAF_ALG_allocMemory_ ========
 */

extern
Int
PAF_ALG_allocMemory_ (
    IALG_MemRec *memTab,
    Int n,
    const IALG_MemRec *common,
    PAF_IALG_Config *p);

/*
 *  ======== PAF_ALG_mallocMemory_ ========
 */

extern
Int
PAF_ALG_mallocMemory_ (
    IALG_MemRec *memTab,
    PAF_IALG_Config *p);

/*
 *  ======== PAF_ALG_memSpace_ ========
 */

extern
Int
PAF_ALG_memSpace_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space);

/*
 *  ======== PAF_ALG_setup_ ========
 */

extern
Void
PAF_ALG_setup_ (
    PAF_IALG_Config *p, 
    Int internalHeap, 
    Int externalHeap,
    Int internalHeap1,
    Int clr);

#endif /* PAF_ALG_PRIV_ */
