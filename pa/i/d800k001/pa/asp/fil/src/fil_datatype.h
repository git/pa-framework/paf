
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== fil.h ========
 *  This header defines all inner FIL types, structures etc. 
 */
 
#ifndef FIL_DATATYPE_
#define FIL_DATATYPE_

#ifndef Int
#define Int    int
#endif

#ifndef Uint
#define Uint   unsigned int
#endif

#ifndef Char
#define Char   char
#endif

#ifndef Uchar
#define Uchar  unsigned char
#endif

#ifndef Float
#define Float  float
#endif

#ifndef Double
#define Double double
#endif

#ifndef Short
#define Short  short
#endif

#ifndef Void
#define Void   void
#endif

#ifndef Const
#define Const  const
#endif

typedef int FIL_int;
typedef unsigned int FIL_uint;
typedef char FIL_char;
typedef unsigned char FIL_uchar;

#define FIL_CONST    const
#define FIL_RESTRICT restrict

#define FIL_PREC_DP  double 
#define FIL_PREC_FIX int
#define FIL_PREC_SP  float

/* Basic FIL coefficient structure's header structure */
typedef struct PAF_FilCoef {
    Uint type;
    Uint sampRate;
} PAF_FilCoef;

/* FIL layer-2 interface structure */
typedef struct PAF_FilParam {
    Void  ** pIn  ;
    Void  ** pOut ;
    Void  ** pCoef;
    Void  ** pVar ;
    Int      sampleCount;
    Uchar    channels;
    Uint     use;
}  PAF_FilParam;

#endif /* FIL_DATATYPE_ */
