*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  ======== iir2tap2ch_ui_mx_BM2.asm ========
*  IIR filter implementation for tap-2 & channels-2, unicoeff & inplace,SP-DP,
*  and b0=1.0, b1=-2.0, b2=1.0 (made for BM).
*

************************ IIR FILTER *****************************************
****************** ORDER 2    and CHANNELS 2 ********************************
*                                                                           *
* Filter equation  : H(z) =  1 - 2*z~1 + z~2                                *
*                           ----------------------                          *
*                            1  - a1*z~1 - a2*z~2                           *
*                                                                           *
*   Direct form    : y(n) = x(n)-2*x(n-1)+x(n-2)+a1*y(n-1)+a2*y(n-2)        *
*                                                                           *
*   Implementation : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    *
*                    y(n) = w(n) - 2*w(n-1) + w(n-2)                        *
*                                                                           *
*   Filter variables :                                                      *
*      y(n) - *y,         x(n)   - *x                                       *
*      w(n) -             w(n-1) - w1         w(n-2) - w2                   *
*      c0=b0 - filtCfsB[0], c1=b1/b0 - filtCfsB[1], c2=b2/b0 - filtCfsB[2]  *
*      c3=a1 - filtCfsA[0], c4=a2 - filtCfsA[1]                             *
*                                                                           *
*                                                                           *
*           1                  w(n)                                         *
*     x(n)-->>--[+]---->--------@------->-----[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a1    +-----+    -2    |                             *
*               [+]----<<----| Z~1 |---->>----[+]                           *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a2    +-----+     1    |                             *
*                 ----<<-----| Z~1 |---->>-----                             *
*                             +-----+                                       *
*                                                                           *
*****************************************************************************
*Note:C equivalent of s-asm code lines are given as comments,for simplicity *

	.global _Filter_iirT2Ch2_ui_mx_BM2 ;Declared as Global function
    .sect	".text:Filter_iirT2Ch2_ui_mx_BM2" ;Function memory section

	.sect	".text:Filter_iirT2Ch2_ui_mx_BM2:_Filter_iirT2Ch2_ui_mx_BM2"
	.clink

_Filter_iirT2Ch2_ui_mx_BM2:
        
        STW         .D2     B3,             *B15--[01]          ;Pushing returnaddress into Stack
 ||     MV          .S1     A4,             A5 ; Make a copy of A4
                                                                                
        STW         .D2     A15,            *B15--[01]          ;Pushing A15 into Stack
 ||     MVC         .S2     CSR,            B7  

        LDW         .D1T1   *A5,            A9 ; pParam->pIn        
         
        STW         .D2     B7,             *B15--[01]          ;Store CSR
 ||     AND         .S2     -2,             B7,         B7      ;Disable Interrupts

        LDW         .D1T2   *+A5(12),       B5 ; pParam->pVar

                
        SUB         .S1X    B15,            04,         A15     ;Setting A15<- SP-1,Twin SP's
 ||     MVC         .S2     B7,             CSR                                                              
 ||     LDW         .D1T1   *+A5(8),        A0 ; pParam->pCoef
 
; Get all pointers into A4,B4,A6,B6,A8,B8
        LDW         .D1T2   *+A5(16),       B8 ; pParam->sampleCount
        
        NOP         

        LDW         .D1T1   *A9,            A4 ; pParam->pIn[0]
        
        LDW         .D1T2   *A9(4),         B4 ; pParam->pIn[1]       
        
        LDW         .D2T2   *B5,            B6 ; pParam->pVar[0]
         
        LDW         .D2T1   *B5(4),         A8 ; pParam->pVar[1]
        
        LDW         .D1T1   *A0,            A6 ; pParam->pCoef[0]
        
        NOP         4
        
;//////////////////////////////////////////////        
        
        STW         .D2     B14,            *B15--[02]          ;Pushing B14 and A14 into stack
 ||     STW         .D1     A14,            *A15--[02]           
 ||     MV          .S1     A4,             A14                 ;A14 = *inL
 ||     MV          .S2     B4,             B14                 ;B14 = *inR                                                                  
        
        STW         .D2     B13,            *B15--[02]          ;Pushing B13 and A13 into stack
 ||     STW         .D1     A13,            *A15--[02]          
 ||     MV          .S1X    B6,             A13                 ;A13 = *varL
 ||     MV          .S2X    A8,             B13                 ;B14 = *varR                                                                  
                                                                     
        LDDW        .D1     *+A13[1],       A9:A8               ;A9:A8 =w2L
 ||     LDDW        .D2     *+B13[1],       B9:B8               ;B9:B8 =w2R
 ||     MV          .S2     B8,             B0                  ;B0 = count
 ||     MV          .L2X    A6,             B6                  ;B6 = *coef                                                               
 
        LDDW        .D1     *+A6[3],        A5:A4               ;A5:A4 = a1 
 ||     LDDW        .D2     *+B6[4],        B5:B4               ;B5:B4 = a2
 
        LDDW        .D1     *+A13[0],       A7:A6               ;A7:A6 =w1L
 ||     LDDW        .D2     *+B13[0],       B7:B6               ;B7:B6 =w1R
            
        STW         .D2     B12,            *B15--[02]          ;Pushing B12 and A12 into stack
 ||     STW         .D1     A12,            *A15--[02]              
          
        STW         .D2     B11,            *B15--[02]          ;Pushing B11 and A11 into stack
 ||     STW         .D1     A11,            *A15--[02]  
                                                                    
        STW         .D2     B10,            *B15--[02]          ;Pushing B10 and A10 into stack
 ||     STW         .D1     A10,            *A15--[02]  
 
        MV          .S1X    B15,            A0                  ;Store the Stack Pointer
 ||     MV          .L1     A13,            A15                 ;*varL      
 ||     MV          .L2     B13,            B15                 ;*varR              
                    
;------------------------------------------------------------------------------;
;----------------------------BEGIN OF PROLOG-----------------------------------;
;------------------------------------------------------------------------------;

        LDW         .D1     *A14++,     A1                      ;A1 = inL[i]
 ||     LDW         .D2     *B14++,     B1                      ;B1 = inR[i]
 ||     MPYDP       .M1X    A9:A8,      B5:B4,      A11:A10     ;a2*w2L
 ||     MPYDP       .M2     B9:B8,      B5:B4,      B11:B10     ;a2*w2R
 
        NOP 4
        
        SPDP        .S1     A1,         A3:A2                   ;A3:A2 =(double)inL[i]
 ||     SPDP        .S2     B1,         B3:B2                   ;B3:B2 =(double)inR[i]
 
        MPYDP       .M1     A7:A6,      A5:A4,      A11:A10     ;a1*w1L
 ||     MPYDP       .M2X    B7:B6,      A5:A4,      B11:B10     ;a1*w1R
 ||     ADDDP       .L1     A7:A6,      A7:A6,      A13:A12     ;w1L+w1L
 ||     ADDDP       .L2     B7:B6,      B7:B6,      B13:B12     ;w1R+w1R
 
        NOP 2
        
        ADDDP       .L1     A3:A2,      A11:A10,    A3:A2       ;inL[i]+a2*w2L
 ||     ADDDP       .L2     B3:B2,      B11:B10,    B3:B2       ;inR[i]+a2*w2R
        
        NOP 3
                
        SUBDP       .L1     A13:A12,    A9:A8,      A13:A12     ;w1L+w1L-w2L
 ||     SUBDP       .L2     B13:B12,    B9:B8,      B13:B12     ;w1R+w1R-w2R
 ||     MV          .S1     A6,         A8                      ;w2L=w1L
 ||     MV          .S2     B6,         B8                      ;w2R=w1R

        MV          .S1     A7,         A9                      ;w2L=w1L
 ||     MV          .S2     B7,         B9                      ;w2R=w1R

;------------------------------------------------------------------------------;

        LDW         .D1     *A14++,     A1                      ;@A1 = inL[i]
 ||     LDW         .D2     *B14++,     B1                      ;@B1 = inR[i]
 ||     MPYDP       .M1X    A9:A8,      B5:B4,      A11:A10     ;@a2*w2L
 ||     MPYDP       .M2     B9:B8,      B5:B4,      B11:B10     ;@a2*w2R
 ||     ADDDP       .L1     A3:A2,      A11:A10,    A7:A6       ;w1L=wL=inL[i]+a2*w2L+a1*w1L
 ||     ADDDP       .L2     B3:B2,      B11:B10,    B7:B6       ;w1R=wR=inR[i]+a2*w2R+a1*w1R

        NOP 4
        
        SPDP        .S1     A1,         A3:A2                   ;@A3:A2 =(double)inL[i]
 ||     SPDP        .S2     B1,         B3:B2                   ;@B3:B2 =(double)inR[i]
 
        MPYDP       .M1     A7:A6,      A5:A4,      A11:A10     ;@a1*w1L
 ||     MPYDP       .M2X    B7:B6,      A5:A4,      B11:B10     ;@a1*w1R
 ||     ADDDP       .L1     A7:A6,      A7:A6,      A13:A12     ;@w1L+w1L
 ||     ADDDP       .L2     B7:B6,      B7:B6,      B13:B12     ;@w1R+w1R
 
        NOP 1
    
   [B0] SUB         .S2     B0,         2,          B0          ;count = count -1 
        
;*******************************************************************************;
;************************BEGIN OF PIPELOOPED KERNEL*****************************;
;*******************************************************************************;       
LOOP:       
    
        ADDDP       .L1     A3:A2,      A11:A10,    A3:A2       ;@inL[i]+a2*w2L
 ||     ADDDP       .L2     B3:B2,      B11:B10,    B3:B2       ;@inR[i]+a2*w2R

        NOP 1
        
        SUBDP       .L1     A7:A6,      A13:A12,    A3:A2       ;wL-(w1L+w1L-w2L)
 ||     SUBDP       .L2     B7:B6,      B13:B12,    B3:B2       ;wR-(w1R+w1R-w2R)
 
        NOP 1
        
        SUBDP       .L1     A13:A12,    A9:A8,      A13:A12     ;@w1L+w1L-w2L
 ||     SUBDP       .L2     B13:B12,    B9:B8,      B13:B12     ;@w1R+w1R-w2R
 ||     MV          .S1     A6,         A8                      ;@w2L=w1L
 ||     MV          .S2     B6,         B8                      ;@w2R=w1R

        MV          .S1     A7,         A9                      ;@w2L=w1L
 ||     MV          .S2     B7,         B9                      ;@w2R=w1R

;-----------------------------------------------------------------------------;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
        LDW         .D1     *A14++,     A1                      ;@@A1 = inL[i]
 ||     LDW         .D2     *B14++,     B1                      ;@@B1 = inR[i]
 ||     MPYDP       .M1X    A9:A8,      B5:B4,      A11:A10     ;@@a2*w2L
 ||     MPYDP       .M2     B9:B8,      B5:B4,      B11:B10     ;@@a2*w2R
 ||     ADDDP       .L1     A3:A2,      A11:A10,    A7:A6       ;@w1L=wL=inL[i]+a2*w2L+a1*w1L
 ||     ADDDP       .L2     B3:B2,      B11:B10,    B7:B6       ;@w1R=wR=inR[i]+a2*w2R+a1*w1R

        NOP 2
        
    [B0]B           .S2     LOOP 
        
        DPSP        .L1     A3:A2,      A1                      ;inL[i] =(float)inL[i]
 ||     DPSP        .L2     B3:B2,      B1                      ;inR[i] =(float)inR[i]

        SPDP        .S1     A1,         A3:A2                   ;@@A3:A2 =(double)inL[i]
 ||     SPDP        .S2     B1,         B3:B2                   ;@@B3:B2 =(double)inR[i]
 
        MPYDP       .M1     A7:A6,      A5:A4,      A11:A10     ;@@a1*w1L
 ||     MPYDP       .M2X    B7:B6,      A5:A4,      B11:B10     ;@@a1*w1R
 ||     ADDDP       .L1     A7:A6,      A7:A6,      A13:A12     ;@@w1L+w1L
 ||     ADDDP       .L2     B7:B6,      B7:B6,      B13:B12     ;@@w1R+w1R
 
        NOP 1
        
        STW         .D1     A1,         *-A14[3]                ;store inL[i]   
 ||     STW         .D2     B1,         *-B14[3]                ;store inR[i]   
 ||[B0] SUB         .S2     B0,         1,          B0          ;@count = count -1                              
;*******************************************************************************;
;**************************END OF PIPELOOPED KERNEL*****************************;
;*******************************************************************************;       

        STW         .D1     A6,         *+A15[0]                ;store w1L
 ||     STW         .D2     B6,         *+B15[0]                ;store w1R
 
        STW         .D1     A7,         *+A15[1]                ;store w1L
 ||     STW         .D2     B7,         *+B15[1]                ;store w1R
         
        SUBDP       .L1     A7:A6,      A13:A12,    A3:A2       ;e-wL-(w1L+w1L-w2L)
 ||     SUBDP       .L2     B7:B6,      B13:B12,    B3:B2       ;e-wR-(w1R+w1R-w2R)
 ||     STW         .D1     A8,         *+A15[2]                ;store w2L
 ||     STW         .D2     B8,         *+B15[2]                ;store w2R
 
        STW         .D1     A9,         *+A15[3]                ;store w2L
 ||     STW         .D2     B9,         *+B15[3]                ;store w2R
  
        MV          .S2X    A0,         B15                     ;Restore Stack Pointer      
        
        SUB         .S1     B15,         4,          A15        ;Twin SP        
 ||     LDW         .D2     *+B15[11],   B7                     ;Restore CSR
        
        LDW         .D2     *++B15[02],  B10             ;Poping B10 and A10 outof Stack
 ||     LDW         .D1     *++A15[02],  A10             
 
        LDW         .D2     *+B15[11],   B3              ;Restoring Return address
        
        LDW         .D2     *++B15[02],  B11             ;Poping B11 and A11 outof Stack
 ||     LDW         .D1     *++A15[02],  A11 
        
        LDW         .D2     *++B15[02],  B12             ;Poping B12 and A12 outof Stack
 ||     LDW         .D1     *++A15[02],  A12             
        
        LDW         .D2     *++B15[02],  B13             ;Poping B13 and A13 outof Stack
 ||     LDW         .D1     *++A15[02],  A13 
 ||     MVC         .S2     B7,          CSR             ;Enable Interrupts                 
        
        DPSP        .L1     A3:A2,       A1              ;e-inL[i]  =(float)inL[i]
 ||     DPSP        .L2     B3:B2,       B1              ;e-inR[i]  =(float)inR[i]
 ||     LDW         .D2     *++B15[02],  B14             ;Poping B14 and A14 outof Stack
 ||     LDW         .D1     *++A15[02],  A14        
        
        B           B3;                                  ;Branch Back to Called Funtion 
 ||     MV          .L1     A14,         A4
 ||     MV          .L2     B14,         B4
        
        LDW         .D2     *++B15[02],  A15             ;Poping A15 out of Stack
        
        ADDAW       .D2     B15,         01,         B15 ;Clearing the Stack
        
        STW         .D1     A1,         *-A4[2]          ;e-store inL[i]    
 ||     STW         .D2     B1,         *-B4[2]          ;e-store inR[i]    
        
        ZERO        .S1     A4
        
        NOP         
;------------------------------------------------------------------------------;
;------------------END OF EPILOG and BRANCH TO MAIN----------------------------;
;------------------------------------------------------------------------------;
                .end
        
        