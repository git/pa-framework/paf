/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IPCM default instance creation parameters
 */
#include <std.h>

#include <ipcm.h>

/*
 *  ======== IPCM_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCM objects.
 */
const IPCM_Status IPCM_PARAMS_STATUS = {
    sizeof(IPCM_Status),
    1,
#ifndef _PATE_
    1,
#else /* _PATE_ */
    0,
#endif /* _PATE_ */
    0,
    2*10,
    0,0,0,0,
    -2*3,
    -2*3, 
    0,
    0,0,0,
	0,0,0,0,
	{ PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, PAF_CC_AUX_STEREO_UNKNOWN, 0,0,0,0,0 },
};
const IPCM_Config IPCM_PARAMS_CONFIG = {
    sizeof(IPCM_Config),
#ifndef _PATE_
    8,
    4096,
#else /* _PATE_ */
    128,  /* minimumFrameLength */
    256,  /* maximumFrameLength */
#endif /* _PATE_ */
    NULL, // &CPL_TII_ICPL,
};
const IPCM_Params IPCM_PARAMS = {
    sizeof(IPCM_Params),
    &IPCM_PARAMS_STATUS,
    &IPCM_PARAMS_CONFIG,
    NULL,
    NULL,
};

