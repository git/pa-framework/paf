/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  PCM Module IALG implementation - MDS's implementation of the
 *  IALG interface for the PCM algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include <ipcm.h>
#include <pcm_mds.h>
#include <pcm_mds_priv.h>

/*
 *  ======== PCM_MDS_alloc ========
 */
Int PCM_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IPCM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IPCM_PARAMS;  /* set default parameters */
    }

    /* Request memory for PCM object */
    memTab[0].size = (sizeof(PCM_MDS_Obj)+3)/4*4 + (sizeof(PCM_MDS_Status)+3)/4*4 + (sizeof(PCM_MDS_Config)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    memTab[1].size = (sizeof(PCM_MDS_Active)+3)/4*4;
    memTab[1].alignment = 4;
    memTab[1].space = IALG_SARAM;
    memTab[1].attrs = IALG_PERSIST;
    
    memTab[2].size = (sizeof(PCM_MDS_Scrach)+3)/4*4;
    memTab[2].alignment = 4;
    memTab[2].space = IALG_SARAM;
    memTab[2].attrs = IALG_SCRATCH;

    return (3);
}

/*
 *  ======== PCM_MDS_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== PCM_MDS_initObj ========
 */
Int PCM_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    PCM_MDS_Obj *pcm = (Void *)handle;
    const IPCM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IPCM_PARAMS;  /* set default parameters */
    }

    pcm->pStatus = (PCM_MDS_Status *)((char *)pcm + (sizeof(PCM_MDS_Obj)+3)/4*4);
    pcm->pConfig = (PCM_MDS_Config *)((char *)pcm + (sizeof(PCM_MDS_Obj)+3)/4*4 + (sizeof(PCM_MDS_Status)+3)/4*4);
    pcm->pActive = (PCM_MDS_Active *)memTab[1].base;
    pcm->pScrach = (PCM_MDS_Scrach *)memTab[2].base;

    if (params->pStatus)
        *pcm->pStatus = *params->pStatus;
    else
        pcm->pStatus->size = sizeof(pcm->pStatus);

    if (params->pConfig)
        *pcm->pConfig = *params->pConfig;
    else
        pcm->pConfig->size = sizeof(pcm->pConfig);

    if (params->pActive)
        *pcm->pActive = *params->pActive;
    else
        pcm->pActive->size = sizeof(pcm->pActive);

    if (params->pScrach)
        *pcm->pScrach = *params->pScrach;
    else
        pcm->pScrach->size = sizeof(*pcm->pScrach);


    return (IALG_EOK);
}

