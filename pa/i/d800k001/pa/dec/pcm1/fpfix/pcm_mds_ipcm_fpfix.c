/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  PCM Module implementation - MDS implementation of a PCM algorithm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>
#include <math.h>
#include <mathf.h>

#include <ipcm.h>
#include <pcm_mds.h>
#include <pcm_mds_priv.h>
#include <pcmerr.h>

#include <inpbuf.h>
#include <paftyp.h>

#include <cpl.h>
#include "pcm_mds_ipcm.h"

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

#if PAF_AUDIODATATYPE_FIXED
#warn fixed-point audio data type not supported
#endif /* PAF_AUDIODATATYPE_FIXED */

// Local symbol definitions

#if PAF_AUDIODATATYPE_FIXED
#define ZERO 0
#else
#define ZERO 0.
#endif

/*
 *  ======== PCM_MDS_decode ========
 *  MDS's implementation of the decode operation.
 */

Int
PCM_MDS_decode(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    PAF_ChannelMask programMask = pcm->pActive->programMask;
    PAF_ChannelMask programWarp = pcm->pActive->programWarp;
    PAF_ChannelMask decodeMask = programMask;

    Int sampleCount = pDecodeInStruct->sampleCount;

    Int i;

    Int errno;

    //
    // Check parameters.
    //

    /* Check sample count against Decode Algorithm */
    if (sampleCount < pcm->pConfig->minimumFrameLength)
        return PCMERR_DECODE_PARAM;
    else if (sampleCount > pcm->pConfig->maximumFrameLength)
        return PCMERR_DECODE_PARAM;

    /* Check warp according to sample count */
    if (programMask && ! programWarp)
        return PCMERR_DECODE_INPUT;
    else if (sampleCount <= pAudioFrame->data.nSamples)
        /* okay */;
    else if (sampleCount <= 2 * pAudioFrame->data.nSamples) {
        if ((programWarp & (programWarp >> 1)) != 0
            || (programWarp & (1 << pAudioFrame->data.nChannels-1)) != 0)
            return PCMERR_DECODE_INPUT;
    }
    else if (sampleCount <= 4 * pAudioFrame->data.nSamples) {
        if ((programWarp & (programWarp >> 1)) != 0
            || (programWarp & (programWarp >> 2)) != 0
            || (programWarp & (programWarp >> 3)) != 0
            || (programWarp & (7 << pAudioFrame->data.nChannels-3)) != 0)
            return PCMERR_DECODE_INPUT;
    }
    else
        return PCMERR_DECODE_PARAM;

#if 0 /* Not used, for now. --Kurt */
    /* Check sample count against Input Buffer */
    if (pBufferSegment->wordCount < 2 * sampleCount)
        return PCMERR_DECODE_INPUT;
#endif

    // Implement decoding phases (nominal):
    // 1. Input
    // 2. Null
    // 3. Ramp Art
    // 4. Downmix

    for (i=0; i < lengthof (handle->fxns->phase); i++) {
        if (handle->fxns->phase[i]
            && (errno = handle->fxns->phase[i](handle, pDecodeInStruct, pDecodeOutStruct, decodeMask)))
            return errno;
    }

    //
    // Set output flags and exit.
    //

    pDecodeOutStruct->outputFlag = 1;
    pDecodeOutStruct->errorFlag = 0;
    pDecodeOutStruct->pAudioFrame = pAudioFrame;
    CPL_setAudioFrame((IALG_Handle)handle, pAudioFrame, sampleCount,pcm->pActive->pDecodeStatus);

    return (0);
}

/*
 *  ======== PCM_MDS_info ========
 *  MDS's implementation of the info operation.
 */

Int
PCM_MDS_info(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeControl *pDecodeControl, PAF_DecodeStatus *pDecodeStatus)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeControl->pAudioFrame;

    Int frameLength = pDecodeControl->frameLength;

    PAF_ChannelConfiguration request
        = pDecodeStatus->channelConfigurationRequest;
    PAF_ChannelConfiguration program
        = pDecodeStatus->channelConfigurationProgram;

    PAF_ChannelConfiguration decode;

    CPL_CDM *PCM_CDMConfig = &(pcm->pScrach->PCM_CDMConfig);

    decode = program;
    if (pcm->pStatus->channelConfigurationProgram.part.aux == PAF_CC_AUX_STEREO_DUAL)
        decode.part.aux = PAF_CC_AUX_STEREO_DUAL;
     // INCONSISTENT USE/REPORT, NOT LIKE AC3, FOR NOW --Jayant, see below the program format

    PCM_CDMConfig->LfeDmxInclude = 0;
    if(pcm->pStatus->LFEDownmixInclude) {
        if (decode.part.sub == PAF_CC_SUB_ONE && request.part.sub == PAF_CC_SUB_ZERO)
            PCM_CDMConfig->LfeDmxInclude = 1;
    }
    PCM_CDMConfig->channelConfigurationFrom = decode;
    PCM_CDMConfig->channelConfigurationRequest = request;
    PCM_CDMConfig->sourceDual = pDecodeStatus->sourceDual;

    CPL_CALL(cdm_downmixConfig)(NULL,PCM_CDMConfig);

    pDecodeStatus->channelConfigurationDecode = decode;
    // Do not perform downmix if decBypass is set
    if (pDecodeStatus->decBypass)
        pDecodeStatus->channelConfigurationDownmix = decode;
    else
        pDecodeStatus->channelConfigurationDownmix = PCM_CDMConfig->channelConfigurationTo;

    pDecodeStatus->frameLength = frameLength;

    // Update sample rate and pre-emphasis status as per control.
    pDecodeStatus->sampleRate = pDecodeControl->sampleRate;
    pDecodeStatus->emphasis = pDecodeControl->emphasis;

    pDecodeStatus->programFormat =
        pAudioFrame->fxns->programFormat (pAudioFrame,
            program,
            pDecodeStatus->sourceDual);
            // INCONSISTENT USE/REPORT, NOT LIKE AC3, FOR NOW --Kurt

    pcm->pActive->pDecodeStatus = pDecodeStatus;
    pcm->pActive->pInpBufConfig = pDecodeControl->pInpBufConfig;

    CPL_setAudioFrame((IALG_Handle)handle, pAudioFrame, frameLength,pcm->pActive->pDecodeStatus);

    return ((Int)0);
}

/*
 *  ======== PCM_MDS_reset ========
 *  MDS's implementation of the reset operation.
 */

static inline PAF_ChannelMask
PCM_MDS_warpMask (PAF_AudioFrame *pAudioFrame, PAF_ChannelMask x)
{
    PAF_AudioData **sample = pAudioFrame->data.sample;

    PAF_AudioData *base = NULL;

    Int i;
    Int n, a, b;

    PAF_ChannelMask y = 0;

    /* Warp is ordered, contiguous allocation of audio frame channel buffers */
    /* Warp mask is a bit-mask version of same, 0 for no warp or a bad warp  */

    n = pAudioFrame->data.nSamples;
    for (i=0; i < pAudioFrame->data.nChannels; i++) {
        if ((1 << i) & x) {
            if (! sample[i])
                return 0; /* (x && ! y) indicates bad warp */
            if (! base)
                base = sample[i];
            a = sample[i] - base;
            b = a / n;
            if (a == b * n && 0 <= b && b < 8 * sizeof (PAF_ChannelMask)) {
                y |= (1 << b);
            }
        }
    }

    return y;
}

Int
PCM_MDS_reset(IPCM_Handle handle, ALG_Handle sioHandle, PAF_DecodeControl *pDecodeControl, PAF_DecodeStatus *pDecodeStatus)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame =
        pDecodeControl->pAudioFrame;

    PAF_ChannelConfiguration program =
        pcm->pStatus->channelConfigurationProgram;

    PAF_ChannelMask programMask =
        pAudioFrame->fxns->channelMask (pAudioFrame, program);

    PAF_ChannelMask programWarp =
        PCM_MDS_warpMask (pAudioFrame, programMask);

    pDecodeStatus->channelConfigurationProgram = program;

    pcm->pActive->rampState = 0;

    pcm->pActive->programMask = programMask;
    pcm->pActive->programWarp = programWarp;

    return 0;
}

/*
 *  ======== PCM_MDS_input ========
 *  MDS's implementation of the input operation.
 */

Int
PCM_MDS_input(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    Int sampleCount = pDecodeInStruct->sampleCount;
    Int nChannels = pAudioFrame->data.nChannels;

    PAF_AudioSize * restrict samsiz = pAudioFrame->data.samsiz;

    Int scaleVolume = pcm->pStatus->scaleVolume;
    PAF_AudioData scale =
        scaleVolume ? pAudioFrame->fxns->dB2ToLinear (scaleVolume) : 1.0;

    Int i;

    Int errno;

    //
    // Load audio data from Input Buffer according to channel map.
    //

    if (errno = handle->fxns->inputCheck(handle, sampleCount))
        return errno;

    for (i=0; i < nChannels; i++) {
        Int from = pcm->pActive->pDecodeStatus->channelMap.from[i];
        Int to = pcm->pActive->pDecodeStatus->channelMap.to[i];
        if (to >= 0 && (decodeMask & (1 << to)) != 0) {
            if (from < -1)
                ;
            else if (from == -1)
                handle->fxns->inputAudio(handle, pAudioFrame->data.sample[to],
                    ZERO, from, sampleCount);
            else
                handle->fxns->inputAudio(handle, pAudioFrame->data.sample[to],
                    scale, from, sampleCount);
        }
        samsiz[i] = 0;
    }

    return 0;
}


/*
 *  ======== PCM_MDS_rampart ========
 *  MDS's implementation of the rampart operation.
 */

Int
PCM_MDS_rampart(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;
    PAF_DecodeStatus *pDecodeStatus = pcm->pActive->pDecodeStatus;

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    PAF_ChannelMask programMask = pcm->pActive->programMask;

    Int iChannel, iSample;

    Int nChannels = pAudioFrame->data.nChannels;
    Int sampleCount = pDecodeInStruct->sampleCount;

    PAF_AudioData ** restrict sample = pAudioFrame->data.sample;

    PAF_AudioData scinc = 1. / sampleCount;
    PAF_AudioData scale;

    // Do not perform ramp if decBypass is set
    if (pDecodeStatus->decBypass)
        return 0;

    if (pcm->pStatus->ramp) {
        switch (pcm->pActive->rampState) {
          case 0:
            for (iChannel=0; iChannel < nChannels; iChannel++) {
                if (programMask & (1 << iChannel)) {
                    scale = ZERO;
                    for (iSample=0; iSample < sampleCount; iSample++) {
                        sample[iChannel][iSample] *= scale;
                        scale += scinc;
                    }
                }
            }
            pcm->pActive->rampState = 1;
            break;
          case 1:
            break;
        }
    }

    return 0;
}


Int
PCM_MDS_downmix(IPCM_Handle handle, PAF_DecodeInStruct *pDecodeInStruct, PAF_DecodeOutStruct *pDecodeOutStruct, PAF_ChannelMask decodeMask)
{
    PCM_MDS_Obj *pcm = (Void *)handle;

    CPL_CDM *PCM_CDMConfig = &(pcm->pScrach->PCM_CDMConfig);

    PAF_AudioFrame *pAudioFrame = pDecodeInStruct->pAudioFrame;

    Int sampleCount = pDecodeInStruct->sampleCount;

    PAF_ChannelConfiguration program
        = pcm->pActive->pDecodeStatus->channelConfigurationProgram;
    PAF_ChannelConfiguration request
        = pcm->pActive->pDecodeStatus->channelConfigurationRequest;
    PAF_ChannelConfiguration decode =
        pcm->pActive->pDecodeStatus->channelConfigurationDecode;
    PAF_ChannelConfiguration downmix =
        pcm->pActive->pDecodeStatus->channelConfigurationDownmix;

    if (decode.legacy != downmix.legacy) {
        PCM_CDMConfig->channelConfigurationFrom = decode;
        PCM_CDMConfig->channelConfigurationTo = downmix;
        PCM_CDMConfig->sourceDual = pcm->pActive->pDecodeStatus->sourceDual;
        PCM_CDMConfig->clev = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->CntrMixLev);
        PCM_CDMConfig->slev = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->SurrMixLev);
        PCM_CDMConfig->LFEDmxVolume = pAudioFrame->fxns->dB2ToLinear(pcm->pStatus->LFEDownmixVolume);

        CPL_CALL(cdm_downmixSetUp)(NULL,pAudioFrame,PCM_CDMConfig);
        CPL_CALL(cdm_samsiz)(NULL,pAudioFrame,PCM_CDMConfig);
        CPL_CALL(cdm_downmixApply)(NULL,pAudioFrame->data.sample,pAudioFrame->data.sample,PCM_CDMConfig,sampleCount);
    }

    return 0;
}
