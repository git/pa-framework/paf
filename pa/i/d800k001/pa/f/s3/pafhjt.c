/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#define PAFHJT_DISABLE_INDIRECTION
#include "pafhjt.h"
#define EDMA_DAT
// no FFT yet for Davinci
#if ((PAF_DEVICE&0xFF000000) == 0xDD000000)
void CPL_fft_(int n, float * ptr_x, const float * ptr_w, float * ptr_y, 
               const unsigned char * brev, int n_min, int offset, int n_max)
{
}

#endif //((PAF_DEVICE&0xFF000000) == 0xDD000000)

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000) || ((PAF_DEVICE&0xFF000000) == 0xDD000000)
#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
/* Include the legacy csl 2.0 dat header */
#include <csl_dat.h>

/* Include EDMA3 low level driver specific implementation APIs for dat */
//#include <csl2_dat_edma3lld.h>
/**
 * EDMA3 Driver Handle, which is used to call all the Driver APIs.
 * It gets initialized during EDMA3 Initialization.
 */
//extern EDMA3_DRV_Handle hEdma;
//extern EDMA3_DRV_Handle DAT_EDMA3LLD_hEdma;
#endif

#if !defined(ROM_BUILD)

Uint32 DAT_copy_dummy (void *src, void *dst, Uint16 cnt)
{
#ifdef EDMA_DAT
#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
	return edma_DAT_copy( src,dst, cnt);
#else
	return (Uint32) memcpy (dst, src, cnt);
#endif	
#else
	return (Uint32) memcpy (dst, src, cnt);
#endif	
}

void DAT_wait_dummy (Uint32 eventid)
{
#ifdef EDMA_DAT
#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
	edma_DAT_wait(eventid);
#endif
#endif
}

#endif /* ROM_BUILD */

#if defined(ROM_BUILD)
#pragma DATA_SECTION(PAFHJT, ".const:PAFHJT");
const PAFHJT_t PAFHJT = {
#else /* ROM_BUILD */
const PAFHJT_t PAFHJT_RAM = {
#endif /* ROM_BUILD */

	CPL_setAudioFrame__patch,
	CPL_fft_,
	CPL_Modulation_new_,
	CPL_Demodulation_new_,
	CPL_window_aac_ac3_,

	// COM ASP
	
	COM_TII_snatchCommon_,
	COM_TII_activateCommon_,
	COM_TII_deactivateCommon_,
	
	// COM DEC

	CPL_cdm_downmixSetUp__patch,
	CPL_cdm_samsiz__patch,
	CPL_cdm_downmixApply__patch,
	CPL_cdm_downmixConfig__patch,
	CDMTo_Mono__patch,
	CDMTo_Stereo__patch,
	CDMTo_Phantom1__patch,
	CDMTo_Phantom2__patch,
	CDMTo_Phantom3__patch,
	CDMTo_Phantom4__patch,
	CDMTo_3Stereo__patch,
	CDMTo_Surround1__patch,
	CDMTo_Surround2__patch,
	CDMTo_Surround3__patch,
	CDMTo_Surround4__patch,

	CPL_vecAdd_,
	CPL_vecLinear2_,
	CPL_vecScale_,
	CPL_vecSet_,
	CPL_ivecCopy_,
	CPL_svecSet_,
	CPL_ivecSet_,
	CPL_imaskScale_,
	CPL_smaskScale_,
	CPL_vecStrideScale_,

	CPL_vecLinear2Incr_,
	CPL_vecLinear2Common_,
	CPL_vecScaleAcc_,
	CPL_vecScaleIncr_,
	CPL_vecSetArr_,
	CPL_GetPAFSampleRate_,
	CPL_GetNumSat_,
	CPL_intDelayProc_,
	CPL_floatDelayProc_,
	CPL_DelaySet_,
	CPL_DelayInit_,
	CPL_sumDiff_,
	CPL_delay__patch,

	CPL_fifoInit_,
	CPL_fifoSize_,
	CPL_fifoSizeFull_,
	CPL_fifoSizeFree_,
	CPL_fifoSizeFullLinear_,
	CPL_fifoSizeFreeLinear_,
	CPL_fifoRead_,
	CPL_fifoWrite_,
	CPL_fifoReadNoPosMv_,
	CPL_fifoGetReadPtr_,
	CPL_fifoReadDone_,
	CPL_fifoGetWritePtr_,
	CPL_fifoWrote_,
	CPL_fifoMoveReadLocation_,
	CPL_rotateLeft_,
	CPL_rotateRight_,

#if defined(ROM_BUILD)
	NULL,													// DAT_copy
	NULL,													// DAT_wait
#else /* ROM_BUILD */
	DAT_copy_dummy,											// DAT_copy
	DAT_wait_dummy,											// DAT_wait
#endif /* ROM_BUILD */

	// BIOS

	// CSL

	// dMAX

	// EDMA

};

#if defined(ROM_BUILD)
#pragma DATA_SECTION (pafhjt, ".far:pafhjt");
far const PAFHJT_t *pafhjt;
#endif /* ROM_BUILD */

#else // PAF_DEVICE = 0xD8000000 ---------------------------------------------

#include <std.h>
#include <clk.h>
#include <dev.h>
#include <hwi.h>
#include <log.h>
#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sio.h>
#include <tsk.h>

#ifndef FAR_SYMBOL
#define FAR_SYMBOL far
#endif

#define PAF_HJT_ORIG
#include "pafhjt.h"

#ifdef PAF_SUPPORT_HJT
const PAF_HJT_fxns PAF_hjtFxns = {
#ifndef PAF_IROM_NOIBIOS
	CLK_countspms,
	CLK_gethtime,
	CLK_getprd,
	DEV_match,
	DEV_mkframe,
	DEV_rmframe,
	HWI_disable,
	HWI_dispatchPlug,
	HWI_restore,
	MEM_alloc,
	MEM_define,
	MEM_free,
	MEM_redefine,
	MEM_valloc,
	QUE_create,
	QUE_get,
	QUE_put,
	SEM_create,
	SEM_delete,
	SEM_ipost,
	SEM_pend,
	SEM_post,
	SIO_create,
	SIO_delete,
	_SIO_idle,
	SIO_issue,
	SIO_reclaim,
	SWI_create,
	SWI_delete,
	SWI_disable,
	SWI_enable,
	TSK_create,
	TSK_deltatime,
	TSK_disable,
	TSK_enable,
	TSK_setpri,
	TSK_settime,
	SYS_error,
#if (PAF_IROM_BUILD == 0xD610A004) || (PAF_IROM_BUILD == 0xD710E001)
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
#else
	calloc,
	malloc,
	realloc,
	free,
	memalign,
#endif
	&KNL_curtask,
	&KNL_curtime,
	&TSK_timerSem,
	CSLDA610_LIB_,
	_CSL_init,
	CACHE_setL2Mode,
	EDMA_intFree,
	EDMA_reset,
	CACHE_flush,
	EDMA_close,
	IRQ_map,
	CACHE_clean,
	MCASP_open,
	EDMA_allocTableEx,
	EDMA_map,
	MCBSP_reset,
	MCBSP_close,
	MCBSP_open,
	MCBSP_getPins,
	TIMER_close,
	TIMER_open,
	EDMA_allocTable,
	EDMA_intAlloc,
	EDMA_open,
	GPIO_open,
	EDMA_freeTableEx,
	_IRQ_eventTable,

#else
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	// CSL CALLS, CSL need to sit in IROM
	CSLDA610_LIB_,
	_CSL_init,
	CACHE_setL2Mode,
	EDMA_intFree,
	EDMA_reset,
	CACHE_flush,
	EDMA_close,
	IRQ_map,
	CACHE_clean,
	MCASP_open,
	EDMA_allocTableEx,
	EDMA_map,
	MCBSP_reset,
	MCBSP_close,
	MCBSP_open,
	MCBSP_getPins,
	TIMER_close,
	TIMER_open,
	EDMA_allocTable,
	EDMA_intAlloc,
	EDMA_open,
	GPIO_open,
	EDMA_freeTableEx,
	_IRQ_eventTable,
#endif
};
FAR_SYMBOL const PAF_HJT_fxns *hjtFxns = &PAF_hjtFxns;
#endif /* PAF_SUPPORT_HJT */

#endif // PAF_DEVICE = 0xD8000000 ---------------------------------------------
