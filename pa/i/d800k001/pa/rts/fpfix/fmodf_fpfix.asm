;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v6.1.5 *
;* Date/Time created: Thu Oct 23 14:04:04 2008                                *
;******************************************************************************
	.compiler_opts --c64p_l1d_workaround=off --endian=little --hll_source=on --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --quiet --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("MATH/fmodf.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v6.1.5 Copyright (c) 1996-2008 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ldexpf")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ldexpf")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$16)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("frexpf")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_frexpf")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$16)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$4


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("_roundf")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("__roundf")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$7


$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("_truncf")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("__truncf")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$9

$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("errno")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_errno")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
;	c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\bin\opt6x.exe OBJ_CYGWIN_RTS6740_LIB\\fmodf.if OBJ_CYGWIN_RTS6740_LIB\\fmodf.opt 
	.sect	".text:_fmodf"
	.clink
	.global	_fmodf

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("fmodf")
	.dwattr $C$DW$12, DW_AT_low_pc(_fmodf)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_fmodf")
	.dwattr $C$DW$12, DW_AT_external
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$12, DW_AT_TI_begin_file("MATH/fmodf.c")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x09)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$12, DW_AT_frame_base[DW_OP_breg31 64]
	.dwattr $C$DW$12, DW_AT_TI_skeletal
	.dwattr $C$DW$12, DW_AT_TI_category("TI Library")
	.dwpsn	file "MATH/fmodf.c",line 10,column 1,is_stmt,address _fmodf
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg4]
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("y")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: fmodf                                                       *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 8 Auto + 56 Save = 64 byte                  *
;******************************************************************************
_fmodf:
;** --------------------------------------------------------------------------*

           STW     .D2T1   A11,*SP--(8)      ; |10| 
||         ZERO    .L2     B5                ; |25| 
||         MVKL    .S1     _errno,A5
||         MVK     .L1     1,A3              ; |25| 

           STW     .D2T1   A10,*SP--(8)      ; |10| 
||         MVKH    .S1     _errno,A5
||         MV      .L1     A4,A10            ; |10| 

           STDW    .D2T2   B13:B12,*SP--     ; |10| 
||         MV      .L1X    B5,A4             ; |25| 

           STDW    .D2T2   B11:B10,*SP--     ; |10| 
||         MV      .L2     B4,B10            ; |10| 

           CMPEQSP .S2     B10,B5,B0         ; |25| 
||         STDW    .D2T1   A15:A14,*SP--     ; |10| 

           STDW    .D2T1   A13:A12,*SP--     ; |10| 
|| [!B0]   ABSSP   .S1X    B10,A12           ; |28| 
|| [!B0]   ABSSP   .S2X    A10,B4            ; |30| 

           STW     .D2T2   B3,*SP--(16)      ; |10| 
|| [ B0]   B       .S1     $C$L5             ; |25| 

   [ B0]   STW     .D1T1   A3,*A5            ; |25| 
||         MVKL    .S1     0xff7fffff,A3
|| [!B0]   CMPLTSP .S2X    A12,B4,B4         ; |30| 

           MVKH    .S1     0xff7fffff,A3
   [!B0]   CMPEQSP .S1     A10,A3,A4         ; |30| 
   [!B0]   CMPEQSP .S1X    B10,A3,A3         ; |30| 
           OR      .L2X    A4,B4,B4
           ; BRANCHCC OCCURS {$C$L5}         ; |25| 
;** --------------------------------------------------------------------------*

           MVK     .S2     127,B11           ; |50| 
||         XOR     .L2     1,B4,B4

           OR      .L2X    B4,A3,B0          ; |30| 
   [ B0]   B       .S1     $C$L4             ; |30| 
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_name("_frexpf")
	.dwattr $C$DW$15, DW_AT_TI_call
   [!B0]   CALL    .S1     _frexpf           ; |43| 
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_name("__divf")
	.dwattr $C$DW$16, DW_AT_TI_call
   [ B0]   CALL    .S1     __divf            ; |32| 
           MV      .L1X    B10,A4            ; |43| 
           MV      .L1X    B5,A11            ; |30| 
           ADD     .L2     4,SP,B4           ; |43| 
           ; BRANCHCC OCCURS {$C$L4}         ; |30| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL0,B3,0       ; |43| 
$C$RL0:    ; CALL OCCURS {_frexpf} {0}       ; |43| 
;** --------------------------------------------------------------------------*
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_name("_frexpf")
	.dwattr $C$DW$17, DW_AT_TI_call

           CALLP   .S2     _frexpf,B3
||         ADD     .L2     8,SP,B4           ; |47| 
||         MV      .L1     A4,A11            ; |43| 
||         MV      .S1     A10,A4            ; |47| 

$C$RL1:    ; CALL OCCURS {_frexpf} {0}       ; |47| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(4),B13       ; |50| 
           LDW     .D2T2   *+SP(8),B4        ; |50| 
           MV      .L1X    B10,A4            ; |51| 
           MVK     .S2     -100,B5           ; |55| 
           NOP             2
           SUB     .L2     B4,B13,B4         ; |50| 
           SUB     .L2     B4,B11,B4         ; |50| 
           CMPGT   .L2     B4,0,B1           ; |50| 
   [ B1]   B       .S1     $C$L1             ; |50| 
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("_ldexpf")
	.dwattr $C$DW$18, DW_AT_TI_call
   [ B1]   CALL    .S1     _ldexpf           ; |51| 
           CMPLT   .L2     B13,B5,B0         ; |55| 
           NOP             3
           ; BRANCHCC OCCURS {$C$L1}         ; |50| 
;** --------------------------------------------------------------------------*

           ADDAW   .D2     B13,25,B12        ; |56| 
|| [!B0]   B       .S1     $C$L2             ; |55| 
|| [!B0]   ZERO    .L1     A15
||         MV      .D1     A10,A4            ; |56| 

   [!B0]   CALL    .S2     __roundf          ; |59| 
|| [!B0]   MVKH    .S1     0x45800000,A15

$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_ldexpf")
	.dwattr $C$DW$19, DW_AT_TI_call
   [ B0]   CALL    .S1     _ldexpf           ; |56| 
   [!B0]   MPYSP   .M1     A15,A11,A4        ; |59| 
           SUB     .L2     B5,B13,B11        ; |56| 
           SUB     .L2     B5,B13,B4         ; |56| 
           ; BRANCHCC OCCURS {$C$L2}         ; |55| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL2,B3,1       ; |56| 
$C$RL2:    ; CALL OCCURS {_ldexpf} {0}       ; |56| 
;** --------------------------------------------------------------------------*
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_ldexpf")
	.dwattr $C$DW$20, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         MV      .L2     B11,B4            ; |56| 
||         MV      .L1     A4,A10            ; |56| 
||         MV      .S1X    B10,A4            ; |56| 

$C$RL3:    ; CALL OCCURS {_ldexpf} {0}       ; |56| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_fmodf")
	.dwattr $C$DW$21, DW_AT_TI_call

           CALLP   .S2     _fmodf,B3
||         MV      .L2X    A4,B4             ; |56| 
||         MV      .L1     A10,A4            ; |56| 

$C$RL4:    ; CALL OCCURS {_fmodf} {0}        ; |56| 
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_ldexpf")
	.dwattr $C$DW$22, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         MV      .L2     B12,B4            ; |56| 

$C$RL5:    ; CALL OCCURS {_ldexpf} {0}       ; |56| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x04)
	.dwattr $C$DW$23, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |12| 
||         RET     .S2     B3                ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L1:    
           ADDKPC  .S2     $C$RL6,B3,0       ; |51| 
$C$RL6:    ; CALL OCCURS {_ldexpf} {0}       ; |51| 
;** --------------------------------------------------------------------------*
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_fmodf")
	.dwattr $C$DW$24, DW_AT_TI_call

           CALLP   .S2     _fmodf,B3
||         MV      .L2X    A4,B4             ; |51| 
||         MV      .L1     A10,A4            ; |51| 

$C$RL7:    ; CALL OCCURS {_fmodf} {0}        ; |51| 
;** --------------------------------------------------------------------------*
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x04)
	.dwattr $C$DW$25, DW_AT_name("__roundf")
	.dwattr $C$DW$25, DW_AT_TI_call

           ZERO    .L1     A15
||         CALL    .S1     __roundf          ; |59| 

           MVKH    .S1     0x45800000,A15
||         LDW     .D2T2   *+SP(4),B13       ; |55| 

           MV      .L1     A4,A10            ; |51| 
||         MPYSP   .M1     A15,A11,A4        ; |59| 

           NOP             2
;** --------------------------------------------------------------------------*
$C$L2:    
           ADDKPC  .S2     $C$RL8,B3,0       ; |59| 
$C$RL8:    ; CALL OCCURS {__roundf} {0}      ; |59| 
;** --------------------------------------------------------------------------*
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_ldexpf")
	.dwattr $C$DW$26, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         SUB     .L2     B13,12,B4         ; |59| 

$C$RL9:    ; CALL OCCURS {_ldexpf} {0}       ; |59| 
;** --------------------------------------------------------------------------*
           CALL    .S1     __divf            ; |71| 
           ZERO    .L2     B4

           MVKH    .S2     0x44800000,B4
||         SUBSP   .L2X    B10,A4,B12        ; |61| 

           MV      .L1     A4,A13            ; |59| 
	.dwpsn	file "MATH\fmodf_i.h",line 67,column 0,is_stmt

           MV      .L1     A10,A4            ; |71| 
||         MV      .S1X    B4,A14
||         MV      .L2     B10,B4            ; |71| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*----------------------------------------------------------------------------*
$C$L3:    
$C$DW$L$_fmodf$16$B:
           ADDKPC  .S2     $C$RL10,B3,0      ; |71| 
$C$RL10:   ; CALL OCCURS {__divf} {0}        ; |71| 
$C$DW$L$_fmodf$16$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_fmodf$17$B:
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("__truncf")
	.dwattr $C$DW$27, DW_AT_TI_call
           CALLP   .S2     __truncf,B3
$C$RL11:   ; CALL OCCURS {__truncf} {0}      ; |71| 
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_frexpf")
	.dwattr $C$DW$28, DW_AT_TI_call

           CALLP   .S2     _frexpf,B3
||         ADD     .L2     4,SP,B4           ; |86| 
||         MV      .D2X    A4,B11            ; |71| 
||         MV      .L1     A10,A4            ; |86| 

$C$RL12:   ; CALL OCCURS {_frexpf} {0}       ; |86| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("__truncf")
	.dwattr $C$DW$29, DW_AT_TI_call

           CALLP   .S2     __truncf,B3
||         MPYSP   .M1     A15,A4,A4         ; |87| 

$C$RL13:   ; CALL OCCURS {__truncf} {0}      ; |87| 
           LDW     .D2T2   *+SP(4),B4        ; |87| 
           NOP             4
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_ldexpf")
	.dwattr $C$DW$30, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         SUB     .L2     B4,12,B4          ; |87| 

$C$RL14:   ; CALL OCCURS {_ldexpf} {0}       ; |87| 
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("_frexpf")
	.dwattr $C$DW$31, DW_AT_TI_call

           CALLP   .S2     _frexpf,B3
||         MV      .L1     A4,A11            ; |87| 
||         MV      .S1X    B11,A4            ; |91| 
||         ADD     .L2     4,SP,B4           ; |91| 

$C$RL15:   ; CALL OCCURS {_frexpf} {0}       ; |91| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("__truncf")
	.dwattr $C$DW$32, DW_AT_TI_call

           CALLP   .S2     __truncf,B3
||         MPYSP   .M1     A14,A4,A4         ; |92| 

$C$RL16:   ; CALL OCCURS {__truncf} {0}      ; |92| 
           LDW     .D2T2   *+SP(4),B4        ; |92| 
           NOP             4
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_ldexpf")
	.dwattr $C$DW$33, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         SUB     .L2     B4,10,B4          ; |92| 

$C$RL17:   ; CALL OCCURS {_ldexpf} {0}       ; |92| 
$C$DW$L$_fmodf$17$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_fmodf$18$B:

           MPYSP   .M1     A13,A4,A4         ; |94| 
||         MPYSP   .M2X    B12,A4,B4         ; |94| 

           SUBSP   .L1     A10,A11,A3        ; |94| 
           NOP             3

           SUBSP   .L1X    A3,B4,A4          ; |94| 
||         SUBSP   .S1     A11,A4,A3         ; |94| 

           NOP             3
           ADDSP   .L1     A4,A3,A10         ; |94| 
           NOP             3
           ABSSP   .S1     A10,A3            ; |98| 
           CMPLTSP .S1     A3,A12,A0         ; |98| 

   [ A0]   LDW     .D2T2   *++SP(16),B3      ; |12| 
|| [!A0]   B       .S1     $C$L3             ; |98| 
|| [ A0]   MV      .L1     A10,A4            ; |100| 
|| [!A0]   MV      .L2     B10,B4            ; |71| 
|| [!A0]   MV      .D1     A10,A4            ; |71| 

$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("__divf")
	.dwattr $C$DW$34, DW_AT_TI_call

   [!A0]   CALL    .S1     __divf            ; |71| 
|| [ A0]   LDDW    .D2T1   *++SP,A13:A12     ; |12| 

   [ A0]   LDDW    .D2T1   *++SP,A15:A14     ; |12| 
   [ A0]   LDDW    .D2T2   *++SP,B11:B10     ; |12| 
   [ A0]   LDDW    .D2T2   *++SP,B13:B12     ; |12| 
	.dwpsn	file "MATH\fmodf_i.h",line 98,column 0,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return

   [ A0]   RET     .S2     B3                ; |12| 
|| [ A0]   LDW     .D2T1   *++SP(8),A10      ; |12| 

           ; BRANCHCC OCCURS {$C$L3}         ; |98| 
$C$DW$L$_fmodf$18$E:
;** --------------------------------------------------------------------------*
           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L4:    
           MV      .L1     A10,A4            ; |32| 

           ADDKPC  .S2     $C$RL18,B3,0      ; |32| 
||         MV      .L2     B10,B4            ; |32| 

$C$RL18:   ; CALL OCCURS {__divf} {0}        ; |32| 
;** --------------------------------------------------------------------------*
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("__truncf")
	.dwattr $C$DW$36, DW_AT_TI_call
           CALLP   .S2     __truncf,B3
$C$RL19:   ; CALL OCCURS {__truncf} {0}      ; |32| 
;** --------------------------------------------------------------------------*

           MV      .L1     A4,A3             ; |32| 
||         ZERO    .L2     B4

           MVKH    .S2     0xbf800000,B4
||         CMPEQSP .S1     A3,A11,A0         ; |34| 

           CMPEQSP .S2X    A3,B4,B0          ; |35| 
|| [!A0]   ADDSP   .L1X    B10,A10,A4        ; |35| 
|| [ A0]   MV      .S1     A10,A4            ; |37| 
|| [!A0]   ZERO    .D1     A11

   [ A0]   MVK     .L2     0x1,B0            ; |36| 
|| [!A0]   SET     .S1     A11,0x17,0x1d,A11

   [ B0]   B       .S2     $C$L6             ; |35| 
|| [!B0]   SUBSP   .L1X    A10,B10,A4        ; |36| 
||         CMPEQSP .S1     A3,A11,A0         ; |36| 
|| [ B0]   LDW     .D2T2   *++SP(16),B3      ; |12| 

   [ B0]   LDDW    .D2T1   *++SP,A13:A12     ; |12| 
   [ B0]   LDDW    .D2T1   *++SP,A15:A14     ; |12| 
   [ B0]   LDDW    .D2T2   *++SP,B11:B10     ; |12| 
   [ B0]   LDDW    .D2T2   *++SP,B13:B12     ; |12| 

   [ B0]   RET     .S2     B3                ; |12| 
|| [ B0]   LDW     .D2T1   *++SP(8),A10      ; |12| 

           ; BRANCHCC OCCURS {$C$L6}         ; |35| 
;** --------------------------------------------------------------------------*
   [!A0]   MV      .L1     A10,A4            ; |37| 
;** --------------------------------------------------------------------------*
$C$L5:    
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_return

           RET     .S2     B3                ; |12| 
||         LDW     .D2T1   *++SP(8),A10      ; |12| 

;** --------------------------------------------------------------------------*
$C$L6:    
           LDW     .D2T1   *++SP(8),A11      ; |12| 
	.dwpsn	file "MATH/fmodf.c",line 12,column 1,is_stmt
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 

$C$DW$38	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$38, DW_AT_name("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib\OBJ_CYGWIN_RTS6740_LIB\\fmodf.asm:$C$L3:1:1224750844")
	.dwattr $C$DW$38, DW_AT_TI_begin_file("MATH\fmodf_i.h")
	.dwattr $C$DW$38, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$38, DW_AT_TI_end_line(0x62)
$C$DW$39	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$39, DW_AT_low_pc($C$DW$L$_fmodf$16$B)
	.dwattr $C$DW$39, DW_AT_high_pc($C$DW$L$_fmodf$16$E)
$C$DW$40	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$40, DW_AT_low_pc($C$DW$L$_fmodf$17$B)
	.dwattr $C$DW$40, DW_AT_high_pc($C$DW$L$_fmodf$17$E)
$C$DW$41	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$41, DW_AT_low_pc($C$DW$L$_fmodf$18$B)
	.dwattr $C$DW$41, DW_AT_high_pc($C$DW$L$_fmodf$18$E)
	.dwendtag $C$DW$38

	.dwattr $C$DW$12, DW_AT_TI_end_file("MATH/fmodf.c")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x0c)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$12

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_ldexpf
	.global	_frexpf
	.global	__roundf
	.global	__truncf
	.global	_errno
	.global	__divf

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("int8_t")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int_least8_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least8_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("int_least16_t")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least16_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$34	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$34, DW_AT_address_class(0x20)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("int_least32_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast8_t")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast16_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast32_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("intptr_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("mantissa_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least32_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast8_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast16_t")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast32_t")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("int40_t")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("int_least40_t")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast40_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("uint40_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least40_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast40_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("int64_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("int_least64_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast64_t")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("intmax_t")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least64_t")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$T$58	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast64_t")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("uintmax_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$19	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$42, DW_AT_name("mantissa")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$42, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x17)
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$43, DW_AT_name("exp")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$43, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x08)
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$44, DW_AT_name("sign")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$44, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$19


$C$DW$T$20	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$45, DW_AT_name("f")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$46, DW_AT_name("fp_format")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("FLOAT2FORM")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$47, DW_AT_name("mantissa1")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_mantissa1")
	.dwattr $C$DW$47, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$48, DW_AT_name("mantissa0")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_mantissa0")
	.dwattr $C$DW$48, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x14)
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$49, DW_AT_name("exp")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$49, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$50, DW_AT_name("sign")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$50, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x08)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$51, DW_AT_name("f")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$52, DW_AT_name("fp_format")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("DOUBLE2FORM")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x0c)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$53, DW_AT_name("sign")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$54, DW_AT_name("exp")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$55, DW_AT_name("mantissa")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("realnum")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg0]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg1]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg2]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg3]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg4]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg5]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg6]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg7]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg8]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg9]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg10]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg11]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg12]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg13]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg14]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg15]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg16]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg17]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg18]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg19]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg20]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg21]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg22]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg23]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg24]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg25]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg26]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg27]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg28]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg29]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg30]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg31]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x20]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x21]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x22]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x23]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x24]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x25]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x26]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x27]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x28]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x29]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x30]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x31]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x32]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x33]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x34]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x35]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x36]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x37]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x38]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x39]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x40]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x41]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x42]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x43]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x44]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x45]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x46]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x47]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x48]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x49]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x50]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x51]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x52]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x53]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x54]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x55]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x56]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x57]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x58]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x59]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x60]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x61]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x62]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x63]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x64]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x65]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x66]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x67]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x68]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x69]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x70]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x71]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x72]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x73]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x74]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x75]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x76]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x77]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x78]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x79]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

