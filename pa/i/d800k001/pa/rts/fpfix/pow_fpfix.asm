;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v6.1.5 *
;* Date/Time created: Thu Oct 23 14:04:21 2008                                *
;******************************************************************************
	.compiler_opts --c64p_l1d_workaround=off --endian=little --hll_source=on --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --quiet --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("MATH/pow.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v6.1.5 Copyright (c) 1996-2008 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib")
;*****************************************************************************
;* CINIT RECORDS                                                             *
;*****************************************************************************
	.sect	".cinit"
	.align	8
	.field  	$C$IR_1,32
	.field  	_A1$1+0,32
	.word	000000000h,03ff00000h		; _A1$1[0] @ 0
	.word	0a2a490dah,03feea4afh		; _A1$1[1] @ 64
	.word	0dcfba487h,03fed5818h		; _A1$1[2] @ 128
	.word	0dd85529ch,03fec199bh		; _A1$1[3] @ 192
	.word	0995ad3adh,03feae89fh		; _A1$1[4] @ 256
	.word	082a3f090h,03fe9c491h		; _A1$1[5] @ 320
	.word	0422aa0dbh,03fe8ace5h		; _A1$1[6] @ 384
	.word	073eb0187h,03fe7a114h		; _A1$1[7] @ 448
	.word	0667f3bcdh,03fe6a09eh		; _A1$1[8] @ 512
	.word	0dd485429h,03fe5ab07h		; _A1$1[9] @ 576
	.word	0d5362a27h,03fe4bfdah		; _A1$1[10] @ 640
	.word	04c123422h,03fe3dea6h		; _A1$1[11] @ 704
	.word	00a31b715h,03fe306feh		; _A1$1[12] @ 768
	.word	06e756238h,03fe2387ah		; _A1$1[13] @ 832
	.word	03c7d517bh,03fe172b8h		; _A1$1[14] @ 896
	.word	06cf9890fh,03fe0b558h		; _A1$1[15] @ 960
	.word	000000000h,03fe00000h		; _A1$1[16] @ 1024
$C$IR_1:	.set	136

	.sect	".cinit"
	.align	8
	.field  	$C$IR_2,32
	.field  	_A2$2+0,32
	.word	0179c0000h,0bc8e9c23h		; _A2$2[0] @ 0
	.word	089500000h,03c711065h		; _A2$2[1] @ 64
	.word	0b0700000h,03c6c7c46h		; _A2$2[2] @ 128
	.word	0ee040000h,0bc741577h		; _A2$2[3] @ 192
	.word	005460000h,03c86324ch		; _A2$2[4] @ 256
	.word	011f00000h,03c7ada09h		; _A2$2[5] @ 320
	.word	0b6c80000h,03c89b07eh		; _A2$2[6] @ 384
	.word	04adc0000h,03c88a62eh		; _A2$2[7] @ 448
$C$IR_2:	.set	64

	.sect	".cinit"
	.align	8
	.field  	$C$IR_3,32
	.field  	_P$3+0,32
	.word	0db4afc28h,03f3c78fdh		; _P$3[0] @ 0
	.word	02e278dach,03f624924h		; _P$3[1] @ 64
	.word	0999e080eh,03f899999h		; _P$3[2] @ 128
	.word	05555554dh,03fb55555h		; _P$3[3] @ 192
$C$IR_3:	.set	32

	.sect	".cinit"
	.align	8
	.field  	$C$IR_4,32
	.field  	_Q$4+0,32
	.word	0e392cc80h,03eef4eddh		; _Q$4[0] @ 0
	.word	0e0384c74h,03f242f7ah		; _Q$4[1] @ 64
	.word	018d7cd9fh,03f55d87eh		; _Q$4[2] @ 128
	.word	06e131d98h,03f83b2abh		; _Q$4[3] @ 192
	.word	0d703026dh,03fac6b08h		; _Q$4[4] @ 256
	.word	0ff82c4ceh,03fcebfbdh		; _Q$4[5] @ 320
	.word	0fefa39efh,03fe62e42h		; _Q$4[6] @ 384
$C$IR_4:	.set	56


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ldexp")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ldexp")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$17)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("frexp")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_frexp")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$17)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$35)
	.dwendtag $C$DW$4


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("modf")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_modf")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$17)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$60)
	.dwendtag $C$DW$7


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("_nround")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("__nround")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("_trunc")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("__trunc")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$12

$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("errno")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_errno")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
_A1$1:	.usect	".far",136,8
_A2$2:	.usect	".far",64,8
_P$3:	.usect	".far",32,8
_Q$4:	.usect	".far",56,8
;	c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\bin\opt6x.exe OBJ_CYGWIN_RTS6740_LIB\\pow.if OBJ_CYGWIN_RTS6740_LIB\\pow.opt 
	.sect	".text:_pow"
	.clink
	.global	_pow

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("pow")
	.dwattr $C$DW$15, DW_AT_low_pc(_pow)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_pow")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$15, DW_AT_TI_begin_file("MATH/pow.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x0a)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x15)
	.dwattr $C$DW$15, DW_AT_frame_base[DW_OP_breg31 96]
	.dwattr $C$DW$15, DW_AT_TI_skeletal
	.dwattr $C$DW$15, DW_AT_TI_category("TI Library")
	.dwpsn	file "MATH/pow.c",line 11,column 1,is_stmt,address _pow
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg4]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("y")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: pow                                                         *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 40 Auto + 56 Save = 96 byte                 *
;******************************************************************************
_pow:
;** --------------------------------------------------------------------------*

           STW     .D2T1   A11,*SP--(8)      ; |11| 
||         ZERO    .L1     A11
||         ZERO    .S1     A8                ; |226| 
||         ZERO    .L2     B7:B6             ; |210| 

           STW     .D2T1   A10,*SP--(8)      ; |11| 
||         SET     .S1     A11,0x14,0x1d,A11
||         ZERO    .L1     A10               ; |226| 

           STDW    .D2T2   B13:B12,*SP--     ; |11| 
||         MV      .L2X    A5,B13            ; |11| 
||         MV      .L1     A11,A9            ; |226| 

           MV      .L2X    A4,B12            ; |11| 
||         ZERO    .L1     A5:A4             ; |208| 
||         STDW    .D2T2   B11:B10,*SP--     ; |11| 
||         MV      .S2     B4,B10            ; |11| 

           MV      .L2     B5,B11            ; |11| 
||         STDW    .D2T1   A15:A14,*SP--     ; |11| 

           CMPGTDP .S1X    B13:B12,A5:A4,A1  ; |208| 
||         STDW    .D2T1   A13:A12,*SP--     ; |11| 
||         CMPEQDP .S2X    B11:B10,A9:A8,B4  ; |226| 

           STW     .D2T2   B3,*SP--(48)      ; |11| 

           CMPEQDP .S1X    B13:B12,A11:A10,A3 ; |226| 
|| [ A1]   ZERO    .L1     A12               ; |233| 
|| [!A1]   B       .S2     $C$L8             ; |208| 

           CMPLTDP .S2     B13:B12,B7:B6,B1  ; |210| 

           OR      .L1X    B4,A3,A0          ; |226| 
|| [ A1]   ZERO    .D2     B4
|| [!A1]   ZERO    .L2     B5:B4             ; |221| 

   [ A1]   SET     .S2     B4,0x15,0x1d,B4
|| [!A1]   ZERO    .L1     A0                ; |230| nullify predicate
|| [ A1]   MV      .S1X    B13,A5            ; |230| 
|| [ A1]   ZERO    .L2     B1                ; |212| nullify predicate

   [ A1]   MV      .L1X    B12,A4            ; |230| 
|| [ A0]   B       .S1     $C$L9             ; |226| 
|| [!A1]   CMPLTDP .S2     B11:B10,B5:B4,B0  ; |221| 

   [ A1]   MV      .L1X    B4,A10            ; |233| 
|| [ A1]   ADD     .L2     4,SP,B4           ; |230| 
|| [ B1]   B       .S1     $C$L10            ; |210| 

           ; BRANCHCC OCCURS {$C$L8}         ; |208| 
;** --------------------------------------------------------------------------*
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("_frexp")
	.dwattr $C$DW$18, DW_AT_TI_call
   [!A0]   CALL    .S1     _frexp            ; |230| 
           NOP             3
           ; BRANCHCC OCCURS {$C$L9}         ; |226| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL0,B3,1       ; |230| 
$C$RL0:    ; CALL OCCURS {_frexp} {0}        ; |230| 
;** --------------------------------------------------------------------------*
           DPTRUNC .L2     B11:B10,B5        ; |236| 
           LDW     .D2T2   *+SP(4),B13       ; |236| 
           MV      .L1     A5,A7             ; |230| 
           MV      .L1     A4,A6             ; |230| 
           INTDP   .L2     B5,B7:B6          ; |236| 
           MV      .L1     A10,A13
           CMPEQDP .S1     A7:A6,A13:A12,A0  ; |233| 
           NOP             1
   [ A0]   SUB     .S2     B13,1,B4          ; |237| 
           CMPEQDP .S2     B7:B6,B11:B10,B0  ; |236| 
   [ A0]   MPY32   .M2     B5,B4,B4          ; |237| 

   [!A0]   ZERO    .L2     B0                ; |237| 
||         MVKL    .S2     _A1$1+64,B6

   [!B0]   B       .S1     $C$L1             ; |236| 
||         MVKH    .S2     _A1$1+64,B6

$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_ldexp")
	.dwattr $C$DW$19, DW_AT_TI_call

   [ B0]   CALL    .S1     _ldexp            ; |237| 
|| [!B0]   LDDW    .D2T2   *B6,B5:B4         ; |241| 

   [ A0]   ZERO    .L1     A4                ; |237| 
   [ A0]   MV      .L1     A11,A5            ; |237| 
           NOP             2
           ; BRANCHCC OCCURS {$C$L1}         ; |236| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL1,B3,0       ; |237| 
$C$RL1:    ; CALL OCCURS {_ldexp} {0}        ; |237| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(48),B3      ; |13| 
           LDDW    .D2T1   *++SP,A13:A12     ; |13| 
           LDDW    .D2T1   *++SP,A15:A14     ; |13| 
           LDDW    .D2T2   *++SP,B11:B10     ; |13| 
           LDDW    .D2T2   *++SP,B13:B12     ; |13| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x04)
	.dwattr $C$DW$20, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |13| 
||         RET     .S2     B3                ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |13| 
;** --------------------------------------------------------------------------*
$C$L1:    

           CMPGTDP .S1X    A7:A6,B5:B4,A0    ; |241| 
||         MVK     .L2     0x1,B12           ; |240| 
||         SUBAW   .D2     B6,16,B31

           STW     .D2T2   B31,*+SP(16)      ; |241| 
   [!A0]   MVK     .L2     0x9,B12           ; |241| 
           ADDAD   .D2     B31,B12,B4        ; |242| 
           LDDW    .D2T2   *+B4(24),B5:B4    ; |242| 
           LDW     .D2T2   *+SP(16),B30      ; |242| 
           MVKL    .S1     _A2$2-8,A3
           MVKH    .S1     _A2$2-8,A3
           NOP             1
           CMPGTDP .S2X    A7:A6,B5:B4,B0    ; |242| 
           ADDAD   .D2     B30,B12,B4        ; |242| 
   [!B0]   ADDK    .S2     32,B4             ; |242| 
           LDDW    .D2T1   *+B4(8),A5:A4     ; |243| 
   [!B0]   ADD     .L2     4,B12,B12         ; |242| 
           NOP             3
           CMPGTDP .S1     A7:A6,A5:A4,A0    ; |243| 
           NOP             1

   [!A0]   ADD     .L2     2,B12,B12         ; |243| 
|| [!A0]   ADDK    .S2     16,B4             ; |243| 

           ADD     .L2     1,B12,B6          ; |252| 
||         LDDW    .D2T2   *B4,B5:B4         ; |252| 

           NOP             1

           SHRU    .S1X    B6,31,A0          ; |252| 
||         MVK     .L2     0x4,B6            ; |252| 

   [!A0]   MV      .L2X    A12,B6            ; |252| 
           ADDAW   .D2     B6,B12,B6         ; |252| 

           ADD     .L2     4,B6,B6           ; |252| 
||         SUBDP   .L1X    A7:A6,B5:B4,A5:A4 ; |252| 

           ADDDP   .L2X    B5:B4,A7:A6,B5:B4 ; |252| 
           SHR     .S1X    B6,3,A8           ; |252| 
           LDDW    .D1T1   *+A3[A8],A9:A8    ; |252| 
           NOP             4
           SUBDP   .L1     A5:A4,A9:A8,A5:A4 ; |252| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("__divd")
	.dwattr $C$DW$21, DW_AT_TI_call
           CALL    .S1     __divd            ; |252| 
           ADDKPC  .S2     $C$RL2,B3,4       ; |252| 
$C$RL2:    ; CALL OCCURS {__divd} {0}        ; |252| 
;** --------------------------------------------------------------------------*
           ADDDP   .L1     A5:A4,A5:A4,A9:A8 ; |252| 
           MVKL    .S1     _P$3,A3
           MVKH    .S1     _P$3,A3
           LDDW    .D1T1   *A3,A5:A4         ; |255| 
           ADD     .L1     8,A3,A3           ; |255| 
           MVK     .L2     0x3,B0            ; |255| 
           NOP             1
           MPYDP   .M1     A9:A8,A9:A8,A7:A6 ; |253| 
           NOP             9
	.dwpsn	file "MATH\powf_i.h",line 255,column 0,is_stmt
           MPYDP   .M1     A7:A6,A5:A4,A5:A4 ; |255| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 255
;*      Loop opening brace source line   : 255
;*      Loop closing brace source line   : 255
;*      Known Minimum Trip Count         : 3                    
;*      Known Maximum Trip Count         : 3                    
;*      Known Max Trip Count Factor      : 3
;*      Loop Carried Dependency Bound(^) : 17
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1        0     
;*      .M units                     4*       0     
;*      .X cross paths               0        0     
;*      .T address paths             1        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        0     
;*
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
$C$L2:    
$C$DW$L$_pow$9$B:
           LDDW    .D1T1   *A3++,A17:A16     ; |255| 
           NOP             8
$C$DW$L$_pow$9$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_pow$10$B:
           ADDDP   .L1     A17:A16,A5:A4,A5:A4 ; |255| 
           SUB     .L2     B0,1,B0           ; |255| 
   [ B0]   BNOP    .S1     $C$L2,4           ; |255| 
           MPYDP   .M1     A7:A6,A5:A4,A5:A4 ; |255| 
           ; BRANCHCC OCCURS {$C$L2}         ; |255| 
$C$DW$L$_pow$10$E:
;** --------------------------------------------------------------------------*
           SHL     .S2     B13,4,B31         ; |260| 
           MV      .L1     A12,A13           ; |261| 
           ZERO    .L2     B30
           ZERO    .L2     B13
           ZERO    .L1     A3
           MV      .L2X    A12,B8            ; |261| 
           MVKH    .S2     0x3fb00000,B13
           MVKH    .S1     0x41a00000,A3
           MVKL    .S1     0x3fdc551d,A7
           MPYDP   .M1     A9:A8,A5:A4,A5:A4 ; |256| 
           MVKL    .S1     0x94ae0bf8,A6
           MVKH    .S1     0x3fdc551d,A7
           MVKH    .S1     0x94ae0bf8,A6
           STW     .D2T1   A3,*+SP(24)       ; |261| 
           MVKH    .S2     0x40300000,B30
           STW     .D2T2   B13,*+SP(32)      ; |261| 
           MV      .L2X    A7,B5             ; |258| 
           MV      .L2X    A6,B4             ; |258| 
           MPYDP   .M2X    B5:B4,A9:A8,B7:B6 ; |259| 
           MPYDP   .M1     A7:A6,A5:A4,A17:A16 ; |258| 
           MV      .L2     B30,B9            ; |261| 
           STW     .D2T2   B30,*+SP(28)      ; |261| 
           ZERO    .L2     B5
           SUB     .L2     B31,B12,B4        ; |260| 
           MVKH    .S2     0xc1a00000,B5
           MV      .L1     A3,A7             ; |261| 
           MV      .L1     A12,A6            ; |261| 
           CMPLTDP .S1X    B11:B10,A7:A6,A31 ; |261| 
           MV      .L2X    A12,B12           ; |261| 
           ADDDP   .S1     A17:A16,A5:A4,A5:A4 ; |258| 
           INTDP   .L1X    B4,A17:A16        ; |260| 
           XOR     .L1     1,A31,A3          ; |261| 
           MV      .L2X    A12,B4            ; |261| 
           CMPLTDP .S2     B11:B10,B5:B4,B4  ; |261| 
           STW     .D2T2   B5,*+SP(20)       ; |261| 
           MPYDP   .M1X    B13:B12,A17:A16,A11:A10 ; |260| 
           OR      .L2X    B4,A3,B0          ; |261| 
           ADDDP   .L2X    B7:B6,A5:A4,B7:B6 ; |259| 
           MVK     .S2     0x4,B4            ; |261| 
           MV      .L1X    B13,A12           ; |261| 
           MV      .L1X    B11,A5            ; |261| 
           MV      .L1X    B10,A4            ; |261| 
           NOP             2

           ADDDP   .L2X    A9:A8,B7:B6,B9:B8 ; |259| 
||         MPYDP   .M2     B9:B8,B11:B10,B7:B6 ; |261| 

           NOP             3
   [!B0]   B       .S1     $C$L3             ; |261| 
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_ldexp")
	.dwattr $C$DW$22, DW_AT_TI_call
   [ B0]   CALL    .S1     _ldexp            ; |261| 
           NOP             1
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("__nround")
	.dwattr $C$DW$23, DW_AT_TI_call
   [!B0]   CALL    .S1     __nround          ; |261| 
           MV      .L1X    B9,A14            ; |261| 
           MV      .L1X    B8,A15            ; |261| 
           ; BRANCHCC OCCURS {$C$L3}         ; |261| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL3,B3,0       ; |261| 
$C$RL3:    ; CALL OCCURS {_ldexp} {0}        ; |261| 
;** --------------------------------------------------------------------------*
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("__nround")
	.dwattr $C$DW$24, DW_AT_TI_call
           CALLP   .S2     __nround,B3
$C$RL4:    ; CALL OCCURS {__nround} {0}      ; |261| 
;** --------------------------------------------------------------------------*

           B       .S1     $C$L4             ; |261| 
||         MV      .L1X    B12,A6            ; |261| 
||         MV      .D1     A12,A7            ; |261| 

           MPYDP   .M1     A7:A6,A5:A4,A13:A12 ; |261| 
           NOP             4
           ; BRANCH OCCURS {$C$L4}           ; |261| 
;** --------------------------------------------------------------------------*
$C$L3:    
           MV      .L1X    B6,A4             ; |261| 
           ADDKPC  .S2     $C$RL5,B3,0       ; |261| 
           MV      .L1X    B7,A5             ; |261| 
$C$RL5:    ; CALL OCCURS {__nround} {0}      ; |261| 
;** --------------------------------------------------------------------------*

           MV      .L1     A13,A6            ; |261| 
||         MV      .S1     A12,A7            ; |261| 

           MPYDP   .M1     A7:A6,A5:A4,A13:A12 ; |261| 
           NOP             4
;** --------------------------------------------------------------------------*
$C$L4:    
           MV      .L2X    A14,B5
           MV      .L2X    A15,B4
           MPYDP   .M2     B11:B10,B5:B4,B5:B4 ; |263| 
           LDW     .D2T1   *+SP(24),A3       ; |269| 
           MV      .L2     B12,B6            ; |269| 
           SUBDP   .L1X    B11:B10,A13:A12,A5:A4 ; |263| 
           LDW     .D2T2   *+SP(28),B7       ; |269| 
           MV      .L1X    B12,A2            ; |263| 
           MV      .L2     B13,B11           ; |269| 
           MV      .L2     B12,B10           ; |269| 
           NOP             2
           MPYDP   .M1     A11:A10,A5:A4,A5:A4 ; |263| 
           NOP             9
           ADDDP   .L1X    A5:A4,B5:B4,A5:A4 ; |263| 
           LDW     .D2T2   *+SP(20),B5       ; |269| 
           MV      .L2     B12,B4            ; |269| 
           NOP             4

           MV      .L1     A4,A15            ; |263| 
||         MV      .D1     A4,A6             ; |263| 
||         CMPLTDP .S1     A5:A4,A3:A2,A4    ; |269| 

           MV      .L1     A5,A7             ; |263| 

           XOR     .L1     1,A4,A3           ; |269| 
||         MV      .S1     A15,A4            ; |269| 

           CMPLTDP .S2X    A7:A6,B5:B4,B4    ; |269| 
||         MPYDP   .M1X    B7:B6,A5:A4,A7:A6 ; |269| 

           MV      .L1     A5,A14            ; |263| 
           OR      .L2X    B4,A3,B0          ; |269| 
   [!B0]   B       .S1     $C$L5             ; |269| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_ldexp")
	.dwattr $C$DW$25, DW_AT_TI_call
   [ B0]   CALL    .S1     _ldexp            ; |269| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("__nround")
	.dwattr $C$DW$26, DW_AT_TI_call
   [!B0]   CALL    .S1     __nround          ; |269| 
           MVK     .L2     0x4,B4            ; |269| 
           NOP             2
           ; BRANCHCC OCCURS {$C$L5}         ; |269| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL6,B3,0       ; |269| 
$C$RL6:    ; CALL OCCURS {_ldexp} {0}        ; |269| 
;** --------------------------------------------------------------------------*
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("__nround")
	.dwattr $C$DW$27, DW_AT_TI_call
           CALLP   .S2     __nround,B3
$C$RL7:    ; CALL OCCURS {__nround} {0}      ; |269| 
;** --------------------------------------------------------------------------*

           B       .S1     $C$L6             ; |269| 
||         MPYDP   .M1X    B13:B12,A5:A4,A9:A8 ; |269| 
||         MVKL    .S2     0x40c62d80,B5
||         MVK     .L1     0xffffffff,A6     ; |280| 
||         MVK     .L2     2,B31             ; |280| 
||         MV      .D2     B12,B4            ; |280| 

           MVKL    .S1     0x7fefffff,A7
||         MVKH    .S2     0x40c62d80,B5

           MVKH    .S1     0x7fefffff,A7
           MVKL    .S2     _errno,B6
           MPYDP   .M1     A13:A12,A11:A10,A5:A4 ; |276| 
           MVKH    .S2     _errno,B6
           ; BRANCH OCCURS {$C$L6}           ; |269| 
;** --------------------------------------------------------------------------*
$C$L5:    
           ADDKPC  .S2     $C$RL8,B3,0       ; |269| 

           MV      .L1     A7,A5             ; |269| 
||         MV      .S1     A6,A4             ; |269| 

$C$RL8:    ; CALL OCCURS {__nround} {0}      ; |269| 
;** --------------------------------------------------------------------------*
           MPYDP   .M1X    B11:B10,A5:A4,A9:A8 ; |269| 
           MVKL    .S2     _errno,B6
           MVKL    .S2     0x40c62d80,B5
           MVKL    .S1     0x7fefffff,A7

           MVKH    .S2     _errno,B6
||         MPYDP   .M1     A13:A12,A11:A10,A5:A4 ; |276| 

           MVKH    .S1     0x7fefffff,A7
||         MVKH    .S2     0x40c62d80,B5
||         MV      .L2     B12,B4            ; |280| 
||         MVK     .D2     2,B31             ; |280| 
||         MVK     .L1     0xffffffff,A6     ; |280| 

;** --------------------------------------------------------------------------*
$C$L6:    
           NOP             8
           ADDDP   .L1     A5:A4,A9:A8,A11:A10 ; |276| 
           MV      .S1X    B10,A4            ; |280| 
           MVKL    .S1     0xc0c61d80,A5
           MVKH    .S1     0xc0c61d80,A5
           NOP             3
           CMPLTDP .S1     A11:A10,A5:A4,A0  ; |282| 

           CMPGTDP .S2X    A11:A10,B5:B4,B0  ; |278| 
||         LDW     .D2T2   *+SP(28),B5
||         MV      .L1     A14,A5            ; |280| 
||         MV      .D1     A15,A4            ; |280| 

           SUBDP   .S1     A5:A4,A9:A8,A13:A12 ; |271| 

   [!B0]   MV      .L2     B12,B4
|| [!B0]   ZERO    .L1     A7:A6             ; |290| 
|| [ B0]   ZERO    .D1     A0                ; nullify predicate
|| [ B0]   B       .S2     $C$L13            ; |280| 
|| [ B0]   STW     .D2T2   B31,*B6           ; |280| 

   [ A0]   B       .S2     $C$L13            ; |282| 
|| [!B0]   ZERO    .L1     A8                ; |286| 
|| [!B0]   ZERO    .S1     A31               ; |287| 
|| [!B0]   STW     .D2T1   A6,*+SP(36)       ; |290| 
|| [!B0]   ZERO    .L2     B12               ; |286| 
|| [!B0]   MV      .D1X    B11,A14           ; |290| 

   [!B0]   STW     .D2T1   A8,*+SP(20)       ; |282| 
|| [!B0]   ZERO    .L2     B10               ; |284| 

           MV      .S1X    B5,A3             ; |290| 
|| [!B0]   MV      .L2     B5,B13            ; |290| 
|| [!B0]   STW     .D2T1   A7,*+SP(40)       ; |290| 
|| [!B0]   ZERO    .L1     A7:A6             ; |282| 

   [!B0]   MPYDP   .M1X    B5:B4,A11:A10,A5:A4 ; |284| 
|| [!B0]   STW     .D2T1   A3,*+SP(32)       ; |284| 

   [!B0]   STW     .D2T1   A31,*+SP(24)      ; |282| 
           ; BRANCHCC OCCURS {$C$L13}        ; |280| 
;** --------------------------------------------------------------------------*
           NOP             1
           ; BRANCHCC OCCURS {$C$L13}        ; |282| 
;** --------------------------------------------------------------------------*
           NOP             1
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("__nround")
	.dwattr $C$DW$28, DW_AT_TI_call
           CALL    .S1     __nround          ; |284| 
           ADDKPC  .S2     $C$RL9,B3,4       ; |284| 
$C$RL9:    ; CALL OCCURS {__nround} {0}      ; |284| 
;** --------------------------------------------------------------------------*
           MPYDP   .M1X    B11:B10,A5:A4,A5:A4 ; |284| 
           NOP             9
           NOP             1

           MV      .L2X    A4,B10            ; |284| 
||         MV      .L1     A5,A15            ; |284| 
||         SUBDP   .S1     A11:A10,A5:A4,A5:A4 ; |285| 

           NOP             6
           ADDDP   .L1     A5:A4,A13:A12,A11:A10 ; |285| 
           NOP             6
           MPYDP   .M1X    B13:B12,A11:A10,A5:A4 ; |286| 
           NOP             3
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("__nround")
	.dwattr $C$DW$29, DW_AT_TI_call
           CALLP   .S2     __nround,B3
$C$RL10:   ; CALL OCCURS {__nround} {0}      ; |286| 
;** --------------------------------------------------------------------------*

           MV      .L1     A14,A7            ; |286| 
||         MV      .S1X    B12,A6            ; |286| 

           MPYDP   .M1     A7:A6,A5:A4,A9:A8 ; |286| 
           MVKL    .S2     _Q$4,B4
           MVKH    .S2     _Q$4,B4
           MV      .L1     A15,A5
           MV      .L1X    B10,A4
           LDDW    .D2T2   *B4,B9:B8         ; |300| 
           MVK     .L2     0x6,B0            ; |300| 
           NOP             3
           ADDDP   .L1     A9:A8,A5:A4,A7:A6 ; |287| 
           MV      .S1X    B13,A5            ; |287| 
           MV      .L1X    B12,A4            ; |287| 
           SUBDP   .L1     A11:A10,A9:A8,A13:A12 ; |288| 
           NOP             3
           MPYDP   .M1     A5:A4,A7:A6,A7:A6 ; |287| 
           NOP             9
           DPTRUNC .L1     A7:A6,A3          ; |287| 
           LDW     .D2T1   *+SP(40),A7       ; |287| 
           LDW     .D2T1   *+SP(36),A6       ; |287| 
           MV      .S1     A14,A5            ; |292| 
           ZERO    .L1     A4                ; |292| 
           NOP             2
           CMPGTDP .S1     A13:A12,A7:A6,A0  ; |290| 
           NOP             1
   [ A0]   SUBDP   .L1     A13:A12,A5:A4,A13:A12 ; |292| 
   [ A0]   ADD     .S1     1,A3,A3           ; |293| 

           CMPLT   .L1     A3,0,A5           ; |297| 
||         SHR     .S1     A3,3,A4           ; |297| 

           XOR     .L1     1,A5,A4           ; |297| 
||         SHRU    .S1     A4,28,A5          ; |297| 

           ADD     .L1     A3,A5,A5          ; |297| 

           AND     .S1     -16,A5,A6         ; |298| 
||         SHL     .S2X    A4,4,B5           ; |298| 

           SHR     .S2X    A5,4,B6           ; |297| 
           ADD     .L2X    B5,A6,B5          ; |298| 

           ADD     .L2X    A4,B6,B4          ; |297| 
||         ADD     .L1X    8,B4,A4           ; |300| 

	.dwpsn	file "MATH\powf_i.h",line 300,column 0,is_stmt

           MPYDP   .M1X    A13:A12,B9:B8,A7:A6 ; |300| 
||         SUB     .L2X    B5,A3,B5          ; |298| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 300
;*      Loop opening brace source line   : 300
;*      Loop closing brace source line   : 300
;*      Known Minimum Trip Count         : 6                    
;*      Known Maximum Trip Count         : 6                    
;*      Known Max Trip Count Factor      : 6
;*      Loop Carried Dependency Bound(^) : 17
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1        0     
;*      .M units                     4*       0     
;*      .X cross paths               0        0     
;*      .T address paths             1        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        0     
;*
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
$C$L7:    
$C$DW$L$_pow$28$B:
           LDDW    .D1T1   *A4++,A9:A8       ; |300| 
           NOP             8
$C$DW$L$_pow$28$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_pow$29$B:

           ADDDP   .L1     A9:A8,A7:A6,A7:A6 ; |300| 
||         SUB     .L2     B0,1,B0           ; |300| 

   [!B0]   LDW     .D2T2   *+SP(16),B6
   [ B0]   BNOP    .S1     $C$L7,3           ; |300| 
   [!B0]   LDDW    .D2T2   *+B6[B5],B7:B6    ; |302| 
           MPYDP   .M1     A13:A12,A7:A6,A7:A6 ; |300| 
           ; BRANCHCC OCCURS {$C$L7}         ; |300| 
$C$DW$L$_pow$29$E:
;** --------------------------------------------------------------------------*
           NOP             9
           MPYDP   .M1X    A7:A6,B7:B6,A5:A4 ; |302| 
           NOP             9
           ADDDP   .L1X    A5:A4,B7:B6,A5:A4 ; |302| 
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_ldexp")
	.dwattr $C$DW$30, DW_AT_TI_call
           CALL    .S1     _ldexp            ; |305| 
           ADDKPC  .S2     $C$RL11,B3,4      ; |305| 
$C$RL11:   ; CALL OCCURS {_ldexp} {0}        ; |305| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(48),B3      ; |13| 
           LDDW    .D2T1   *++SP,A13:A12     ; |13| 
           LDDW    .D2T1   *++SP,A15:A14     ; |13| 
           LDDW    .D2T2   *++SP,B11:B10     ; |13| 
           LDDW    .D2T2   *++SP,B13:B12     ; |13| 
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x04)
	.dwattr $C$DW$31, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |13| 
||         RET     .S2     B3                ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |13| 
;** --------------------------------------------------------------------------*
$C$L8:    
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x04)
	.dwattr $C$DW$32, DW_AT_name("__trunc")
	.dwattr $C$DW$32, DW_AT_TI_call

   [!B1]   MVK     .L1     1,A3              ; |221| 
|| [ B1]   CALL    .S1     __trunc           ; |212| 

           MV      .L1X    B10,A4            ; |212| 
           MV      .L1X    B11,A5            ; |212| 
   [!B1]   MVKL    .S1     _errno,A4
   [!B1]   MVKH    .S1     _errno,A4
           ; BRANCHCC OCCURS {$C$L10}        ; |210| 
;** --------------------------------------------------------------------------*

   [ B0]   B       .S2     $C$L13            ; |221| 
||         MVKL    .S1     0xffefffff,A7
||         MVK     .L1     0xffffffff,A6     ; |221| 
|| [ B0]   STW     .D1T1   A3,*A4            ; |221| 

           CMPEQDP .S2     B11:B10,B5:B4,B1  ; |222| 
||         MVKH    .S1     0xffefffff,A7
|| [!B0]   ZERO    .L1     A6                ; |222| 

   [!B0]   MV      .L1     A9,A7             ; |222| 
   [ B0]   ZERO    .L2     B1                ; |222| nullify predicate
   [ B1]   BNOP    .S1     $C$L14,1          ; |222| 
           ; BRANCHCC OCCURS {$C$L13}        ; |221| 
;** --------------------------------------------------------------------------*
   [ B1]   LDW     .D2T2   *++SP(48),B3      ; |13| 
   [ B1]   LDDW    .D2T1   *++SP,A13:A12     ; |13| 
   [ B1]   LDDW    .D2T1   *++SP,A15:A14     ; |13| 
   [ B1]   LDDW    .D2T2   *++SP,B11:B10     ; |13| 
           ; BRANCHCC OCCURS {$C$L14}        ; |222| 
;** --------------------------------------------------------------------------*
$C$L9:    
           LDW     .D2T2   *++SP(48),B3      ; |13| 
           LDDW    .D2T1   *++SP,A13:A12     ; |13| 
           LDDW    .D2T1   *++SP,A15:A14     ; |13| 

           LDDW    .D2T2   *++SP,B11:B10     ; |13| 
||         MV      .L1X    B12,A4            ; |223| 

           MV      .L1X    B13,A5            ; |223| 
||         LDDW    .D2T2   *++SP,B13:B12     ; |13| 

$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x04)
	.dwattr $C$DW$33, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |13| 
||         RET     .S2     B3                ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |13| 
;** --------------------------------------------------------------------------*
$C$L10:    
           ADDKPC  .S2     $C$RL12,B3,0      ; |212| 
$C$RL12:   ; CALL OCCURS {__trunc} {0}       ; |212| 
;** --------------------------------------------------------------------------*

           ZERO    .L2     B5
||         ZERO    .D2     B4                ; |214| 
||         ZERO    .L1     A7
||         ZERO    .S1     A6                ; |214| 
||         CMPEQDP .S2X    A5:A4,B11:B10,B0  ; |212| 

           MVKH    .S1     0x41e00000,A7
||         MV      .L1X    B11,A5            ; |212| 
||         MVK     .L2     1,B6              ; |212| 

           MV      .L1X    B10,A4            ; |212| 
||         MVKH    .S2     0xc1e00000,B5

   [ B0]   B       .S1     $C$L11            ; |212| 
||         CMPLTDP .S2     B11:B10,B5:B4,B4  ; |214| 

           CMPLTDP .S1X    B11:B10,A7:A6,A3  ; |214| 
||         MV      .L2     B11,B5            ; |213| 

$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_pow")
	.dwattr $C$DW$34, DW_AT_TI_call

   [ B0]   CALL    .S2     _pow              ; |213| 
||         XOR     .L2     1,B4,B4           ; |214| 

$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x04)
	.dwattr $C$DW$35, DW_AT_name("__nround")
	.dwattr $C$DW$35, DW_AT_TI_call

           MVKL    .S1     _errno,A6
|| [!B0]   CALL    .S2     __nround          ; |212| 

           AND     .L1X    A3,B4,A3          ; |214| 
||         MVKH    .S1     _errno,A6
||         MV      .L2     B10,B4            ; |213| 
||         ABSDP   .S2     B13:B12,B9:B8     ; |213| 

           MV      .L1     A3,A12            ; |214| 
           ; BRANCHCC OCCURS {$C$L11}        ; |212| 
;** --------------------------------------------------------------------------*
           STW     .D1T2   B6,*A6            ; |212| 
           ADDKPC  .S2     $C$RL13,B3,1      ; |212| 
$C$RL13:   ; CALL OCCURS {__nround} {0}      ; |212| 
;** --------------------------------------------------------------------------*

           MV      .L2X    A4,B4             ; |212| 
||         MV      .L1X    B12,A4            ; |212| 

$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_pow")
	.dwattr $C$DW$36, DW_AT_TI_call

           CALLP   .S2     _pow,B3
||         MV      .L2X    A5,B5             ; |212| 
||         MV      .L1X    B13,A5            ; |212| 

$C$RL14:   ; CALL OCCURS {_pow} {0}          ; |212| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(48),B3      ; |13| 
           LDDW    .D2T1   *++SP,A13:A12     ; |13| 
           LDDW    .D2T1   *++SP,A15:A14     ; |13| 
           LDDW    .D2T2   *++SP,B11:B10     ; |13| 
           LDDW    .D2T2   *++SP,B13:B12     ; |13| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x04)
	.dwattr $C$DW$37, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |13| 
||         RET     .S2     B3                ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |13| 
;** --------------------------------------------------------------------------*
$C$L11:    
           MV      .L1X    B9,A5             ; |213| 

           MV      .L1X    B8,A4             ; |213| 
||         ADDKPC  .S2     $C$RL15,B3,0      ; |213| 

$C$RL15:   ; CALL OCCURS {_pow} {0}          ; |213| 
;** --------------------------------------------------------------------------*

           ZERO    .L1     A11:A10           ; |217| 
||         ZERO    .L2     B5
||         MV      .S2X    A10,B4            ; |217| 

           SET     .S2     B5,0x15,0x1d,B5
           MPYDP   .M2     B5:B4,B11:B10,B7:B6 ; |217| 
           MV      .L1     A5,A13            ; |213| 
           DPTRUNC .L2     B11:B10,B31       ; |215| 
           MV      .L1     A12,A0            ; |217| 
           MV      .L2X    A4,B12            ; |213| 
   [ A0]   ZERO    .L1     A3                ; |215| 
   [ A0]   SET     .S1     A3,31,31,A3       ; |215| 
   [ A0]   B       .S1     $C$L12            ; |214| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_modf")
	.dwattr $C$DW$38, DW_AT_TI_call
   [!A0]   CALL    .S1     _modf             ; |217| 
           AND     .L2     1,B31,B0          ; |215| 
           ADD     .L2     8,SP,B4           ; |217| 
           MV      .L1X    B7,A5             ; |217| 
           MV      .L1X    B6,A4             ; |217| 
           ; BRANCHCC OCCURS {$C$L12}        ; |214| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL16,B3,0      ; |217| 
$C$RL16:   ; CALL OCCURS {_modf} {0}         ; |217| 
;** --------------------------------------------------------------------------*
           CMPEQDP .S1     A5:A4,A11:A10,A0  ; |217| 
           ZERO    .L1     A3                ; |217| 
           SET     .S1     A3,31,31,A3       ; |217| 

           XOR     .L1     A13,A3,A7         ; |217| 
||         LDW     .D2T2   *++SP(48),B3      ; |13| 

   [ A0]   MV      .L1     A13,A7            ; |219| 
||         LDDW    .D2T1   *++SP,A13:A12     ; |13| 

           LDDW    .D2T1   *++SP,A15:A14     ; |13| 
           LDDW    .D2T2   *++SP,B11:B10     ; |13| 

           MV      .L1X    B12,A4            ; |217| 
||         LDDW    .D2T2   *++SP,B13:B12     ; |13| 

$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x04)
	.dwattr $C$DW$39, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |13| 
||         RET     .S2     B3                ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           MV      .L1     A7,A5             ; |219| 
           NOP             3
           ; BRANCH OCCURS {B3}              ; |13| 
;** --------------------------------------------------------------------------*
$C$L12:    

   [ B0]   XOR     .L1     A13,A3,A13        ; |215| 
||         MV      .S1X    B12,A6            ; |219| 

           MV      .L1     A13,A7            ; |219| 
;** --------------------------------------------------------------------------*
$C$L13:    
           LDW     .D2T2   *++SP(48),B3      ; |13| 
           LDDW    .D2T1   *++SP,A13:A12     ; |13| 
           LDDW    .D2T1   *++SP,A15:A14     ; |13| 
           LDDW    .D2T2   *++SP,B11:B10     ; |13| 
           NOP             3
;** --------------------------------------------------------------------------*
$C$L14:    
           LDDW    .D2T2   *++SP,B13:B12     ; |13| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return

           RET     .S2     B3                ; |13| 
||         LDW     .D2T1   *++SP(8),A10      ; |13| 

           LDW     .D2T1   *++SP(8),A11      ; |13| 
           MV      .L1     A6,A4             ; |219| 
           MV      .L1     A7,A5             ; |219| 
	.dwpsn	file "MATH/pow.c",line 13,column 1,is_stmt
           NOP             2
           ; BRANCH OCCURS {B3}              ; |13| 

$C$DW$41	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$41, DW_AT_name("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib\OBJ_CYGWIN_RTS6740_LIB\\pow.asm:$C$L7:1:1224750862")
	.dwattr $C$DW$41, DW_AT_TI_begin_file("MATH\powf_i.h")
	.dwattr $C$DW$41, DW_AT_TI_begin_line(0x12c)
	.dwattr $C$DW$41, DW_AT_TI_end_line(0x12c)
$C$DW$42	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$42, DW_AT_low_pc($C$DW$L$_pow$28$B)
	.dwattr $C$DW$42, DW_AT_high_pc($C$DW$L$_pow$28$E)
$C$DW$43	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$43, DW_AT_low_pc($C$DW$L$_pow$29$B)
	.dwattr $C$DW$43, DW_AT_high_pc($C$DW$L$_pow$29$E)
	.dwendtag $C$DW$41


$C$DW$44	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$44, DW_AT_name("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib\OBJ_CYGWIN_RTS6740_LIB\\pow.asm:$C$L2:1:1224750862")
	.dwattr $C$DW$44, DW_AT_TI_begin_file("MATH\powf_i.h")
	.dwattr $C$DW$44, DW_AT_TI_begin_line(0xff)
	.dwattr $C$DW$44, DW_AT_TI_end_line(0xff)
$C$DW$45	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$45, DW_AT_low_pc($C$DW$L$_pow$9$B)
	.dwattr $C$DW$45, DW_AT_high_pc($C$DW$L$_pow$9$E)
$C$DW$46	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$46, DW_AT_low_pc($C$DW$L$_pow$10$B)
	.dwattr $C$DW$46, DW_AT_high_pc($C$DW$L$_pow$10$E)
	.dwendtag $C$DW$44

	.dwattr $C$DW$15, DW_AT_TI_end_file("MATH/pow.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x0d)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$15

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_ldexp
	.global	_frexp
	.global	_modf
	.global	__nround
	.global	__trunc
	.global	_errno
	.global	__divd

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int8_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int_least8_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least8_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("int_least16_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least16_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x20)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("int_least32_t")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast8_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast16_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast32_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("intptr_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least32_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast8_t")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast16_t")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast32_t")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("int40_t")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("int_least40_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast40_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("uint40_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least40_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast40_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("int64_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("int_least64_t")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast64_t")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("intmax_t")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("mantissa_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least64_t")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$T$58	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast64_t")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("uintmax_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$60	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x20)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$19	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$47, DW_AT_name("mantissa")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$47, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x17)
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$48, DW_AT_name("exp")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$48, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x08)
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$49, DW_AT_name("sign")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$49, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$19


$C$DW$T$20	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$50, DW_AT_name("f")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$51, DW_AT_name("fp_format")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("FLOAT2FORM")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$52, DW_AT_name("mantissa1")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_mantissa1")
	.dwattr $C$DW$52, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$53, DW_AT_name("mantissa0")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_mantissa0")
	.dwattr $C$DW$53, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x14)
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$54, DW_AT_name("exp")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$54, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$55, DW_AT_name("sign")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$55, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x08)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$56, DW_AT_name("f")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$57, DW_AT_name("fp_format")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("DOUBLE2FORM")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x10)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$58, DW_AT_name("sign")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$59, DW_AT_name("exp")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$60, DW_AT_name("mantissa")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("realnum")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg0]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg1]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg2]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg3]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg4]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg5]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg6]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg7]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg8]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg9]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg10]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg11]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg12]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg13]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg14]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg15]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg16]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg17]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg18]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg19]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg20]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg21]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg22]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg23]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg24]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg25]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg26]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg27]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg28]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg29]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg30]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg31]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x20]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x21]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x22]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x23]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x24]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x25]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x26]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x27]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x28]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x29]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x30]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x31]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x32]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x33]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x34]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x35]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x36]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x37]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x38]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x39]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x40]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x41]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x42]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x43]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x44]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x45]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x46]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x47]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x48]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x49]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x50]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x51]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x52]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x53]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x54]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x55]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x56]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x57]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x58]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x59]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x60]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x61]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x62]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x63]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x64]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x65]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x66]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x67]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x68]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x69]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x70]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x71]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x72]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x73]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x74]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x75]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x76]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x77]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x78]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x79]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

