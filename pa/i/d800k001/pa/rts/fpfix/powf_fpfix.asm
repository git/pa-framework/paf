;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v6.1.5 *
;* Date/Time created: Thu Oct 23 14:04:22 2008                                *
;******************************************************************************
	.compiler_opts --c64p_l1d_workaround=off --endian=little --hll_source=on --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --quiet --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("MATH/powf.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v6.1.5 Copyright (c) 1996-2008 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib")
;*****************************************************************************
;* CINIT RECORDS                                                             *
;*****************************************************************************
	.sect	".cinit"
	.align	8
	.field  	$C$IR_1,32
	.field  	_A1$1+0,32
	.word	03f800000h		; _A1$1[0] @ 0
	.word	03f75257dh		; _A1$1[1] @ 32
	.word	03f6ac0c7h		; _A1$1[2] @ 64
	.word	03f60ccdfh		; _A1$1[3] @ 96
	.word	03f5744fdh		; _A1$1[4] @ 128
	.word	03f4e248ch		; _A1$1[5] @ 160
	.word	03f45672ah		; _A1$1[6] @ 192
	.word	03f3d08a4h		; _A1$1[7] @ 224
	.word	03f3504f3h		; _A1$1[8] @ 256
	.word	03f2d583fh		; _A1$1[9] @ 288
	.word	03f25fed7h		; _A1$1[10] @ 320
	.word	03f1ef532h		; _A1$1[11] @ 352
	.word	03f1837f0h		; _A1$1[12] @ 384
	.word	03f11c3d3h		; _A1$1[13] @ 416
	.word	03f0b95c2h		; _A1$1[14] @ 448
	.word	03f05aac3h		; _A1$1[15] @ 480
	.word	03f000000h		; _A1$1[16] @ 512
$C$IR_1:	.set	68

	.sect	".cinit"
	.align	8
	.field  	$C$IR_2,32
	.field  	_A2$2+0,32
	.word	031a92436h		; _A2$2[0] @ 0
	.word	0b19eab59h		; _A2$2[1] @ 32
	.word	031a8fc24h		; _A2$2[2] @ 64
	.word	0b2c14fe8h		; _A2$2[3] @ 96
	.word	0b1adeaf6h		; _A2$2[4] @ 128
	.word	032c12342h		; _A2$2[5] @ 160
	.word	032e75624h		; _A2$2[6] @ 192
	.word	032cf9891h		; _A2$2[7] @ 224
$C$IR_2:	.set	32

	.sect	".cinit"
	.align	8
	.field  	$C$IR_3,32
	.field  	_P$3+0,32
	.word	03daab75ch		; _P$3[0] @ 0
$C$IR_3:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_4,32
	.field  	_Q$4+0,32
	.word	03d5ea8feh		; _Q$4[0] @ 0
	.word	03e75f315h		; _Q$4[1] @ 32
	.word	03f317211h		; _Q$4[2] @ 64
$C$IR_4:	.set	12


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ldexpf")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ldexpf")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$16)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("frexpf")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_frexpf")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$16)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$4


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("modff")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_modff")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$16)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$64)
	.dwendtag $C$DW$7


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("_roundf")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("__roundf")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("_truncf")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("__truncf")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$12

$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("errno")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_errno")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
_A1$1:	.usect	".far",68,8
_A2$2:	.usect	".far",32,8
_P$3:	.usect	".far",4,8
_Q$4:	.usect	".far",12,8
;	c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\bin\opt6x.exe OBJ_CYGWIN_RTS6740_LIB\\powf.if OBJ_CYGWIN_RTS6740_LIB\\powf.opt 
	.sect	".text:_powf"
	.clink
	.global	_powf

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("powf")
	.dwattr $C$DW$15, DW_AT_low_pc(_powf)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_powf")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$15, DW_AT_TI_begin_file("MATH/powf.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x09)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$15, DW_AT_frame_base[DW_OP_breg31 64]
	.dwattr $C$DW$15, DW_AT_TI_skeletal
	.dwattr $C$DW$15, DW_AT_TI_category("TI Library")
	.dwpsn	file "MATH/powf.c",line 10,column 1,is_stmt,address _powf
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg4]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("y")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: powf                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Local Frame Size  : 0 Args + 8 Auto + 56 Save = 64 byte                  *
;******************************************************************************
_powf:
;** --------------------------------------------------------------------------*

           STW     .D2T1   A11,*SP--(8)      ; |10| 
||         ZERO    .L1     A11               ; |208| 

           STW     .D2T1   A10,*SP--(8)      ; |10| 
||         MV      .L1     A4,A10            ; |10| 

           STDW    .D2T2   B13:B12,*SP--     ; |10| 
||         CMPGTSP .S1     A10,A11,A0        ; |208| 

           STDW    .D2T2   B11:B10,*SP--     ; |10| 
||         MV      .L2     B4,B10            ; |10| 
||         CMPLTSP .S1     A10,A11,A1        ; |210| 

           STDW    .D2T1   A15:A14,*SP--     ; |10| 
|| [!A0]   B       .S1     $C$L6             ; |208| 

           STDW    .D2T1   A13:A12,*SP--     ; |10| 
||         ZERO    .L1     A12

           SET     .S1     A12,0x17,0x1d,A12
||         STW     .D2T2   B3,*SP--(16)      ; |10| 

           CMPEQSP .S1     A10,A12,A3        ; |226| 
           CMPEQSP .S2X    B10,A12,B4        ; |226| 
           OR      .L2X    B4,A3,B0          ; |226| 
           ; BRANCHCC OCCURS {$C$L6}         ; |208| 
;** --------------------------------------------------------------------------*
   [ B0]   B       .S1     $C$L7             ; |226| 
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("_frexpf")
	.dwattr $C$DW$18, DW_AT_TI_call
   [!B0]   CALL    .S1     _frexpf           ; |230| 
           ADD     .L2     4,SP,B4           ; |230| 
           ZERO    .L2     B11
           SET     .S2     B11,0x18,0x1d,B11
           NOP             1
           ; BRANCHCC OCCURS {$C$L7}         ; |226| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL0,B3,0       ; |230| 
$C$RL0:    ; CALL OCCURS {_frexpf} {0}       ; |230| 
;** --------------------------------------------------------------------------*
           SPTRUNC .L2     B10,B5            ; |236| 
           LDW     .D2T2   *+SP(4),B13       ; |236| 
           MV      .L1     A4,A3             ; |230| 
           CMPEQSP .S1X    A3,B11,A0         ; |233| 
           INTSP   .L2     B5,B6             ; |236| 
   [ A0]   MV      .L1     A12,A4            ; |237| 
   [ A0]   SUB     .L2     B13,1,B4          ; |237| 
   [ A0]   MPY32   .M2     B5,B4,B4          ; |237| 
           CMPEQSP .S2     B6,B10,B0         ; |236| 
   [!A0]   MV      .L2X    A11,B0            ; |236| 
   [!B0]   MVKL    .S1     _A1$1+32,A4

   [!B0]   MVKH    .S1     _A1$1+32,A4
|| [!B0]   B       .S2     $C$L1             ; |236| 

$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_ldexpf")
	.dwattr $C$DW$19, DW_AT_TI_call

   [ B0]   CALL    .S1     _ldexpf           ; |237| 
|| [!B0]   LDW     .D1T2   *A4,B4            ; |240| 

   [!B0]   MVK     .S2     32,B5
           MV      .L2     B0,B1             ; guard predicate rewrite
   [!B1]   MVK     .L2     0x1,B11           ; |240| 
           NOP             1
           ; BRANCHCC OCCURS {$C$L1}         ; |236| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL1,B3,0       ; |237| 
$C$RL1:    ; CALL OCCURS {_ldexpf} {0}       ; |237| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x04)
	.dwattr $C$DW$20, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |12| 
||         RET     .S2     B3                ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L1:    
           CMPGTSP .S2X    A3,B4,B0          ; |240| 

   [!B0]   MVK     .L2     0x9,B11           ; |241| 
||         SUB     .S2X    A4,B5,B12

           ADDAW   .D2     B12,B11,B4        ; |242| 
           LDW     .D2T2   *+B4(12),B4       ; |242| 
           MVKL    .S1     _A2$2-4,A5
           MVKH    .S1     _A2$2-4,A5
           NOP             2

           CMPGTSP .S2X    A3,B4,B0          ; |242| 
||         ADDAW   .D2     B12,B11,B4        ; |242| 

   [!B0]   ADDK    .S2     16,B4             ; |242| 
           LDW     .D2T2   *+B4(4),B5        ; |243| 
   [!B0]   ADD     .L2     4,B11,B11         ; |242| 
           NOP             3
           CMPGTSP .S2X    A3,B5,B0          ; |243| 

   [!B0]   ADD     .L2     2,B11,B11         ; |243| 
|| [!B0]   ADD     .S2     8,B4,B4           ; |243| 

           ADD     .L2     1,B11,B5          ; |259| 
||         LDW     .D2T2   *B4,B4            ; |259| 

           SHRU    .S2     B5,31,B0          ; |259| 
||         MVK     .L2     0x2,B5            ; |259| 

   [!B0]   MV      .L2X    A11,B5            ; |259| 
           ADDAH   .D2     B5,B11,B5         ; |259| 
           ADD     .L2     2,B5,B5           ; |259| 
           NOP             1
           SHR     .S1X    B5,2,A4           ; |259| 
           LDW     .D1T1   *+A5[A4],A4       ; |259| 
           SUBSP   .L1X    A3,B4,A5          ; |259| 
           ADDSP   .L2X    B4,A3,B4          ; |259| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("__divf")
	.dwattr $C$DW$21, DW_AT_TI_call
           CALL    .S1     __divf            ; |259| 
           ADDKPC  .S2     $C$RL2,B3,0       ; |259| 
           SUBSP   .L1     A5,A4,A4          ; |259| 
           NOP             3
$C$RL2:    ; CALL OCCURS {__divf} {0}        ; |259| 
;** --------------------------------------------------------------------------*

           ADDSP   .L1     A4,A4,A3          ; |259| 
||         MVKL    .S2     _P$3,B4

           ZERO    .L2     B5
           MVKH    .S2     _P$3,B4
           LDW     .D2T2   *B4,B4            ; |259| 
           MPYSP   .M1     A3,A3,A4          ; |259| 
           ZERO    .L1     A15
           MVKL    .S1     0x3ee2a8ed,A5
           ZERO    .L1     A10
           MPYSP   .M1X    A4,B4,A4          ; |259| 
           MVKH    .S1     0x3ee2a8ed,A5
           SHL     .S2     B13,4,B31         ; |260| 
           MVKH    .S1     0x4d000000,A15
           MPYSP   .M1     A3,A4,A4          ; |259| 
           MVKH    .S1     0x3d800000,A10
           MVKH    .S2     0xcd000000,B5
           CMPLTSP .S2X    B10,A15,B30       ; |261| 
           MPYSP   .M1     A5,A4,A6          ; |259| 
           CMPLTSP .S2     B10,B5,B5         ; |261| 
           MPYSP   .M1     A5,A3,A31         ; |259| 
           SUB     .L2     B31,B11,B4        ; |260| 
           ADDSP   .L1     A6,A4,A4          ; |259| 
           ZERO    .L2     B11
           INTSP   .L2     B4,B6             ; |260| 
           MVKH    .S2     0x41800000,B11
           ADDSP   .L1     A31,A4,A4         ; |259| 
           XOR     .S2     1,B30,B4          ; |261| 
           OR      .L2     B5,B4,B0          ; |261| 
           MVK     .L2     0x4,B4            ; |261| 

           ADDSP   .L1     A3,A4,A3          ; |259| 
|| [!B0]   B       .S1     $C$L2             ; |261| 

$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_ldexpf")
	.dwattr $C$DW$22, DW_AT_TI_call
   [ B0]   CALL    .S1     _ldexpf           ; |261| 
           MPYSP   .M1X    A10,B6,A14        ; |260| 
           MV      .S1X    B10,A4            ; |261| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("__roundf")
	.dwattr $C$DW$23, DW_AT_TI_call
   [!B0]   CALL    .S1     __roundf          ; |261| 

           MV      .L2X    A3,B13            ; |259| Define a twin register
|| [!B0]   MV      .L1X    B11,A3            ; |261| Register A/B partition copy

           ; BRANCHCC OCCURS {$C$L2}         ; |261| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL3,B3,0       ; |261| 
$C$RL3:    ; CALL OCCURS {_ldexpf} {0}       ; |261| 
;** --------------------------------------------------------------------------*
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("__roundf")
	.dwattr $C$DW$24, DW_AT_TI_call
           CALLP   .S2     __roundf,B3
$C$RL4:    ; CALL OCCURS {__roundf} {0}      ; |261| 
;** --------------------------------------------------------------------------*

           BNOP    .S1     $C$L3,3           ; |261| 
||         MPYSP   .M1     A10,A4,A13        ; |261| 

           SUBSP   .L1X    B10,A13,A3        ; |263| 
           MPYSP   .M2     B10,B13,B4        ; |263| 
           ; BRANCH OCCURS {$C$L3}           ; |261| 
;** --------------------------------------------------------------------------*
$C$L2:    
           MPYSP   .M1X    A3,B10,A4         ; |261| 
           ADDKPC  .S2     $C$RL5,B3,2       ; |261| 
$C$RL5:    ; CALL OCCURS {__roundf} {0}      ; |261| 
;** --------------------------------------------------------------------------*
           MPYSP   .M1     A10,A4,A13        ; |261| 
           MPYSP   .M2     B10,B13,B4        ; |263| 
           NOP             2
           SUBSP   .L1X    B10,A13,A3        ; |263| 
           NOP             1
;** --------------------------------------------------------------------------*
$C$L3:    
           NOP             2
           MPYSP   .M1     A14,A3,A3         ; |263| 
           NOP             3

           ADDSP   .L1X    A3,B4,A12         ; |263| 
||         ZERO    .S1     A3

           MVKH    .S1     0xcd000000,A3
           NOP             1
           MV      .L2X    A3,B4
           CMPLTSP .S1     A12,A15,A3        ; |269| 
           XOR     .L1     1,A3,A3           ; |269| 
           CMPLTSP .S2X    A12,B4,B4         ; |269| 
           OR      .L2X    B4,A3,B0          ; |269| 
   [!B0]   B       .S1     $C$L4             ; |269| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_ldexpf")
	.dwattr $C$DW$25, DW_AT_TI_call
   [ B0]   CALL    .S1     _ldexpf           ; |269| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("__roundf")
	.dwattr $C$DW$26, DW_AT_TI_call
   [!B0]   CALL    .S1     __roundf          ; |269| 
           MV      .L1     A12,A4            ; |269| 
   [!B0]   MPYSP   .M1X    B11,A12,A4        ; |269| 
           MVK     .L2     0x4,B4            ; |269| 
           ; BRANCHCC OCCURS {$C$L4}         ; |269| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL6,B3,0       ; |269| 
$C$RL6:    ; CALL OCCURS {_ldexpf} {0}       ; |269| 
;** --------------------------------------------------------------------------*
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("__roundf")
	.dwattr $C$DW$27, DW_AT_TI_call
           CALLP   .S2     __roundf,B3
$C$RL7:    ; CALL OCCURS {__roundf} {0}      ; |269| 
;** --------------------------------------------------------------------------*

           MPYSP   .M1     A10,A4,A3         ; |269| 
||         MVKL    .S2     0x44b14000,B4
||         B       .S1     $C$L5             ; |269| 
||         MVK     .L2     2,B5              ; |280| 

           MPYSP   .M1     A13,A14,A4        ; |276| 
||         MVKH    .S2     0x44b14000,B4
||         MVKL    .S1     0xc4ad4000,A5

           MVKL    .S2     _errno,B6
||         MVKH    .S1     0xc4ad4000,A5

           MVKH    .S2     _errno,B6
           SUBSP   .L1     A12,A3,A12        ; |271| 

           ADDSP   .L1     A4,A3,A13         ; |276| 
||         MVKL    .S1     0x7f7fffff,A4

           ; BRANCH OCCURS {$C$L5}           ; |269| 
;** --------------------------------------------------------------------------*
$C$L4:    
           ADDKPC  .S2     $C$RL8,B3,1       ; |269| 
$C$RL8:    ; CALL OCCURS {__roundf} {0}      ; |269| 
;** --------------------------------------------------------------------------*
           MPYSP   .M1     A10,A4,A3         ; |269| 
           MPYSP   .M1     A13,A14,A4        ; |276| 
           MVKL    .S2     0x44b14000,B4

           MVKL    .S2     _errno,B6
||         MVKL    .S1     0xc4ad4000,A5

           SUBSP   .L1     A12,A3,A12        ; |271| 
||         MVKH    .S1     0xc4ad4000,A5
||         MVKH    .S2     0x44b14000,B4

           MVKL    .S1     0x7f7fffff,A4
||         ADDSP   .L1     A4,A3,A13         ; |276| 
||         MVKH    .S2     _errno,B6
||         MVK     .L2     2,B5              ; |280| 

;** --------------------------------------------------------------------------*
$C$L5:    
           MVKH    .S1     0x7f7fffff,A4
           NOP             2
           CMPLTSP .S1     A13,A5,A0         ; |282| 
           CMPGTSP .S2X    A13,B4,B0         ; |278| 

   [ B0]   ZERO    .L1     A0                ; |284| nullify predicate
|| [ B0]   B       .S1     $C$L11            ; |280| 
||         MV      .L2X    A4,B4             ; |280| 
|| [!B0]   MPYSP   .M1X    B11,A13,A4        ; |284| 
|| [ B0]   STW     .D2T2   B5,*B6            ; |280| 

   [ A0]   BNOP    .S1     $C$L12,4          ; |282| 
|| [!B0]   MV      .L2X    A11,B4            ; |282| 

           ; BRANCHCC OCCURS {$C$L11}        ; |280| 
;** --------------------------------------------------------------------------*
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("__roundf")
	.dwattr $C$DW$28, DW_AT_TI_call

   [!A0]   CALL    .S1     __roundf          ; |284| 
|| [ A0]   LDW     .D2T2   *++SP(16),B3      ; |12| 

           ; BRANCHCC OCCURS {$C$L12}        ; |282| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL9,B3,4       ; |284| 
$C$RL9:    ; CALL OCCURS {__roundf} {0}      ; |284| 
;** --------------------------------------------------------------------------*
           MPYSP   .M1     A10,A4,A14        ; |284| 
           NOP             3
           SUBSP   .L1     A13,A14,A3        ; |285| 
           NOP             3
           ADDSP   .L1     A3,A12,A12        ; |285| 
           NOP             3
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("__roundf")
	.dwattr $C$DW$29, DW_AT_TI_call

           CALLP   .S2     __roundf,B3
||         MPYSP   .M1X    B11,A12,A4        ; |286| 

$C$RL10:   ; CALL OCCURS {__roundf} {0}      ; |286| 
           MPYSP   .M1     A10,A4,A3         ; |286| 
           MVKL    .S2     _Q$4+4,B6
           MVKH    .S2     _Q$4+4,B6
           ADD     .L2     -4,B6,B4
           SUBSP   .L1     A12,A3,A12        ; |288| 
           ADDSP   .L1     A3,A14,A3         ; |287| 
           LDW     .D2T2   *B4,B4            ; |305| 
           LDNDW   .D2T2   *B6,B9:B8         ; |300| 
           CMPGTSP .S1     A12,A11,A0        ; |290| 
   [ A0]   SUBSP   .L1     A12,A10,A12       ; |292| 
           MPYSP   .M2X    B11,A3,B5         ; |287| 
           NOP             3
           MPYSP   .M2X    A12,B4,B6         ; |305| 
           SPTRUNC .L2     B5,B4             ; |287| 
           NOP             2
           ADDSP   .L2     B8,B6,B5          ; |305| 
   [ A0]   ADD     .L2     1,B4,B4           ; |293| 
           SHR     .S2     B4,3,B6           ; |297| 
           SHRU    .S2     B6,26,B8          ; |305| 
           MPYSP   .M2X    A12,B5,B5         ; |305| 
           AND     .L2     -4,B8,B8          ; |305| 
           ADDAW   .D2     B8,B4,B16         ; |305| 

           CMPLT   .L2     B4,0,B7           ; |297| 
||         SHR     .S2     B16,6,B16         ; |305| 

           XOR     .D2     1,B7,B7           ; |297| 
||         ADDSP   .L2     B9,B5,B5          ; |305| 
||         SHL     .S2     B16,6,B16         ; |305| 

           SHL     .S2     B7,6,B8           ; |305| 
           ADD     .L2     B16,B8,B8         ; |305| 
           SUBAW   .D2     B8,B4,B8          ; |305| 
           ADD     .L2     B12,B8,B8         ; |305| 
           LDW     .D2T2   *B8,B8            ; |305| 
           MPYSP   .M1X    A12,B5,A3         ; |305| 
           SHRU    .S1X    B6,28,A4          ; |297| 
           ADD     .L1X    B4,A4,A4          ; |297| 
           SHR     .S1     A4,4,A5           ; |297| 
           MPYSP   .M1X    B8,A3,A3          ; |305| 
           ADD     .L2X    B7,A5,B4          ; |297| 
           STW     .D2T2   B4,*+SP(4)        ; |297| 
           NOP             1
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_ldexpf")
	.dwattr $C$DW$30, DW_AT_TI_call

           CALLP   .S2     _ldexpf,B3
||         ADDSP   .L1X    A3,B8,A4          ; |305| 

$C$RL11:   ; CALL OCCURS {_ldexpf} {0}       ; |305| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x04)
	.dwattr $C$DW$31, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |12| 
||         RET     .S2     B3                ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L6:    

           CMPLTSP .S2X    B10,A11,B0        ; |221| 
|| [ A1]   B       .S1     $C$L8             ; |210| 

$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("__truncf")
	.dwattr $C$DW$32, DW_AT_TI_call
   [ A1]   CALL    .S1     __truncf          ; |212| 
   [!A1]   MVK     .L1     1,A3              ; |221| 
           MV      .L1X    B10,A4            ; |212| 
   [!A1]   MVKL    .S1     _errno,A4
   [!A1]   MVKH    .S1     _errno,A4
           ; BRANCHCC OCCURS {$C$L8}         ; |210| 
;** --------------------------------------------------------------------------*

   [ B0]   B       .S1     $C$L11            ; |221| 
||         MVKL    .S2     0xff7fffff,B4
|| [ B0]   STW     .D1T1   A3,*A4            ; |221| 

           MVKH    .S2     0xff7fffff,B4
           CMPEQSP .S2X    B10,A11,B1        ; |222| 

   [ B0]   ZERO    .L2     B1                ; nullify predicate
|| [!B0]   MV      .S2X    A12,B4

   [ B1]   BNOP    .S1     $C$L13,1          ; |222| 
           ; BRANCHCC OCCURS {$C$L11}        ; |221| 
;** --------------------------------------------------------------------------*
   [ B1]   LDW     .D2T2   *++SP(16),B3      ; |12| 
   [ B1]   LDDW    .D2T1   *++SP,A13:A12     ; |12| 
   [ B1]   LDDW    .D2T1   *++SP,A15:A14     ; |12| 
   [ B1]   LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           ; BRANCHCC OCCURS {$C$L13}        ; |222| 
;** --------------------------------------------------------------------------*
$C$L7:    
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return

           RET     .S2     B3                ; |12| 
||         MV      .L1     A10,A4            ; |223| 
||         LDW     .D2T1   *++SP(8),A10      ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L8:    
           ADDKPC  .S2     $C$RL12,B3,0      ; |212| 
$C$RL12:   ; CALL OCCURS {__truncf} {0}      ; |212| 
;** --------------------------------------------------------------------------*

           CMPEQSP .S2X    A4,B10,B0         ; |212| 
||         ZERO    .L2     B4
||         ZERO    .L1     A3
||         MV      .S1X    B10,A4            ; |212| 
||         MVK     .D2     1,B5              ; |212| 

   [ B0]   B       .S1     $C$L9             ; |212| 
||         MVKH    .S2     0xcf000000,B4

           CMPLTSP .S2     B10,B4,B4         ; |214| 
||         MVKH    .S1     0x4f000000,A3

$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_powf")
	.dwattr $C$DW$34, DW_AT_TI_call

   [ B0]   CALL    .S2     _powf             ; |213| 
||         XOR     .L2     1,B4,B4           ; |214| 
||         CMPLTSP .S1X    B10,A3,A3         ; |214| 

$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("__roundf")
	.dwattr $C$DW$35, DW_AT_TI_call

   [!B0]   CALL    .S1     __roundf          ; |212| 
||         MVKL    .S2     _errno,B6

           ABSSP   .S1     A10,A3            ; |213| 
||         MVKH    .S2     _errno,B6
||         AND     .L1X    A3,B4,A12         ; |214| 

           MV      .L2     B10,B4            ; |213| 
           ; BRANCHCC OCCURS {$C$L9}         ; |212| 
;** --------------------------------------------------------------------------*
           STW     .D2T2   B5,*B6            ; |212| 
           ADDKPC  .S2     $C$RL13,B3,1      ; |212| 
$C$RL13:   ; CALL OCCURS {__roundf} {0}      ; |212| 
;** --------------------------------------------------------------------------*
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_powf")
	.dwattr $C$DW$36, DW_AT_TI_call

           CALLP   .S2     _powf,B3
||         MV      .L2X    A4,B4             ; |212| 
||         MV      .L1     A10,A4            ; |212| 

$C$RL14:   ; CALL OCCURS {_powf} {0}         ; |212| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *++SP(16),B3      ; |12| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x04)
	.dwattr $C$DW$37, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |12| 
||         RET     .S2     B3                ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L9:    
           MV      .L1     A3,A4             ; |213| 
           ADDKPC  .S2     $C$RL15,B3,0      ; |213| 
$C$RL15:   ; CALL OCCURS {_powf} {0}         ; |213| 
;** --------------------------------------------------------------------------*

           ZERO    .S2     B4
||         MV      .L1     A12,A0            ; |217| 
||         SPTRUNC .L2     B10,B31           ; |215| 
||         MV      .S1     A4,A10            ; |213| 

           SET     .S2     B4,0x18,0x1d,B4
|| [ A0]   MV      .L1     A11,A3

           MPYSP   .M2     B4,B10,B5         ; |217| 
|| [ A0]   B       .S1     $C$L10            ; |214| 

$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_modff")
	.dwattr $C$DW$38, DW_AT_TI_call
   [!A0]   CALL    .S1     _modff            ; |217| 
   [ A0]   SET     .S1     A3,31,31,A3       ; |215| 
           ADD     .L2     8,SP,B4           ; |217| 
           AND     .L2     1,B31,B0          ; |215| 
           MV      .L1X    B5,A4             ; |217| 
           ; BRANCHCC OCCURS {$C$L10}        ; |214| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL16,B3,0      ; |217| 
$C$RL16:   ; CALL OCCURS {_modff} {0}        ; |217| 
;** --------------------------------------------------------------------------*

           LDW     .D2T2   *++SP(16),B3      ; |12| 
||         MV      .L1     A11,A3            ; |217| 

           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
||         SET     .S1     A3,31,31,A3       ; |217| 

           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
||         XOR     .L1     A10,A3,A3         ; |217| 

           LDDW    .D2T2   *++SP,B11:B10     ; |12| 

           MV      .L2X    A3,B4             ; |217| 
||         LDDW    .D2T2   *++SP,B13:B12     ; |12| 
||         CMPEQSP .S1     A4,A11,A0         ; |217| 

$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x08)
	.dwattr $C$DW$39, DW_AT_TI_return

   [ A0]   MV      .L2X    A10,B4            ; |219| 
||         LDW     .D2T1   *++SP(8),A10      ; |12| 
||         RET     .S2     B3                ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           MV      .L1X    B4,A4             ; |219| 
           NOP             3
           ; BRANCH OCCURS {B3}              ; |12| 
;** --------------------------------------------------------------------------*
$C$L10:    
   [ B0]   XOR     .L1     A10,A3,A10        ; |215| 
           NOP             1
           MV      .L2X    A10,B4            ; |219| 
;** --------------------------------------------------------------------------*
$C$L11:    
           LDW     .D2T2   *++SP(16),B3      ; |12| 
;** --------------------------------------------------------------------------*
$C$L12:    
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           LDDW    .D2T1   *++SP,A15:A14     ; |12| 
           LDDW    .D2T2   *++SP,B11:B10     ; |12| 
;** --------------------------------------------------------------------------*
$C$L13:    
           LDDW    .D2T2   *++SP,B13:B12     ; |12| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return

           RET     .S2     B3                ; |12| 
||         LDW     .D2T1   *++SP(8),A10      ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
           MV      .L1X    B4,A4             ; |219| 
	.dwpsn	file "MATH/powf.c",line 12,column 1,is_stmt
           NOP             3
           ; BRANCH OCCURS {B3}              ; |12| 
	.dwattr $C$DW$15, DW_AT_TI_end_file("MATH/powf.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x0c)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$15

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_ldexpf
	.global	_frexpf
	.global	_modff
	.global	__roundf
	.global	__truncf
	.global	_errno
	.global	__divf

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("int8_t")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int_least8_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least8_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("int_least16_t")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least16_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$34	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$34, DW_AT_address_class(0x20)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("int_least32_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast8_t")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast16_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast32_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("intptr_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("mantissa_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least32_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast8_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast16_t")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast32_t")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("int40_t")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("int_least40_t")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast40_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("uint40_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least40_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast40_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("int64_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("int_least64_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast64_t")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("intmax_t")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least64_t")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$T$58	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast64_t")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("uintmax_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$64	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$64, DW_AT_address_class(0x20)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$19	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$41, DW_AT_name("mantissa")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$41, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x17)
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$42, DW_AT_name("exp")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$42, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x08)
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$43, DW_AT_name("sign")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$43, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$19


$C$DW$T$20	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$44, DW_AT_name("f")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$45, DW_AT_name("fp_format")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("FLOAT2FORM")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$46, DW_AT_name("mantissa1")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_mantissa1")
	.dwattr $C$DW$46, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$47, DW_AT_name("mantissa0")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_mantissa0")
	.dwattr $C$DW$47, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x14)
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$48, DW_AT_name("exp")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$48, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$49, DW_AT_name("sign")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$49, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x08)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$50, DW_AT_name("f")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$51, DW_AT_name("fp_format")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("DOUBLE2FORM")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x0c)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$52, DW_AT_name("sign")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$53, DW_AT_name("exp")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$54, DW_AT_name("mantissa")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("realnum")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg0]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg1]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg2]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg3]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg4]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg5]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg6]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg7]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg8]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg9]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg10]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg11]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg12]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg13]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg14]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg15]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg16]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg17]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg18]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg19]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg20]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg21]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg22]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg23]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg24]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg25]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg26]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg27]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg28]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg29]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg30]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg31]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x20]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x21]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x22]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x23]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x24]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x25]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x26]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x27]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x28]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x29]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x30]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x31]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x32]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x33]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x34]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x35]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x36]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x37]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x38]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x39]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x40]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x41]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x42]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x43]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x44]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x45]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x46]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x47]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x48]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x49]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x50]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x51]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x52]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x53]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x54]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x55]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x56]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x57]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x58]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x59]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x60]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x61]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x62]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x63]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x64]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x65]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x66]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x67]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x68]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x69]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x70]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x71]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x72]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x73]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x74]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x75]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x76]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x77]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x78]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x79]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

