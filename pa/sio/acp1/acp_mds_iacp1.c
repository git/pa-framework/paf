
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Alpha Code Processor Algorithm functionality implementation
// Code in this file is used for UART only.
// 
//
//




/*
 *  ACP Module implementation - MDS implementation of a ACP Algorithm.
 */ 

#include <std.h>
#include <mem.h> /* for MEM_alloc */

#include <iacp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>
#include <acperr.h>

#include "acptype.h"

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include <logp.h>
#define ENABLE_ALPHA_ERROR_TRACE
#ifdef ENABLE_ALPHA_ERROR_TRACE
 #define AE_TRACE(a) LOG_printf a
#else
 #define AE_TRACE(a)
#endif
/*
 *  
 *  MDS's implementation of the sprintf operation.
 */

static const char ascii_tab[16]={0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,
								 0x38,0x39,0x41,0x42,0x43,0x44,0x45,0x46};
void ACP_MDS_sprintf1(IACP_Handle handle , char *p, int arg1, int arg2)
{
	*p++ = 'S';
	*p++ = '1';
	*p++ = ascii_tab[(arg1 >> 4) & 0xf];
	*p++ = ascii_tab[arg1 & 0xf];
	*p++ = ascii_tab[(arg2 >> 12) & 0xf];
    *p++ = ascii_tab[(arg2 >> 8) & 0xf];
    *p++ = ascii_tab[(arg2 >> 4) & 0xf];
	*p++ = ascii_tab[arg2 & 0xf];
	*p = '\0'; 
}  

void ACP_MDS_sprintf2(IACP_Handle handle ,char *p, int arg1, int arg2)
{
	*p++ = ascii_tab[(arg1 >> 4) & 0xf];
	*p++ = ascii_tab[arg1 & 0xf]; 
	*p++ = ascii_tab[(arg2 >> 4) & 0xf];
	*p++ = ascii_tab[arg2 & 0xf];
	*p = '\0'; 
}

void ACP_MDS_sprintf3(IACP_Handle handle ,char *p, int arg1)
{
	*p++ = ascii_tab[(arg1 >> 4) & 0xf];
	*p++ = ascii_tab[arg1 & 0xf];
	*p++ = '\n'; 
	*p = '\0'; 
}


/*
 *  ======== ACP_MDS_srecord ========
 *  MDS's implementation of the srecord operation.
 */

Uns
ACP_MDS_srecordByte (const char data[]) 
{
    if (! isxdigit (data[0]) || ! isxdigit (data[1]))
        return 0;
    else {
        Uns i0 = isdigit (data[0]) ? data[0] - '0' : toupper (data[0]) - 'A' + 10;
        Uns i1 = isdigit (data[1]) ? data[1] - '0' : toupper (data[1]) - 'A' + 10;
        return (i0 << 4) + i1;
    }
}

Uns
ACP_MDS_srecordWord (const char data[]) 
{
    return ACP_MDS_srecordByte (data) + (ACP_MDS_srecordByte (data+2) << 8);
}

Int
ACP_MDS_srecordByteChar (char data[], SmInt unit)
{
    Uns nib0, nib1;

    nib0 = (unit >>  4) & 0xf;
    nib1 = (unit >>  0) & 0xf;

    data[0] = nib0 > 9 ? nib0 - 10 + 'A' : nib0 + '0';
    data[1] = nib1 > 9 ? nib1 - 10 + 'A' : nib1 + '0';

    return 0;
}

Int
ACP_MDS_srecordWordChar (char data[], ACP_Unit unit)
{
    Uns nib0, nib1, nib2, nib3;

    nib0 = (unit >>  4) & 0xf;
    nib1 = (unit >>  0) & 0xf;
    nib2 = (unit >> 12) & 0xf;
    nib3 = (unit >>  8) & 0xf;

    data[0] = nib0 > 9 ? nib0 - 10 + 'A' : nib0 + '0';
    data[1] = nib1 > 9 ? nib1 - 10 + 'A' : nib1 + '0';
    data[2] = nib2 > 9 ? nib2 - 10 + 'A' : nib2 + '0';
    data[3] = nib3 > 9 ? nib3 - 10 + 'A' : nib3 + '0';

    return 0;
}

Uns
ACP_MDS_srecordChecksum (Int length, const char data[]) 
{
    Int i;

    Uns sum = 0;

    if (data[2*length-2] == '?' && data[2*length-1] == '?')
        return 0;

    for (i=0; i < length; i++)
        sum += ACP_MDS_srecordByte (data+2*i);

    return ~sum & 0xff;
}

Int
ACP_MDS_srecord (IACP_Handle handle, char *fromText, char *to)
{
    ACP_MDS_Obj *acp = (Void *)handle;

    IACP_SRecordState *pSRS = acp->config.pSRecordState;

    Int fromLength;

    Int tryBytes;

    if (! pSRS)
        return ACPERR_SRECORD_NULL;
    else if (! pSRS->fromSequence)
        return ACPERR_SRECORD_FROMNULL;
    else if (! pSRS->toSequence)
        return ACPERR_SRECORD_TOTONULL;

    if (pSRS->fromBytes < 0) {
    
        fromLength = strlen (pSRS->from = fromText);

        if (pSRS->from[0] != 'S') {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_FORM;
        }
        else if ((tryBytes = ACP_MDS_srecordByte (pSRS->from+2)) * 2 + 4 > fromLength) {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_LENG;
        }
        else if (tryBytes < 3) {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_MIN;
        }
        else if (ACP_MDS_srecordChecksum (tryBytes+1, pSRS->from+2)) {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_CRC;
        }
        else {
            switch (pSRS->from[1]) {
              case '0':
                pSRS->toPlace = 0;
                pSRS->toCount = 0;
                pSRS->fromSequenceCount = 0;
                strcpy (to, pSRS->from);
                return 0;
              case '1':
                pSRS->from += 8;
                tryBytes -= 3;
                break;
              case '2':
                pSRS->from += 10;
                tryBytes -= 4;
                break;
              case '3':
                pSRS->from += 12;
                tryBytes -= 5;
                break;
              case '9':
                strcpy (to, pSRS->from);
                return pSRS->fromSequenceCount == 0 ? 0 : ACPERR_SRECORD_PART;
              default:
                if (to)
                    *to = '\0';
                return ACPERR_SRECORD_TYPE;
            }
        }

        if (tryBytes < 0) {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_MIN;
        }
        if (tryBytes & 1) {
            if (to)
                *to = '\0';
            return ACPERR_SRECORD_ODD;
        }
        else if (tryBytes == 0) {
            if (to)
                *to = '\0';
            return 0;
        }
        pSRS->fromBytes = tryBytes;
    }

    do {
        if (pSRS->toPlace == pSRS->toCount) {
            Int type, length, offset;
            if (pSRS->fromSequenceCount == 0) {
                pSRS->fromSequence[0] = ACP_MDS_srecordWord (pSRS->from);
                pSRS->from += 4;
                pSRS->fromBytes -= 2;
                if ((pSRS->fromSequence[0] & 0xff00) == 0xc900
                    || pSRS->fromSequence[0] == 0xcd01
                    || (pSRS->fromSequence[0] & 0xc000) == 0x0000
                    || (pSRS->fromSequence[0] & 0xc000) == 0x8000)
                    pSRS->fromSequenceCount++;
                continue;
            }
            if (pSRS->fromSequenceCount == pSRS->fromSequenceLength)
                return ACPERR_SRECORD_FROMOVER;
            pSRS->fromSequence[(Uns )pSRS->fromSequenceCount++] = ACP_MDS_srecordWord (pSRS->from);
            pSRS->from += 4;
            pSRS->fromBytes -= 2;
            type = pSRS->fromSequence[0];
            if ((type & 0xff00) == 0xc900) {
                offset = 1;
                length = pSRS->fromSequence[0] & 0xff;
            }
            else if (type == 0xcd01) {
                offset = 2;
                length = (Uns )pSRS->fromSequence[1];
            }
            else if ((type & 0xc000) == 0x0000 || (type & 0xc000) == 0x8000) {
                offset = 2;
                length = ((Uns )pSRS->fromSequence[1] & 0x7fff) + 14;
            }
            else
                return ACPERR_SRECORD_FROMTYPE;
            if ((Uns )pSRS->fromSequenceCount == offset + length) {
                if (((IACP_Obj *)acp)->fxns->sequence(handle, pSRS->fromSequence, pSRS->toSequence)) {
                    pSRS->toCount++;
                    pSRS->toSequence[0] = 0xdead;
                    AE_TRACE((&trace, "ACP_MDS_srecord.%d:  0xdead: alpha error.", __LINE__));
                }
                else {
                    type = pSRS->toSequence[0];
                    if ((type & 0xff00) == 0xc900) {
                        offset = 1;
                        length = pSRS->toSequence[0] & 0xff;
                    }
                    else if (type == 0xcd01) {
                        offset = 2;
                        length = (Uns )pSRS->toSequence[1];
                    }
                    else if ((type & 0xc000) == 0x0000 
                        || (type & 0xc000) == 0x8000) {
                        offset = 2;
                        length = ((Uns )pSRS->toSequence[1] & 0x7fff) + 14;
                    }
                    else
                        return ACPERR_SRECORD_TOTOTYPE;
                    pSRS->toCount += offset + length;
                    // No check of response size? --Kurt
                }
                pSRS->toStart = pSRS->toPlace;
                pSRS->fromSequenceCount = 0;
            }
        }
        if (pSRS->toPlace != pSRS->toCount) {
            Int i, iN;
            iN = pSRS->toCount - pSRS->toPlace < 16 ? pSRS->toCount - pSRS->toPlace : 16;
            if (to)
/*
#ifndef IROM      
                    sprintf (to, "S1%02X%04X", 2*iN+3, 2*pSRS->toPlace);
#else                    
*/
            ((IACP_Obj *)acp)->fxns->sprintf1(handle,to,2*iN+3, 2*pSRS->toPlace);
//#endif            
            for (i=0; i < iN; i++, pSRS->toPlace++) {
                if (to)
/*
#ifndef IROM                
                    sprintf (to+8+4*i, "%02X%02X", 
                        pSRS->toSequence[pSRS->toPlace-pSRS->toStart] & 0xff, 
                        (pSRS->toSequence[pSRS->toPlace-pSRS->toStart] >> 8) & 0xff);
#else                    
*/
                      ((IACP_Obj *)acp)->fxns->sprintf2(handle,to+8+4*i, 
                        pSRS->toSequence[pSRS->toPlace-pSRS->toStart] & 0xff, 
                        (pSRS->toSequence[pSRS->toPlace-pSRS->toStart] >> 8) & 0xff);
//#endif            
            }
            if (to) {
/*
#ifndef IROM            
                sprintf (to+8+4*i, "%02X\n", 
                    ACP_MDS_srecordChecksum (2*iN+3, to+2));
#else                     
*/
                ((IACP_Obj *)acp)->fxns->sprintf3(handle,to+8+4*i,
                    ACP_MDS_srecordChecksum (2*iN+3, to+2));
//#endif                   
            }
            if (pSRS->toPlace != pSRS->toCount)
                return 1;
            else if (pSRS->fromBytes > 0)
                return 1;
            pSRS->fromBytes = -1;
            return 0;
        }
    } while (pSRS->fromBytes > 0);

    if (to)
        *to = '\0';
    pSRS->fromBytes = -1;
    return 0;
}

