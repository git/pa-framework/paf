
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Alpha Code Processor Algorithm interface auxiliary implementation
//
//
//

#include <std.h>
#include <clk.h> 
#include <hwi.h> 
#include <mem.h> 
#include <tsk.h> 

#include <stdlib.h>
#include <iacp.h>
#include <alpha/acp_a.h> /* ACP_OP_* and ACP_EQ_* */
#include "pafhjt.h"

#ifndef _TMS320C6X
#include "c67x_cintrins.h"
#endif

/*
 *  ======== IACP_echo ========
 *  Common implementation of the echo (null) operation.
 *
 *  The use of int in the prototype here is "sloppy," and the use of
 *  void * is weird. This is on purpose to show that such usage will
 *  produce functional code throughout the system. The "correct" types
 *  in both cases should be LgInt here.
 */

void *
IACP_echo(int x)
{
    return (void *)x;
}

/*
 *  ======== IACP_arithmetic ========
 *  Common implementation of the arithmetic operation.
 */

/* For the Field Insert function:
   Return value : The address for the register modified
   Arg x: The address for the register to be modified.
   Arg y: [31:24] top of the bit location of the field
   Arg y: [23:16] length of the field
   Arg y: [15:0] value to be inserted to the field of the register.
*/

LgInt
IACP_arithmetic(LgInt code, LgInt x, LgInt y, LgInt z)
{
    volatile LgInt temp, loc, len, value;
    LgUns gie;

    if( (code & 0x80)==0 ){
      switch (code) {
        case ACP_OP_IDENTITY:
          return x;
        case ACP_OP_ADD:
          return x + y;
        case ACP_OP_SADD:
          return _sadd (x, y);
        case ACP_OP_SADD16:
          return _sadd (x << 16, y << 16) >> 16;
        case ACP_OP_AND:
          return x & y;
        case ACP_OP_ANDNOT:
          return x & ~y;
        case ACP_OP_OR:
          return x | y;
        case ACP_OP_ORNOT:
          return x | ~y;
      }
    }else{
      gie = HWI_disable ();

      switch (code) {
        case ACP_EQ_IDENTITY:
          *(LgInt *)x = y;
          break;
        case ACP_EQ_ADD:
          *(LgInt *)x += y;
          break;
        case ACP_EQ_SADD:
          *(LgInt *)x = _sadd (*(LgInt *)x, y);
          break;
        case ACP_EQ_SADD16:
          *(MdInt *)x = (MdInt )(_sadd ((LgInt )*(MdInt *)x << 16, y << 16) >> 16);
          break;
        case ACP_EQ_AND:
          *(LgInt *)x &= y;
          break;
        case ACP_EQ_ANDNOT:
          *(LgInt *)x &= ~y;
          break;
        case ACP_EQ_OR:
          *(LgInt *)x |= y;
          break;
        case ACP_EQ_ORNOT:
          *(LgInt *)x |= ~y;
          break;

        case ACP_EQ_FLDWRT:
          temp = *(LgInt *)x;
          loc = (y&0xFF000000)>>24;
          len = (y&0xFF0000)>>16;
          value = (y&0xFFFF);
          temp |= value<<(loc-len+1);
          temp &= (((1<<(31-loc))-1)<<(loc+1))|(value<<(loc-len+1))|(((1<<(loc-len+1))-1));
          *(LgInt *)x = temp;
          break;
        }
      HWI_restore (gie);
      return x;
    }
    return 0;
}

/*
 *  ======== IACP_alloc ========
 *  Common implementation of the alloc operation.
 */

void *
IACP_alloc(LgInt heap, LgInt size)
{
    return MEM_alloc(heap, size, 4);
}

/*
 *  ======== IACP_free ========
 *  Common implementation of the free operation.
 */

void *
IACP_free(LgInt heap, void *pntr, LgInt size)
{
    return MEM_free(heap, pntr, size) ? 0 : pntr;
}

/*
 *  ======== IACP_sleep ========
 *  Common implementation of the TSK_sleep operation.
 */

LgInt
IACP_sleep(LgUns milliseconds)
{
    // Convert milliseconds to ticks as per Frank Minich.
    Int nticks = (CLK_countspms() * milliseconds) / CLK_getprd();

    TSK_sleep (nticks);

    return 0;
}

/*
 *  ======== IACP_stat ========
 *  Common implementation of the "stat" (memory statistics) operation.
 */

extern int IRAM, SDRAM;
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
extern int L3RAM;
#endif
LgInt
IACP_stat(Int seg, Int stat)
{
    Int segid = seg == ACP_SEG_SDRAM ? SDRAM : IRAM;
    LgInt retval = -1;    /* in case of MEM_stat() error -- unrecognized "seg" */
    MEM_Stat statbuf;
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
	if( seg == ACP_SEG_L3RAM)
    		segid = L3RAM;
#endif
    if( MEM_stat( segid, &statbuf)) // segid corresponds to valid memory segment?
        retval =
            stat == ACP_STAT_SIZE   ? (LgInt) statbuf.size :
            stat == ACP_STAT_USED   ? (LgInt) statbuf.used :
            stat == ACP_STAT_LENGTH ? (LgInt) statbuf.length :
            stat == ACP_STAT_FREE   ? (LgInt) (statbuf.size - statbuf.used) :
            -2;    /* unrecognized "stat" */

    return retval;
}
