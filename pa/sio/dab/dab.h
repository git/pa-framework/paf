
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Header file for dab SIO driver.
//
//
//

#ifndef DAB_H
#define DAB_H

#include <xdc/std.h>
#include <dev.h>
#include  <std.h>
#include <xdas.h>


#if (PAF_DEVICE&0xFF000000) == 0xD8000000
#include <dab_csl_mcbsp.h>
// HACK: definitions no longer in CSL layer
#define _MCBSP_PORT_CNT 2
#else
#include <csl_edma.h>
#include <csl_mcbsp.h>
// HACK: definitions no longer in CSL layer
#define _MCBSP_PORT_CNT 1
#endif
#include <pafsio.h>



// ..........................................................................
// Global context for the DAB layer

typedef struct DAB_DriverObject
{
    DEV_Handle       pDevice[2*_MCBSP_PORT_CNT];
    CSL_McbspHandle  hMcbsp[_MCBSP_PORT_CNT];       
    Int              configCount[_MCBSP_PORT_CNT];
    SmInt            numDevices;
    SmInt            fifoPresent[_MCBSP_PORT_CNT];
    SmInt            unused;
} DAB_DriverObject;

// .............................................................................
// device configuration structure -- used as argument to SIO_ctrl

typedef struct DAB_Params_
{
    XDAS_Int16  numChannels; 
    XDAS_Int16  mcbspWordSize;  // in bytes, 2 or 4 supported
} DAB_Params_;

typedef struct DAB_Params
{
    Int                 size;   // Type-specific size
    struct DXX_Params_  sio;    // Common parameters
    struct DAB_Params_  dab;    // Driver parameters
} DAB_Params;

// .............................................................................
//Function table defs

typedef Int	(*DAB_Tshutdown)(DEV_Handle);
typedef Int	(*DAB_Tstart)(DEV_Handle);
typedef Int	(*DAB_Tconfig)(DEV_Handle, const DAB_Params *);
typedef Int	(*DAB_TmcbspEnable)(DEV_Handle);
typedef Int	(*DAB_TmcbspReset)(DEV_Handle);
typedef Int	(*DAB_TsetupParam)(DEV_Handle, XDAS_UInt32, XDAS_UInt32, unsigned int, unsigned int);
typedef Int	(*DAB_TsetupXfer)(DEV_Handle, XDAS_UInt32, XDAS_UInt32, XDAS_UInt32, DEV_Frame *);

typedef struct DAB_Fxns
{
    //common (must be same as DEV_Fxns)
    DEV_Tclose		close;
    DEV_Tctrl		ctrl;
    DEV_Tidle		idle;
    DEV_Tissue		issue;
    DEV_Topen		open;
    DEV_Tready		ready;
    DEV_Treclaim	reclaim;

    //DAB specific
    DAB_Tshutdown       shutdown;
    DAB_Tstart          start;
    DAB_Tconfig         config;
    DAB_TmcbspEnable    mcbspEnable;
    DAB_TmcbspReset     mcbspReset;
    DAB_TsetupParam     setupParam;
    DAB_TsetupXfer      setupXfer;
} DAB_Fxns;

extern DAB_Fxns DAB_FXNS;
extern Void DAB_init (void);

// .............................................................................

typedef struct DAB_EDMA_Param
{
    QUE_Elem         link;	 // queue link
    XDAS_UInt32      hEdma;  // parameter table handle
} DAB_EDMA_Param;

// .............................................................................
// device extension (device specific context).

/* note: some of the config parameters are not needed except at init, we could
   make use of this for memory efficiency. However, since we are copying the
   config the user provided during SIO_ctrl we are requiring the user to keep
   this data resident for the life of the stream handle
*/

#define MAX_EDMA_PARAM_ENTRIES 14 /* minimum is 2 */

typedef struct DAB_DeviceExtension
{
    DEV_Handle   device;

    XDAS_Int8   numQueued;
    XDAS_Int8   errorState; 
    XDAS_Int8   runState;
    XDAS_Int8   deviceNum;

    SEM_Handle  sync;
    QUE_Obj     xferQue; // frame bin between todevice and fromdevice

    DAB_Fxns         *pFxns;
    const DAB_Params *pParams;

    XDAS_UInt32 firstTCC;

    QUE_Obj     paramQue;
    XDAS_Int8   numEdmaParams;
    XDAS_Int8   unused[3];

    LOG_Obj     *pTraceLog; 
    
    XDAS_Int32        edmaWordSize;
    XDAS_UInt32        shortTCC;
    XDAS_UInt32       shortEdma;
    XDAS_Int32        shutDown; 

    XDAS_UInt32       activeEdma;
    XDAS_UInt32       errorEdma;
    SEM_Handle        ready;
    DAB_EDMA_Param    edmaParams[MAX_EDMA_PARAM_ENTRIES];

    Int    optLevel;
    Int    numParamSetup;
} DAB_DeviceExtension;

// .............................................................................

#endif // DAB_H





























