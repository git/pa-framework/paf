
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Header file for dap SIO driver.
//
//
//

#define DAP_DMA_DMAX
#define DAP_PORT_MCASP

#ifndef DAP_H
#define DAP_H

#include <std.h>
#include <dev.h>
#include <que.h>
#include <sem.h>
#include  <std.h>
#include <xdas.h>

// include dap_mcasp here although not needed locally, but allows user code
// (i.e. dap_xx.c) to include just dap.h and not require other includes; like csl.
#ifdef DAP_PORT_MCASP
#include "dap_csl_mcasp.h"
#endif

#include <pafsio.h>

#define MAX_DAP_DEVICES     32  // Must be >= total number input and output devices
                                // For example, from project *io_a.h file:
                                //     total devices = DEVINP_N + DEVOUT_N

// ..........................................................................
// Global context for the DAP layer

typedef struct DAP_DriverObject
{
    DEV_Handle    device[MAX_DAP_DEVICES];

    SmInt         numDevices;
    SmInt         unused[3];

} DAP_DriverObject;

extern DAP_DriverObject dapDrv;

// ..........................................................................
// device configuration structure -- used as argument to SIO_ctrl

struct DAP_Params_
{
    XDAS_Int32  pinMask;
};

typedef struct DAP_Params
{
    Int                 size;   // Type-specific size
    struct DXX_Params_  sio;    // Common parameters
    struct DAP_Params_  dap;    // Driver parameters
} DAP_Params;

// .............................................................................
// DMA Function table defs

typedef Int  (*DAP_DMA_Talloc)(DEV_Handle);
typedef Int  (*DAP_DMA_Tconfig)(DEV_Handle, DEV_Frame *);
typedef Int  (*DAP_DMA_Tctrl)(DEV_Handle, Uns, Arg);
typedef Int  (*DAP_DMA_Tdisable)(DEV_Handle);
typedef Int  (*DAP_DMA_Tenable)(DEV_Handle);
typedef Int  (*DAP_DMA_Tfree)(DEV_Handle);
typedef Void (*DAP_DMA_Tinit)(Void);
typedef Void (*DAP_DMA_Tisr)(Void);
typedef Int  (*DAP_DMA_Tlock)(DEV_Handle);
typedef Int  (*DAP_DMA_Topen)(DEV_Handle);
typedef Int  (*DAP_DMA_Tshutdown)(DEV_Handle);
typedef Int  (*DAP_DMA_Treclaim)(DEV_Handle);
typedef Int  (*DAP_DMA_Tunlock)(DEV_Handle);

typedef struct DAP_DMA_Fxns {    
    DAP_DMA_Talloc      alloc;
    DAP_DMA_Tconfig     config;
    DAP_DMA_Tctrl       ctrl;
    DAP_DMA_Tdisable    disable;
    DAP_DMA_Tenable     enable;
    DAP_DMA_Tfree       free;
    DAP_DMA_Tinit       init;
    DAP_DMA_Tisr        isr;
    DAP_DMA_Tlock       lock;
    DAP_DMA_Topen       open;
    DAP_DMA_Treclaim    reclaim;
    DAP_DMA_Tshutdown   shutdown;
    DAP_DMA_Tunlock     unlock;
} DAP_DMA_Fxns;

#define DAP_DMA_FTABLE_alloc(_a)         (*pDevExt->pFxns->pDmaFxns->alloc)(_a)
#define DAP_DMA_FTABLE_config(_a,_b)     (*pDevExt->pFxns->pDmaFxns->config)(_a,_b)
#define DAP_DMA_FTABLE_ctrl(_a,_b,_c)    (*pDevExt->pFxns->pDmaFxns->ctrl)(_a,_b,_c)
#define DAP_DMA_FTABLE_disable(_a)       (*pDevExt->pFxns->pDmaFxns->disable)(_a)
#define DAP_DMA_FTABLE_enable(_a)        (*pDevExt->pFxns->pDmaFxns->enable)(_a)
#define DAP_DMA_FTABLE_free(_a)          (*pDevExt->pFxns->pDmaFxns->free)(_a)
#define DAP_DMA_FTABLE_init()            (*pFxns->pDmaFxns->init)()
#define DAP_DMA_FTABLE_lock(_a)          (*pDevExt->pFxns->pDmaFxns->lock)(_a)
#define DAP_DMA_FTABLE_open(_a)          (*pDevExt->pFxns->pDmaFxns->open)(_a)
#define DAP_DMA_FTABLE_reclaim(_a)       (*pDevExt->pFxns->pDmaFxns->reclaim)(_a)
#define DAP_DMA_FTABLE_shutdown(_a)      (*pDevExt->pFxns->pDmaFxns->shutdown)(_a)
#define DAP_DMA_FTABLE_unlock(_a)        (*pDevExt->pFxns->pDmaFxns->unlock)(_a)

// .............................................................................
// PORT Function table defs

typedef Int	 (*DAP_PORT_Talloc)(DEV_Handle);
typedef Int	 (*DAP_PORT_Tclose)(DEV_Handle);
typedef Int	 (*DAP_PORT_Tenable)(DEV_Handle);
typedef Void     (*DAP_PORT_Tinit)(Void);
typedef Int	 (*DAP_PORT_Topen)(DEV_Handle);
typedef Int	 (*DAP_PORT_Treset)(DEV_Handle);
typedef Int	 (*DAP_PORT_TwatchDog)(DEV_Handle);

typedef struct DAP_PORT_Fxns {    
    DAP_PORT_Talloc      alloc;
    DAP_PORT_Tclose      close;
    DAP_PORT_Tenable     enable;
    DAP_PORT_Tinit       init;
    DAP_PORT_Topen       open;
    DAP_PORT_Treset      reset;
    DAP_PORT_TwatchDog   watchDog;
} DAP_PORT_Fxns;

#define DAP_PORT_FTABLE_alloc(_a)        (*pDevExt->pFxns->pPortFxns->alloc)(_a)
#define DAP_PORT_FTABLE_close(_a)        (*pDevExt->pFxns->pPortFxns->close)(_a)
#define DAP_PORT_FTABLE_enable(_a)       (*pDevExt->pFxns->pPortFxns->enable)(_a)
#define DAP_PORT_FTABLE_init()           (*pFxns->pPortFxns->init)()
#define DAP_PORT_FTABLE_open(_a)         (*pDevExt->pFxns->pPortFxns->open)(_a)
#define DAP_PORT_FTABLE_reset(_a)        (*pDevExt->pFxns->pPortFxns->reset)(_a)
#define DAP_PORT_FTABLE_watchDog(_a)     (*pDevExt->pFxns->pPortFxns->watchDog)(_a)

// .............................................................................
// DAP Function table defs

typedef Int	(*DAP_Tshutdown)(DEV_Handle);
typedef Int	(*DAP_Tstart)(DEV_Handle);
typedef Int	(*DAP_Tconfig)(DEV_Handle, const DAP_Params *);
typedef Int	(*DAP_TfreeResources)(DEV_Handle);

typedef struct DAP_Fxns {
    //common (must be same as DEV_Fxns)
    DEV_Tclose		close;
    DEV_Tctrl		ctrl;
    DEV_Tidle		idle;
    DEV_Tissue		issue;
    DEV_Topen		open;
    DEV_Tready		ready;
    DEV_Treclaim	reclaim; //optional

    //DAP specific
    DAP_Tshutdown       shutdown;
    DAP_Tstart          start;
    DAP_Tconfig         config;
    DAP_TfreeResources  freeResources;

    DAP_PORT_Fxns       *pPortFxns;
    DAP_DMA_Fxns        *pDmaFxns;

} DAP_Fxns;

extern DAP_Fxns DAP_FXNS;
extern Void DAP_init (void);     
extern Void DAP_watchDog (void); 

// macros assume pDevExt is available and pDevExt->pFxns is valid
#define DAP_FTABLE_shutdown(_a)          (*pDevExt->pFxns->shutdown)(_a)
#define DAP_FTABLE_start(_a)             (*pDevExt->pFxns->start)(_a)
#define DAP_FTABLE_config(_a,_b)         (*pDevExt->pFxns->config)(_a,_b)
#define DAP_FTABLE_ctrl(_a,_b,_c)        (*pDevExt->pFxns->ctrl)(_a,_b,_c)
#define DAP_FTABLE_edmaLink(_a,_b)       (*pDevExt->pFxns->edmaLink)(_a,_b)
#define DAP_FTABLE_freeResources(_a)     (*pDevExt->pFxns->freeResources)(_a)

// .............................................................................

// device extension (device specific context).

typedef struct DAP_DeviceExtension
{
    DEV_Handle   device;

    XDAS_Int8    numQueued;

    XDAS_Int8    errorState;
    XDAS_Int8    runState;
    XDAS_Int8    deviceNum;

    SEM_Handle   sync;
    QUE_Obj      xferQue;

    DAP_Fxns     *pFxns;

    const DAP_Params   *pParams;

    Int    shutDown;

    SmInt  numSers;
    SmInt  numSlots;
    SmInt  unused2[2];

    Void  *pPortExt; // unused for now
    Void  *pDmaExt;

} DAP_DeviceExtension;

// .............................................................................

#endif // DAP_H





























