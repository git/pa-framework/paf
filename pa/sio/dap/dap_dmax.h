
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX definitions for DAP_DMA
//
//
//

#ifndef DAP_DMAX
#define DAP_DMAX

#include <std.h>
#include <sio.h>

#include "dap.h"

#ifdef dMAX_CFG
#include <dmax_params.h> //needed for dMAX_Handle
#else
#include "dmax.h"
#endif

// ..........................................................................
// Global context for the DAP DMAX layer

typedef struct DAP_DMAX_DriverObject
{
    Int8       intTab[3]; 
    Uint8      lockCnt;
    QUE_Handle hParamQue;
} DAP_DMAX_DriverObject;

// ..........................................................................

// if this structure is updated then so must t:/pa/dmax/mcasp.inc
typedef struct DMAX_MCASP_Param {
    short cnt;
    short link;
    int   xfer;
    char  tcc;
	char  slotParity;
    char  intNum;
    char  stat;
} DMAX_MCASP_Param;

typedef struct DAP_DMAX_Param
{
    QUE_Elem         link;         // queue link
    XDAS_UInt16      offset;       // parameter offset from dmax data base
} DAP_DMAX_Param, *DAP_DMAX_ParamHandle;

// .............................................................................
//Function table defs

typedef DAP_DMAX_ParamHandle  (*DAP_DMAX_TallocParam)(DEV_Handle);
typedef Int                   (*DAP_DMAX_TfreeParam)(DEV_Handle, DAP_DMAX_ParamHandle);
typedef Int                   (*DAP_DMAX_TinitErrorParam)(DEV_Handle, DAP_DMAX_ParamHandle);
typedef Int                   (*DAP_DMAX_TinitStats)(DEV_Handle, float);
typedef Int                   (*DAP_DMAX_TnumRemain)(DEV_Handle, Int *);

typedef struct DAP_DMAX_Fxns {
    //common (must be same as DAP_DMA_Fxns)
    DAP_DMA_Talloc       alloc;
    DAP_DMA_Tconfig      config;
    DAP_DMA_Tctrl        ctrl;
    DAP_DMA_Tdisable     disable;
    DAP_DMA_Tenable      enable;
    DAP_DMA_Tfree        free;
    DAP_DMA_Tinit        init;
    DAP_DMA_Tisr         isr;
    DAP_DMA_Tlock        lock;
    DAP_DMA_Topen        open;
    DAP_DMA_Treclaim     reclaim;
    DAP_DMA_Tshutdown    shutdown;
    DAP_DMA_Tunlock      unlock;

    // dMAX specific
    DAP_DMAX_TallocParam        allocParam;
    DAP_DMAX_TfreeParam         freeParam;
    DAP_DMAX_TinitErrorParam    initErrorParam;
    DAP_DMAX_TinitStats         initStats;
    DAP_DMAX_TnumRemain         numRemain;
} DAP_DMAX_Fxns;

extern DAP_DMAX_Fxns DAP_DMAX_FXNS;

#define DAP_DMAX_FTABLE_allocParam(_a)        (*((DAP_DMAX_Fxns *)pDevExt->pFxns->pDmaFxns)->allocParam)(_a)
#define DAP_DMAX_FTABLE_freeParam(_a,_b)      (*((DAP_DMAX_Fxns *)pDevExt->pFxns->pDmaFxns)->freeParam)(_a,_b)
#define DAP_DMAX_FTABLE_initErrorParam(_a,_b) (*((DAP_DMAX_Fxns *)pDevExt->pFxns->pDmaFxns)->initErrorParam)(_a,_b)
#define DAP_DMAX_FTABLE_initStats(_a,_b)      (*((DAP_DMAX_Fxns *)pDevExt->pFxns->pDmaFxns)->initStats)(_a,_b)
#define DAP_DMAX_FTABLE_numRemain(_a,_b)      (*((DAP_DMAX_Fxns *)pDevExt->pFxns->pDmaFxns)->numRemain)(_a,_b)

// .............................................................................

#define MAX_EXTRA_TCC 3

typedef struct DAP_DMAX_DeviceExtension
{
    dMAX_Handle           pDmax;
    XDAS_UInt8            wordSize; 
    XDAS_UInt8            event;
    XDAS_UInt8            maxID;
    XDAS_Int8             tcc;
    PAF_SIO_Stats        *pStats;
    DAP_DMAX_ParamHandle  pDmaxLastParam;
    DAP_DMAX_ParamHandle  pDmaxErrorParam1;
    DAP_DMAX_ParamHandle  pDmaxErrorParam2;
    XDAS_UInt16           linkToggleOffset;
    XDAS_UInt8            maxExtraTcc; // set max in var to facilitate rom patching in the future
    XDAS_Int8             tccExtra[MAX_EXTRA_TCC];

} DAP_DMAX_DeviceExtension;

// .............................................................................
// macros for accessing dMax APIs (assumes pDmax handle is locally available)

#ifdef dMAX_CFG
// APIs for use with configurable dMAX
#define DMAX_FTABLE_enableEvent(_a,_b)         pDmax->fxns->enaEvt(_a,_b)
#define DMAX_FTABLE_disableEvent(_a,_b)        pDmax->fxns->disEvt(_a,_b)
#define DMAX_FTABLE_eventPriorityHigh(_a,_b)   pDmax->fxns->evtPri(_a,dMAX_MAX0_ID,_b)
#define DMAX_FTABLE_clearEventFlag(_a,_b)      pDmax->fxns->clrEvtFlg(_a,_b)
#define DMAX_FTABLE_allocateTable(_a,_b,_c)    pDmax->fxns->allocPT(_a,_b,_c)
#define DMAX_FTABLE_allocateInt(_a,_b)         pDmax->fxns->allocInt(_a,_b)
#define DMAX_FTABLE_mapInt(_a,_b)              pDmax->fxns->mapInt(_a,_b)
#define DMAX_FTABLE_allocateTCC(_a,_b)         pDmax->fxns->allocTCC(_a,_b)
#define DMAX_FTABLE_freeTCC(_a,_b)             pDmax->fxns->freeTCC(_a,_b)
#define DMAX_FTABLE_checkTCC(_a,_b)            pDmax->fxns->chkTCC(_a,_b)
#define DMAX_FTABLE_clrTCC(_a,_b)              pDmax->fxns->clrTCC(_a,_b)
#else
// APIs for older PA only interface
#define DMAX_FTABLE_gioReset(_a)               pDmax->fxns->gioReset(_a)
#define DMAX_FTABLE_gioResetRelease(_a)        pDmax->fxns->gioResetRelease(_a) 
#define DMAX_FTABLE_eventPolarity(_a,_b,_c)    pDmax->fxns->eventPolarity(_a,_b_c)
#define DMAX_FTABLE_enableEvent(_a,_b)         pDmax->fxns->enableEvent(_a,_b)
#define DMAX_FTABLE_disableEvent(_a,_b)        pDmax->fxns->disableEvent(_a,_b)
#define DMAX_FTABLE_disableEventAll(_a,_b)     pDmax->fxns->disableEventAll(_a)
#define DMAX_FTABLE_eventPriorityHigh(_a,_b)   pDmax->fxns->eventPriorityHigh(_a,_b)
#define DMAX_FTABLE_eventPriorityLow(_a,_b)    pDmax->fxns->eventPriorityLow(_a,_b)
#define DMAX_FTABLE_clearEventFlag(_a,_b)      pDmax->fxns->clearEventFlag(_a,_b)
#define DMAX_FTABLE_clearEventFlagAll(_a)      pDmax->fxns->clearEventFlagAll(_a)
#define DMAX_FTABLE_dirInputAll(_a)            pDmax->fxns->dirInputAll(_a)
#define DMAX_FTABLE_resetRelease(_a,_b)        pDmax->fxns->resetRelease(_a,_b)
#define DMAX_FTABLE_initIRAM(_a,_b)            pDmax->fxns->initIRAM(_a,_b)
#define DMAX_FTABLE_initDRAM(_a,_b)            pDmax->fxns->initDRAM(_a,_b)
#define DMAX_FTABLE_updateEET(_a,_b,_c,_d)     pDmax->fxns->updateEET(_a,_b,_c,_d)
#define DMAX_FTABLE_updatePTT(_a,_b,_c,_d)     pDmax->fxns->updatePTT(_a,_b,_c,_d)
#define DMAX_FTABLE_allocateTable(_a,_b,_c)    pDmax->fxns->allocateTable(_a,_b,_c)
#define DMAX_FTABLE_config(_a,_b,_c,_d)        pDmax->fxns->config(_a,_b,_c,_d)
#define DMAX_FTABLE_freeTable(_a,_b,_c,_d)     pDmax->fxns->freeTable(_a,_b,_c,_d)
#define DMAX_FTABLE_allocateInt(_a,_b)         pDmax->fxns->allocateInt(_a,_b)
#define DMAX_FTABLE_freeInt(_a,_b)             pDmax->fxns->freeInt(_a,_b)
#define DMAX_FTABLE_mapInt(_a,_b)              pDmax->fxns->mapInt(_a,_b)
#define DMAX_FTABLE_allocateTCC(_a,_b)         pDmax->fxns->allocateTCC(_a,_b)
#define DMAX_FTABLE_freeTCC(_a,_b)             pDmax->fxns->freeTCC(_a,_b)
#define DMAX_FTABLE_checkTCC(_a,_b)            pDmax->fxns->checkTCC(_a,_b)
#define DMAX_FTABLE_setTCC(_a,_b)              pDmax->fxns->setTCC(_a,_b) 
#define DMAX_FTABLE_clrTCC(_a,_b)              pDmax->fxns->clrTCC(_a,_b)
#define DMAX_FTABLE_allocatePend(_a,_b,_c)     pDmax->fxns->allocatePend(_a,_b,_c)
#define DMAX_FTABLE_mbxPost(_a,_b,_c,_d,_e)    pDmax->fxns->mbxPost(_a,_b,_c,_d,_e)
#endif //dMAX_CFG

// .............................................................................

#endif //DAP_DMAX
