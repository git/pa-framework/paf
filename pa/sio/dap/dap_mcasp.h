
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// McASP definitions for DAP_PORT (e4 specific)
//
//
//

#ifndef DAP_MCASP
#define DAP_MCASP

#include <std.h>
#include <sio.h>

#include "dap.h"
#include "dap_csl_mcasp.h"

// ..........................................................................
// Global context for the DAP layer

typedef struct DAP_MCASP_DriverObject
{
    MCASP_Handle        hPort[_MCASP_PORT_CNT];
    char          fifoPresent[_MCASP_PORT_CNT];
} DAP_MCASP_DriverObject;

extern DAP_MCASP_DriverObject dapMcaspDrv;

// ..........................................................................

typedef Int (*DAP_MCASP_TwaitSet) (MCASP_Handle hMcasp, Uint32 wrReg, Uint32 rdReg, Uint32 mask, Uint32 timeout);

//STATIC Int  DAP_updateDITRam(DEV_Handle device );

typedef struct DAP_MCASP_Fxns {
    //common (must be same as DAP_PORT_Fxns)
    DAP_PORT_Talloc    alloc;
    DAP_PORT_Tclose    close;
    DAP_PORT_Tenable   enable;
    DAP_PORT_Tinit     init;
    DAP_PORT_Topen     open;
    DAP_PORT_Treset    reset;
    DAP_PORT_TwatchDog watchDog;

    //mcasp specific portion
    DAP_MCASP_TwaitSet     waitSet;
} DAP_MCASP_Fxns;

extern DAP_MCASP_Fxns DAP_MCASP_FXNS;

#define DAP_MCASP_FTABLE_waitSet(_a,_b,_c,_d,_e)  (*((DAP_MCASP_Fxns *)pDevExt->pFxns->pPortFxns)->waitSet)(_a,_b,_c,_d,_e)

//#define DAP_MCASP_FTABLE_updateDITRam(_a)      (*pDevExt->pFxns->updateDITRam)(_a)

// .............................................................................
// unused for now
// typedef struct DAP_MCASP_DeviceExtension
// {
    
// } DAP_MCASP_DeviceExtension;

// .............................................................................

#endif //DAP_MCASP_H
