
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//
//

#include <dcs6.h>

volatile DCS6_Handle DCS6_HANDLE=NULL;

void DCS6_errorRecover(
    DCS6_Handle handle)
{
    handle->fxns->errorAssert();
    handle->fxns->reset(handle);
    CSL_DMAXINTC_0_REGS->STATIDXCLR=handle->dmax->ec;
    handle->fxns->configure(handle);
    handle->ecnt++;
    handle->fxns->errorDeassert();
}

void DCS6_slc(
    DCS6_Handle handle)
{
    DCS6_dMAXParam *p=handle->dmax->param;
    Uint16 slc,sls;

    slc=*(Uint16*)(p->rbuf_start+p->rbuf_poff);
    if(handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE)
        handle->fxns->cacheInv(handle,(void*)(p->rbuf_start+p->rbuf_poff),2);
    if(slc==0x8001||slc==0x8101)
        sls=0xf001;
    else
        sls=0xff01;
    handle->fxns->log1(handle,handle->params->logObj,"DCS6: Rx: 0x%x",slc);
    handle->fxns->log1(handle,handle->params->logObj,"DCS6: Tx: 0x%x",sls);
    *(Uint16*)(p->xbuf_start+p->xbuf_off)=sls;
    if(handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE)
        handle->fxns->cacheWbInv(handle,(void*)(p->xbuf_start+p->xbuf_off),2);
    p->rbuf_poff+=2;
    p->xbuf_off+=2;
    CSL_DMAXINTC_0_REGS->STATIDXCLR=handle->dmax->rc;
}

Uint32 DCS6_read(
    DCS6_Handle handle,
    Uint16 *aBuf,
    Uint16 aSize)
{
    DCS6_dMAXParam *p=handle->dmax->param;
    Uint32 len=0;
    Uint16 rbuf_poff,rbuf_len;
    Uint32 w0;
    Uint32 i;

    while(!len){
        handle->fxns->semPend(handle,handle->sem);
        if(handle->params->edr&&
            CSL_DMAXINTC_0_REGS->STATSETINT1&1<<handle->dmax->ec-32)
            handle->fxns->errorRecover(handle);
        else{
	    rbuf_poff=p->rbuf_poff;
	    rbuf_len=p->rbuf_len;

	    if(rbuf_poff == rbuf_len){
		rbuf_poff = 0;
	    }

	    if(handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE){
		handle->fxns->cacheInv(handle,(void*)(p->rbuf_start+rbuf_poff),2);
	    }

            w0=*(Uint16*)(p->rbuf_start+rbuf_poff);
            if(w0&0x8000)
                handle->fxns->slc(handle);
            else
                len=w0;
        }
    }
    if(len<<1>aSize){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: Application buffer size too small");
        p->rbuf_poff+=len+1<<1;
//        CSL_DMAXINTC_0_REGS->STATIDXCLR=handle->dmax->rc;
        return 8;
    }
    else{
        if(handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE){
	    if((rbuf_poff + ((len+1)<<1)) > rbuf_len){ // if going to wrap
		handle->fxns->cacheInv(handle,(void*)(p->rbuf_start+rbuf_poff+2),(rbuf_len-(rbuf_poff+2)));
		handle->fxns->cacheInv(handle,(void*)(p->rbuf_start),((len+1)<<1)-(rbuf_len-rbuf_poff));
	    }
	    else{
		// len is in terms 16bit unit; shift it left by 1bit to make it byte count
		handle->fxns->cacheInv(handle,(void*)(p->rbuf_start+rbuf_poff+2),(len<<1));
	    }
	}
        for(i=0;i<=len;++i){
            aBuf[i]=*(Uint16*)(p->rbuf_start+rbuf_poff);
            handle->fxns->log1(handle,
                handle->params->logObj,"DCS6: Rx: 0x%x",aBuf[i]);
            rbuf_poff+=2;
            if(rbuf_poff>=rbuf_len)
                rbuf_poff-=rbuf_len;
        }
        p->rbuf_poff=rbuf_poff;
        return 0;
    }
}

Uint32 DCS6_write(
    DCS6_Handle handle,
    Uint16 *aBuf,
    Uint16 aSize)
{
    DCS6_dMAXParam *p=handle->dmax->param;
    Uint16 len=aBuf[0];
    Uint16 xbuf_off,xbuf_len;
    Uint32 i;

    if(len){
        xbuf_off=p->xbuf_off;
        xbuf_len=p->xbuf_len;
        if(len<<1>xbuf_len){
            handle->fxns->log(handle,handle->params->logObj,
                "DCS6: Response too big");
            return 9;
        }
        else{
            for(i=0;i<=len;++i){
                *(Uint16*)(p->xbuf_start+xbuf_off)=aBuf[i];
                handle->fxns->log1(handle,
                    handle->params->logObj,"DCS6: Tx: 0x%x",aBuf[i]);
                xbuf_off+=2;
                if(xbuf_off>=xbuf_len)
                    xbuf_off-=xbuf_len;
            }
            if(handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE){
                if(xbuf_off>=p->xbuf_off||!xbuf_off)
                    handle->fxns->cacheWbInv(handle,(void*)(p->xbuf_start+p->xbuf_off),
                        ((len+1)<<1));
                else{
                    handle->fxns->cacheWbInv(handle,(void*)(p->xbuf_start+p->xbuf_off),
                        p->xbuf_len-p->xbuf_off);
                    handle->fxns->cacheWbInv(handle,(void*)p->xbuf_start,
                        ((len+1)<<1)-(p->xbuf_len-p->xbuf_off));
                }
            }
            p->xbuf_off=xbuf_off;
        }
    }
    p->beta_prime_value=
        ((ACP_MDS_Obj *)handle->acp)->pStatus->betaPrimeValue;
    CSL_DMAXINTC_0_REGS->STATIDXCLR=handle->dmax->rc;
    return 0;
}

void DCS6_isr(
    UArg h)
{
    DCS6_Handle handle=(DCS6_Handle)h;
    handle->fxns->semPost(handle,handle->sem);
}

void DCS6_configuredMAX(
    DCS6_Handle handle)
{
    DCS6_dMAXParam *p=handle->dmax->param;
    Uint8 *chanmap=(Uint8*)&CSL_DMAXINTC_0_REGS->CHANMAP0;
    Uint32 *DMAXDRAM_1=(Uint32*)CSL_DMAX_DRAM1_BASEADDR;
    ACP_MDS_Obj *acpObj=(ACP_MDS_Obj*)handle->acp;

    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0||
        handle->params->dev==DCS6_PARAMS_DEV_SPI1){
        DCS6_Params_Dev_Spi *pSpiParam=handle->params->pDev->pDevData.pSpi;
        if(handle->params->dev==DCS6_PARAMS_DEV_SPI0){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_SPI0;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_SPI0;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_SPI0]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_SPI0;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_SPI0]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_SPI1){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_SPI1;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_SPI1;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_SPI1]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_SPI1;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_SPI1]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        p->control=
            2-(pSpiParam->clen==DCS6_PARAMS_DEV_SPI_CLEN_8)<<DCS6_dMAXPARAM_CONTROL_PROT_SHFT|
            handle->params->edr<<DCS6_dMAXPARAM_CONTROL_EDR_SHFT|
            (pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5_SRDY)<<DCS6_dMAXPARAM_CONTROL_SRDY_SHFT|
            handle->params->pw<<DCS6_dMAXPARAM_CONTROL_PW_SHFT|
            handle->params->pr<<DCS6_dMAXPARAM_CONTROL_PR_SHFT|
            handle->params->pslc<<DCS6_dMAXPARAM_CONTROL_PSLC_SHFT|
            handle->params->psfil<<DCS6_dMAXPARAM_CONTROL_PSFIL_SHFT;
        p->status=DCS6_dMAXPARAM_STATUS_NOLINK<<DCS6_dMAXPARAM_STATUS_LINK_SHFT;
        if(handle->params->dev==DCS6_PARAMS_DEV_SPI0){
            p->drr=(Uint32)&CSL_SPI_0_REGS->SPIBUF;
            p->dxr=(Uint32)&CSL_SPI_0_REGS->SPIDAT0;
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_SPI1){
            p->drr=(Uint32)&CSL_SPI_1_REGS->SPIBUF;
            p->dxr=(Uint32)&CSL_SPI_1_REGS->SPIDAT0;
        }
        p->rbuf_hwm=handle->params->rxHwm;
        p->rbuf_lwm=handle->params->rxLwm;
        p->rbuf_off=0;
        p->xbuf_off=0;
        if(handle->params->edr)
            p->err_mask=pSpiParam->emask;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C0||
        handle->params->dev==DCS6_PARAMS_DEV_I2C1){
        DCS6_Params_Dev_I2c *pI2cParam=handle->params->pDev->pDevData.pI2c;
        if(handle->params->dev==DCS6_PARAMS_DEV_I2C0){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_I2C0;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_I2C0;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_I2C0]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_I2C0;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_I2C0]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_I2C1){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_I2C1;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_I2C1;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_I2C1]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_I2C1;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_I2C1]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        p->control=0<<DCS6_dMAXPARAM_CONTROL_PROT_SHFT|
            handle->params->edr<<DCS6_dMAXPARAM_CONTROL_EDR_SHFT|
            0<<DCS6_dMAXPARAM_CONTROL_SRDY_SHFT|
            handle->params->pw<<DCS6_dMAXPARAM_CONTROL_PW_SHFT|
            handle->params->pr<<DCS6_dMAXPARAM_CONTROL_PR_SHFT|
            handle->params->pslc<<DCS6_dMAXPARAM_CONTROL_PSLC_SHFT|
            handle->params->psfil<<DCS6_dMAXPARAM_CONTROL_PSFIL_SHFT;
        p->status=DCS6_dMAXPARAM_STATUS_LINK<<DCS6_dMAXPARAM_STATUS_LINK_SHFT;
        if(handle->params->dev==DCS6_PARAMS_DEV_I2C0){
            p->drr=(Uint32)&CSL_I2C_0_REGS->ICDRR;
            p->dxr=(Uint32)&CSL_I2C_0_REGS->ICDXR;
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_I2C1){
            p->drr=(Uint32)&CSL_I2C_1_REGS->ICDRR;
            p->dxr=(Uint32)&CSL_I2C_1_REGS->ICDXR;
        }
        p->rbuf_hwm=handle->params->rxHwm;
        p->rbuf_lwm=handle->params->rxLwm;
        p->rbuf_off=0;
        p->xbuf_off=0;
        if(handle->params->edr)
            p->err_mask=pI2cParam->emask;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2){
        DCS6_Params_Dev_Uart *pUartParam=handle->params->pDev->pDevData.pUart;
        if(handle->params->dev==DCS6_PARAMS_DEV_UART0){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_UART0;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_UART0;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_UART0]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_UART0;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_UART0]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_UART1){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_UART1;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_UART1;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_UART1]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_UART1;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_UART1]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_UART2){
            CSL_DMAXINTC_0_REGS->ENIDXCLR=DCS6_dMAXPARAM_INTERRUPT_UART2;
            CSL_DMAXINTC_0_REGS->STATCLRINT0=1<<DCS6_dMAXPARAM_INTERRUPT_UART2;
            chanmap[DCS6_dMAXPARAM_INTERRUPT_UART2]=dMAX_MAX1_ID;
            CSL_DMAXINTC_0_REGS->ENIDXSET=DCS6_dMAXPARAM_INTERRUPT_UART2;
            DMAXDRAM_1[DCS6_dMAXPARAM_INTERRUPT_UART2]=
                DCS6_dMAXPARAM_MKEE((Uint32)handle->dmax->param,
                DCS6_dMAXPARAM_TYPE);
        }
        p->control=3<<DCS6_dMAXPARAM_CONTROL_PROT_SHFT|
            handle->params->edr<<DCS6_dMAXPARAM_CONTROL_EDR_SHFT|
            0<<DCS6_dMAXPARAM_CONTROL_SRDY_SHFT|
            handle->params->pw<<DCS6_dMAXPARAM_CONTROL_PW_SHFT|
            handle->params->pr<<DCS6_dMAXPARAM_CONTROL_PR_SHFT|
            handle->params->pslc<<DCS6_dMAXPARAM_CONTROL_PSLC_SHFT|
            handle->params->psfil<<DCS6_dMAXPARAM_CONTROL_PSFIL_SHFT;
        p->status=DCS6_dMAXPARAM_STATUS_LINK<<DCS6_dMAXPARAM_STATUS_LINK_SHFT;
        if(handle->params->dev==DCS6_PARAMS_DEV_UART0){
            p->drr=(Uint32)&CSL_UART_0_REGS->RBR;
            p->dxr=(Uint32)&CSL_UART_0_REGS->THR;
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_UART1){
            p->drr=(Uint32)&CSL_UART_1_REGS->RBR;
            p->dxr=(Uint32)&CSL_UART_1_REGS->THR;
        }
        else if(handle->params->dev==DCS6_PARAMS_DEV_UART2){
            p->drr=(Uint32)&CSL_UART_2_REGS->RBR;
            p->dxr=(Uint32)&CSL_UART_2_REGS->THR;
        }
        p->rbuf_hwm=pUartParam->xrxHwm;
        p->rbuf_lwm=pUartParam->xrxLwm;
        p->rbuf_off=2;
        p->xbuf_off=4;
        if(handle->params->edr)
            p->err_mask=pUartParam->emask;
        {
            DCS6_dMAXUartParam *u=(DCS6_dMAXUartParam*)handle->dmax->param;
            u->rx_line_cnt=0;
            u->tx_line_cnt=0;
            u->rxfiflen=pUartParam->fl==DCS6_PARAMS_DEV_UART_FL_4?4:
                pUartParam->fl==DCS6_PARAMS_DEV_UART_FL_8?8:14;
            u->res3=0;
            u->errcnt=0;
            u->errflg=0;
            u->res4=0;
            u->string_base=(Uint32)UART_STRINGS;
            u->xrbuf_start=(Uint32)handle->xrxBuf;
            u->xrbuf_len=pUartParam->xrxSize;
            u->res5=0;
            u->xrbuf_off=0;
            u->xrbuf_poff=0;
            u->xxbuf_start=(Uint32)handle->xtxBuf;
            u->xxbuf_len=pUartParam->xtxSize;
            u->res6=0;
            u->xxbuf_off=0;
            u->xxbuf_poff=0;
        }
    }
    p->rbuf_start=(Uint32)handle->rxBuf;
    p->rbuf_len=handle->params->rxSize;
    p->ec=handle->dmax->ec;
    p->rc=handle->dmax->rc;
    p->xbuf_start=(Uint32)handle->txBuf;
    p->xbuf_len=handle->params->txSize;
    p->res1=0;
    p->ext_status=0;
    p->rbuf_poff=0;
    p->xbuf_poff=0;
    p->std_beta_base=(Uint32)acpObj->config.betaTable[ACP_SERIES_STD]->pStatus;
    p->alt_beta_base=(Uint32)acpObj->config.betaTable[ACP_SERIES_ALT]->pStatus;
    p->oem_beta_base=(Uint32)acpObj->config.betaTable[ACP_SERIES_OEM]->pStatus;
    p->cus_beta_base=(Uint32)acpObj->config.betaTable[ACP_SERIES_CUS]->pStatus;
    p->beta_prime_value=acpObj->pStatus->betaPrimeValue;
    p->beta_prime_base=acpObj->pStatus->betaPrimeBase;
    p->beta_prime_offset=acpObj->pStatus->betaPrimeOffset;
    p->res2=0;
}

void DCS6_configureSPI(
    DCS6_Handle handle)
{
    CSL_SpiRegsOvly CSL_SPI_SLAVE_REGS;
    DCS6_Params_Dev_Spi *pSpiParam=handle->params->pDev->pDevData.pSpi;

    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0)
        CSL_SPI_SLAVE_REGS=CSL_SPI_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_SPI1)
        CSL_SPI_SLAVE_REGS=CSL_SPI_1_REGS;
    CSL_SPI_SLAVE_REGS->SPIGCR0=
        CSL_SPI_SPIGCR0_RESET_NO<<CSL_SPI_SPIGCR0_RESET_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIGCR1=
        CSL_SPI_SPIGCR1_CLKMOD_EXTERNAL<<CSL_SPI_SPIGCR1_CLKMOD_SHIFT|
        CSL_SPI_SPIGCR1_MASTER_SLAVE<<CSL_SPI_SPIGCR1_MASTER_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIPC0=
        CSL_SPI_SPIPC0_SOMIFUN_SPI<<CSL_SPI_SPIPC0_SOMIFUN_SHIFT|
        CSL_SPI_SPIPC0_SIMOFUN_SPI<<CSL_SPI_SPIPC0_SIMOFUN_SHIFT|
        CSL_SPI_SPIPC0_CLKFUN_SPI<<CSL_SPI_SPIPC0_CLKFUN_SHIFT;
    if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_4_ENA||
        pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5)
        CSL_SPI_SLAVE_REGS->SPIPC0|=
            CSL_SPI_SPIPC0_ENAFUN_SPI<<CSL_SPI_SPIPC0_ENAFUN_SHIFT;
    else if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5_SRDY){
        CSL_SPI_SLAVE_REGS->SPIPC1=
            CSL_SPI_SPIPC1_ENADIR_OUTPUT<<CSL_SPI_SPIPC1_ENADIR_SHIFT;
        CSL_SPI_SLAVE_REGS->SPIPC4=
            CSL_SPI_SPIPC4_ENADSET_SET<<CSL_SPI_SPIPC4_ENADSET_SHIFT;
    }
    if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_4_SCS||
        pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5||
        pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5_SRDY)
        CSL_SPI_SLAVE_REGS->SPIPC0|=
            CSL_SPI_SPIPC0_SCSFUN_SPI<<CSL_SPI_SPIPC0_SCSFUN_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIDEF=1;
    CSL_SPI_SLAVE_REGS->SPIDAT1=
        CSL_SPI_SPIDAT1_DFSEL_FMT0<<CSL_SPI_SPIDAT1_DFSEL_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIFMT[0]=
        CSL_SPI_SPIFMT_SHIFTDIR_MSBFIRST<<CSL_SPI_SPIFMT_SHIFTDIR_SHIFT|
        pSpiParam->pol<<CSL_SPI_SPIFMT_POLARITY_SHIFT|
        pSpiParam->pha<<CSL_SPI_SPIFMT_PHASE_SHIFT|
        pSpiParam->clen<<CSL_SPI_SPIFMT_CHARLEN_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIDELAY=CSL_SPI_SPIDELAY_RESETVAL;
    CSL_SPI_SLAVE_REGS->SPIINT0=
        CSL_SPI_SPIINT0_RXINT_ENABLE<<CSL_SPI_SPIINT0_RXINT_SHIFT;
    CSL_SPI_SLAVE_REGS->SPILVL=
        CSL_SPI_SPILVL_RXINTLVL_INT1<<CSL_SPI_SPILVL_RXINTLVL_SHIFT;
    if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_4_ENA||
        pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5)
        CSL_SPI_SLAVE_REGS->SPIINT0|=
            pSpiParam->enahiz<<CSL_SPI_SPIINT0_ENAHIGHZ_SHIFT;
    CSL_SPI_SLAVE_REGS->SPIGCR1|=
        CSL_SPI_SPIGCR1_SPIEN_ACTIVE<<CSL_SPI_SPIGCR1_SPIEN_SHIFT;
    if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_4_ENA||
        pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5)
        CSL_SPI_SLAVE_REGS->SPIDAT0=0;
    else if(pSpiParam->np==DCS6_PARAMS_DEV_SPI_NP_5_SRDY)
        CSL_SPI_SLAVE_REGS->SPIPC5=
            CSL_SPI_SPIPC5_ENADCLR_CLEAR<<CSL_SPI_SPIPC5_ENADCLR_SHIFT;
}

void DCS6_configureI2C(
    DCS6_Handle handle)
{
    CSL_I2cRegsOvly CSL_I2C_SLAVE_REGS;
    DCS6_Params_Dev_I2c *pI2cParam=handle->params->pDev->pDevData.pI2c;

    if(handle->params->dev==DCS6_PARAMS_DEV_I2C0)
        CSL_I2C_SLAVE_REGS=CSL_I2C_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C1)
        CSL_I2C_SLAVE_REGS=CSL_I2C_1_REGS;
    CSL_I2C_SLAVE_REGS->ICPSC=pI2cParam->icpsc;
    CSL_I2C_SLAVE_REGS->ICCLKL=pI2cParam->icclkl;
    CSL_I2C_SLAVE_REGS->ICCLKH=pI2cParam->icclkh;
    CSL_I2C_SLAVE_REGS->ICMDR=CSL_I2C_ICMDR_IRS_MASK;
    CSL_I2C_SLAVE_REGS->ICEMDR=0;
    CSL_I2C_SLAVE_REGS->ICCNT=1;
    CSL_I2C_SLAVE_REGS->ICOAR=pI2cParam->icoar;
    CSL_I2C_SLAVE_REGS->ICIMR=
        CSL_I2C_ICIMR_ICXRDY_ENABLE<<CSL_I2C_ICIMR_ICXRDY_SHIFT|
        CSL_I2C_ICIMR_ICRRDY_ENABLE<<CSL_I2C_ICIMR_ICRRDY_SHIFT;
}

void DCS6_configureUART(
    DCS6_Handle handle)
{
    CSL_UartRegsOvly CSL_UART_SLAVE_REGS;
    DCS6_Params_Dev_Uart *pUartParam=handle->params->pDev->pDevData.pUart;

    if(handle->params->dev==DCS6_PARAMS_DEV_UART0)
        CSL_UART_SLAVE_REGS=CSL_UART_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART1)
        CSL_UART_SLAVE_REGS=CSL_UART_1_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART2)
        CSL_UART_SLAVE_REGS=CSL_UART_2_REGS;
    CSL_UART_SLAVE_REGS->DLL=pUartParam->dll;
    CSL_UART_SLAVE_REGS->DLH=pUartParam->dlh;
    CSL_UART_SLAVE_REGS->MDR=pUartParam->mdr;
    CSL_UART_SLAVE_REGS->FCR=
        CSL_UART_FCR_FIFOEN_ENABLE<<CSL_UART_FCR_FIFOEN_SHIFT;
    CSL_UART_SLAVE_REGS->FCR=
        pUartParam->fl<<CSL_UART_FCR_RXFIFTL_SHIFT|
        CSL_UART_FCR_DMAMODE1_DISABLE<<CSL_UART_FCR_DMAMODE1_SHIFT|
        CSL_UART_FCR_TXCLR_CLR<<CSL_UART_FCR_TXCLR_SHIFT|
        CSL_UART_FCR_RXCLR_CLR<<CSL_UART_FCR_RXCLR_SHIFT|
        CSL_UART_FCR_FIFOEN_ENABLE<<CSL_UART_FCR_FIFOEN_SHIFT;
    CSL_UART_SLAVE_REGS->LCR=
        CSL_UART_LCR_DLAB_DISABLE<<CSL_UART_LCR_DLAB_SHIFT|
        CSL_UART_LCR_BC_DISABLE<<CSL_UART_LCR_BC_SHIFT|
        CSL_UART_LCR_PEN_DISABLE<<CSL_UART_LCR_PEN_SHIFT|
        CSL_UART_LCR_STB_1STOPBIT<<CSL_UART_LCR_STB_SHIFT|
        CSL_UART_LCR_WLS_BITS8<<CSL_UART_LCR_WLS_SHIFT;
    CSL_UART_SLAVE_REGS->MCR=CSL_UART_MCR_RESETVAL;
    CSL_UART_SLAVE_REGS->IER=
        CSL_UART_IER_ELSI_ENABLE<<CSL_UART_IER_ELSI_SHIFT|
        CSL_UART_IER_ETBEI_ENABLE<<CSL_UART_IER_ETBEI_SHIFT|
        CSL_UART_IER_ERBI_ENABLE<<CSL_UART_IER_ERBI_SHIFT;
    CSL_UART_SLAVE_REGS->PWREMU_MGMT=
        CSL_UART_PWREMU_MGMT_UTRST_ENABLE<<CSL_UART_PWREMU_MGMT_UTRST_SHIFT|
        CSL_UART_PWREMU_MGMT_URRST_ENABLE<<CSL_UART_PWREMU_MGMT_URRST_SHIFT;
}

void DCS6_configure(
    DCS6_Handle handle)
{
    /* Configure dMAX */
    handle->fxns->configuredMAX(handle);

    /* Configure interrupts */
    handle->fxns->configureInt(handle);
    ((Uint32*)&CSL_INTC_REGS->INTMUX1)[handle->dmax->cpuint-4>>2]=
        ((Uint32*)&CSL_INTC_REGS->INTMUX1)[handle->dmax->cpuint-4>>2]&
        ~(0xFF<<8*(handle->dmax->cpuint&3))|
        handle->dmax->cpusi<<8*(handle->dmax->cpuint&3);

    /* Configure selected device */
    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0||
        handle->params->dev==DCS6_PARAMS_DEV_SPI1)
        handle->fxns->configureSPI(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C0||
        handle->params->dev==DCS6_PARAMS_DEV_I2C1)
        handle->fxns->configureI2C(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2)
        handle->fxns->configureUART(handle);
}

void DCS6_resetSPI(
    DCS6_Handle handle)
{
    CSL_SpiRegsOvly CSL_SPI_SLAVE_REGS;

    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0)
        CSL_SPI_SLAVE_REGS=CSL_SPI_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_SPI1)
        CSL_SPI_SLAVE_REGS=CSL_SPI_1_REGS;
    CSL_SPI_SLAVE_REGS->SPIGCR0=CSL_SPI_SPIGCR0_RESETVAL;
}

void DCS6_resetI2C(
    DCS6_Handle handle)
{
    CSL_I2cRegsOvly CSL_I2C_SLAVE_REGS;

    if(handle->params->dev==DCS6_PARAMS_DEV_I2C0)
        CSL_I2C_SLAVE_REGS=CSL_I2C_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C1)
        CSL_I2C_SLAVE_REGS=CSL_I2C_1_REGS;
    CSL_I2C_SLAVE_REGS->ICMDR=CSL_I2C_ICMDR_RESETVAL;
    CSL_I2C_SLAVE_REGS->ICPFUNC=CSL_I2C_ICPFUNC_RESETVAL;
}

void DCS6_resetUART(
    DCS6_Handle handle)
{
    CSL_UartRegsOvly CSL_UART_SLAVE_REGS;

    if(handle->params->dev==DCS6_PARAMS_DEV_UART0)
        CSL_UART_SLAVE_REGS=CSL_UART_0_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART1)
        CSL_UART_SLAVE_REGS=CSL_UART_1_REGS;
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART2)
        CSL_UART_SLAVE_REGS=CSL_UART_2_REGS;
    CSL_UART_SLAVE_REGS->PWREMU_MGMT=
        CSL_UART_PWREMU_MGMT_UTRST_RESET<<CSL_UART_PWREMU_MGMT_UTRST_SHIFT|
        CSL_UART_PWREMU_MGMT_URRST_RESET<<CSL_UART_PWREMU_MGMT_URRST_SHIFT|
        CSL_UART_PWREMU_MGMT_FREE_STOP<<CSL_UART_PWREMU_MGMT_FREE_SHIFT;
}

void DCS6_reset(
    DCS6_Handle handle)
{
    /* Reset selected device */
    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0||
        handle->params->dev==DCS6_PARAMS_DEV_SPI1)
        handle->fxns->resetSPI(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C0||
        handle->params->dev==DCS6_PARAMS_DEV_I2C1)
        handle->fxns->resetI2C(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2)
        handle->fxns->resetUART(handle);
}

Uint32 DCS6_resAllocdMAX(
    DCS6_Handle handle)
{
    DCS6_Config_Dmax *pDmaxConfig=handle->config->dmax;

    /* Allocate dMAX host interrupt, CPU system interrupt and CPU interrupt */
    if((handle->dmax->cpusi=handle->dmaxHandle->fxns->allocInt(handle->dmaxHandle,
        pDmaxConfig->hint))<0){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: dMAX interrupt allocation failed");
        return 4;
    }
    handle->dmax->cpuint=handle->dmaxHandle->fxns->mapInt(handle->dmaxHandle,
        handle->dmax->cpusi);

    /* Allocate dMAX receive and error codes */
    if((handle->dmax->rc=handle->dmaxHandle->fxns->allocTCC(handle->dmaxHandle,
        pDmaxConfig->rc,handle->dmax->cpusi))<0){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: dMAX receive code allocation failed");
        return 5;
    }
    if(handle->params->edr&&
        (handle->dmax->ec=handle->dmaxHandle->fxns->allocTCC(handle->dmaxHandle,
            pDmaxConfig->ec,handle->dmax->cpusi))<0){
            handle->fxns->log(handle,handle->params->logObj,
                "DCS6: dMAX error code allocation failed");
            return 6;
    }

    /* Allocate dMAX parameter table */
    if(!(handle->dmax->param=(DCS6_dMAXParam*)handle->dmaxHandle->fxns->allocPT(
        handle->dmaxHandle,dMAX_MAX1_ID,
        handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2?sizeof(DCS6_dMAXUartParam):
        sizeof(DCS6_dMAXParam)))){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: dMAX parameter allocation failed");
        return 7;
    }

    return 0;
}

void DCS6_resAllocSPI(
    DCS6_Handle handle)
{
    handle->fxns->taskDisable(handle);
    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0){
        CSL_BOOTCFG_0_REGS->PINMUX7=
            CSL_BOOTCFG_0_REGS->PINMUX7&0x00000FFF|0x11111000;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_SPI0_DSP<<CSL_BOOTCFG_SUSPSRC_SPI0_SHIFT;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_SPI1){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0x0FFFF000|0x10000111;
        CSL_BOOTCFG_0_REGS->PINMUX9=
            CSL_BOOTCFG_0_REGS->PINMUX9&0xFFFFFFF0|0x00000001;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_SPI1_DSP<<CSL_BOOTCFG_SUSPSRC_SPI1_SHIFT;
    }
    handle->fxns->taskEnable(handle);
}

void DCS6_resAllocI2C(
    DCS6_Handle handle)
{
    handle->fxns->taskDisable(handle);
    if(handle->params->dev==DCS6_PARAMS_DEV_I2C0){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0xFFF00FFF|0x00022000;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_I2C0_DSP<<CSL_BOOTCFG_SUSPSRC_I2C0_SHIFT;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C1){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0xFFFFFF00|0x00000022;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_I2C1_DSP<<CSL_BOOTCFG_SUSPSRC_I2C1_SHIFT;
    }
    handle->fxns->taskEnable(handle);
}

void DCS6_resAllocUART(
    DCS6_Handle handle)
{
    handle->fxns->taskDisable(handle);
    if(handle->params->dev==DCS6_PARAMS_DEV_UART0){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0xFFF00FFF|0x00011000;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART0_DSP<<CSL_BOOTCFG_SUSPSRC_UART0_SHIFT;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART1){
        CSL_BOOTCFG_0_REGS->PINMUX11=
            CSL_BOOTCFG_0_REGS->PINMUX11&0xFFFF00FF|0x00001100;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART1_DSP<<CSL_BOOTCFG_SUSPSRC_UART1_SHIFT;
    }
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART2){
        CSL_BOOTCFG_0_REGS->PINMUX8=
            CSL_BOOTCFG_0_REGS->PINMUX8&0x0FFFFFFF|0x20000000;
        CSL_BOOTCFG_0_REGS->PINMUX9=
            CSL_BOOTCFG_0_REGS->PINMUX9&0xFFFFFFF0|0x00000002;
        CSL_BOOTCFG_0_REGS->SUSPSRC|=
            CSL_BOOTCFG_SUSPSRC_UART2_DSP<<CSL_BOOTCFG_SUSPSRC_UART2_SHIFT;
    }
    handle->fxns->taskEnable(handle);
}

Uint32 DCS6_resAlloc(
    DCS6_Handle handle)
{
    /* Allocate buffers */
    handle->rxBuf=(Uint16*)handle->fxns->memAlloc(handle,
        handle->config->bufMemSeg,handle->params->rxSize,
        handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE?
        handle->config->cacheBufAlign:4);
    handle->txBuf=(Uint16*)handle->fxns->memAlloc(handle,
        handle->config->bufMemSeg,handle->params->txSize,
        handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE?
        handle->config->cacheBufAlign:4);
    if(!handle->rxBuf||!handle->txBuf){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: Buffer allocation failed");
        return 1;
    }
    if(handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2){
        DCS6_Params_Dev_Uart *pUartParam=handle->params->pDev->pDevData.pUart;
        handle->xrxBuf=(Uint8*)handle->fxns->memAlloc(handle,
            handle->config->bufMemSeg,pUartParam->xrxSize,
            handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE?
            handle->config->cacheBufAlign:4);
        handle->xtxBuf=(Uint8*)handle->fxns->memAlloc(handle,
            handle->config->bufMemSeg,pUartParam->xtxSize,
            handle->config->cacheMode==DCS6_CONFIG_CACHEMODE_ENABLE?
            handle->config->cacheBufAlign:4);
        if(!handle->xrxBuf||!handle->xtxBuf){
            handle->fxns->log(handle,handle->params->logObj,
                "DCS6: Uart buffer allocation failed");
            return 1;
        }
    }

    /* Allocate semaphore for synchronization between task and interrupt */
    if(!(handle->sem=handle->fxns->semAlloc(handle))){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: Semaphore allocation failed");
        return 2;
    }

    /* Allocate dMAX resources */
    if(handle->fxns->resAllocdMAX(handle)){
        handle->fxns->log(handle,handle->params->logObj,
            "DCS6: dMAX resources allocation failed");
        return 3;
    }

    /* Allocate selected device */
    if(handle->params->dev==DCS6_PARAMS_DEV_SPI0||
        handle->params->dev==DCS6_PARAMS_DEV_SPI1)
        handle->fxns->resAllocSPI(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_I2C0||
        handle->params->dev==DCS6_PARAMS_DEV_I2C1)
        handle->fxns->resAllocI2C(handle);
    else if(handle->params->dev==DCS6_PARAMS_DEV_UART0||
        handle->params->dev==DCS6_PARAMS_DEV_UART1||
        handle->params->dev==DCS6_PARAMS_DEV_UART2)
        handle->fxns->resAllocUART(handle);

    return 0;
}

DCS6_Handle DCS6_open(
    const DCS6_Params *p,
    const DCS6_Config *c,
    const DCS6_Fxns *f,
    dMAX_Handle h,
    ACP_Handle a)
{
    if(!p)p=(DCS6_Params*)&DCS6_PARAMS;
    if(!c)c=(DCS6_Config*)&DCS6_CONFIG;
    if(!f)f=(DCS6_Fxns*)&DCS6_FXNS;
    f->log(NULL,p->logObj,"DCS6: LOG started");
    if(!h||!a){
        f->log(NULL,p->logObj,"DCS6: Invalid parameters");
    }
    else{
        /* Allocate space for DCS6 object */
        if(!(DCS6_HANDLE=f->memAlloc(NULL,c->objMemSeg,
            sizeof(DCS6_Obj)+sizeof(DCS6_dMAXObj),4))){
            f->log(NULL,p->logObj,"DCS6: Object allocation failed");
        }
        else{
            DCS6_HANDLE->size=sizeof(DCS6_Obj);
            DCS6_HANDLE->fxns=f;
            DCS6_HANDLE->params=p;
            DCS6_HANDLE->config=c;
            DCS6_HANDLE->dmaxHandle=h;
            DCS6_HANDLE->acp=a;
            DCS6_HANDLE->dmax=(DCS6_dMAXObj*)((Uint32)DCS6_HANDLE+sizeof(DCS6_Obj));
            DCS6_HANDLE->ecnt=0;
    
            /* Allocate resources */
            if(!DCS6_HANDLE->fxns->resAlloc(DCS6_HANDLE)){
                /* Configure */
                DCS6_HANDLE->fxns->reset(DCS6_HANDLE);
                DCS6_HANDLE->fxns->configure(DCS6_HANDLE);
                DCS6_HANDLE->fxns->errorInit();
            }
        }
    }

    return DCS6_HANDLE;
}

