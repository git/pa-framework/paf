
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// RingIO Data Driver implementation
//


#include <stdio.h>
#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sys.h>

#include "dri.h"
#include "drierr.h"
#include "dri_ringio.h"

#include <string.h>

#include <clk.h>
#include <logp.h>

//grab from samrat.c
#include <paftyp.h>
#include <stdasp.h>
#include <math.h>

#define PI		3.1415926f

Int   DRI_ctrl (DEV_Handle device, Uns code, Arg Arg);
Int   DRI_idle (DEV_Handle device, Bool Flush);
Int   DRI_issue (DEV_Handle device);
Int   DRI_open (DEV_Handle device, String Name);
Int   DRI_reclaim (DEV_Handle device);
Int   DRI_requestFrame (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   DRI_reset (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   DRI_waitForData (DEV_Handle device, PAF_InpBufConfig *pBufConfig, XDAS_UInt32 count);

// Driver function table.
DRI_Fxns DRI_FXNS = {
    NULL,              // close not used in PA/F systems
    DRI_ctrl,
    DRI_idle,
    DRI_issue,
    DRI_open,
    NULL,              // ready not used in PA/F systems
    DRI_reclaim,
    DRI_requestFrame,
    DRI_reset,
    DRI_waitForData
};

// macros assume pDevExt is available and pDevExt->pFxns is valid

#define DRI_FTABLE_requestFrame(_a,_b)        (*pDevExt->pFxns->requestFrame)(_a,_b)
#define DRI_FTABLE_reset(_a,_b)               (*pDevExt->pFxns->reset)(_a,_b)
#define DRI_FTABLE_waitForData(_a,_b,_c)      (*pDevExt->pFxns->waitForData)(_a,_b,_c)

#define DEFAULT_AUTOREQUESTSIZE  128
// .............................................................................
// syncState

enum
{
    SYNC_NONE,
    SYNC_ONE,   
    SYNC_PCM_FORCED
   };

#if 0
// -----------------------------------------------------------------------------
static void _fillSine (int * p, int size)
{
    static unsigned int j = 0;
    int               srv = 48000;
    float             sri = 1.0f / 48000.0f;
    int              samp = j % srv;
    float            time = samp * sri;
    float            tinc = sri;
    float          omega1 = 2 * PI * 500.0f;
    float          omega2 = 2 * PI * 2000.0f;
    int                 i;

    for (i = 0; i < size/8; i++) {

		  *p++ = ((short) (32767 *  sinf (omega1 * time)) << 16) & 0xFFFF0000;
        *p++ = ((short) (32767 *  sinf (omega2 * time)) << 16) & 0xFFFF0000;

        time += tinc;
    }
    j = samp + 256;

    return;
} //_fillSine
#endif

// -----------------------------------------------------------------------------
inline void IncrementPtr (PAF_InpBufConfig *pBufConfig, Ptr *pPtr, int numWords)
{
    int addr;


    addr = (int) *pPtr + numWords*pBufConfig->sizeofElement;
    if (addr > ((int) pBufConfig->base.pVoid + pBufConfig->sizeofBuffer - 1))
        addr -= pBufConfig->sizeofBuffer;

    *pPtr = (Ptr) addr;
    return;
} // IncrementPtr

// -----------------------------------------------------------------------------

inline int GetNumAvail (PAF_InpBufConfig * pBufConfig)
{
    int numBytes;

    if ((Uns) pBufConfig->head.pVoid >= (Uns) pBufConfig->pntr.pVoid)
        numBytes = ((int) pBufConfig->head.pVoid - (int) pBufConfig->pntr.pVoid);
    else
        numBytes = ((int) pBufConfig->head.pVoid - (int) pBufConfig->pntr.pVoid + pBufConfig->sizeofBuffer);

    // return in words
    return (numBytes / pBufConfig->sizeofElement);
}  // GetNumAvail

// -----------------------------------------------------------------------------

static Void _DRI_RingIO_reader_notify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) param;
    (Void) handle;		// To avoid compiler warning


    // sanity check -- param should never be null
    if (!pDevExt)
        return;

    switch (msg) {

        case NOTIFY_DATA_END:
            // Got data transfer end notification from GPP
            pDevExt->freadEnd = TRUE;
            break;

        default:
            break;
    }

    SEM_post ((SEM_Handle) & (pDevExt->readerSemObj));

    return;
} //_DRI_RingIO_reader_notify

// -----------------------------------------------------------------------------

Int DRI_issue (DEV_Handle device)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                   status = 0;
    DEV_Frame          *srcFrame;
    PAF_InpBufConfig *pBufConfig;


    srcFrame   = QUE_get (device->todevice);
    pBufConfig = (PAF_InpBufConfig *) srcFrame->addr;
    if (!pBufConfig || !pBufConfig->pBufStatus)
        return SYS_EINVAL;

    QUE_put (device->fromdevice, srcFrame);
	if (pDevExt->syncState == SYNC_NONE) 
        status = DRI_FTABLE_reset (device, pBufConfig);    
	// if first issue after open then init internal buf config view	
	
	if (srcFrame->arg == PAF_SIO_REQUEST_NEWFRAME) {    
   
		status = DRI_FTABLE_requestFrame (device, &pDevExt->bufConfig);
	}
	
	return status;
}// DRI_issue

// -----------------------------------------------------------------------------
// This function uses the local definition of frameLength and lengthofData in
// pDevExt to request the next frame of data.

Int DRI_requestFrame (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    int                   status = 0;


    // flush previous frame
    pBufConfig->pntr.pVoid   = pBufConfig->head.pVoid;

	// update bufConfig with info for use in next reclaim call
    // the interface to DRI is based on a single frame. So the amount
    // of data requested in this issue is assumed to be what is wanted in the next
    // reclaim.
	pDevExt->lengthofData = pDevExt->frameLength;
    pBufConfig->frameLength  = pDevExt->frameLength;
    pBufConfig->lengthofData = pDevExt->lengthofData;

    return status;
} // DRI_requestFrame

// -----------------------------------------------------------------------------

Int DRI_waitForData (DEV_Handle device, PAF_InpBufConfig * pBufConfig, XDAS_UInt32 count)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    int  status;

		while (GetNumAvail (pBufConfig) < count) {
        // check that decoding still requested -- allows for status
        // register to be updated via IOS to cancel autoProcessing
        if (pDevExt->pDecodeStatus) {
            if (pDevExt->pDecodeStatus->sourceSelect == PAF_SOURCE_NONE)
                return DRIERR_SYNC;
        }

		status = DRI_RingIO_Reclaim (pDevExt, pBufConfig);

        if (status)
            return status;
    }
    return 0;
} // DRI_waitForData

// -----------------------------------------------------------------------------
// Although interface allows for arbitrary BufConfigs we only support 1 -- so
// we can assume the one on the fromdevice is the one we want

Int DRI_reclaim (DEV_Handle device)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                   status = 0;
    Uint32         validDataLevel = 0;
    DEV_Frame          *dstFrame;
    PAF_InpBufConfig *pBufConfig;

	PAD_MetaData attrs;
	Uint32 meta_size;
	Uint32 param;
	Uint16 type;
    

	meta_size = sizeof(attrs);

    dstFrame = (DEV_Frame *) QUE_head (device->fromdevice);

    if (dstFrame == (DEV_Frame *) device->fromdevice)
        return DRIERR_UNSPECIFIED;
    if (!dstFrame->addr)
        return DRIERR_UNSPECIFIED;

    pBufConfig = (Ptr) dstFrame->addr;
    dstFrame->size = sizeof (PAF_InpBufConfig);

    // If we haven't reached prefill yet, as indicated by firstRead, then
    // check valid size. If still don't have enough then indicate with sync
    // error and keep sourceProgram to unknown. For cases where the input
    // file is smaller than the prefill threshold we move on since we have
    // all the data we're going to get.
    if (pDevExt->firstRead == TRUE) {
	
        validDataLevel = RingIO_getValidSize (pDevExt->RingHandle);

        if ((validDataLevel < (pDevExt->frameLength*2*READER_BEGIN_SIZE_INFRAMES)) && (pDevExt->freadEnd != TRUE)) {
                status = DRIERR_SYNC;		
				return status;
        }
        else {
            pDevExt->firstRead     = FALSE;
        }
    }	
	

    if ((pDevExt->syncState == SYNC_NONE) || (dstFrame->arg == PAF_SIO_REQUEST_SYNC)) {
		
		status = RingIO_getvAttribute (pDevExt->RingHandle, &type, &param, (RingIO_BufPtr)&attrs, &meta_size) ;
		if((RINGIO_SUCCESS == status) || (RINGIO_SPENDINGATTRIBUTE == status)) 
		{
			pDevExt->meta = attrs;
		}
			
	    switch (pDevExt->meta.codec) 
		{
        case PCM: //stereo 48kHz/
			if((pDevExt->sourceSelect == PAF_SOURCE_PCM) || (pDevExt->sourceSelect == PAF_SOURCE_AUTO))
			{
				int sizeofStride;
				pDevExt->sourceProgram = PAF_SOURCE_PCM;
			    pDevExt->bufConfig.deliverZeros = 1;
				pDevExt->syncState = SYNC_PCM_FORCED;
				pDevExt->bufConfig.sizeofElement = pDevExt->meta.sample_width; //4;
				pDevExt->bufConfig.stride        = pDevExt->meta.num_channels; //2;
				pDevExt->frameLength = pDevExt->meta.frame_size;//256*num_channels
				sizeofStride             = pDevExt->bufConfig.sizeofElement * pDevExt->frameLength;
				pDevExt->bufConfig.sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride;				
			
			}
            break;

        case DSD:
			if (pDevExt->sourceSelect == PAF_SOURCE_DSD1 || pDevExt->sourceSelect == PAF_SOURCE_DSD2 ||
				pDevExt->sourceSelect == PAF_SOURCE_DSD3 || pDevExt->sourceSelect == PAF_SOURCE_AUTO)
			{
				int sizeofStride;
				pDevExt->sourceProgram = PAF_SOURCE_DSD1;//pDevExt->sourceSelect;
			    pDevExt->bufConfig.deliverZeros = 1;
				pDevExt->syncState = SYNC_PCM_FORCED;
				pDevExt->bufConfig.sizeofElement = pDevExt->meta.sample_width; //4;
				pDevExt->bufConfig.stride        = pDevExt->meta.num_channels*2; //4;
				sizeofStride             = pDevExt->bufConfig.sizeofElement * pDevExt->bufConfig.stride * pDevExt->pcmFrameLength;
				pDevExt->bufConfig.sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride;
				/*TODO: Framelength calculation has to be moved to ARM side application and should be passed through the metadata information.*/
				if (pDevExt->sourceSelect == PAF_SOURCE_DSD1)
					pDevExt->frameLength = 256;
				else if (pDevExt->sourceSelect == PAF_SOURCE_DSD2)
					pDevExt->frameLength = 128;
				else if (pDevExt->sourceSelect == PAF_SOURCE_DSD3)
					pDevExt->frameLength = 64;
		  
				pDevExt->frameLength = pDevExt->bufConfig.stride*pDevExt->frameLength; //pBufConfig->sizeofElement*pBufConfig->stride*pDevExt->frameLength;				//pDevExt->frameLength  = pDevExt->lengthofData;
			}
            break;
        case AC3:
			if (pDevExt->sourceSelect == PAF_SOURCE_AC3 || pDevExt->sourceSelect == PAF_SOURCE_AUTO)
			{
				int sizeofStride;
				pDevExt->sourceProgram = PAF_SOURCE_AC3;//pDevExt->sourceSelect;
			    pDevExt->bufConfig.deliverZeros = 0;
				pDevExt->syncState = SYNC_ONE;
	            pDevExt->frameLength = pDevExt->meta.frame_size;//2*1536
				pDevExt->bufConfig.sizeofElement = pDevExt->meta.sample_width;
				pDevExt->bufConfig.stride        = pDevExt->meta.num_channels;
				sizeofStride             = pDevExt->bufConfig.sizeofElement * pDevExt->bufConfig.stride;
				pDevExt->bufConfig.sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride;
				pDevExt->pSync = pDevExt->bufConfig.pntr;
				pDevExt->headerSize = 0;
			}
			break;
        case EAC3:
			if (pDevExt->sourceSelect == PAF_SOURCE_DDP || pDevExt->sourceSelect == PAF_SOURCE_AUTO)
			{
				int sizeofStride;
				pDevExt->sourceProgram = PAF_SOURCE_DDP;//pDevExt->sourceSelect;
			    pDevExt->bufConfig.deliverZeros = 0;
				pDevExt->syncState = SYNC_ONE;
	            pDevExt->frameLength = pDevExt->meta.frame_size;//1536*2*4
				pDevExt->bufConfig.sizeofElement = pDevExt->meta.sample_width;
				pDevExt->bufConfig.stride        = pDevExt->meta.num_channels;
				sizeofStride             = pDevExt->bufConfig.sizeofElement * pDevExt->bufConfig.stride;
				pDevExt->bufConfig.sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride;
				pDevExt->pSync = pDevExt->bufConfig.pntr;
				pDevExt->headerSize = 0;
			}
			break;
		case THD:
			if (pDevExt->sourceSelect == PAF_SOURCE_THD || pDevExt->sourceSelect == PAF_SOURCE_AUTO)
			{
				int sizeofStride;
				pDevExt->sourceProgram = PAF_SOURCE_THD;//pDevExt->sourceSelect;
			    pDevExt->bufConfig.deliverZeros = 0;
				pDevExt->syncState = SYNC_ONE;
	            pDevExt->frameLength = pDevExt->meta.frame_size;//1536*2*4
				pDevExt->bufConfig.sizeofElement = pDevExt->meta.sample_width;
				pDevExt->bufConfig.stride        = pDevExt->meta.num_channels;
				sizeofStride             = pDevExt->bufConfig.sizeofElement * pDevExt->bufConfig.stride;
				pDevExt->bufConfig.sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride;
				pDevExt->pSync = pDevExt->bufConfig.pntr;
				pDevExt->headerSize = 4; //TODO IEC_HEADER_SIZE in MAT
			}
			break;
		}//codec
		
        if (pDevExt->syncState == SYNC_NONE)
            return DRIERR_SYNC;

        *pBufConfig = pDevExt->bufConfig;

    } //((pDevExt->syncState == SYNC_NONE) || (dstFrame->arg == PAF_SIO_REQUEST_SYNC))


    if (dstFrame->arg == PAF_SIO_REQUEST_NEWFRAME) {
        // wait for enough data to check for sync at expected location
        status = DRI_FTABLE_waitForData (device, &pDevExt->bufConfig, pDevExt->bufConfig.frameLength);
        if (status)
		    return status;
		
        // clear this flag immediately to play new audio ASAP
        // this check is OK for forced PCM since pcmTimeout and zeroCount are not
        // updated
        if (pDevExt->bufConfig.deliverZeros && !pDevExt->pcmTimeout &&
            (pDevExt->zeroCount < 2 * pDevExt->bufConfig.pBufStatus->zeroRunTrigger))
            pDevExt->bufConfig.deliverZeros = 0;

        // update external view of buffer with local view we do this with current 
        // frame info before the following update to the local view for the next 
        // frame to be captured. Need to subtract headerSize from lengthofData
        // since, at least for THD, it is needed by decoders.
        *pBufConfig = pDevExt->bufConfig;
        pBufConfig->lengthofData -= pDevExt->headerSize;

        // HACK: for DSD the frameLength needs to be the number of samples to generate.
        if ((pDevExt->sourceSelect >= PAF_SOURCE_DSD1) &&
            (pDevExt->sourceSelect <= PAF_SOURCE_DSD3))
		{
            pBufConfig->frameLength /= pBufConfig->stride;
		}
		
		/*TruHD MAT encoded frames has IEC header (4 Bytes). This has to be stripped off. 
		Thats why increment pointer is called to update the pointer to skip IEC header*/
		
		if (pDevExt->syncState == SYNC_ONE) {
            IncrementPtr (pBufConfig, &pBufConfig->pntr.pVoid, pDevExt->headerSize);
		} 
    } //(dstFrame->arg == PAF_SIO_REQUEST_NEWFRAME)

    return status;
} // DRI_reclaim

//--------------------------------------------------------------------------------
Int DRI_initStats (DEV_Handle device, float seed)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
	int iseed = (unsigned int)(seed+0.1);
    
    // allocate structure only once to avoid fragmentation
    if (!pDevExt->pStats) {
        pDevExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDevExt->pStats)
        {
            printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
            return SYS_EALLOC;
    }
    }
	if(iseed == 48000)
			pDevExt->pStats->dpll.dv = 3.637978807091713e-007;
	else if(iseed == 44100)
			pDevExt->pStats->dpll.dv = 3.959703462896842e-007;
	else if(iseed == 32000)
			pDevExt->pStats->dpll.dv = 5.4569682106375695e-007;

    return SYS_OK;
} //DRI_initStats

Int DRI_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
    PAF_SIO_InputStatus *tmpStatus = (PAF_SIO_InputStatus *) arg;
    Int                     status = 0;


    switch (code) {

        case PAF_SIO_CONTROL_SET_DECSTATUSADDR:
            pDevExt->pDecodeStatus = (PAF_DecodeStatus *) arg;
            break;

        case PAF_SIO_CONTROL_SET_PCMFRAMELENGTH:
            pDevExt->pcmFrameLength = (XDAS_Int32) arg;
            break;

        case PAF_SIO_CONTROL_SET_SOURCESELECT:
            pDevExt->sourceSelect = (XDAS_Int8) arg;
            break;

        case PAF_SIO_CONTROL_SET_AUTOREQUESTSIZE:
            pDevExt->autoRequestSize = (XDAS_Int16) arg;
            break;

        case PAF_SIO_CONTROL_GET_SOURCEPROGRAM:
            if (!arg)
                return DRIERR_UNSPECIFIED;
            *((XDAS_Int8 *) arg) = pDevExt->sourceProgram;
            break;

        case PAF_SIO_CONTROL_SET_IALGADDR:
            pDevExt->pSioIalg = (PAF_SIO_IALG_Obj *) arg;
            break;

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        case PAF_SIO_CONTROL_OPEN:
            // set syncState to none to force reset in first issue call after
            // open which is needed to init internal bufConfig view.
            pDevExt->syncState   = SYNC_NONE;
            pDevExt->RingHandle = PA_DSPLINK_readerHandle;
            pDevExt->linkLock    = FALSE;
            break;

        case PAF_SIO_CONTROL_GET_NUM_REMAINING:
            if (!arg)
                return DRIERR_UNSPECIFIED;
            break;

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:

            // LINK is initialized and ring open in afp_link task return unlocked
            // until this happens to force framework to skip data requests and keep
            // checking.
            if ((PA_DSPLINK_isInitialized == FALSE) || (pDevExt->RingHandle == NULL)) {
                tmpStatus->lock = 0;
                break;
            }

            // if setNotifier has not succeeded, as indicated by local lock status, then
            // call again. If it still hasn't succeeded then return unlocked to force
            // framework to skip data requests and keep trying.
            if (pDevExt->linkLock == FALSE) {

                // we swallow any error reported by this call and indicate success or failure
                // solely through the lock status
                int localStatus = RingIO_setNotifier (pDevExt->RingHandle,
                                                      RINGIO_NOTIFICATION_ONCE,
                                                      READER_WATERMARK_VALUE,
                                                      &_DRI_RingIO_reader_notify,
                                                      (RingIO_NotifyParam) pDevExt);

                pDevExt->linkLock = (localStatus == RINGIO_SUCCESS) ? TRUE : FALSE;
            }

            tmpStatus->lock = (pDevExt->linkLock == TRUE) ? 1 : 0;
            break;
		case PAF_SIO_CONTROL_ENABLE_STATS:
            DRI_initStats (device, (float) arg);
            break;
		case PAF_SIO_CONTROL_GET_STATS:
            // pass pointer to local stats structure
            // TODO: should this be a ptr to const ?
            *((Ptr *) arg) = pDevExt->pStats;
        default:
            break;
    }

    return status;
} // DRI_ctrl
// -----------------------------------------------------------------------------

Int DRI_idle (DEV_Handle device, Bool flush)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                     status = 0;


    while (!QUE_empty (device->todevice))
        QUE_enqueue (device->fromdevice, QUE_dequeue (device->todevice));

    while (!QUE_empty (device->fromdevice))
        QUE_enqueue (&((SIO_Handle) device)->framelist, QUE_dequeue (device->fromdevice));

    status = DRI_FTABLE_reset (device, NULL);
    if (status)
        return status;

    return 0;
} // DRI_idle

// -----------------------------------------------------------------------------

Int DRI_open (DEV_Handle device, String name)
{
    DRI_DeviceExtension *pDevExt;
    DEV_Device            *entry;
    (Void)                  name; // To avoid compiler warning


    // only one frame interface supported
    if (device->nbufs != 1)
        return SYS_EALLOC;

    if ((pDevExt = MEM_alloc (device->segid, sizeof (DRI_DeviceExtension), 0)) == MEM_ILLEGAL) 
    {
        SYS_error ("DRI MEM_alloc", SYS_EALLOC);
        printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
        return SYS_EALLOC;
    }
  
    pDevExt->pcmFrameLength = 0;
    pDevExt->sourceSelect   = PAF_SOURCE_NONE;
    pDevExt->autoRequestSize = DEFAULT_AUTOREQUESTSIZE;
    pDevExt->pInpBufStatus  = NULL;
    pDevExt->pDecodeStatus  = NULL;
    pDevExt->pStats  = NULL;

	pDevExt->meta.codec = NO_CODEC;
	pDevExt->meta.sampling_rate = 0;
	pDevExt->meta.frame_size = 0;
	pDevExt->meta.num_channels = 0;
	pDevExt->meta.sample_width = 0;
	pDevExt->meta.sample_depth = 0;

    device->object          = (Ptr) pDevExt;

    // use dev match to fetch function table pointer for DRI
    name = DEV_match ("/DRI", &entry);
    if (entry == NULL) {
        SYS_error ("DRI", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns     = (DRI_Fxns *) entry->fxns;
    pDevExt->syncState = SYNC_NONE;
	pDevExt->frameLength = 1024;
    SEM_new (&(pDevExt->readerSemObj), 0);

    return 0;
} // DRI_open

// -----------------------------------------------------------------------------
// Although this is void it is still needed since BIOS calls all DEV inits on
// startup.

Void DRI_init (Void)
{
} // DRI_init

// -----------------------------------------------------------------------------

Int DRI_reset (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;


    if (pBufConfig) {
        int sizeofStride;

        pBufConfig->pntr          = pBufConfig->base;
        pBufConfig->head          = pBufConfig->base;
        pBufConfig->futureHead    = pBufConfig->base;
        pBufConfig->lengthofData  = 0;

        pBufConfig->sizeofElement = 2; //default.
        pBufConfig->stride        = 2; //default.

        // compute and use *effective buffer size*
        sizeofStride             = pBufConfig->sizeofElement * pBufConfig->stride;
        pBufConfig->sizeofBuffer = pBufConfig->allocation / sizeofStride * sizeofStride; //////TODO

        // hack -- save status context for use in close
        pDevExt->pInpBufStatus                = pBufConfig->pBufStatus;
        pBufConfig->pBufStatus->lastFrameFlag = 0;

        // initialize internal view with external buf config
        pDevExt->bufConfig = *pBufConfig;
        pDevExt->pSync = pBufConfig->base;
    }

    pDevExt->syncState     = SYNC_NONE;
    pDevExt->pcmTimeout    = 0;
    pDevExt->zeroCount     = 0;
    pDevExt->sourceProgram = PAF_SOURCE_UNKNOWN;

    if (pDevExt->pInpBufStatus)
        pDevExt->pInpBufStatus->zeroRun = 0;

    pDevExt->headerSize = 0;

    // indicate the ring is empty
    pDevExt->freadEnd  = FALSE;
    pDevExt->firstRead = TRUE;
	pDevExt->frameLength = 1024;
	pDevExt->meta.codec = NO_CODEC;
	pDevExt->meta.sampling_rate = 0;
	pDevExt->meta.frame_size = 0;
	pDevExt->meta.num_channels = 0;
	pDevExt->meta.sample_width = 0;
	pDevExt->meta.sample_depth = 0;
    SEM_reset (&(pDevExt->readerSemObj), 0);

    return 0;
} //DRI_reset

// -----------------------------------------------------------------------------
