
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// RingIO Data Driver declarations
//
// MID 2021 RingIO Input Driver

#ifndef DRI_H
#define DRI_H

#include <std.h>

#include <dev.h>
#include <sio.h>
#include <xdas.h>

#include <inpbuf.h>
#include <pafdec.h>
#include <pafsio.h>
#include <pafsio_ialg.h>
#include <paftyp.h>

#include <ringio.h>

// .............................................................................

#define RING_IO_NAME     			"RINGIO_ARM_DSP_01"
#define NOTIFY_DATA_END         	4u
#define READER_WATERMARK_VALUE  	1024	//If there is no data in RingIO buffer reader waits for atleast this level of data
#ifndef READER_BEGIN_SIZE_INFRAMES
  #define READER_BEGIN_SIZE_INFRAMES  3    // Number of frames to be filled before calling the decoder
#else
  #warn Using READER_BEGIN_SIZE_INFRAMES from local compilation.
#endif

// HACK: grab globals from afp_link task
extern Bool PA_DSPLINK_isInitialized;
extern RingIO_Handle PA_DSPLINK_readerHandle;

enum PAD_audio_Codec_Type
{
  NO_CODEC = 0,
  PCM = 1,     //PCM, mono stereo or multichannel
  DSD = 2,     //Direct Stream Digital
  AC3 = 3,     //Dolby Digital
  EAC3 = 4,    //Dolby Digital Plus
  THD = 5	   //TrueHD
};

typedef struct PAD_MetaData
{
  enum PAD_audio_Codec_Type codec; //Raw-PCM or Encoded bitstream
  Uint32 sampling_rate;          //in Hz
  Uint32 frame_size;             //Number of samples per frame or size of one
                              //encoded frame for framebased codec
  Uint32 num_channels;           //Number of Channels
  Uint32 sample_width;           //Bits allocated per sample in non-frame based codec
  Uint32 sample_depth;           //Actual bits per sample in a non-frame based codec
} PAD_MetaData;

// .............................................................................
//Function table defs

typedef Int	(*DRI_TrequestFrame)(DEV_Handle, PAF_InpBufConfig *);
typedef Int	(*DRI_Treset)(DEV_Handle, PAF_InpBufConfig *);
typedef Int	(*DRI_TwaitForData)(DEV_Handle, PAF_InpBufConfig *, XDAS_UInt32);

typedef struct DRI_Fxns {
    //common (must be same as DEV_Fxns)
    DEV_Tclose		close;
    DEV_Tctrl		ctrl;
    DEV_Tidle		idle;
    DEV_Tissue		issue;
    DEV_Topen		open;
    DEV_Tready		ready;
    DEV_Treclaim	reclaim;

    //DRI specific
    DRI_TrequestFrame       requestFrame;
    DRI_Treset              reset;
    DRI_TwaitForData        waitForData;

} DRI_Fxns;

extern DRI_Fxns DRI_FXNS;

extern Void DRI_init (Void);

// .............................................................................

typedef struct DRI_DeviceExtension {

    DEV_Handle           device;
    DRI_Fxns            *pFxns;

    XDAS_UInt32          zeroCount;
    XDAS_Int8            sourceSelect;
    XDAS_Int8            sourceProgram;

    XDAS_Int8            syncState;

    XDAS_Int16           headerSize;  // in words
    XDAS_Int16           autoRequestSize;

	// second buffer info
    XDAS_Int32           frameLength;
    XDAS_Int32           lengthofData;
    PAF_UnionPointer     pSync;
 
	XDAS_Int32           pcmFrameLength;
    XDAS_UInt32          pcmTimeout;

	PAF_SIO_Stats        *pStats;

    //hack
    PAF_InpBufStatus    *pInpBufStatus;
    PAF_DecodeStatus    *pDecodeStatus;

    PAF_SIO_IALG_Obj    *pSioIalg;
    PAF_InpBufConfig     bufConfig;

    // RingIO specifics
    SEM_Obj       readerSemObj;   // Reader SEM object
    RingIO_Handle RingHandle;   // DSP Side Ring IO Reader-Handle
    Bool              freadEnd;   // Indicates EOS Notification 
    Bool             firstRead;   // Indicates first-time read
	Bool              linkLock;   // Indicates DSPLINK handshake status

	PAD_MetaData		meta; //Codec Meta Info coming from PAD

} DRI_DeviceExtension;

// .............................................................................

#endif /* DRI_H */
