
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// LINK interface to DRO driver
//
//
//


#include <std.h>
#include <log.h>
#include <swi.h>
#include <sys.h>
#include <sio.h>
#include <tsk.h>
#include <pool.h>
#include <gbl.h>
#include <string.h>

#include <failure.h>
#include <dsplink.h>
#include <platform.h>
#include <notify.h>

#include <ringio.h>
#include <sma_pool.h>

#include <paftyp.h>

#include "dro.h"
#include "dro_ringio.h"
#include "droerr.h"

#include <csl_dat.h>
#include <pafhjt.h>

#include <ti/sysbios/hal/Cache.h>

/* DRO driver can be used either in primary or secondary output. 
To use DRO in secondary output define DRO_SECONDARY. Undefine if DRO is in Primary output.*/

//#define DRO_SECONDARY

/*DRO driver can be used either in synchronous(real time input) system or in Asynchronous(offline input through DRI driver) system.
If DRO is used in synchronous system define DRO_SYNCHRONOUS otherwise undefine this. */

#define DRO_SYNCHRONOUS

/*Behaviour of DRO module in different scenario
-------------------------------------------------------------------------------------
					|	Real time input			|		Offline input				|
					|	(synchronous.System)	|		Asynchronous System)		|					
					-----------------------------------------------------------------
RRingIO as Primary	|	Return with error		|	Wait till space is available*	|
-------------------------------------------------------------------------------------
RRingIO as Secondary|	If RRingIO is secondary then primary is real time output. 	|
					|	So flush and write data is the best option. 				|
					|		It won�t affect primary audio.							|
// ----------------------------------------------------------------------------------

*/

void FillData (char *,PAF_OutBufConfig *, Uint16, Uint32);

Int DRO_RingIO_Issue (DRO_DeviceExtension *pDevExt,
                        PAF_OutBufConfig * pBufConfig,
                        Uint32 size)

{
    Int		wrRingStatus = RINGIO_SUCCESS;
    Int     status = SYS_OK;
    Uint32  writerAcqSize = 0;
    Char    * writerBuf;
	Uint16 type;
	Uint32 param,bytesFlushed;
	
	if (pDevExt->state == DRO_STATE_IDLE)
		RingIO_flush (pDevExt->RRingHandle, TRUE, &type, &param, &bytesFlushed);
	
    while (size != 0)
	{

        writerAcqSize = size;
        wrRingStatus  = RingIO_acquire (pDevExt->RRingHandle, (RingIO_BufPtr *) &writerBuf, &writerAcqSize);
		
        if(wrRingStatus == RINGIO_EBUFFULL)
		{
	#ifdef DRO_SECONDARY
			
			status = RingIO_flush (pDevExt->RRingHandle, TRUE, &type, &param, &bytesFlushed);
			if (status != RINGIO_SUCCESS)
			{
				SET_FAILURE_REASON (status);								
			}	
			return SYS_OK;
#else				
			
	#ifdef DRO_SYNCHRONOUS			
			return wrRingStatus;
	#else
			status = SYS_OK;
	#endif
	
#endif				
		}
			
        else if ((wrRingStatus == RINGIO_SUCCESS) || ((writerAcqSize > 0) && (wrRingStatus == RINGIO_EBUFWRAP)))
        {

           	FillData (writerBuf, pBufConfig,pDevExt->dri_dro_link,writerAcqSize);
			
			size -= writerAcqSize;
			wrRingStatus = RingIO_release (pDevExt->RRingHandle, writerAcqSize);
				
           	if (wrRingStatus != RINGIO_SUCCESS)
              	SET_FAILURE_REASON (wrRingStatus);	

			// update the buffer pointer as there may be more data to be read because of partial acquire size
            pBufConfig->pntr.pSmInt += writerAcqSize;
	    } 
			
		else
		{
            status = RINGIO_EFAILURE;
            SET_FAILURE_REASON (status);
            return status;
        } 

    } //while			 
	//Output buffer pointer has to be updated after reading all the contents from the buffer.
	//writerAcqSize is same as the size PA/F writes at once (one frame)
	pBufConfig->pntr.pVoid = pBufConfig->base.pVoid;
    return status;
} //DRO_RingIO_Reclaim

inline void FillData (char* destptr,
                         PAF_OutBufConfig * pBufConfig,
                         Uint16 dri_dro_link,  // true if working on the same DSP, no cache invalidate required.
                         Uint32 copySize )
{
	Int datid;
    Char *readPtr = (char *)pBufConfig->pntr.pVoid; 
					 
    if (dri_dro_link)
    {
        Cache_inv(readPtr, copySize, Cache_Type_ALL, TRUE);
        memcpy (destptr, readPtr, copySize);
        Cache_wb(destptr, copySize, Cache_Type_ALL, TRUE);
    }
    else
    {
        Int datid;
        // memcpy replaced with DAT_copy.
        // must handle caching for off chip destinations
        Cache_inv(readPtr,copySize,Cache_Type_ALL,TRUE);
        datid = DAT_copy (readPtr, destptr,copySize);
        DAT_wait(datid);
        Cache_inv(destptr,copySize,Cache_Type_ALL,TRUE);
    }
    return;
}


// -----------------------------------------------------------------------------
