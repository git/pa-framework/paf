
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// PA/F I2C Master definitions header file
//
//
//

#define I2C_DEV0  0
#define I2C_DEV1  1
#define _I2C_BASE_PORT0  0x49000000
#define _I2C_BASE_PORT1  0x4A000000
#define _I2C_I2COAR_OFFSET 0x0
#define _I2C_I2CIER_OFFSET 0x1
#define _I2C_I2CSTR_OFFSET 0x2
#define _I2C_I2CCLKL_OFFSET 0x3
#define _I2C_I2CCLKH_OFFSET 0x4
#define _I2C_I2CCNT_OFFSET 0x5
#define _I2C_I2CDRR_OFFSET 0x6
#define _I2C_I2CSAR_OFFSET 0x7
#define _I2C_I2CDXR_OFFSET 0x8
#define _I2C_I2CMDR_OFFSET 0x9
#define _I2C_I2CISR_OFFSET 0xa
#define _I2C_I2CEMDR_OFFSET 0xb
#define _I2C_I2CPSC_OFFSET 0xc
#define _I2C_I2CPID1_OFFSET 0xd
#define _I2C_I2CPID2_OFFSET 0xe
#define _I2C_I2CPFUNC_OFFSET 0x12
#define _I2C_I2CPDIR_OFFSET 0x13
#define _I2C_I2CPDIN_OFFSET 0x14
#define _I2C_I2CPDOUT_OFFSET 0x15
#define _I2C_I2CPDSET_OFFSET 0x16
#define _I2C_I2CPDCLR_OFFSET 0x17
#define _I2C_I2CSTR_ICRRDY_MASK 0x8
#define _I2C_I2CSTR_ICXRDY_MASK 0x10
#define _I2C_I2CSTR_AL_MASK 0x1
#define _I2C_I2CSTR_BB_MASK 0x1000
#define _I2C_I2CSTR_NACK_MASK 0x2
#define _I2C_I2CSTR_ICXRDY_MASK 0x10
#define _I2C_I2CMDR_STT_MASK 0x2000
#define _I2C_I2CMDR_MST_MASK 0x400

#define _I2C_I2CIMR_OFFSET _I2C_I2CIER_OFFSET
#define _I2C_I2CIVR_OFFSET _I2C_I2CISR_OFFSET
