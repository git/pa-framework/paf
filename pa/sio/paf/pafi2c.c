
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// PA/F I2C Master definitions
//
//
//

#include <std.h>
#include <sem.h>
#include <tsk.h>

#include <t:\pa\sio\paf\i2c.h>

#include <pafi2c.h>

void PAF_I2C_open (Int dev);
void PAF_I2C_close (Int dev);
Int PAF_I2C_message (Int mode, Int dev, unsigned char *data, Int length, Int timeOut, Int slaveAddress);

#define STATIC
#ifndef STATIC
#define STATIC static
#endif

//two i2c peripherals
STATIC SEM_Handle hI2CSem[2] = {NULL, NULL};

#define DEVCFG (*(volatile LgUns *)_CHIP_DEVCFG_ADDR)
/* Replace this with what that is more proper? --Kurt */

Int PAF_I2C_init (Int dev)
{
    if ((dev != I2C_DEV0) && (dev != I2C_DEV1))
        return 1;

    // create SEM handle if needed,
    // and initialize to ready by setting count to 1
    // need to prevent re-entrancy at this point
    TSK_disable();
    if (hI2CSem[dev] == NULL) {
        hI2CSem[dev] = SEM_create (1, NULL);
        if (hI2CSem[dev] == NULL) {
            TSK_enable ();
            return 1;
        }
    }
    TSK_enable ();

    // wait for semaphore
    SEM_pend (hI2CSem[dev], SYS_FOREVER);

    PAF_I2C_open (dev);
    
    // signal resource is free
    SEM_post (hI2CSem[dev]);

    return 0;
} // PAF_I2C_init


Int PAF_I2C_write (Int dev, Int addr, Int cnt, const SmUns *data)
{
    Int result;


    if ((dev != I2C_DEV0) && (dev != I2C_DEV1))
        return 1;

    // need init to create sem handles
    if (hI2CSem[dev] == NULL)
        return 1;

    // wait for semaphore
    SEM_pend (hI2CSem[dev], SYS_FOREVER);

    result = PAF_I2C_message (1, dev, (unsigned char *)data, cnt, -1, addr);

    // signal resource is free
    SEM_post (hI2CSem[dev]);

    return result;
} // PAF_I2C_write


Int PAF_I2C_flush (Int dev)
{
    if ((dev != I2C_DEV0) && (dev != I2C_DEV1))
        return 1;

    // Null function, for now.
    return 1; /* unknown synchronicity so return error code, for now. */
} // PAF_I2C_flush

Int PAF_I2C_read (Int dev, Int addr, Int cnt, SmUns *data)
{
    Int result;


    if ((dev != I2C_DEV0) && (dev != I2C_DEV1))
        return 1;

    // need init to create sem handles
    if (hI2CSem[dev] == NULL)
        return 1;

    // wait for semaphore
    SEM_pend (hI2CSem[dev], SYS_FOREVER);

    result = PAF_I2C_message (0, dev, (unsigned char *)data, cnt, -1, addr);

    // signal resource is free
    SEM_post (hI2CSem[dev]);

    return result;
} // PAF_I2C_read


/* PAF_I2C_message :: Send/Receive the Message accross using I2C.

   return:
   0 - Success
   1 - unsupported option or invalid argument
   2 - TimeOut
   3 - Slave Not Acknowledging
   4 - AL

   Follow state machine in Figure 18 of spru877b, October 2002 (write)
   Follow state machine in Figure 21 of spru877b, October 2002 (read)

   derived from pa/boards/dsk6713/board_util/ctp/iic/ctp_dim.c rev 1.1

   note that we don't wait for Bus Free since the use of semaphores
   guarantees atmoic access. i.e. this functions are not re-entrant
   and they finish with bus free.

   I have no confidence that the system can recover from a timeout
   condition. For example, on error we do not generate a stop
   condition.
*/
          
Int PAF_I2C_message (Int mode, Int dev, unsigned char *data, Int length, Int timeOut, Int slaveAddress)
{
    Int localTimeOut = 0;
    Int index = 0;
    Int status, rdyMask;
    volatile Uint32 *base;


    if (dev == I2C_DEV0)
        base = (volatile Uint32 *) _I2C_BASE_PORT0;
    else 
        base = (volatile Uint32 *) _I2C_BASE_PORT1;
    
    // Configure the length of the message
    base[_I2C_I2CCNT_OFFSET] = length;

    // Specify the Slave Address
    base[_I2C_I2CSAR_OFFSET] = slaveAddress;
    
    // Wait for Bus Free -- see above

    // Enable the Xmt/Rcv, Master Mode and generate Start Condition
    // generate stop condition automatically when internal counter=0
    if (mode) {
        rdyMask = _I2C_I2CSTR_ICXRDY_MASK;
        base[_I2C_I2CMDR_OFFSET] = 0xE20 | _I2C_I2CMDR_STT_MASK;
    }
    else {
        length--; //read last word after stop condition
        base[_I2C_I2CMDR_OFFSET] = 0xC20 | _I2C_I2CMDR_STT_MASK;
    }

    localTimeOut = 0;
    while (length) {
        status = base[_I2C_I2CSTR_OFFSET];
        if (mode)
            rdyMask = _I2C_I2CSTR_ICXRDY_MASK;
		else
		    rdyMask = _I2C_I2CSTR_ICRRDY_MASK;

        // If NACK or AL then error
        // -- do we need to generate a stop condition?
        if (status & _I2C_I2CSTR_AL_MASK)
            return 4;
        if (status & _I2C_I2CSTR_NACK_MASK)
            return 3;    

        // if data remaining then send if XRDY or
        // if data ready then read
        if (status & rdyMask) {
            if (mode)
                base[_I2C_I2CDXR_OFFSET] = data[index++];
            else
                data[index++] = base[_I2C_I2CDRR_OFFSET];
            length--;
        }

        if ((timeOut >= 0) && (localTimeOut++ > timeOut))
            return 2;
    }

    // MST transitions from 1->0 when a STOP condition is generated
    // I believe this also implies that the bus is now free
    localTimeOut = 0;
    while ((base[_I2C_I2CMDR_OFFSET] & _I2C_I2CMDR_MST_MASK)) {
        if ((timeOut >= 0) && (localTimeOut++ > timeOut))
            return 2;
    }
    
    if (!mode)
        // read last word
        data[index++] = base[_I2C_I2CDRR_OFFSET];

    return 0;
} //PAF_I2C_message


void PAF_I2C_open (Int dev)
{
    volatile Uint32 *base;

    if (dev == I2C_DEV0)
        base = (volatile Uint32 *) _I2C_BASE_PORT0;
    else {
        base = (volatile Uint32 *) _I2C_BASE_PORT1;
//        DEVCFG_ENABLE_I2C1(); 
    }

    base[_I2C_I2CMDR_OFFSET] = 0x0;
    base[_I2C_I2CPSC_OFFSET] = 0x10;
    base[_I2C_I2CCLKL_OFFSET] = 20; //was 6
    base[_I2C_I2CCLKH_OFFSET] = 30; //was 1
    base[_I2C_I2COAR_OFFSET] = 0x1; 
} //PAF_I2C_open


void PAF_I2C_close (Int dev)
{
    volatile Uint32 *base;

    if (dev == I2C_DEV0)
        base = (volatile Uint32 *) _I2C_BASE_PORT0;
    else {
        base = (volatile Uint32 *) _I2C_BASE_PORT1;
//    DEVCFG_DISABLE_I2C1();
    }

    base[_I2C_I2COAR_OFFSET] = 0x0; 
    base[_I2C_I2CIMR_OFFSET] = 0x0;
    base[_I2C_I2CSTR_OFFSET] = 0x1410; //is this needed?
    base[_I2C_I2CCNT_OFFSET] = 0x0;
    base[_I2C_I2CDRR_OFFSET] = 0x0; //is this needed?
    base[_I2C_I2CSAR_OFFSET] = 0x3FF; //why this?
    base[_I2C_I2CDXR_OFFSET] = 0x0; //is this needed?
    base[_I2C_I2CMDR_OFFSET] = 0x0;
    base[_I2C_I2CIVR_OFFSET] = 0x0;
} //PAF_I2C_close

