
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#
#
# Tools definitions for using MV Pro5 Linux.
#
#
# -----------------------------------------------------------------------------

# ---- User customization -----------------------------------------------------

# Directory under which MontaVista tool chain is installed.
# PA_MVDIR:=/opt
PA_MVDIR:=${PA_MVDIR}

# Directory under which DSP LINK is installed.
# PA_LINKDIR:=${HOME}
PA_LINKDIR:=${PA_LINKDIR}

# Version of DSP LINK
# PA_LINKVER:=1_61_02
PA_LINKVER:=${PA_LINKVER}

# ---- End of user customization ----------------------------------------------

BASE_CGTOOLS=${PA_MVDIR}/montavista/pro/devkit/arm/v5t_le/bin
CC=${BASE_CGTOOLS}/arm_v5t_le-gcc

DSPLINK=${PA_LINKDIR}/dsplink_linux_${PA_LINKVER}/dsplink

INCLUDES  = -I.
INCLUDES += -I${PA_MVDIR}/montavista/pro/devkit/arm/v5t_le/target/usr/include
INCLUDES += -I${DSPLINK}/gpp/inc
INCLUDES += -I${DSPLINK}/gpp/inc/usr
INCLUDES += -I${DSPLINK}/gpp/inc/sys/Linux
INCLUDES += -I${DSPLINK}/gpp/inc/sys/Linux/2.6.18

DEFINES += -DPA_NO_BOOT
DEFINES += -DOS_LINUX
DEFINES += -DMAX_DSPS=1
DEFINES += -DMAX_PROCESSORS=2
DEFINES += -DID_GPP=1
DEFINES += -DDA8XX
DEFINES += -DPROC_COMPONENT
DEFINES += -DPOOL_COMPONENT
DEFINES += -DNOTIFY_COMPONENT
DEFINES += -DMPCS_COMPONENT
DEFINES += -DRINGIO_COMPONENT
DEFINES += -DMPLIST_COMPONENT
DEFINES += -DMSGQ_COMPONENT
DEFINES += -DMSGQ_ZCPY_LINK
DEFINES += -DCHNL_COMPONENT
DEFINES += -DCHNL_ZCPY_LINK
DEFINES += -DZCPY_LINK
DEFINES += -DKFILE_DEFAULT
DEFINES += -DDA8XXGEM
DEFINES += -DDA8XXGEM_PHYINTERFACE=SHMEM_INTERFACE

LDFLAGS =
LDFLAGS += -lpthread
LDFLAGS += ${DSPLINK}/gpp/export/BIN/Linux/${PLATFORM}/RELEASE/dsplink.lib

