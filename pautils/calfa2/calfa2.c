
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Command line alpha code processor
//
//
//

// TODO: add I@C addr as option
//       -h fILE support (multiple files)
//       -I fILE support (multiple files)

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include <getopt.h>

#include <pasystem.h>

#include "calfa2.h"
#include "calfa2_link.h"
#include "calfa2_i2c.h"
#include "calfa2_ip.h"

static int cleanup (CALFA_Args *pCalfaArgs);
static int parseArgs (int argc, char **argv, CALFA_Args *pCalfaArgs);
static int preprocess (int argc, char **argv, CALFA_Args *pCalfaArgs);
static int postProcess (CALFA_Args *pCalfaArgs);
static void printHelp (void);
static void printUsage (void);


static char *usage =
    "usage: calfa2 [--help] [--version]\n"
    "              [--com*] [-h*] [-I*] [FILE] [ALPHA] [-] ...";

static char *help =
    "Options:        In any order. Order is often pertinent.\n"
    "\n"
    "  -?             Display usage and exit (1)\n"
    "  --com=i2c      Communicate via /dev/i2c-0 device\n"
    "  --com=link     Communicate via DSPLINK\n"
    "  --com=ip	 Communicate via IP from Windows host calfa application\n"
    "  -h<FILE>       Add FILE_a.h to alpha code symbol definition file list\n"
    "                 Add FILE_a.hdM to alpha code symbol decompilation file list\n"
    "  -I<DIR>        Add <DIR> to the include path. Default is ./alpha\n"
    "  -r             Retain temporary files (alfa[0-9]*.*)\n"
    "  --help         Print this message and exit (0)\n"
    "  --version      Print version number and exit (0)\n"
    "\n"
    "\n"
    "Temporary Files:\n"
    "\n"
    "  The temporary files can be used to understand the exact nature of the\n"
    "  data manipulation and communication made by the tool. The temporary files \n"
    "  have names of the form `alfa[0-9]*.*', where `[0-9]*' is derived from \n"
    "  the process id and so is unique for each data exchange. The file extension \n"
    "  indicates what data that file contains:\n"
    "\n"
    "    .c  Alpha code symbolic source file corresponding to the desired command \n"
    "        sequence, may contain comments, #include statements, and both symbolic\n"
    "        and numeric (non-symbolic) alpha code sequences.\n"
    "\n"
    "    .i  Alpha code preprocessed source file (command), contains numeric\n"
    "        alpha code sequences.\n"
    "\n"
    "    .x  Alpha code object file (command), contains either S records or\n"
    "        binary data depending upon the communication medium in use.\n"
    "\n"
    "    .s  Alpha code object file (response), contains either S records or\n"
    "        binary data depending upon the communication medium in use.\n"
    "\n"
    "    .y  Alpha code preprocessed source file (response), contains numeric \n"
    "        alpha code sequences.\n"
    "\n"
    "    .z  Alpha code symbolic source file (response), contains symbolic and\n"
    "        numeric alpha code sequences but no #include statements or comments.\n"
    "\n"
    "   These files are normally deleted. These files are not deleted if the -r \n"
    "   option is specified or if the tool crashes.\n"
	"\n"
	"\n"
	"Typical Option/Argument Combinations:\n"
	"\n"
	"  # display version and exit\n"
	"  ./calfa2 --version\n"
	"\n"
	"  # check ready status using LINK transport, retain files\n"
	"  ./calfa2 -r --com=link -h pa17i11-evmda708-d708e001 readSTDReady\n"
	"\n"
	"  # turn decoding off using I2C transport\n"
    "  ./calfa2 --com=i2c -h pa17i11-evmda708-d708e001 writeDECSourceSelectNone\n"
	"\n"
	"  # Use IP mode to communicate with calfa app running on connected Windows host\n"
    "  ./calfa2 --com=ip\n";

// -----------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

int main (int argc, char* argv[])
{
    int retVal;
    CALFA_Args calfaArgs;


    // defaults
    calfaArgs.mode      = 0;
    calfaArgs.addr      = DEFAULT_I2C_ADDR;
    calfaArgs.retain    = false;
    calfaArgs.cMode     = COM_MODE_I2C;
    calfaArgs.hFileName = NULL;

    calfaArgs.iPathName = malloc (strlen ("alpha"));
    if (!calfaArgs.iPathName) {
        fprintf (stderr, "Calfa2: Error allocating memory\n");
        return 1;
    }
    strcpy (calfaArgs.iPathName, "alpha");


    retVal = parseArgs (argc, argv, &calfaArgs);
    if (retVal) {
        printf ("Error %d parsing arguments\n", retVal);
        goto commonExit;
    }

    if (calfaArgs.cMode != COM_MODE_IP) {
        retVal = preprocess (argc, argv, &calfaArgs);
        if (retVal) {
            printf ("Error %d pre processing\n", retVal);
            goto commonExit;
        }
    }

    switch (calfaArgs.cMode) {
        case COM_MODE_I2C:
            retVal = transferI2C (&calfaArgs);
            if (retVal) {
                fprintf (stderr, "Error %d transferring via I2C\n", retVal);
                goto commonExit;
            }
            break;

        case COM_MODE_LINK:	    
            retVal = transferLink (&calfaArgs);
            if (retVal) {
                fprintf (stderr, "Error %d transferring via LINK\n", retVal);
                goto commonExit;
            }
            break;

        case COM_MODE_IP:	    
            retVal = transferIP (&calfaArgs);		// Calfa2 should spawn the CalfaIP process as a daemon
            if (retVal) {
                fprintf (stderr, "Error %d transferring via IP\n", retVal);
                goto commonExit;
            }
            break;
    }

    retVal = postProcess (&calfaArgs);
    if (retVal) {
        printf ("Error %d post processing\n", retVal);
        goto commonExit;
    }

commonExit:

    cleanup (&calfaArgs);

    return 0;
} //main

// -----------------------------------------------------------------------------
// This function uses global variables from getopt.

enum
{
    LONG_OPTION_COM,
    LONG_OPTION_HELP,
    LONG_OPTION_VERSION,
};

static struct option long_options[] =
{
    {"com",      required_argument, NULL, LONG_OPTION_COM},
    {"help",     no_argument,       NULL, LONG_OPTION_HELP},
    {"version",  no_argument,       NULL,  LONG_OPTION_VERSION},
    {0,0,0,0}
};

static int parseArgs (int argc, char **argv, CALFA_Args *pCalfaArgs)
{
    int c;
    int index = 0;


    while (1) {

        c = getopt_long (argc, argv, "h:I:r?", long_options, &index);

        // Detect the end of the options.
        if (c == -1)
            break;

        switch (c) {
            case LONG_OPTION_COM:
                if ((strcmp ("I2C", optarg) == 0) ||
                    (strcmp ("i2c", optarg) == 0))
                    pCalfaArgs->cMode = COM_MODE_I2C;
                else if ((strcmp ("link", optarg) == 0) ||
                         (strcmp ("LINK", optarg) == 0))
                    pCalfaArgs->cMode = COM_MODE_LINK;
                else if ((strcmp ("ip", optarg) == 0) ||
                         (strcmp ("IP", optarg) == 0))
                    pCalfaArgs->cMode = COM_MODE_IP;
                else {
                    fprintf (stderr, "Unsupported com type: %s\n", optarg);
                    return 1;
                }
                break;

            case 'h':                
                pCalfaArgs->hFileName = malloc (strlen (optarg));
                if (!pCalfaArgs->hFileName) {
                    fprintf (stderr, "Calfa2: Error allocating memory\n");
                    return 1;
                }
                strcpy (pCalfaArgs->hFileName, optarg);
                break;

            case 'I':                
                pCalfaArgs->iPathName = realloc (pCalfaArgs->iPathName, strlen (optarg));
                if (!pCalfaArgs->iPathName) {
                    fprintf (stderr, "Calfa2: Error allocating memory\n");
                    return 1;
                }
                strcpy (pCalfaArgs->iPathName, optarg);
                break;

            case 'r':
                pCalfaArgs->retain = true;
                break;

            case LONG_OPTION_VERSION:
                fprintf (stdout, "%s\n", version);
                exit (0);
                break;

            case LONG_OPTION_HELP:
                printHelp ();
                exit (0);
                break;

            default:
            case '?':
                printUsage ();
                exit (1);
                break;
        }
    } //while

    return 0;
} //parseArgs

// -----------------------------------------------------------------------------

static int preprocess (int argc, char **argv, CALFA_Args *pCalfaArgs)
{
    unsigned char ident;
    int index, retVal;
    char processString[256];
    FILE *pFile;

    // create temporary fileNames
    srand (time(0));
    ident = rand ();
    sprintf (pCalfaArgs->cFileName, "alfa%04hhu.c", ident);
    sprintf (pCalfaArgs->iFileName, "alfa%04hhu.i", ident);
    sprintf (pCalfaArgs->sFileName, "alfa%04hhu.s", ident);
    sprintf (pCalfaArgs->xFileName, "alfa%04hhu.x", ident);
    sprintf (pCalfaArgs->yFileName, "alfa%04hhu.y", ident);
    sprintf (pCalfaArgs->zFileName, "alfa%04hhu.z", ident);

    // if first non-option argument is an existing file then use that as input
    // file otherwise interpret as alpha codes and concantentate them to a
    // temporary input file with "alpha" prefix
    pFile = fopen (argv[optind], "rt");
    if (!pFile) {
        pFile = fopen (pCalfaArgs->cFileName, "wt");
        if (pCalfaArgs->hFileName)
            fprintf (pFile, "#include \"%s_a.h\"\n", pCalfaArgs->hFileName);
        fprintf (pFile, "alpha ");
        for (index = optind; index < argc; index++)
            fprintf (pFile, "%s", argv[index]);
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    // run input file through preprocessor
    if (pFile) {
        sprintf (processString, "/usr/bin/gcc -E -I%s %s -o %s", pCalfaArgs->iPathName, pCalfaArgs->cFileName ,pCalfaArgs->iFileName);
        //printf ("/usr/bin/gcc -E -I%s %s -o %s", pCalfaArgs->iPathName, pCalfaArgs->cFileName ,pCalfaArgs->iFileName);
	retVal = pasystem (processString);
        if (retVal) {
            printf ("Error %d executing gcc preprocessor\n", retVal);
            return retVal;
        }
    }

    // create binary file from preprocessed file
    // TODO: itox is a system provided executable. Do we rename?
    sprintf (processString, "./itox < %s > %s", pCalfaArgs->iFileName, pCalfaArgs->xFileName);
    //printf("%s\n", processString);
    retVal = pasystem (processString);
    if (retVal) {
        printf ("Error %d executing itox\n", retVal);
        return retVal;
    }

    return 0;
} //preprocess

// -----------------------------------------------------------------------------

static int postProcess (CALFA_Args *pCalfaArgs)
{
    char processString[256];
    int retVal;


    sprintf (processString, "./xtoi < %s > %s", pCalfaArgs->sFileName, pCalfaArgs->yFileName);
    retVal = pasystem (processString);
    if (retVal) {
        printf ("Error %d executing itox\n", retVal);
        return retVal;
    }

    if (pCalfaArgs->hFileName)
        sprintf (processString, "./idm.pl --ifiles %s/%s_a.hdM --ifiles %s --ofile %s", pCalfaArgs->iPathName, pCalfaArgs->hFileName, pCalfaArgs->yFileName, pCalfaArgs->zFileName);
    else
        sprintf (processString, "./idm.pl --ifiles %s --ofile %s", pCalfaArgs->yFileName, pCalfaArgs->zFileName);
    retVal = pasystem (processString);
    if (retVal) {
        printf ("Error %d executing idm\n", retVal);
        return retVal;
    }
    sprintf (processString, "cat %s", pCalfaArgs->zFileName);
    retVal = pasystem (processString);
    if (retVal) {
        printf ("Error %d executing cat\n", retVal);
        return retVal;
    }

    return retVal;
} //postProcess

// -----------------------------------------------------------------------------

static int cleanup (CALFA_Args *pCalfaArgs)
{
    if (pCalfaArgs->retain == false) {
        remove (pCalfaArgs->cFileName);
        remove (pCalfaArgs->iFileName);
        remove (pCalfaArgs->sFileName);
        remove (pCalfaArgs->xFileName);
        remove (pCalfaArgs->yFileName);
        remove (pCalfaArgs->zFileName);
    }
    free (pCalfaArgs->hFileName);
    free (pCalfaArgs->iPathName);

    return 0;
} //cleanup

// -----------------------------------------------------------------------------

static void printHelp (void)
{
    fprintf (stdout, "%s\n", version);
    fprintf (stdout, "%s\n", copyright);
    fprintf (stdout, "\n");
    printUsage ();
    fprintf (stdout, "\n");
    fprintf (stdout, "%s\n", help);

} //printHelp

// -----------------------------------------------------------------------------

static void printUsage (void)
{
    fprintf (stdout, "%s\n", usage);

} //printUsage

// -----------------------------------------------------------------------------
