
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// I2C transport implementation for calfa2
//
//
//

#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "calfa2.h"
#include "calfa2_i2c.h"

#define CHUNK_SIZE 128
static unsigned char inpData[CHUNK_SIZE];


// -----------------------------------------------------------------------------

int transferI2C (CALFA_Args *pCalfaArgs)
{
    int dev, retVal;
    int inpSize, numBytesRem;
    FILE *pInpFile;
    FILE *pOutFile;


    if ((dev = open ("/dev/i2c/0", O_RDWR)) < 0) {
        printf ("Unable to open I2c-0 device\n");
        goto commonExit;
    }

    if (ioctl (dev, I2C_SLAVE, pCalfaArgs->addr) < 0) {
        printf ("Unable to set I2c slave address\n");
        goto commonExit;
    }

    pInpFile = fopen (pCalfaArgs->xFileName, "rb");
    if (pInpFile) {
        fseek (pInpFile, 0, SEEK_END);
        inpSize = ftell (pInpFile);
        fseek (pInpFile, 0, SEEK_SET);
        numBytesRem = inpSize;

        while (numBytesRem) {
            unsigned short readSize = min (numBytesRem, CHUNK_SIZE);
            retVal = fread (inpData, 1, readSize, pInpFile);
            if (retVal != readSize) {
                printf ("Error reading from input file\n");
                break;
            }
            numBytesRem -= readSize;
            readSize += 1;
            retVal = write (dev, &readSize, 2);
            if (retVal != 2) {
                printf ("I2C write (of header) failed %d\n", retVal);
                break;
            }
            readSize -= 1;
            retVal = write (dev, inpData, readSize);
            if (retVal != readSize) {
                printf ("I2C write (of payload) failed %d\n", retVal);
                break;
            }
        }
        fclose (pInpFile);

        retVal = read (dev, &numBytesRem, 1);
        if (retVal != 1) {
            printf ("I2C read (of count) failed %d\n", retVal);
            goto commonExit;
        }

        pOutFile = fopen (pCalfaArgs->sFileName, "wb");
        retVal = read (dev, &inpData, numBytesRem);
        if (retVal != numBytesRem) {
            printf ("I2C read (of payload) failed %d\n", retVal);
            goto commonExit;
        }
        fwrite (inpData, 1, numBytesRem, pOutFile);
        fclose (pOutFile);
        retVal = 0;
    } // if (pInpFile)

commonExit:
    if (dev > 0)
        close (dev);

    return retVal;
} //transferI2C

// -----------------------------------------------------------------------------

