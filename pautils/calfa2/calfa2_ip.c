
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// IP transport implementation for calfa2
//
//



#include <string.h>
#include <unistd.h>

#include "inetwork.h"

#include "calfa2.h"
#include "calfa2_link.h"


#ifndef  WIN32
	#include <unistd.h>//POSIX declarations and macros
	#include <sys/stat.h> //File attribute declartions and macros
	#include <sys/types.h>//System typedefs

#endif

#define  TEST_LOOPBACK  0

#if TEST_LOOPBACK == 0

	#include <gpptypes.h>
	#include <dsplink.h>
	#include <errbase.h>
	#include <loaderdefs.h>
	#include <msgq.h>
	#include <pool.h>
	#include <proc.h>
	
	#include "../link/palink.h"
	#include "../link/pamlink.h"
	#include "../pam/pamapp.h"
	
	#include "../calfa2/calfa2.h"
	#include "../calfa2/calfa2_link.h"

	static char    dataBuf[PAM_LINK_MAX_CODE_LEN];
	static char dataOutBuf[PAM_LINK_MAX_CODE_LEN];
	// -----------------------------------------------------------------------------
	// This function processes a file (in one message) via LINK transport.
	// Consequently the contents of the file must be a complete alpha code packet
	// and must fit inside the message payload limit of PAM_LINK_MAX_CODE_LEN.



#else

	#define PAM_LINK_MAX_CODE_LEN (1024 )
	static char    dataBuf[PAM_LINK_MAX_CODE_LEN];
	static char dataOutBuf[PAM_LINK_MAX_CODE_LEN];	


#endif //TEST_LOOPBACK!=0

#define CHUNK_SIZE 128





int hstSocket;

#define send_number(x) {send (hstSocket, (void *) &x, sizeof(x), 0);}
#define send_binary(x, y) send (hstSocket, (void *) x, y, 0)
#define recv_number(x) (recv_all(hstSocket, (void *) &x, sizeof(x), 0) == sizeof(x))
#define recv_binary(x, y) (recv_all(hstSocket, (void *) x, y, 0) == (int)(y))
#define abort_connection(x) {socket_close(hstSocket); continue;}


#define d1printf if (debugLevel >= 1) printf
#define d2printf if (debugLevel >= 2) printf
#define d3printf if (debugLevel >= 3) printf



enum IO_MODES{BIN_PURE,NUM_ACSI,SYM_ACSI};
//mode 0:binary[x], 1:numeric_ascii[i],2:symbolic_ascii[c] 

enum _boolean{_true,_false};





int transferIP (CALFA_Args *pCalfaArgs)
{
	int sockFd;

	struct timeval timeptr;
	int starttime;
	int stoptime;
	struct sockaddr_in cliAddr, servAddr;

	int debugLevel = 3;
	int flgPaLoad = 0;
	int retVal = 0;

#if 0
	argv++;
	argc--;
	while (argc) {
		if (!strcmp (*argv, "paload"))
			flgPaLoad = 1;
		if (!strcmp (*argv, "d1"))
			debugLevel = 1;
		if (!strcmp (*argv, "d2"))
			debugLevel = 2;
		if (!strcmp (*argv, "d3"))
			debugLevel = 3;
		argv++;
		argc--;
	}
#endif

#if defined(BCC_WIN32) || defined(WIN32)
	WSADATA wsaData;

	if (WSAStartup (0x2, &wsaData) != 0)
	{
		printf ("calfa2: Socket initialization failed.\n");
		return;
	}
#endif

#if defined(BCC_WIN32) || defined(WIN32)
	if ((sockFd = socket (AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		printf ("calfa2: Could not open socket.\n");
		return;
	}
#else
	sockFd = socket (AF_INET, SOCK_STREAM, 0);
#endif

	bzero ((char *) &servAddr, sizeof (servAddr));
	servAddr.sin_family = AF_INET;
#if defined(BCC_WIN32) || defined(WIN32)
	servAddr.sin_len = sizeof (servAddr);
#endif
	servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
	servAddr.sin_port = htons (2891);

	if (bind (sockFd, (struct sockaddr *) &servAddr, sizeof (servAddr)) < 0)
	{
		printf ("calfa2: Could not bind to local address.\n");
		return;
	}

	if (listen (sockFd, 1) < 0)
	{
		printf ("calfa2: Could not listen to specified port.\n");
		return;
	}

	printf ("calfa2: initialized\n");

#if 1
	    retVal = PA_LINK_open ();
	    if (retVal) {
	        fprintf (stderr, "PA_LINK_open failed %d\n", retVal);
	        retVal = 1;
	    }
#endif




	for (;;)
	{
		int i, cliLen, release, retValue;
		long int size;
		void *tx, *rx;
		char txFile[64], rxFile[64], cmd[256];
		FILE *fp;

		cliLen = sizeof (cliAddr);
		hstSocket = accept (sockFd, (struct sockaddr *) &cliAddr, &cliLen);
		sprintf (rxFile, "/tmp/calfa2s.%s.cmd.bin", inet_ntoa (cliAddr.sin_addr));
		sprintf (txFile, "/tmp/calfa2s.%s.resp.bin", inet_ntoa (cliAddr.sin_addr));
		//sprintf (cmd, "./bin_pam_link -v -s%s -r%s", rxFile, txFile);

		printf ("Request from %s\n", inet_ntoa (cliAddr.sin_addr));

		// Run paload if required
		if (flgPaLoad)
		{
			d2printf ("calfa2: Starting paload\n");
			if (fork () == 0)
				execl ("./paload", "./paload");
			sleep (2);
			flgPaLoad = 0;
		}

		// Receive
		if (!recv_number (size))
			abort_connection (0);
		d2printf ("calfa2: Receiving %d bytes\n", size);
		if ((rx = (void *) malloc (size)) == NULL)
		{
			fprintf (stderr, "calfa2: error: Cannot allocate RX memory.\n");
			abort_connection (0);
		}
		recv_binary (rx, size);
		d2printf ("calfa2: Received\n");
		if (debugLevel >= 1) {
			printf (">> ");
			for (i = 0; i < size / 2; i++)
				printf ("%04x ", ((unsigned short *) rx)[i]);
			printf ("\n");
		}

		// Save to temporary file
		if ((fp = fopen (rxFile, "wb")) == NULL)
		{
			fprintf (stderr, "calfa2: error: Cannot open RX file.\n");
			abort_connection (0);
		}
		if (fwrite (rx, size, 1, fp) != 1)
		{
			fprintf (stderr, "calfa: error: Error writing to RX file.\n");
			abort_connection (0);
		}
		d3printf ("calfa: RX file size: %d\n", ftell (fp));
		fclose (fp);

		// Execute
		d3printf ("calfa2s: %s\n", rxFile);
		d3printf ("calfa2s: %s\n", txFile);
		d3printf ("calfa2s: %s\n", cmd);
		//system (cmd);

		gettimeofday(timeptr);
		d3printf ("Start time: %d\n", timeptr.tv_sec);

		retValue = pam_link(rxFile, txFile);
		gettimeofday(timeptr);
		d3printf ("Stop time: %d\n", timeptr.tv_sec);


		// Read from temporary file
		if ((fp = fopen (txFile, "rb")) == NULL)
		{
			fprintf (stderr, "calfa2s: error: Cannot open TX file.\n");
			abort_connection (0);
		}
		fseek (fp, 0, SEEK_END);
		size = ftell (fp);
		rewind (fp);
		d2printf ("calfa2s: Sending %d bytes\n", size);
		if ((tx = (void *) malloc (size)) == NULL)
		{
			fprintf (stderr, "calfa2s: error: Cannot allocate TX memory.\n");
			abort_connection (0);
		}
		if (fread (tx, size, 1, fp) != 1)
		{
			fprintf (stderr, "calfa2s: error: Error reading from TX file.\n");
			abort_connection (0);
		}
		fclose (fp);
		if (debugLevel >= 1) {
			printf ("<< ");
			for (i = 0; i < size / 2; i++)
				printf ("%04x ", ((unsigned short *) tx)[i]);
			printf ("\n");
		}

		// Send
		send_number (size);
		send_binary (tx, size);
		d2printf ("calfa2s: Sent\n");

		// Close connection
		free (rx);
		abort_connection (0);
	}
}






int pam_link(char rxFile[64] , char txFile[64])

{
	int c,i,err,verbose;
	FILE *fpTxFile,*fpRxFile;
	#define  SIZE_STD_FNAME 1024 //User may override
	char *txFname,*rxFname;

	#define  SIZE_BUF 1024 //User may override
	unsigned long  sizeTxBuf;
	unsigned char  *pTxBuf,*pRxBuf;
	unsigned char MARKER_STDIO='0';

	enum IO_MODES  io_mode; 
	enum _boolean bUseStdIO;

#if 0	
	DSP_STATUS status  = DSP_SOK;
	MSGQ_LocateAttrs syncLocateAttrs;
	PA_LINK_Msg *pMsg;
	NOLOADER_ImageInfo  imageInfo; 
#endif//TEST_LOOPBACK!=0	
   
	PAM_APP_Handle	handle;
	int retVal = 0;
	int retBytes;	
	
	//
	//---
	// Parameter Defaults
	 err = 0;
	verbose =1;
	 io_mode=BIN_PURE;
	 bUseStdIO = _false;
	 fpTxFile=NULL;
	 fpRxFile=NULL;
	//---
	//


	txFname=calloc(SIZE_STD_FNAME ,sizeof(unsigned char));
	rxFname=calloc(SIZE_STD_FNAME ,sizeof(unsigned char));
	if((NULL==txFname)|| (NULL==rxFname))
	{
	 printf("Fname alloc failed\n");
	}	

	//User may override
	pTxBuf=calloc(SIZE_BUF ,sizeof(unsigned char));
	pRxBuf=calloc(SIZE_BUF ,sizeof(unsigned char));
	if((NULL==pTxBuf)|| (NULL==pRxBuf) )
	{
	 printf("Buf alloc failed\n");
	}



/////////////////////////////////////


			//case 's':
				// sscanf(argv[++i],"%lx",&reloc)
				//printf("File to send - txFile : %s\n", &argv[i][2]); //optarg ==argv[optind]??
				//sprintf(txFname,&argv[i][2]);
				//txFname = rxFile;
				if (verbose){ printf("File to send - txFile : %s\n", rxFile);} //optarg ==argv[optind]?? 
				fpTxFile=fopen(rxFile,"rb");


			//case 'r':
				//printf("File  received - RxFile : %s\n",&argv[i][2]);
				//sprintf(rxFname,&argv[i][2]);
				//rxFname = txFile;
				if (verbose){printf("File  received - RxFile : %s\n",txFile);}
				fpRxFile=fopen(txFile,"wb");

/////////////////////////////////////



	if(err){
		//fprintf(stderr, " %s - version: %s \n" ,argv[0] ,VER);
				
  		//fprintf(stderr, "usage : %s  [-v]  -s_txFile/file2Send  -r_rxFile \n", argv[0]  );
		fprintf(stderr, "NOTE:\n");	  	
		fprintf(stderr, " 1 File names need to be less than %d\n",SIZE_STD_FNAME);
		fprintf(stderr, " 2 Input file size need to be less than Max buf-size=%d [or  Max chunksize=%d ??]\n ",SIZE_BUF,CHUNK_SIZE); 
		fprintf(stderr, "\n");
		fprintf(stderr, "To Do\n");
		fprintf(stderr, "[-- which will do <(STDIN_BIN) >(STDOUT_BIN)]  ; but  > & < is interpreted by shell for redirection\n");		
		fprintf(stderr, "\n");
		fprintf(stderr, "ERROR : Check the options !!\n"); 		
  		exit (-1);
  		
	}//if(err)
	//---
	//

	 if ('-'==MARKER_STDIO){

	    if((fpTxFile==NULL) ) 
	    { 
			fprintf(stderr, "ERROR: 'Send file' open failed!!\n");
			fpTxFile=stdin;
	    }
	    if((fpRxFile==NULL) ) 
	    { 		
		sprintf(rxFname,"stdout");
	        fpRxFile=stdout;
	    }
	 }
	 
	 
    //-- Setup:end   
    //

    // 
    //--Processing:start
    

    {
        FILE *pInpFile;
        FILE *pOutFile;
        int inpSize, numBytesRem;
        int readElems;


			////pInpFile = fopen (pCalfaArgs->xFileName, "rb");//To DSP
			pInpFile=fpTxFile;
			////pOutFile= fopen (pCalfaArgs->sFileName, "rb");//From DSP
			pOutFile=fpRxFile;
#if 1
	if (pInpFile) {

           if ((fpTxFile!=stdin)|| (fpTxFile!=NULL) ){          	

            fseek (pInpFile, 0, SEEK_END);
            inpSize = ftell (pInpFile);
            fseek (pInpFile, 0, SEEK_SET);
            numBytesRem = inpSize;
			   if (inpSize > PAM_LINK_MAX_CODE_LEN) {
			    fprintf (stderr, "Error: File size %d is greater than limit %d", inpSize, PAM_LINK_MAX_CODE_LEN);
			    retVal = 1;
			    goto commonExit;
			   } 
	    
	     }else{//if ((fpTxFile!=stdin)|| (fpTxFile!=NULL) )           
	         // NOT working - stdin mode !!!!! 
            fseek (pInpFile, 0, SEEK_END);
            inpSize = ftell (pInpFile);
            fseek (pInpFile, 0, SEEK_SET);
            numBytesRem = inpSize;
            //printf(" Exiting ..\n");
            //exit(-1);	    
	    
		}
        }


#endif


#if 0
           if ((fpTxFile!=stdin)|| (fpTxFile!=NULL) ){          	

            fseek (fpTxFile, 0, SEEK_END);
            inpSize = ftell (fpTxFile);
            fseek (fpTxFile, 0, SEEK_SET);
            numBytesRem = inpSize;
			   if (inpSize > PAM_LINK_MAX_CODE_LEN) {
			    fprintf (stderr, "Error: File size %d is greater than limit %d", inpSize, PAM_LINK_MAX_CODE_LEN);
			    retVal = 1;
			    goto commonExit;
			   } 
	    
	     }else{//if ((fpTxFile!=stdin)|| (fpTxFile!=NULL) )           
	         // NOT working - stdin mode !!!!! 
            fseek (fpTxFile, 0, SEEK_END);
            inpSize = ftell (fpTxFile);
            fseek (fpTxFile, 0, SEEK_SET);
            numBytesRem = inpSize;
            //printf(" Exiting ..\n");
            //exit(-1);	    
	    
		}
#endif
	     	

	     	

#if TEST_LOOPBACK==0

#if 0
	    retVal = PA_LINK_open ();
	    if (retVal) {
	        fprintf (stderr, "PA_LINK_open failed %d\n", retVal);
	        retVal = 1;
	        goto commonExit;
	    }
#endif
	
	    retVal = PAM_APP_open ("pamlink", NULL, &handle);
	    if (retVal) {
	        fprintf (stderr, "PAM_APP_open failed %d\n", retVal);
	        return 1;
	    }


#endif//TEST_LOOPBACK!=0	

	    retVal = fread (dataBuf, 1, inpSize, fpTxFile);
	    if (retVal != inpSize) {
	        fprintf (stderr, "Error reading from input file\n");
	        retVal = 1;
	        goto commonExit;
	    }
         
#if TEST_LOOPBACK==0            
	    // write and read alpha code packet to/from DSP
	    //int PAM_APP_message  (char *inpBuf, char *outBuf, int inSize, int *outSize, PAM_APP_Handle handle)
	    //retVal = PAM_APP_message  (dataBuf, dataBuf, inpSize, &retBytes, handle);
	    retVal = PAM_APP_message  (dataBuf, dataOutBuf, inpSize, &retBytes, handle);
	    if (retVal) {
	        fprintf (stderr, "PAM_APP_message error %d\n", retVal);
	        retVal = 1;
	        goto commonExit;
	    }

	    // write result to output file
	    //fwrite (dataBuf, 1, retBytes, pOutFile);
	    fwrite (dataOutBuf, 1, retBytes, pOutFile);
	    //fwrite (&pMsg->alfaCode[2], 1, pMsg->alfaCode[0], pOutFile);	    	    
#else

	    // write result to output file
	    retBytes=inpSize;
	    //fwrite (&pMsg->alfaCode[2], 1, pMsg->alfaCode[0], pOutFile);
	    //fwrite (dataBuf, 1, retBytes, pOutFile);
	    fwrite (dataBuf, 1, retBytes, pOutFile);	   
	    
#endif//TEST_LOOPBACK!=0
	    

            

        } // if (pInpFile)
    
     //proc block
   // 
   //--Processing:end







    
goto   commonExit;
//Don't get to  errorExit in normal execution. 

LBL_ERROR:
errorExit:
err++;






commonExit:

#if TEST_LOOPBACK==0                
       if (handle!=NULL){
		// free resources associated with PAM LINK transport
		retVal = PAM_APP_close (handle);
		if (retVal) {
		    fprintf (stderr, "PAM_APP_close failed %d\n", retVal);
		    retVal = 1;
		    //goto commonExit;
		}

		retVal = PA_LINK_close ();
		if (retVal) {
			fprintf (stderr, "PA_LINK_close failed %d\n", retVal);
			retVal = 1;
			//goto commonExit;
		}

	}    
#endif //TEST_LOOPBACK!=0                

   	//
	//--Cleanup


	if(pTxBuf     !=NULL){
		free(pTxBuf);
		}

	if(pRxBuf    !=NULL){
		free(pRxBuf);
		}

	
	
	if(txFname  !=NULL) {
		free(txFname);
		}
	
	if(rxFname  !=NULL) {
		free(rxFname);
		}
	
	//
	// Don't close stdio-s , we may need it later.
	if((fpTxFile    !=stdin) ||  (fpTxFile!=NULL)){
		 fflush(fpTxFile); // For console scripting
		fclose(fpTxFile);
		printf("Closed fpTxFile\n");
	}
	if ((fpRxFile!=stdout) || (fpRxFile!=NULL)){
		fflush(fpRxFile);// For console scripting
		fclose(fpRxFile);
		printf("Closed fpRxFile\n");
	}
	//--
	//

	printf("After Cleanup with err = %d\n", err);

    
	if(err==0){
	 retVal = 0;
	}else{
	  #ifdef DEBUG
	     fprintf (stderr, "app exited abnormally \n");
	  #endif
	 retVal = -1;
	}
   
   
    	return retVal;
  
}
