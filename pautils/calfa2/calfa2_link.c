
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DSPLINK transport implementation for calfa
//
//
//

#include <stdio.h>
#include <stdlib.h>

#include <gpptypes.h>
#include <dsplink.h>
#include <errbase.h>
#include <loaderdefs.h>
#include <msgq.h>
#include <pool.h>
#include <proc.h>

#include <palink.h>
#include <pamlink.h>
#include <pamapp.h>

#include "calfa2.h"
#include "calfa2_link.h"

static char dataBuf[PAM_LINK_MAX_CODE_LEN];

// -----------------------------------------------------------------------------
// This function processes a file (in one message) via LINK transport.
// Consequently the contents of the file must be a complete alpha code packet
// and must fit inside the message payload limit of PAM_LINK_MAX_CODE_LEN.

int transferLink (CALFA_Args *pCalfaArgs)
{
    PAM_APP_Handle            handle;
    FILE                   *pInpFile;
    FILE                   *pOutFile;
    int                      inpSize;
    int                        retVal   = 0;
    int                      retBytes;


    pInpFile = fopen (pCalfaArgs->xFileName, "rb");
    if (!pInpFile) {
        fprintf (stderr, "Unable to open %s for read\n", pCalfaArgs->xFileName);
        retVal = 1;
        goto commonExit;
    }

    pOutFile = fopen (pCalfaArgs->sFileName, "wb");
    if (!pOutFile) {
        fprintf (stderr, "Unable to open %s for write\n", pCalfaArgs->sFileName);
        retVal = 1;
        goto commonExit;
    }

    fseek (pInpFile, 0, SEEK_END);
    inpSize = ftell (pInpFile);
    fseek (pInpFile, 0, SEEK_SET);
    if (inpSize > PAM_LINK_MAX_CODE_LEN) {
        fprintf (stderr, "Error: File size %d is greater than limit %d", inpSize, PAM_LINK_MAX_CODE_LEN);
        retVal = 1;
        goto commonExit;
    }

    retVal = PA_LINK_open ();
    if (retVal) {
        fprintf (stderr, "PA_LINK_open failed %d\n", retVal);
        retVal = 1;
        goto commonExit;
    }

    retVal = PAM_APP_open ("pamlink", NULL, &handle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_open failed %d\n", retVal);
        return 1;
    }

    retVal = fread (dataBuf, 1, inpSize, pInpFile);
    if (retVal != inpSize) {
        fprintf (stderr, "Error reading from input file\n");
        retVal = 1;
        goto commonExit;
    }

    // write and read alpha code packet to/from DSP
    retVal = PAM_APP_message  (dataBuf, dataBuf, inpSize, &retBytes, handle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_message error %d\n", retVal);
        retVal = 1;
        goto commonExit;
    }

    // write result to output file
    fwrite (dataBuf, 1, retBytes, pOutFile);

    // free resources associated with PAM LINK transport
    retVal = PAM_APP_close (handle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_close failed %d\n", retVal);
        retVal = 1;
        goto commonExit;
    }

commonExit:
    if (pInpFile)
        fclose (pInpFile);
    if (pOutFile)
        fclose (pOutFile);

    return retVal;
} //transferLink

// -----------------------------------------------------------------------------
