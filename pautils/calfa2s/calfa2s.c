
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// NDK Test Setup source code
//
//
//
//

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "inetwork.h"

#if defined(ECHO)
#define PAM_LINK_MAX_CODE_LEN 1024
#else
#include <palink.h>
#include <pamlink.h>
#include <pamapp.h>
#endif /* ECHO */

#include <calfa2s.h>

int hstSocket;

void daemonize (void)
{
	pid_t pid, sid;

	if (getppid () == 1)
		return;

	pid = fork ();
	if (pid < 0)
		exit (1);
	if (pid > 0)
		exit (0);

	umask (0);
	sid = setsid ();
	if (sid < 0)
		exit (1);

//  if ((chdir ("/")) < 0)
//      exit (1);

	freopen ("/dev/null", "r", stdin);
	freopen ("/tmp/calfa2s.stdout", "w", stdout);
	freopen ("/tmp/calfa2s.stderr", "w", stderr);
}

void ctrlc (int sig)
{
	printf ("\n");
	printf ("Closing clafa2s may permanently terminate LINK communication with DSP.\n");
	printf ("Get another terminal (using ssh) to get access to shell.\n");
	printf ("\n");
	printf ("To avoid this in future, start calfa2s in daemon mode.\n");
	printf ("\n");
	signal (SIGINT, ctrlc);
}

char rx[PAM_LINK_MAX_CODE_LEN];
char tx[PAM_LINK_MAX_CODE_LEN];

int process_alpha_commands (int debugLevel, int size);
int process_radio_commands (int debugLevel, int size);

static int gLoaded = 0;

int main (int argc, char *argv[])
{
	int sockFd;
	struct sockaddr_in cliAddr, servAddr;

	char proc_load_fname[MAX_FNAME];

	int debugLevel = 1;
	int flgDaemon = 0;
	int firstTime = 1;

	argv++;
	argc--;
	while (argc)
	{
		if (!strcmp (*argv, "d1"))
			debugLevel = 1;
		if (!strcmp (*argv, "d2"))
			debugLevel = 2;
		if (!strcmp (*argv, "d3"))
			debugLevel = 3;
		if (!strcmp (*argv, "daemon"))
			flgDaemon = 1;
		argv++;
		argc--;
	}

	if (flgDaemon)
	{
		printf ("calfa2s: running as daemon.\n");
		daemonize ();
	}

#if defined(BCC_WIN32) || defined(WIN32)
	WSADATA wsaData;

	if (WSAStartup (0x2, &wsaData) != 0)
	{
		fprintf (stderr, "calfa2s: Socket initialization failed.\n");
		return 1;
	}
#endif

#if defined(BCC_WIN32) || defined(WIN32)
	if ((sockFd = socket (AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		fprintf (stderr, "calfa2s: Could not open socket.\n");
		return 1;
	}
#else
	sockFd = socket (AF_INET, SOCK_STREAM, 0);
#endif

	bzero ((char *) &servAddr, sizeof (servAddr));
	servAddr.sin_family = AF_INET;
#if defined(BCC_WIN32) || defined(WIN32)
	servAddr.sin_len = sizeof (servAddr);
#endif
	servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
	servAddr.sin_port = htons (2891);

	if (bind (sockFd, (struct sockaddr *) &servAddr, sizeof (servAddr)) < 0)
	{
		fprintf (stderr, "calfa2s: Could not bind to local address.\n");
		return 1;
	}

	if (listen (sockFd, 1) < 0)
	{
		fprintf (stderr, "calfa2s: Could not listen to specified port.\n");
		return 1;
	}

	printf ("calfa2s: initialized.\n");

	for (;;)
	{
		int error;
		unsigned int cliLen, opcode, zone_id;
		int size;

		char *target_path = NULL;

		cliLen = sizeof (cliAddr);
		hstSocket = accept (sockFd, (struct sockaddr *) &cliAddr, &cliLen);
		d1printf ("In Main(): Request from %s\n", inet_ntoa (cliAddr.sin_addr));
		fflush (stdout);

		for (;;)
		{
			// Receive
			// At this point we are going to receive an opcode
			// We maintain compatibility with the old way of sending alpha commands
			if (!recv_number (opcode))
			{
				break;
			}

			d1printf ("In main(): opcode = %d\n", opcode);
			fflush (stdout);
			// check the opcode. it can be just size of alpha code
			switch ((opcode & 0xFF000000) >> 24)
			{
				case CALFA2C_COMMAND_ALPHA:
					// Open LINK for the first time
					if (firstTime)
					{
#if !defined (ECHO)
						if (!flgDaemon)
							signal (SIGINT, ctrlc);
#endif
						if (!gLoaded)
						{
							// default file name for PROC_load
							strncpy (proc_load_fname, "/home/root/pa.out", MAX_FNAME);
							error = calfa2_load (debugLevel, proc_load_fname);
							if (error)
							{
								// this is a fatal error
								d1printf ("In main(): Fatal error in calfa2_load()\n");
								exit (1);
							}
						}
						firstTime = 0;
					}
					// we received size of alpha commands
					size = opcode;
					error = process_alpha_commands (debugLevel, size);
					if (error)
					{
						// do housekeeping?
						// TODO
						d1printf ("In main(): error from process_alpha_commands()\n");
					}
					break;
#if !defined (ECHO)
				case CALFA2C_COMMAND_RELOAD:
					error = calfa2_load (debugLevel, proc_load_fname);
					if (error)
					{
						// this is a fatal error
						d1printf ("In main(): Fatal error in calfa2_load()\n");
						exit (1);
					}
					break;
				case CALFA2C_COMMAND_XFER:
				case CALFA2C_COMMAND_LOCAL_LOAD:
				case CALFA2C_COMMAND_REMOTE_LOAD:
					// receive target_path
					recv_number (size);
					d1printf ("size of target path %d\n", size);
					fflush (stdout);
					target_path = malloc (size);
					recv_binary (target_path, size);
					d1printf ("target path = %s\n", target_path);
					fflush (stdout);

					// opcode specific code starts here
					if ((((opcode & 0xFF000000) >> 24) == CALFA2C_COMMAND_REMOTE_LOAD) ||
							(((opcode & 0xFF000000) >> 24) == CALFA2C_COMMAND_XFER))

					{
						xfer_file (debugLevel, target_path);
					}
					if ((((opcode & 0xFF000000) >> 24) == CALFA2C_COMMAND_REMOTE_LOAD) ||
							(((opcode & 0xFF000000) >> 24) == CALFA2C_COMMAND_LOCAL_LOAD))
					{
						strncpy (proc_load_fname, target_path, MAX_FNAME);
						d2printf ("Going to call calfa2_load()\n");
						fflush (stdout);

						error = calfa2_load (debugLevel, proc_load_fname);
						if (error)
						{
							// this is a fatal error
							d1printf ("In main(): Fatal error in calfa2_load()\n");
							exit (1);
						}
					}
					free (target_path);
					break;
				case CALFA2C_COMMAND_RADIO:
					if (!gLoaded)
					{
						// default file name for PROC_load
						strncpy (proc_load_fname, "/home/root/pa.out", MAX_FNAME);
						error = calfa2_load (debugLevel, proc_load_fname);
						if (error)
						{
							// this is a fatal error
							d1printf ("In main(): Fatal error in calfa2_load()\n");
							exit (1);
						}
					}
					if (!recv_number (zone_id))
					{
						break;
					}
					d1printf ("In main(): zone_id = %d\n", zone_id);
					fflush (stdout);
					// we received size of radio commands
					size = opcode & 0x00FFFFFF;
					// subtract length of zone_id
					size -= 4;
					error = process_radio_commands (debugLevel, size);
					break;
#endif
				default:
					// let's igonre and continue
					continue;
					break;									// not needed
			}												// switch
		}													// for (command)
		socket_close (hstSocket);
	}														// for (connection)
}

int xfer_file (int debugLevel, char *fname)
{
	int size, xsize = 0;
	char *rxBuf = NULL;
	FILE *fp = NULL;
	int i = 0;

	// receive file length
	recv_number (size);
	d2printf ("size of file %d\n", size);
	fflush (stdout);
	// Open RX file
	if ((fp = fopen (fname, "wb")) == NULL)
	{
		d1printf ("In xfer_file(): Fatal error opening RX file\n");
		exit (1);
	}

	rxBuf = malloc (MAX_XFER_SIZE);
	i = 0;
	while (size >= 0)
	{
		xsize = (size > MAX_XFER_SIZE ? MAX_XFER_SIZE : size);

		recv_binary (rxBuf, xsize);
		if ((fwrite (rxBuf, xsize, 1, fp)) == NULL)
		{
			d1printf ("In xfer_file(): Fatal error writing RX file\n");
			exit (1);
		}
		size -= MAX_XFER_SIZE;
		i++;
		//printf (".", i);
	}
	printf ("\n");
	d2printf ("Written the file\n");
	free (rxBuf);
	d3printf ("Freed rxBuf[]\n");
	fclose (fp);
	d3printf ("Closed the file\n");
	fflush (stdout);
}

long long timediff (struct timeval *difference, struct timeval *end_time, struct timeval *start_time)
{
	struct timeval temp_diff;

	if (difference == NULL)
	{
		difference = &temp_diff;
	}

	difference->tv_sec = end_time->tv_sec - start_time->tv_sec;
	difference->tv_usec = end_time->tv_usec - start_time->tv_usec;

	/* Using while instead of if below makes the code slightly more robust. */

	while (difference->tv_usec < 0)
	{
		difference->tv_usec += 1000000;
		difference->tv_sec -= 1;
	}

	return 1000000LL * difference->tv_sec + difference->tv_usec;

}															/* timediff() */

int calfa2_load (int debugLevel, char *fname)
{
	int error = 0;
	struct timeval tv1, tv2, tv_diff;

#if !defined(ECHO)
	if (gLoaded)
	{
		d2printf ("going to close Link\n");
		fflush (stdout);
		// one image must be loaded
		// Let's close the Link
		gettimeofday (&tv1, NULL);
		error = PA_LINK_close ();
		gettimeofday (&tv2, NULL);

		// get time diff
		timediff (&tv_diff, &tv2, &tv1);
		d2printf ("PA_LINK_close took @sec=%d, @msec=%d\n", tv_diff.tv_sec, tv_diff.tv_usec / 1000);
		fflush (stdout);

		if (error)
		{
			// this is a fatal error
			d1printf ("In calfa2_load(): Fatal error closing Link\n");
			fflush (stdout);
			return (error);
		}
		d3printf ("Link closed\n");
		fflush (stdout);
	}
	d2printf ("Going to call PA_LINK_open()\n");
	fflush (stdout);
	d1printf ("file name = %s\n", fname);
	fflush (stdout);
	//gettimeofday (&tv1, NULL);
	error = PA_LINK_open (fname);
	//gettimeofday (&tv2, NULL);
	// get time diff
	//timediff (&tv_diff, &tv2, &tv1);
	d2printf ("PA_LINK_open took @sec=%d, @msec=%d\n", tv_diff.tv_sec, tv_diff.tv_usec / 1000);
	fflush (stdout);

	if (error)
	{
		fprintf (stderr, "calfa2s: PA_LINK_open failed (error %d)\n", error);
		return (error);
	}
	d1printf ("In calfa2_load(): PA_LINK_open successful.\n");
	fflush (stdout);
	d2printf ("Link opened again\n");
	fflush (stdout);
	gLoaded = 1;
#endif /* ECHO */
	return (error);
}

int process_alpha_commands (int debugLevel, int size)
{
	int i, error = 0;

#if !defined(ECHO)
	PAM_APP_Handle handle;
#endif /* ECHO */
	d2printf ("calfa2s: Receiving %d bytes\n", size);
	fflush (stdout);
#if !defined(ECHO)
	if (size > PAM_LINK_MAX_CODE_LEN)
	{
		fprintf (stderr, "calfa2s: RX size too big (> %d).\n", PAM_LINK_MAX_CODE_LEN);
		fflush (stderr);
		goto exit_on_error;
	}
#endif /* ECHO */
	recv_binary (rx, size);

	// Show what we received
	d2printf ("calfa2s: Received\n");
	fflush (stdout);
	if (debugLevel >= 1)
	{
		printf (">> ");
		for (i = 0; i < size / 2; i++)
			printf ("%04x ", ((unsigned short *) rx)[i]);
		printf ("\n");
	}

#if defined(ECHO)
	{
		int i;

		for (i = 0; i < size; i++)
			tx[i] = rx[i];
	}
#else
	// Open PAM
	d2printf ("calfa2s: PAM_APP_open\n");
	fflush (stdout);
	error = PAM_APP_open ("pamlink", NULL, &handle);
	d3printf ("calfa2s: return value: %d\n", error);
	fflush (stdout);
	if (error)
	{
		fprintf (stderr, "calfa2s: PAM_APP_open failed %d\n", error);
		fflush (stderr);
		goto exit_on_error;
	}

	// Execute
	d2printf ("calfa2s: PAM_APP_message\n");
	fflush (stdout);
	error = PAM_APP_message (rx, tx, size, &size, handle);
	d3printf ("calfa2s: return value: %d\n", error);
	fflush (stdout);
	if (error)
	{
		fprintf (stderr, "cala2s: PAM_APP_message failed %d\n", error);
		fflush (stderr);
		error = PAM_APP_close (handle);
		d3printf ("calfa2s: return value: %d\n", error);
		fflush (stdout);
		if (error)
		{
			fprintf (stderr, "cala2s: PAM_APP_close failed %d\n", error);
			fflush (stderr);
		}
		goto exit_on_error;
	}

	// Close PAM
	d2printf ("calfa2s: PAM_APP_close\n");
	fflush (stdout);
	error = PAM_APP_close (handle);
	d3printf ("calfa2s: return value: %d\n", error);
	fflush (stdout);
	if (error)
	{
		fprintf (stderr, "cala2s: PAM_APP_close failed %d\n", error);
		fflush (stderr);
		goto exit_on_error;
	}
#endif /* ECHO */

	// Show what we are sending
	if (debugLevel >= 1)
	{
		printf ("<< ");
		for (i = 0; i < size / 2; i++)
			printf ("%04x ", ((unsigned short *) tx)[i]);
		printf ("\n");
	}

	// Send
	send_number (size);
	send_binary (tx, size);
	d2printf ("calfa2s: Sent\n");
	fflush (stdout);
	goto exit_normally;

  exit_on_error:
	error = 1;
  exit_normally:
	return error;
}

