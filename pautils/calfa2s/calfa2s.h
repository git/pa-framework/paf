
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// NDK Test Setup source code
//
//
//
//

#define send_number(x) {send (hstSocket, (void *) &x, sizeof(x), 0);}
#define send_binary(x, y) send (hstSocket, (void *) x, y, 0)
#define recv_number(x) (recv_all(hstSocket, (void *) &x, sizeof(x), 0) == sizeof(x))
#define recv_binary(x, y) (recv_all(hstSocket, (void *) x, y, 0) == (int)(y))
#define abort_connection(x) {socket_close(hstSocket); continue;}

#define d1printf if (debugLevel >= 1) printf
#define d2printf if (debugLevel >= 2) printf
#define d3printf if (debugLevel >= 3) printf

int process_alpha_commands(int debugLevel, int size);

#define MAX_XFER_SIZE 1024
#define MAX_FNAME 256

// enumeration for calfa2c commands
typedef enum _Cafa2c_commands {
	CALFA2C_COMMAND_ALPHA = 0,
	CALFA2C_COMMAND_RELOAD,
	CALFA2C_COMMAND_REMOTE_LOAD,
	CALFA2C_COMMAND_LOCAL_LOAD,
	CALFA2C_COMMAND_XFER,
	CALFA2C_COMMAND_RADIO,
	CALFA2C_COMMAND_NUMBER,
} Calfa2c_commands;

extern int hstSocket;
