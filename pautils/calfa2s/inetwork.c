
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// NDK Test Setup source code
//
//
//
//

#include "inetwork.h"

#if defined(BCC_WIN32) || defined(WIN32)
int bzero (char *src, size_t size)
{
	while (size-- > 0)
	{
		*src++ = 0;
	}
	return size;
}
#endif

#if defined(_TMS320C6X)
int recv_all (SOCKET s, char *buf, int len, int flags)
#elif defined(BCC_WIN32)
WINSOCK_API_LINKAGE int WSAAPI recv_all (IN SOCKET s, OUT char FAR * buf, IN int len, IN int flags)
#else
int recv_all (int s, char *buf, int len, int flags)
#endif
{
	int retValue, recvSize;

	recvSize = 0;
	while (recvSize < len)
	{
		retValue = recv (s, (buf + recvSize), (len - recvSize), flags);
		if (retValue <= 0)
			return (recvSize);
		recvSize += retValue;
	}
	return (recvSize);
}

#if defined(_TMS320C6X)
char *inet_ntoa(const struct in_addr in)
{
	static char ip_addr[16];
	
	sprintf(ip_addr, "%u.%u.%u.%u", (in.s_addr & 0xff), ((in.s_addr >> 8) & 0xff), ((in.s_addr >> 16) & 0xff), ((in.s_addr >> 24) & 0xff));
	
	return ip_addr;
}
#endif
