
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// NDK Test Setup source code
//
//
//
//

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "inetwork.h"

#include <palink.h>
#include <pamlink.h>
#include <pamapp.h>

#include <calfa2s.h>

extern char rx[PAM_LINK_MAX_CODE_LEN];
extern char tx[PAM_LINK_MAX_CODE_LEN];

#define OFFSET_OPCODE 		0
#define OFFSET_LENGTH 		1
#define OFFSET_DATA   		5
#define PACKET_FIXED_SIZE   8

int checkDAQinpReady(PAM_APP_Handle handle, int debugLevel)
{
	int t2r_size = 4;
	int error = 0;
	unsigned short read_daq_inp_ready [2] = {0xc26f,0x0500};

	error = PAM_APP_message ((char*)read_daq_inp_ready, tx, t2r_size, &t2r_size, handle);

	//d2printf ("DAQinpReady\n");
	//fflush (stdout);
	// check if input is ready!
	return(((unsigned short *) tx)[1] & 0x00ff);
}

void writeDAQinpSize(PAM_APP_Handle handle, unsigned int len, int debugLevel)
{
	int t4w_size = 8;
	int error = 0;
	unsigned short write_daq_inp_size [4] = {0xcc6f,0x000c};

	d2printf ("writeDAQinpSize => %d\n", len);

	write_daq_inp_size[2] = (len & 0x0000ffff);
	write_daq_inp_size[3] = (len & 0xffff0000) >> 16;
	d2printf ("LSW => %d\n", write_daq_inp_size[2]);
	d2printf ("MSW => %d\n", write_daq_inp_size[3]);
	fflush (stdout);

	error = PAM_APP_message ((char*)write_daq_inp_size, tx, t4w_size, &t4w_size, handle);
}

void writeDAQinpData(PAM_APP_Handle handle, unsigned int len, int debugLevel)
{
	int t5w_size;
	int t5w_header_size = 8;
	int error = 0;
	char* t5w;

	unsigned short write_daq_inp_data_header [4] = {0xcd06,0x006f,0x0014,0x0000};
	
	t5w = malloc(len+t5w_header_size);
	// correct the len field of CPM
	t5w_size = t5w_header_size+len;
	// correct the len field of t5w
	write_daq_inp_data_header[3] = len;

	memcpy(t5w, write_daq_inp_data_header, t5w_header_size);
	memcpy((t5w+t5w_header_size), rx, len);

	error = PAM_APP_message (t5w, tx, t5w_size, &t5w_size, handle);

	d2printf ("writeDAQinpData\n");
	fflush (stdout);
}

int writeDAQinpReady(PAM_APP_Handle handle, int debugLevel)
{
	int t2w_size = 4;
	int error = 0;
	unsigned short write_daq_inp_ready [2] = {0xca6f,0x0501};

	error = PAM_APP_message ((char*)write_daq_inp_ready, tx, t2w_size, &t2w_size, handle);

	d2printf ("writeDAQinpReady\n");
	fflush (stdout);
}

int checkDAQoutReady(PAM_APP_Handle handle, int debugLevel)
{
	int t2r_size = 4;
	int error = 0;
	unsigned short read_daq_out_ready [2] = {0xc26f,0x0700};

	error = PAM_APP_message ((char*)read_daq_out_ready, tx, t2r_size, &t2r_size, handle);
	
	//d2printf ("checkDAQoutReady\n");
	//fflush (stdout);

	return(((unsigned short *) tx)[1] & 0x00ff);
}

void readDAQoutSize(PAM_APP_Handle handle, unsigned int *len, int debugLevel)
{
	int t4r_size = 4;
	int i, error = 0;
	unsigned short read_daq_out_size [2] = {0xc46f,0x0010};

	error = PAM_APP_message ((char*)read_daq_out_size, tx, t4r_size, &t4r_size, handle);

	*len = (((unsigned short *) tx)[3] << 16) | (((unsigned short *) tx)[2]);
	
#if 0	
	if (debugLevel >= 1)
	{
		printf ("<< ");
		for (i = 0; i < t4r_size / 2; i++)
			printf ("%04x ", ((unsigned short *) tx)[i]);
		printf ("\n");
	}
	fflush (stdout);
#endif

	d2printf ("readDAQoutSize = %d\n", *len);
	fflush (stdout);
}

void* readDAQoutData(PAM_APP_Handle handle, unsigned int len, int debugLevel)
{
	int t5r_size;
	int t5r_header_size = 8;
	int i, error = 0;
	char* t5r;
	unsigned short read_daq_out_data_header [4] = {0xc506,0x006f,0x0214,0x0000};

	// correct the len field of CPM
	t5r_size = t5r_header_size;
	// correct the len field of t5r
	read_daq_out_data_header[3] = len;

#if 0	
	if (debugLevel >= 1)
	{
		printf (">> ");
		for (i = 0; i < t5r_size / 2; i++)
			printf ("%04x ", ((unsigned short *) read_daq_out_data_header)[i]);
		printf ("\n");
	}
	fflush (stdout);
#endif

	error = PAM_APP_message ((char*)read_daq_out_data_header, tx, t5r_size, &t5r_size, handle);

	d2printf ("readDAQoutData => %d\n", t5r_size);
	fflush (stdout);
	return(tx+t5r_header_size);
}

int writeDAQoutReady(PAM_APP_Handle handle, int debugLevel)
{
	int t2w_size = 4;
	int error = 0;
	unsigned short write_daq_out_ready [2] = {0xca6f,0x0700};

	error = PAM_APP_message ((char*)write_daq_out_ready, tx, t2w_size, &t2w_size, handle);
	d2printf ("writeDAQoutReady\n");
	fflush (stdout);
}

unsigned char radio_res_buffer[2048];

int process_radio_commands (int debugLevel, int size)
{
	int i, error = 0;
	unsigned int length_rcmd, length_payload, res_buf_size, zone_id;
	unsigned char *res_payload, *ptr_res;

	PAM_APP_Handle handle;
	d2printf ("calfa2s: Going to receive radio commands\n", size);
	d2printf ("calfa2s: Receiving %d bytes\n", size);
	fflush (stdout);

	if (size > PAM_LINK_MAX_CODE_LEN)
	{
		fprintf (stderr, "calfa2s: RX size too big (> %d).\n", PAM_LINK_MAX_CODE_LEN);
		fflush (stderr);
		goto exit_on_error;
	}

	recv_binary (rx, size);

	// Show what we received
	printf ("calfa2s: Received\n");
	fflush (stdout);
#if 1	
	if (debugLevel >= 1)
	{
		printf (">> ");
		for (i = 0; i < size; i++)
			printf ("%02x ", rx[i]);
		printf ("\n");
	}
#endif

	// Convert byte stream
	// Form the byte stream to be sent to target.
	// The fwdPacket contains the following.
	// opcode         => 1 byte
	// length         => 4 bytes
	// data           => length number of bytes
	// command_type   => 1 byte
	// reserved_byte  => 1 byte
	// command_status => 1 byte

	length_payload = size;
	// determine if padding needed.
	if(length_payload % 2)
		length_payload++;
	// Open PAM
	d2printf ("calfa2s: PAM_APP_open\n");
	fflush (stdout);
	error = PAM_APP_open ("pamlink", NULL, &handle);
	d3printf ("calfa2s: return value: %d\n", error);
	fflush (stdout);
	if (error)
	{
		fprintf (stderr, "calfa2s: PAM_APP_open failed %d\n", error);
		fflush (stderr);
		goto exit_on_error;
	}

	// read AQInput ready
	printf("Going to check input\n");
	fflush (stdout);
	while(checkDAQinpReady(handle, debugLevel)) { //returns 0 if ready
		printf("Checking input\n");
		fflush (stdout);
	}
	d2printf ("DAQinpReady\n");
	fflush (stdout);
	// write AQInput size
	writeDAQinpSize(handle, length_payload, debugLevel);
	// write AQInput data
	writeDAQinpData(handle, length_payload, debugLevel);
	// write AQInput ready
	writeDAQinpReady(handle, debugLevel);
	
	ptr_res = radio_res_buffer;
	res_buf_size = 0;

	while(1) 
	{
		// read AQOutput ready
		while(!checkDAQoutReady(handle, debugLevel)) { //returns 1 if ready
			printf("Checking output\n");
			fflush (stdout);
		}
		d2printf ("checkDAQoutReady\n");
		fflush (stdout);
		// read AQOutput size
		readDAQoutSize(handle, &length_payload, debugLevel);
		// read AQOutput data
		res_payload = readDAQoutData(handle, length_payload, debugLevel);
#if 0
		// Show what we are sending
		if (debugLevel >= 1)
		{
			printf ("<< ");
			for (i = 0; i < length_payload / 2; i++)
				printf ("%04x ", ((unsigned short *) res_payload)[i]);
			printf ("\n");
			fflush (stdout);
		}
#endif
		// write AQOutput ready
		writeDAQoutReady(handle, debugLevel);
		memcpy(ptr_res, (res_payload+2), (length_payload-2));
		ptr_res += (length_payload - 2);
		res_buf_size += (length_payload - 2);

#if 0
		printf ("%02x ", res_payload[0]);
		printf ("%02x ", res_payload[1]);
#endif
		if(res_payload[0] == res_payload[1]) {
			d2printf ("captured all packets => going to exit\n");
			fflush (stdout);
			break;
		}
	}

	// Close PAM
	d2printf ("calfa2s: PAM_APP_close\n");
	fflush (stdout);
	error = PAM_APP_close (handle);
	d3printf ("calfa2s: return value: %d\n", error);
	fflush (stdout);
	if (error)
	{
		fprintf (stderr, "cala2s: PAM_APP_close failed %d\n", error);
		fflush (stderr);
		goto exit_on_error;
	}

#if 1
	// Show what we are sending
	if (debugLevel >= 1)
	{
		printf ("<< ");
		for (i = 0; i < res_buf_size; i++)
			printf ("%02x ", radio_res_buffer[i]);
		printf ("\n");
	}
#endif
	// Send
	// Add length of zone_id to res_buf_size
	zone_id = 1;
	res_buf_size += sizeof(zone_id);
	send_number (res_buf_size);
	// send zone_id
	send_number (zone_id);
	send_binary (radio_res_buffer, (res_buf_size-sizeof(zone_id)));
	d2printf ("calfa2s: Sent\n");
	fflush (stdout);
	goto exit_normally;

  exit_on_error:
	error = 1;
  exit_normally:
	return error;
}
