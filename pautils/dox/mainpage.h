//
// $Source: /cvsstl/ti10/la/ti/pautils/dox/mainpage.h,v $
// $Revision: 1.3 $
//
// Doxygen main page information
//
// Copyright 2008 Texas Instruments Incorporated.  All rights reserved.
// Texas Instruments Proprietary and Confidential.
// Use without a license from Texas Instruments is prohibited.
//
//

/// \mainpage Linux PA API Documentation
///
/// \section Introduction
/// There are several API components provided that work in concert to enable
/// creation of applications for communicating to a DSP running PA.
/// The first of these is the PA_LINK layer which provides for system wide
/// LINK configuration. Currently only control and status communication
/// services are provided but data transport is a future need. This layer
/// while currently only providing configuration and initialization for
/// basic communication will be extended when data APIs are created. Therefore
/// this API layer is required of all applications interracting via LINK to
/// DSP PA images . Moreover, this layer provides a single location to
/// bottleneck the myriad of LINK configuration options.
///
/// The next are the PAM and NIC layers which provide control and status
/// communication with the DSP. The PAM API is a messaging communication
/// model and is used to read and write PA DSP component register files.
/// The NIC API is a subscribe and publish, or signalling communication
/// model. The DSP image must be built to generate a signal based on
/// internal events. The ARM side NIC API provides functionality to block
/// processes until such notification from the DSP is provided. Each of 
/// these APIs is in turn split into Application and Transport Protocol
/// layers. The application layer provides a common interface to the
/// programmer which abstracts the details of physical transport. Such
/// abstraction allows for creation of codes which can handle multiple
/// targets in a seemless fashion. The physical layer handles the details
/// of the communication medium such as SPI, I2C, or LINK. Currently only
/// the LINK physical transport is provided.
///
/// By design, users can intersect and use the API at either the PAM_LINK
/// or PAM_APP layer. It is recommended to use the PAM_APP for a couple
/// of reasons. For one it is independent of physical transport so that
/// when other trasnports are available, e.g. PAM_SPI/PAM_I2C, then the
/// application will not need to change. Also if the application state
/// machine handles mutliple targets (e.g. one LINK and one SPI) then
/// the resultant code is cleaner / more general. Also this layer hides
/// some of the LINK transport implementation details. Therefore when
/// these details change then the impact on their application code 
/// should be minimized.
///
/// \example test_pamlink.c
///
/// \defgroup LINK Link Control
/// \defgroup PAM Alpha Messaging
/// \defgroup NIC Notify of Information Change
