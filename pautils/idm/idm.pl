
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http:\/\/www.ti.com\/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#! /usr/bin/perl
#
# Alpha code inverse compilation
#
# -----------------------------------------------------------------------------

use Getopt::Long;

GetOptions ("ifiles=s" => \@inpFileList, "ofile=s" => \$outFileName);
@inpFileList = split(/,/,join(',',@inpFileList));

# Loop over each input file and create hash table of symbols keyed by values
# If file contains lines different than the form
#    #define symbol value
# Then these lines are passed through a regexp search/replace with those values
# in the hash table. I believe this assumes the hdM files are parsed first to
# create the hash table then the actual response file. This should likely be
# cleaned up and made more modular. In particular the response file should
# not contain anything to be added to the hash table so why parse it for that
# purpose?

open outFile, ">$outFileName" or die $!;
for $inpFileName (@inpFileList) {
    open inpFile, "<$inpFileName" or die $!;
    while ($line = <inpFile>) {

        # If line matches symbol definition form then add to tables
        # else if line is not blank or a comment then search/replace with hash table
        if ($line =~ /#define (.*) (.*)$/) {
            $symbol = $1;
            $value = $2;
            push (@hdMSort, $2);
            $hdM{$2} = $1;
        }
        elsif (!($line =~ /^\s*$/) && !($line =~ /^\/\//)) {
            foreach $i (@hdMSort) {
                $line =~ s/$i/$hdM{$i}/g;
            }
            print outFile $line;
        }
    }
    close inpFile;
} #for

close outFile;

#EOF
