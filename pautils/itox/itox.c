
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Preprocessed C (.i) to alpha stream (.?) conversion
//
//
// Derived from rev 1.30 of /pa/te/s3/itox.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <pasystem.h>

//#include "asp.h" /* for ASP_BUFFER_SIZE */
#define ASP_BUFFER_SIZE 8192 /* for ASP_BUFFER_SIZE */

#define lengthof(X) (sizeof (X) / sizeof (*(X)))

void
usage ()
{
    fprintf (stderr, "usage: itox [-v] [-i] [-{BIS}[n]] <input >output\n");
    fprintf (stderr, "       -v:    Display version (and continue)\n");
    fprintf (stderr, "       -i:    Ignore file limit, as when producing .hdM file\n");

    fprintf (stderr, "       -B:    Output binary records [default]\n");
    fprintf (stderr, "       -C:    Output C records, 8-bit\n");
    fprintf (stderr, "       -D:    Output C records, 16-bit\n");
    fprintf (stderr, "       -I[n]: Output I records, address n [default 80]\n");
    fprintf (stderr, "       -N:    Output Numeric alpha code\n");
    fprintf (stderr, "       -S:    Output S records \n");
    exit (1);
}

int fileLength = 0;
int fileLimit = ASP_BUFFER_SIZE;

int sequenceLength = 0;
short lengthData[2];
short sequenceData[65536];

int sflag = 0;
int iAddress = 80;

void setSequence (unsigned long);
void fileSequence (int, FILE *);
void kickSRec (FILE *);
int  evaluate (char *);
int  wrdspn (char *);

int line = 0;

int
main (int argc, char **argv)
{
    FILE *fp;

    char string[32768];

    char *s;

    int alphaType = 'a';
    int alphaFlag = 0;

    if (argc > 1 && ! strncmp (argv[1], "-v", 2)) {
        argc--, argv++;
    }

    if (argc > 1 && ! strcmp (argv[1], "-i")) {
        argc--, argv++;
        fileLimit = 0;
    }

    if (argc > 1 && ! strcmp (argv[1], "-B")) {
        argc--, argv++;
        sflag = 0;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-S")) {
        argc--, argv++;
        sflag = 1;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-I")) {
        argc--, argv++;
        sflag = 2;
    }
    else if (argc > 1 && ! strncmp (argv[1], "-I", 2)) {
        char *errstr;
        long l = strtol (argv[1]+2, &errstr, 0);
        if (! errstr || errstr == argv[1]+2) {
            fprintf (stderr, "itox: can't grok '%s'\n", argv[1]);
            exit (1);
        }
        iAddress = l;
        sflag = 2;
        argc--, argv++;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-C")) {
        argc--, argv++;
        sflag = 3;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-D")) {
        argc--, argv++;
        sflag = 4;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-N")) {
        argc--, argv++;
        sflag = 5;
    }

    if (argc != 1)
        usage ();

    if ((unsigned )sflag >= 6) {
        fprintf (stderr, "itox: internal error\n");
        exit (1);
    }

    if (sflag == 0) {
        if (! (fp = fdopen (1, "wb"))) {
            fprintf (stderr, "itox: can't reopen stdout\n");
            exit (1);
        }
    }
    else if (sflag == 1) {
        fprintf (fp = stdout, "S0030000FC\n");
    }
    else if (sflag == 2) {
        fp = stdout;
        if (iAddress > 0xfe) {
            fprintf (stderr, "itox: MIIC-202 supports only 7-bit addressing\n");
            exit (1);
        }
        else if (iAddress & 1) {
            fprintf (stderr, "itox: MIIC-202 address format required (*2)\n");
            exit (1);
        }
        fprintf (fp, "/D%02X\n", iAddress);
        fprintf (fp, "/O\n");
    }
    else if (sflag == 3 || sflag == 4 || sflag == 5) {
        fp = stdout;
    }

    while (fgets (string, lengthof (string)-1, stdin)) {
        line++;
        if (string[0] == '\0')
            break;
        else if (string[strlen(string)-1] != '\n') {
            fprintf (stderr, "itox: string too long at line %d\n", line);
            exit (1);
        }
        for (s=string; *s; ) {
            if (isspace (*s)) 
                s++;
            else if (*s == '#')
                *s = '\0';
            else if (*s == '"') {
                unsigned char *t = s;
                unsigned char saving;
                int wanting = 0;
                int slashing = 0;
                int stringing = 0;
                for (;;) {
                    if (stringing) {
                        int c = *t++;
                        if (c == '\0') {
                            if (t[-2] == '\n')
                                t[-2] = '\0';
                            fprintf (stderr, "itox: unterminated string '%s' "
                                "at line %d\n", s, line);
                            exit (1);
                        }
                        else if (slashing) {
                            slashing = 0;
                            if (strspn (t-1, "01234567") >= 3) {
                                c = c - '0';
                                c = *t++ - '0' + (c << 3);
                                c = *t++ - '0' + (c << 3);
                            }
                            else if (c == '0')
                                c = '\0';
                            else if (c == 'a')
                                c = '\a';
                            else if (c == 'n')
                                c = '\n';
                            else if (c == 'r')
                                c = '\r';
                        }
                        else if (c == '\\') {
                            slashing = 1;
                            continue;
                        }
                        else if (c == '"') {
                            stringing = 0;
                            continue;
                        }
                        if (wanting ^= 1)
                            saving = c;
                        else
                            setSequence (saving + (c<<24>>16));
                    }
                    else if (*t == '\0' || *t == ',' || *t == ';')
                        break;
                    else if (isspace (*t))
                        t++;
                    else if (*t == '"') {
                        stringing = 1;
                        t++;
                    }
                    else {
                        if (t[-1] == '\n')
                            t[-1] = '\0';
                        fprintf (stderr, "itox: characters '%s' follow "
                            "string at line %d\n", s, line);
                        exit (1);
                    }
                }
                if (wanting)
                    setSequence (saving);
                s = t;
                alphaFlag = 2;
            }
            else if (! strncmp (s, "alph", 4)) {
                fileSequence (alphaType, fp);
                alphaFlag = 1;
                alphaType = s[4];
                s += 5;
            }
            else if (! alphaFlag) {
                fprintf (stderr, "itox: no alpha keyword in '%s' at line %d\n",
                    s, line);
                exit (1);
            }
            else if (*s == ',') {
                if (alphaFlag != 2) {
                    fprintf (stderr, "itox: null value at line %d\n", line);
                    exit (1);
                }
                alphaFlag = 1;
                s++;
            }
            else if (*s == ';') {
                if (alphaFlag != 2) {
                    fprintf (stderr, "itox: null value at line %d\n", line);
                    exit (1);
                }
                alphaFlag = 0;
                s++;
            }
            else {
                int n;
                int t;
                int tt;
                if ((t = wrdspn (s))) {
                    tt = s[t];
                    s[t] = '\0';
                }
                else {
                    t = strlen (s);
                    tt = '\0';
                }

                n = evaluate (s);
                if ((n & ~0xffff) != 0 && (n & ~0x7fff) != ~0x7fff) {
                    fprintf (stderr, "itox: integer overflow in "
                        "evaluation of (%s) as (0x%08x) at line %d\n", 
                        s, n, line);
                    exit (1);
                }
                setSequence (n << 16 >> 16);
                alphaFlag = 2;
                *(s += t) = tt;
            }
        }
    }

    if (alphaFlag == 1) {
        fprintf (stderr, "itox: null value at line %d\n", line);
        exit (1);
    }

    fileSequence (alphaType, fp);

    if (sflag == 0) {
        if (fclose (fp)) {
        /* needed to properly flush prior fdopen (1, "wb") -- Cygwin bug? */
            fprintf (stderr, "itox: can't close/flush output file/stream\n");
            exit (1);
        }
    }
    if (sflag == 1) {
        kickSRec (fp);
        fprintf (fp, "S9030000FC\n");
    }
    else if (sflag == 2) {
        fprintf (fp, "/C\n");
    }

    exit (0);
}

char *evaluateString = NULL;

void
evaluateError () {
    char *s;

    for (s=evaluateString + strlen (evaluateString) - 1; 
         s >= evaluateString && isspace(*s); s--)
        *s = '\0';

    for (s=evaluateString; isspace(*s); s++)
        ;

    fprintf (stderr, "itox: can't evaluate '%s'\n", 
        s ? *s ? s : "<null string>" : "<internal error>");

    exit (1);
}

int
evaluate (char *s) {
    int n;

#if 0
    /* cint: start and initialize error handling */
    G__init_cint ("cint");
    evaluateString = s;
    G__set_aterror (&evaluateError);

    /* cint: evaluate expression */
    n = G__int (G__calc (s));

    /* cint: reset */
    G__scratch_all();
#else
    FILE *pFile;
    char fileName[20];
    char processString[256];
    int retVal = 0;

    srand (time(0));
    sprintf (fileName, "itox%04hhu.n", rand());

    sprintf (processString, "awk 'BEGIN { printf \"0x%%0x\\n\",%s}' > %s", s, fileName);
    retVal = pasystem (processString);
    if (retVal) {
	printf ("itox error %d executing awk\n", retVal);
	return 0;
    }
    pFile = fopen (fileName, "rt");
    fscanf (pFile, "0x%04x", &n);
    fclose (pFile);
    remove (fileName);

//    sscanf (s,"0x%04x", &n);

#endif
    return n;
}


unsigned int
lengthSequence (int alphaType)
{
    if (alphaType == 'a' && sequenceLength < 256) {
        lengthData[0] = 0xc900 + sequenceLength;
        return 1;
    }
    else if (alphaType == 'a' || alphaType == 'b') {
        lengthData[0] = 0xcd01;
        lengthData[1] = sequenceLength;
        return 2;
    }
    else {
        return 0;
    }

}

void
binarySequence (int alphaType, FILE *fp)
{
    int errno;

    unsigned int m, n;

    n = sequenceLength;
    m = lengthSequence (alphaType);

    if (m && (errno = fwrite(lengthData, 2, m, fp)) != m) {
        fprintf (stderr, "itox: write length failure (%d)\n at line %d\n", errno, line);
        exit (1);
    }

    if ((errno = fwrite(sequenceData, 2, n, fp)) != n) {
        fprintf (stderr, "itox: write data failure (%d) at line %d\n", errno, line);
        exit (1);
    }

    fileLength += 2 * (n + m);

    if (fileLength > fileLimit && fileLimit > 0) {
        fprintf (stderr, "itox: write file length (%d) exceeds maximum (%d) at line %d\n", fileLength, fileLimit, line);
        exit (1);
    }
}

static int cntSRec = 0;
static short dataSRec[15];

void
wordSRec (FILE *fp, short n)
{
    dataSRec[cntSRec++] = n;
    if (cntSRec == lengthof (dataSRec))
        kickSRec (fp);
}

void
kickSRec (FILE *fp)
{
    int i, n;
    int sum = 0;

    if (cntSRec > 0) {
    
        if (fileLength > 0xffff) {
            fprintf (stderr, "itox: S1 record overflow after line %d\n", line);
            exit (1);
        }

        n = 2*cntSRec + 3;
        fprintf (fp, "S1%02X%04X", n, fileLength);
        sum += n;
        sum += fileLength;
        sum += fileLength >> 8;
    
        for (i=0; i < cntSRec; i++) {
            fprintf (fp, "%02X%02X", dataSRec[i] & 0xff, (dataSRec[i] >> 8) & 0xff);
            sum += dataSRec[i];
            sum += dataSRec[i] >> 8;
        }
    
        fprintf (fp, "%02X\n", ~sum & 0xff);
        fileLength += cntSRec;
        if (fileLength > fileLimit && fileLimit > 0) {
            fprintf (stderr, "itox: write file length (%d) exceeds maximum (%d) at line %d\n", fileLength, fileLimit, line);
            exit (1);
        }
        cntSRec = 0;
    }
}

int
wrdspn (char *s)
{
    /* Like strcspn (s, ",;"), but allow characters within strings. */

    int n = 0;

    int quoted = 0;
    int slashed = 0;

    for (; *s; s++, n++) {
        if (slashed) {
            int u = strspn (s, "01234567");
            slashed = 0;
            if (u > 0) {
                u = (u >= 3 ? 2 : u-1);
                s += u;
                n += u;
            }
        }
        else if (*s == '\\')
            slashed = 1;
        else if (quoted) {
            if (*s == '"')
                quoted = 0;
        }
        else if (*s == '"')
            quoted = 1;
        else if (*s == ',' || *s == ';')
            return n;
    }

    return n;
}

void
recordSequence (int alphaType, FILE *fp)
{
    int i;

    unsigned int m, n;

    n = sequenceLength;
    m = lengthSequence (alphaType);

    for (i=0; i < m; i++)
        wordSRec (fp, lengthData[i]);
    for (i=0; i < n; i++)
        wordSRec (fp, sequenceData[i]);
}

void
mprintf (FILE *fp, int x)
{
    if (isascii (x) && isgraph(x) && x != '~')
        fprintf (fp, "%c", x);
    else
        fprintf (fp, "~%02X", x);
}

void
nprintf (FILE *fp, int x)
{
    mprintf (fp, x & 0xff);
    mprintf (fp, (x >> 8) & 0xff);
}

void
mccSequence (int alphaType, FILE *fp)
{
    int i, iLength;

    unsigned int m, n;

    n = sequenceLength;
    m = lengthSequence (alphaType);

    iLength = 2 * (n + m) + 2;
    if (iLength <= 256)
        iLength--;

    fprintf (fp, "/T");

    if (iLength & 1)
        mprintf (fp, iLength - 1);
    else
        nprintf (fp, iLength - 1);

    for (i=0; i < m; i++)
        nprintf (fp, lengthData[i]);
    for (i=0; i < n; i++)
        nprintf (fp, sequenceData[i]);

    fprintf (fp, "\n");
    fprintf (fp, "/R0\n");

    fileLength += iLength;

    if (fileLength > fileLimit && fileLimit > 0) {
        fprintf (stderr, "itox: write file length (%d) exceeds maximum (%d) at line %d\n", fileLength, fileLimit, line);
        exit (1);
    }
}

int
cprintf (FILE *fp, int x, int c)
{
    if (! c && x < 128) {
        fprintf (fp, "%02X\n", 2*x & 0xff);
        return 2*x+1;
    }
    else if (! c) {
        fprintf (fp, "%02X\n%02X\n", (2*x+1) & 0xff, (2*x+1)/256 & 0xff);
        return 2*x+2;
    }
    else {
        fprintf (fp, "%04X\n", x & 0xffff);
        return 2*x+2;
    }
}

static void
_dprintf (FILE *fp, int x, int c)
{
    if (! c)
        fprintf (fp, "%02X\n%02X\n", x & 0xff, (x >> 8) & 0xff);
    else
        fprintf (fp, "%04X\n", x & 0xffff);
}

void
countSequence (int alphaType, FILE *fp, int countType)
{
    int i, iLength;

    unsigned int m, n;

    n = sequenceLength;
    m = lengthSequence (alphaType);

    iLength = cprintf (fp, n + m, countType);

    for (i=0; i < m; i++)
        _dprintf (fp, lengthData[i], countType);
    for (i=0; i < n; i++)
        _dprintf (fp, sequenceData[i], countType);

    fileLength += iLength;

    if (fileLength > fileLimit && fileLimit > 0) {
        fprintf (stderr, "itox: write file length (%d) exceeds maximum (%d) "
            "at line %d\n", fileLength, fileLimit, line);
        exit (1);
    }
}

void
alphaSequence (int alphaType, FILE *fp)
{
    int i;

    for (i=0; i < sequenceLength; i++)
        fprintf (fp, !i ? "alpha 0x%04x" : ",0x%04x", sequenceData[i] & 0xffff);

    /* fileLength not modified & fileLimit not checked */
}

void
setSequence (unsigned long n)
{
    if (sequenceLength >= lengthof (sequenceData)) {
        fprintf (stderr, "itox: sequence too long at line %d\n", line);
        exit (1);
    }
    if ((n & ~0x7fff) != 0L && (n & ~0x7fff) != ~0x7fff) {
        fprintf (stderr, "itox: data '0x%x' exceeds 16-bit word size at line %d\n", (unsigned int) n, line);
        exit (1);
    }
    sequenceData[sequenceLength++] = n & 0xffff;
}

void
fileSequence (int alphaType, FILE *fp)
{
    if (sequenceLength != 0) {
        if (sflag == 0)
            binarySequence (alphaType, fp);
        else if (sflag == 1)
            recordSequence (alphaType, fp);
        else if (sflag == 2)
            mccSequence (alphaType, fp);
        else if (sflag == 3 || sflag == 4)
            countSequence (alphaType, fp, sflag-3);
        else if (sflag == 5 )
            alphaSequence (alphaType, fp);
    }

    sequenceLength = 0;
}

