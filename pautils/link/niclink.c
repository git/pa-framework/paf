
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// NIC LINK Protocol implementations
//
//
//

/// \file niclink.c
/// \brief NIC LINK Protocol implementations
/// \ingroup NIC

#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#include <gpptypes.h>
#include <dsplink.h>
#include <notify.h>

#include "niclink.h"

static void _NIC_LINK_callback (unsigned int event, void *arg, void *info);


// -----------------------------------------------------------------------------
/// \brief Free Resources associated with NIC LINK
///
/// \param [in]  handle Handle returned from NIC_LINK_open
/// \return 0 if successful and 1 otherwise

int NIC_LINK_close (NIC_LINK_Handle handle)
{
    int retVal;


    retVal = sem_destroy (&(handle->semObj));
    if (retVal < 0) {
        fprintf (stderr, "NIC_LINK_close: sem_init () failed %d.\n", retVal);
        return 1;
    }

    free (handle);

    return 0;
} //NIC_LINK_close

// -----------------------------------------------------------------------------
/// \brief Allocate and initialize resources associated with NIC LINK
///
/// \param [in,out]  *pHandle Pointer, which on successful return, contains address of handle
/// \param [in]     *callback Pointer to void function which is called on event notification
/// \param [in]      arg      User provided argument to be passed into callback function when called
/// \param [in,out]  reserved Reserved for future use, pass in NULL to ensure compatibility
/// \return 0 if successful and 1 otherwise

int NIC_LINK_open (NIC_LINK_Handle *pHandle, void *callback, int arg, int reserved)
{
    int retVal;


    *pHandle = (NIC_LINK_Handle) malloc (sizeof(NIC_LINK_Obj));
    if (!*pHandle) {
        fprintf (stderr, "NIC_LINK_open: malloc () failed.\n");
        return 1;
    }

    retVal = sem_init (&((*pHandle)->semObj), 0, 0);
    if (retVal < 0) {
        fprintf (stderr, "NIC_LINK_open: sem_init () failed %d.\n", retVal);
        return 1;
    }

    (*pHandle)->registerMask = 0;
    (*pHandle)->userCallback = callback;
    (*pHandle)->userArg      = arg;

    return 0;
} //NIC_LINK_open

// -----------------------------------------------------------------------------
/// \brief Enable event for callback notification
///
/// \param [in]   event Event number (0 to 31) to register. Must match DSP side code
/// \param [in]  handle Handle returned from NIC_LINK_open
/// \return 0 if successful and 1 otherwise

int NIC_LINK_register (unsigned int event, NIC_LINK_Handle handle)
{
    DSP_STATUS status;


    // only call LINK module if not already registered, otherwise do nothing
    if (!(handle->registerMask & (1 << event))) {

        status = NOTIFY_register (0,
                                  NIC_LINK_IPS_ID,
                                  NIC_LINK_IPS_EVENTNO_BASE,
                                  _NIC_LINK_callback,
                                  (void *) handle);

        if (DSP_FAILED (status)) {
            fprintf (stderr, "NIC_LINK_register: NOTIFY_register () failed. Status = [0x%0x]\n", (unsigned int) status);
            return 1;
        }

        handle->registerMask |= (1 << event);
    }

    return 0;
} //NIC_LINK_register

// -----------------------------------------------------------------------------
/// \brief Disable event for callback notification
///
/// \param [in]   event Event number (0 to 31) to unregister. 
/// \param [in]  handle Handle returned from NIC_LINK_open
/// \return 0 if successful and 1 otherwise

int NIC_LINK_unregister (unsigned int event, NIC_LINK_Handle handle)
{
    DSP_STATUS                status;


    handle->registerMask &= ~(1 << event);

    // only deregister from LINK module if all events are unregistered
    if (!handle->registerMask) {

        status = NOTIFY_unregister (0,
                                    NIC_LINK_IPS_ID,
                                    NIC_LINK_IPS_EVENTNO_BASE,
                                    _NIC_LINK_callback,
                                    (void *) handle);
        if (DSP_FAILED (status)) {
            fprintf (stderr, "NIC_LINK_unregister: NOTIFY_unregister () failed. Status = [0x%0x]\n", (unsigned int) status);
            return 1;
        }
    }

    return 0;
} //NIC_LINK_unregister

// -----------------------------------------------------------------------------
/// \brief Private function for handling MOFITY callbacks
///
/// \param [in]  event LINK NOTIFY event -- note this is different than user event registered
/// \param [in]    arg Handle returned by NIC_LINK_open
/// \param [in]   info Pointer to user event number
/// \return none

static void _NIC_LINK_callback (unsigned int event, void *arg, void *info)
{
    NIC_LINK_Handle handle = (NIC_LINK_Handle) arg;
    unsigned int userEvent = (unsigned int) info;



    if (!(handle->registerMask & (1 << userEvent))) {
        fprintf (stdout, "NIC_LINK: callback received for unregistered event %d\n", userEvent);
        return;
    }

    if (handle->userCallback)
        (*handle->userCallback) (userEvent, handle->userArg);

    return;
} //_NIC_LINK_callback

// -----------------------------------------------------------------------------
