
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA LINK configuration and initialization implementations
//
//
//

/// \file palink.c
/// \brief PA LINK configuration and initialization implementations
/// \ingroup LINK

#include <stdio.h>
#include <string.h>

#include <gpptypes.h>
#include <dsplink.h>
#include <errbase.h>
#include <loaderdefs.h>
#include <msgq.h>
#include <pool.h>
#include <proc.h>

#include <pamlink.h>
#include "palink.h"


extern LINKCFG_Object LINKCFG_config;


ZCPYMQT_Attrs  PA_LINK_mqtAttrs;

static Uint32 SampleBufSizes [NUMMSGPOOLS + NUMRINGIOPOOLS+NUMRINGIOPOOLS] =
{
    PAM_LINK_MSG_SIZE,
    SAMPLEMQT_CTRLMSG_SIZE,
    DSPLINK_ALIGN (sizeof (MSGQ_AsyncLocateMsg), DSPLINK_BUF_ALIGN),
    DSPLINK_ALIGN (sizeof (MSGQ_AsyncErrorMsg), DSPLINK_BUF_ALIGN),
    RINGIODATABUFSIZE,
    RINGIOATTRBUFSIZE,
    DSPLINK_ALIGN (sizeof (RingIO_ControlStruct), DSPLINK_BUF_ALIGN),
    DSPLINK_ALIGN (sizeof (MPCS_ShObj), DSPLINK_BUF_ALIGN),
    RRINGIODATABUFSIZE,
    RRINGIOATTRBUFSIZE,
    DSPLINK_ALIGN (sizeof (RingIO_ControlStruct), DSPLINK_BUF_ALIGN),
    DSPLINK_ALIGN (sizeof (MPCS_ShObj), DSPLINK_BUF_ALIGN)
			
};

static Uint32 SampleNumBuffers [NUMMSGPOOLS + NUMRINGIOPOOLS+NUMRINGIOPOOLS] =
{
    NUMMSGINPOOL0,
    NUMMSGINPOOL1,
    NUMMSGINPOOL2,
    NUMMSGINPOOL3,
    NUMRINGIOOBJPOOL0,
    NUMRINGIOOBJPOOL1,
    NUMRINGIOOBJPOOL2,
    NUMRINGIOOBJPOOL3,
    NUMRINGIOOBJPOOL0,
    NUMRINGIOOBJPOOL1,
    NUMRINGIOOBJPOOL2,
    NUMRINGIOOBJPOOL3

};

SMAPOOL_Attrs PA_LINK_poolAttrs =
{
    NUMMSGPOOLS + NUMRINGIOPOOLS+NUMRINGIOPOOLS,
    SampleBufSizes,
    SampleNumBuffers,
    TRUE
};

Char8 SampleGppMsgqName [DSP_MAX_STRLEN] = "GPPMSGQ1";
Char8 SampleDspMsgqName [DSP_MAX_STRLEN] = "DSPMSGQ1";
MSGQ_Queue SampleGppMsgq = (Uint32) MSGQ_INVALIDMSGQ;
MSGQ_Queue SampleDspMsgq = (Uint32) MSGQ_INVALIDMSGQ;


// -----------------------------------------------------------------------------
/// \brief Open LINK communication with PA DSP image
///k
/// \return 0 if successful and 1 otherwise

#ifndef PA_NO_BOOT
int PA_LINK_open  (char *fname)
#else
int PA_LINK_open  ()
#endif

{
    DSP_STATUS             status  = DSP_SOK;
    int                    retVal  = 0;
    NOLOADER_ImageInfo  imageInfo;



#ifdef DEBUG
    fprintf (stderr, "PROC_setup start\n");
#endif

#ifdef DEBUG
    // Print DSP name as sanity check to see if structure is valid
    fprintf (stderr, "DSP Name: %s \n", LINKCFG_config.dspConfigs [0]->dspObject->name);
#endif

#ifdef PA_NO_BOOT
    strcpy (LINKCFG_config.dspConfigs [0]->dspObject->loaderName, "NOLOADER");
    LINKCFG_config.dspConfigs [0]->dspObject->doDspCtrl = DSP_BootMode_NoBoot;

    // Pass in the modified structure instead of NULL
    status = PROC_setup (&LINKCFG_config) ;
#else
    status = PROC_setup (NULL) ;
#endif

    if (DSP_FAILED (status)) {
	fprintf (stderr, "PROC_setup () failed. Status = [0x%x]\n", (unsigned int) status);
	retVal = 1;
	goto commonExit;
    }

#ifdef DEBUG
    fprintf (stdout, "PROC_attach start\n");
#endif
    status = PROC_attach (ID_PROCESSOR, NULL) ;
    if (DSP_FAILED (status)) {
	fprintf (stderr, "PROC_attach () failed. Status = [0x%x]\n", (unsigned int) status);
	retVal = 1;
	goto commonExit;
    }

#ifdef DEBUG
    fprintf (stdout, "POOL_open start\n");
#endif
    // create common pool used by MSGQ transport
    status = POOL_open (POOL_makePoolId(ID_PROCESSOR, SAMPLE_POOL_ID), &PA_LINK_poolAttrs) ;
    if (DSP_FAILED (status)) {
	fprintf (stderr, "POOL_open () failed. Status = [0x%x]\n", (unsigned int) status);
	retVal = 1;
	goto commonExit;
    }



#ifdef DEBUG
    fprintf (stdout, "PROC_load start\n");
#endif
#ifdef PA_NO_BOOT
    // dspRunAddr and argsAddr are not needed since using NOBOOT_MODE.
    // shmBaseAddr is not needed sincePA image is using pa.cmd to inform ARM of address.

    imageInfo.dspRunAddr  = 0;
    imageInfo.argsAddr    = 0;
    imageInfo.shmBaseAddr = 0;
    imageInfo.argsSize    = 50;
    status = PROC_load (ID_PROCESSOR, (Char8 *) &imageInfo, 0, NULL) ;
#else
    status = PROC_load (ID_PROCESSOR, fname, 0, NULL);
#endif
    if (DSP_FAILED (status)) {
	fprintf (stderr, "PROC_load () failed. Status = [0x%x]\n", (unsigned int) status);
	retVal = 1;
	goto commonExit;
    }

#ifdef DEBUG
    fprintf (stdout, "PROC_start start\n");
#endif
    status = PROC_start (ID_PROCESSOR) ;
    if (DSP_FAILED (status)) {
	printf ("PROC_start () failed. Status = [0x%x]\n", (unsigned int) status);
	retVal = 1;
	goto commonExit;
    }

    // I think this is needed since, like above, if the link libraries are built
    // with MSGQ then a handshake is required for each component. Specifically
    // the DSPLINK_init call in the DSP main function will wait until this is open
    // to complete the handshake.
    // TODO: confirm
    PA_LINK_mqtAttrs.poolId = POOL_makePoolId(ID_PROCESSOR, SAMPLE_POOL_ID);
    status = MSGQ_transportOpen (ID_PROCESSOR, &PA_LINK_mqtAttrs) ;
    if (DSP_FAILED (status)) {
	printf ("MSGQ_transportOpen () failed. Status = [0x%x]\n", (unsigned int) status) ;
	retVal = 1;
	goto commonExit;
    }

commonExit:
    return retVal;

} //PA_LINK_open

// -----------------------------------------------------------------------------
/// \brief Close LINK communication with PA DSP image
///
/// \return 0 if successful and 1 otherwise

int PA_LINK_close (void)
{
    DSP_STATUS             status  = DSP_SOK;
    int                    retVal  = 0;
    // implementation TBD
    status = MSGQ_transportClose (ID_PROCESSOR) ;
    if (DSP_FAILED (status)) {
		printf ("MSGQ_transportClose () failed. Status = [0x%x]\n", (unsigned int) status) ;
		retVal = 1;
		goto commonExit;
    }
	status = PROC_stop(ID_PROCESSOR);
    if (DSP_FAILED (status)) {
		printf ("PROC_stop () failed. Status = [0x%x]\n", (unsigned int) status) ;
		retVal = 1;
		goto commonExit;
    }
	status = POOL_close(PA_LINK_mqtAttrs.poolId);
    if (DSP_FAILED (status)) {
		printf ("POOL_close() failed. Status = [0x%x]\n", (unsigned int) status) ;
		retVal = 1;
		goto commonExit;
    }
	status = PROC_detach(ID_PROCESSOR);
    if (DSP_FAILED (status)) {
		printf ("PROC_detach failed. Status = [0x%x]\n", (unsigned int) status) ;
		retVal = 1;
		goto commonExit;
    }
	status = PROC_destroy();
    if (DSP_FAILED (status)) {
		printf ("PROC_destroy failed. Status = [0x%x]\n", (unsigned int) status) ;
		retVal = 1;
		goto commonExit;
    }

commonExit:
    return retVal;
} //PA_LINK_close

// -----------------------------------------------------------------------------
