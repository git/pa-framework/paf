
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA LINK configuration and initialization definitions
//
//
//

/// \file palink.h
/// \brief PA LINK configuration and initialization definitions
/// \ingroup LINK

#ifndef PALINK_H
#define PALINK_H

#include <gpptypes.h>
#include <dsplink.h>
#include <msgq.h>
#include <pool.h>
#include <proc.h>
#include <mpcs.h>
#include <pool.h>
#include <ringio.h>


#define ID_PROCESSOR    0

#define SAMPLE_POOL_ID  0

#define NUMMSGPOOLS     4

#define NUMMSGINPOOL0   1
#define NUMMSGINPOOL1   2
#define NUMMSGINPOOL2   2
#define NUMMSGINPOOL3   4

#define NUMRINGIOPOOLS  4

#define NUMRINGIOOBJPOOL0   1
#define NUMRINGIOOBJPOOL1   1
#define NUMRINGIOOBJPOOL2   1
#define NUMRINGIOOBJPOOL3   1

#define RINGIODATABUFSIZE   0x3C000
#define RRINGIODATABUFSIZE  0x6000
#define RINGIOATTRBUFSIZE   0x600
#define RRINGIOATTRBUFSIZE  0x100




#define SAMPLEMQT_CTRLMSG_SIZE  ZCPYMQT_CTRLMSG_SIZE

extern SMAPOOL_Attrs PA_LINK_poolAttrs;
extern ZCPYMQT_Attrs PA_LINK_mqtAttrs;

extern Char8 SampleGppMsgqName [DSP_MAX_STRLEN];
extern Char8 SampleDspMsgqName [DSP_MAX_STRLEN];
extern MSGQ_Queue SampleGppMsgq;
extern MSGQ_Queue SampleDspMsgq;

#ifndef PA_NO_BOOT
extern int PA_LINK_open  (char *);
#else
extern int PA_LINK_open  ();
#endif

extern int PA_LINK_close (void);

#endif //PALINK_H

