
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/M LINK Transport implemenations
//
//
//

/// \file pamlink.c
/// \brief PA/M LINK Transport implementations
/// \ingroup PAM

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sched.h>

#include <gpptypes.h>
#include <dsplink.h>
#include <msgq.h>

#include <palink.h>
#include "pamlink.h"

//#define MANAGE_PRIORITY

// -----------------------------------------------------------------------------
/// \brief Free Resources associated with PAM LINK transport
///
/// \param [in]  handle Handle returned from PAM_LINK_open
/// \return 0 if successful and 1 otherwise

int PAM_LINK_close  (PAM_LINK_Handle handle)
{
    DSP_STATUS                status   = DSP_SOK;


    status = MSGQ_free ((MSGQ_Msg) handle->pMsg);
    if (DSP_FAILED (status)) {
        fprintf (stderr, "PAM_LINK_close: MSGQ_free () failed. Status = [0x%x]\n", (unsigned int) status);
        return 1;
    }

    status = MSGQ_close (SampleGppMsgq);
    if (DSP_FAILED (status)) {
        fprintf (stderr, "PAM_LINK_close: MSGQ_close () failed. Status = [0x%x]\n", (unsigned int) status);
        return 1;
    }

    free (handle);
    
    return 0;
} //PAM_LINK_close

// -----------------------------------------------------------------------------
/// \brief Allocate and initialize resources associated with PAM LINK transport
///
/// \param [in,out]  *pHandle Pointer, which on successful return, contains address of handle
/// \param [in,out]  reserved Reserved for future use, pass in NULL to ensure compatibility.
/// \return 0 if successful and 1 otherwise

int PAM_LINK_open (PAM_LINK_Handle *pHandle, int reserved)
{
    DSP_STATUS                status   = DSP_SOK;
    MSGQ_LocateAttrs syncLocateAttrs;


    status = MSGQ_open (SampleGppMsgqName, &SampleGppMsgq, NULL) ;
    if (DSP_FAILED (status)) {
        fprintf (stderr, "PAM_LINK_open: MSGQ_open () failed. Status = [0x%x]\n", (unsigned int) status) ;
        return 1;
    }
    status = MSGQ_setErrorHandler (SampleGppMsgq, SAMPLE_POOL_ID) ;
    if (DSP_FAILED (status)) {
        fprintf (stderr, "PAM_LINK_open: MSGQ_setErrorHandler () failed. Status = [0x%x]\n", (unsigned int) status) ;
        return 1;
    }

    // Locate the DSP's message queue
    if (DSP_SUCCEEDED (status)) {
        syncLocateAttrs.timeout = WAIT_FOREVER;
        status = DSP_ENOTFOUND ;
        while ((status == DSP_ENOTFOUND) || (status == DSP_ENOTREADY)) {
            status = MSGQ_locate (SampleDspMsgqName,
                                  &SampleDspMsgq,
                                  &syncLocateAttrs) ;
            if ((status == DSP_ENOTFOUND) || (status == DSP_ENOTREADY))
                usleep (100000) ;
            else if (DSP_FAILED (status)) {
                fprintf (stderr, "PAM_LINK_open: MSGQ_locate () failed. Status = [0x%x]\n", (unsigned int) status);
                return 1;
            }
        }
    }

    *pHandle = (PAM_LINK_Handle) malloc (sizeof(PAM_LINK_Obj));
    if (!*pHandle) {
        fprintf (stderr, "PAM_LINK_open: malloc () failed.\n");
        return 1;
    }

    status = MSGQ_alloc (SAMPLE_POOL_ID, PAM_LINK_MSG_SIZE, (MSGQ_Msg *) &((*pHandle)->pMsg));
    if (DSP_FAILED (status)) {
        fprintf (stderr, "PAM_LINK_open: MSGQ_alloc () failed. Status = [0x%x]\n", (unsigned int) status);
        return 1;
    }

    return 0;
} //PAM_LINK_open

// -----------------------------------------------------------------------------
/// \brief Read response from DSP to ARM. May or may not contain alpha code response
///
/// \param [out]       *pBuf Pointer to user buffer which will contain response data
/// \param [out]     *pBytes Pointer to int which contains number of bytes written to pBuf
/// \param [in,out]   handle Handle returned from PAM_LINK_open
/// \return 0 if successful and 1 otherwise

int PAM_LINK_read  (char *pBuf, int *pBytes, PAM_LINK_Handle handle)
{
    DSP_STATUS                status   = DSP_SOK;
#if defined(MANAGE_PRIORITY)
	struct sched_param priority;
#endif /* MANAGE_PRIORITY */

#if defined(MANAGE_PRIORITY)
	priority.sched_priority = 10;
	sched_setscheduler (0, SCHED_RR, &priority);
#endif /* MANAGE_PRIORITY */

    status = MSGQ_get (SampleGppMsgq, WAIT_FOREVER, (MSGQ_Msg *) &handle->pMsg) ;

#if defined(MANAGE_PRIORITY)
	priority.sched_priority = 0;
	sched_setscheduler (0, SCHED_RR, &priority);
#endif /* MANAGE_PRIORITY */

    if (DSP_FAILED (status)) {
        fprintf (stderr, "MSGQ_get () failed. Status = [0x%x]\n", (unsigned int) status);
        return 1;
    }

    *pBytes  = (handle->pMsg->alphaCode[1] << 8) | handle->pMsg->alphaCode[0];
    memcpy (pBuf, &handle->pMsg->alphaCode[2], *pBytes);

    return 0;
} //PAM_LINK_read

// -----------------------------------------------------------------------------
/// \brief Write alpha code packet via MSGQ from ARM to DSP
///
/// \param [in]       *pBuf Pointer to user buffer containing alpha code packet
/// \param [in]        size Number of bytes of valid data in pBuf
/// \param [in,out]   handle Handle returned from PAM_LINK_open
/// \return 0 if successful and 1 otherwise

int PAM_LINK_write  (char *pBuf, int size, PAM_LINK_Handle handle)
{
    DSP_STATUS                status   = DSP_SOK;
#if defined(MANAGE_PRIORITY)
	struct sched_param priority;
#endif /* MANAGE_PRIORITY */

    handle->pMsg->alphaCode[0] = (size & 0xFF);
    handle->pMsg->alphaCode[1] = (size & 0xFF00) >> 8;
    memcpy (&handle->pMsg->alphaCode[2], pBuf, size);

#if defined(MANAGE_PRIORITY)
	priority.sched_priority = 10;
	sched_setscheduler (0, SCHED_RR, &priority);
#endif /* MANAGE_PRIORITY */

    status = MSGQ_put (SampleDspMsgq, (MSGQ_Msg) handle->pMsg) ;

#if defined(MANAGE_PRIORITY)
	priority.sched_priority = 0;
	sched_setscheduler (0, SCHED_RR, &priority);
#endif /* MANAGE_PRIORITY */

    if (DSP_FAILED (status)) {
        fprintf (stderr, "MSGQ_put () failed. Status = [0x%x]\n", (unsigned int) status) ;
        return 1;
    }

    return 0;
} //PAM_LINK_write

// -----------------------------------------------------------------------------
