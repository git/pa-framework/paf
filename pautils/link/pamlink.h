
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/M LINK Transport definitions
//
//
//

/// \file pamlink.h
/// \brief PA/M LINK Transport definitions
/// \ingroup PAM

#ifndef PAMLINK_H
#define PAMLINK_H

#include <gpptypes.h>
#include <dsplink.h>
#include <msgq.h>

#define PAM_LINK_MAX_CODE_LEN (1024 - sizeof (MSGQ_MsgHeader))

typedef struct PAM_LINK_Msg { 
    // MSGQ_MsgHeader portion
    Uint32 reserved[MSG_HEADER_RESERVED_SIZE];   // Transport specific                         
    Uint16 srcProcId;                            // Proc Id for the src message queue          
    Uint16 poolId;                               // Id of the allocator that allocated the msg 
    Uint16 size;                                 // Size of the allocated msg                  
    Uint16 dstId;                                // Destinaton message queue id                
    Uint16 srcId;                                // Source message queue id                    
    Uint16 msgId;                                // User specified message id                  
    // PAM_LINK specific portion
    Uint8  alphaCode[PAM_LINK_MAX_CODE_LEN];     // alpha code payload
} PAM_LINK_Msg;

typedef struct PAM_LINK_Obj {
    // PAM LINK specific portion
    PAM_LINK_Msg *pMsg;
} PAM_LINK_Obj, *PAM_LINK_Handle;

#define PAM_LINK_MSG_SIZE  DSPLINK_ALIGN (sizeof (PAM_LINK_Msg), DSPLINK_BUF_ALIGN)

extern int PAM_LINK_close    (PAM_LINK_Handle handle);
extern int PAM_LINK_open     (PAM_LINK_Handle *pHandle, int reserved);
extern int PAM_LINK_read     (char *pBuf, int *pBytes, PAM_LINK_Handle handle);
extern int PAM_LINK_write    (char *pBuf, int size, PAM_LINK_Handle handle);

#endif //PAMLINK_H
