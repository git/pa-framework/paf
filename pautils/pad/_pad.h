
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/** ============================================================================
 *	@file	_pad.h
 *
 *	============================================================================
 *	Copyright (c) Texas Instruments Incorporated 2002-2008
 *
 *	Use of this software is controlled by the terms and conditions found in the
 *	license agreement under which this software has been supplied or provided.
 *	============================================================================
 */

/// \file _pad.h
/// \brief PAD - Ring IO Access APIs
/// \ingroup PAD

#if !defined (_PAD_H)
#define _PAD_H


/*	----------------------------------- DSP/BIOS Link				  */
#include <dsplink.h>


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */


// -----------------------------------------------------------------------------
/// \brief	PAD_Object 
///
/// \brief	Struct for PAD_Handles.

typedef struct PAD_Object
{
	RingIO_Handle		ringIOHandle;
	Pvoid				semPtr;
	int					mode;
} *PAD_Handle;


// -----------------------------------------------------------------------------
/// \brief	PAD_ErrorType 
///
/// \brief	Enum for error values.


enum PAD_ErrorType {
	PAD_SUCCESS = DSP_SOK,
	PAD_FAILURE = DSP_EFAIL
};


// -----------------------------------------------------------------------------
/// \brief	PAD_SUCCEEDED 
///
/// \brief	Inline function to check for success.

static inline int PAD_SUCCEEDED(int status) {
	return DSP_SUCCEEDED(status);
}


// -----------------------------------------------------------------------------
/// \brief	PAD_FAILED 
///
/// \brief	Inline function to check for success.


static inline int PAD_FAILED(int status) {
	return DSP_FAILED(status);
}

// -----------------------------------------------------------------------------
/// \brief	PAD_0Print
///
/// \brief	Print a message without any arguments.
///			This is a OS specific function and is implemented in file: (GPPOS)\ring_io_os.c
/// \param [in]	str	- String message to be printed.

NORMAL_API
Void
PAD_0Print (Char8 * str) ;


// -----------------------------------------------------------------------------
/// \brief	PAD_1Print
///
/// \brief	Print a message with one arguments.
///			This is a OS specific function and is implemented in file: (GPPOS)\message_os.c
/// \param [in]	str	- String message to be printed.
/// \param [in]	arg - Argument to be printed.

NORMAL_API
Void
PAD_1Print (Char8 * str, Uint32 arg) ;

// -----------------------------------------------------------------------------
/// \brief   PAD_Sleep
///
/// \brief   Sleeps for the specified number of microseconds.
/// \param [in]    uSec -  Microseconds to sleep.

NORMAL_API
Void
PAD_Sleep (Uint32 uSec) ;


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (_PAD_H) */
