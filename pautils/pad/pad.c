
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PAD API Implementations
//
//
//

/****************************WARNING*******************************/
/*****The following APIs are not final, they may change in future ******/

/// \file pad.c
/// \brief PAD - Ring IO Access APIs
/// \ingroup PAD

/*	------------------------ System ----------------------------------*/
#include <string.h>
#include <stdlib.h>
#ifdef PAD_DEBUG
#include <stdio.h>
#endif

/*	------------------------ DSP/BIOS Link ----------------------------------*/
#include <dsplink.h>

/*	------------------------DSP/BIOS LINK API -------------------------------*/
#include <proc.h>
#include <mpcs.h>
#include <pool.h>
#include <ringio.h>

/*	------------------------ Application Header------------------------------*/
#include <palink.h>
#include <pad_os.h>
#include <_pad.h>
#include <pad.h>

#define MAX_FNAME 256
// -----------------------------------------------------------------------------
/// \brief	RingIOWriterName
///
/// \brief	Name of the RingIO used by the application in reader and writer mode.

STATIC Char8 PAD_OutRingIOName [RINGIO_NAME_MAX_LEN] = "RRINGIO_ARM_DSP_01"; 
STATIC Char8 PAD_RingIOName  [RINGIO_NAME_MAX_LEN] =  "RINGIO_ARM_DSP_01";

volatile int dataend = 0;

// -----------------------------------------------------------------------------
/// \brief	NUMPADOBJECTS
///
/// \brief	varible that specifies number of PAD Objects.



// ----------------------------------------------------------------------------
#define NUMPADOBJECTS 2
// -----------------------------------------------------------------------------

#define DUMMY_SIZE 1
#define NOTIFY_DATA_END         4u

// -----------------------------------------------------------------------------
/// \brief	PAD_WRITER_WATERMARK
/// \brief	If RingIO buffer is full, ARM is waiting for space in RingIO buffer
/// \varible that specifies minimum empty space needs before acquiring the RingIO buffer to write.
/// \Once PAD_WRITER_WATERMARK level space is available in RingIO, writer starts to write

/// \brief	PAD_READER_WATERMARK
/// \brief	If RingIO buffer is empty, ARM is waiting for data in RingIO buffer
/// \Varible that specifies minimum data required before acquiring the RingIO buffer to read.
/// \Once PAD_READER_WATERMARK level space is available in RingIO, Reader starts to read

/// \brief	PAD_READER_BUF_SIZE
/// \brief	varible that specifies maximum acquire size in read mode.


STATIC const Uint32 PAD_WRITER_WATERMARK = 1024*2;
STATIC const Uint32 PAD_READER_BUF_SIZE = 1024;
STATIC const Uint32 PAD_READER_WATERMARK = 1;

// -----------------------------------------------------------------------------
/// \brief	padObjectStatus
///
/// \brief	array that track pad object usage.

STATIC Uint32 padObjectStatus[NUMPADOBJECTS];

// -----------------------------------------------------------------------------
/// \brief	Reader and writerClientInfo
///
/// \brief	Reader and Writer task information structure.

STATIC struct PAD_Object padObject[NUMPADOBJECTS];

#ifdef PAD_DEBUG
FILE *pOutFile;
int count_data = 0;
int count_meta = 0;
#endif

// -----------------------------------------------------------------------------
/// \brief	readerNotify
///
/// \brief	This function implements the notification callback for the RingIO
///			opened by the GPP in reader  mode.
/// \param [in] handle 	-	Handle to the RingIO.
/// \param [in] param 	- Parameter used while registering the notification.
/// \param [in] msg 		-   Message passed along with notification.

static Void _readerNotify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg);

// -----------------------------------------------------------------------------
/// \brief	writerNotify
///
/// \brief	This function implements the notification callback for the RingIO
///			opened by the GPP in writer  mode.
/// \param [in] handle 	-	Handle to the RingIO.
/// \param [in] param 	- Parameter used while registering the notification.
/// \param [in] msg 		-   Message passed along with notification.

static Void _writerNotify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg);

// -----------------------------------------------------------------------------
/// \brief This function allocates and initializes PAD & DSPLink infrastructure
///
/// \return 0 if successful and 1 otherwise

int PAD_init (void)
{
    int status = 0;
    int      i;
    char fname[MAX_FNAME];

#ifndef PAD_DEBUG
   // default file name for PROC_load
    strncpy( fname, "/home/root/pa.out", MAX_FNAME);

    status = PA_LINK_open (fname);
    if (status) {
        PAD_1Print ("PA_LINK_open failed %d\n", status);
        return status;
    }

    for (i=0; i < NUMPADOBJECTS; i++) {
        padObject[i].ringIOHandle = NULL;
        padObject[i].semPtr       = NULL;
    }

#endif
    return status;
} //PAD_init

// -----------------------------------------------------------------------------
/// \brief This function allocates and initializes RingIO object
///
/// \param [in,out]  **pHandle Pointer, which on successful return, contains address of handle
/// \param [in,out]  mode - Specifies the RingIO client mode. Either as reader client or writer client.
/// \param [in,out]  reserved2 Reserved for future use, pass in NULL to ensure compatibility.
/// \return 0 if successful and 1 otherwise

int PAD_open (void **pHandle, int mode, int reserved2)
{
    int               status = PAD_SUCCESS;
    PAD_Handle     padHandle;
    unsigned int       flags;


    *pHandle = NULL;
     /* TODO: allocate PAD object from free one and return if all are in use */
     /* Till then below code solves the problem using a crude way */
     if(mode == O_RDONLY) padHandle = &padObject[0];
     else if (mode == O_WRONLY) padHandle = &padObject[1];
     else status = PAD_FAILURE;
	 
#ifndef PAD_DEBUG
    // Open the RingIO to be used with GPP as the reader/writer based on the mode.
    if (PAD_SUCCEEDED (status)) {
	    if (padHandle->ringIOHandle == NULL) {
			flags = RINGIO_CONTROL_CACHEUSE; 
		    if(mode ==O_RDONLY)	
			{  
				padHandle->ringIOHandle = RingIO_open (PAD_OutRingIOName, mode, flags) ;
				dataend = 0;//Reader end flag
			}
			else
			{
				padHandle->ringIOHandle = RingIO_open (PAD_RingIOName, mode, flags) ;
			}
	        if (padHandle->ringIOHandle == NULL) {
	            status = RINGIO_EFAILURE;
	            PAD_0Print ("RingIO_open () Open failed.\n");
	        }
	        else {
		        padHandle->mode = mode;
	        }
	    }
	}

    if (PAD_SUCCEEDED (status)) {
        if (padHandle->semPtr == NULL) {
            // Create the semaphore to be used for notification
            status = PAD_CreateSem (&(padHandle->semPtr)) ;
            if (PAD_FAILED (status)) {
                PAD_1Print ("PAD_CreateSem () SEM creation failed "
                            "Status = [0x%x]\n", status) ;
            }
        }
    }

    if (PAD_SUCCEEDED (status)) {
        // Set the notification for Writer.
        status = RingIO_setNotifier (padHandle->ringIOHandle,
                                     RINGIO_NOTIFICATION_ONCE,
                                     mode?PAD_WRITER_WATERMARK:PAD_READER_WATERMARK,
                                     mode?&_writerNotify:&_readerNotify,
                                     (RingIO_NotifyParam) (padHandle)) ;

        if (status != RINGIO_SUCCESS) {
            PAD_1Print ("RingIO_setNotifier failed Status = [0x%x]\n",status);
            RingIO_close (padHandle->ringIOHandle);
            padHandle->ringIOHandle = NULL;
            PAD_DeleteSem (padHandle->semPtr);
            padHandle->semPtr= NULL;
            return PAD_NOTIFYERR;
        }

        *pHandle = padHandle;
    }
#else
    pOutFile = fopen("rio_output", "wb");
    if (pOutFile == NULL) {
        fprintf(stderr, "Couldnot open the output file\n");
        return 1;
    }
    fprintf (stdout, "exiting PAD_open\n");
    *pHandle = padHandle;
#endif

    if (PAD_SUCCEEDED (status))
        status = 0;
    else
        status = 1;

    return (status);
} //PAD_open

// -----------------------------------------------------------------------------
/// \brief This function implements the read API
///
/// \param [in,out]    *handle Handle returned from PAD_open
/// \param [in]        *pBuffer Pointer to user temporary buffer where the acquired (read) data from RingIO is stored
/// \param [in]        size maximum Number of bytes to be acquired (read) from the RingIO.
/// \return 0 if successful and 1 otherwise

int PAD_read (void *handle, void * pBuffer, Uint32 size, int reserved)
{
    int                 status = PAD_SUCCESS;
    RingIO_BufPtr		bufPtr = NULL;
    Uint32              bytesTransfered = 0;
    Uint32              acqSize ;
    PAD_Handle          padHandle = (PAD_Handle)handle;
    RingIO_Handle       ringIOHandle = padHandle->ringIOHandle;
    
    //TODO: Check for valid handle
    if (pBuffer == NULL)
        status = PAD_FAILURE;
#ifdef PAD_DEBUG
    count_meta++;
#endif
    while ((bytesTransfered < size) && (PAD_SUCCEEDED (status))) {

        // Acquire reader bufs and initialize and release them.
        if((size - bytesTransfered) < PAD_READER_BUF_SIZE)
            acqSize = (size - bytesTransfered);
        else
            acqSize = PAD_READER_BUF_SIZE;
#ifndef PAD_DEBUG
        status = RingIO_acquire (ringIOHandle, &bufPtr, &acqSize);
        if(status == RINGIO_EBUFEMPTY) {
			if(dataend == 1)
			{
				//stopSignal = 1;
				memset((int)pBuffer + bytesTransfered, 0, size - bytesTransfered);
				return 1;
			}
           // Wait for data to read from the reverse ring IO buffer
            status = PAD_WaitSem (padHandle->semPtr) ;
            if (PAD_FAILED (status))
                PAD_1Print ("PAD_WaitSem () Reader SEM failed Status = [0x%x]\n", status);
        }

        else if (acqSize > 0) {
		
            // If acquire success then write to temp buffer and then release
            // the acquired.
			memcpy ((void *) ((int)pBuffer + bytesTransfered), bufPtr,acqSize);
		   
			bytesTransfered += acqSize;

            status = RingIO_release (ringIOHandle, acqSize);
            if (DSP_FAILED (status))
                PAD_1Print ("RingIO_release () in Reader task failed. relStatus = [0x%x]\n", status);
        }
#else
		count_data++;
        memcpy ((void *) ((int)pBuffer + bytesTransfered), bufPtr,acqSize);
        bytesTransfered += acqSize; 
#endif
    }

    if(PAD_SUCCEEDED (status))
        status = 0;
    else
        status = 1;
    return (status) ;
} //PAD_read
// -----------------------------------------------------------------------------
/// \brief This function implements the write API
///
/// \param [in,out]    *handle Handle returned from PAD_open
/// \param [in]        *pBuffer Pointer to user buffer containing audio data
/// \param [in]        size Number of bytes of valid data in pBuffer
/// \return 0 if successful and 1 otherwise

int PAD_write (void *handle, void * pBuffer, Uint32 size, PAD_MetaData * meta)
{
    int                             status = PAD_SUCCESS;
    int                             attr_status = PAD_SUCCESS;
    RingIO_BufPtr                   bufPtr = NULL;
    Uint32                 bytesTransfered = 0;
    Uint32                         acqSize ;
    PAD_Handle                   padHandle = (PAD_Handle)handle;
    RingIO_Handle             ringIOHandle = padHandle->ringIOHandle;
    PAD_MetaData              *attr = meta;


    //TODO: Check for valid handle
    if (pBuffer == NULL)
        status = PAD_FAILURE;

    do{
		attr_status = RingIO_setvAttribute(ringIOHandle, 0, 0, 0, attr, sizeof(PAD_MetaData));
		if (DSP_FAILED (attr_status)) {
			usleep(10);
		}	   
	} while (DSP_FAILED (attr_status)) ; 
  
#ifdef PAD_DEBUG
    count_meta++;
#endif

    while ((bytesTransfered < size) && (PAD_SUCCEEDED (status))) {

        // Acquire writer bufs and initialize and release them.
        
        acqSize = size - bytesTransfered;

#ifndef PAD_DEBUG
        status = RingIO_acquire (ringIOHandle, &bufPtr, &acqSize);
        if (PAD_FAILED (status)) {
            // Wait for space to write in buffer
            status = PAD_WaitSem (padHandle->semPtr) ;
			if (PAD_FAILED (status))
                PAD_1Print ("PAD_WaitSem () Writer SEM failed Status = [0x%x]\n", status);
        }
        else if (acqSize > 0) {
 	        // If acquire success then write to ring bufer and then release the acquired.
            memcpy (bufPtr, (void *) ((int)pBuffer + bytesTransfered), acqSize);
            bytesTransfered += acqSize;

            status = RingIO_release (ringIOHandle, acqSize);
            if (DSP_FAILED (status))
                PAD_1Print ("RingIO_release () in Writer task failed. relStatus = [0x%x]\n", status);
        }
#else
		count_data++;
        size = fwrite((void *) ((int)pBuffer + bytesTransfered), acqSize, 1, pOutFile);
        if (size < 1) {
            fprintf(stderr, "Could not write to file\n");
            return 1;
        }
        bytesTransfered += acqSize;
#endif
    }

    if(PAD_SUCCEEDED (status))
        status = 0;
    else
        status = 1;
		
    return (status) ;
} //PAD_write

// -----------------------------------------------------------------------------
/// \brief This function closes RingIO object
///
/// \param [in]  *handle Handle returned from PAD_open
/// \return 0 if successful and 1 otherwise

int PAD_close (void *handle)
{
    int                 status = PAD_SUCCESS;
    PAD_Handle       padHandle = (PAD_Handle)handle;
    RingIO_Handle ringIOHandle = padHandle->ringIOHandle;
    Uint16                type;  // Dummy Variable to satisfy the Flush Call
    Uint32               param;  // Dummy Variable to satisfy the Flush Call
    Uint32        bytesFlushed,validDataLevel=1;

#ifdef PAD_DEBUG
	PAD_1Print ("count_data %d ", count_data);
    PAD_1Print ("count_meta %d \n", count_meta);
#endif
    if (ringIOHandle == NULL) {
        PAD_1Print ("PAD_close () Null Handle ", 1);
        return PAD_FAILURE;
    }

    if(padHandle->mode == O_WRONLY) {
	    	
		while(validDataLevel>0)
			validDataLevel = RingIO_getValidSize (ringIOHandle);
	    
					status = RingIO_sendNotify (ringIOHandle, (RingIO_NotifyMsg)NOTIFY_DATA_END);
	    if (PAD_FAILED (status))
	        PAD_1Print ("PAD_close () SendNotify failed Status = [0x%x]\n", status);	
	}
	
    status = RingIO_close (ringIOHandle);
    if (PAD_FAILED (status)) 
        PAD_1Print ("PAD_close () close failed Status = [0x%x]\n", status);

    padHandle->ringIOHandle = NULL;

    if (padHandle->semPtr != NULL) {
        PAD_DeleteSem (padHandle->semPtr);
        padHandle->semPtr = NULL;
    }

#ifdef PAD_DEBUG
    fclose(pOutFile);
#endif

    if(PAD_SUCCEEDED (status))
        status = 0;
    else
        status = 1;

    return (status);
} //PAD_close

// -----------------------------------------------------------------------------

// \brief This function implements the notification callback for the RingIO reader client

static Void _readerNotify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg)
{
    int           status = PAD_SUCCESS;
    PAD_Handle padHandle = (PAD_Handle) param;

	switch (msg) {
	
        case NOTIFY_DATA_END:
			dataend = 1;	
	    break;

    default:
        break;
    }
    // Post the semaphore.

    status = PAD_PostSem (padHandle->semPtr);
    if (PAD_FAILED (status))
        PAD_1Print ("PAD_PostSem () failed. Status = [0x%x]\n", status);
		
} //_readerNotify

// -----------------------------------------------------------------------------

int PAD_ioctl (void *handle, Uint32 command, ...)
{
    int                 status  = PAD_SUCCESS;
    PAD_Handle       padHandle = (PAD_Handle)handle;
    RingIO_Handle ringIOHandle = padHandle->ringIOHandle;
	Uint16  type;
    Uint32  param, bytesFlushed;

    switch (command) {
        case PAD_EOS:
            status = RingIO_sendNotify (ringIOHandle, (RingIO_NotifyMsg) NOTIFY_DATA_END);
            if (PAD_FAILED (status)) {
                PAD_1Print ("PAD_IOctrl () SendNotify failed Status = [0x%x]\n", status);
                status = PAD_FAILURE;
            }
            break;
			
		case PAD_FLUSH:
			status = RingIO_flush (ringIOHandle, TRUE, &type, &param, &bytesFlushed);
            if (PAD_FAILED (status)) {
                PAD_1Print ("PAD_IOctrl () flush failed Status = [0x%x]\n", status);
                status = PAD_FAILURE;
            }
            break;

        default:
            PAD_0Print ("PAD_IOctrl () Unrecognized command\n");
            status = PAD_FAILURE;
            break;
    }

    return status;
} //PAD_ioctl

// -----------------------------------------------------------------------------
/// \brief This function implements the notification callback for the RingIO writer client

static Void _writerNotify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg)
{
    int           status = PAD_SUCCESS;
    PAD_Handle padHandle = (PAD_Handle) param;

    // Post the semaphore.
    status = PAD_PostSem (padHandle->semPtr);
    if (PAD_FAILED (status))
        PAD_1Print ("PAD_PostSem () failed. Status = [0x%x]\n", status);

} //_writerNotify

// -----------------------------------------------------------------------------
