
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/** ============================================================================
 *	@file	pad_int.h
 *
 *	============================================================================
 *	Copyright (c) Texas Instruments Incorporated 2002-2008
 *
 *	Use of this software is controlled by the terms and conditions found in the
 *	license agreement under which this software has been supplied or provided.
 *	============================================================================
 */

/// \file pad.h
/// \brief PAD - Ring IO Access APIs
/// \ingroup PAD

#if !defined (PAD_H)
#define PAD_H

/****************************WARNING*******************************/
/*****The following APIs are not final, they may change in future ******/

/*	----------------------------------- DSP/BIOS Link				  */
#include <dsplink.h>


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */

// -----------------------------------------------------------------------------
/// \brief	PAD_init
///
/// \brief	initialize PAD

NORMAL_API
int
PAD_init (void);

// -----------------------------------------------------------------------------
/// \brief	PAD_open
///
/// \brief	Create and open a RingIO connection
/// \param [in]	pHandle - Handle for opened PAD object.

NORMAL_API
int
PAD_open (OUT void **phandle, IN int reserved1, IN int reserved2);

#define	O_RDONLY	0
#define	O_WRONLY	1

enum PAD_audio_Codec_Type
{
  PCM = 1, //PCM, mono stereo or multichannel
  DSD,     //Direct Stream Digital
  AC3,     //Dolby Digital
  EAC3,    //Dolby Digital Plus
  THD      //Dolby TrueHD
};

typedef struct PAD_MetaData
{
  enum PAD_audio_Codec_Type codec; //Raw-PCM or Encoded bitstream
  Uint32 sampling_rate;          //in Hz
  Uint32 frame_size;             //Number of samples per frame or size of one 
                              //encoded frame for framebased codec
  Uint32 num_channels;           //Number of Channels
  Uint32 sample_width;           //Bits allocated per sample in non-frame based codec
  Uint32 sample_depth;           //Actual bits per sample in a non-frame based codec
} PAD_MetaData;

// -----------------------------------------------------------------------------
/// \brief	PAD_write
///
/// \brief	Write data to RingIO object
/// \param [in]	handle - handle for PAD object.
/// \param [in]	pBuffer - Data buffer to be written.
/// \param [in]	size - Size of the Data buffer to be written.

NORMAL_API
int
PAD_write (IN void *handle, IN Void * pBuffer, IN Uint32 size, IN PAD_MetaData * meta) ;

// -----------------------------------------------------------------------------
/// \brief	PAD_read
///
/// \brief	Read data from RingIO object
/// \param [in]	handle - handle for PAD object.
/// \param [in]	pBuffer - Data buffer to be read.
/// \param [in]	size - Size of the Data buffer to be read.

NORMAL_API
int
PAD_read (IN void *handle, IN Void * pBuffer, IN Uint32 size, int reserved) ;

// -----------------------------------------------------------------------------
/// \brief	PAD_close
///
/// \brief	Close PAD object
/// \param [in]	handle - handle for PAD object.

NORMAL_API
int
PAD_close (IN void *handle);

// -----------------------------------------------------------------------------
/// \brief	PAD_ioctl
///
/// \brief	ioctl implementation of PAD
/// \param [in]	handle - handle for PAD object.

NORMAL_API
int 
PAD_ioctl (IN void *handle, IN Uint32 command, ...);


enum PAD_ioctl_Command
 {
 	PAD_EOS = 0x100,						// Hack - End Of Stream indication
	PAD_FLUSH = 0x101
 };
 
enum PAD_ErrorValue
 {
	PAD_NOTIFYERR = 0x109
 };
	
 
#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (PAD_H) */
