
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PAD Player Example Application
//
//
//

/*	------------------------ System ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

/*	------------------------ Application Header------------------------------*/
#include <pad.h>

// For wave format details see
//      http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
#define 	WAV_HEADERSIZE		44
#define 	WAV_CHUNKID	    	0x46464952	//RIFF
#define 	WAV_CHUNKID_LSB	    0x4952	
#define 	WAV_CHUNKID_MSB	    0x4646	
#define 	BLOCK_PER_CHANNEL 	4096
#define 	DSF_HEADERSIZE 		(28+52+12)
#define 	DSF_CHUNKID    		0x20445344
#define 	PCM_BUFFSIZE		256*8*4		//Frame size, 8 channels max,4 bytes per sample
#define 	PCM_FRAMESIZE		256			//number of samples/Channels

//DD/DDP Bit stream Definitions
//-----------------------------
#define 	DDSYNCWORD			0x0B77
#define 	DDSYNCWORDREV		0x770B
#define 	AC3_BUFFSIZE		1024*6		//Bytes
#define 	DDP_BUFFSIZE		1024*24		//Bytes
#define		DDPI_FRMSIZEBYTES	20			// Number of frame data bytes required to get frame size data 

//TruHD bit stream definitions
//------------------------------
#define 	FORMATSYNC_FBB      	0xf8726fbb
#define 	FORMATSYNC_FBA      	0xf8726fba
#define 	FRAME_HEADER_SIZE		28
#define 	IEC_TRUHD_FRAME_SIZE	61440
#define 	MAT_SYNCWORD			0x079E
#define 	MAT_SYNCWORDREV			0x9E07
#define 	MAT_SYNCWORD_LSB		0x07
#define 	MAT_SYNCWORD_MSB		0x9E

#define		ERR_NO_ERROR		0

static unsigned char isWav;
static unsigned char wavBitsPerSample;

static unsigned char isDsf;
static unsigned char dsfBitsPerSample;

static int stopSignal;
static void _signalHandler (int sigId);

#define	O_RDONLY	0
#define	O_WRONLY	1

const short gbl_frmsizetab[3][38] =
{
	/* 48kHz */
	{	64, 64, 80, 80, 96, 96, 112, 112,
		128, 128, 160, 160, 192, 192, 224, 224,
		256, 256, 320, 320, 384, 384, 448, 448,
		512, 512, 640, 640, 768, 768, 896, 896,
		1024, 1024, 1152, 1152, 1280, 1280 },
	/* 44.1kHz */
	{	69, 70, 87, 88, 104, 105, 121, 122,
		139, 140, 174, 175, 208, 209, 243, 244,
		278, 279, 348, 349, 417, 418, 487, 488,
		557, 558, 696, 697, 835, 836, 975, 976,
		1114, 1115, 1253, 1254, 1393, 1394 },
	/* 32kHz */
	{	96, 96, 120, 120, 144, 144, 168, 168,
		192, 192, 240, 240, 288, 288, 336, 336,
		384, 384, 480, 480, 576, 576, 672, 672,
		768, 768, 960, 960, 1152, 1152, 1344, 1344,
		1536, 1536, 1728, 1728, 1920, 1920 } };
		

// -----------------------------------------------------------------------------
/// \brief This function prints the usage for the application

void usage (char *programName)
{
    fprintf(stderr,"Usage: %s [streamtype(pcm/ac3/ec3/thd)] [inputFileName] \n",programName);
} //usage

// -----------------------------------------------------------------------------
/// \brief This is the main function for the sample app

int main (int argc,char *argv[])
{
    FILE				*pInFile = NULL;
    FILE				*pFile = NULL;
    unsigned short     	*pHeader = NULL;
    unsigned char      	*pBuff = NULL;
    unsigned char      	*pBuffOut = NULL;
    PAD_MetaData       	*pMeta = NULL;
    void                *handle = NULL;
    int                 status = 0;
    unsigned long       size;
    unsigned long      	bytesLeft;
    int                 i=0;
    int                 codec = 0;
	unsigned long 		frameLengthInByte;
	int 				zerostoPadInByte; 
	int					bytesToWrite;
    unsigned int 		numChan = 0, numBit = 0, numData = 0, numDataH=0;
	int 				BUFF_SIZE;
	static int 			frame_counter = 0;
	
	/*
	frameLengthInByte - This variable indicates number of bytes to read from the bit stream. This is depends on:
			PCM_FRAMESIZE
			Number of channels in the bit stream (Read from the header)
			Bits/Sample(Read from the header)
			
	bytesToWrite - This variable indicates number of bytes to write to the buffer. It depends on:
			frameLengthInByte - Bytes read from the input stream
			Width of each sample. For example, number of bits/sample = 16. and the sample width specified as 4 Bytes.
									Then Each sample is padded with 2 additional bytes with zero. 
									So bytesToWrite = frameLengthInByte * samplewidth
	*/
 		
#ifndef PAD_DEBUG	
	if (argc < 3) {
       	usage (argv[0]);
        status = 1;
        goto BAIL;
   }

	pInFile = fopen (argv[2], "rb");
#else	
	pInFile = fopen ("D:/Workspace/PAD_App/bd.spd", "rb");
#endif

    if(pInFile == NULL)     {
        fprintf (stderr, "Couldn't open the input file %s\n", argv[2]);
        status = 1;
        goto BAIL;
    }
    
    pHeader = malloc(WAV_HEADERSIZE);
	if (pHeader == NULL) {
		fprintf (stderr, "Couldn't allocate memory of size %d bytes\n", WAV_HEADERSIZE);
		status = 1;
		goto BAIL;
	}
	
	pMeta = malloc (sizeof(PAD_MetaData));
    if (pMeta == NULL) {
        fprintf (stderr, "Couldn't allocate memory of size %d bytes\n", sizeof(PAD_MetaData));
        status = 1;
        goto BAIL;
    }

    // determine file size
    fseek (pInFile, 0, SEEK_END);
    bytesLeft = ftell (pInFile);
    fseek (pInFile, 0, SEEK_SET);
	
	/* default frame information*/
	pMeta->sampling_rate = 48000;
	pMeta->num_channels = 2; 
	pMeta->sample_depth = 2; 		
	pMeta->sample_width = 2; 									
	
	/*To check the sync words in encoded streams minimum number of Bytes needed
	So WAV_HEADERSIZE number of bytes read*/
	
	size = fread (pHeader, 1,WAV_HEADERSIZE,pInFile);
	
	if((!strcmp(argv[1],"pcm"))) 
    {
		isWav = 0;
				
		if(pHeader[0] == WAV_CHUNKID_LSB && pHeader[1] == WAV_CHUNKID_MSB)
		{
			isWav = 1;
			pMeta->num_channels = (pHeader[11] & 0x00FF); 
			pMeta->sampling_rate = pHeader[13]<<16 | pHeader[12];
			pMeta->sample_depth = ((pHeader[17] & 0x00FF))/8; 	// Number of Bytes/sample
			fprintf(stderr, "PCM Wave format\n");
		}
		pMeta->sample_width = 4; 										// To pack 4 bytes per channel
		pMeta->frame_size = pMeta->num_channels * PCM_FRAMESIZE;		// number of samples/frame 
		frameLengthInByte = pMeta->frame_size * pMeta->sample_depth;	// Total Number of bytes in a frame to be read
		bytesToWrite = pMeta->frame_size * pMeta->sample_width;			// Number of bytes to write
				
		BUFF_SIZE = PCM_BUFFSIZE;	
		codec = 1;
		fprintf (stderr, "PCM File type\n");
	}

 	else if((!strcmp(argv[1],"ac3"))) 
    {			
		if (((pHeader[0] & 0xFFFF) == DDSYNCWORD) || ((pHeader[0] & 0xFFFF) == DDSYNCWORDREV)){
			codec = 3;
			BUFF_SIZE = AC3_BUFFSIZE;	
			bytesToWrite = AC3_BUFFSIZE;
			fprintf (stderr, "AC3 File type\n");			
		}
		else{
			fprintf (stderr, "AC3 SYNC Not Found\n");
			goto BAIL;
		}
    }
	
	else if((!strcmp(argv[1],"ec3"))) 
    {			
		if (((pHeader[0] & 0xFFFF) == DDSYNCWORD) || ((pHeader[0] & 0xFFFF) == DDSYNCWORDREV)){
			codec = 4;
			BUFF_SIZE = DDP_BUFFSIZE;
			bytesToWrite = DDP_BUFFSIZE;			
			fprintf (stderr, "DDP File type\n");			
		}
		else{
			fprintf (stderr, "DDP SYNC Not Found\n");
			goto BAIL;
		}
    }   	
	
	else if((!strcmp(argv[1],"thd"))) 
    {			
		if ((pHeader[4] == MAT_SYNCWORD) | (pHeader[4] == MAT_SYNCWORDREV)){
			codec = 5;
			BUFF_SIZE = IEC_TRUHD_FRAME_SIZE;
			bytesToWrite = IEC_TRUHD_FRAME_SIZE;			
			fprintf (stderr, "TrueHD File type\n");			
		}
		else{
			fprintf (stderr, "TrueHD SYNC Not Found\n");
			goto BAIL;
		}
    }   	
    
    else 
	{
        fprintf (stderr, "Invalid file type %s\n", argv[1]);
        status = 1;
        goto BAIL;
    }
	
	if (codec != 5)// Output buffer not needed for TruHD (in place computation)
	{	
		pBuff = malloc (BUFF_SIZE);
		if (pBuff == NULL) {
			fprintf (stderr, "Couldn't allocate memory of size %d bytes\n", BUFF_SIZE);
			status = 1;
			goto BAIL;
		}
	}   
	pBuffOut = malloc (BUFF_SIZE);
    if (pBuffOut == NULL) {
        fprintf (stderr, "Couldn't allocate memory of size %d bytes\n", BUFF_SIZE);
        status = 1;
        goto BAIL;
    }
	
	fseek (pInFile, 0, SEEK_SET);					// To reset the pointer to the beginning of the file
	
	if(codec == 1)
	{
		if(isWav == 1)
			fseek (pInFile, WAV_HEADERSIZE, SEEK_SET);			//Skip the waveheader
			
		pMeta->codec = PCM;
	}
	else if(codec == 3)
		pMeta->codec = AC3;
	else if (codec == 4)
		pMeta->codec = EAC3;
	else 
		pMeta->codec = THD;
				
	

    // .........................................................................

    status = PAD_init ();
    if (status) {
        fprintf(stderr, "PAD init error\n");
        goto BAIL;
    }

    // .........................................................................
    // Setup to trap exit signals so that proper LINK cleanup is done
    // Do this after PAD_init since the underlying LINK functions called therein
    // setup signal handling and we are overwriting this here with our own
    // application specific handler.

    stopSignal = 0;
    
#ifndef PAD_DEBUG
    if (SIG_ERR == signal (SIGABRT, _signalHandler)) {
        fprintf (stderr,  "Unable to assign abnormal termination handler\n" );
    }
    if (SIG_ERR == signal (SIGINT, _signalHandler)) {
        fprintf (stderr, "Unable to assign ^C handler\n");
    }
    if (SIG_ERR == signal (SIGTERM, _signalHandler)) {
        fprintf (stderr, "Unable to assign termination request handler\n" );
    }
#endif

    // .........................................................................

    status = PAD_open (&handle, O_WRONLY, 0);
    if (status) {
        fprintf (stderr, "PAD open error = %d\n", status);
        goto BAIL;
    }
	//To flushout the RingIo contents. RingIO has to be flushed whenever there is a need to flushout the data. For example,
	//When stream switches 
	PAD_ioctl (handle, PAD_FLUSH);

    // .........................................................................

    fprintf (stdout, "Playing %s. Hit Ctrl-C to exit\n", argv[1]);

    while (!stopSignal) {

        unsigned char *pChar;
		unsigned short *pShort;
        unsigned int *pLong;	
		
		if(codec ==1)
		{
			pShort = (unsigned short *) pBuff;
			pLong = (unsigned int *) pBuffOut;
			pChar = (unsigned char *) pBuff;
			
			size = fread (pBuff, 1,frameLengthInByte, pInFile);
			bytesLeft -= frameLengthInByte;		
					
			// special case: reformating of 16bit wav files into 32bits
			// Reformat Rule: 12 34 56 78 --> 12 34 00 00   56 78 00 00
			if(pMeta->sample_depth == 2)
			{                        
				for (i=0; i < size/2; i++) //writing 2 bytes at a time, so loop count is size/2
				{
					pLong[i] = (pShort[i] <<16) & 0xFFFF0000;     
				}	
			}

			// special case: reformating of 24bit wav files into 32bits
			// Reformat Rule: 12 34 56 78 9A BC --> 12 34 56 00   78 9A BC 00
			else if (pMeta->sample_depth == 3)
			{                
				for (i=0;i < size/3; i++)
				{
					pLong[i] = ((pChar[3*i+2] << 24) | (pChar[3*i+1] << 16) | (pChar[3*i+0] <<  8)) & 0xFFFFFF00;             				
				}			
			}
		
			else 
			{
				for (i=0;i < size/4; i++)
				{
					pLong[i] = ((pChar[4*i+3] << 24) | (pChar[4*i+2] << 16) | (pChar[4*i+1] << 8) | (pChar[4*i+0]));
				}				
			}
		}
		
		else if (codec == 3)
    	{	
    		short temp;    		
			short stream_ID = 0;
			char bytereverse = 0;
							
			size = fread (pHeader, 1,DDPI_FRMSIZEBYTES, pInFile);
			
			if (size < DDPI_FRMSIZEBYTES){
				status = 1;
				goto BAIL;
			}
			
			if((pHeader[0] == DDSYNCWORDREV))	
			{
				//printf ("sync found in reverse = \n");
				bytereverse = 1;
				byterev(size, pHeader);
			}
			else if((pHeader[0] == DDSYNCWORD))	
			{
				bytereverse = 0;				
			}
			else
			{
				printf ("sync Not found \n");
			}
			temp = pHeader[2];
			stream_ID = (temp & 0x00F8)>>3;

			if(stream_ID <=8)
			{			
				pMeta->sampling_rate = (temp & 0xC000)>>14;
				pMeta->frame_size = (temp & 0x3F00)>>8;
		 		pMeta->frame_size = gbl_frmsizetab[pMeta->sampling_rate][pMeta->frame_size];
				if(pMeta->sampling_rate == 0)
				pMeta->sampling_rate = 48000;
				else if(pMeta->sampling_rate == 1)
				pMeta->sampling_rate = 44100;
				else if(pMeta->sampling_rate == 2)
				pMeta->sampling_rate = 32000;												
			}
			
			else
			{
				fprintf (stderr, "Unsupported Stream ID %d\n", stream_ID);
				status = 1;
				goto BAIL;
			}					
							
			frameLengthInByte = pMeta->frame_size * pMeta->sample_depth;
			fseek (pInFile, -DDPI_FRMSIZEBYTES, SEEK_CUR);
			size = fread (pBuffOut, 1,frameLengthInByte, pInFile);
			bytesLeft -= frameLengthInByte;	
			
			if (bytereverse ==1)
				status = byterev(size, pBuffOut);					
					
			//To pad with zeros						
			for(i=size; i<bytesToWrite; i++)
			{
				pBuffOut[i] = 0;
			}						
		}
		
		else if (codec == 4)
    	{	
    		short temp;    		
			short Bsid = 0, stream_type = 0,numblkscod =0;
			char bytereverse = 0;
					
			size = fread (pHeader, 1,DDPI_FRMSIZEBYTES, pInFile);
			
			if (size < DDPI_FRMSIZEBYTES){
				status = 1;
				goto BAIL;
			}
			
			if((pHeader[0] == DDSYNCWORDREV))	
			{
				//printf ("sync found in reverse = \n");
				bytereverse = 1;
				byterev(size, pHeader);
			}
			else if((pHeader[0] == DDSYNCWORD))	
			{
				bytereverse = 0;				
			}
			else
			{
				printf ("sync Not found\n");
			}
			temp = pHeader[2];
			Bsid = (temp & 0x00F8)>>3;
			
			if(Bsid <=8)
			{			
				pMeta->sampling_rate = (temp & 0xC000)>>14;
				pMeta->frame_size = (temp & 0x3F00)>>8;
		 		pMeta->frame_size = gbl_frmsizetab[pMeta->sampling_rate][pMeta->frame_size];															
			}
			else if(Bsid >=11 && Bsid <=16)
			{
				numblkscod = (temp & 0x3000)>>12;
				temp = (short) pHeader[1];
				pMeta->frame_size = (temp & 0x07FF)+1;		
				//printf ("blocks  %d \n",numblkscod);
				//printf ("First framelength  %d \n",pMeta->frame_size);
				frameLengthInByte = pMeta->frame_size*2;
				stream_type = (temp & 0xC000)>>14;	
				//printf ("stream type  %d \n",stream_type);
				pMeta->frame_size = 0;
				if(stream_type ==0)//There may be a dependent stream
				{
					fseek (pInFile, -DDPI_FRMSIZEBYTES, SEEK_CUR);
					fseek (pInFile, frameLengthInByte, SEEK_CUR);
					size = fread (pHeader, 1,DDPI_FRMSIZEBYTES, pInFile);
					if(bytereverse ==1)
						byterev(size, pHeader);
					if((pHeader[0] == DDSYNCWORDREV) || (pHeader[0] == DDSYNCWORD))
					{
						temp = pHeader[2];
						Bsid = (temp & 0x00F8)>>3;
						if(Bsid >=11 && Bsid <=16)
						{			
							temp = (short) pHeader[1];
							stream_type = (temp & 0xC000)>>14;	
							//printf ("stream type  %d \n",stream_type);
							if(stream_type ==1)
							{
								temp = (short) pHeader[1];
								pMeta->frame_size = (temp & 0x07FF)+1;	
								//printf ("sec framelength  %d \n",pMeta->frame_size);
							}
							/*else
							{
								printf ("No Dep.stream\n");
								//pMeta->frame_size*=2;
							}*/
							
						}
						//else
							//printf ("Not DDP stream\n");
					}
					//else
						//printf ("Dep.sync Not found \n");
					
					fseek (pInFile, -(frameLengthInByte), SEEK_CUR);
					
				}
				pMeta->frame_size = pMeta->frame_size+frameLengthInByte/2;
			}
			else
			{
				fprintf (stderr, "Unsupported Stream ID %d\n", Bsid);
				status = 1;
				goto BAIL;
			}					
				
			frameLengthInByte = pMeta->frame_size * pMeta->sample_depth;
			if(numblkscod == 0)
				frameLengthInByte *= 6;
			else if(numblkscod == 1)
				frameLengthInByte *= 3;
			else if(numblkscod == 2)
				frameLengthInByte *= 2;
			
			fseek (pInFile, -DDPI_FRMSIZEBYTES, SEEK_CUR);
			size = fread (pBuffOut, 1,frameLengthInByte, pInFile);
			bytesLeft -= frameLengthInByte;					
				
			if (bytereverse ==1)
				status = byterev(size, pBuffOut);					
					
			//To pad with zeros						
			for(i=size; i<bytesToWrite; i++)
			{
				pBuffOut[i] = 0;
			}						
        }//AC3 & EAC3	
		
		else if (codec == 5) //TruHD
    	{	    
			unsigned short temp;
			char bytereverse = 0;
			
		 	pMeta->frame_size = IEC_TRUHD_FRAME_SIZE;					
			pMeta->codec = THD;								
			pMeta->num_channels = 8; // optional info				
			frameLengthInByte = pMeta->frame_size;
			bytesToWrite = frameLengthInByte;			
			size = fread (pBuffOut, 1,frameLengthInByte, pInFile);
			bytesLeft -= frameLengthInByte;	
			
			temp = (pBuffOut[9]<<8 | pBuffOut[8]);
			
			if(temp == MAT_SYNCWORDREV)
			{
				//printf ("sync found in reverse =%d\n",temp);
				bytereverse = 1;
				byterev(size, pBuffOut);
			}
			else if(temp == MAT_SYNCWORD)	
			{
				//printf ("sync found = \n");
				bytereverse = 0;				
			}
			else
			{
				printf ("sync Not found \n");
			}				
			//To pad with zeros						
			for(i=size; i<bytesToWrite; i++)
			{
				pBuffOut[i] = 0;
			} 							
        }//TruHD
				
		frame_counter =frame_counter+1;
		pMeta->frame_size = bytesToWrite/pMeta->sample_width;								

		status = PAD_write (handle, pBuffOut,bytesToWrite, pMeta);
		if (status)
		{
			fprintf (stderr, "PAD write error %d\n", status);
			break;
		}	
		
		if (size < frameLengthInByte || bytesLeft <= 0 )
			stopSignal = 1;
				
		#ifdef PAD_DEBUG		
			printf ("Bytes remaining = %8u\n", bytesLeft);
		#endif
    }

    // .........................................................................

BAIL:
    if (handle) {
		PAD_ioctl (handle, PAD_EOS);
		PAD_close (handle);
		printf("writer closed\n");
		printf ("Number of frames = %d\n", frame_counter);
		printf ("framesize = %d\n", frameLengthInByte);
    }

    if (pMeta)
        free (pMeta);

    if (pBuffOut)
        free (pBuffOut);

    if (pHeader)
        free (pHeader);

    if (pBuff)
        free (pBuff);

    if (pInFile)
        fclose (pInFile);

   	return (ERR_NO_ERROR);
} //main

// -----------------------------------------------------------------------------

static void _signalHandler (int sigId)
{
    // signal main thread to clean up gracefully
    stopSignal = 1;

} //signalHandler

// -----------------------------------------------------------------------------
int byterev(const unsigned long	length,	short *p_buffer)			
{
	unsigned long				count;
	unsigned short				word;
	char						byte1;
	char						byte2;

	count = length;
	
	while (count--)
	{
		word = (short)*p_buffer;
		byte1 = word & 0x00ff;
		byte2 = (word >> 8) & 0x00ff;
		*p_buffer++ = (short)((byte1 << 8) | byte2);
	}

	return (ERR_NO_ERROR);
}
