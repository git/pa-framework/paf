
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/** ============================================================================
 *	@file	pad_os.c
 *
 *	@desc	OS specific implementation of functions 
 *
 *	============================================================================
 *	Copyright (c) Texas Instruments Incorporated 2002-2008
 *
 *	Use of this software is controlled by the terms and conditions found in the
 *	license agreement under which this software has been supplied or provided.
 *	============================================================================
 */

 /// \file pad_os.c
/// \brief OS specific implementation of functions
/// \ingroup PAD

/*	----------------------------------- OS Specific Headers 		  */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

/*	----------------------------------- DSP/BIOS Link				  */
#include <dsplink.h>
#include <proc.h>

/*	------------------------DSP/BIOS LINK API -------------------------------*/
#include <ringio.h>

/*	----------------------------------- Application Header			  */
#include <pad_os.h>
#include <_pad.h>

#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */


// -----------------------------------------------------------------------------
/// \brief	PAD_SemObject
/// \brief	This object is used by various SEM API's.

typedef struct PAD_SemObject_tag {
	sem_t  sem ;
} PAD_SemObject ;

// -----------------------------------------------------------------------------
/// \brief	PAD_0Print
/// \brief Print a message without any arguments.


NORMAL_API
Void
PAD_0Print (Char8 * str)
{
	printf (str) ;
} //PAD_0Print

// -----------------------------------------------------------------------------
/// \brief	PAD_1Print
/// \brief	Print a message with one argument.


NORMAL_API
Void
PAD_1Print (Char8 * str, Uint32 arg)
{
	printf (str, arg) ;
} //PAD_1Print

// -----------------------------------------------------------------------------
/// \brief	PAD_Sleep
/// \brief  Sleeps for the specified number of microseconds.

NORMAL_API
Void
PAD_Sleep (Uint32 uSec)
{
	usleep (uSec) ;
} //PAD_Sleep

// -----------------------------------------------------------------------------
/// \brief	PAD_CreateSem
/// \brief	This function creates a semaphore.
/// \param [in] semPtr Pointer to the semaphore object
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_CreateSem (OUT Pvoid * semPtr)
{
	DSP_STATUS			status = DSP_SOK ;
	PAD_SemObject * semObj ;
	int 				osStatus ;

	semObj = malloc (sizeof (PAD_SemObject)) ;
	if (semObj != NULL) {
		osStatus = sem_init (&(semObj->sem), 0, 0) ;
		if (osStatus < 0) {
			status = DSP_EFAIL ;
		}
		else {
			*semPtr = (Pvoid) semObj ;
		}
	}
	else {
		*semPtr = NULL ;
		status = DSP_EFAIL ;
	}

	return (status) ;
} //PAD_CreateSem

// -----------------------------------------------------------------------------
/// \brief	PAD_DeleteSem
/// \brief	This function deletes a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_DeleteSem (IN Pvoid semHandle)
{
	DSP_STATUS			status = DSP_SOK ;
	PAD_SemObject * semObj = semHandle ;
	int 				osStatus ;

	osStatus = sem_destroy (&(semObj->sem)) ;
	if (osStatus < 0) {
		status = DSP_EFAIL ;
	}

	free (semObj) ;

	return (status) ;
} //PAD_DeleteSem

// -----------------------------------------------------------------------------
/// \brief	PAD_WaitSem
/// \brief	This function waits on a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_WaitSem (IN Pvoid semHandle)
{
	DSP_STATUS			status = DSP_SOK ;
	PAD_SemObject * semObj = semHandle ;
	int 				osStatus ;

	osStatus = sem_wait (&(semObj->sem)) ;
	if (osStatus < 0) {
		status = DSP_EFAIL ;
	}

	return (status) ;
} //PAD_WaitSem

// -----------------------------------------------------------------------------
/// \brief	PAD_PostSem
/// \brief	This function posts a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_PostSem (IN Pvoid semHandle)
{
	DSP_STATUS			status = DSP_SOK ;
	PAD_SemObject * semObj = semHandle ;
	int 				osStatus ;

	osStatus = sem_post (&(semObj->sem)) ;
	if (osStatus < 0) {
		status = DSP_EFAIL ;
	}

	return (status) ;
} //PAD_PostSem

#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */
// -----------------------------------------------------------------------------
