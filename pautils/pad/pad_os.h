
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/** ============================================================================
 *	@file	pad_os.h
 *
 *	@desc	OS specific definitions 
 *
 *	============================================================================
 *	Copyright (c) Texas Instruments Incorporated 2002-2008
 *
 *	Use of this software is controlled by the terms and conditions found in the
 *	license agreement under which this software has been supplied or provided.
 *	============================================================================
 */

/// \file pad_os.h
/// \brief OS specific implementation of functions
/// \ingroup PAD

#if !defined (PAD_OS_H)
#define PAD_OS_H

/*	----------------------------------- OS Specific Headers 		  */
#include <pthread.h>
#include <sys/types.h>
/*	----------------------------------- DSP/BIOS Link				  */
#include <dsplink.h>


#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */

// -----------------------------------------------------------------------------
/// \brief	PAD_CreateSem
/// \brief	This function creates a semaphore.
/// \param [in] semPtr Pointer to the semaphore object
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_CreateSem (OUT Pvoid * semPtr) ;

// -----------------------------------------------------------------------------
/// \brief	PAD_DeleteSem
/// \brief	This function deletes a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_DeleteSem (IN Pvoid semHandle) ;

// -----------------------------------------------------------------------------
/// \brief	PAD_WaitSem
/// \brief	This function waits on a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_WaitSem (IN Pvoid semHandle) ;

// -----------------------------------------------------------------------------
/// \brief	PAD_PostSem
/// \brief	This function posts a semaphore.
/// \param [in]	semHandle - Pointer to the semaphore object to be deleted.
/// \return DSP_SOK if successful and DSP_EFAIL otherwise

NORMAL_API
DSP_STATUS
PAD_PostSem (IN Pvoid semHandle) ;

#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (PAD_OS_H) */
