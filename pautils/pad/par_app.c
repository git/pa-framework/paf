
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

/*	------------------------ System ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

/*	------------------------ Application Header------------------------------*/
#include <pad.h>

static int stopSignal;
static void _signalHandler (int sigId);

#define FRAME_SIZE	256*2*3//blocksize*No.of channels*bits/sample
#define	O_RDONLY	0
#define	O_WRONLY	1
// -----------------------------------------------------------------------------
/// \brief This function prints the usage for the application

void usage (char *programName)
{
    fprintf(stderr,"Usage: %s outputFileName\n",programName);
} //usage

// -----------------------------------------------------------------------------
/// \brief This is the main function for the sample app

int main (int argc,char *argv[])
{
    FILE		*pOutFile = NULL;
    unsigned char      	*pBuff = NULL;
    void                *handle = NULL;
    int                 status = 0;
    unsigned int	size;

    // .........................................................................

    if (argc < 2) {
        /* If arguments are not proper call the usage routine and exit */
        usage (argv[0]);
        status = 1;
        goto BAIL;
    }

    pOutFile = fopen (argv[1], "w");
    if(pOutFile == NULL)     {
        fprintf (stderr, "Couldn't open the output file %s\n", argv[1]);
        status = 1;
        goto BAIL;
    }

    pBuff = malloc (FRAME_SIZE);
    if (pBuff == NULL) {
        fprintf (stderr, "Couldn't allocate memory of size %d bytes\n", FRAME_SIZE);
        status = 1;
        goto BAIL;
    }
    
    // .........................................................................

    status = PAD_init ();
    if (status) {
        fprintf(stderr, "PAD init error\n");
        goto BAIL;
    }

    // .........................................................................
    // Setup to trap exit signals so that proper LINK cleanup is done
    // Do this after PAD_init since the underlying LINK functions called therein
    // setup signal handling and we are overwriting this here with our own
    // application specific handler.

    stopSignal = 0;

    if (SIG_ERR == signal (SIGABRT, _signalHandler)) {
        fprintf (stderr,  "Unable to assign abnormal termination handler\n" );
    }
    if (SIG_ERR == signal (SIGINT, _signalHandler)) {
        fprintf (stderr, "Unable to assign ^C handler\n");
    }
    if (SIG_ERR == signal (SIGTERM, _signalHandler)) {
        fprintf (stderr, "Unable to assign termination request handler\n" );
    }

    // .........................................................................

    status = PAD_open (&handle, O_RDONLY, 0);
    if (status) {
        fprintf (stderr, "PAR open error = %d\n", status);
        goto BAIL;
    }

    // .........................................................................

    fprintf(stdout,"Reader Ready\n");

    while (!stopSignal) 
    {
	    status = PAD_read (handle, pBuff, FRAME_SIZE, NULL);
        if (status) {
            fprintf (stderr, "PAR read error %d\n", status);
            break;
        }       

		size = fwrite(pBuff,1, FRAME_SIZE, pOutFile);
        if (size != FRAME_SIZE) {
			fprintf(stderr, "Could not write to file\n");
			break;
        }
    }

    // .........................................................................

BAIL:

if (handle) {
        PAD_close (handle);
		printf("Reader application closed\n ");
    }
	
	if (pBuff)
        free (pBuff);

    if (pOutFile)
        fclose (pOutFile);


    return 0;
} //main

// -----------------------------------------------------------------------------

static void _signalHandler (int sigId)
{
    // signal main thread to clean up gracefully
    stopSignal = 1;

} //signalHandler

// -----------------------------------------------------------------------------
