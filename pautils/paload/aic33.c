
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// AIC33 support routines
//
//
//


#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h> //for open, read, etc.

#include "aic33.h"

// .............................................................................

unsigned char aic33Config[][2] =
{
   {0x00, 0x00}, // 0-0  Page Select                     <- [Page=0]
//    {0x01, 0x80}, // 0-1  Software Reset                  <- [assert SW reset]
    {0x02, 0x00}, // 0-2  Codec Sample Rate Select        <- [ADC=FS][DAC=FS]
    {0x03, 0x92}, // 0-3  PLL Reg A                       <- [PLL=ON][P=2][Q=2]
    {0x04, 0x20}, // 0-4  PLL Reg B                       <- [J=8]
    {0x05, 0x6E}, // 0-5  PLL Reg C                       <- [D=7075]
    {0x06, 0x23}, // 0-6  PLL Reg D                       <- [D=7075]
    {0x07, 0x0A}, // 0-7  Codec Datapath Setup            <- [FS=48 kHz][LeftDACPlays=left][RightDACPlays=right]
    {0x08, 0xC0}, // 0-8  Audio Serial Data Reg A         <- [BCLK=Master][WCLK=Master][3DEffects=OFF][DigitalMIC=OFF]
    {0x09, 0x00}, // 0-9  Audio Serial Data Reg B         <- [Mode=I2S][Lenght=16-bit][TransferMode=Continous]
    {0x0A, 0x00}, // 0-10 Audio Serial Data Reg C         <- [DataOffset=0]
    {0x0F, 0x00}, // 0-15 Left  ADC PGA Gain Ctrl Reg     <- [Mute=OFF]
    {0x10, 0x00}, // 0-16 Right ADC PGA Gain Ctrl Reg     <- [Mute=OFF]
    {0x13, 0x04}, // 0-19 LINE1L  to Left  ADC Ctrl Reg   <- [LINE1L=SingleEnd][LINE1L=0dBGain][SoftStep=OncePerFS]
    {0x14, 0x78}, // 0-20 LINE2L  to Left  ADC Ctrl Reg   <- [LINE2L=SingleEnd][LeftADCPower=ON][SoftStep=OncePerFS]
    {0x15, 0x78}, // 0-21 LINE1R  to Left  ADC Ctrl Reg   <- [LINE1R=SingleEnd][LINE1R=NoConnect]
    {0x16, 0x04}, // 0-22 LINE1R  to Right ADC Ctrl Reg   <- [LINE1R=SingleEnd][LINE1R=0dBGain][SoftStep=OncePerFS]
    {0x17, 0x78}, // 0-23 LINE2R  to Right ADC Ctrl Reg   <- [LINE2R=SingleEnd][LINE2R=NoConnect]
    {0x18, 0x78}, // 0-24 LINE1L  to Right ADC Ctrl Reg   <- [LINE1L=SingleEnd][LINE1L=NoConnect]
    {0x25, 0xE0}, // 0-37 DAC Power & Output Dvr Ctrl Reg <- [LeftDACPower=ON][RightDACPower=ON][HPLCOM=SingleEnd]
    {0x2B, 0x00}, // 0-43 Left  DAC Digital Vol Reg       <- [LeftDACMute=OFF]
    {0x2C, 0x00}, // 0-44 Right DAC Digital Vol Reg       <- [RightDACMute=OFF]
    {0x2F, 0x80}, // 0-47 DAC_L1 to HPLOUT Vol Reg        <- [Routed]
    {0x33, 0x09}, // 0-51           HPLOUT Output Reg     <- [Mute=OFF][Power=ON]
    {0x40, 0x80}, // 0-64 DAC_R1 to HPROUT Vol Reg        <- [Routed]
    {0x41, 0x09}, // 0-65           HPROUT Output Reg     <- [Mute=OFF][Power=ON]
    {0x52, 0x80}, // 0-82 DAC_L1 to LEFT_LOP/M Vol Reg    <- [Routed]
    {0x56, 0x09}, // 0-86           LEFT_LOP/M Output Reg <- [Mute=OFF][Power=ON]
    {0x5C, 0x80}, // 0-92 DAC_R1 to RIGHT_LOP/M Vol Reg   <- [Routed]
    {0x5D, 0x09}, // 0-93           RIGHT_LOP/M Output Reg<- [Mute=OFF][Power=ON]
};

// -----------------------------------------------------------------------------

void AIC33_rset (unsigned char regnum, unsigned char regval, int dev)
{
    unsigned char cmd[2];
    int retVal;


    cmd[0] = regnum;
    cmd[1] = regval;                // 8-bit Register Data
    retVal = write (dev, cmd, 2);
    if (retVal != 2)
        printf ("Rset failed %d\n", retVal);

    return;
}

// -----------------------------------------------------------------------------

void AIC33_rget (unsigned short regnum, unsigned short *regval, int dev)
{
    int retVal;
    unsigned char data[2];
    i2c_msg msg[2] =
        {
            {AIC33_I2C_ADDR, 0,        1, &data[0]},
            {AIC33_I2C_ADDR, I2C_M_RD, 1, &data[1]}
        };
    i2c_rdwr_ioctl_data  ioctl_data = {msg, 2};


    data[0] = regnum;
    if ((retVal = ioctl (dev, I2C_RDWR, &ioctl_data)) < 0)
        printf ("Error I2C_RDWR ioctl: %d\n", retVal);
    else
        *regval = data[1];

    return;
}

// -----------------------------------------------------------------------------

void AIC33_config (int dev)
{
    unsigned short i;

    for (i=0; i < sizeof (aic33Config)/(2*sizeof(char)); i++)
            AIC33_rset (aic33Config[i][0], aic33Config[i][1], dev);
    return;
}

// -----------------------------------------------------------------------------

void AIC33_open (void)
{
    int dev;


    if ((dev = open ("/dev/i2c/0", O_RDWR)) < 0)
        printf ("Unable to open I2c-0 device\n");
    else {
        if (ioctl (dev, I2C_SLAVE, AIC33_I2C_ADDR) < 0)
            printf ("Unable to set I2c slave address\n");
        else
            AIC33_config (dev);

        close (dev);
    }

    return;
}

// -----------------------------------------------------------------------------
