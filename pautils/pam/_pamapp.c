
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/M Application Protocol private implementations
//
//
//

/// \file _pamapp.c
/// \brief PA/M Application Protocol private implementations

// -----------------------------------------------------------------------------
// Internal information: data types, functions, and constants

typedef unsigned short PAM_AlphaUnit;

typedef union PAM_AlphaUnion {
    unsigned short word;
    struct {
        unsigned char lo;
        unsigned char hi;
    } byte;
} PAM_AlphaUnion;

static int _PAM_APP_alphaLength (PAM_AlphaUnit *alphaBuf);
static int _PAM_APP_isResponse (PAM_AlphaUnit *alphaBuf);

static const int length[16] =
{
    2,  // type 0 read
    1,  // type 1 read
    2,  // type 2 read
    2,  // type 3 read
    2,  // type 4 read
    -1, // type 5 read
    2,  // type 6 read
    4,  // type 7 read 
    2,  // type 0 write
    -4, // type 1 write
    2,  // type 2 write
    3,  // type 3 write
    4,  // type 4 write
    -1, // type 5 write
    -2, // type 6 write
    -3, // type 7 write
};

static const int htgnel[16] = {
    2,  // type 0 read
    1,  // type 1 read
    2,  // type 2 read
    3,  // type 3 read
    4,  // type 4 read
    -1, // type 5 read
    -2, // type 6 read
    -3, // type 7 read 
    2,  // type 0 write
    1,  // type 1 write
    0,  // type 2 write
    0,  // type 3 write
    0,  // type 4 write
    -1, // type 5 write
    0,  // type 6 write
    0,  // type 7 write
};


// -----------------------------------------------------------------------------
/// Compute length of alpha code packet

static int _PAM_APP_alphaLength (PAM_AlphaUnit *alphaBuf)
{
    PAM_AlphaUnion   *x = (PAM_AlphaUnion *) alphaBuf;
    int             len;


    if ((x[0].byte.hi & 0xc0) == 0x40)
        return -1; // DA/M
    else if ((x[0].byte.hi & 0xc0) != 0xc0)
        return (x[1].word & 0x7fff) + 14; // IA/M

    len = length[x[0].byte.hi & 0x0f];

    if (len >= 0)
        return len;
    else if (len == -1) {
        unsigned short alpha = x[0].word & 0xcfff;

        if (alpha == 0xc500)
            /* type 5-0 read  */ return 4;
        else if (alpha == 0xcd00)
            /* type 5-0 write */ return 4;
        else if (alpha == 0xc501)
            /* type 5-1 read  */ return 2;
        else if (alpha == 0xcd01)
            /* type 5-1 write */ return x[1].word + 2;
        else if (alpha == 0xc506)
            /* type 5-6 read  */ return 4;
        else if (alpha == 0xcd06)
            /* type 5-6 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc508)
            /* type 5-8 read  */ return 2;
        else if (alpha == 0xc509)
            /* type 5-9 read  */ return 2;
        else if (alpha == 0xcd09)
            /* type 5-9 write */ return 2;
        else if (alpha == 0xc50a)
            /* type 5-10 read  */ return 4;
        else if (alpha == 0xcd0a)
            /* type 5-10 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc50b)
            /* type 5-11 read  */ return x[1].word + 2;
        else if (alpha == 0xcd0b)
            /* type 5-11 write */ return x[1].word + 2;
        else if (alpha == 0xc50c)
            /* type 5-12 read  */ return 2;
        else if (0xc5f0 <= alpha && alpha <= 0xc5f3)
            /* type 5-24X read  */ return 2;
        else if (0xcdf0 <= alpha && alpha <= 0xcdff)
            /* type 5-24X write */ return 2;
        return -3; // Unknown ?
    }
    else if (len == -2) {
        unsigned char kappa = (unsigned char) x[1].byte.lo;
        return 2+(kappa+1)/2;
    }
    else if (len == -3) {
        unsigned int kappa = (unsigned short) x[1].word;
        return 4+(kappa+1)/2;
    }
    else if (len == -4) {
        unsigned int lambda = (unsigned short) x[0].byte.lo;
        return 1+lambda;
    }
    else 
        return -3; // Unknown ?
} //_PAM_APP_alphaLength

// -----------------------------------------------------------------------------
/// Determine if single alpha code packet will generate a response from the
/// target

static int _PAM_APP_isResponse (PAM_AlphaUnit *alphaBuf)
{
    PAM_AlphaUnion   *x = (PAM_AlphaUnion *) alphaBuf;
    int             len;


    if ((x[0].byte.hi & 0xc0) == 0x40)
        return -1; // DA/M
    else if ((x[0].byte.hi & 0xc0) != 0xc0)
        return 1; // IA/M

    len = htgnel[x[0].byte.hi & 0x0f];

    if (len >= 0) {
        if (len == 0)
            return 0;
        else
            return 1;
    }    
    else if (len == -1) {        
        unsigned short alpha = x[0].word & 0xcfff;

        if (alpha == 0xc500)
            /* type 5-0 read  */ return 1;
        else if (alpha == 0xcd00)
            /* type 5-0 write */ return 1;
        else if (alpha == 0xc501)
            /* type 5-1 read  */ return 1;
        else if (alpha == 0xcd01)
            /* type 5-1 write */ return 1;
        else if (alpha == 0xc506)
            /* type 5-6 read  */ return 1;
        else if (alpha == 0xcd06)
            /* type 5-6 write */ return 0;
        else if (alpha == 0xc508) 
                                 return 1; /**/       
        else if (alpha == 0xc509)
            /* type 5-9 read  */ return 1;
        else if (alpha == 0xcd09)
            /* type 5-9 write */ return 0;
        else if (alpha == 0xc50a)
            /* type 5-10 read */ return 1;
        else if (alpha == 0xcd0a)
            /* type 5-10 write */ return 0;
        else if (alpha == 0xc50b)
            /* type 5-11 read  */ return 1;
        else if (alpha == 0xcd0b)
            /* type 5-11 write */ return 0;
        else if (alpha == 0xc50c)
            /* type 5-12 read  */ return 0;
        else if (0xc5f0 <= alpha && alpha <= 0xc5f3)
            /* type 5-X read  */ return 1;
        else if (0xcdf0 <= alpha && alpha <= 0xcdff)
            /* type 5-X write */ return 0;
        return -3; // Unknown ?
    }
    else if (len == -2)
        return 1;
    else if (len == -3) 
        return 1;
    else 
        return -3; // Unknown ?
} //_PAM_APP_isResponse

// -----------------------------------------------------------------------------
/// Determine if multiple alpha code packet will generate a response from the
/// target

int _PAM_APP_isResponseMessage (char *buf, int numBytes)
{
    PAM_AlphaUnit     *from = (PAM_AlphaUnit *) buf;
    int           loopAlpha = 0;
    int           numAlpha = numBytes*2;     


    // loop over packet and if any code generates a response then return 1
    // else return 0
    do {
        if (_PAM_APP_isResponse (from + loopAlpha))
            return 1;           
        loopAlpha += _PAM_APP_alphaLength (from + loopAlpha);
    } while (loopAlpha < numAlpha);  
     
    return 0;
} //_PAM_APP_isResponseMessage

// -----------------------------------------------------------------------------
