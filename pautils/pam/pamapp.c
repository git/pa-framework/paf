
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/M Application Protocol implementations
//
//
//

/// \file pamapp.c
/// \brief PA/M Application Protocol implementations
/// \ingroup PAM

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pamapp.h"
#include "_pamapp.h"
#include <pamlink.h>

static void *waitThread_ (void *pArg);


// -----------------------------------------------------------------------------
/// \brief Free instance
///
/// \param [in]  handle Handle returned from PAM_APP_open
/// \return 0 if successful and 1 otherwise

int PAM_APP_close  (PAM_APP_Handle handle)
{
    int retVal;


    retVal = sem_destroy (&(handle->semObj));
    if (retVal < 0) {
        fprintf (stderr, "PAM_APP__close: sem_init () failed %d.\n", retVal);
        return 1;
    }

    retVal = pthread_cancel (handle->waitThread);
    if (retVal) {
        fprintf (stderr, "PAM_APP_close: pthread_cancel failed %d\n", retVal);
        return 1;
    }

    retVal = pthread_join (handle->waitThread, NULL);
    if (retVal) {
        fprintf (stderr, "PAM_APP_close: pthread_join failed %d\n", retVal);
        return 1;
    }
    
    retVal = PAM_LINK_close ((PAM_LINK_Handle) (handle->pTransport));
    if (retVal) {
        fprintf (stderr, "PAM_APP_close: PAM_LINK_destroy failed %d\n", retVal);
        return 1;
    }

    free (handle);

    return 0;
} //PAM_APP_close

// -----------------------------------------------------------------------------
/// \brief Blocking (synchronous) message exchange
///
/// \param [in]      *inpBuf Pointer to user buffer containing alpha code packet
/// \param [out]     *outBuf Pointer to user buffer which will contain response data
/// \param [in]       inSize Number of bytes of valid data in inpBuf
/// \param [out]    *outSize Pointer to int which contains number of bytes written to outBuf
/// \param [in,out]   handle Handle returned from PAM_APP_open
/// \return 0 if successful and 1 otherwise

int PAM_APP_message  (char *inpBuf, char *outBuf, int inSize, int *outSize, PAM_APP_Handle handle)
{
    PAM_LINK_Handle  plHandle = (PAM_LINK_Handle) handle->pTransport;
    int                retVal = 0;


    if (handle->state == 1) {
        fprintf (stderr, "PAM_APP_message: Message queue full.\n");
        return 1;
    }

    retVal = PAM_LINK_write (inpBuf, inSize, plHandle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_message: PAM_LINK_write error %d\n", retVal);
        return 1;
    }

    // Nota bene: since this layer only supports LINK transport atm we don't check
    // isResponseMessage and always fetch a return message. In the future
    // this read will be conditioned on transport so other physical connections,
    // such as I2C or SPI, will not request a return message when not needed.
    retVal = PAM_LINK_read (outBuf, outSize, plHandle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_message: PAM_LINK_read error %d\n", retVal);
        return 1;
    }

    return 0;
} //PAM_APP_message

// -----------------------------------------------------------------------------
/// \brief Non-Blocking (asynchronous) message exchange
/// User must wait for callback function before calling again
///
/// \param [in]      *inpBuf Pointer to user buffer containing alpha code packet
/// \param [out]     *outBuf Pointer to user buffer which will contain response data
/// \param [in]       inSize Number of bytes of valid data in inpBuf
/// \param [out]    *outSize Pointer to int which contains number of bytes written to outBuf
/// \param [in]    *callback Pointer to void function which is called on response is ready
/// \param [in]          arg User provided argument to be passed into callback function when called
/// \param [in,out]   handle Handle returned from PAM_APP_open
/// \return 0 if successful and 1 otherwise

int PAM_APP_messageNB  (char *inpBuf, char *outBuf, int inSize, int *outSize, void *callback, int arg, PAM_APP_Handle handle)
{
    PAM_LINK_Handle  plHandle = (PAM_LINK_Handle) handle->pTransport;
    int                retVal = 0;

    
    if (handle->state == 1) {
        fprintf (stderr, "PAM_APP_messageNB: Message queue full.\n");
        return 1;
    }

    retVal = PAM_LINK_write (inpBuf, inSize, plHandle);
    if (retVal) {
        fprintf (stderr, "PAM_APP_message: PAM_LINK_write error %d\n", retVal);
        return 1;
    }

    // setup context, flag that we are waiting, spawn another thread to do the blocking, and
    // return immediately.
    handle->userCallback = callback;
    handle->userArg      = arg;
    handle->outBuf       = outBuf;
    handle->outSize      = outSize;
    handle->state        = 1;
    
    sem_post (&handle->semObj);

    return 0;
} //PAM_APP_messageNB

// -----------------------------------------------------------------------------
/// \brief Create instance of application layer tied to physical transport
///
/// \param [in]         *path Pointer to string specifying physical transport to use
/// \param [in,out]     *mode Reserved for future use, pass in NULL to ensure compatibility
/// \param [in,out]  *pHandle Pointer, which on successful return, contains address of handle
/// \return 0 if successful and 1 otherwise

int PAM_APP_open  (char *path, char *mode, PAM_APP_Handle *pHandle)
{
    int retVal;


    // currently only supports LINK transport
    if (strncmp (path, "pamlink", strlen ("pamlink")) != 0) {
        fprintf (stderr, "PAM_APP_open failed. Unsupported path = %s\n", path);
        return 1;
    }

    *pHandle = (PAM_APP_Handle) malloc (sizeof(PAM_APP_Obj));
    if (!*pHandle) {
        fprintf (stderr, "PAM_APP_open: malloc () failed\n");
        return 1;
    }

    retVal = PAM_LINK_open ((PAM_LINK_Handle *) &((*pHandle)->pTransport), 0);
    if (retVal || ! (*pHandle)->pTransport) {
        fprintf (stderr, "PAM_APP_Open: PAM_LINK_create failed %d\n", retVal);
        return 1;
    }

    retVal = sem_init (&((*pHandle)->semObj), 0, 0);
    if (retVal < 0) {
        fprintf (stderr, "PAM_APP_open: sem_init () failed %d.\n", retVal);
        return 1;
    }

    retVal = pthread_create (&(*pHandle)->waitThread, NULL, waitThread_, (void*) *pHandle); 
    if (retVal) {
        fprintf (stderr, "PAM_APP_open: pthread_create () failed %d.\n", retVal);
        return 1;
    }

    (*pHandle)->state = 0;

    return 0;
} //PAM_APP_open

// -----------------------------------------------------------------------------
/// \brief Private function used to facilitate non-blocking messaging

static void *waitThread_ (void *pArg) 
{
    PAM_APP_Handle     handle = (PAM_APP_Handle) pArg;
    PAM_LINK_Handle  plHandle = (PAM_LINK_Handle) handle->pTransport;
    int                retVal;


    // wait for signal to read data, block on read, then signal user thread
    while (1) {

        sem_wait (&handle->semObj);

        if (handle->state != 1) {
            fprintf (stderr, "PAM_APP _waitThread_: unexpected state = %d\n", handle->state);
            continue;
        }
        
        if (!handle->userCallback) {
            fprintf (stderr, "PAM_APP _waitThread_: userCallback not defined\n");
            continue;
        }
      
        // Nota bene: since this layer only supports LINK transport atm we don't check
        // isResponseMessage and always fetch a return message. In the future
        // this read will be conditioned on transport so other physical connections,
        // such as I2C or SPI, will not request a return message when not needed.
        retVal = PAM_LINK_read (handle->outBuf, handle->outSize, plHandle);
        if (retVal) {
            fprintf (stderr, "PAM_APP_message: PAM_LINK_read error %d\n", retVal);
            continue;
        }

        // signal user
        (*handle->userCallback) (handle->userArg);

    }
} //waitThread_

// -----------------------------------------------------------------------------
