
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/M Application Protocol definitions
//
//
//

/// \file pamapp.h
/// \brief PA/M Application Protocol definitions
/// \ingroup PAM

#ifndef PAMAPP_H
#define PAMAPP_H

#include <pthread.h>
#include <semaphore.h>

typedef enum 
{
    PAM_TRANSPORT_LINK
} PAM_TRANSPORT_MODE;

typedef struct PAM_APP_Obj {
    PAM_TRANSPORT_MODE                ptMode;
    void                         *pTransport;
    sem_t                             semObj;
    pthread_t                     waitThread;
    int                                state;
    void               (*userCallback) (int);
    int                              userArg;
    char                             *outBuf;
    int                             *outSize;
} PAM_APP_Obj, *PAM_APP_Handle;

extern int PAM_APP_close      (PAM_APP_Handle handle);
extern int PAM_APP_message    (char *inpBuf, char *outBuf, int inSize, int *outSize, PAM_APP_Handle handle);
extern int PAM_APP_messageNB  (char *inpBuf, char *outBuf, int inSize, int *outSize, void *callback, int arg, PAM_APP_Handle handle);
extern int PAM_APP_open       (char *path, char *mode, PAM_APP_Handle *pHandle);

#endif //PAMAPP_H

