
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Program for testing PAM_APP + PAM_LINK layer
//
//
//

#include <stdio.h>

#include <niclink.h>
#include <palink.h>
#include <pamlink.h>
#include <pamapp.h>

char inpBuf[PAM_LINK_MAX_CODE_LEN];
char outBuf[PAM_LINK_MAX_CODE_LEN];

int test1 (void);
int test2 (void);
int test3 (void);
int test4 (void);
int test5 (void);
int test6 (void);
int test7 (void);

int (*tests[])(void) = 
{
    test1,
    test2,
    test3,
    test4,
    test5,
    test6,
    test7
};

void test5Callback (int event, int arg);
void test6Callback (int arg);

// -----------------------------------------------------------------------------

int main (void)
{
    int numTests;
    int   retVal;
    int        i;


    // open LINK with PA DSP image
    retVal = PA_LINK_open ();
    if (retVal) {
        fprintf (stderr, "PA_LINK_open failed %d\n", retVal);
        return 1;
    }
    
    numTests = sizeof(tests) / sizeof (int);
    for (i=0; i < numTests; i++) {
        retVal = (*tests[i]) ();
        if (retVal) {
            fprintf (stderr, "test%d failed %d\n", i+1, retVal);
            return 1;
        }
    }

    // close LINK with PA DSP image
    retVal = PA_LINK_close ();
    if (retVal) {
        fprintf (stderr, "PA_LINK_close failed %d\n", retVal);
        return 1;
    }

    fprintf (stdout, "All (%d) tests passed\n", numTests);

    return 0;
} //main

// -----------------------------------------------------------------------------
// Test single message transfer with response (PAM_LINK layer only)

int test1 (void) 
{
    PAM_LINK_Handle           handle;
    int                       retVal;
    int                     retBytes;


    retVal = PAM_LINK_open (&handle, 0);
    if (retVal || !handle) {
        fprintf (stderr, "test1: PAM_LINK_create failed %d\n", retVal);
        return 1;
    }

    // send readSTDReady = {0xC100}
    inpBuf[0] = 0x00;
    inpBuf[1] = 0xC1;
    retVal = PAM_LINK_write (inpBuf, 2, handle);
    if (retVal) {
        fprintf (stderr, "test1: PAM_LINK_write error %d\n", retVal);
        return 1;
    }

    // invalidate data since not calloc'ed
    outBuf[0] = 0xFF;
    outBuf[1] = 0xFF;

    // fetch message back from DSP and check expectation
    retVal = PAM_LINK_read (outBuf, &retBytes, handle);
    if (retVal) {
        fprintf (stderr, "test1: PAM_LINK_read error %d\n", retVal);
        return 1;
    }

    if ((retBytes != 2) || (outBuf[0] != 0x00) || outBuf[1] != 0xC1) {
        fprintf (stderr, "test1: didn't get expected value\n");
        return 1;
    }

    retVal = PAM_LINK_close (handle);
    if (retVal) {
        fprintf (stderr, "test1: PAM_LINK_destroy failed %d\n", retVal);
        return 1;
    }

    return 0;
} //test1

// -----------------------------------------------------------------------------
// Repeated application of test 1

int test2 (void) 
{
    int retVal;


    retVal = test1 ();
    if (retVal) {
        fprintf (stderr, "test2: test1 failed %d\n", retVal);
        return 1;
    }

    retVal = test1 ();
    if (retVal) {
        fprintf (stderr, "tes2: successive test1 failed %d\n", retVal);
        return 1;
    }

    return 0;
} //test2

// -----------------------------------------------------------------------------
// Open and close using PAM_APP and PAM_LINK layers

int test3 (void) 
{
    PAM_APP_Handle handle;
    int            retVal;


    retVal = PAM_APP_open ("pamlink", NULL, &handle);
    if (retVal) {
        fprintf (stderr, "test3: PAM_APP_open failed %d\n", retVal);
        return 1;
    }

    retVal = PAM_APP_close (handle);
    if (retVal) {
        fprintf (stderr, "test3: PAM_APP_close failed %d\n", retVal);
        return 1;
    }

    return 0;
} //test3

// -----------------------------------------------------------------------------
// Test single message transfer with response (PAM_LINK and PAM_APP layers)

int test4 (void) 
{
    PAM_APP_Handle  handle;
    int             retVal;
    int           retBytes;


    retVal = PAM_APP_open ("pamlink", NULL, &handle);
    if (retVal) {
        fprintf (stderr, "test4: PAM_APP_open failed %d\n", retVal);
        return 1;
    }

    // invalidate data since not calloc'ed
    outBuf[0] = 0xFF;
    outBuf[1] = 0xFF;

    // send readSTDReady = {0xC100}
    inpBuf[0] = 0x00;
    inpBuf[1] = 0xC1;

    retVal = PAM_APP_message  (inpBuf, outBuf, 2, &retBytes, handle);
    if (retVal) {
        fprintf (stderr, "test4: PAM_APP_message failed %d\n", retVal);
        return 1;
    }

    if ((retBytes != 2) || (outBuf[0] != 0x00) || outBuf[1] != 0xC1) {
        fprintf (stderr, "test4: didn't get expected value\n");
        return 1;
    }

    retVal = PAM_APP_close (handle);
    if (retVal) {
        fprintf (stderr, "test4: PAM_APP_close failed %d\n", retVal);
        return 1;
    }

    return 0;
} //test4

// -----------------------------------------------------------------------------
// Test NIC_LINK functionality (single event).

int test5 (void) 
{

    PAM_APP_Handle   pamHandle;
    NIC_LINK_Handle  nicHandle;
    int                 retVal;
    int               retBytes;
    int                 cbFlag;

    cbFlag = 0;
    retVal = NIC_LINK_open (&nicHandle, test5Callback, (int) &cbFlag, 0);
    if (retVal) {
        fprintf (stderr, "test5: NIC_LINK_open failed %d\n", retVal);
        return 1;
    }

    retVal = PAM_APP_open ("pamlink", NULL, &pamHandle);
    if (retVal) {
        fprintf (stderr, "test5: PAM_APP_open failed %d\n", retVal);
        return 1;
    }

    // .........................................................................
    // Clear any pending assertions which may have occured before here

    inpBuf[0] = 0x27;
    inpBuf[1] = 0xCA;
    inpBuf[2] = 0x01;
    inpBuf[3] = 0x07;

    retVal = PAM_APP_message  (inpBuf, outBuf, 4, &retBytes, pamHandle);
    if (retVal) {
        fprintf (stderr, "test5: PAM_APP_message failed %d\n", retVal);
        return 1;
    }

    if (retBytes != 0) {
        fprintf (stderr, "test5: PAM_APP_message returned %d bytes expected 0\n", retBytes);
        return 1;        
    }

    // .........................................................................
    // Register callback and wait for callback

    retVal = NIC_LINK_register (1, nicHandle);
    if (retVal) {
        fprintf (stderr, "test5: NIC_LINK_register failed %d\n", retVal);
        return 1;
    }

    fprintf (stdout, "Waiting for DSP to notify of source program change.\n");
    while (!cbFlag);

    // .........................................................................

    // uregister notification
    retVal = NIC_LINK_unregister (1, nicHandle);
    if (retVal) {
        fprintf (stderr, "test5: NIC_LINK_unregister failed %d\n", retVal);
        return 1;
    }

    retVal = NIC_LINK_close (nicHandle);
    if (retVal) {
        fprintf (stderr, "test5: NIC_LINK_close failed %d\n", retVal);
        return 1;
    }

    retVal = PAM_APP_close (pamHandle);
    if (retVal) {
        fprintf (stderr, "test5: PAM_APP_close failed %d\n", retVal);
        return 1;
    }

    return 0;
} //test5

// .............................................................................

void test5Callback (int event, int arg)
{
    *((int *) arg) = 1;
} //test5Callback

// -----------------------------------------------------------------------------
// Test PAM non-blocking message

int test6 (void) 
{
    PAM_APP_Handle            handle;
    int                       retVal;
    int                     retBytes;
    int                       cbFlag;


    retVal = PAM_APP_open ("pamlink", NULL, &handle);
    if (retVal) {
        fprintf (stderr, "test6: PAM_APP_open failed %d\n", retVal);
        return 1;
    }

    // invalidate data since not calloc'ed
    outBuf[0] = 0xFF;
    outBuf[1] = 0xFF;
    outBuf[2] = 0xFF;
    outBuf[3] = 0xFF;

    // send InNone and InDigital
    inpBuf[0] = 0x20;
    inpBuf[1] = 0xF1;
    inpBuf[2] = 0x21;
    inpBuf[3] = 0xF1;

    cbFlag = 0;
    retVal = PAM_APP_messageNB  (inpBuf, outBuf, 4, &retBytes, test6Callback, (int) &cbFlag, handle);
    if (retVal) {
        fprintf (stderr, "test6: PAM_APP_message failed %d\n", retVal);
        return 1;
    }

    while (!cbFlag);

    if ((retBytes != 4) || (outBuf[0] != 0x20) || (outBuf[1] != 0xF1)
                        || (outBuf[2] != 0x21) || (outBuf[3] != 0xF1)) {
        fprintf (stderr, "test6: didn't get expected value\n");
        return 1;
    }


    retVal = PAM_APP_close (handle);
    if (retVal) {
        fprintf (stderr, "test6: PAM_APP_close failed %d\n", retVal);
        return 1;
    }

    return retVal;
} //test6

// .............................................................................

void test6Callback (int arg)
{
    *((int *) arg) = 1;
} //test6Callback

// -----------------------------------------------------------------------------
// Repeated application of test 4

int test7 (void) 
{
    int retVal;
    int i;

    
    for (i=0; i < 1000; i++) {
        retVal = test4 ();
        if (retVal) {
            fprintf (stderr, "test7: test4 iteration %d failed %d\n", i, retVal);
            return 1;
        }
    }

    return 0;
} //test7

// -----------------------------------------------------------------------------

