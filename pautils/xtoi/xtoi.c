
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Alpha stream (.?) to preprocessed C (.i) conversion
//
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define lengthof(X) (sizeof (X) / sizeof (*(X)))

void
usage ()
{
    fprintf (stderr, "usage: xtoi [-v] [{+-}a] [{+-}l] [-{BIS}] <input >output\n");
    fprintf (stderr, "       -v:    Display version (and continue)\n");
    fprintf (stderr, "       -a:    Output alpha code [default]\n");
    fprintf (stderr, "       +a:    Output C code\n");
    fprintf (stderr, "       -l:    Legacy processing (unused)\n");
    fprintf (stderr, "       +l:    Non-legacy processing [default]\n");

    fprintf (stderr, "       -B:    Input binary records [default]\n");
    fprintf (stderr, "       -C:    Output C records, 8-bit\n");
    fprintf (stderr, "       -D:    Output C records, 16-bit\n");
    fprintf (stderr, "       -I:    Input I records\n");
    fprintf (stderr, "       -S:    Input S records\n");
    exit (1);
}

int aflag = 1;
int lflag = 0;
int sflag = 0;

int line = 0;

int parseFirst = -1;
int parseLast = 0;
int parseCount = 0;

void
parsePrint (unsigned short si) {

    char *head = aflag ? "alpha" : "{";
    char *tail = aflag ? "\n" : " }\n";

    if (parseCount > 0) {
        printf ("%s0x%04x", parseFirst ? " " : ",", si);
        parseFirst = 0;
        si = 0;
        parseCount--;
    }
    else if (parseLast == 0xcd01) {
        if (aflag) {
            printf ("%s%s%s", parseFirst<0 ? "" : tail, head,
		    si ? "" : " /* none */");
            parseFirst = 1;
        }
        else {
            printf ("%s%s 0x%04x,0x%04x%s", parseFirst<0 ? "" : tail, head,
		    parseLast, si, si ? "" : " /* none */");
            parseFirst = 0;
        }
        parseCount = si;
    }
    else if ((si & 0xff00) == 0xc900
	     || (lflag && parseFirst && (si & 0x8000) == 0)) {
        if (aflag) {
            printf ("%s%s%s", parseFirst<0 ? "" : tail, head,
		    si & 0xff ? "" : " /* none */");
            parseFirst = 1;
        }
        else {
            printf ("%s%s%s 0x%04x", parseFirst<0 ? "" : tail, head,
		    si & 0xff ? "" : " /* none */", si);
            parseFirst = 0;
        }
        parseCount = si & 0x00ff;
    }
    else if (si == 0xcd01) {
    }
    else {

        printf ("%s0x%04x", parseFirst ? "" : "\n", si);
        parseFirst = 0;
    }
    parseLast = si;
}

void parseFinal () {
    char *tail = aflag ? "\n" : " }\n";

    printf ("%s", parseFirst<0 ? "" : tail);
}

int
parse_b (unsigned short *s, int n)
{
    if (s) {
        int i;
        for (i=0; i < n; i++)
            parsePrint (s[i]);
    }
    else {
        parseFinal ();
    }

    return 0;
}

int
do_b (FILE *fp)
{
    unsigned short s[64];

    int n;

    while (n = fread (s, sizeof (*s), lengthof (s), fp))
        parse_b (s, n);
    parse_b (NULL, 0);

    return 0;
}

char *
sgets (FILE *fp)
{
    static char string[1024];

    if (! fgets (string, lengthof (string)-1, fp))
        return NULL;

    line++;

    if (string[0] != '\0') {
        if (string[strlen(string)-1] != '\n') {
            fprintf (stderr, "xtoi: string too long at line %d\n", line);
            exit (1);
        }
        if (string[strlen(string)-1] == '\n')
            string[strlen(string)-1] = '\0';
        if (string[strlen(string)-1] == '\r')
            string[strlen(string)-1] = '\0';
    }

    return string;
}

int
parse_s (char *s)
{
    char *head = aflag ? "alpha" : "{";
    char *tail = aflag ? "\n" : " }\n";

    if (s) {
        for ( ; *s; s+= 4) {
            int ih, il;
            if (sscanf (s, "%02x%02x", &il, &ih) != 2)
                return 1;
            parsePrint ((ih << 8) + il);
        }
    }
    else {
        parseFinal ();
    }

    return 0;
}

int
do_s (FILE *fp)
{
    char *s;

    int ss = -1;

    for (;;) {
        if (! (s = sgets (fp))) {
            if (ss != -1 && ss != 9) {
                fprintf (stderr, "xtoi: EOF at line %d\n", line);
                exit (1);
            }
            else
                return 0;
        }
        else if (! strcmp (s, "")) {
            continue;
        }
        else if (! strcmp (s, "S0030000FC") || ! strcmp (s, "S0030000??")) {
            ss = 0;
        }
        else if (! strcmp (s, "S9030000FC") || ! strcmp (s, "S9030000??")) {
            if (ss != 0 && ss != 1) {
                fprintf (stderr, "xtoi: S-record phasing error at line %d\n",
			 line);
                exit (1);
            }
            if (parse_s (NULL)) {
                fprintf (stderr, "xtoi: S-record terminal error at line %d\n",
			 line);
                exit (1);
            }
            ss = 9;
        }
        else if (! strncmp (s, "S1", 2)) {
            s[strlen (s) - 2] = '\0';
            if (parse_s (s+8)) {
                fprintf (stderr, "xtoi: S-record content error at line %d\n",
			 line);
                exit (1);
            }
        }
        else {
            fprintf (stderr, "xtoi: not S record at line %d\n", line);
            exit (1);
        }
    }
}

int
mgets (char **pS)
{
    unsigned char c;
    int x1, x0;

    c = **pS;
    (*pS)++;

    if (c == '\0')
        return EOF;
    else if (c != '~')
        return c;

    if (sscanf (*pS, "%01x%01x", &x1, &x0) != 2)
        return EOF;
    else {
        *pS += 2;
        return 16*x1 + x0;
    }
}

int
parse_i (char *s)
{
    char *head = aflag ? "alpha" : "{";
    char *tail = aflag ? "\n" : " }\n";

    if (s) {
        int n;

        n = mgets (&s);
        if (n & 1)
            n += (mgets (&s) << 8) - 1;

        for ( ; n; n-=2) {
            int ih, il;
            il = mgets (&s);
            ih = mgets (&s);
            if (il == EOF || ih == EOF)
                return 1;
            parsePrint ((ih << 8) + il);
        }
    }
    else {
        parseFinal ();
    }

    return 0;
}

int
do_i (FILE *fp)
{
    char *s;

    int ss = -1;

    for (;;) {
        if (! (s = sgets (fp))) {
            if (ss != -1 && ss != 9) {
                fprintf (stderr, "xtoi: EOF at line %d\n", line);
                exit (1);
            }
            else
                return 0;
        }
        else if (! strcmp (s, "")) {
            continue;
        }
        else if (! strncmp (s, "/O", 2)) {
            ss = 0;
        }
        else if (! strncmp (s, "/C", 2)) {
            if (ss != 0 && ss != 1) {
                fprintf (stderr, "xtoi: I-record phasing error at line %d\n",
			 line);
                exit (1);
            }
            if (parse_i (NULL)) {
                fprintf (stderr, "xtoi: I-record terminal error at line %d\n",
			 line);
                exit (1);
            }
            ss = 9;
        }
        else if (! strncmp (s, "/T", 2)) {
            if (parse_i (s+2)) {
                fprintf (stderr, "xtoi: I-record content error at line %d\n",
			 line);
                exit (1);
            }
        }
        else if (! strncmp (s, "/MRC", 4)) {
            if (parse_i (s+4)) {
                fprintf (stderr, "xtoi: I-record content error at line %d\n",
			 line);
                exit (1);
            }
        }
        else if (strncmp (s, "/", 1) && strncmp (s, "*", 1)) {
            fprintf (stderr, "xtoi: not I record at line %d\n", line);
            exit (1);
        }
    }
}

int
cgets (char **pS)
{
    int x;

    if (sscanf (*pS, "%02x", &x) != 1)
        return EOF;
    else
        return x;
}

int
parse_c (char *s)
{
    static int state = 0;
    static int cnt;
    static int lsb;

    if (s) {
        int n;

        if ((n = cgets (&s)) == EOF)
            return 1;

        switch (state) {
	    case 0:
		if (n & 1) {
		    lsb = n;
		    state = 1;
		}
		else {
		    cnt = n / 2;
		    state = cnt ? 2 : 0;
		}
		break;
	    case 1:
		cnt = (256 * n + lsb) / 2;
		state = cnt ? 2 : 0;
		break;
	    case 2:
		lsb = n;
		state = 3;
		break;
	    case 3:
		parsePrint (256 * n + lsb);
		state = --cnt ? 2 : 0;
		break;
        }
    }
    else {
        if (! state)
            parseFinal ();
        else
            return 1;
    }

    return 0;
}

int
dgets (char **pS)
{
    int x;

    if (sscanf (*pS, "%04x", &x) != 1)
        return EOF;
    else
        return x;
}

int
parse_d (char *s)
{
    static int state = 0;
    static int cnt;
    static int lsb;

    if (s) {
        int n;

        if ((n = dgets (&s)) == EOF)
            return 1;

        switch (state) {
	    case 0:
		cnt = n;
		state = 1;
		break;
	    case 1:
		parsePrint (n);
		state = --cnt ? 1 : 0;
		break;
        }
    }
    else {
        if (! state)
            parseFinal ();
        else
            return 1;
    }
    return 0;
}

int
do_c (FILE *fp, int cc)
{
    char *s;

    int (*parse) (char *) = ! cc ? parse_c : parse_d;

    for (;;) {
        if (! (s = sgets (fp))) {
            if ((*parse) (NULL)) {
                fprintf (stderr,
			 "xtoi: C-record (%d-bit) terminal error at line %d\n",
			 ! cc ? 8 : 16, line);
                exit (1);
            }
            else
                return 0;
        }
        else if (! strcmp (s, "")) {
            continue;
        }
        else {
            if ((*parse) (s)) {
                fprintf (stderr,
			 "xtoi: C-record (%d-bit) content error at line %d\n",
			 ! cc ? 8 : 16, line);
                exit (1);
            }
        }
    }
}

int
main (int argc, char **argv)
{
    if (argc > 1 && ! strncmp (argv[1], "-v", 2)) {
        argc--, argv++;
    }

    if (argc > 1 && ! strcmp (argv[1], "-a")) {
        argc--, argv++;
        aflag = 1;
    }
    else if (argc > 1 && ! strcmp (argv[1], "+a")) {
        argc--, argv++;
        aflag = 0;
    }

    if (argc > 1 && ! strcmp (argv[1], "-l")) {
        argc--, argv++;
        lflag = 1;
    }
    else if (argc > 1 && ! strcmp (argv[1], "+l")) {
        argc--, argv++;
        lflag = 0;
    }

    if (argc > 1 && ! strcmp (argv[1], "-B")) {
        argc--, argv++;
        sflag = 0;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-S")) {
        argc--, argv++;
        sflag = 1;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-I")) {
        argc--, argv++;
        sflag = 2;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-C")) {
        argc--, argv++;
        sflag = 3;
    }
    else if (argc > 1 && ! strcmp (argv[1], "-D")) {
        argc--, argv++;
        sflag = 4;
    }

    if (argc != 1)
        usage ();

    if (sflag >= 5) {
        fprintf (stderr, "itox: internal error\n");
        exit (1);
    }

    if (sflag == 0) {
        FILE *fp;
        if (! (fp = fdopen (0, "rb"))) {
            fprintf (stderr, "xtoi: can't reopen stdin\n");
            exit (1);
        }
        exit (do_b (stdin));
    }
    else if (sflag == 1) {
        exit (do_s (stdin));
    }
    else if (sflag == 2) {
        exit (do_i (stdin));
    }
    else if (sflag == 3 || sflag == 4) {
        exit (do_c (stdin, sflag-3));
    }
}
