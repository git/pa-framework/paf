
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// AAC Decoder alpha codes
//
//
//

#ifndef _AAC_A
#define _AAC_A

#include <acpbeta.h>

#define  readAACMode 0xc200+STD_BETA_AAC,0x0400
#define writeAACModeDisable 0xca00+STD_BETA_AAC,0x0400
#define writeAACModeEnable 0xca00+STD_BETA_AAC,0x0401

/* Return entire AAC bit stream information */
#define  readAACBitStreamInformation 0xc600+STD_BETA_AAC2,0x0428

#define  readAACBitStreamInformation0 0xc300+STD_BETA_AAC2,0x0004

/* LSB indicates ID, MSB indicates layer -- given by header */
#define  readAACBitStreamInformation1 0xc300+STD_BETA_AAC2,0x0006

/* LSB indicates protection_absent, MSB indicates profile -- given by header */
#define  readAACBitStreamInformation2 0xc300+STD_BETA_AAC2,0x0008

/* LSB indicates sampling_frequency_index, MSB indicates private-bit -- given by header */
#define  readAACBitStreamInformation3 0xc300+STD_BETA_AAC2,0x000a

/* LSB indicates channel configuration, 
*  Lower nibble of MSB indicates original copy
*  Higher nibble of MSB indicates home -- given by header */
#define  readAACBitStreamInformation4 0xc300+STD_BETA_AAC2,0x000c

/* LSB indicates copyright_identification_bit,
 *  MSB indicates copyright_indentification_start -- given by header */
#define  readAACBitStreamInformation5 0xc300+STD_BETA_AAC2,0x000e

/* Returns framelength in bytes -- given by header */
#define  readAACBitStreamInformation6 0xc300+STD_BETA_AAC2,0x0010


/* Returns framelengthadts_buffer_fullness -- given by header */
#define  readAACBitStreamInformation7 0xc300+STD_BETA_AAC2,0x0012

/* Returns number of raw_data_blocks in ADTS frame -- given by header */
#define  readAACBitStreamInformation8 0xc300+STD_BETA_AAC2,0x0014

/* Returns crc value if protection_absent is zero else returns 0 -- given by header */
#define  readAACBitStreamInformation9 0xc300+STD_BETA_AAC2,0x0016

/* Indicates whether PCE is present or not. A non-zero value means PCE is present */
#define  readAACBitStreamInformation10 0xc300+STD_BETA_AAC2,0x0018

/* LSB indicates number of front channel elements
 * MSB indicates number of side channel elements -- given by PCE syntactic element. */
#define  readAACBitStreamInformation11 0xc300+STD_BETA_AAC2,0x001A

/* LSB indicates number of back channel elements
 * MSB indicates number of lfe channel elements -- given by PCE syntactic element */
#define  readAACBitStreamInformation12 0xc300+STD_BETA_AAC2,0x001C

/* Note that if (as given by PCE syntactic element in the bistream),
   number of side channel elements is zero and
   number of back channel elements is non-zero, then
   information regarding the back channels are put into side channels
   and back channel information is resetted.
   This is in accordance with the ARIB standard.
*/   


/* LSB indicates whether first front channel element is CPE or not,
 * MSB indicates whether second front channel element is CPE or not.
 * A non-zero value indicates that the corresponding channel emenet is CPE.
 * -- given by PCE syntactic element
*/
#define  readAACBitStreamInformation13 0xc300+STD_BETA_AAC2,0x001E

/* LSB indicates whether side channel element is CPE or not,
 * MSB indicates whether back channel element is CPE or not.
 * A non-zero value indicates that the corresponding channel emenet is CPE.
 * -- given by PCE syntactic element
*/
#define  readAACBitStreamInformation14 0xc300+STD_BETA_AAC2,0x0020

/* LSB indicates whether matric mixdown present or not,
 * Lower nibble of MSB indicates whether Pseudo surround  or not.
 * Higher nibble of MSB indicates Matrix mixdown index.
 * -- given by PCE syntactic element
*/
#define  readAACBitStreamInformation15 0xc300+STD_BETA_AAC2,0x0022

/* Returns the input bitrate  */
#define  readAACBitRate 0xc400+STD_BETA_AAC2,0x0024

/* Returns the input channel configuration thats used for decoding the input.
 * This may be different from LSB of readAACBitStreamInformation4 when what
 * the header indicates is different than the bitstream content.   
*/
#define  readAACBitStreamInformation18 0xc300+STD_BETA_AAC2,0x0028

/* LSB indicates the number of single channel element
 * Lower nibble of MSB indicates the number of channel pair elements
 * Higher nibble of MSB indicates the number of LFE channel elements.
*/
#define  readAACBitStreamInformation19 0xc300+STD_BETA_AAC2,0x002A


/* Indicates whether CRC check is enabled or disabled */
#define readAACCRCCheckMode 0xc200+STD_BETA_AAC,0x0500

/* Enable CRC check */
#define writeAACCRCCheckModeEnable 0xca00+STD_BETA_AAC,0x0501

/* Disable CRC check */
#define writeAACCRCCheckModeDisable 0xca00+STD_BETA_AAC,0x0500

/* Indicates whether the channel config. info in header
 * is used for decoding or not.
*/
#define readAACHeaderMode 0xc200+STD_BETA_AAC,0x0600

/* Enable usage of channel config info from header in decoding.
 * When the channel config info from header doesn't match with
 * that of the bitstream content, this aborts decoding.
*/
#define writeAACHeaderModeEnable 0xca00+STD_BETA_AAC,0x0601

/* Disable usage of channel config info from header in decoding.
 * When the channel config info from header doesn't match with
 * that of the bitstream content, this doesn't abort decoding.
*/
#define writeAACHeaderModeDisable 0xca00+STD_BETA_AAC,0x0600

/* Read CRC error */
#define readAACCRCError 0xc200+STD_BETA_AAC,0x0700

/* Indicates there is no CRC error */
#define wroteAACCRCErrorAbsent 0xca00+STD_BETA_AAC,0x0700

/* Indicates there is a CRC error */
#define wroteAACCRCErrorPresent 0xca00+STD_BETA_AAC,0x0701

/* Indicates whether channel assignment mode is Normal or special */
#define readAACChannelAssignmentMode 0xc200+STD_BETA_AAC,0x0800

/* In Normal mode, audio data is present in their respective channels.
 * e.g bistream havign data in C, Ls and Rs will have output in C,L,R,Ls and Rs with
 * data in L and R being 0.
*/
#define writeAACChannelAssignmentModeNormal 0xca00+STD_BETA_AAC,0x0800

/* In Special mode, audio data is copied to Land R wherever applicable.
 * e.g bistream havign data in C, Ls and Rs will have output in C,L and R with
 * data in Ls(Rs) being copied to L(R).
*/
#define writeAACChannelAssignmentModeSpecial 0xca00+STD_BETA_AAC,0x0801

/* Read whether LFE is included in downmix or not */
#define readAACLFEDownmixInclude 0xc200+STD_BETA_AAC,0x0900

/* Enable LFE in downmix operation */
#define writeAACLFEDownmixIncludeNo 0xca00+STD_BETA_AAC,0x0900

/* Disable LFE in downmix operation */
#define writeAACLFEDownmixIncludeYes 0xca00+STD_BETA_AAC,0x0901

/* Read LFE volume control register value */
#define readAACLFEDownmixVolume 0xc200+STD_BETA_AAC,0x0a00

/* Set AAC volume control register value to NN dB in units of 0.5dB */
#define writeAACLFEDownmixVolumeN(NN) 0xca00+STD_BETA_AAC,0x0a00+(0xff&(NN))
/* in support of inverse compilation only */
#define writeAACLFEDownmixVolumeN__0__ writeAACLFEDownmixVolumeN(0)

/* Read whether special ARIB downmix is enabled or not */
#define readAACARIBDownmixMode 0xc200+STD_BETA_AAC,0x0b00

/* Disable special ARIB downmix mode */
#define writeAACARIBDownmixModeDisable 0xca00+STD_BETA_AAC,0x0b00

/* Enable special ARIB downmix mode */
#define writeAACARIBDownmixModeEnable 0xca00+STD_BETA_AAC,0x0b01

/* Read whether special ARIB downmix for Surround processor is enabled or not */
#define readAACARIBDownmixForSurrProcessor 0xc200+STD_BETA_AAC,0x0c00

/* Disable special ARIB downmix for Surround processor.*/
#define writeAACARIBDownmixForSurrProcessorDisable 0xca00+STD_BETA_AAC,0x0c00

/* Enable special ARIB downmix for Surround processor.*/
#define writeAACARIBDownmixForSurrProcessorEnable 0xca00+STD_BETA_AAC,0x0c01

/* Read mode whether PCE information to be retained 
   when channel_config=0 as per header and PCE is present */
#define readAACRetainPCEInfo 0xc200+STD_BETA_AAC,0x0d00

/* Enable the mode to retain PCE info when 
   when channel_config=0 as per header and PCE is present */
#define writeAACRetainPCEInfoEnable 0xca00+STD_BETA_AAC,0x0d01

/* Disable the mode to retain PCE info when 
   when channel_config=0 as per header and PCE is present */
#define writeAACRetainPCEInfoDisable 0xca00+STD_BETA_AAC,0x0d00


#define wroteAACBitStreamInformation 0xce00+STD_BETA_AAC2,0x0425

/* Return entire AAC status structure */
#define  readAACStatus 0xc508,STD_BETA_AAC

/* Return entire AAC bitstream information structure */
#define  readAACCommon 0xc508,STD_BETA_AAC2

#define  readAACControl \
         readAACMode, \
         readAACCRCCheckMode, \
         readAACHeaderMode, \
         readAACCRCError, \
         readAACChannelAssignmentMode, \
         readAACLFEDownmixInclude, \
         readAACLFEDownmixVolume, \
         readAACARIBDownmixMode, \
         readAACARIBDownmixForSurrProcessor, \
		 readAACRetainPCEInfo

#endif /* _AAC_A */
