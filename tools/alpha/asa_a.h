
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


// Advanced Speaker Array Algorithm alpha codes
//
//


#ifndef _ASA_A
#define _ASA_A

#include <acpbeta.h>

#define  readASAMode 0xc200+STD_BETA_ASA,0x0400
#define writeASAModeDisable 0xca00+STD_BETA_ASA,0x0400
#define writeASAModeEnable 0xca00+STD_BETA_ASA,0x0401

#define  readASAOperationalMode 		0xc200+STD_BETA_ASA,0x0500
#define writeASAOperationalModeOFF      0xca00+STD_BETA_ASA,0x0500
#define writeASAOperationalModeCinema   0xca00+STD_BETA_ASA,0x0501
#define writeASAOperationalModeMusic    0xca00+STD_BETA_ASA,0x0502
#define writeASAOperationalModeEX       0xca00+STD_BETA_ASA,0x0503
#define writeASAOperationalModeGames    0xca00+STD_BETA_ASA,0x0505

#define  readASAOperationMode 			 readASAOperationalMode 		
#define writeASAOperationModeOFF      	writeASAOperationalModeOFF      
#define writeASAOperationModeCinema   	writeASAOperationalModeCinema   
#define writeASAOperationModeMusic    	writeASAOperationalModeMusic    
#define writeASAOperationModeEX       	writeASAOperationalModeEX       
#define writeASAOperationModeGames     	writeASAOperationalModeGames    

#define readASADistanceMode 		  	0xc200+STD_BETA_ASA,0x0600
#define writeASADistanceModeNear     	0xca00+STD_BETA_ASA,0x0600
#define writeASADistanceModeFar   		0xca00+STD_BETA_ASA,0x0601
#define writeASADistanceModeVeryFar    	0xca00+STD_BETA_ASA,0x0602

#define readASAThxStatus		  		0xc200+STD_BETA_ASA,0x0700
#define wroteASAThxOFF      	        0xca00+STD_BETA_ASA,0x0700
#define wroteASAThxCinema   	        0xca00+STD_BETA_ASA,0x0701
#define wroteASAThxMusic    	        0xca00+STD_BETA_ASA,0x0702
#define wroteASAThxEX       	        0xca00+STD_BETA_ASA,0x0703
#define wroteASAThxUltra2Cinema       	0xca00+STD_BETA_ASA,0x0704
#define wroteASAThxGames		       	0xca00+STD_BETA_ASA,0x0705

#define readASAStatus 0xc508,STD_BETA_ASA
#define readASAControl \
        readASAMode, \
        readASAOperationalMode, \
        readASADistanceMode, \
        readASAThxStatus

#endif /* _ASA_A */
