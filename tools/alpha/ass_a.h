
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Split alpha codes
//
//
//

#ifndef _ASS_A
#define _ASS_A

#include <acpbeta.h>

#define  readASSMode 0xc200+STD_BETA_ASS,0x0400
#define writeASSModeDisable 0xca00+STD_BETA_ASS,0x0400
#define writeASSModeEnable 0xca00+STD_BETA_ASS,0x0401

#define  readASSChannelConfigurationRequest 0xc400+STD_BETA_ASS,0x0008
#define writeASSChannelConfigurationRequestNone 0xcc00+STD_BETA_ASS,0x0008,0x0001,0x0000
#define writeASSChannelConfigurationRequestMono 0xcc00+STD_BETA_ASS,0x0008,0x0002,0x0000
#define writeASSChannelConfigurationRequestStereo 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0000
#define writeASSChannelConfigurationRequestStereoLtRt 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0002
#define writeASSChannelConfigurationRequestStereoMono 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0003
#define writeASSChannelConfigurationRequest3Stereo 0xcc00+STD_BETA_ASS,0x0008,0x0108,0x0000
#define writeASSChannelConfigurationRequestPhantom 0xcc00+STD_BETA_ASS,0x0008,0x0105,0x0000
#define writeASSChannelConfigurationRequestSurround 0xcc00+STD_BETA_ASS,0x0008,0x010a,0x0000

#define writeASSChannelConfigurationRequestNone_0 0xcc00+STD_BETA_ASS,0x0008,0x0001,0x0000
#define writeASSChannelConfigurationRequestMono_0 0xcc00+STD_BETA_ASS,0x0008,0x0002,0x0000
#define writeASSChannelConfigurationRequestPhantom0_0 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0000
#define writeASSChannelConfigurationRequestPhantom0Stereo_0 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0001
#define writeASSChannelConfigurationRequestPhantom0LtRt_0 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0002
#define writeASSChannelConfigurationRequestPhantom0Mono_0 0xcc00+STD_BETA_ASS,0x0008,0x0003,0x0003
#define writeASSChannelConfigurationRequestPhantom1_0 0xcc00+STD_BETA_ASS,0x0008,0x0004,0x0000
#define writeASSChannelConfigurationRequestPhantom2_0 0xcc00+STD_BETA_ASS,0x0008,0x0005,0x0000
#define writeASSChannelConfigurationRequestPhantom2LtRt_0 0xcc00+STD_BETA_ASS,0x0008,0x0005,0x0002
#define writeASSChannelConfigurationRequestPhantom3_0 0xcc00+STD_BETA_ASS,0x0008,0x0006,0x0000
#define writeASSChannelConfigurationRequestPhantom4_0 0xcc00+STD_BETA_ASS,0x0008,0x0007,0x0000
#define writeASSChannelConfigurationRequestSurround0_0 0xcc00+STD_BETA_ASS,0x0008,0x0008,0x0000
#define writeASSChannelConfigurationRequestSurround1_0 0xcc00+STD_BETA_ASS,0x0008,0x0009,0x0000
#define writeASSChannelConfigurationRequestSurround2_0 0xcc00+STD_BETA_ASS,0x0008,0x000a,0x0000
#define writeASSChannelConfigurationRequestSurround2LtRt_0 0xcc00+STD_BETA_ASS,0x0008,0x000a,0x0002
#define writeASSChannelConfigurationRequestSurround3_0 0xcc00+STD_BETA_ASS,0x0008,0x000b,0x0000
#define writeASSChannelConfigurationRequestSurround4_0 0xcc00+STD_BETA_ASS,0x0008,0x000c,0x0000

#define writeASSChannelConfigurationRequestNone_1 0xcc00+STD_BETA_ASS,0x0008,0x0101,0x0000
#define writeASSChannelConfigurationRequestMono_1 0xcc00+STD_BETA_ASS,0x0008,0x0102,0x0000
#define writeASSChannelConfigurationRequestPhantom0_1 0xcc00+STD_BETA_ASS,0x0008,0x0103,0x0000
#define writeASSChannelConfigurationRequestPhantom0Stereo_1 0xcc00+STD_BETA_ASS,0x0008,0x0103,0x0001
#define writeASSChannelConfigurationRequestPhantom0LtRt_1 0xcc00+STD_BETA_ASS,0x0008,0x0103,0x0002
#define writeASSChannelConfigurationRequestPhantom0Mono_1 0xcc00+STD_BETA_ASS,0x0008,0x0103,0x0003
#define writeASSChannelConfigurationRequestPhantom1_1 0xcc00+STD_BETA_ASS,0x0008,0x0104,0x0000
#define writeASSChannelConfigurationRequestPhantom2_1 0xcc00+STD_BETA_ASS,0x0008,0x0105,0x0000
#define wroteASSChannelConfigurationRequestPhantom2Stereo_1 0xcc00+STD_BETA_ASS,0x0008,0x0105,0x0001
#define writeASSChannelConfigurationRequestPhantom2LtRt_1 0xcc00+STD_BETA_ASS,0x0008,0x0105,0x0002
#define writeASSChannelConfigurationRequestPhantom3_1 0xcc00+STD_BETA_ASS,0x0008,0x0106,0x0000
#define writeASSChannelConfigurationRequestPhantom4_1 0xcc00+STD_BETA_ASS,0x0008,0x0107,0x0000
#define writeASSChannelConfigurationRequestSurround0_1 0xcc00+STD_BETA_ASS,0x0008,0x0108,0x0000
#define writeASSChannelConfigurationRequestSurround1_1 0xcc00+STD_BETA_ASS,0x0008,0x0109,0x0000
#define writeASSChannelConfigurationRequestSurround2_1 0xcc00+STD_BETA_ASS,0x0008,0x010a,0x0000
#define writeASSChannelConfigurationRequestSurround2LtRt_1 0xcc00+STD_BETA_ASS,0x0008,0x010a,0x0002
#define writeASSChannelConfigurationRequestSurround3_1 0xcc00+STD_BETA_ASS,0x0008,0x010b,0x0000
#define writeASSChannelConfigurationRequestSurround4_1 0xcc00+STD_BETA_ASS,0x0008,0x010c,0x0000

#define writeASSChannelConfigurationRequestHL(HH,LL) 0xcc00+STD_BETA_ASS,0x0008,LL,HH

#define  readASSChannelConfigurationDownmix 0xc400+STD_BETA_ASS,0x000c
#define wroteASSChannelConfigurationDownmixNone 0xcc00+STD_BETA_ASS,0x000c,0x0001,0x0000
#define wroteASSChannelConfigurationDownmixMono 0xcc00+STD_BETA_ASS,0x000c,0x0002,0x0000
#define wroteASSChannelConfigurationDownmixStereo 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0000
#define wroteASSChannelConfigurationDownmixStereoLtRt 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0002
#define wroteASSChannelConfigurationDownmixStereoMono 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0003
#define wroteASSChannelConfigurationDownmix3Stereo 0xcc00+STD_BETA_ASS,0x000c,0x0108,0x0000
#define wroteASSChannelConfigurationDownmixPhantom 0xcc00+STD_BETA_ASS,0x000c,0x0105,0x0000
#define wroteASSChannelConfigurationDownmixSurround 0xcc00+STD_BETA_ASS,0x000c,0x010a,0x0000

#define wroteASSChannelConfigurationDownmixNone_0 0xcc00+STD_BETA_ASS,0x000c,0x0001,0x0000
#define wroteASSChannelConfigurationDownmixMono_0 0xcc00+STD_BETA_ASS,0x000c,0x0002,0x0000
#define wroteASSChannelConfigurationDownmixPhantom0_0 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0000
#define wroteASSChannelConfigurationDownmixPhantom0Stereo_0 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0001
#define wroteASSChannelConfigurationDownmixPhantom0LtRt_0 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0002
#define wroteASSChannelConfigurationDownmixPhantom0Mono_0 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0003
#define wroteASSChannelConfigurationDownmixPhantom0Dual_0 0xcc00+STD_BETA_ASS,0x000c,0x0003,0x0004
#define wroteASSChannelConfigurationDownmixPhantom1_0 0xcc00+STD_BETA_ASS,0x000c,0x0004,0x0000
#define wroteASSChannelConfigurationDownmixPhantom2_0 0xcc00+STD_BETA_ASS,0x000c,0x0005,0x0000
#define wroteASSChannelConfigurationDownmixPhantom2Stereo_0 0xcc00+STD_BETA_ASS,0x000c,0x0005,0x0001
#define wroteASSChannelConfigurationDownmixPhantom2LtRt_0 0xcc00+STD_BETA_ASS,0x000c,0x0005,0x0002
#define wroteASSChannelConfigurationDownmixPhantom2Mono_0 0xcc00+STD_BETA_ASS,0x000c,0x0005,0x0003
#define wroteASSChannelConfigurationDownmixPhantom3_0 0xcc00+STD_BETA_ASS,0x000c,0x0006,0x0000
#define wroteASSChannelConfigurationDownmixPhantom4_0 0xcc00+STD_BETA_ASS,0x000c,0x0007,0x0000
#define wroteASSChannelConfigurationDownmixSurround0_0 0xcc00+STD_BETA_ASS,0x000c,0x000c,0x0000
#define wroteASSChannelConfigurationDownmixSurround1_0 0xcc00+STD_BETA_ASS,0x000c,0x0009,0x0000
#define wroteASSChannelConfigurationDownmixSurround2_0 0xcc00+STD_BETA_ASS,0x000c,0x000a,0x0000
#define wroteASSChannelConfigurationDownmixSurround2Stereo_0 0xcc00+STD_BETA_ASS,0x000c,0x000a,0x0001
#define wroteASSChannelConfigurationDownmixSurround2LtRt_0 0xcc00+STD_BETA_ASS,0x000c,0x000a,0x0002
#define wroteASSChannelConfigurationDownmixSurround2Mono_0 0xcc00+STD_BETA_ASS,0x000c,0x000a,0x0003
#define wroteASSChannelConfigurationDownmixSurround3_0 0xcc00+STD_BETA_ASS,0x000c,0x000b,0x0000
#define wroteASSChannelConfigurationDownmixSurround4_0 0xcc00+STD_BETA_ASS,0x000c,0x000c,0x0000

#define wroteASSChannelConfigurationDownmixNone_1 0xcc00+STD_BETA_ASS,0x000c,0x0101,0x0000
#define wroteASSChannelConfigurationDownmixMono_1 0xcc00+STD_BETA_ASS,0x000c,0x0102,0x0000
#define wroteASSChannelConfigurationDownmixPhantom0_1 0xcc00+STD_BETA_ASS,0x000c,0x0103,0x0000
#define wroteASSChannelConfigurationDownmixPhantom0Stereo_1 0xcc00+STD_BETA_ASS,0x000c,0x0103,0x0001
#define wroteASSChannelConfigurationDownmixPhantom0LtRt_1 0xcc00+STD_BETA_ASS,0x000c,0x0103,0x0002
#define wroteASSChannelConfigurationDownmixPhantom0Mono_1 0xcc00+STD_BETA_ASS,0x000c,0x0103,0x0003
#define wroteASSChannelConfigurationDownmixPhantom0Dual_1 0xcc00+STD_BETA_ASS,0x000c,0x0103,0x0004
#define wroteASSChannelConfigurationDownmixPhantom1_1 0xcc00+STD_BETA_ASS,0x000c,0x0104,0x0000
#define wroteASSChannelConfigurationDownmixPhantom2_1 0xcc00+STD_BETA_ASS,0x000c,0x0105,0x0000
#define wroteASSChannelConfigurationDownmixPhantom2Stereo_1 0xcc00+STD_BETA_ASS,0x000c,0x0105,0x0001
#define wroteASSChannelConfigurationDownmixPhantom2LtRt_1 0xcc00+STD_BETA_ASS,0x000c,0x0105,0x0002
#define wroteASSChannelConfigurationDownmixPhantom2Mono_1 0xcc00+STD_BETA_ASS,0x000c,0x0105,0x0003
#define wroteASSChannelConfigurationDownmixPhantom3_1 0xcc00+STD_BETA_ASS,0x000c,0x0106,0x0000
#define wroteASSChannelConfigurationDownmixPhantom4_1 0xcc00+STD_BETA_ASS,0x000c,0x0107,0x0000
#define wroteASSChannelConfigurationDownmixSurround0_1 0xcc00+STD_BETA_ASS,0x000c,0x0108,0x0000
#define wroteASSChannelConfigurationDownmixSurround1_1 0xcc00+STD_BETA_ASS,0x000c,0x0109,0x0000
#define wroteASSChannelConfigurationDownmixSurround2_1 0xcc00+STD_BETA_ASS,0x000c,0x010a,0x0000
#define wroteASSChannelConfigurationDownmixSurround2Stereo_1 0xcc00+STD_BETA_ASS,0x000c,0x010a,0x0001
#define wroteASSChannelConfigurationDownmixSurround2Mono_1 0xcc00+STD_BETA_ASS,0x000c,0x010a,0x0003


/* newly added on top of ASS, Number 1 */

#define  readASSLFEDownmixVolume 0xc200+STD_BETA_ASS,0x0500
#define writeASSLFEDownmixVolumeN(NN) 0xca00+STD_BETA_ASS,0x0500+(0xff&(NN))
/* in support of inverse compilation only */
#define writeASSLFEDownmixVolumeN__20__ writeASSLFEDownmixVolumeN(20)

#define  readASSLFEDownmixInclude 0xc200+STD_BETA_ASS,0x0600
#define writeASSLFEDownmixIncludeNo 0xca00+STD_BETA_ASS,0x0600
#define writeASSLFEDownmixIncludeYes 0xca00+STD_BETA_ASS,0x0601

#define  readASSCenterMixLevel 0xc300+STD_BETA_ASS,0x0010
#define writeASSCenterMixLevelN(NN) 0xcb00+STD_BETA_ASS,0x0010,(0xFFFF&(NN))
#define wroteASSCenterMixLevel 0xcb00+STD_BETA_ASS,0x0010

#define  readASSSurroundMixLevel 0xc300+STD_BETA_ASS,0x0012
#define writeASSSurroundMixLevelN(NN) 0xcb00+STD_BETA_ASS,0x0012,(0xFFFF&(NN))
#define wroteASSSurroundMixLevel 0xcb00+STD_BETA_ASS,0x0012


#define  readASSStatus 0xc508,STD_BETA_ASS
#define  readASSControl \
         readASSMode, \
         readASSChannelConfigurationRequest, \
         readASSLFEDownmixVolume, \
         readASSLFEDownmixInclude, \
         readASSCenterMixLevel, \
         readASSSurroundMixLevel

#endif /* _ASS_A */

