
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Bass Peak Level Manager Alpha Codes
//
//

#ifndef _BC_A
#define _BC_A

#include <acpbeta.h>

#define  readBPLMMode 0xc200+STD_BETA_BC,0x0400
#define writeBPLMModeDisable 0xca00+STD_BETA_BC,0x0400
#define writeBPLMModeEnable 0xca00+STD_BETA_BC,0x0401

#define  readBPLMBypass         0xc200+STD_BETA_BC,0x0500
#define writeBPLMBypassEnable   0xca00+STD_BETA_BC,0x0500
#define writeBPLMBypassDisable  0xca00+STD_BETA_BC,0x0501

#define  readBPLMUse             readBPLMBypass
#define writeBPLMUseDisable     writeBPLMBypassEnable
#define writeBPLMUseEnable      writeBPLMBypassDisable

#define  readBPLMHardLimitN 0xc200+STD_BETA_BC,0x0600
#define writeBPLMHardLimitN(NN) 0xca00+STD_BETA_BC,0x0600+((NN)&0xff)


#define readBPLMSoftLimitQ7N       0xc200+STD_BETA_BC,0x0700
#define writeBPLMSoftLimitQ7N(NN)  0xca00+STD_BETA_BC,0x0700+((NN)&0xff)

#define  readBPLMMstVolumeN 0xc200+STD_BETA_BC,0x0800
#define writeBPLMMstVolumeN(NN) 0xca00+STD_BETA_BC,0x0800+((NN)&0xff)

#define readBPLMStatus 0xc508,STD_BETA_BC
#define readBPLMControl \
        readBPLMMode, \
        readBPLMBypass, \
        readBPLMHardLimitN, \
        readBPLMSoftLimitQ7N, \
        readBPLMMstVolumeN

// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define readBPLMSoftLimitQ7XX      readBPLMSoftLimitQ7N
#define writeBPLMSoftLimitQ7XX(N)  writeBPLMSoftLimitQ7N(0x##N)

#endif /* _BC_A */
