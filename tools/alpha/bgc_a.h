
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// Boundary Gain Compensation Algorithm alpha codes
//
//

#ifndef _BGC_A
#define _BGC_A

#include <acpbeta.h>

// Use Alpha Code Type 3 for 16 bit write/read
#define  readBGCMaskSelect 0xc300+STD_BETA_BGC,0x0c0a
#define writeBGCMaskSelect0 0xcb00+STD_BETA_BGC,0x0c0a,0x0000,0x0000,0x0000,0x0000    // 0 channels
#define writeBGCMaskSelectAll 0xcb00+STD_BETA_BGC, 0x0c0a,0xdff7,0x0000,0x0000,0x0000 // L,C,R,Ls,Lr,Lb,Rb,Lw,Rw,Lh,Rh channels

#define  readBGCUse 0xc300+STD_BETA_BGC,0x0008
#define writeBGCUseDisable 0xcb00+STD_BETA_BGC,0x0008,0x0
#define writeBGCUseEnable 0xcb00+STD_BETA_BGC,0x0008,0x0001

#define readBGCStatus 0xc508,STD_BETA_BGC
#define readBGCControl \
        readBGCMaskSelect, \
		readBGCUse

#endif /* _BGC_A */
