
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



#ifndef _DDP_A
#define _DDP_A
#include <stdbeta.h>

#define  readDDPMode 0xc200+STD_BETA_DDP,0x0400
#define  readDDPOperationalMode 0xc200+STD_BETA_DDP,0x0600
#define  readDDPDynrngScaleHiQ6 0xc200+STD_BETA_DDP,0x0b00
#define  readDDPDynrngScaleLoQ6 0xc200+STD_BETA_DDP,0x0c00
#define  readDDPSpeakerRemapping 0xc200+STD_BETA_DDP,0x0e00


#define writeDDPOperationalModeCustom0 0xca00+STD_BETA_DDP,0x0600
#define writeDDPOperationalModeCustom1 0xca00+STD_BETA_DDP,0x0601
#define writeDDPOperationalModeLine 0xca00+STD_BETA_DDP,0x0602
#define writeDDPOperationalModeRF 0xca00+STD_BETA_DDP,0x0603

#define readDDPLfe 0xc200+STD_BETA_DDP,0x0700
#define writeDDPLfeDwnmixInclude 0xca00+STD_BETA_DDP,0x0700
// to be used with stereo downmix and does lfe downmix to stereo
#define writeDDPLfeOnDefault 0xca00+STD_BETA_DDP,0x0701
// this should be the default - the decoder must always generate LFE and let the system decide what to do with it.
#define writeDDPLfe2 0xca00+STD_BETA_DDP,0x0702
// this generates the second LFE also - currently dont know how to connect speakers for second LFE

#define writeDDPDynrngScaleHiQ6N(NN)  0xca00+STD_BETA_DDP,0x0b00+((NN)&0xff)
#define writeDDPDynrngScaleLoQ6N(NN)  0xca00+STD_BETA_DDP,0x0c00+((NN)&0xff)
/* in support of inverse compilation only */
#define writeDDPDynrngScaleHiQ6N__64__ writeDDPDynrngScaleHiQ6N(64)
#define writeDDPDynrngScaleLoQ6N__64__ writeDDPDynrngScaleLoQ6N(64)

// currently only mapping to PA Surround4 i.e. Lb,Rb is supported
#define writeDDPSpeakerRemappingOff 0xca00+STD_BETA_DDP,0x0e00
#define writeDDPSpeakerRemappingOn 0xca00+STD_BETA_DDP,0x0e01

#define readDDPRemappingScaleFactor 0xc300+STD_BETA_DDP,0x0010

/* Ensure that DECChannelConfigurationRequest.part.sat is set to PAF_CC_SAT_UNKNOWN before using these options */

#define readDDPExtDownmixMode 0xc200+STD_BETA_DDP,0x0800
#define writeDDPExtDownmixRaw 0xca00+STD_BETA_DDP,0x08ff
#define writeDDPExtDownmixMode8 0xca00+STD_BETA_DDP,0x0808
/* 8  - L, C, R, Cvh */
#define writeDDPExtDownmixMode9 0xca00+STD_BETA_DDP,0x0809
/* 9  - L, R, l, r, Ts */
#define writeDDPExtDownmixMode10 0xca00+STD_BETA_DDP,0x080a
/* 10 - L, C, R, l, r, Ts */
#define writeDDPExtDownmixMode11 0xca00+STD_BETA_DDP,0x080b
 /* 11 - L, C, R, l, r, Cvh */
#define writeDDPExtDownmixMode12 0xca00+STD_BETA_DDP,0x080c
/* 12 - L, C, R, Lc, Rc */
#define writeDDPExtDownmixMode13 0xca00+STD_BETA_DDP,0x080d
/* 13 - L, R, l, r, Lw, Rw */
#define writeDDPExtDownmixMode14 0xca00+STD_BETA_DDP,0x080e
/* 14 - L, R, l, r, Lvh, Rvh */
#define writeDDPExtDownmixMode15 0xca00+STD_BETA_DDP,0x080f
/* 15 - L, R, l, r, Lsd, Rsd */
#define writeDDPExtDownmixMode16 0xca00+STD_BETA_DDP,0x0810
/* 16 - L, R, l, r, Lrs, Rrs */
#define writeDDPExtDownmixMode17 0xca00+STD_BETA_DDP,0x0811
 /* 17 - L, C, R, l, r, Lc, Rc */
#define writeDDPExtDownmixMode18 0xca00+STD_BETA_DDP,0x0812
/* 18 - L, C, R, l, r, Lw, Rw */
#define writeDDPExtDownmixMode19 0xca00+STD_BETA_DDP,0x0813
/* 19 - L, C, R, l, r, Lvh, Rvh */
#define writeDDPExtDownmixMode20 0xca00+STD_BETA_DDP,0x0814
/* 20 - L, C, R, l, r, Lsd, Rsd */
#define writeDDPExtDownmixMode21 0xca00+STD_BETA_DDP,0x0815
/* 21 - L, C, R, l, r, Lrs, Rrs */
#define writeDDPExtDownmixMode22 0xca00+STD_BETA_DDP,0x0816
 /* 22 - L, C, R, l, r, Ts, Cvh */


#define readDDPStereoMode 0xc200+STD_BETA_DDP,0x0900
#define writeDDPStereoModeAuto 0xca00+STD_BETA_DDP,0x0900
#define writeDDPStereoModeLtRt 0xca00+STD_BETA_DDP,0x0901
#define writeDDPStereoModeLoRo 0xca00+STD_BETA_DDP,0x0902
#define writeDDPStereoModeAutoLoRo 0xca00+STD_BETA_DDP,0x0903

#define readDDPChanInfo 0xc300+STD_BETA_DDP,0x0012

#define readDDPDlgNorm 0xc200+STD_BETA_DDP,0x0f00
#define writeDDPDlgNormAuto 0xca00+STD_BETA_DDP,0x0f00
#define writeDDPDlgNormEnable 0xca00+STD_BETA_DDP,0x0f01
#define writeDDPDlgNormDisable 0xca00+STD_BETA_DDP,0x0f02

#define readDDPRFModeGain 0xc200+STD_BETA_DDP,0x0500
#define writeDDPRFModeGainAuto 0xca00+STD_BETA_DDP,0x0500
#define writeDDPRFModeGainEnable 0xca00+STD_BETA_DDP,0x0501
#define writeDDPRFModeGainDisable 0xca00+STD_BETA_DDP,0x0502



#define  readDDPBitStreamInformation 0xc600+STD_BETA_DDP2,0x0424
#define  readDDPBitStreamInformation0 0xc300+STD_BETA_DDP2,0x0004
#define  readDDPBitStreamInformationAcmodBsid 0xc300+STD_BETA_DDP2,0x0006
#define  readDDPBitStreamInformation1 readDDPBitStreamInformationAcmodBsid
#define  readDDPBitStreamInformationBsmodCmixLev 0xc300+STD_BETA_DDP2,0x0008
#define  readDDPBitStreamInformation2 readDDPBitStreamInformationBsmodCmixLev 
#define  readDDPBitStreamInformationComprCopyrightB 0xc300+STD_BETA_DDP2,0x000a
#define  readDDPBitStreamInformation3 readDDPBitStreamInformationComprCopyrightB 
#define  readDDPBitStreamInformationDHeadPhonmodDialNorm 0xc300+STD_BETA_DDP2,0x000c
#define  readDDPBitStreamInformation4 readDDPBitStreamInformationDHeadPhonmodDialNorm 
#define  readDDPBitStreamInformationDmixModDSurExMod 0xc300+STD_BETA_DDP2,0x000e
#define  readDDPBitStreamInformation5 readDDPBitStreamInformationDmixModDSurExMod 
#define  readDDPBitStreamInformationDSurModFrmSizCod 0xc300+STD_BETA_DDP2,0x0010
#define  readDDPBitStreamInformation6 readDDPBitStreamInformationDSurModFrmSizCod 
#define  readDDPBitStreamInformationFrmSize 0xc300+STD_BETA_DDP2,0x0012
#define  readDDPBitStreamInformation7 readDDPBitStreamInformationFrmSize 
#define  readDDPBitStreamInformationLfeOnLfeMixLevCod 0xc300+STD_BETA_DDP2,0x0014
#define  readDDPBitStreamInformation8 readDDPBitStreamInformationLfeOnLfeMixLevCod 
#define  readDDPBitStreamInformationLoRoCMixLevLoRoSurMixLev 0xc300+STD_BETA_DDP2,0x0016
#define  readDDPBitStreamInformation9 readDDPBitStreamInformationLoRoCMixLevLoRoSurMixLev 
#define  readDDPBitStreamInformationLtRtCmixLevLtRtSurMixLev 0xc300+STD_BETA_DDP2,0x0018
#define  readDDPBitStreamInformation10 readDDPBitStreamInformationLtRtCmixLevLtRtSurMixLev 
#define  readDDPBitStreamInformationMixLevOrigBs 0xc300+STD_BETA_DDP2,0x001a
#define  readDDPBitStreamInformation11 readDDPBitStreamInformationMixLevOrigBs 
#define  readDDPBitStreamInformationRoomTypSourceFsCod 0xc300+STD_BETA_DDP2,0x001c
#define  readDDPBitStreamInformation12 readDDPBitStreamInformationRoomTypSourceFsCod 
#define  readDDPBitStreamInformationStrmTypeSubstreamId 0xc300+STD_BETA_DDP2,0x01e
#define  readDDPBitStreamInformation13 readDDPBitStreamInformationStrmTypeSubstreamId 
#define  readDDPBitStreamInformationSurMixLevFsCod 0xc300+STD_BETA_DDP2,0x0020
#define  readDDPBitStreamInformation14 readDDPBitStreamInformationSurMixLevFsCod 
#define  readDDPBitStreamInformationMixExtProgProgAdjust 0xc300+STD_BETA_DDP2,0x0022
#define  readDDPBitStreamInformation15 readDDPBitStreamInformationMixExtProgProgAdjust 
#define  readDDPBitStreamInformationAtoDConverterType 0xc300+STD_BETA_DDP2,0x0024
#define  readDDPBitStreamInformation16 readDDPBitStreamInformationAtoDConverterType 
#define  readDDPBitStreamInformationChanMap 0xc300+STD_BETA_DDP2,0x0026
#define  readDDPBitStreamInformation17 readDDPBitStreamInformationChanMap 

#define  readDDPStatus 0xc508,STD_BETA_DDP
#define  readDDPCommon 0xc508,STD_BETA_DDP2
#define  readDDPControl \
         readDDPOperationalMode, \
         readDDPDynrngScaleHiQ6, \
         readDDPDynrngScaleLoQ6, \
         readDDPStereoMode, \
         readDDPSpeakerRemapping, \
         readDDPLfe, \
         readDDPExtDownmixMode, \
         readDDPDlgNorm, \
         readDDPRFModeGain


#define  readAC3Mode readDDPMode
#define  readAC3OperationalMode readDDPOperationalMode
#define  readAC3DynrngScaleHiQ6 readDDPDynrngScaleHiQ6
#define  readAC3DynrngScaleLoQ6 readDDPDynrngScaleLoQ6

#define writeAC3OperationalModeCustom0 writeDDPOperationalModeCustom0
#define writeAC3OperationalModeCustom1 writeDDPOperationalModeCustom1
#define writeAC3OperationalModeLine writeDDPOperationalModeLine
#define writeAC3OperationalModeRF writeDDPOperationalModeRF

#define writeAC3DynrngScaleHiQ6N(NN)  writeDDPDynrngScaleHiQ6N(NN)
#define writeAC3DynrngScaleLoQ6N(NN)  writeDDPDynrngScaleLoQ6N(NN)
/* in support of inverse compilation only */
#define writeAC3DynrngScaleHiQ6N__64__ writeAC3DynrngScaleHiQ6N(64)
#define writeAC3DynrngScaleLoQ6N__64__ writeAC3DynrngScaleLoQ6N(64)

#define readAC3StereoMode readDDPStereoMode
#define writeAC3StereoModeAuto writeDDPStereoModeAuto
#define writeAC3StereoModeLtRt writeDDPStereoModeLtRt
#define writeAC3StereoModeLoRo writeDDPStereoModeLoRo
#define writeAC3StereoModeAutoLoRo writeDDPStereoModeAutoLoRo

#define  readAC3BitStreamInformation readDDPBitStreamInformation
#define  readAC3BitStreamInformation0 readDDPBitStreamInformation0
#define  readAC3BitStreamInformation1 readDDPBitStreamInformation1
#define  readAC3BitStreamInformation2 readDDPBitStreamInformation2
#define  readAC3BitStreamInformation3 readDDPBitStreamInformation3
#define  readAC3BitStreamInformation4 readDDPBitStreamInformation4
#define  readAC3BitStreamInformation5 readDDPBitStreamInformation5
#define  readAC3BitStreamInformation6 readDDPBitStreamInformation6
#define  readAC3BitStreamInformation7 readDDPBitStreamInformation7
#define  readAC3BitStreamInformation8 readDDPBitStreamInformation8
#define  readAC3BitStreamInformation9 readDDPBitStreamInformation9

/* Currently not available in DDP as AC3
#define wroteAC3BitStreamInformation 0xce00+STD_BETA_AC32,0x0414
*/
#define  readAC3Status readDDPStatus
#define  readAC3Common readDDPCommon
#define  readAC3Control readDDPControl

#define readAC3RFModeGain readDDPRFModeGain
#define writeAC3RFModeGainAuto writeDDPRFModeGainAuto
#define writeAC3RFModeGainEnable writeDDPRFModeGainEnable
#define writeAC3RFModeGainDisable writeDDPRFModeGainDisable

#define readAC3DlgNorm readDDPDlgNorm
#define writeAC3DlgNormAuto writeDDPDlgNormAuto
#define writeAC3DlgNormEnable writeDDPDlgNormEnable
#define writeAC3DlgNormDisable writeDDPDlgNormDisable


// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define writeAC3DynrngScaleHiQ6XX(N)  writeAC3DynrngScaleHiQ6N(0x##N)
#define writeAC3DynrngScaleLoQ6XX(N)  writeAC3DynrngScaleLoQ6N(0x##N)


#endif /* _DDP_A */
