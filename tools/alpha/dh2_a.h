
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DH2 Algorithm alpha codes
//
//
//

#ifndef _DH2_A
#define _DH2_A

#include <acpbeta.h>

#define  readDH2Mode                0xc200+(STD_BETA_DH2),0x0400
#define writeDH2ModeDisable         0xca00+(STD_BETA_DH2),0x0400
#define writeDH2ModeEnable          0xca00+(STD_BETA_DH2),0x0401

#define  readDH2Bypass              0xc200+(STD_BETA_DH2),0x0500
#define writeDH2BypassEnable        0xca00+(STD_BETA_DH2),0x0500
#define writeDH2BypassDisable       0xca00+(STD_BETA_DH2),0x0501

#define  readDH2Use                 readDH2Bypass
#define writeDH2UseDisable          writeDH2BypassEnable
#define writeDH2UseEnable           writeDH2BypassDisable

#define  readDH2LfeMix              0xc200+(STD_BETA_DH2),0x0600
#define writeDH2LfeMixDisable       0xca00+(STD_BETA_DH2),0x0600
#define writeDH2LfeMixEnable        0xca00+(STD_BETA_DH2),0x0601

#define  readDH2OutputLevel         0xc200+(STD_BETA_DH2),0x0700
#define writeDH2OutputLevel6dbdown  0xca00+(STD_BETA_DH2),0x0700
#define writeDH2OutputLevel0db      0xca00+(STD_BETA_DH2),0x0701


#define  readDH2Status 0xc508,0x0000+(STD_BETA_DH2)
#define  readDH2Control \
         readDH2Mode, \
         readDH2Bypass, \
         readDH2LfeMix, \
         readDH2OutputLevel

#endif /* _DH2_A */

