
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DTSHD Decoder alpha codes
//
//
//

#ifndef _DTSHD_A
#define _DTSHD_A

#include <acpbeta.h>

#define  readDTSHDDecodeMode 					0xc200+STD_BETA_DTSHD,0x0400
#define writeDTSHDDecodeModeDigitalSurround 	0xca00+STD_BETA_DTSHD,0x0400
#define writeDTSHDDecodeModeCoreSS 				0xca00+STD_BETA_DTSHD,0x0401
#define writeDTSHDDecodeModeLossy 				0xca00+STD_BETA_DTSHD,0x0402
#define writeDTSHDDecodeModeXLL 				0xca00+STD_BETA_DTSHD,0x0404

#define  readDTSHD9624Mode 						0xc200+STD_BETA_DTSHD,0x0600
#define writeDTSHD9624ModeDisable 				0xca00+STD_BETA_DTSHD,0x0600
#define writeDTSHD9624ModeEnable 				0xca00+STD_BETA_DTSHD,0x0601

#define  readDTSHDDynrngScaleQ6 				0xc200+STD_BETA_DTSHD,0x0d00
#define writeDTSHDDynrngScaleQ6N(NN)  			0xca00+STD_BETA_DTSHD,0x0d00+((NN)&0xFF)

#define  readDTSHDDialNormMode 					0xc200+STD_BETA_DTSHD,0x1000
#define writeDTSHDDialNormModeDisable 			0xca00+STD_BETA_DTSHD,0x1000
#define writeDTSHDDialNormModeEnable 			0xca00+STD_BETA_DTSHD,0x1001

#define  readDTSHDVersionChkMode  				0xc200+STD_BETA_DTSHD,0x1100
#define writeDTSHDVersionChkDisable 			0xca00+STD_BETA_DTSHD,0x1100
#define writeDTSHDVersionChkEnable 				0xca00+STD_BETA_DTSHD,0x1101

#define  readDTSHDDownMixMode 					0xc200+STD_BETA_DTSHD,0x1200
#define writeDTSHDDownMixModeDisable 			0xca00+STD_BETA_DTSHD,0x1200
#define writeDTSHDDownMixModeEnable 			0xca00+STD_BETA_DTSHD,0x1201

#define  readDTSHDXBRMode 						0xc200+STD_BETA_DTSHD,0x1A00
#define writeDTSHDXBRModeDisable 				0xca00+STD_BETA_DTSHD,0x1A00
#define writeDTSHDXBRModeEnable 				0xca00+STD_BETA_DTSHD,0x1A01

#define  readDTSHDXXCHMode 						0xc200+STD_BETA_DTSHD,0x1B00
#define writeDTSHDXXCHModeDisable 				0xca00+STD_BETA_DTSHD,0x1B00
#define writeDTSHDXXCHModeEnable 				0xca00+STD_BETA_DTSHD,0x1B01

#define  readDTSHDXLLMode 						0xc200+STD_BETA_DTSHD,0x1C00
#define writeDTSHDXLLModeDisable 				0xca00+STD_BETA_DTSHD,0x1C00
#define writeDTSHDXLLModeEnable 				0xca00+STD_BETA_DTSHD,0x1C01

#define  readDTSHDSpeakerRemapMode 				0xc200+STD_BETA_DTSHD,0x1D00
#define writeDTSHDSpeakerRemapModeDisable 		0xca00+STD_BETA_DTSHD,0x1D00
#define writeDTSHDSpeakerRemapModeEnable 		0xca00+STD_BETA_DTSHD,0x1D01

#define  readDTSHDDRCMode 						0xc200+STD_BETA_DTSHD,0x1E00
#define writeDTSHDDRCModeDisable 				0xca00+STD_BETA_DTSHD,0x1E00
#define writeDTSHDDRCModeEnable 				0xca00+STD_BETA_DTSHD,0x1E01

#define  readDTSHDAudioPresentationIndex 		0xc200+STD_BETA_DTSHD,0x1F00
#define writeDTSHDAudioPresentationIndex(NN) 	0xca00+STD_BETA_DTSHD,0x1F00+((NN)&0xFF)

#define  readDTSHDSoundFieldConfigIndex 		0xc200+STD_BETA_DTSHD,0x2000
#define writeDTSHDSoundFieldConfigIndex(NN) 	0xca00+STD_BETA_DTSHD,0x2000+((NN)&0xFF)

#define  readDTSHDLFEMix 						0xc200+STD_BETA_DTSHD,0x2100
#define writeDTSHDLFEMixDisable 				0xca00+STD_BETA_DTSHD,0x2100
#define writeDTSHDLFEMixEnable 					0xca00+STD_BETA_DTSHD,0x2101
//Include LFE channel for decoder downmix
#define writeDTSHDLFEMixDisableOn writeDTSHDLFEMixDisable
//The decoder will generate LFE and let the system decide what to do with it.
#define writeDTSHDLFEMixDisableOff 0xca00+STD_BETA_DTSHD,0x2102
//The decoder will not generate LFE when it is not requested

#define   readDTSHDListeningSpace     		 	0xc200+STD_BETA_DTSHD,0x2200
#define  writeDTSHDListeningSpaceAuto 		 	0xca00+STD_BETA_DTSHD,0x2200
#define  writeDTSHDListeningSpace51  		 	0xca00+STD_BETA_DTSHD,0x2201
#define  writeDTSHDListeningSpace61   		 	0xca00+STD_BETA_DTSHD,0x2202
#define  writeDTSHDListeningSpace71Config1  	0xca00+STD_BETA_DTSHD,0x2203
#define  writeDTSHDListeningSpace71Config2  	0xca00+STD_BETA_DTSHD,0x2204
#define  writeDTSHDListeningSpace71Config3  	0xca00+STD_BETA_DTSHD,0x2205
#define  writeDTSHDListeningSpace71Config4  	0xca00+STD_BETA_DTSHD,0x2206
#define  writeDTSHDListeningSpace71Config5  	0xca00+STD_BETA_DTSHD,0x2207
#define  writeDTSHDListeningSpace71Config6  	0xca00+STD_BETA_DTSHD,0x2208
#define  writeDTSHDListeningSpace71Config7  	0xca00+STD_BETA_DTSHD,0x2209

#define  readDTSHDEMBSpeakerRemapMode		 	0xc200+STD_BETA_DTSHD,0x2300
#define writeDTSHDEMBSpeakerRemapModeDisable 	0xca00+STD_BETA_DTSHD,0x2300
#define writeDTSHDEMBSpeakerRemapModeEnable  	0xca00+STD_BETA_DTSHD,0x2301

#define  readDTSHDEMBDownMixMode 				0xc200+STD_BETA_DTSHD,0x2400
#define writeDTSHDEMBDownMixModeDisable	 		0xca00+STD_BETA_DTSHD,0x2400
#define writeDTSHDEMBDownMixModeEnable  		0xca00+STD_BETA_DTSHD,0x2401

#define  readDTSHDSoftLimiterMode 		 		0xc200+STD_BETA_DTSHD,0x2500
#define writeDTSHDSoftLimiterModeDisable 		0xca00+STD_BETA_DTSHD,0x2500
#define writeDTSHDSoftLimiterModeEnable  		0xca00+STD_BETA_DTSHD,0x2501

#define  readDTSHDForce96For192StrmMode  		0xc200+STD_BETA_DTSHD,0x0E00
#define writeDTSHDForce96For192StrmDisable 		0xca00+STD_BETA_DTSHD,0x0E00
#define writeDTSHDForce96For192StrmEnable  		0xca00+STD_BETA_DTSHD,0x0E01
#define writeDTSHDForce96For192Strm2ChDisable 	0xca00+STD_BETA_DTSHD,0x0E02
#define writeDTSHDForce96For192Strm6ChDisable 	0xca00+STD_BETA_DTSHD,0x0E03

#define  readDTSHD51Mode 				 		0xc200+STD_BETA_DTSHD,0x2600
#define writeDTSHD51EnhancedMode		 		0xca00+STD_BETA_DTSHD,0x2600
#define writeDTSHD51StandardMode		 		0xca00+STD_BETA_DTSHD,0x2601

#define  readDTSHDRouteCsMode 	 				0xc200+STD_BETA_DTSHD,0x2700
#define writeDTSHDRouteCsDisable		 		0xca00+STD_BETA_DTSHD,0x2700
#define writeDTSHDRouteCsEnable  		 		0xca00+STD_BETA_DTSHD,0x2701

#define  readDTSHDFadeInMode 	 			0xc200+STD_BETA_DTSHD,0x2800
#define writeDTSHDFadeInDisable			 	0xca00+STD_BETA_DTSHD,0x2800
#define writeDTSHDFadeInRamp	  		 	0xca00+STD_BETA_DTSHD,0x2801
#define writeDTSHDFadeInMute	  		 	0xca00+STD_BETA_DTSHD,0x2802

#define  readDTSHDMatrixMode 					0xc200+STD_BETA_DTSHD,0x2900
#define writeDTSHDMatrixModeDisable 			0xca00+STD_BETA_DTSHD,0x2900
#define writeDTSHDMatrixModeEnable 				0xca00+STD_BETA_DTSHD,0x2901

#define  readDTSHDNEOInternalGain				0xc200+STD_BETA_DTSHD,0x2A00
#define writeDTSHDNEOInternalGainDisable 		0xca00+STD_BETA_DTSHD,0x2A00
#define writeDTSHDNEOInternalGainEnable 		0xca00+STD_BETA_DTSHD,0x2A01


#define  readDTSHDBitStreamInformation 			0xc600+STD_BETA_DTSHD2,0x0450
#define  readDTSHDBitStreamInformation0 		0xc300+STD_BETA_DTSHD2,0x0004
#define  readDTSHDBitStreamInformation1 		0xc300+STD_BETA_DTSHD2,0x0006
#define  readDTSHDBSIAMODE 						0xc300+STD_BETA_DTSHD2,0x0006
#define  readDTSHDBitStreamInformation2 		0xc300+STD_BETA_DTSHD2,0x0008
#define  readDTSHDBSISFREQ 						0xc300+STD_BETA_DTSHD2,0x0008
#define  readDTSHDBitStreamInformation3 		0xc300+STD_BETA_DTSHD2,0x000a
#define  readDTSHDBSIRATE 						0xc300+STD_BETA_DTSHD2,0x000a
#define  readDTSHDBitStreamInformation4 		0xc300+STD_BETA_DTSHD2,0x000c
#define  readDTSHDBSIDYNF 						0xc300+STD_BETA_DTSHD2,0x000c
#define  readDTSHDBitStreamInformation5 		0xc300+STD_BETA_DTSHD2,0x000e
#define  readDTSHDBSIRANGE 						0xc300+STD_BETA_DTSHD2,0x000e
#define  readDTSHDBitStreamInformation6 		0xc300+STD_BETA_DTSHD2,0x0010
#define  readDTSHDBSIHDCD 						0xc300+STD_BETA_DTSHD2,0x0010
#define  readDTSHDBitStreamInformation7 		0xc300+STD_BETA_DTSHD2,0x0012
#define  readDTSHDBSIEXT_AUDIO 					0xc300+STD_BETA_DTSHD2,0x0012
#define  readDTSHDBitStreamInformation8 		0xc300+STD_BETA_DTSHD2,0x0014
#define  readDTSHDBSILFF 						0xc300+STD_BETA_DTSHD2,0x0014
#define  readDTSHDBitStreamInformation9 		0xc300+STD_BETA_DTSHD2,0x0016
#define  readDTSHDBSIPCMR 						0xc300+STD_BETA_DTSHD2,0x0016
#define  readDTSHDBitStreamInformation10 		0xc300+STD_BETA_DTSHD2,0x0018
#define  readDTSHDBSIFSIZE 						0xc300+STD_BETA_DTSHD2,0x0018
#define  readDTSHDBitStreamInformation11 		0xc300+STD_BETA_DTSHD2,0x001a
#define  readDTSHDBSIEXT_AUDIO_ID 				0xc300+STD_BETA_DTSHD2,0x001a
#define  readDTSHDBitStreamInformation12 		0xc300+STD_BETA_DTSHD2,0x001c
#define  readDTSHDBSIASPF 						0xc300+STD_BETA_DTSHD2,0x001c
#define  readDTSHDBitStreamInformation13 		0xc300+STD_BETA_DTSHD2,0x001e
#define  readDTSHDBSIVERNUM 					0xc300+STD_BETA_DTSHD2,0x001e
#define  readDTSHDBitStreamInformation14 		0xc300+STD_BETA_DTSHD2,0x0020
#define  readDTSHDBSIREVNO 						0xc300+STD_BETA_DTSHD2,0x0020
#define  readDTSHDBitStreamInformation15 		0xc300+STD_BETA_DTSHD2,0x0022
#define  readDTSHDBSIXCHFLAG 					0xc300+STD_BETA_DTSHD2,0x0022
#define  readDTSHDBitStreamInformation16 		0xc300+STD_BETA_DTSHD2,0x0024
#define  readDTSHDBSIX96FLAG 					0xc300+STD_BETA_DTSHD2,0x0024
#define  readDTSHDBitStreamInformation17 		0xc300+STD_BETA_DTSHD2,0x0026
#define  readDTSHDBSIXCHFSIZE 					0xc300+STD_BETA_DTSHD2,0x0026
#define  readDTSHDBitStreamInformation18 		0xc300+STD_BETA_DTSHD2,0x0028
#define  readDTSHDBSIX96FSIZE 					0xc300+STD_BETA_DTSHD2,0x0028
#define  readDTSHDBitStreamInformation19 		0xc300+STD_BETA_DTSHD2,0x002a
#define  readDTSHDBSIDIALNORM 					0xc300+STD_BETA_DTSHD2,0x002a
#define  readDTSHDBitStreamInformation20 		0xc300+STD_BETA_DTSHD2,0x002c
#define  readDTSHDBSIEXTSTREAM 					0xc300+STD_BETA_DTSHD2,0x002c
#define  readDTSHDBitStreamInformation21 		0xc300+STD_BETA_DTSHD2,0x002e
#define  readDTSHDBSIEXTSTREAMHSIZE 			0xc300+STD_BETA_DTSHD2,0x002e
#define  readDTSHDBitStreamInformation22 		0xc300+STD_BETA_DTSHD2,0x0030
#define  readDTSHDBSIEXTSTREAMFSIZE 			0xc300+STD_BETA_DTSHD2,0x0030
#define  readDTSHDBitStreamInformation23 		0xc300+STD_BETA_DTSHD2,0x0032
#define  readDTSHDBSINumAudioPresentation 		0xc300+STD_BETA_DTSHD2,0x0032
#define  readDTSHDBitStreamInformation24 		0xc300+STD_BETA_DTSHD2,0x0034
#define  readDTSHDBSINumAssets 					0xc300+STD_BETA_DTSHD2,0x0034
#define  readDTSHDBitStreamInformation25 		0xc300+STD_BETA_DTSHD2,0x0036
#define  readDTSHDBSICoreExtensionMask 			0xc300+STD_BETA_DTSHD2,0x0036
#define  readDTSHDBitStreamInformation26 		0xc300+STD_BETA_DTSHD2,0x0038
#define  readDTSHDBSIrScale 					0xc300+STD_BETA_DTSHD2,0x0038
#define  readDTSHDBitStreamInformation27 		0xc300+STD_BETA_DTSHD2,0x003A
#define  readDTSHDBSINumSoundFieldConfig 		0xc300+STD_BETA_DTSHD2,0x003A
#define  readDTSHDBitStreamInformation28 		0xc400+STD_BETA_DTSHD2,0x0040
#define  readDTSHDBSIDecodeChannelMask 			0xc400+STD_BETA_DTSHD2,0x0040
#define  readDTSHDDecodeChannelMask  			readDTSHDBSIDecodeChannelMask //For backward compatability
#define  readDTSHDBitStreamInformation29 		0xc400+STD_BETA_DTSHD2,0x0044
#define  readDTSHDBSIProgramChannelMask 		0xc400+STD_BETA_DTSHD2,0x0044
#define  readDTSHDBitStreamInformation30 		0xc300+STD_BETA_DTSHD2,0x0048
#define  readDTSHDBSIXLLSampleRate 				0xc300+STD_BETA_DTSHD2,0x0048
#define  readDTSHDBitStreamInformation31 		0xc300+STD_BETA_DTSHD2,0x004A
#define  readDTSHDBSIEMBDownMix  				0xc300+STD_BETA_DTSHD2,0x004A
#define  readDTSHDBitStreamInformation32 		0xc300+STD_BETA_DTSHD2,0x004C
#define  readDTSHDBSISoftLimiterScale  			0xc300+STD_BETA_DTSHD2,0x004C
#define  readDTSHDBitStreamInformation33 		0xc300+STD_BETA_DTSHD2,0x004E
#define  readDTSHDBSIEmbDwnMxEmbCoefInfo 		0xc300+STD_BETA_DTSHD2,0x004E
#define  readDTSHDBitStreamInformation34 		0xc300+STD_BETA_DTSHD2,0x0050
#define  readDTSHDBSICoreSyncInfo 				0xc300+STD_BETA_DTSHD2,0x0050
#define  readDTSHDBitStreamInformation35 		0xc300+STD_BETA_DTSHD2,0x0052
#define  readDTSHDBSIEMBSpkrRemapMask			0xc300+STD_BETA_DTSHD2,0x0052

#define wroteDTSHDBitStreamInformation 			0xce00+STD_BETA_DTSHD2,0x044E

// AMODE
#define wroteDTSHDAMODE 						0xcb00+STD_BETA_DTSHD2,0x0006
// SFREQ
#define wroteDTSHDSFREQ8kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x0001
#define wroteDTSHDSFREQ16kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x0002
#define wroteDTSHDSFREQ32kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x0003
#define wroteDTSHDSFREQ11_025kHz 				0xcb00+STD_BETA_DTSHD2,0x0008,0x0006
#define wroteDTSHDSFREQ22_05kHz 				0xcb00+STD_BETA_DTSHD2,0x0008,0x0007
#define wroteDTSHDSFREQ44_1kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x0008
#define wroteDTSHDSFREQ12kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x000b
#define wroteDTSHDSFREQ24kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x000c
#define wroteDTSHDSFREQ48kHz 					0xcb00+STD_BETA_DTSHD2,0x0008,0x000d
#define wroteDTSHDSFREQInvalid 					0xcb00+STD_BETA_DTSHD2,0x0008
//Bit rate
#define wroteDTSHDBitRate32kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0000
#define wroteDTSHDBitRate56kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0001
#define wroteDTSHDBitRate64kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0002
#define wroteDTSHDBitRate96kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0003
#define wroteDTSHDBitRate112kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0004
#define wroteDTSHDBitRate128kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0005
#define wroteDTSHDBitRate192kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0006
#define wroteDTSHDBitRate224kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0007
#define wroteDTSHDBitRate256kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0008
#define wroteDTSHDBitRate320kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0009
#define wroteDTSHDBitRate384kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000a
#define wroteDTSHDBitRate448kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000b
#define wroteDTSHDBitRate512kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000c
#define wroteDTSHDBitRate576kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000d
#define wroteDTSHDBitRate640kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000e
#define wroteDTSHDBitRate768kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x000f
#define wroteDTSHDBitRate960kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0010
#define wroteDTSHDBitRate1024kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0011
#define wroteDTSHDBitRate1152kbps				0xcb00+STD_BETA_DTSHD2,0x000a,0x0012
#define wroteDTSHDBitRate1280kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0013
#define wroteDTSHDBitRate1344kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0014
#define wroteDTSHDBitRate1408kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0015
#define wroteDTSHDBitRate1411_2kbps 			0xcb00+STD_BETA_DTSHD2,0x000a,0x0016
#define wroteDTSHDBitRate1472kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0017
#define wroteDTSHDBitRate1536kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0018
#define wroteDTSHDBitRate1920kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x0019
#define wroteDTSHDBitRate2048kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x001a
#define wroteDTSHDBitRate3072kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x001b
#define wroteDTSHDBitRate3840kbps 				0xcb00+STD_BETA_DTSHD2,0x000a,0x001c
#define wroteDTSHDBitRateOpen 					0xcb00+STD_BETA_DTSHD2,0x000a,0x001d
#define wroteDTSHDBitRateVariable 				0xcb00+STD_BETA_DTSHD2,0x000a,0x001e
#define wroteDTSHDBitRateLossless 				0xcb00+STD_BETA_DTSHD2,0x000a,0x001f
// DRC
#define wroteDTSHDDRCOFF 						0xcb00+STD_BETA_DTSHD2,0x000c,0x0000
#define wroteDTSHDDRCON 						0xcb00+STD_BETA_DTSHD2,0x000c,0x0001
// DRC index
#define wroteDTSHDDRCRangeIndex 				0xcb00+STD_BETA_DTSHD2,0x000e
// HDCD
#define wroteDTSHDHDCD 							0xcb00+STD_BETA_DTSHD2,0x0010
// combination of EXT_AUDIO and EXT_AUDIO
#define wroteDTSHDXChNotPresent96kHzNotPresent 	0xcb00+STD_BETA_DTSHD2,0x0012,0x0000,0xcb00+STD_BETA_DTSHD2,0x001a,0x0000
#define wroteDTSHDXChPresent96kHzNotPresent 	0xcb00+STD_BETA_DTSHD2,0x0012,0x0001,0xcb00+STD_BETA_DTSHD2,0x001a,0x0000
#define wroteDTSHDXChNotPresent96kHzPresent 	0xcb00+STD_BETA_DTSHD2,0x0012,0x0001,0xcb00+STD_BETA_DTSHD2,0x001a,0x0002
#define wroteDTSHDXChPresent96kHzPresent 		0xcb00+STD_BETA_DTSHD2,0x0012,0x0001,0xcb00+STD_BETA_DTSHD2,0x001a,0x0003
#define wroteDTSHDExtensionReserved 			0xcb00+STD_BETA_DTSHD2,0x0012,0x0001,0xcb00+STD_BETA_DTSHD2,0x001a
// EXT_AUDIO
#define wroteDTSHDEXT_AUDIONotPresent 			0xcb00+STD_BETA_DTSHD2,0x0012,0x0000
#define wroteDTSHDEXT_AUDIOPresent 				0xcb00+STD_BETA_DTSHD2,0x0012,0x0001
// EXT_AUDIO_ID
#define wroteDTSHDEXT_AUDIO_IDXChPresent 		0xcb00+STD_BETA_DTSHD2,0x001a,0x0000
#define wroteDTSHDEXT_AUDIO_ID96kHzPresent 		0xcb00+STD_BETA_DTSHD2,0x001a,0x0002
#define wroteDTSHDEXT_AUDIO_IDXChAnd96kHzPresent 0xcb00+STD_BETA_DTSHD2,0x001a,0x0003
#define wroteDTSHDEXT_AUDIO_IDReserved 			0xcb00+STD_BETA_DTSHD2,0x001a
// ASPF
#define wroteDTSHDASPFDSYNCPlacedAtEndOfEachSubFrame 	0xcb00+STD_BETA_DTSHD2,0x001c,0x0000
#define wroteDTSHDASPFDSYNCPlacedAtEndOfEachSubsubFrame 0xcb00+STD_BETA_DTSHD2,0x001c,0x0001
// LFF
#define wroteDTSHDLFFNotPresent 				0xcb00+STD_BETA_DTSHD2,0x0014,0x0000
#define wroteDTSHDLFFPresent128 				0xcb00+STD_BETA_DTSHD2,0x0014,0x0001
#define wroteDTSHDLFFPresent64 					0xcb00+STD_BETA_DTSHD2,0x0014,0x0002
#define wroteDTSHDLFFInvalid 					0xcb00+STD_BETA_DTSHD2,0x0014
// VERNUM
#define wroteDTSHDVERNUM 						0xcb00+STD_BETA_DTSHD2,0x001e
// REVNO
#define wroteDTSHDREVNO 						0xcb00+STD_BETA_DTSHD2,0x0020
// XCHFLAG
#define  wroteDTSHDBSIXCHFLAGTrueXCHSyncTrueXCHFSize 	0xcb00+STD_BETA_DTSHD2,0x0022,0x0001
#define  wroteDTSHDBSIXCHFLAGTrueXCHSyncFalseXCHFSize 	0xcb00+STD_BETA_DTSHD2,0x0022,0x0002
#define  wroteDTSHDBSIXCHFLAGFalseXCHSync 				0xcb00+STD_BETA_DTSHD2,0x0022,0x0003
#define  wroteDTSHDBSIXCHFLAGOff 						0xcb00+STD_BETA_DTSHD2,0x0022,0x0000
// X96FLAG
#define  wroteDTSHDBSIX96FLAGTrueX96SyncTrueX96FSize 	0xcb00+STD_BETA_DTSHD2,0x0024,0x0001
#define  wroteDTSHDBSIX96FLAGTrueX96SyncFalseX96FSize 	0xcb00+STD_BETA_DTSHD2,0x0024,0x0002
#define  wroteDTSHDBSIX96FLAGFalseX96Sync 				0xcb00+STD_BETA_DTSHD2,0x0024,0x0003
#define  wroteDTSHDBSIX96FLAGOff 						0xcb00+STD_BETA_DTSHD2,0x0024,0x0000
//XCHFSIZE
#define  wroteDTSHDBSIXCHFSIZE 							0xcb00+STD_BETA_DTSHD2,0x0026
//X96FSIZE
#define  wroteDTSHDBSIX96FSIZE 					0xcb00+STD_BETA_DTSHD2,0x0028
//DIALNORM
#define  wroteDTSHDBSIDIALNORM 					0xcb00+STD_BETA_DTSHD2,0x002a
//EXTSTREAM
#define  wroteDTSHDBSIEXTSTREAMPresent 			0xcb00+STD_BETA_DTSHD2,0x002c,0x0001
#define  wroteDTSHDBSIEXTSTREAMNotPresent 		0xcb00+STD_BETA_DTSHD2,0x002c,0x0000
//EXTSTREAM HeaderSize
#define  wroteDTSHDBSIEXTSTREAMHSIZE 			0xcb00+STD_BETA_DTSHD2,0x002e
//EXTSTREAM FrameSize
#define  wroteDTSHDBSIEXTSTREAMFSIZE 			0xcb00+STD_BETA_DTSHD2,0x0030
//NumAudioPresentation
#define  wroteDTSHDBSINumAudioPresentation 		0xcb00+STD_BETA_DTSHD2,0x0032
//NumAssets
#define  wroteDTSHDBSINumAssets 				0xcb00+STD_BETA_DTSHD2,0x0034
//CoreExtensionMask
#define  wroteDTSHDBSICoreExtensionMask 		0xcb00+STD_BETA_DTSHD2,0x0036
//rScale
#define  wroteDTSHDBSIrScale 					0xcb00+STD_BETA_DTSHD2,0x0038
//NumSoundFeildConfig
#define  wroteDTSHDBSINumSoundFieldConfig 		0xcb00+STD_BETA_DTSHD2,0x003A
//Decode Channel Mask
#define wroteDTSHDBSIDecodeChannelMask 			0xcc00+STD_BETA_DTSHD2,0x0040
//Program Channel Mask
#define  wroteDTSHDBSIProgramChannelMask 		0xcc00+STD_BETA_DTSHD2,0x0044
//XLL SampleRate

#define  wroteDTSHDBSIXLLSampleRateUnknown 		0xcb00+STD_BETA_DTSHD2,0x0048,0x0000
#define  wroteDTSHDBSIXLLSampleRate44100Hz 		0xcb00+STD_BETA_DTSHD2,0x0048,0x0001
#define  wroteDTSHDBSIXLLSampleRate48000Hz 		0xcb00+STD_BETA_DTSHD2,0x0048,0x0002
#define  wroteDTSHDBSIXLLSampleRate88200Hz 		0xcb00+STD_BETA_DTSHD2,0x0048,0x0003
#define  wroteDTSHDBSIXLLSampleRate96000Hz 		0xcb00+STD_BETA_DTSHD2,0x0048,0x0004
#define  wroteDTSHDBSIXLLSampleRate176400Hz 	0xcb00+STD_BETA_DTSHD2,0x0048,0x0005
#define  wroteDTSHDBSIXLLSampleRate192000Hz 	0xcb00+STD_BETA_DTSHD2,0x0048,0x0006

#define  wroteDTSHDBSIEMBDownMixOFF  		  	0xcb00+STD_BETA_DTSHD2,0x004A,0x0000
#define  wroteDTSHDBSIEMBDownMixSURROUND2_1    	0xcb00+STD_BETA_DTSHD2,0x004A,0x0001
#define  wroteDTSHDBSIEMBDownMixSTEREO   	   	0xcb00+STD_BETA_DTSHD2,0x004A,0x0002

#define  wroteDTSHDBSISoftLimiterScaleOFF  	   	0xcb00+STD_BETA_DTSHD2,0x004C,0x0000
#define  wroteDTSHDBSISoftLimiterScale3dBDown  	0xcb00+STD_BETA_DTSHD2,0x004C,0x0001
#define  wroteDTSHDBSISoftLimiterScale6dBDown  	0xcb00+STD_BETA_DTSHD2,0x004C,0x0002

#define  wroteDTSHDBSIEmbDwnMxOFFEmbCoefOFF			0xcb00+STD_BETA_DTSHD2,0x004E,0x0000
#define  wroteDTSHDBSIEmbDwnMx2chONEmbCoefOFF		0xcb00+STD_BETA_DTSHD2,0x004E,0x0001
#define  wroteDTSHDBSIEmbDwnMx6chONEmbCoefOFF		0xcb00+STD_BETA_DTSHD2,0x004E,0x0002
#define  wroteDTSHDBSIEmbDwnMx6chON2chONEmbCoefOFF 	0xcb00+STD_BETA_DTSHD2,0x004E,0x0003
#define  wroteDTSHDBSIEmbDwnMxOFFEmbCoefONCore	   	0xcb00+STD_BETA_DTSHD2,0x004E,0x0004
#define  wroteDTSHDBSIEmbDwnMxOFFEmbCoefONCoreLL   	0xcb00+STD_BETA_DTSHD2,0x004E,0x000C
#define  wroteDTSHDBSIEmbDwnMx6chONEmbCoefONCoreLL 	0xcb00+STD_BETA_DTSHD2,0x004E,0x000E
#define  wroteDTSHDBSIEmbDwnMx2chONEmbCoefONCore   	0xcb00+STD_BETA_DTSHD2,0x004E,0x0005
#define  wroteDTSHDBSIEmbDwnMx6chONEmbCoefONCore	0xcb00+STD_BETA_DTSHD2,0x004E,0x0006
#define  wroteDTSHDBSIEmbDwnMx6chON2chONEmbCoefONCore  0xcb00+STD_BETA_DTSHD2,0x004E,0x0007

#define  wroteDTSHDBSICoreSyncInfo 					0xcb00+STD_BETA_DTSHD2,0x0050,0x0000
#define  wroteDTSHDBSICoreSyncError					0xcb00+STD_BETA_DTSHD2,0x0050,0x0001

#define  wroteDTSHDBSIEMBSpkrRemapMask				0xcb00+STD_BETA_DTSHD2,0x0052

//PCMR
#define wroteDTSHDPCMR16BitsESOff 					0xcb00+STD_BETA_DTSHD2,0x0016,0x000
#define wroteDTSHDPCMR16BitsESOn 					0xcb00+STD_BETA_DTSHD2,0x0016,0x001
#define wroteDTSHDPCMR20BitsESOff 					0xcb00+STD_BETA_DTSHD2,0x0016,0x002
#define wroteDTSHDPCMR20BitsESOn 					0xcb00+STD_BETA_DTSHD2,0x0016,0x003
#define wroteDTSHDPCMR24BitsESOff 					0xcb00+STD_BETA_DTSHD2,0x0016,0x006
#define wroteDTSHDPCMR24BitsESOn 					0xcb00+STD_BETA_DTSHD2,0x0016,0x005
#define wroteDTSHDPCMRInvalid 						0xcb00+STD_BETA_DTSHD2,0x0016
// FSIZE
#define wroteDTSHDFSIZE 							0xcb00+STD_BETA_DTSHD2,0x0018

#define  readDTSHDStatus 0xc508,STD_BETA_DTSHD
#define  readDTSHDCommon 0xc508,STD_BETA_DTSHD2
#define  readDTSHDControl \
         readDTSHDDecodeMode, \
         readDTSHD9624Mode, \
         readDTSHDDynrngScaleQ6, \
         readDTSHDDialNormMode, \
         readDTSHDVersionChkMode, \
         readDTSHDDownMixMode, \
         readDTSHDXBRMode, \
         readDTSHDXXCHMode, \
         readDTSHDXLLMode, \
         readDTSHDSpeakerRemapMode, \
         readDTSHDDRCMode, \
         readDTSHDAudioPresentationIndex, \
         readDTSHDSoundFieldConfigIndex, \
         readDTSHDLFEMix, \
         readDTSHDListeningSpace, \
         readDTSHDEMBSpeakerRemapMode, \
         readDTSHDEMBDownMixMode, \
         readDTSHDSoftLimiterMode, \
         readDTSHDForce96For192StrmMode, \
         readDTSHD51Mode, \
         readDTSHDRouteCsMode, \
		 readDTSHDMatrixMode, \
		 readDTSHDNEOInternalGain



#define writeDTSHDAudioPresentationIndex(NN) 0xca00+STD_BETA_DTSHD,0x1F00+((NN)&0xFF)

//Support of inverse compilation
#define writeDTSHDAudioPresentationIndex__0__ writeDTSHDAudioPresentationIndex(0)
#define writeDTSHDAudioPresentationIndex__1__ writeDTSHDAudioPresentationIndex(1)

#define writeDTSHDSoundFieldConfigIndex__0__  writeDTSHDSoundFieldConfigIndex(0)
#define writeDTSHDSoundFieldConfigIndex__1__  writeDTSHDSoundFieldConfigIndex(1)

// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define writeDTSHDDynrngScaleQ6XX(N)  writeDTSHDDynrngScaleQ6N(0x##N)


//DTS alpha codes

#define  readDTSOperationalMode 					readDTSHDDecodeMode
#define writeDTSOperationalModeDigitalSurround 		writeDTSHDDecodeModeDigitalSurround
#define writeDTSOperationalModeESDiscrete 			writeDTSHDDecodeModeCoreSS

#define  readDTS9624Mode 							readDTSHD9624Mode
#define writeDTS9624ModeDisable 					writeDTSHD9624ModeDisable
#define writeDTS9624ModeEnable 						writeDTSHD9624ModeEnable

#define  readDTSDynrngScaleQ6 						readDTSHDDynrngScaleQ6
#define writeDTSDynrngScaleQ6N(NN)  				writeDTSHDDynrngScaleQ6N(NN)

#define  readDTSDialNormMode 						readDTSHDDialNormMode
#define writeDTSDialNormModeDisable 				writeDTSHDDialNormModeDisable
#define writeDTSDialNormModeEnable 					writeDTSHDDialNormModeEnable

#define  readDTSVersionChkMode  					readDTSHDVersionChkMode
#define writeDTSVersionChkDisable 					writeDTSHDVersionChkDisable
#define writeDTSVersionChkEnable 					writeDTSHDVersionChkEnable
#define writeDTSVersionChk8Enable 					0xca00+STD_BETA_DTSHD,0x1102

#define  readDTSBitStreamInformation 				readDTSHDBitStreamInformation
#define  readDTSBitStreamInformation0 				readDTSHDBitStreamInformation0
#define  readDTSBitStreamInformation1 				readDTSHDBitStreamInformation1
#define  readDTSBSIAMODE 							readDTSBitStreamInformation1
#define  readDTSBitStreamInformation2 				readDTSHDBitStreamInformation2
#define  readDTSBSISFREQ 							readDTSBitStreamInformation2
#define  readDTSBitStreamInformation3 				readDTSHDBitStreamInformation3
#define  readDTSBSIRATE 							readDTSBitStreamInformation3
#define  readDTSBitStreamInformation4 				readDTSHDBitStreamInformation4
#define  readDTSBSIDYNF 							readDTSBitStreamInformation4
#define  readDTSBitStreamInformation5 				readDTSHDBitStreamInformation5
#define  readDTSBSIRANGE 							readDTSBitStreamInformation5
#define  readDTSBitStreamInformation6 				readDTSHDBitStreamInformation6
#define  readDTSBSIHDCD 							readDTSBitStreamInformation6
#define  readDTSBitStreamInformation7 				readDTSHDBitStreamInformation7
#define  readDTSBSIEXT_AUDIO 						readDTSBitStreamInformation7
#define  readDTSBitStreamInformation8 				readDTSHDBitStreamInformation8
#define  readDTSBSILFF 								readDTSBitStreamInformation8
#define  readDTSBitStreamInformation9 				readDTSHDBitStreamInformation9
#define  readDTSBSIPCMR 							readDTSBitStreamInformation9
#define  readDTSBitStreamInformation10 				readDTSHDBitStreamInformation10
#define  readDTSBSIFSIZE 							readDTSBitStreamInformation10
#define  readDTSBitStreamInformation11 				readDTSHDBitStreamInformation11
#define  readDTSBSIEXT_AUDIO_ID 					readDTSBitStreamInformation11
#define  readDTSBitStreamInformation12 				readDTSHDBitStreamInformation12
#define  readDTSBSIASPF 							readDTSBitStreamInformation12
#define  readDTSBitStreamInformation13 				readDTSHDBitStreamInformation13
#define  readDTSBSIVERNUM 							readDTSBitStreamInformation13
#define  readDTSBitStreamInformation14 				readDTSHDBitStreamInformation14
#define  readDTSBSIREVNO 							readDTSBitStreamInformation14
#define  readDTSBitStreamInformation15 				readDTSHDBitStreamInformation15
#define  readDTSBSIXCHFLAG 							readDTSBitStreamInformation15
#define  readDTSBitStreamInformation16 				readDTSHDBitStreamInformation16
#define  readDTSBSIX96FLAG 							readDTSBitStreamInformation16
#define  readDTSBitStreamInformation17 				readDTSHDBitStreamInformation17
#define  readDTSBSIXCHFSIZE 						readDTSBitStreamInformation17
#define  readDTSBitStreamInformation18 				readDTSHDBitStreamInformation18
#define  readDTSBSIX96FSIZE 						readDTSBitStreamInformation18
#define  readDTSBitStreamInformation19 				readDTSHDBitStreamInformation19
#define  readDTSBSIDIALNORM 						readDTSBitStreamInformation19

#define  readDTSStatus 								readDTSHDStatus
#define  readDTSCommon 								readDTSHDCommon
#define  readDTSControl 							readDTSHDControl



#endif /* _DTSHD_A */
