
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DVS2 Algorithm alpha codes
//
//
//

#ifndef _DVS2_A
#define _DVS2_A

#include <acpbeta.h>

#define  readDVS2Mode               0xc200+(STD_BETA_DVS2),0x0400
#define writeDVS2ModeDisable        0xca00+(STD_BETA_DVS2),0x0400
#define writeDVS2ModeEnable         0xca00+(STD_BETA_DVS2),0x0401

#define  readDVS2Bypass             0xc200+(STD_BETA_DVS2),0x0500
#define writeDVS2BypassEnable       0xca00+(STD_BETA_DVS2),0x0500
#define writeDVS2BypassDisable      0xca00+(STD_BETA_DVS2),0x0501

#define  readDVS2Use                 readDVS2Bypass
#define writeDVS2UseDisable         writeDVS2BypassEnable
#define writeDVS2UseEnable          writeDVS2BypassDisable

#define  readDVS2SpeakerMode        0xc200+(STD_BETA_DVS2),0x0600
#define writeDVS2SpeakerModeRef     0xca00+(STD_BETA_DVS2),0x0600
#define writeDVS2SpeakerModeWide    0xca00+(STD_BETA_DVS2),0x0601

#define  readDVS2LfeMix             0xc200+(STD_BETA_DVS2),0x0700
#define writeDVS2LfeMixDisable      0xca00+(STD_BETA_DVS2),0x0700
#define writeDVS2LfeMixEnable       0xca00+(STD_BETA_DVS2),0x0701

#define  readDVS2SpeakerLayout      0xc200+(STD_BETA_DVS2),0x0800
#define writeDVS2SpeakerLayout2     0xca00+(STD_BETA_DVS2),0x0802
#define writeDVS2SpeakerLayout3     0xca00+(STD_BETA_DVS2),0x0803
#define writeDVS2SpeakerLayout4     0xca00+(STD_BETA_DVS2),0x0804
#define writeDVS2SpeakerLayout5     0xca00+(STD_BETA_DVS2),0x0805

#define  readDVS2OutputLevel        0xc200+(STD_BETA_DVS2),0x0900
#define writeDVS2OutputLevel6dbdown 0xca00+(STD_BETA_DVS2),0x0900
#define writeDVS2OutputLevel0db     0xca00+(STD_BETA_DVS2),0x0901


#define  readDVS2Status 0xc508,0x0000+(STD_BETA_DVS2)
#define  readDVS2Control \
         readDVS2Mode, \
         readDVS2Bypass, \
         readDVS2SpeakerMode, \
         readDVS2LfeMix, \
         readDVS2SpeakerLayout, \
         readDVS2OutputLevel

#endif /* _DVS2_A */
