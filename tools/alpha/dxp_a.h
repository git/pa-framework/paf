
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Add Description here 
//

#ifndef _DXP_A
#define _DXP_A
#include <stdbeta.h>


#define readDXPUpSample 0xc200+STD_BETA_DXP,0x0400
#define readDXPBitDepth 0xc200+STD_BETA_DXP,0x0500
#define readDXPEmbedStereoDownmix 0xc200+STD_BETA_DXP,0x0600
#define readDXPDownmixSoftLimit 0xc200+STD_BETA_DXP,0x0700
#define readDXPLfeDownmixInclude 0xc200+STD_BETA_DXP,0x0800
#define readDXP51Mode 0xc200+STD_BETA_DXP,0x0900

#define writeDXPUpSampleDisable 0xca00+STD_BETA_DXP,0x0400
#define writeDXPUpSampleTo48000 0xca00+STD_BETA_DXP,0x0401
#define writeDXPUpSampleInternal 0xca00+STD_BETA_DXP,0x0402

#define writeDXPBitDepthInternal 0xca00+STD_BETA_DXP,0x0500
#define writeDXPBitDepth16 0xca00+STD_BETA_DXP,0x0501
#define writeDXPBitDepth24 0xca00+STD_BETA_DXP,0x0502

#define writeDXPEmbedStereoDownmixDisable 0xca00+STD_BETA_DXP,0x0600
#define writeDXPEmbedStereoDownmixEnable 0xca00+STD_BETA_DXP,0x0601

#define writeDXPDownmixSoftLimitDisable 0xca00+STD_BETA_DXP,0x0700
#define writeDXPDownmixSoftLimitEnable 0xca00+STD_BETA_DXP,0x0701

#define writeDXPLfeDownmixIncludeDisable 0xca00+STD_BETA_DXP,0x0800
#define writeDXPLfeDownmixIncludeEnable 0xca00+STD_BETA_DXP,0x0801

#define writeDXP51EnhancementMode 0xca00+STD_BETA_DXP,0x0900
#define writeDXP51StandardMode 	  0xca00+STD_BETA_DXP,0x0901

#define  readDXPBitStreamInformation 0xc600+STD_BETA_DXP2,0x0420
#define  readDXPBitStreamInformation0 0xc300+STD_BETA_DXP2,0x0004
#define  readDXPBitStreamInformationNumChannels 0xc300+STD_BETA_DXP2,0x0008
#define readDXPBitStreamInformation1 0xc300+STD_BETA_DXP2,0x0008
#define readDXPBitStreamInformationVersion 0xc300+STD_BETA_DXP2,0x000a
#define readDXPBitStreamInformation2 0xc300+STD_BETA_DXP2,0x000a
#define readDXPBitStreamInformationSampleRate 0xc400+STD_BETA_DXP2,0x000c
#define readDXPBitStreamInformation3 0xc400+STD_BETA_DXP2,0x000c
#define readDXPBitStreamInformationOriginalBitRate  0xc400+STD_BETA_DXP2,0x0010
#define readDXPBitStreamInformation4 0xc400+STD_BETA_DXP2,0x0010
#define readDXPBitStreamInformationScaledBitRate 0xc400+STD_BETA_DXP2,0x0014
#define readDXPBitStreamInformation5 0xc400+STD_BETA_DXP2,0x0014
#define readDXPBitStreamInformationInputBytes 0xc400+STD_BETA_DXP2,0x0018
#define readDXPBitStreamInformation6 0xc400+STD_BETA_DXP2,0x0018
#define readDXPBitStreamInformationOutputBytes 0xc400+STD_BETA_DXP2,0x001c
#define readDXPBitStreamInformation7 0xc400+STD_BETA_DXP2,0x001c
#define readDXPBitStreamInformationInitialisationFlags 0xc400+STD_BETA_DXP2,0x0020
#define readDXPBitStreamInformation8 0xc400+STD_BETA_DXP2,0x0020

#define readDXPStatus 0xc508, STD_BETA_DXP
#define readDXPCommon 0xc508, STD_BETA_DXP2

#define readDXPControl \
        readDXPUpSample, \
        readDXPBitDepth, \
        readDXPEmbedStereoDownmix, \
        readDXPDownmixSoftLimit, \
        readDXPLfeDownmixInclude, \
        readDXP51Mode 


#endif
