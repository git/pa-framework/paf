
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// HDCD Dynamic Processing algorithm alpha codes
//
//
//

#ifndef _HDD_A
#define _HDD_A

#include <acpbeta.h>

#define readHDDMode 0xc200+STD_BETA_HDD,0x0400
#define writeHDDModeDisable 0xca00+STD_BETA_HDD,0x0400
#define writeHDDModeEnable 0xca00+STD_BETA_HDD,0x0401

#define readHDDDetect 0xc200+STD_BETA_HDD,0x0500

/* Only 16bit input bit depth is supported */
#define readHDDInpBitDepth 0xc200+STD_BETA_HDD,0x0600
#define writeHDDInpBitDepth16 0xca00+STD_BETA_HDD,0x0610
#define writeHDDInpBitDepth18 0xca00+STD_BETA_HDD,0x0612
#define writeHDDInpBitDepth20 0xca00+STD_BETA_HDD,0x0614
#define writeHDDInpBitDepth24 0xca00+STD_BETA_HDD,0x0618

#define readHDDAutoLevel 0xc200+STD_BETA_HDD,0x0700
#define writeHDDAutoLevelOff 0xca00+STD_BETA_HDD,0x0700
#define writeHDDAutoLevelUp 0xca00+STD_BETA_HDD,0x0702
#define writeHDDAutoLevelDown 0xca00+STD_BETA_HDD,0x0701
// For compatibility only
#define writeHDDAutoLevelOn writeHDDAutoLevelDown

#define readHDDInputFormat 0xc200+STD_BETA_HDD,0x0900
#define writeHDDInputFloat 0xca00+STD_BETA_HDD,0x0901
#define writeHDDInputFixed 0xca00+STD_BETA_HDD,0x0900

#define readHDDOutputFormat 0xc200+STD_BETA_HDD,0x0A00
#define writeHDDOutputFloat 0xca00+STD_BETA_HDD,0x0A01
#define writeHDDOutputFixed 0xca00+STD_BETA_HDD,0x0A00

/* This alpha code is added to choose whether to PASS any input that is not detected as HDCD */
#define readHDDPassNonHDCD       0xc200+STD_BETA_HDD,0x0B00
#define writeHDDPassNonHDCDOff  0xca00+STD_BETA_HDD,0x0B00
#define writeHDDPassNonHDCDOn  0xca00+STD_BETA_HDD,0x0B01

#define  readHDDStatus 0xc508,0x0000+STD_BETA_HDD
#define  readHDDControl \
         readHDDMode, \
         readHDDDetect, \
         readHDDInpBitDepth, \
         readHDDAutoLevel, \
         readHDDInputFormat, \
         readHDDOutputFormat, \
         readHDDPassNonHDCD

#endif /* _HDD_A */
