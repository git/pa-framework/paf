
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// HDCD Precision Filter algorithm alpha codes
//
//
//

#ifndef _HDF_A
#define _HDF_A

#include <acpbeta.h>

#define readHDFMode 0xc200+STD_BETA_HDF,0x0400
#define writeHDFModeDisable 0xca00+STD_BETA_HDF,0x0400
#define writeHDFModeEnable 0xca00+STD_BETA_HDF,0x0401

#define readHDFOpBitDepth 0xc200+STD_BETA_HDF,0x0500
#define writeHDFOpBitDepth16 0xca00+STD_BETA_HDF,0x0510
#define writeHDFOpBitDepth18 0xca00+STD_BETA_HDF,0x0512
#define writeHDFOpBitDepth20 0xca00+STD_BETA_HDF,0x0514
#define writeHDFOpBitDepth24 0xca00+STD_BETA_HDF,0x0518

#define readHDFDither 0xc200+STD_BETA_HDF,0x0600
#define writeHDFDitherOn 0xca00+STD_BETA_HDF,0x0601
#define writeHDFDitherOff 0xca00+STD_BETA_HDF,0x0600

#define readHDFDitherMode 0xc200+STD_BETA_HDF,0x0700
#define writeHDFDitherMode0 0xca00+STD_BETA_HDF,0x0700
#define writeHDFDitherMode1 0xca00+STD_BETA_HDF,0x0701
#define writeHDFDitherMode2 0xca00+STD_BETA_HDF,0x0702
#define writeHDFDitherMode3 0xca00+STD_BETA_HDF,0x0703
#define writeHDFDitherMode4 0xca00+STD_BETA_HDF,0x0704
#define writeHDFDitherMode5 0xca00+STD_BETA_HDF,0x0705
#define writeHDFDitherMode6 0xca00+STD_BETA_HDF,0x0706
#define writeHDFDitherMode7 0xca00+STD_BETA_HDF,0x0707

#define readHDFDeEmphasis 0xc200+STD_BETA_HDF,0x0800
#define writeHDFDeEmphasisOn 0xca00+STD_BETA_HDF,0x0801
#define writeHDFDeEmphasisOff 0xca00+STD_BETA_HDF,0x0800

#define readHDFUpFactor 0xc200+STD_BETA_HDF,0x0900
#define writeHDFUpFactor1 0xca00+STD_BETA_HDF,0x0900
#define writeHDFUpFactor2 0xca00+STD_BETA_HDF,0x0901
#define writeHDFUpFactor3 0xca00+STD_BETA_HDF,0x0902
#define writeHDFUpFactor4 0xca00+STD_BETA_HDF,0x0903

/* To choose whether -1dB(0.5dB) pre-attenuation for upsampling is required 
  If On, 0dBFS PCM input will give -1dBFS PCM output
*/
#define  readHDFDigitalAtt      0xc200+STD_BETA_HDF,0x0b00
#define  writeHDFDigitalAttOn 0xca00+STD_BETA_HDF,0x0b01
#define  writeHDFDigitalAttOff 0xca00+STD_BETA_HDF,0x0b00

/* To optionally disable the 1x upsampling compensation filter */
#define  readHDF1XUpFilter      0xc200+STD_BETA_HDF,0x0c00
#define  writeHDF1XUpFilterOn 0xca00+STD_BETA_HDF,0x0c01
#define  writeHDF1XUpFilterOff 0xca00+STD_BETA_HDF,0x0c00


#define  readHDFStatus 0xc508,0x0000+STD_BETA_HDF
#define  readHDFControl \
         readHDFMode, \
         readHDFOpBitDepth, \
         readHDFDither, \
         readHDFDitherMode, \
         readHDFDeEmphasis, \
         readHDFUpFactor, \
         readHDFDigitalAtt, \
         readHDF1XUpFilter

#endif /* _HDF_A */
