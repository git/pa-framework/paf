
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// MP3 Decoder alpha codes
//
//
//

#ifndef _MP3_A
#define _MP3_A

#include <acpbeta.h>

#define  readMP3Mode 0xc200+STD_BETA_MP3,0x0400

#define  readMP3BitStreamInformation 0xc600+STD_BETA_MP32,0x0414
#define  readMP3BitStreamInformation0 0xc300+STD_BETA_MP32,0x0004 // IDex=LSB, ID=MSB
#define  readMP3BitStreamInformation1 0xc300+STD_BETA_MP32,0x0006 // Layer=LSB, protectionbit=MSB

#define  readMP3BitStreamInformation2 0xc400+STD_BETA_MP32,0x0008 // Bitrate
#define  readMP3BitStreamInformation3 0xc400+STD_BETA_MP32,0x000c // Sampling Rate

#define  readMP3BitStreamInformation4 0xc300+STD_BETA_MP32,0x0010 // padding bit=LSB, private bit=MSB
#define  readMP3BitStreamInformation5 0xc300+STD_BETA_MP32,0x0012 // mode=LSB, mode_extension=MSB
#define  readMP3BitStreamInformation6 0xc300+STD_BETA_MP32,0x0014 // copyright=LSB, original=MSB
#define  readMP3BitStreamInformation7 0xc300+STD_BETA_MP32,0x0016 // emphasis=LSB, isStereo=MSB

#define wroteMP3BitStreamInformation 0xce00+STD_BETA_MP32,0x0414

#define  readMP3Status 0xc508,0x0000+STD_BETA_MP3
#define  readMP3Common 0xc508,0x0000+STD_BETA_MP32
#define  readMP3Control readMP3Status

#endif /* _MP3_A */
