
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// MTX alpha codes
//
//
//

#ifndef _MTX_A
#define _MTX_A

#include <acpbeta.h>

#define  readMTXMode 0xc200+STD_BETA_MTX,0x0400
#define writeMTXModeDisable 0xca00+STD_BETA_MTX,0x0400
#define writeMTXModeEnable 0xca00+STD_BETA_MTX,0x0401

#define  readMTXBypass          0xc200+STD_BETA_MTX,0x0500
#define writeMTXBypassEnable    0xca00+STD_BETA_MTX,0x0500
#define writeMTXBypassDisable   0xca00+STD_BETA_MTX,0x0501

#define  readMTXUse              readMTXBypass
#define writeMTXUseDisable      writeMTXBypassEnable
#define writeMTXUseEnable       writeMTXBypassDisable

#define  readMTXPreset 0xc300+STD_BETA_MTX,6
#define writeMTXPreset(N) 0xcb00+STD_BETA_MTX,6,N
#define wroteMTXPreset 0xcb00+STD_BETA_MTX,6

#define writeMTXMusic              writeMTXPreset(0)
#define writeMTXDialog             writeMTXPreset(1)
#define writeMTXMultichannelStereo writeMTXPreset(2)

#define  readMTXCtrMix 0xc300+STD_BETA_MTX,8
#define writeMTXCtrMix(N) 0xcb00+STD_BETA_MTX,8,N
#define wroteMTXCtrMix 0xcb00+STD_BETA_MTX,8

#define  readMTXSubMix 0xc300+STD_BETA_MTX,0xA
#define writeMTXSubMix(N) 0xcb00+STD_BETA_MTX,0xA,N
#define wroteMTXSubMix 0xcb00+STD_BETA_MTX,0xA

#define  readMTXSurrDry 0xc300+STD_BETA_MTX,0xC
#define writeMTXSurrDry(N) 0xcb00+STD_BETA_MTX,0xC,N
#define wroteMTXSurrDry 0xcb00+STD_BETA_MTX,0xC

#define  readMTXSurrWet 0xc300+STD_BETA_MTX,0xE
#define writeMTXSurrWet(N) 0xcb00+STD_BETA_MTX,0xE,N
#define wroteMTXSurrWet 0xcb00+STD_BETA_MTX,0xE

#define  readMTXBackDry 0xc300+STD_BETA_MTX,0x10
#define writeMTXBackDry(N) 0xcb00+STD_BETA_MTX,0x10,N
#define wroteMTXBackDry 0xcb00+STD_BETA_MTX,0x10

#define  readMTXBackWet 0xc300+STD_BETA_MTX,0x12
#define writeMTXBackWet(N) 0xcb00+STD_BETA_MTX,0x12,N
#define wroteMTXBackWet 0xcb00+STD_BETA_MTX,0x12

#define  readMTXSurrGain 0xc300+STD_BETA_MTX,0x14
#define writeMTXSurrGain(N) 0xcb00+STD_BETA_MTX,0x14,N
#define wroteMTXSurrgain 0xcb00+STD_BETA_MTX,0x14

#define  readMTXBackGain 0xc300+STD_BETA_MTX,0x16
#define writeMTXBackGain(N) 0xcb00+STD_BETA_MTX,0x16,N
#define wroteMTXBackGain 0xcb00+STD_BETA_MTX,0x16

#define  readMTXsurDelay 0xc300+STD_BETA_MTX,0x18
#define writeMTXsurDelay(N) 0xcb00+STD_BETA_MTX,0x18,N
#define wroteMTXsurDelay 0xcb00+STD_BETA_MTX,0x18


#define  readMTXStatus 0xc508,STD_BETA_MTX
#define  readMTXControl \
         readMTXMode, \
         readMTXBypass, \
         readMTXPreset, \
         readMTXCtrMix, \
         readMTXSubMix, \
         readMTXSurrDry, \
         readMTXSurrWet, \
         readMTXBackDry, \
         readMTXBackWet, \
         readMTXSurrGain, \
         readMTXBackGain, \
         readMTXsurDelay

#endif /* _MTX_A */
