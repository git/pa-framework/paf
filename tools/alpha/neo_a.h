
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DTS Neo:6 Audio Stream Processing alpha codes
//
//
//

/* DTS Neo:6 Audio Stream Processing alpha codes */

#ifndef _NEO_A
#define _NEO_A

#include <acpbeta.h>

/* NEO Algorithm enable/disable */
#define readNEOMode         0xc200+STD_BETA_NEO,0x0400
#define writeNEOModeEnable  0xca00+STD_BETA_NEO,0x0401
#define writeNEOModeDisable 0xca00+STD_BETA_NEO,0x0400


/* Control of NEO processing on selected input bit-streams */
#define readNEOApplicationLevel   0xc200+STD_BETA_NEO,0x0500
#define writeNEOApplicationLevel0 0xca00+STD_BETA_NEO,0x0500 /* OFF */
#define writeNEOApplicationLevel1 0xca00+STD_BETA_NEO,0x0501 /* OFF - if AC3 or non-LtRt */
#define writeNEOApplicationLevel2 0xca00+STD_BETA_NEO,0x0502 /* OFF - if AC3 or stereo   */
#define writeNEOApplicationLevel3 0xca00+STD_BETA_NEO,0x0503 /* OFF - if AC3 */
#define writeNEOApplicationLevel4 0xca00+STD_BETA_NEO,0x0504 /* ON */
#define writeNEOApplicationLevel5 0xca00+STD_BETA_NEO,0x0505 /* OFF - if non-LtRt */

/* Cinema/Music mode selection */
#define readNEOOperationalMode        0xc200+STD_BETA_NEO,0x0600
#define writeNEOOperationalModeCinema 0xca00+STD_BETA_NEO,0x0600 /* Cinema-LtRt encoded i/p */
#define writeNEOOperationalModeMusic  0xca00+STD_BETA_NEO,0x0601 /* Music -LoRo audio i/p   */


/* Enable/Disable 64 Band NEO-processing for various sample-rates */

/* There is a  bit masks for 64 band processing, with the below bit-mapping */
/*
   Bit  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  |
   KHz    192   176   96    88    64    48   44.1   32
*/
#define readNEOSRate64BandMode          0xc300+STD_BETA_NEO,0x0010
/* 64 band processing is enabled for bits 0 to 5, ie 32 to 96 KHz sample rates */
#define writeNEOSRate64BandEnable       0xcb00+STD_BETA_NEO,0x0010,0x003f
/* 64 band processing is disabled for all the sample rates */
#define writeNEOSRate64BandDisable       0xcb00+STD_BETA_NEO,0x0010,0x0000
/* Specify the required 64 band bit-mask(NN) */
#define writeNEOSRate64BandEnableN(NN)  0xcb00+STD_BETA_NEO,0x0010,(NN)

/* NEO configuration mode(2 to N ch conversion) selection */
/*
   0 - 2 to 3 ch conversion
   1 - 2 to 5 ch conversion
   2 - 2 to 7 ch conversion
*/
#define readNEOConfigMode     0xc200+STD_BETA_NEO,0x0b00
#define writeNEOConfigMode3Ch 0xca00+STD_BETA_NEO,0x0b00 /* 2 to 3 ch conversion */
#define writeNEOConfigMode5Ch 0xca00+STD_BETA_NEO,0x0b01 /* 2 to 5 ch conversion */
#define writeNEOConfigMode7Ch 0xca00+STD_BETA_NEO,0x0b02 /* 2 to 7 ch conversion */

#define writeNEOConfigMode6Ch writeNEOConfigMode7Ch
//for backward compatibility

/* Centre-Gain control(CGain) : L -> C <- R */
#define readNEOCGainQ6        0xc200+STD_BETA_NEO,0x0800
/*
   float cgain = (int)N/(int)64,
   where, NN = 0x00  to 0x40,
   ie  cgain = 0.0 to 1.0 [Note:If cgain > 1.0, it is saturated to 1.0]

   Q6 means 2^6 = 64.
*/
#define writeNEOCGainQ6N(NN)  0xca00+STD_BETA_NEO,0x0800+((NN)&0xff)


/* Generating one back channel from Ls-Rs channels */
/* Note :
    Even though enabled, the back channel is generated if
     a) Input audio stream contains Ls and Rs channels.
     b) The requested channel configuration contains a back channel
*/
#define readNEOMatrix         0xc200+STD_BETA_NEO,0x0900
#define writeNEOMatrixDisable 0xca00+STD_BETA_NEO,0x0900
#define writeNEOMatrixEnable  0xca00+STD_BETA_NEO,0x0901


/* Internal gain control
 * If in Cinema mode, +3dB for all channels.
 * If in Music  mode, +3dB for front channels(L-C-R) and +6dB for others.
 */
#define readNEOInternalGain         0xc200+STD_BETA_NEO,0x0a00
#define writeNEOInternalGainDisable 0xca00+STD_BETA_NEO,0x0a00
#define writeNEOInternalGainEnable  0xca00+STD_BETA_NEO,0x0a01

/* NEO channel configuration request override */
/* This will override NEO using the PAF's channel configuration request */
#define readNEOChannelConfigurationOverride             0xc400+STD_BETA_NEO,0x0020
#define writeNEOChannelConfigurationOverrideUnknown     0xcc00+STD_BETA_NEO,0x0020,0x0000,0x0000
#define writeNEOChannelConfigurationOverridePhantom1_0  0xcc00+STD_BETA_NEO,0x0020,0x0004,0x0000
#define writeNEOChannelConfigurationOverridePhantom2_0  0xcc00+STD_BETA_NEO,0x0020,0x0005,0x0000
#define writeNEOChannelConfigurationOverridePhantom3_0  0xcc00+STD_BETA_NEO,0x0020,0x0006,0x0000
#define writeNEOChannelConfigurationOverridePhantom4_0  0xcc00+STD_BETA_NEO,0x0020,0x0007,0x0000
#define writeNEOChannelConfigurationOverrideSurround0_0 0xcc00+STD_BETA_NEO,0x0020,0x0008,0x0000
#define writeNEOChannelConfigurationOverrideSurround1_0 0xcc00+STD_BETA_NEO,0x0020,0x0009,0x0000
#define writeNEOChannelConfigurationOverrideSurround2_0 0xcc00+STD_BETA_NEO,0x0020,0x000a,0x0000
#define writeNEOChannelConfigurationOverrideSurround3_0 0xcc00+STD_BETA_NEO,0x0020,0x000b,0x0000
#define writeNEOChannelConfigurationOverrideSurround4_0 0xcc00+STD_BETA_NEO,0x0020,0x000c,0x0000

/* Read the complete NEO status structure */
#define  readNEOStatus 0xc508,STD_BETA_NEO

/* Read various NEO control registers at a time */
#define  readNEOControl \
         readNEOMode, \
         readNEOApplicationLevel, \
         readNEOOperationalMode, \
         readNEOCGainQ6, \
         readNEOMatrix, \
         readNEOInternalGain, \
         readNEOSRate64BandMode, \
         readNEOChannelConfigurationOverride, \
         readNEOConfigMode

/* in support of inverse compilation only */
#define writeNEOCGainQ6N__0x13__ writeNEOCGainQ6N(0x13)
#define writeNEOCGainQ6N__0x40__ writeNEOCGainQ6N(0x40)

// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define writeNEOSRate64BandEnableXX(N)  writeNEOSRate64BandEnableN(0x##N)
#define writeNEOSRate32BandEnableXX(N)  writeNEOSRate32BandEnableN(0x##N)
#define writeNEOCGainQ6XX(N)            writeNEOCGainQ6N(0x##N)

#endif /* _NEO_A */
