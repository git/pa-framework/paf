
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// Initial version .
//

/* DTS Neo:X Audio Stream Processing alpha codes */

#ifndef _NEOX_A
#define _NEOX_A

#include <acpbeta.h>

/* NEOX Algorithm enable/disable */
#define readNEOXMode                  0xc200+STD_BETA_NEOX,0x0400
#define writeNEOXModeEnable           0xca00+STD_BETA_NEOX,0x0401
#define writeNEOXModeDisable          0xca00+STD_BETA_NEOX,0x0400

/* Cinema/Music mode selection */
#define readNEOXOperationalMode        0xc200+STD_BETA_NEOX,0x0500
#define writeNEOXOperationalModeCinema 0xca00+STD_BETA_NEOX,0x0500 /* Cinema */
#define writeNEOXOperationalModeMusic  0xca00+STD_BETA_NEOX,0x0501 /* Music  */
#define writeNEOXOperationalModeGame   0xca00+STD_BETA_NEOX,0x0502 /* Game   */

/* Centre-Gain control(CGain) : L -> C <- R */
#define readNEOXCGainQ7        0xc200+STD_BETA_NEOX,0x0600
/*
   float cgain = (int)N/(int)128,
   where, NN = 0x00  to 0x80,
   ie  cgain = 0.0 to 1.0 [Note:If cgain > 1.0, it is saturated to 1.0]

   Q7 means 2^7 = 128.
*/
#define writeNEOXCGainQ7N(NN)  0xca00+STD_BETA_NEOX,0x0600+((NN)&0xff)

/* NEOX Algorithm Output LFE enable/disable */
#define readNEOXOutputLFE         0xc200+STD_BETA_NEOX,0x0700
#define writeNEOXOutputLFEEnable  0xca00+STD_BETA_NEOX,0x0701
#define writeNEOXOutputLFEDisable 0xca00+STD_BETA_NEOX,0x0700

/* NEOX channel configuration request override */
/* This will override NEOX using the PAF's channel configuration request */
#define readNEOXChannelCfg                       0xc400+STD_BETA_NEOX,0x0008
#define writeNEOXChannelCfgUnknown               0xcc00+STD_BETA_NEOX,0x0008,0x0000,0x0000

//L_R
#define writeNEOXChannelCfgStereo                0xcc00+STD_BETA_NEOX,0x0008,0x0003,0x0000
//L_R_Ls_Rs 
#define writeNEOXChannelCfgPhantom2_0            0xcc00+STD_BETA_NEOX,0x0008,0x0005,0x0000
//L_R_Ls_Rs_Lsr(Cs) 
#define writeNEOXChannelCfgPhantom3_0            0xcc00+STD_BETA_NEOX,0x0008,0x0006,0x0000
//L_R_Ls_Rs_Lsr_Rsr 
#define writeNEOXChannelCfgPhantom4_0            0xcc00+STD_BETA_NEOX,0x0008,0x0007,0x0000

//C_L_R
#define writeNEOXChannelCfgStereo3               0xcc00+STD_BETA_NEOX,0x0008,0x0008,0x0000
//C_L_R_Ls_Rs
#define writeNEOXChannelCfgSurround2_0           0xcc00+STD_BETA_NEOX,0x0008,0x000a,0x0000
//C_L_R_Ls_Rs_Cs
#define writeNEOXChannelCfgSurround3_0           0xcc00+STD_BETA_NEOX,0x0008,0x000b,0x0000
//C_L_R_Ls_Rs_Lsr_Rsr
#define writeNEOXChannelCfgSurround4_0           0xcc00+STD_BETA_NEOX,0x0008,0x000c,0x0000

//L_R_Ls_Rs == Phantom2
#define writeNEOXChannelCfgPhantomX_0(n)         0xcc00+STD_BETA_NEOX,0x0008,0x0005,(n<<8)
//L_R_Ls_Rs_Lw_Rw           
#define writeNEOXChannelCfgPhantomLwRw_0         writeNEOXChannelCfgPhantomX_0(0x01)      
//L_R_Ls_Rs_Lh_Rh                                
#define writeNEOXChannelCfgPhantomLhRh_0         writeNEOXChannelCfgPhantomX_0(0x04)      
//L_R_Ls_Rs_Lh_Rh_Lw_Rw           
#define writeNEOXChannelCfgPhantomLwRwLhRh_0     writeNEOXChannelCfgPhantomX_0(0x05)      

//C_L_R_Ls_Rs == Surround2
#define writeNEOXChannelCfgSurroundX_0(n) 			 0xcc00+STD_BETA_NEOX,0x0008,0x000a,(n<<8)
//C_L_R_Ls_Rs_Lw_Rw           
#define writeNEOXChannelCfgSurroundLwRw_0 			 writeNEOXChannelCfgSurroundX_0(0x01)     
//C_L_R_Ls_Rs_Lh_Rh               
#define writeNEOXChannelCfgSurroundLhRh_0 			 writeNEOXChannelCfgSurroundX_0(0x04)     
//C_L_R_Ls_Rs_Lh_Rh_Lw_Rw           
#define writeNEOXChannelCfgSurroundLwRwLhRh_0 	 writeNEOXChannelCfgSurroundX_0(0x05)     

//L_R_Ls_Rs_Lsr_Rsr == Phantom4
#define writeNEOXChannelCfgPhantom4X_0(n)        0xcc00+STD_BETA_NEOX,0x0008,0x0007,(n<<8)
//L_R_Ls_Rs_Lsr_Rsr_Lw_Rw                                
#define writeNEOXChannelCfgPhantom4LwRw_0        writeNEOXChannelCfgPhantom4X_0(0x01)     
//L_R_Ls_Rs_Lsr_Rsr_Lh_Rh                                
#define writeNEOXChannelCfgPhantom4LhRh_0        writeNEOXChannelCfgPhantom4X_0(0x04)     
//L_R_Ls_Rs_Lsr_Rsr_Lh_Rh_Lw_Rw           
#define writeNEOXChannelCfgPhantom4LwRwLhRh_0    writeNEOXChannelCfgPhantom4X_0(0x05)     

//C_L_R_Ls_Rs_Lsr_Rsr  == Surround4
#define writeNEOXChannelCfgSurround4X_0(n) 			 0xcc00+STD_BETA_NEOX,0x0008,0x000c,(n<<8)
//C_L_R_Lss_Rss_Lsr_Rsr_Lw_Rw       
#define writeNEOXChannelCfgSurround4LwRw_0 			 writeNEOXChannelCfgSurround4X_0(0x01)    
//C_L_R_Lss_Rss_Lsr_Rsr_Lh_Rh       
#define writeNEOXChannelCfgSurround4LhRh_0 			 writeNEOXChannelCfgSurround4X_0(0x04)    
//C_L_R_Lss_Rss_Lsr_Rsr_Lh_Rh_Lw_Rw 
#define writeNEOXChannelCfgSurround4LwRwLhRh_0 	 writeNEOXChannelCfgSurround4X_0(0x05)    

/* Read input channel configuration */
#define readNEOXInputChannelCfg                  0xc400+STD_BETA_NEOX,0x000c

/* NEOX Operation @96kHz enable/disable */
#define readNEOX96kHzOperationMode              0xc200+STD_BETA_NEOX,0x1000
#define writeNEOX96kHzOperationNormal           0xca00+STD_BETA_NEOX,0x1001
#define writeNEOX96kHzOperationDisable          0xca00+STD_BETA_NEOX,0x1000

/* Read the complete NEOX status structure */
#define  readNEOXStatus 0xc508,STD_BETA_NEOX

/* Read various NEOX control registers at a time */
#define  readNEOXControl \
         readNEOXMode, \
         readNEOXOperationalMode, \
         readNEOXCGainQ7, \
         readNEOXOutputLFE, \
         readNEOXChannelCfg, \
         readNEOXInputChannelCfg, \
         readNEOX96kHzOperationMode

#endif /* _NEOX_A */
