
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef _PAY_EVMDA830_IO_A
#define _PAY_EVMDA830_IO_A

#include <acpbeta.h>
#include <y15_a.h>

// execPAIInOutError : if returned, input/output "none" selected
#define  execPAYInOutError    0xf1ff

// -----------------------------------------------------------------------------
// IB SIO Select Register is set by the execPAIIn* shortcuts

#define  execPAYInNone          0xf120
#define  execPAYInDigital       0xf121
#define  execPAYInAnalog        0xf122
#define  execPAYInAnalogStereo  0xf123
#define  execPAYInAnalog_32ch   0xf124
#define  execPAYInSing          0xf126
#define  execPAYInHDMI		    0xf128
#define  execPAYInHDMIStereo    0xf129
#define  execPAYIn1394Stereo    0xf12d
#define  execPAYIn1394          0xf12e
#define  execPAYInRingIO        0xf12f

#define  execPAYInAnalog96        0xf12a
#define  execPAYInAnalogStereo96  0xf12b


// These values reflect the definition of devinp[]
#define DEVINP_NULL             0
#define DEVINP_DIR              1
#define DEVINP_ADC1             2
#define DEVINP_ADC_STEREO       3
#define DEVINP_1394_STEREO      4
#define DEVINP_1394             5
#define DEVINP_RIO              6
#define DEVINP_HDMI             7
#define DEVINP_HDMI_STEREO      8
#define DEVINP_ADC1_96             9
#define DEVINP_ADC_STEREO_96       10
#define DEVINP_N                   11

#define wroteIBSioCommandNone           0xca00+STD_BETA_IB,0x0500+DEVINP_NULL
#define wroteIBSioCommandDigital        0xca00+STD_BETA_IB,0x0500+DEVINP_DIR
#define wroteIBSioCommandAnalog         0xca00+STD_BETA_IB,0x0500+DEVINP_ADC1
#define wroteIBSioCommandAnalogStereo   0xca00+STD_BETA_IB,0x0500+DEVINP_ADC_STEREO
#define wroteIBSioCommand1394Stereo     0xca00+STD_BETA_IB,0x0500+DEVINP_1394_STEREO
#define wroteIBSioCommand1394           0xca00+STD_BETA_IB,0x0500+DEVINP_1394
#define wroteIBSioCommandRingIO         0xca00+STD_BETA_IB,0x0500+DEVINP_RIO
#define wroteIBSioCommandHDMI		    0xca00+STD_BETA_IB,0x0500+DEVINP_HDMI
#define wroteIBSioCommandHDMIStereo     0xca00+STD_BETA_IB,0x0500+DEVINP_HDMI_STEREO
#define wroteIBSioCommandAnalog96		    0xca00+STD_BETA_IB,0x0500+DEVINP_ADC1_96
#define wroteIBSioCommandAnalogStereo96     0xca00+STD_BETA_IB,0x0500+DEVINP_ADC_STEREO_96


#define wroteIBSioSelectNone            0xca00+STD_BETA_IB,0x0580+DEVINP_NULL
#define wroteIBSioSelectDigital         0xca00+STD_BETA_IB,0x0580+DEVINP_DIR
#define wroteIBSioSelectAnalog          0xca00+STD_BETA_IB,0x0580+DEVINP_ADC1
#define wroteIBSioSelectAnalogStereo    0xca00+STD_BETA_IB,0x0580+DEVINP_ADC_STEREO
#define wroteIBSioSelect1394Stereo      0xca00+STD_BETA_IB,0x0580+DEVINP_1394_STEREO
#define wroteIBSioSelect1394            0xca00+STD_BETA_IB,0x0580+DEVINP_1394
#define wroteIBSioSelectRingIO          0xca00+STD_BETA_IB,0x0580+DEVINP_RIO
#define wroteIBSioSelectHDMI		    0xca00+STD_BETA_IB,0x0580+DEVINP_HDMI
#define wroteIBSioSelectHDMIStereo      0xca00+STD_BETA_IB,0x0580+DEVINP_HDMI_STEREO
#define wroteIBSioSelectAnalog96		    0xca00+STD_BETA_IB,0x0580+DEVINP_ADC1_96
#define wroteIBSioSelectAnalogStereo96      0xca00+STD_BETA_IB,0x0580+DEVINP_ADC_STEREO_96


// -----------------------------------------------------------------------------
// OB SIO Select Register is set by the execPAIOut* shortcuts

#define  execPAYOutPrimaryNone                 0xf130
#define  execPAYOutPrimaryAnalog               0xf131 //8 channel output analog (24bit)
#define  execPAYOutPrimaryDigital              0xf132 //8 channel output Digital (24bit)
#define  execPAYOutPrimaryAnalog_32ch          0xf135 //32 channel output analog (24bit)
#define  execPAYOutPrimaryAnalogSlave          0xf138 //8 channel output analog (24bit) slave to input
#define  execPAYOutPrimaryAnalogSlaveStereo    0xf13a //2 channel output analog (24bit) slave to input
#define  execPAYOutPrimaryAnalogSlave2Stereo   0xf13b //4 channel output analog (24bit) slave to input
#define  execPAYOutSecondaryNone               0xf13c
#define  execPAYOutSecondaryDigital            0xf13d
#define execPAYOutPrimaryAnalogExt             0xf133
#define  execPAYOutPrimaryAnalogSlaveExt       0xf134

// These values reflect the definition of devout[]
#define DEVOUT_NULL             0
#define DEVOUT_DAC              1
#define DEVOUT_DIT              2
#define DEVOUT_DAC_SLAVE        3
#define DEVOUT_DAC_STEREO       4
#define DEVOUT_DAC_2STEREO      5
#define DEVOUT_DAC_EXT          6
#define DEVOUT_DAC_SLAVE_EXT    7
#define DEVOUT_N                8


#define wroteOBSioCommandNone                0xca00+STD_BETA_OB,0x0500+DEVOUT_NULL
#define wroteOBSioCommandAnalog              0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC
#define wroteOBSioCommandDigital             0xca00+STD_BETA_OB,0x0500+DEVOUT_DIT
#define wroteOBSioCommandAnalogSlave         0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_SLAVE
#define wroteOBSioCommandAnalogSlaveStereo   0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_STEREO
#define wroteOBSioCommandAnalogSlave2Stereo  0xca00+STD_BETA_OB,0x0500+DEVOUT_DAC_2STEREO

#define wroteOBSioSelectNone                 0xca00+STD_BETA_OB,0x0580+DEVOUT_NULL
#define wroteOBSioSelectAnalog               0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC
#define wroteOBSioSelectDigital              0xca00+STD_BETA_OB,0x0580+DEVOUT_DIT
#define wroteOBSioSelectAnalogSlave          0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_SLAVE
#define wroteOBSioSelectAnalogSlaveStereo    0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_STEREO
#define wroteOBSioSelectAnalogSlave2Stereo   0xca00+STD_BETA_OB,0x0580+DEVOUT_DAC_2STEREO
// -----------------------------------------------------------------------------

#endif // _PAY_EVMDA830_IO_A
