
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Dolby Prologic IIx Demonstration algorithm alpha codes
//
//

#ifndef _PL2X_A
#define _PL2X_A

#include <acpbeta.h>

//
// Non-backwards-compatible symbols
//

#define  readPL2XMode          0xc200+STD_BETA_PL2x,0x0400
#define writePL2XModeDisable   0xca00+STD_BETA_PL2x,0x0400
#define writePL2XModeEnable    0xca00+STD_BETA_PL2x,0x0401

#define  readPL2XBypass         0xc200+STD_BETA_PL2x,0x0500
#define writePL2XBypassEnable   0xca00+STD_BETA_PL2x,0x0500
#define writePL2XBypassDisable  0xca00+STD_BETA_PL2x,0x0502

#define  readPL2XUse            readPL2XBypass
#define writePL2XUseDisable     writePL2XBypassEnable
#define writePL2XUsePL2x        writePL2XBypassDisable

#define  readPL2XOperation     0xc200+STD_BETA_PL2x,0x0600
#define writePL2XOperation0    0xca00+STD_BETA_PL2x,0x0600
#define writePL2XOperation1    0xca00+STD_BETA_PL2x,0x0601
#define writePL2XOperation2    0xca00+STD_BETA_PL2x,0x0602
#define writePL2XOperation3    0xca00+STD_BETA_PL2x,0x0603

#define  readPL2XApplicationLevel     0xc200+STD_BETA_PL2x,0x0700
#define writePL2XApplicationLevel0    0xca00+STD_BETA_PL2x,0x0700
#define writePL2XApplicationLevel1    0xca00+STD_BETA_PL2x,0x0701
#define writePL2XApplicationLevel2    0xca00+STD_BETA_PL2x,0x0702
#define writePL2XApplicationLevel3    0xca00+STD_BETA_PL2x,0x0703
#define writePL2XApplicationLevel4    0xca00+STD_BETA_PL2x,0x0704

#define  readPL2XDEXBypass          0xc200+STD_BETA_PL2x,0x0800
#define writePL2XDEXBypassEnable    0xca00+STD_BETA_PL2x,0x0800
#define writePL2XDEXBypassDisable   0xca00+STD_BETA_PL2x,0x0801

#define  readPL2XDEXUse             readPL2XDEXBypass
#define writePL2XDEXUseDisable      writePL2XDEXBypassEnable
#define writePL2XDEXUseEnable       writePL2XDEXBypassDisable

#define  readPL2XDEXOperation     0xc200+STD_BETA_PL2x,0x0900
#define writePL2XDEXOperation0    0xca00+STD_BETA_PL2x,0x0900
#define writePL2XDEXOperation1    0xca00+STD_BETA_PL2x,0x0901
#define writePL2XDEXOperation2    0xca00+STD_BETA_PL2x,0x0902
#define writePL2XDEXOperation3    0xca00+STD_BETA_PL2x,0x0903

#define  readPL2XDEXApplicationLevel   0xc200+STD_BETA_PL2x,0x0a00
#define writePL2XDEXApplicationLevel0  0xca00+STD_BETA_PL2x,0x0a00
#define writePL2XDEXApplicationLevel1  0xca00+STD_BETA_PL2x,0x0a01
#define writePL2XDEXApplicationLevel2  0xca00+STD_BETA_PL2x,0x0a02
#define writePL2XDEXApplicationLevel3  0xca00+STD_BETA_PL2x,0x0a03
#define writePL2XDEXApplicationLevel4  0xca00+STD_BETA_PL2x,0x0a04

//Rs Polarity(bit 0) disable = 0, Panorma enable(bit 1) = 0

#define  readPL2XEffect     0xc200+STD_BETA_PL2x,0x0b00
#define writePL2XEffect0    0xca00+STD_BETA_PL2x,0x0b00
#define writePL2XEffect1    0xca00+STD_BETA_PL2x,0x0b01  //default
#define writePL2XEffect2    0xca00+STD_BETA_PL2x,0x0b02
#define writePL2XEffect3    0xca00+STD_BETA_PL2x,0x0b03

//Dimension Setting for PL2x

#define  readPL2XDimension          0xc200+STD_BETA_PL2x,0x0c00
#define writePL2XDimensionN(NN)     0xca00+STD_BETA_PL2x,0x0c00+((NN)&0xff)

//Mode Select for PL2x

#define  readPL2XOperationalMode             0xc200+STD_BETA_PL2x,0x0d00
#define writePL2XOperationalModeEmulation    0xca00+STD_BETA_PL2x,0x0d00   //PL1 emulation mode
#define writePL2XOperationalModeVirtual      0xca00+STD_BETA_PL2x,0x0d01   //Virtual compatible mode
#define writePL2XOperationalModeMusic        0xca00+STD_BETA_PL2x,0x0d02   //Music mode
#define writePL2XOperationalModeMovie        0xca00+STD_BETA_PL2x,0x0d03   //Movie mode(default)
#define writePL2XOperationalModeMatrix       0xca00+STD_BETA_PL2x,0x0d04   //Matrix mode
#define writePL2XOperationalModeCustom       0xca00+STD_BETA_PL2x,0x0d07   //Custom(Override mode)
#define writePL2XOperationalModeReserved1    0xca00+STD_BETA_PL2x,0x0d08
#define writePL2XOperationalModeReserved2    0xca00+STD_BETA_PL2x,0x0d09
#define writePL2XOperationalModeReserved3    0xca00+STD_BETA_PL2x,0x0d0a
#define writePL2XOperationalModeGame         0xca00+STD_BETA_PL2x,0x0d0e

#define  readPL2XOperationMode                readPL2XOperationalMode
#define writePL2XOperationModeEmulation      writePL2XOperationalModeEmulation
#define writePL2XOperationModeVirtual        writePL2XOperationalModeVirtual
#define writePL2XOperationModeMusic          writePL2XOperationalModeMusic
#define writePL2XOperationModeMovie          writePL2XOperationalModeMovie
#define writePL2XOperationModeMatrix         writePL2XOperationalModeMatrix
#define writePL2XOperationModeCustom         writePL2XOperationalModeCustom
#define writePL2XOperationModeReserved1      writePL2XOperationalModeReserved1
#define writePL2XOperationModeReserved2      writePL2XOperationalModeReserved2
#define writePL2XOperationModeReserved3      writePL2XOperationalModeReserved3
#define writePL2XOperationModeGame           writePL2XOperationalModeGame

//Center width for PL2x

#define  readPL2XCenterWidth          0xc200+STD_BETA_PL2x,0x0e00
#define writePL2XCenterWidthN(NN)     0xca00+STD_BETA_PL2x,0x0e00+((NN)&0xff)

#define  readPL2XDEXEffect     0xc200+STD_BETA_PL2x,0x0f00
#define writePL2XDEXEffect0    0xca00+STD_BETA_PL2x,0x0f00
#define writePL2XDEXEffect1    0xca00+STD_BETA_PL2x,0x0f01  //default
#define writePL2XDEXEffect2    0xca00+STD_BETA_PL2x,0x0f02
#define writePL2XDEXEffect3    0xca00+STD_BETA_PL2x,0x0f03

#define  readPL2XDEXDimension          0xc200+STD_BETA_PL2x,0x1000
#define writePL2XDEXDimensionN(NN)     0xca00+STD_BETA_PL2x,0x1000+((NN)&0xff)

#define  readPL2XDEXOperationalMode             0xc200+STD_BETA_PL2x,0x1100
#define writePL2XDEXOperationalModeEX           0xca00+STD_BETA_PL2x,0x1100
#define writePL2XDEXOperationalMode6ChMusic     0xca00+STD_BETA_PL2x,0x1101
#define writePL2XDEXOperationalMode7ChMovie     0xca00+STD_BETA_PL2x,0x1102
#define writePL2XDEXOperationalMode7ChMusic     0xca00+STD_BETA_PL2x,0x1103
#define writePL2XDEXOperationalModeCustom       0xca00+STD_BETA_PL2x,0x1104
#define writePL2XDEXOperationalModeUnknown      0xca00+STD_BETA_PL2x,0x1105
#define writePL2XDEXOperationalModeReserved2    0xca00+STD_BETA_PL2x,0x1109
#define writePL2XDEXOperationalModeReserved3    0xca00+STD_BETA_PL2x,0x110A

#define  readPL2XDEXOperationMode                readPL2XDEXOperationalMode
#define writePL2XDEXOperationModeEX             writePL2XDEXOperationalModeEX
#define writePL2XDEXOperationMode6ChMusic       writePL2XDEXOperationalMode6ChMusic
#define writePL2XDEXOperationMode7ChMovie       writePL2XDEXOperationalMode7ChMovie
#define writePL2XDEXOperationMode7ChMusic       writePL2XDEXOperationalMode7ChMusic
#define writePL2XDEXOperationModeCustom         writePL2XDEXOperationalModeCustom
#define writePL2XDEXOperationModeUnknown        writePL2XDEXOperationalModeUnknown
#define writePL2XDEXOperationModeReserved2      writePL2XDEXOperationalModeReserved2
#define writePL2XDEXOperationModeReserved3      writePL2XDEXOperationalModeReserved3

#define  readPL2XDEXDepthCtrl                   0xc200+STD_BETA_PL2x,0x1200
#define writePL2XDEXDepthCtrlN(NN)              0xca00+STD_BETA_PL2x,0x1200+((NN)&0xff)

#define  readPL2XInternalGain        0xc200+STD_BETA_PL2x,0x1300
#define writePL2XInternalGainEnable  0xca00+STD_BETA_PL2x,0x1301
#define writePL2XInternalGainDisable 0xca00+STD_BETA_PL2x,0x1300

#define  readPL2XChannelConfigurationOverride            0xc400+STD_BETA_PL2x,0x0038
#define writePL2XChannelConfigurationOverrideUnknown     0xcc00+STD_BETA_PL2x,0x0038,0x0000,0x0000
#define writePL2XChannelConfigurationOverrideSurround0_0 0xcc00+STD_BETA_PL2x,0x0038,0x0008,0x0000
#define writePL2XChannelConfigurationOverridePhantom1_0  0xcc00+STD_BETA_PL2x,0x0038,0x0004,0x0000
#define writePL2XChannelConfigurationOverridePhantom2_0  0xcc00+STD_BETA_PL2x,0x0038,0x0005,0x0000
#define writePL2XChannelConfigurationOverridePhantom3_0  0xcc00+STD_BETA_PL2x,0x0038,0x0006,0x0000
#define writePL2XChannelConfigurationOverridePhantom4_0  0xcc00+STD_BETA_PL2x,0x0038,0x0007,0x0000
#define writePL2XChannelConfigurationOverrideSurround1_0 0xcc00+STD_BETA_PL2x,0x0038,0x0009,0x0000
#define writePL2XChannelConfigurationOverrideSurround2_0 0xcc00+STD_BETA_PL2x,0x0038,0x000a,0x0000
#define writePL2XChannelConfigurationOverrideSurround3_0 0xcc00+STD_BETA_PL2x,0x0038,0x000b,0x0000
#define writePL2XChannelConfigurationOverrideSurround4_0 0xcc00+STD_BETA_PL2x,0x0038,0x000c,0x0000
#define writePL2XChannelConfigurationOverridePhantom2X_0(n) 0xcc00+STD_BETA_PL2x,0x0038,0x0005,((n)<<8)
#define writePL2XChannelConfigurationOverrideSurround2X_0(n) 0xcc00+STD_BETA_PL2x,0x0038,0x000a,((n)<<8)
#define writePL2XChannelConfigurationOverridePhantom4X_0(n) 0xcc00+STD_BETA_PL2x,0x0038,0x0007,((n)<<8)
#define writePL2XChannelConfigurationOverrideSurround4X_0(n) 0xcc00+STD_BETA_PL2x,0x0038,0x000c,((n)<<8)

#define readPL2XStatus 0xc508,STD_BETA_PL2x
#define readPL2XControl \
        readPL2XMode, \
        readPL2XBypass, \
        readPL2XOperation, \
        readPL2XApplicationLevel, \
        readPL2XDEXBypass, \
        readPL2XDEXOperation, \
        readPL2XDEXApplicationLevel, \
        readPL2XEffect, \
        readPL2XDimension, \
        readPL2XOperationalMode, \
        readPL2XCenterWidth, \
        readPL2XDEXEffect, \
        readPL2XDEXEffect, \
        readPL2XDEXDimension, \
        readPL2XDEXOperationalMode, \
        readPL2XDEXDepthCtrl, \
        readPL2XInternalGain, \
        readPL2XChannelConfigurationOverride, \
        readPL2X7KLPFApplyMode, \
        readPL2XInvMatrixStatus, \
        readPL2XDEXInvMatrixStatus, \
        readPL2XDEXChannelConfigurationOverride, \
        readPL2XForceCwidth0ForPhantom, \
        readPL2XForceCntrDelay, \
        readPL2XForceSurrDelay, \
        readPL2XForceBackDelay, \
        readPL2XDEXChannelConfigurationRequestOverride, \
        readPL2XHeightOperationMode, \
        readPL2XHeightGainControl


#define  readPL2X7KLPFApplyMode 0xc200+STD_BETA_PL2x,0x1800
#define writePL2X7KLPFApplyModeAuto 0xca00+STD_BETA_PL2x,0x1800
#define writePL2X7KLPFApplyOn 0xca00+STD_BETA_PL2x,0x1801
#define writePL2X7KLPFApplyOff 0xca00+STD_BETA_PL2x,0x1802

#define  readPL2XInvMatrixStatus  0xc200+STD_BETA_PL2x,0x1900
#define writePL2XInvMatrixDisable 0xca00+STD_BETA_PL2x,0x1900
#define writePL2XInvMatrixEnable  0xca00+STD_BETA_PL2x,0x1901

#define  readPL2XDEXInvMatrixStatus  0xc200+STD_BETA_PL2x,0x1a00
#define writePL2XDEXInvMatrixDisable 0xca00+STD_BETA_PL2x,0x1a00
#define writePL2XDEXInvMatrixEnable  0xca00+STD_BETA_PL2x,0x1a01

#define  readPL2XDEXChannelConfigurationOverride                0xc200+STD_BETA_PL2x,0x1b00
#define writePL2XDEXChannelConfigurationOverride6Ch             0xca00+STD_BETA_PL2x,0x1b00
#define writePL2XDEXChannelConfigurationOverride7Ch             0xca00+STD_BETA_PL2x,0x1b01

#define  readPL2XForceCwidth0ForPhantom        0xc200+STD_BETA_PL2x,0x2000
#define writePL2XForceCwidth0ForPhantomDisable 0xca00+STD_BETA_PL2x,0x2000
#define writePL2XForceCwidth0ForPhantomEnable  0xca00+STD_BETA_PL2x,0x2001

#define  readPL2XForceCntrDelay                0xc200+STD_BETA_PL2x,0x2100
#define writePL2XForceCntrDelayAuto            0xca00+STD_BETA_PL2x,0x2100
#define writePL2XForceCntrDelay0msec           0xca00+STD_BETA_PL2x,0x2101
#define writePL2XForceCntrDelay2msec           0xca00+STD_BETA_PL2x,0x2102

#define  readPL2XForceSurrDelay                0xc200+STD_BETA_PL2x,0x2200
#define writePL2XForceSurrDelayAuto            0xca00+STD_BETA_PL2x,0x2200
#define writePL2XForceSurrDelay0msec           0xca00+STD_BETA_PL2x,0x2201
#define writePL2XForceSurrDelay10msec          0xca00+STD_BETA_PL2x,0x2202

#define  readPL2XForceBackDelay                0xc200+STD_BETA_PL2x,0x2300
#define writePL2XForceBackDelayAuto            0xca00+STD_BETA_PL2x,0x2300
#define writePL2XForceBackDelay0msec           0xca00+STD_BETA_PL2x,0x2301
#define writePL2XForceBackDelay10msec          0xca00+STD_BETA_PL2x,0x2302
#define writePL2XForceBackDelay20msec          0xca00+STD_BETA_PL2x,0x2303

#define  readPL2XDEXChannelConfigurationRequestOverride            0xc400+STD_BETA_PL2x,0x0040
#define writePL2XDEXChannelConfigurationRequestOverrideUnknown     0xcc00+STD_BETA_PL2x,0x0040,0x0000,0x0000
#define writePL2XDEXChannelConfigurationRequestOverridePhantom3_0  0xcc00+STD_BETA_PL2x,0x0040,0x0006,0x0000
#define writePL2XDEXChannelConfigurationRequestOverridePhantom4_0  0xcc00+STD_BETA_PL2x,0x0040,0x0007,0x0000
#define writePL2XDEXChannelConfigurationRequestOverrideSurround3_0 0xcc00+STD_BETA_PL2x,0x0040,0x000b,0x0000
#define writePL2XDEXChannelConfigurationRequestOverrideSurround4_0 0xcc00+STD_BETA_PL2x,0x0040,0x000c,0x0000
#define writePL2XDEXChannelConfigurationRequestOverridePhantom2X_0(n) 0xcc00+STD_BETA_PL2x,0x0040,0x0005,((n)<<8)
#define writePL2XDEXChannelConfigurationRequestOverrideSurround2X_0(n) 0xcc00+STD_BETA_PL2x,0x0040,0x000a,((n)<<8)
#define writePL2XDEXChannelConfigurationRequestOverridePhantom4X_0(n) 0xcc00+STD_BETA_PL2x,0x0040,0x0007,((n)<<8)
#define writePL2XDEXChannelConfigurationRequestOverrideSurround4X_0(n) 0xcc00+STD_BETA_PL2x,0x0040,0x000c,((n)<<8)

#define readPL2XHeightStatus 					 0xc200+STD_BETA_PL2x,0x2C00
#define wrotePL2XHeightStatusON					 0xca00+STD_BETA_PL2x,0x2C01
#define wrotePL2XHeightStatusOFF				 0xca00+STD_BETA_PL2x,0x2C00

#define readPL2XHeightOperationMode 			 0xc200+STD_BETA_PL2x,0x2D00
#define writePL2XHeightOperationModeEnable 		 0xca00+STD_BETA_PL2x,0x2D01
#define writePL2XHeightOperationModeDisable		 0xca00+STD_BETA_PL2x,0x2D00

#define readPL2XOcfg 							 0xc300+STD_BETA_PL2x,0x002E
#define wrotePL2XOcfg 							 0xcb00+STD_BETA_PL2x,0x002E

#define readPL2XOctrl			 				 0xc300+STD_BETA_PL2x,0x0030
#define wrotePL2XOctrl							 0xcb00+STD_BETA_PL2x,0x0030
		
#define readPL2XOctrl2							 0xc300+STD_BETA_PL2x,0x0032		
#define wrotePL2XOctrl2							 0xcb00+STD_BETA_PL2x,0x0032

#define readPL2XHeightGainControl				 0xc300+STD_BETA_PL2x,0x0034
#define writePL2XHeightGainControlLow			 0xcb00+STD_BETA_PL2x,0x0034,0x0000
#define writePL2XHeightGainControlMid			 0xcb00+STD_BETA_PL2x,0x0034,0x0006
#define writePL2XHeightGainControlHigh			 0xcb00+STD_BETA_PL2x,0x0034,0x000a

// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define writePL2XDimensionXX(N)          writePL2XDimensionN(0x##N)
#define writePL2XCenterWidthXX(N)        writePL2XCenterWidthN(0x##N)
#define writePL2XDEXDimensionXX(N)       writePL2XDEXDimensionN(0x##N)
#define writePL2XDEXDepthCtrlXX(N)       writePL2XDEXDepthCtrlN(0x##N)

//
// Backwards-compatible symbols
//

#define  readPLUse              readPL2XBypass
#define writePLUseDisable       writePL2XBypassEnable
#define writePLUseEnable        writePL2XBypassDisable

#define  readDEXUse             readPL2XDEXBypass
#define writeDEXUseDisable      writePL2XDEXBypassEnable
#define writeDEXUseEnable       writePL2XDEXBypassDisable

#endif /* _PL2X_A */

