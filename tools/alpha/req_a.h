
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// ReEqualisation Algorithm alpha codes
//
//

#ifndef _REQ_A
#define _REQ_A

#include <acpbeta.h>

#define  readREQMode 0xc600+STD_BETA_REQ,0x0408
#define wroteREQModeDisable 0xce00+STD_BETA_REQ,0x0408,0x0,0x0,0x0,0x0
#define wroteREQModeEnable 0xce00+STD_BETA_REQ,0x0408,0xcc37,0x0000,0x0000,0x0000

#define  readREQBypass          0xc300+STD_BETA_REQ,0x0008
#define writeREQBypassEnable    0xcb00+STD_BETA_REQ,0x0008,0000
#define writeREQBypassDisable   0xcb00+STD_BETA_REQ,0x0008,0001

#define  readREQUse              readREQBypass
#define writeREQUseDisable      writeREQBypassEnable
#define writeREQUseEnable       writeREQBypassDisable

// Use Alpha Code Type 3 for 16 bit write/read
#define  readREQMaskSelect 0xc600+STD_BETA_REQ,0x0c0a
#define writeREQMaskSelect0 0xce00+STD_BETA_REQ,0x0c0a,0x0000,0x0000,0x0000,0x0000   // 0 channels
#define writeREQMaskSelectAll 0xce00+STD_BETA_REQ,0x0c0a,0xcc37,0x0000,0x0000,0x0000 // L,C,R, channels

//add 13/1/9
#define writeREQMaskSelect3 0xce00+STD_BETA_REQ,0x0c0a,0x0007,0x0000,0x0000,0x0000 	//  L,C,R, channels
#define writeREQMaskSelect5_B 0xce00+STD_BETA_REQ,0x0c0a,0x0c07,0x0000,0x0000,0x0000 	//  L,C,R,BL,BR channels
#define writeREQMaskSelect5_H 0xce00+STD_BETA_REQ,0x0c0a,0xc007,0x0000,0x0000,0x0000 	//  L,C,R,LH,RH channels


#define readREQStatus 0xc508,STD_BETA_REQ
#define readREQControl \
        readREQMode, \
        readREQBypass, \
        readREQMaskSelect

#endif /* _REQ_A */
