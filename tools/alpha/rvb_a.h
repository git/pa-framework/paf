
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// RVB alpha codes
//
// Copyright (c) 2003 Wave Arts, Inc., 99 Mass. Ave.,
// Arlington, MA 02474. All rights reserved.
//
//

#ifndef _RVB_A
#define _RVB_A

#include <acpbeta.h>

#define RVB_Mode_OFFSET                4
#define RVB_Use_OFFSET                 5

#define RVB_paramChg_OFFSET            6

//Dynamic
#define RVB_fxMix_OFFSET             0x8
#define RVB_reverbTime_OFFSET        0xA
#define RVB_damping_OFFSET           0xC

// Static
#define RVB_frontInGain_OFFSET       0xE
#define RVB_centerInGain_OFFSET      0x10
#define RVB_surrInGain_OFFSET        0x12

#define RVB_frontOutGain_OFFSET      0x14
#define RVB_centerOutGain_OFFSET     0x16
#define RVB_surrOutGain_OFFSET       0x18

#define RVB_roomSize_OFFSET          0x1A
#define RVB_LFCut_OFFSET             0x1C
#define RVB_PreDelay_OFFSET          0x1E
#define RVB_SDelay_OFFSET            0x20
#define RVB_loadPreset_OFFSET        0x22

// ALPHA type 2 functions

#define  readRVBMode        0xc200+(STD_BETA_RVB),0x0400
#define writeRVBModeDisable 0xca00+(STD_BETA_RVB),0x0400
#define writeRVBModeEnable  0xca00+(STD_BETA_RVB),0x0401

#define  readRVBBypass          0xc200+(STD_BETA_RVB),0x0500
#define writeRVBBypassEnable    0xca00+(STD_BETA_RVB),0x0500
#define writeRVBBypassDisable   0xca00+(STD_BETA_RVB),0x0501

#define  readRVBUse              readRVBBypass
#define writeRVBUseDisable      writeRVBBypassEnable
#define writeRVBUseEnable       writeRVBBypassDisable

// ALPHA type 3 functions

#define STD_ALPHA_R3(BETA,OFFSET)   0xc300+(BETA), OFFSET
#define RVB_R3(OFFSET)              STD_ALPHA_R3(STD_BETA_RVB, OFFSET)

#define STD_ALPHA_Wrote3(BETA,OFFSET) 0xcb00+(BETA), OFFSET
#define RVB_W3(OFFSET)              STD_ALPHA_Wrote3(STD_BETA_RVB, OFFSET)

#define STD_ALPHA_Write3(BETA,OFFSET,DATA) 0xcb00+(BETA), OFFSET, DATA
#define RVB_WD3(OFFSET, DATA)       STD_ALPHA_Write3(STD_BETA_RVB, OFFSET, DATA)

#define  readRVBparamChg            RVB_R3 (RVB_paramChg_OFFSET)
#define writeRVBparamChgDyn        RVB_WD3 (RVB_paramChg_OFFSET, 1)
#define writeRVBparamChgStat       RVB_WD3 (RVB_paramChg_OFFSET, 2)
#define writeRVBparamChgAll        RVB_WD3 (RVB_paramChg_OFFSET, 3)
#define writeRVBparamChgPreset     RVB_WD3 (RVB_paramChg_OFFSET, 4)
#define wroteRVBparamChg            RVB_W3 (RVB_paramChg_OFFSET)

// dynamic

#define  readRVBfxMix            RVB_R3 (RVB_fxMix_OFFSET)
#define writeRVBfxMix(N)         RVB_WD3 (RVB_fxMix_OFFSET, N)
#define wroteRVBfxMix            RVB_W3 (RVB_fxMix_OFFSET)

#define  readRVBreverbTime       RVB_R3 (RVB_reverbTime_OFFSET)
#define writeRVBreverbTime(N)    RVB_WD3 (RVB_reverbTime_OFFSET, N)
#define wroteRVBreverbTime       RVB_W3 (RVB_reverbTime_OFFSET)

#define  readRVBdamping          RVB_R3 (RVB_damping_OFFSET)
#define writeRVBdamping(N)       RVB_WD3 (RVB_damping_OFFSET, N)
#define wroteRVBdamping          RVB_W3 (RVB_damping_OFFSET)

// static

#define  readRVBfrontInGain      RVB_R3 (RVB_frontInGain_OFFSET)
#define writeRVBfrontInGain(N)   RVB_WD3 (RVB_frontInGain_OFFSET, N)
#define wroteRVBfrontInGain      RVB_W3 (RVB_frontInGain_OFFSET)
#define  readRVBcenterInGain     RVB_R3 (RVB_centerInGain_OFFSET)
#define writeRVBcenterInGain(N)  RVB_WD3 (RVB_centerInGain_OFFSET, N)
#define wroteRVBcenterInGain     RVB_W3 (RVB_centerInGain_OFFSET)
#define  readRVBsurrInGain       RVB_R3 (RVB_surrInGain_OFFSET)
#define writeRVBsurrInGain(N)    RVB_WD3 (RVB_surrInGain_OFFSET, N)
#define wroteRVBsurrInGain       RVB_W3 (RVB_surrInGain_OFFSET)

#define  readRVBfrontOutGain     RVB_R3 (RVB_frontOutGain_OFFSET)
#define writeRVBfrontOutGain(N)  RVB_WD3 (RVB_frontOutGain_OFFSET, N)
#define wroteRVBfrontOutGain     RVB_W3 (RVB_frontOutGain_OFFSET)
#define  readRVBcenterOutGain    RVB_R3 (RVB_centerOutGain_OFFSET)
#define writeRVBcenterOutGain(N) RVB_WD3 (RVB_centerOutGain_OFFSET, N)
#define wroteRVBcenterOutGain    RVB_W3 (RVB_centerOutGain_OFFSET)
#define  readRVBsurrOutGain      RVB_R3 (RVB_surrOutGain_OFFSET)
#define writeRVBsurrOutGain(N)   RVB_WD3 (RVB_surrOutGain_OFFSET, N)
#define wroteRVBsurrOutGain      RVB_W3 (RVB_surrOutGain_OFFSET)

#define  readRVBroomSize         RVB_R3 (RVB_roomSize_OFFSET)
#define writeRVBroomSize(N)      RVB_WD3 (RVB_roomSize_OFFSET, N)
#define wroteRVBroomSize         RVB_W3 (RVB_roomSize_OFFSET)

#define  readRVBLFCut            RVB_R3 (RVB_LFCut_OFFSET)
#define writeRVBLFCut(N)         RVB_WD3 (RVB_LFCut_OFFSET, N)
#define wroteRVBLFCut            RVB_W3 (RVB_LFCut_OFFSET)

#define  readRVBPreDelay         RVB_R3 (RVB_PreDelay_OFFSET)
#define writeRVBPreDelay(N)      RVB_WD3 (RVB_PreDelay_OFFSET, N)
#define wroteRVBPreDelay         RVB_W3 (RVB_PreDelay_OFFSET)

#define  readRVBSDelay           RVB_R3 (RVB_SDelay_OFFSET)
#define writeRVBSDelay(N)        RVB_WD3 (RVB_SDelay_OFFSET, N)
#define wroteRVBSDelay           RVB_W3 (RVB_SDelay_OFFSET)

#define  readRVBloadPreset       RVB_R3 (RVB_loadPreset_OFFSET)
#define writeRVBloadPreset(N)    RVB_WD3 (RVB_loadPreset_OFFSET, N)
#define wroteRVBloadPreset       RVB_W3 (RVB_loadPreset_OFFSET)

#define writeRVBBigHall          RVB_WD3 (RVB_loadPreset_OFFSET, 0)
#define writeRVBBrightHall       RVB_WD3 (RVB_loadPreset_OFFSET, 1)
#define writeRVBDarkHall         RVB_WD3 (RVB_loadPreset_OFFSET, 2)
#define writeRVBCathedral        RVB_WD3 (RVB_loadPreset_OFFSET, 3)
#define writeRVBClub             RVB_WD3 (RVB_loadPreset_OFFSET, 4)
#define writeRVBBrightClub       RVB_WD3 (RVB_loadPreset_OFFSET, 5)
#define writeRVBSmokyClub        RVB_WD3 (RVB_loadPreset_OFFSET, 6)
#define writeRVBBrightStadium    RVB_WD3 (RVB_loadPreset_OFFSET, 7)
#define writeRVBArena            RVB_WD3 (RVB_loadPreset_OFFSET, 8)
#define writeRVBVoxCinema        RVB_WD3 (RVB_loadPreset_OFFSET, 9)
#define writeRVBMusicCinema      RVB_WD3 (RVB_loadPreset_OFFSET, 10)
#define writeRVBBrightSpace      RVB_WD3 (RVB_loadPreset_OFFSET, 11)
// new delay presets
#define writeRVBBiggerHall       RVB_WD3 (RVB_loadPreset_OFFSET, 12)
#define writeRVBBigArena         RVB_WD3 (RVB_loadPreset_OFFSET, 13)
#define writeRVBBiggestStadium   RVB_WD3 (RVB_loadPreset_OFFSET, 14)
#define writeRVBJazzHall         RVB_WD3 (RVB_loadPreset_OFFSET, 15)
#define writeRVBSlapHappyClub    RVB_WD3 (RVB_loadPreset_OFFSET, 16)
#define writeRVBSurroundClub     RVB_WD3 (RVB_loadPreset_OFFSET, 17)



#define  readRVBStatus 0xc508,0x0000+(STD_BETA_RVB)
#define  readRVBControl \
         readRVBMode, \
         readRVBBypass, \
         readRVBparamChg, \
         readRVBfxMix, \
         readRVBreverbTime, \
         readRVBdamping, \
         readRVBfrontInGain, \
         readRVBcenterInGain, \
         readRVBsurrInGain, \
         readRVBfrontOutGain, \
         readRVBcenterOutGain, \
         readRVBsurrOutGain, \
         readRVBroomSize, \
         readRVBLFCut, \
         readRVBPreDelay, \
         readRVBSDelay, \
         readRVBloadPreset

#endif /* _RVB_A */
