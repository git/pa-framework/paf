
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// THD Decoder alpha codes
//
//
//

#ifndef _THD_A
#define _THD_A

#include <acpbeta.h>

#define  readTHDDecodeMode 0xc200+STD_BETA_THD,0x0400
#define writeTHDDecode2Channel 0xca00+STD_BETA_THD,0x0401
#define writeTHDDecode6Channel 0xca00+STD_BETA_THD,0x0402
#define writeTHDDecode8Channel 0xca00+STD_BETA_THD,0x0404
#define writeTHDDecodeAuto6Channel 0xca00+STD_BETA_THD,0x0412
#define writeTHDDecodeAuto8Channel 0xca00+STD_BETA_THD,0x0414
#define writeTHDDecodeAuto62Channel 0xca00+STD_BETA_THD,0x0413
#define writeTHDDecodeAuto82Channel 0xca00+STD_BETA_THD,0x0415
#define writeTHDDecodeAuto86Channel 0xca00+STD_BETA_THD,0x0416

#define  readTHDDecodeChannelSet 0xc200+STD_BETA_THD,0x0A00
#define wroteTHDDecodeNoChannelSet 0xca00+STD_BETA_THD,0x0A00
#define wroteTHDDecode2ChannelSet 0xca00+STD_BETA_THD,0x0A01
#define wroteTHDDecode6ChannelSet 0xca00+STD_BETA_THD,0x0A02
#define wroteTHDDecode8ChannelSet 0xca00+STD_BETA_THD,0x0A06

#define  readTHDDRCMode 0xc200+STD_BETA_THD,0x0500
#define writeTHDDRCDisable 0xca00+STD_BETA_THD,0x0500
#define writeTHDDRCFollow 0xca00+STD_BETA_THD,0x0501
#define writeTHDDRCOn 0xca00+STD_BETA_THD,0x0502

#define  readTHDDRCBoost 0xc200+STD_BETA_THD,0x0600
#define writeTHDDRCBoostN(N) 0xca00+STD_BETA_THD,0x0600+((N)&0xff)
/* in support of inverse compilation only */
#define writeTHDDRCBoostN__100__ writeTHDDRCBoostN(100)

#define  readTHDDRCCut 0xc200+STD_BETA_THD,0x0700
#define writeTHDDRCCutN(N) 0xca00+STD_BETA_THD,0x0700+((N)&0xff)
/* in support of inverse compilation only */
#define writeTHDDRCCutN__100__ writeTHDDRCCutN(100)

#define  readTHDLosslessMode 0xc200+STD_BETA_THD,0x0900
#define writeTHDPostProcessingEnable 0xca00+STD_BETA_THD,0x0900
#define writeTHDPostProcessingDisable 0xca00+STD_BETA_THD,0x0910
#define writeTHDLosslessEnable writeTHDPostProcessingDisable
#define writeTHDLosslessDisable writeTHDPostProcessingEnable

#define  readTHDCenterMixLevel 0xc300+STD_BETA_THD,0x000C
#define writeTHDCenterMixLevelN(NN) 0xcb00+STD_BETA_THD,0x000C,(0xFFFF&(NN))
#define wroteTHDCenterMixLevel 0xcb00+STD_BETA_THD,0x000C

#define  readTHDSurroundMixLevel 0xc300+STD_BETA_THD,0x000E
#define writeTHDSurroundMixLevelN(NN) 0xcb00+STD_BETA_THD,0x000E,(0xFFFF&(NN))
#define wroteTHDSurroundMixLevel 0xcb00+STD_BETA_THD,0x000E

#define  readTHDLFEDownmixInclude 0xc200+STD_BETA_THD,0x1000
#define writeTHDLFEDownmixIncludeNo 0xca00+STD_BETA_THD,0x1000
#define writeTHDLFEDownmixIncludeYes 0xca00+STD_BETA_THD,0x1001

#define  readTHDLFEDownmixVolume 0xc200+STD_BETA_THD,0x1100
#define writeTHDLFEDownmixVolumeN(NN) 0xca00+STD_BETA_THD,0x1100+(0xff&(NN))
/* in support of inverse compilation only */
#define writeTHDLFEDownmixVolumeN__20__ writeTHDLFEDownmixVolumeN(20) 

#define  readTHDCRCCheckMode 0xc200+STD_BETA_THD,0x1200
#define writeTHDCRCCheckEnable 0xca00+STD_BETA_THD,0x1201
#define writeTHDCRCCheckDisable 0xca00+STD_BETA_THD,0x1200

#define  readTHDMinorErrorMute 0xc200+STD_BETA_THD,0x1400
#define writeTHDMinorErrorMuteEnable 0xca00+STD_BETA_THD,0x1401
#define writeTHDMinorErrorMuteDisable 0xca00+STD_BETA_THD,0x1400
#define writeTHDMinorErrorNoMute writeTHDMinorErrorMuteDisable

#define  readTHDGainRequired 0xc200+STD_BETA_THD,0x1500

#define  readTHDSpeakerRemap 0xc200+STD_BETA_THD,0x1600
#define writeTHDSpeakerRemapEnable 0xca00+STD_BETA_THD,0x1601
#define writeTHDSpeakerRemapDisable 0xca00+STD_BETA_THD,0x1600

#define  readTHDSamSizIgnore 0xc200+STD_BETA_THD,0x1700
#define writeTHDSamSizIgnore 0xca00+STD_BETA_THD,0x1701
#define writeTHDSamSizNoIgnore 0xca00+STD_BETA_THD,0x1700

#define  readTHDChannelIdentifier 0xc400+STD_BETA_THD,0x0018


#define  readTHDRemappingScalefactor 0xc300+STD_BETA_THD,0x001c


#define  readTHDDialNormMode 0xc200+STD_BETA_THD,0x1e00
#define writeTHDDialNormModeEnable  0xca00+STD_BETA_THD,0x1e01
#define writeTHDDialNormModeDisable 0xca00+STD_BETA_THD,0x1e00

#define  readTHD192kHzDecode 0xc200+STD_BETA_THD,0x1f00
#define writeTHD192kHzDecodeEnable  0xca00+STD_BETA_THD,0x1f01
#define writeTHD192kHzDecodeDisable 0xca00+STD_BETA_THD,0x1f00

#define  readTHDSelectMinorError 0xc200+STD_BETA_THD,0x2000
#define writeTHDSelectMinorErrorN(N)  0xca00+STD_BETA_THD,0x2000+((N)&0xff)

#define  readTHDBitStreamInformation 0xc600+STD_BETA_THD2,0x0470

#define  readTHDBitStreamInformation0 0xc300+STD_BETA_THD2,0x0004

#define  readTHDBSIFBBChannelAssignment 0xc300+STD_BETA_THD2,0x0006
#define  readTHDBitStreamInformation1 readTHDBSIFBBChannelAssignment

#define  readTHDBSIFBBSampleRate 0xc300+STD_BETA_THD2,0x0008
#define  readTHDBitStreamInformation2 readTHDBSIFBBSampleRate

#define  readTHDBSIFBBMultiChannelType 0xc300+STD_BETA_THD2,0x000a
#define  readTHDBitStreamInformation3 readTHDBSIFBBMultiChannelType

#define  readTHDBSIFBBQuantWordLength1 0xc300+STD_BETA_THD2,0x000c
#define  readTHDBitStreamInformation4 readTHDBSIFBBQuantWordLength1

#define  readTHDBSIFBBSummaryInfo 0xc300+STD_BETA_THD2,0x000e
#define  readTHDBitStreamInformation5 readTHDBSIFBBSummaryInfo

#define  readTHDBSIFBBSourceFormat 0xc300+STD_BETA_THD2,0x0010
#define  readTHDBitStreamInformation6 readTHDBSIFBBSourceFormat

#define  readTHDBSIFBBVariableRate 0xc300+STD_BETA_THD2,0x0012
#define  readTHDBitStreamInformation7 readTHDBSIFBBVariableRate

#define  readTHDBSIFBBPeakDataRate 0xc300+STD_BETA_THD2,0x0014
#define  readTHDBitStreamInformation8 readTHDBSIFBBPeakDataRate

#define  readTHDBSIFBBSubstreams 0xc300+STD_BETA_THD2,0x0016
#define  readTHDBitStreamInformation9 readTHDBSIFBBSubstreams

#define  readTHDBSIFBBSubstreamInfo 0xc300+STD_BETA_THD2,0x0018
#define  readTHDBitStreamInformation10 readTHDBSIFBBSubstreamInfo

#define  readTHDBSIFBBLevelControl 0xc300+STD_BETA_THD2,0x001a
#define  readTHDBitStreamInformation11 readTHDBSIFBBLevelControl

#define  readTHDBSIFBBCopyRightProtection 0xc300+STD_BETA_THD2,0x001c
#define  readTHDBitStreamInformation12 readTHDBSIFBBCopyRightProtection

#define  readTHDBSIFBBSpeakerLayout 0xc300+STD_BETA_THD2,0x01e
#define  readTHDBitStreamInformation13 readTHDBSIFBBSpeakerLayout

#define  readTHDBSIFBBMultiChannelOccupancy 0xc300+STD_BETA_THD2,0x0020
#define  readTHDBitStreamInformation14 readTHDBSIFBBMultiChannelOccupancy

#define  readTHDBSIFBBMultiChannelType2 0xc300+STD_BETA_THD2,0x0022
#define  readTHDBitStreamInformation15 readTHDBSIFBBMultiChannelType2

#define  readTHDBSIFBBWordWidth 0xc300+STD_BETA_THD2,0x0024
#define  readTHDBitStreamInformation16 readTHDBSIFBBWordWidth

#define  readTHDBSIFBBFlags 0xc300+STD_BETA_THD2,0x0026
#define  readTHDBitStreamInformation17 readTHDBSIFBBFlags

#define  readTHDBSIFBBQuantWordLength2 0xc300+STD_BETA_THD2,0x0028
#define  readTHDBitStreamInformation18 readTHDBSIFBBQuantWordLength2

#define  readTHDBSIFBBSampleRate2 0xc300+STD_BETA_THD2,0x002a
#define  readTHDBitStreamInformation19 readTHDBSIFBBSampleRate2

#define  readTHDBSIFBBfs 0xc300+STD_BETA_THD2,0x002c
#define  readTHDBitStreamInformation20 readTHDBSIFBBfs


#define  readTHDBSIFBAChannelAssignment8Ch 0xc300+STD_BETA_THD2,0x0034
#define  readTHDBitStreamInformation21 readTHDBSIFBAChannelAssignment8Ch

#define  readTHDBSIFBAChannelModifier8Ch 0xc300+STD_BETA_THD2,0x0036
#define  readTHDBitStreamInformation22 readTHDBSIFBAChannelModifier8Ch

#define  readTHDBSIFBAChannelAssignment6Ch 0xc300+STD_BETA_THD2,0x0038
#define  readTHDBitStreamInformation23 readTHDBSIFBAChannelAssignment6Ch

#define  readTHDBSIFBAChannelModifier6Ch 0xc300+STD_BETA_THD2,0x003a
#define  readTHDBitStreamInformation24 readTHDBSIFBAChannelModifier6Ch

#define  readTHDBSIFBAChannelModifier2Ch 0xc300+STD_BETA_THD2,0x003c
#define  readTHDBitStreamInformation25 readTHDBSIFBAChannelModifier2Ch

#define  readTHDBSIFBAMultiChannelType8Ch 0xc300+STD_BETA_THD2,0x003e
#define  readTHDBitStreamInformation26 readTHDBSIFBAMultiChannelType8Ch

#define  readTHDBSIFBAMultiChannelType6Ch 0xc300+STD_BETA_THD2,0x0040
#define  readTHDBitStreamInformation27 readTHDBSIFBAMultiChannelType6Ch

#define  readTHDBSIFBASampleRate 0xc300+STD_BETA_THD2,0x0042
#define  readTHDBitStreamInformation28 readTHDBSIFBASampleRate

#define  readTHDBSIFBAVariableRate 0xc300+STD_BETA_THD2,0x0044
#define  readTHDBitStreamInformation29 readTHDBSIFBAVariableRate

#define  readTHDBSIFBAPeakDataRate 0xc300+STD_BETA_THD2,0x0046
#define  readTHDBitStreamInformation30 readTHDBSIFBAPeakDataRate

#define  readTHDBSIFBASubstreams 0xc300+STD_BETA_THD2,0x0048
#define  readTHDBitStreamInformation31 readTHDBSIFBASubstreams

#define  readTHDBSIFBASubstreamInfo 0xc300+STD_BETA_THD2,0x004a
#define  readTHDBitStreamInformation32 readTHDBSIFBASubstreamInfo

#define  readTHDBSIFBAControlEnabled2Ch 0xc300+STD_BETA_THD2,0x004c
#define  readTHDBitStreamInformation33 readTHDBSIFBAControlEnabled2Ch

#define  readTHDBSIFBAControlEnabled6Ch 0xc300+STD_BETA_THD2,0x004e
#define  readTHDBitStreamInformation34 readTHDBSIFBAControlEnabled6Ch

#define  readTHDBSIFBAControlEnabled8Ch 0xc300+STD_BETA_THD2,0x0050
#define  readTHDBitStreamInformation35 readTHDBSIFBAControlEnabled8Ch

#define  readTHDBSIFBAStartUpGain 0xc300+STD_BETA_THD2,0x0052
#define  readTHDBitStreamInformation36 readTHDBSIFBAStartUpGain

#define  readTHDBSIFBADialNorm2Ch 0xc300+STD_BETA_THD2,0x0054
#define  readTHDBitStreamInformation37 readTHDBSIFBADialNorm2Ch

#define  readTHDBSIFBAMixLevel2Ch 0xc300+STD_BETA_THD2,0x0056
#define  readTHDBitStreamInformation38 readTHDBSIFBAMixLevel2Ch

#define  readTHDBSIFBADialNorm6Ch 0xc300+STD_BETA_THD2,0x0058
#define  readTHDBitStreamInformation39 readTHDBSIFBADialNorm6Ch

#define  readTHDBSIFBAMixLevel6Ch 0xc300+STD_BETA_THD2,0x005a
#define  readTHDBitStreamInformation40 readTHDBSIFBAMixLevel6Ch

#define  readTHDBSIFBASourceFormat6Ch 0xc300+STD_BETA_THD2,0x005c
#define  readTHDBitStreamInformation41 readTHDBSIFBASourceFormat6Ch

#define  readTHDBSIFBADialNorm8Ch 0xc300+STD_BETA_THD2,0x005e
#define  readTHDBitStreamInformation42 readTHDBSIFBADialNorm8Ch

#define  readTHDBSIFBAMixLevel8Ch 0xc300+STD_BETA_THD2,0x0060
#define  readTHDBitStreamInformation43 readTHDBSIFBAMixLevel8Ch

#define  readTHDBSIFBASourceFormat8Ch 0xc300+STD_BETA_THD2,0x0062
#define  readTHDBitStreamInformation44 readTHDBSIFBASourceFormat8Ch

#define  readTHDBSIFBAExtraChMeaningPresent 0xc300+STD_BETA_THD2,0x0064
#define  readTHDBitStreamInformation45 readTHDBSIFBAExtraChMeaningPresent

#define  readTHDBSIFBAFlags 0xc300+STD_BETA_THD2,0x0066
#define  readTHDBitStreamInformation46 readTHDBSIFBAFlags

/* Returns the THD sync Word; This shows whether the stream format is FBA or FBB.
   Accordingly corresponding alpha codes for the returned format ca be used */

#define   readTHDBSISyncWord 0xc400+STD_BETA_THD2,0x0070


#define   readTHDStatus 0xc508,STD_BETA_THD
#define   readTHDCommon 0xc508,STD_BETA_THD2
#define   readTHDControl \
          readTHDDecodeMode, \
          readTHDDRCMode, \
          readTHDDRCBoost, \
          readTHDDRCCut, \
          readTHDLosslessMode, \
          readTHDCenterMixLevel, \
          readTHDSurroundMixLevel, \
          readTHDLFEDownmixInclude, \
          readTHDLFEDownmixVolume, \
          readTHDCRCCheckMode, \
          readTHDMinorErrorMute, \
          readTHDSpeakerRemap, \
          readTHDSamSizIgnore, \
          readTHDDialNormMode, \
          readTHD192kHzDecode, \
          readTHDSelectMinorError

#endif /* _THD_A */
