
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//

//
//

#include <acpbeta.h>

#define wroteSYSListeningModeTHXCinema 0xca00+STD_BETA_SYSIDL,0x0503
#define wroteSYSListeningModeTHXEX 0xca00+STD_BETA_SYSIDL,0x0504
#define wroteSYSListeningModeTHXMusic 0xca00+STD_BETA_SYSIDL,0x0505
#define wroteSYSListeningModeTHXGames 0xca00+STD_BETA_SYSIDL,0x0506

#define  execTHXListeningModeTHXCinema 0xc113
#define  execTHXListeningModeTHXMusic 0xc114
#define  execTHXListeningModeTHXEX 0xc115
#define  execTHXListeningModeTHXGames 0xc116

#define writeSYSSpeakerSubwBass1THXUltra2 0xca00+STD_BETA_SYSIDL,0x0b81
#define writeSYSSpeakerSubwBass1THXNoUltra2 0xca00+STD_BETA_SYSIDL,0x0b41

#define writeSYSSpeakerBackSmallFar2 0xca00+STD_BETA_SYSIDL,0x0a62
#define writeSYSSpeakerBackLargeFar2 0xca00+STD_BETA_SYSIDL,0x0a72
#define writeSYSSpeakerBackSmallVeryFar2 0xca00+STD_BETA_SYSIDL,0x0aa2
#define writeSYSSpeakerBackLargeVeryFar2 0xca00+STD_BETA_SYSIDL,0x0ab2



