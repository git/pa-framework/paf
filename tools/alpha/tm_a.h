
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Timbre Matching Algorithm alpha codes
//
//
//    

#ifndef _TM_A
#define _TM_A

#include <acpbeta.h>

#define readTMMode 0xc200+STD_BETA_TM,0x0400
#define writeTMModeDisable 0xca00+STD_BETA_TM,0x0400
#define writeTMModeEnable 0xca00+STD_BETA_TM,0x0401      

#define  readTMOperationalMode		0xc200+STD_BETA_TM,0x0500
#define writeTMOperationalModeOFF	0xca00+STD_BETA_TM,0x0500
#define writeTMOperationalModeON	0xca00+STD_BETA_TM,0x0501 
#define writeTMOperationalModeMusic	0xca00+STD_BETA_TM,0x0502

#define  readTMOperationMode		 readTMOperationalMode		
#define writeTMOperationModeOFF		writeTMOperationalModeOFF	
#define writeTMOperationModeON		writeTMOperationalModeON	 
#define writeTMOperationModeMusic	writeTMOperationalModeMusic	

#define  readTMStatus 0xc508,STD_BETA_TM
#define  readTMControl \
         readTMMode, \
         readTMOperationalMode

#endif /* _TM_A */
