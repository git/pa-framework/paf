#!/usr/bin/env python
#
# $Source: /cvsstl/ti/pa/bin/symcheck,v $
# $Revision: 1.3 $
#
# symbol check (symcheck) tool
#
# Copyright 2008 Texas Instruments Incorporated.  All rights reserved.
# Texas Instruments Proprietary and Confidential.
# Use without a license from Texas Instruments is prohibited.
#
# $Log: symcheck,v $
# Revision 1.3  2009/05/21 05:21:06  mkumar
# Rejuggled the letters representing the symbols type from STABED to DABEST :)
#
# Revision 1.2  2009/05/20 08:01:15  mkumar
# nmsetup updated
#
# Revision 1.1  2009/05/20 06:16:48  mkumar
# symbols check tool
#
#
#
#

import sys,re,os,optparse

usage= "usage: %prog [options] files"

#nm6x setup
if 'NM' in os.environ:
    nm=os.environ['NM']
    if not os.path.isfile(nm):
       print "NM enviroment variable specified, but",nm,"does not exist"
       sys.exit(1)
else:
    nm="nm6x"

#parser options
parser = optparse.OptionParser(usage)
parser.add_option("-d", action="store_true", 
			  default=False, help='Show duplicate symbols')
parser.add_option("--nm", default=nm,
			help='Absolute path of nm6x.exe if not in path (defualt:nm6x)\n OR set NM enviroment variable')
(options, args) = parser.parse_args()

#help
if len(args) == 0:
    parser.print_help()
    sys.exit(1)

symbolsall={}

# get symbols from a file
#look for lines like 0000000 T symbol
p =  re.compile('[0-9a-fA-F]{8,} [DABEST] ')
def get_symbols(fileName):
    if os.path.isfile(tmpfile):
        os.remove(tmpfile)
    os.system("\"" + options.nm + "\"" + " " + fileName + ">" + tmpfile )
    f=open(tmpfile,"rb")
    symbols={}
    for line in f:
      if p.match(line):
        sp = line.split()
        if sp[1] in symbols.keys():
          symbols[sp[1]].append(sp[2])
        else:
          symbols[sp[1]] = [sp[2]]
    #os.remove(tmpfile)
    return symbols

pid=os.getpid()
tmpfile = "sym" + str(pid) + ".tmp"

# build the dictinoary of all symbols
for file in args:
    if os.path.isfile(file):
        symbolsall[file]=get_symbols(file)

# check for duplicate symbols
if options.d:
    dups=[]
    symbollist=[]
    filelist=[]
    for file in symbolsall.keys():
       for symtype in symbolsall[file].keys():
           for sym in symbolsall[file][symtype]:
               if sym in symbollist:
                   #found duplicate
                   dups.append((sym,file))
                   if (sym,filelist[symbollist.index(sym)]) not in dups:
                       dups.append((sym,filelist[symbollist.index(sym)]))
               else:
                   symbollist.append(sym)
                   filelist.append(file)
    if len(dups):
        print "Found duplicate symbols"
        print "duplicate symbol, file"
        for sym,file in dups:
            print sym,",",file
        sys.exit(1)

sys.exit(0)
